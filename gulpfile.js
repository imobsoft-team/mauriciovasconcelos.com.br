var gulp 			= require("gulp");
var sass 			= require("gulp-sass");
var htmlmin 		= require("gulp-htmlmin");
var notify 			= require("gulp-notify");
var concat 			= require("gulp-concat");
var uglify 			= require("gulp-uglify");
var imagemin 		= require('gulp-imagemin');	
var browserSync 	= require("browser-sync").create();
var del 			= require("del");

/*/!* Tasks cached *!/
gulp.task("cache:html", function() {
	return del("./dist/!*.html")
});*/

gulp.task("cache:css", function() {
	return del("./application/css/style.css")
});

gulp.task("cache:js", function() {
	return del("./application/js/app.js")
});

gulp.task("cache:img", function() {
	return del("./application/images")
});

gulp.task("cache:fonts", function() {
	return del("./application/fonts")
});


/*/!* Task minify html *!/
gulp.task("html", ['cache:html'], function() {
	return gulp.src("./src/!*.html")
				.pipe(htmlmin({collapseWhitespace: true}))
				.pipe(gulp.dest("./dist"))
				.pipe(browserSync.stream());
});*/

/* Task compile scss to css */
gulp.task("sass", ['cache:css'], function() {
	return gulp.src("./src/scss/style.scss")
				.pipe(sass({outputStyle: 'compressed'}))
				.on('error', notify.onError({title: "erro scss", message: "<%= error.message %>"}))
				.pipe(gulp.dest("./application/css"))
				.pipe(browserSync.stream());
});

/* Task minify js */
gulp.task("js", ['cache:js'], function() {
	return gulp.src("./src/js/app.js")
				.pipe(uglify())
				.pipe(gulp.dest("./application/js"))
				.pipe(browserSync.stream());
});

/* Task concat js */
gulp.task("concat-js", function() {
	return gulp.src([
					'./src/components/jquery/dist/jquery.js',
					'./src/components/tether/dist/js/tether.js',
					'./src/components/bootstrap/dist/js/bootstrap.js',
					'./src/components/wow/dist/wow.js'
				])
				.pipe(concat("main.js"))
				.pipe(uglify())
				.pipe(gulp.dest("./application/js"))

});

/* Task move fonts  to font awesome */
gulp.task("move-fonts", ['cache:fonts'], function(){
	return gulp.src('./src/components/components-font-awesome/fonts/**.* ')
				.pipe(gulp.dest('./application/fonts'));
});


//task minify imagens
gulp.task('imagemin',['cache:img'], function(){
	return gulp.src('src/images/**/*')
			   .pipe(imagemin())
			   .pipe(gulp.dest('./application/images'));
});


/* Task server local */
/*gulp.task("server", function() {
	browserSync.init({
		server: {
			baseDir: "./dist"
		}
	});

	/!* Watch *!/
	gulp.watch("./src/scss/!**!/!*.scss", ['sass']);
	gulp.watch("./src/components/bootstrap/scss/!**!/!*.scss", ['sass']);
	gulp.watch("./src/js/!**!/!*.js", ['js']);
	gulp.watch("./src/!**!/!*", ['html']);
});*/

//gulp.task("default", ["sass", "html", "js", "concat-js", "move-fonts","imagemin", "server"]);

gulp.task("default", ["sass", "js", "concat-js", "move-fonts","imagemin"]);












