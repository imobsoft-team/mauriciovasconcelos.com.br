<link rel='stylesheet' type='text/css' href='<?php echo $local; ?>application/css/geraMaps.css' media='screen' />
<div style="float:left;width:200px;  margin-top:60px; padding-left:45px;">
<form name="frmBuscaMap" id="frmBuscaMap" method="post" action="" onSubmit="return false;">
 <input type="hidden" name="numresult" id="numresultMap" value="5" />
 <input type="hidden" name="valorini" id="valoriniMap" value="" />
 <input type="hidden" name="valorfim" id="valorfimMap" value="" />
 <input type="hidden" name="valorBus" id="valorBusMap" value="0"/> 
 
<table style="margin-top:4px;  width:220px; " border="0" cellpadding="1" align="left" cellspacing="1" id="buscaMap" >
                     <tr>
                        <td style="text-align:right; font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif;" width="25" height="28" class="busca-label">Código:</td>
                        <td class="busca-label"><div align="left">
                          <input name="codigo" id="codigoMap" type="text" class="busca-campo" style="width:142px;" onKeyPress="this.value = this.value.toUpperCase()" />
                       </div></td>
                     </tr>
					 <tr>
                        <td style="text-align:right; font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif;" class="busca-label">Op&ccedil;&atilde;o</td>
                        <td class="busca-label"><div align="left">
                          <select name="opcoes" id="opcoesMap" class="busca-campo" style="width:145px;"  onChange="listarCidadesMap(this.value,'<?php echo $local?>')">
                            <option value="0">Escolha</option>
                             <option value="1" selected="selected">Alugar</option>
                            <option value="2" selected="selected">Comprar</option>
                          </select>
                        </div></td>
                     </tr>
                     
                      <tr>
                        <td style="text-align:right; font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif;" class="busca-label">Finalidade</td>
                        <td class="busca-label"><div align="left">
                          <select style="width:145px;" class="busca-campo" name="finalidade" id="finalidadeMap">
                                        <option value="0" selected="selected"><?php echo $tradutor['buscaRapida'][1]?></option>
                                        <option value="1">Residencial</option>
                                        <option value="2">Comercial</option>					
                                          </select>
                        </div></td>
                     </tr>
                     <tr>
                        <td style="text-align:right; font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif;" class="busca-label">Tipo</td>
                        <td class="busca-label"><select name="tipo" id="tipoMap" style="width:145px;" class="busca-campo" title="Selecione o tipo de im&oacute;vel">
                          <option value="0" selected="selected">Escolha uma op&ccedil;&atilde;o</option>
                        </select></td>
                     </tr>
                     
                       <tr>
                        <td style="text-align:right; font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif;" class="busca-label"><div align="right">Cidade</div></td>
                        <td class="busca-label"><select name="cidade" style="width:145px;" id="cidadeMap" class="busca-campo" onchange="javascript: carregarBairrosMap('<?php echo $local?>')">
                          <option value="0" selected="selected">Escolha uma Op&ccedil;&atilde;o</option>
                        </select></td>
                     </tr>
                     
					  <tr><td style="text-align:right; font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif;" class="busca-label"><div align="right">Bairro</div></td>
                                    <td colspan="2"> <div align="left">
                                      <select name="bairro[]" id="bairroMap"  size="3" style="width:145px; height:70px;" multiple class="busca-campo" title="Para selecionar mais de um bairro: pressione a tecla CTRL e selecione com o mouse">
                                        <option value="0">Escolha uma Cidade</option>
                                      </select>
                                   </div></td>
                                 </tr>
								 <tr> 
                                    <td colspan="2" > <div align="center">
                                      <input type="button" style="margin-top:8px; width:80%" name="busc" id="busc" value="Buscar" onclick="buscaRapida2Map();" />									
                                    </div></td>
                                 </tr>
      </table>
</form>



<div style=" height:20px; width:100%; color:#F00; margin:auto;" id="msgCli"></div>



    <div style="margin-top:-370px; cursor:pointer; position:absolute; float:left; left:940px; color:#FFFFFF; font-weight:bold; font-size:42px" onclick="fechar('paraMaps')">X</div>
    
    
</div>

<div id="cmdFormGMaps" style="margin-top:-0px;" >

	<div id="ProgressBar">

		<div id="loaderData" ><img src="<?php echo $local; ?>application/images/maps/loading.gif" tppabs="<?php echo $local; ?>application/images/loading.gif" width="24" style="vertical-align:middle;" /> carregando dados...</div>
		<div id="txtInfo" ></div>
		<div id="barinfo" ><div id="txtBarInfo" ></div></div>
	</div>
	<div id="divMap" style="width:650px;height:330px;" ></div>  
</div>
<script type="text/javascript">
initializeMap();
listarCidadesMap(1,'<?php echo $local?>');
</script>