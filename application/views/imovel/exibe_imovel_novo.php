<?php
/**
 * Created by PhpStorm.
 * User: Diego Pedrosa
 * Date: 07/09/2015
 * Time: 12:41
 */
?>

<input type="hidden" name="marcados" id="marcados" value="<?php echo $gopag?>" />
<input type="hidden" name="marcados2" id="marcados2" value="<?php echo $gopag2?>" />
<input type="hidden" name="imoveis" id="imoveis" value="<?php echo ($imoveis);?>" />

<div class="row">
    <div class="sixteen columns">
        <div id="titulo_busca">
            <h4>Resultado da Busca</h4>
        </div>
    </div>
</div>

<?php if($imoveis && $atual <= $pags){
    for($i=0;$i<$quant && $i<$total && $aux<$total;$i++){
        if(empty($imoveis[$aux]["emp_isn"])){
            $foto = $imoveis[$aux]["foto"];
            $tip==1?$valorimo = $imoveis[$aux]["imo_val_alu"]:$valorimo = $imoveis[$aux]["imo_val_ven"];
            $tip==1?$areaimo = $imoveis[$aux]["imo_val_are"]:$areaimo = $imoveis[$aux]["imo_val_are_priv"];
            $mostra = $imoveis[$aux]["imo_isn"];
            //!empty($mostra)?$mostra=$mostra:$mostra=str_pad($imoveis[$aux][imo_isn],4,'0','STR_PAD_LEFT');
            $fotos = $imoveis[$aux]["fotos"];
?>

<div class="row">
    <script language="javascript">
        <!--
        $(function (){
            $("#gal<?php echo $imoveis[$aux]["imo_isn"]?> a").lightBox();
        });
        -->
    </script>
    <div class="sixteen columns" style="margin-bottom:20px;">
        <div id="imovel">
            <div id="barra_vermelha">
                <span class="branco">CÓDIGO:</span><span class="amarelo">#<?php echo $imoveis[$aux]["imo_isn"]?></span><span class="branco">TIPO:</span> <span class="amarelo"><?php echo $imoveis[$aux]["tim_des"]?></span>
            </div>
            <div id="foto_imovel">
                <img src="<?php echo $foto?>"/>
            </div>
            <div id="texto_imovel">
                <span class="azul">Endereço:</span><span><?php echo preg_replace('/^([\-\(\)]|.+\,\s+([0-9]+|\*+))(\s(.*|\(\)))$/','$1',$imoveis[$aux]["imo_des_end"]);?></span><br />
                <span class="azul">Bairro:</span><span><?php echo  $imoveis[$aux]["imo_des_bai"]?></span><br />
                <span class="azul">Valor:</span><span><?php echo  number_format($valorimo,2,",",".")?></span><br />
                <span class="azul">Área:</span><span><?php echo !empty($areaimo)?$areaimo.' M&sup2;':'&nbsp;'?></span>
            </div>
            <div class="clearfix"></div>
            <div id="botoes_imovel">
                <div id="botao_imprimir">
                    <a href="javascript:void(0);" <?php if(is_array($fotos)){ ?>onClick="$('#pri<?php echo $imoveis[$aux]["imo_isn"]?>').click();"<?php }else { ?>onClick="alert('Sem fotos para o imóvel!');"<?php } ?>>
                        <img src="<?php echo $imagepath;?>photos.jpg"/>
                        <span>Fotos do Imóvel</span>
                    </a>
                </div>
                <div id="botao_mapa">
                    <a href="javascript:void(0);" onclick="verViaSatelite('<?php echo $tip==1?'aluguel':'venda'?>',<?php echo $imoveis[$aux]["imo_isn"]?>,'<?php echo $local?>')">
                        <img src="<?php echo $imagepath;?>satelite.jpg"/>
                        <span>Localização no Mapa</span>
                    </a>
                </div>
                <div id="botao_amigo">
                    <a href="javascript:void(0);" onClick="javascript: popupEnvia(<?php echo $imoveis[$aux]["imo_isn"]?>,'<?php echo $local?>','<?php echo $tip==1?'aluguel':'venda'?>')">
                        <img src="<?php echo $imagepath;?>envia_amigo.jpg"/>
                        <span>Enviar a amigo</span>
                    </a>
                </div>
                <div id="botao_detalhes_imovel">
                    <a href="javascript:void(0);" onClick="window.location = '<?php echo $local?>index.php/imoveis/detalhes/<?php echo $tip==1?'aluguel':'venda'?>/<?php echo $imoveis[$aux]['tim_des']?>/<?php echo $imoveis[$aux]["imo_isn"]?>'">
                        <img src="<?php echo $imagepath;?>deta_imo.jpg"/>
                        <span>Detalhes do Imóvel</span>
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <?php
    }else if(!empty($imoveis[$aux]['emp_isn'])){
        $foto = $imoveis[$aux]['foto'];
        $fotos = $imoveis[$aux]['fotos'];
    ?>

    <script language="javascript">
        <!--
        $(function (){
            $("#galemp<?php echo $imoveis[$aux]["emp_isn"]?> a").lightBox();
        });
        -->
    </script>
    <div class="sixteen columns" style="margin-bottom:20px;">
        <div id="imovel">
            <div id="barra_vermelha">
                <span class="branco">CÓDIGO:</span><span class="amarelo">#<?php echo $imoveis[$aux]["emp_isn"]?></span><span class="branco">TIPO:</span> <span class="amarelo"><?php echo $imoveis[$aux]["tim_des"]?></span>
            </div>
            <div id="foto_imovel">
                <img src="<?php echo $foto?>"/>
            </div>
            <div id="texto_imovel">
                <span class="azul">Endereço:</span><span><?php echo preg_replace('/^([\-\(\)]|.+\,\s+([0-9]+|\*+))(\s(.*|\(\)))$/','$1',$imoveis[$aux]['emp_des']);?></span><br />
                <span class="azul">Bairro:</span><span><?php echo  $imoveis[$aux]["emp_des_bai"]?></span><br />
                <span class="azul">Valor:</span><span><?php echo  'Sob Consulta';?></span><br />
                <span class="azul">Área:</span><span><?php echo 'Sob Consulta';?></span>
            </div>
            <div class="clearfix"></div>
            <div id="botoes_imovel">
                <div id="botao_imprimir">
                    <a href="javascript:void(0);" <?php if(is_array($fotos)){ ?>onClick="$('#pri<?php echo $imoveis[$aux]["emp_isn"]?>').click();"<?php }else { ?>onClick="alert('Sem fotos para o imóvel!');"<?php } ?>>
                        <img src="<?php echo $imagepath;?>photos.jpg"/>
                        <span>Fotos do Imóvel</span>
                    </a>
                </div>
                <div id="botao_mapa">
                    <a href="javascript:void(0);" onclick="verViaSatelite('<?php echo $tip==1?'aluguel':'venda'?>',<?php echo $imoveis[$aux]["emp_isn"]?>,'<?php echo $local?>')">
                        <img src="<?php echo $imagepath;?>satelite.jpg"/>
                        <span>Localização no Mapa</span>
                    </a>
                </div>
                <div id="botao_amigo">
                    <a href="javascript:void(0);" onClick="javascript: popupEnvia(<?php echo $imoveis[$aux]["emp_isn"]?>,'<?php echo $local?>','empreendimento')">
                        <img src="<?php echo $imagepath;?>envia_amigo.jpg"/>
                        <span>Enviar a amigo</span>
                    </a>
                </div>
                <div id="botao_detalhes_imovel">
                    <a href="javascript:void(0);" onClick="window.location = '<?php echo $local?>index.php/imoveis/detalhes/empreendimento/<?php echo $imoveis[$aux]['tim_des']?>/<?php echo $imoveis[$aux]["emp_isn"]?>'">
                        <img src="<?php echo $imagepath;?>deta_imo.jpg"/>
                        <span>Detalhes do Imóvel</span>
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="galemp<?php echo $imoveis[$aux]["emp_isn"]?>">
            <?php if(is_array($fotos)){ foreach($fotos as $fot){?>
                <a href="<?php echo $fot["nome"]?>" id="priemp<?php echo $imoveis[$aux]["emp_isn"]?>" title="<?php echo $fot["descricao"]?>"></a>
            <?php } } ?>
        </div>
    </div>
        <?php
        }
        $aux++;
        }
        } else {
            ?>
            <div class="sixteen columns" style="text-align: center;">
                <h4>Sem resultado para a busca</h4>
            </div>
        <?php } ?>
</div>

<div class="row">
    <div class="sixteen columns">
        <div id="paginas_busca">
            <?php if(is_array($imoveis) && $atual<=$pags){ if($pags > 1){ ?>
                <table width="auto" border="0" align="center" style="margin: 0 auto;">
                    <tr>
                        <td align="center" style="font-size:16px;font-family: FuturaMedium;">
                            <?php if($atual <= 1){ ?>
                                <a href="javascript:void(0);" style="color:#000000"><</a>
                            <?php }else{ ?>
                                <a href="javascript:gopag('<?php echo $local?>index.php/imoveis/buscaimovel/<?php echo $atual-1?>/<?php echo $pagst?>')">< <?php echo $tradutor['buscaImoveis'][18]?></a>
                            <?php }?>&nbsp;&nbsp;&nbsp;

                            <?php for(;$x<=$limite;$x++){ if($atual == $x+$pagst){ ?>
                                <a href="javascript:void(0);" style="color:#FF0000; width: auto;"><?php echo $x+$pagst?></a>
                            <?php }else{ ?>
                                <a style="margin:0 0 0 5px;" href="javascript:gopag('<?php echo $local?>index.php/imoveis/buscaimovel/<?php echo $x+$pagst?>/<?php echo $pagst?>')"><?php echo $x+$pagst?>
                                </a>
                            <?php } } ?>&nbsp;&nbsp;&nbsp;
                            <?php if($atual >= $pags){ ?>
                                <a href="javascript:void(0);" style="color:#000000"><?php echo $tradutor['buscaImoveis'][19]?> ></a>
                            <?php }else{ ?>
                                <a href="javascript:gopag('<?php echo $local?>index.php/imoveis/buscaimovel/<?php echo $atual+1?>/<?php echo $pagst?>')">></a>
                            <?php } ?>&nbsp;&nbsp;
                            <?php if($atual >= $pags){ ?>
                                <a href="javascript:void(0);" style="color:#000000"><?php echo $tradutor['buscaImoveis'][20]?> ></a>
                            <?php }else{ ?>
                                <!--a href="javascript:gopag('<?php echo $local?>index.php/imoveis/buscaimovel/<?php echo $pags?>/<?php echo $ultimoreg?>')"><?php echo $tradutor['buscaImoveis'][20]?> ></a-->
                            <?php } ?>	</td>
                    </tr>
                </table>
            <?php } } ?>
        </div>
    </div>
</div>



