<style type="text/css">
<!--
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
}
.style3 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; font-weight: bold; }
-->
</style>
<table width="51%"  border="0" align="center">
  <tr>
    <td width="37%">
	   <img src="<?php echo $logomarca?>" width="135" height="51">
	</td>
    <td width="63%">
		<table width="100%"  border="0">
		  <tr>
			<td width="72%"><span class="style1">
		    <?php echo $endereco?>
			</span></td>
		  </tr>
		  <tr>
			<td><span class="style1">
		    <?php echo $fones?>
			</span></td>
		  </tr>
	</table>	</td>
  </tr>
</table>
<br>
<?php if(is_array($avulso)){ ?>
<table width="50%"  border="0" align="center">
<?php $qtd = count($avulso);
	 	for($i=0;$i<$qtd;$i++) {	
?>
			  <tr>
				 <td width="32%"><span class="style1"><?php echo $tradutor['impressaoBusca'][2]?></span></td>
				 <td width="68%"><span class="style1">
					<?php echo $avulso[$i]["imo_isn"]?>
				 </span></td>
			  </tr>
			  <tr>
				 <td><span class="style1"><?php echo $tradutor['impressaoBusca'][6]?></span></td>
				 <td><span class="style1">
					<?php echo $avulso[$i]["tim_des"]?>
				 </span></td>
			  </tr>
			  <tr>
				 <td><span class="style1"><?php echo $tradutor['impressaoBusca'][3]?></span></td>
				 <td><span class="style1">
					<?php echo preg_replace('/^([\-\(\)]|.+\,\s+([0-9]+|\*+))(\s(.*|\(\)))$/','$1',$avulso[$i]["imo_des_end"]);?>
				 </span></td>
			  </tr>
			  <tr>
				 <td><span class="style1"><?php echo $tradutor['impressaoBusca'][4]?></span></td>
				 <td><span class="style1">
					<?php echo $avulso[$i]["imo_des_bai"]?>
				 </span></td>
			  </tr>
			  <tr>
				 <td><span class="style1"><?php echo $tradutor['impressaoBusca'][7]?></span></td>
				 <td><span class="style1">
					R$&nbsp;<?php echo number_format($tipo=="1"?$avulso[$i]["imo_val_alu"]:$avulso[$i]["imo_val_ven"],2,",",".")?>
				 </span></td>
			  </tr>
			  <?php if(!empty($avulso[$i]["imo_val_are"])){ ?>
			  <tr>
				 <td><span class="style1"><?php echo $tradutor['impressaoBusca'][5]?></span></td>
				 <td><span class="style1">
					<?php echo $avulso[$i]["imo_val_are"]?>&nbsp;M2
				 </span></td>
			  </tr>
			  <?php }
				$crcPai = $avulso[$i]["caracs"];
				if($crcPai) {
			  ?>
			  <tr>
				 <td colspan="2"><div align="left"><span class="style3">
				 <?php echo $tradutor['impressaoBusca'][12]?> </span></div></td>
			  </tr>
			  <?php $quantidade = count($crcPai);
				  $ult = $quantidade - 1;
				  $result="";
				  for($cont=0;$cont<$quantidade;$cont++) {
					 if(($crcPai[$cont]["car_car_tipPai"] == 1) && ($cont < $ult)) {
						  $result = $result."".$crcPai[$cont]["car_car_desPai"].", ";
					}else if(($crcPai[$cont]["car_car_tipPai"] == 1) && ($cont == $ult)) {
						  $result = $result."".$crcPai[$cont]["car_car_desPai"];
					}else if(($crcPai[$cont]["car_car_tipPai"] == 2) && ($cont < $ult)) {
						$result = $result."".$crcPai[$cont]["imc_qtd"]." ".$crcPai[$cont]["car_car_desPai"].", ";
					}else if(($crcPai[$cont]["car_car_tipPai"] == 2) && ($cont == $ult)) {
						$result = $result."".$crcPai[$cont]["imc_qtd"]." ".$crcPai[$cont]["car_car_desPai"];
					}else if(($crcPai[$cont]["car_car_tipPai"] == 3) && ($cont < $ult)) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["dca_des"].", ";
					}else if(($crcPai[$cont]["car_car_tipPai"] == 3) && ($cont == $ult)) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["dca_des"];
					}else if(($crcPai[$cont]["car_car_tipPai"] == 4) && ($cont < $ult)) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["imc_des"].", ";
					}else if(($crcPai[$cont]["car_car_tipPai"] == 4) && ($cont == $ult)) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["imc_des"];
					}else if(($crcPai[$cont]["car_car_tipPai"] == 5) && ($cont < $ult) && ($crcPai[$cont]["car_car_des_uni"] == "R$")) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["car_car_des_uni"]." ".number_format($crcPai[$cont]["imc_val"],2,",",".").", ";
					}else if(($crcPai[$cont]["car_car_tipPai"] == 5) && ($cont == $ult) && ($crcPai[$cont]["car_car_des_uni"] == "R$")) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["car_car_des_uni"]." ".number_format($crcPai[$cont]["imc_val"],2,",",".");
					}else if(($crcPai[$cont]["car_car_tipPai"] == 5) && ($cont < $ult) && ($crcPai[$cont]["car_car_des_uni"] != "R$")) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["imc_val"]." ".$crcPai[$cont]["car_car_des_uni"].", ";
					}else if(($crcPai[$cont]["car_car_tipPai"] == 5) && ($cont == $ult) && ($crcPai[$cont]["car_car_des_uni"] != "R$")) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["imc_val"]." ".$crcPai[$cont]["car_car_des_uni"];
					}
				  }
			  ?>
				  <tr><td colspan="2"><span class="style1">
					<?php echo $result?>
				  </span></td>
				  </tr>
			  <?php $result = "";
				}
			  ?>
			  <tr>
				 <td colspan="2">-------------------------------------------------</td>
			  </tr>
<?php }	 	 
?>  
</table>
<br />
<?php } ?>

<?php if(is_array($lanc)){ ?>
<div style="width:100%;height:1px;border:#000000 solid 1px;"></div>
<br />
<center style="font-family:Verdana;"><?php echo $tradutor['impressaoBusca'][13]?></center>
<br />
<table width="50%"  border="0" align="center">
<?php $qtd = count($lanc);
	 	for($i=0;$i<$qtd;$i++) {	
?>
			  <tr>
				 <td width="32%"><span class="style1"><?php echo $tradutor['impressaoBusca'][2]?></span></td>
				 <td width="68%"><span class="style1">
					<?php echo $lanc[$i]["emp_isn"]?>
				 </span></td>
			  </tr>
			  <tr>
				 <td><span class="style1"><?php echo $tradutor['impressaoBusca'][8]?></span></td>
				 <td><span class="style1">
					<?php echo $lanc[$i]["emp_des"]?>
				 </span></td>
			  </tr>
			  <tr>
				 <td><span class="style1"><?php echo $tradutor['impressaoBusca'][6]?></span></td>
				 <td><span class="style1">
					<?php echo $lanc[$i]["tim_des"]?>
				 </span></td>
			  </tr>
			  <tr>
				 <td><span class="style1"><?php echo $tradutor['impressaoBusca'][3]?></span></td>
				 <td><span class="style1">
					<?php echo $lanc[$i]["emp_des_end"]?>
				 </span></td>
			  </tr>
			  <tr>
				 <td><span class="style1"><?php echo $tradutor['impressaoBusca'][4]?></span></td>
				 <td><span class="style1">
					<?php echo $lanc[$i]["emp_des_bai"]?>
				 </span></td>
			  </tr>
			  <tr>
				 <td><span class="style1"><?php echo $tradutor['impressaoBusca'][7]?></span></td>
				 <td><span class="style1">
					<?php echo $tradutor['impressaoBusca'][14]?>
				 </span></td>
			  </tr>
			  <?php if(!empty($lanc[$i]["imo_val_are"])){ ?>
			  <tr>
				 <td><span class="style1"><?php echo $tradutor['impressaoBusca'][5]?></span></td>
				 <td><span class="style1">
					<?php echo $lanc[$i]["emp_val_are"]?>&nbsp;M2
				 </span></td>
			  </tr>
			  <?php }
				$crcPai = $lanc[$i]["caracs"];
				if($crcPai) {
			  ?>
			  <tr>
				 <td colspan="2"><div align="left"><span class="style3">
				 <?php echo $tradutor['impressaoBusca'][12]?> </span></div></td>
			  </tr>
			  <?php
				  $quantidade = count($crcPai);
				  $ult = $quantidade - 1;
				  $result = "";
				  for($cont=0;$cont<$quantidade;$cont++) {
					 if(($crcPai[$cont]["car_car_tipPai"] == 1) && ($cont < $ult)) {
						  $result = $result."".$crcPai[$cont]["car_car_desPai"].", ";
					}else if(($crcPai[$cont]["car_car_tipPai"] == 1) && ($cont == $ult)) {
						  $result = $result."".$crcPai[$cont]["car_car_desPai"];
					}else if(($crcPai[$cont]["car_car_tipPai"] == 2) && ($cont < $ult)) {
						$result = $result."".$crcPai[$cont]["imc_qtd"]." ".$crcPai[$cont]["car_car_desPai"].", ";
					}else if(($crcPai[$cont]["car_car_tipPai"] == 2) && ($cont == $ult)) {
						$result = $result."".$crcPai[$cont]["imc_qtd"]." ".$crcPai[$cont]["car_car_desPai"];
					}else if(($crcPai[$cont]["car_car_tipPai"] == 3) && ($cont < $ult)) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["dca_des"].", ";
					}else if(($crcPai[$cont]["car_car_tipPai"] == 3) && ($cont == $ult)) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["dca_des"];
					}else if(($crcPai[$cont]["car_car_tipPai"] == 4) && ($cont < $ult)) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["imc_des"].", ";
					}else if(($crcPai[$cont]["car_car_tipPai"] == 4) && ($cont == $ult)) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["imc_des"];
					}else if(($crcPai[$cont]["car_car_tipPai"] == 5) && ($cont < $ult) && ($crcPai[$cont]["car_car_des_uni"] == "R$")) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["car_car_des_uni"]." ".number_format($crcPai[$cont]["imc_val"],2,",",".").", ";
					}else if(($crcPai[$cont]["car_car_tipPai"] == 5) && ($cont == $ult) && ($crcPai[$cont]["car_car_des_uni"] == "R$")) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["car_car_des_uni"]." ".number_format($crcPai[$id][$cont]["imc_val"],2,",",".");
					}else if(($crcPai[$cont]["car_car_tipPai"] == 5) && ($cont < $ult) && ($crcPai[$cont]["car_car_des_uni"] != "R$")) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["imc_val"]." ".$crcPai[$cont]["car_car_des_uni"].", ";
					}else if(($crcPai[$cont]["car_car_tipPai"] == 5) && ($cont == $ult) && ($crcPai[$cont]["car_car_des_uni"] != "R$")) {
						$result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["imc_val"]." ".$crcPai[$cont]["car_car_des_uni"];
					}
				  }
			  ?>
				  <tr><td colspan="2"><span class="style1">
					<?php echo $result?>
				  </span></td>
				  </tr>
			  <?php $result = "";
				}
			  ?>
			  <tr>
				 <td colspan="2">-------------------------------------------------</td>
			  </tr>
<?php }
?>  
</table>
<?php } ?>

<table width="50%"  border="0" align="center">
  <tr>
    <td>
      <div align="right">
        <input name="button" type="button" onClick="javascript: window.print()" value="<?php echo $tradutor['impressaoBusca'][10]?>">	
    </div></td>
    <td><input type="button" value="<?php echo $tradutor['impressaoBusca'][11]?>" onClick="javascript: window.close()"></td>
  </tr>
</table>