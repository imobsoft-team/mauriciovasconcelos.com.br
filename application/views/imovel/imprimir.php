<?php //echo "<pre>"; print_r($imovel); print_r($caracsP); die;?>

<html>
<head>
    <script type="text/javascript"> window.print(); </script>
    <style type="text/css">
        .body { font-family:Verdana; font-size:13px; width:750px; margin:0 auto; padding-top:30px; }
        .img { float:left; width:150px; height:100px; margin-right:10px; margin-left:20px; }
        .infos { float:left; width:550px; }
        .clear { clear:both; }
        .fotoImv { float:left; width:250px; margin-right:20px; margin-bottom:10px; }
        .detalhes { border:1px solid #898989; padding:10px; margin-bottom:20px; }
        .sub { position:relative; float:left; width:100%; margin-bottom:10px; font-size:15px; }
        .referencia { float: left; display: inline-block; margin-bottom: 10px; margin-top: -15px; }
        ul { margin:5px 0px; }
    </style>
</head><body><div class="body">
    <div class="topo">
        <div class="img"><img alt="<?php echo $empresa;?>" width="150" height="95" src="<?= $imagePath.$logomarca ?>"></div>
        <div class="infos">
            <?php echo $empresa;?><br>
            <?php echo $enderecoEmp;?><br>
            <?php echo $telefone;?><br>
        </div>
    </div>
    <br class="clear">
    <h1><?php echo utf8_encode($imovel[0]['imo_des_est']);?></h1>
    <span class="referencia">Código: <?php echo $imovel[0]['imo_isn'];?></span>
    <div style="float:left; width:100%;">
        <img class="fotoImv" alt="" width="250" height="190" src="<?php echo $imovel[0]['foto'];?>"><br><br><br>
        <p style="text-align:justify;"><b><?php echo utf8_encode($imovel[0]['imo_dest_det']).utf8_encode($imovel[0]['imo_dest_det_2']);?></b></p>
    </div>
    <br class="clear">
    <br class="clear">
    <div class="detalhes">
        <strong class="sub">Localização</strong><br>
        <span><strong>Tipo de imóvel:</strong> <span class="notranslate"><?php echo utf8_encode($imovel[0]['tim_des']);?></span></span><br>
        <span><strong>Endereço:</strong> <span class="notranslate"><?php echo utf8_encode($imovel[0]['imo_des_end']);?></span></span><br>
        <span><strong>Bairro:</strong> <span class="notranslate"><?php echo utf8_encode($imovel[0]['imo_des_bai']);?></span></span><br>
        <span><strong>Cidade: </strong><span class="notranslate"><?php echo utf8_encode($imovel[0]['imo_des_loc']);?></span></span><br>
        <span><strong>Área Total:</strong> <span class="notranslate"><?php echo utf8_encode($imovel[0]['imo_val_are']);?>m2</span></span><br>
        <span><strong>Valor: </strong><span class="notranslate"><?php echo number_format($imovel[0]['imo_val_alu'],2,',','.');?></span></span><br>

    </div>
    <div class="detalhes">
        <strong class="sub">Características do imóvel</strong><br><br>
        <ul>
            <?php

            //print_r($caracsP); die;

            for($i =0; $i < count($caracsP); $i++) {
                $carac = $caracsP[$i]['car_car_desPai'];
                $qtdCarac = "";
                if ($caracsP[$i]['car_car_tipPai'] == 2) { $qtdCarac = $caracsP[$i]['imc_qtd'];}
                ?>
                <li style="list-style:inside square !important; font-size:11px !important;">
                    <?php

                    if (!empty($caracsP[$i]['imc_qtd'])) {
                        echo "<strong>".utf8_encode($carac)."</strong>".': '."<span>".$caracsP[$i]['imc_qtd']."</span>";
                    } elseif (!empty($caracsP[$i]['imc_tip_con'])) {
                        echo "<strong>".utf8_encode($carac)."</strong>".': '."<span>".$caracsP[$i]['imc_tip_con']."</span>";
                    } elseif (!empty($caracsP[$i]['dca_des'])) {
                        echo "<strong>".utf8_encode($carac)."</strong>".': '."<span>".utf8_encode($caracsP[$i]['dca_des'])."</span>";
                    } elseif (!empty($caracsP[$i]['imc_des'])) {
                        echo "<strong>".utf8_encode($carac)."</strong>".': '."<span>".utf8_encode($caracsP[$i]['imc_des'])."</span>";
                    } elseif (!empty($caracsP[$i]['imc_val'])) {
                        echo "<strong>".utf8_encode($carac)."</strong>".': '."<span> R$ ".number_format($caracsP[$i]['imc_val'],2,',','.')."</span>";
                    } else {
                        echo "<strong>".utf8_encode($carac)."</strong>";
                    }

                    ?>
                </li>
            <?php }?>
        </ul>
        <br>
    </div>
</div>
</body>
</html>