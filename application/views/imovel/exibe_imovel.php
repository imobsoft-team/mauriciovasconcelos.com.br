<?php //echo "<pre>"; print_r($imoveis); die; ?>

<section>
    <input type="hidden" name="marcados" id="marcados" value="<?php echo $gopag?>" />
    <input type="hidden" name="marcados2" id="marcados2" value="<?php echo $gopag2?>" />
    <input type="hidden" name="imoveis" id="imoveis" value="<?php echo ($imoveis);?>" />
    <div class="section-locacao">
        <div class="busca-locacao">
            <div class="container">
                <div class="row wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h1 class="col headline">Imóveis para <span class="mark">Aluguel</span></h1>
                    <!--<div class="col icones">
                        <ul class="nav">
                            <li class="nav-item">
                                <a href="#" class="nav-link" data-toggle="modal" data-target=".busca-avancada" title="Busca avançada"><i class="fa fa-search-plus"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link" title="Compartilhar"><i class="fa fa-instagram"></i></a>
                            </li><li class="nav-item">
                                <a href="#" class="nav-link" data-toggle="modal" data-target=".enviar-para-amigo" title="Enviar para um amigo"><i class="fa fa-envelope-o"></i></a>
                            </li>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="fa fa-print" title="Imprimir"></i></a>
                            </li>
                        </ul>
                    </div><!-- icones -->
                </div><!-- row -->

                <div class="row">
                    <?php include_once "application/helpers/sidebarBusca.php" ?>

                    <div class="col imoveis wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                        <?php if($imoveis && $atual <= $pags){
                                for($i=0;$i<$quant && $i<$total && $aux<$total;$i++){
                                if(empty($imoveis[$aux]["emp_isn"])){
                                $foto = $imoveis[$aux]["foto"];
                                $tip==1?$valorimo = $imoveis[$aux]["imo_val_alu"]:$valorimo = $imoveis[$aux]["imo_val_ven"];
                                $tip==1?$areaimo = $imoveis[$aux]["imo_val_are"]:$areaimo = $imoveis[$aux]["imo_val_are_priv"];
                                $mostra = $imoveis[$aux]["imo_isn"];
                                //!empty($mostra)?$mostra=$mostra:$mostra=str_pad($imoveis[$aux][imo_isn],4,'0','STR_PAD_LEFT');
                                $fotos = $imoveis[$aux]["fotos"];
                        ?>
                        <div class="item-card">
                            <div class="card" style="max-width: 16rem;">
                                <ul class="nav">
                                    <!--<li class="nav-item">
                                        <a href="" class="nav-link" title="Compartilhar"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="" class="nav-link" title="Localização"><i class="fa fa-map-marker"></i></a>
                                    </li>-->
                                    <li class="nav-item">
                                        <a href="<?= $local ?>index.php/imoveis/detalhes/aluguel/<?= $imoveis[$aux]["tim_des"]?>/<?= $imoveis[$aux]["imo_isn"] ?>" class="nav-link" title="Detalhes"><i class="fa fa-eye"></i></a>
                                    </li>
                                </ul>
                                <a href="<?= $local ?>index.php/imoveis/detalhes/aluguel/<?= $imoveis[$aux]["tim_des"]?>/<?= $imoveis[$aux]["imo_isn"] ?>" >
                                    <img class="card-img-top img-fluid" style="width: 271px; height: 176px;" src="<?= $foto ?>" alt="Imóvel para locação">
                                </a>
                                <div class="card-block">
                                    <p class="card-text">
                                        <span class="endereco" style="text-transform: capitalize;"><?= utf8_encode($imoveis[$aux]["imo_des_end"]. ' - ' .$imoveis[$aux]["imo_des_bai"]);?></span><br>
                                        <span class="valor">R$ <?= number_format($imoveis[$aux]["imo_val_alu"], 2, ',', '.') ?></span>
                                    </p>
                                </div>
                                <div class="card-footer">
                                    <span><i class="fa fa-hotel"></i><?= $imoveis[$aux]['caracsDestaque'][0]['qtd_quartos'] ?> quartos</span>
                                    <span><i class="fa fa-bathtub"></i><?= $imoveis[$aux]['caracsDestaque'][0]['qtd_suites'] ?> suítes</span>
                                    <span><i class="fa fa-car"></i><?= $imoveis[$aux]['caracsDestaque'][0]['qtd_vagas'] ?> vagas</span>
                                </div>
                            </div>
                        </div>
                        <?php
                            } else if(!empty($imoveis[$aux]['emp_isn'])) {
                                $foto = $imoveis[$aux]['foto'];
                                $fotos = $imoveis[$aux]['fotos'];

                            }
                                $aux++;
                            }
                            } else {
                        ?>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                                    <h4>Sem resultado para a busca</h4>
                                </div>
                            </div>
                        <?php } ?>
                    </div><!--col 2 imoveis -->

                </div><!-- row -->
            </div><!-- container -->
        </div><!-- busca locacao -->

        <div class="container">
            <div class="paginacao wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                <nav class="navigation">
                    <?php if(is_array($imoveis) && $atual<=$pags) { if($pags > 1) { ?>
                        <ul class="pagination">
                            <?php if($atual <= 1){ ?>
                                <!--li class="navegacao ocultar"><</li-->
                            <?php } else { ?>
                                <!--li class="navegacao ocultar" onclick="gopag('<?php echo $local?>index.php/imoveis/buscaimovel/<?php echo $atual-1?>/<?php echo $pagst?>')">Anterior</li-->
                            <?php } ?>

                            <?php for(;$x<=$limite;$x++){ if($atual == $x+$pagst){ ?>
                                <li class="page-item"><a class="page-link active" href="#"><?php echo $x+$pagst?></a></li>
                            <?php } else { ?>
                                <li class="page-item" onclick="gopag('<?php echo $local?>index.php/imoveis/buscaimovel/<?php echo $x+$pagst?>/<?php echo $pagst?>')"><a class="page-link" href="#"><?php echo $x+$pagst?></a></li>
                            <?php } } ?>

                            <?php if($atual >= $pags){ ?>
                                <li class="page-item">></li>
                            <?php } else { ?>
                                <!--li class="navegacao ocultar" onclick="gopag('<?php echo $local?>index.php/imoveis/buscaimovel/<?php echo $atual+1?>/<?php echo $pagst?>')">Próximo</li-->
                            <?php } ?>

                            <?php if($atual >= $pags){ ?>
                                <!--li class="numero">Ultimo</li-->
                            <?php } else { ?>
                                <!--li class="numero" onclick="gopag('<?php echo $local?>index.php/imoveis/buscaimovel/<?php echo $pags?>/<?php echo $ultimoreg?>')">6</li-->
                            <?php } ?>
                        </ul>
                    <?php } } ?>
                </nav>
            </div><!-- paginacao -->
        </div><!-- container -->

    </div><!-- section locacao -->
</section>