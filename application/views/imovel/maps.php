<?php
/**
 * Created by PhpStorm.
 * User: Diego Pedrosa
 * Date: 09/09/2015
 * Time: 10:33
 */
?>
<div style="background-color:#fff; width:auto;">
<div style="float:left;width:200px;margin-top:60px; padding-left:45px;">
<form name="frmBuscaMap" id="frmBuscaMap" method="post" action="" onSubmit="return false;">
    <input type="hidden" name="numresult" id="numresultMap" value="5" />
    <input type="hidden" name="valorini" id="valoriniMap" value="" />
    <input type="hidden" name="valorfim" id="valorfimMap" value="" />
    <input type="hidden" name="valorBus" id="valorBusMap" value="0"/>
    <table style="text-align:left;">
        <tr>
            <td><label>C&oacute;digo:</label></td>
            <td><input type="text" name="codigo" id="codigoMap" onKeyPress="this.value = this.value.toUpperCase()"/></td>
        </tr>
        <tr>
            <td><label>Op&ccedil;&atilde;o:</label></td>
            <td>
                <select style="width:150px;" name="opcoes" id="opcoesMap" onChange="listarCidadesMap(this.value,'<?php echo $local?>')">
                    <option value="0">Escolha</option>
                    <option value="1" selected="selected">Alugar</option>
                    <!--option value="2" selected="selected">Comprar</option-->
                </select>
            </td>
        </tr>
        <tr>
            <td><label>Finalidade:</label></td>
            <td>
                <select style="width:150px;" name="finalidade" id="finalidadeMap" onChange="listarCidadesMap(this.value,'<?php echo $local?>')">
                    <option value="0" selected="selected"><?php echo $tradutor['buscaRapida'][1]?></option>
                    <option value="1">Residencial</option>
                    <option value="2">Comercial</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><label>Tipo:</label></td>
            <td>
                <select style="width:150px;" name="tipo" id="tipoMap">
                    <option value="0" selected="selected">Escolha uma op&ccedil;&atilde;o</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><label>Cidade:</label></td>
            <td>
                <select style="width:150px;" name="cidade" id="cidadeMap" onchange="javascript: carregarBairrosMap('<?php echo $local?>')">
                    <option value="0" selected="selected">Escolha</option>
                </select>
            </td>
        </tr>
        <!--tr>
            <td><label>Valor:</label></td>
            <td>
                <div><input type="text" style="width:75px; float:left;"  id="valorini" name="valorini"  size="5" onKeyPress="javascript: return validaNumeros2(this,event)"/></div>
                <div style="float:left;">a</div>
                <div><input type="text" style="width:75px; float:left;" id="valorfim" name="valorfim"  size="5" onKeyPress="javascript: return validaNumeros2(this,event)"/></div>
            </td>
        </tr-->
        <tr>
            <td><label>Bairro:</label></td>
            <td>
                <select style="width:150px;height:75px;" name="bairro[]" id="bairroMap" multiple="multiple" title="Para selecionar mais de um bairro: pressione a tecla CTRL e selecione com o mouse" >
                    <option>Selecione</option>
                </select>
            </td>
        </tr>
    </table>
    <button class="btn" style="margin-left: 65px; margin-top:00px; width:80px; height: 25px; padding:2px; cursor:pointer;" onclick="buscaRapida2Map();">Buscar</button>
</form>
</div>

<div style=" height:20px; width:100%; color:#F00; margin:auto;" id="msgCli"></div>



<div style="background-color:#fff; width:50px; heigth:auto; margin-top:0px; cursor:pointer; position:absolute; float:left; left:970px; color:#000; font-weight:bold; font-size:42px" onclick="fechar('paraMaps')">X</div>


</div>

<div id="cmdFormGMaps" style="margin-top:-19px; margin-left:110px;" >

    <div id="ProgressBar">

        <div id="loaderData" ><img src="<?php echo $local; ?>application/images/maps/loading.gif" tppabs="<?php echo $local; ?>application/images/loading.gif" width="24" style="vertical-align:middle;" /> carregando dados...</div>
        <div id="txtInfo" ></div>
        <div id="barinfo" ><div id="txtBarInfo" ></div></div>
    </div>
    <div id="divMap" style="width:650px;height:330px;" ></div>
</div>


<script type="text/javascript">
    initializeMap();
    listarCidadesMap(1,'<?php echo $local?>');
</script>