<?php

//echo "<pre>"; print_r($imovel);

if(empty($imovel[0]["imo_isn"])){
    ?>
    <script type="text/javascript">
        <!--
        alert('Imovel não encontrado!');
        window.location = "<?php echo $local?>";
        -->
    </script>
    <?php
    die;
}

?>

<main>
    <section>
        <div class="detalhes-imovel-aluguel">
            <div class="dados-imovel">
                <div class="container">
                    <div class="row">
                        <div class="col endereco wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                            <p class="codigo">Código: <?= $imovel[0]["imo_isn"] ?></p>
                            <p class="logradouro"><?= utf8_encode($imovel[0]["imo_des_end"]. ' - ' .$imovel[0]['imo_des_bai']); ?></p>
                            <p class="localidade"><?= $imovel[0]["imo_des_loc"] ?></p>
                        </div><!-- col 1 endereco -->

                        <div class="col valor wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                            <p class="parcela">Aluguel: <span class="mark">R$ <?= number_format($imovel[0]["imo_val_alu"], 2, ',', '.'); ?></span></p>
                            <div class="icones">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $local?>index.php/imoveis/detalhes/aluguel/<?php echo utf8_encode($imovel[0]['tim_des'])?>/<?php echo $imovel[0]['imo_isn']?>" class="nav-link" title="Compartilhar"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a target="_blank" href="http://web.whatsapp.com/send?text=Mota%20da%20Costa:%20<?php echo htmlentities($local.'index.php/imoveis/detalhes/aluguel/'.utf8_encode($imovel[0]['tim_des']).'/'.$imovel[0]['imo_isn'])?>" class="nav-link" title="Compartilhar"><i class="fa fa-whatsapp"></i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:void(0)" onClick="popupFicha(<?php echo $imovel[0]["imo_isn"]?>,'<?php echo $local?>','<?php echo $tipo?>')" class="nav-link" title="Ficha do Imóvel"><i class="fa fa-file-text-o"></i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" data-toggle="modal" data-target=".documentos" title="Documentos para download"><i class="fa fa-download"></i></a>
                                    </li>
                                    <!--li class="nav-item">
                                        <a href="" class="nav-link"><i class="fa fa-print" title="Imprimir"></i></a>
                                    </li-->
                                </ul>
                            </div>
                        </div><!-- col 2 valor-->
                    </div><!-- row -->

                    <div class="row">
                        <div class="col contato wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                            <h1 class="headline">Gostou? Fale conosco!</h1>
                            <form name="frmInteresse" id="frmInteresse" method="post" action="<?php echo $local; ?>index.php/imoveis/envioInteresse" onsubmit="return false">
                                <input type="hidden" name="tipo" value="aluguel" />
                                <input type="hidden" name="codigo" value="<?php echo $imovel[0]["imo_isn"];?>" />
                                <div class="form-group">
                                    <input name="nome" type="text" class="form-control input-custom" placeholder="Nome" required>
                                </div>
                                <div class="form-group">
                                    <input name="emailInteresse" type="email" class="form-control input-custom" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <input name="fone" type="text" class="form-control fone input-custom" placeholder="Telefone" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="codigo" id="codigo" readonly="true" style="background-color: #fff;" class="form-control input-custom" value="<?php echo $imovel[0]["imo_isn"];?>" placeholder="Codigo do Imovel" />
                                </div>
                                <div class="form-group">
                                    <textarea name="mensagem" class="form-control input-custom textarea-custom" placeholder="Mensagem"></textarea>
                                </div>
                                <div class="form-group">
                                    <button onClick="validaInteresse()" class="btn btn-secondary input-custom"><i class="fa fa-send"></i>
                                        Enviar
                                    </button>
                                </div>
                            </form>
                        </div><!-- col 1 form de contato-->

                        <div class="col slide wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                            <div id="detalhes-imovel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner" role="listbox">
                                    <?php $x = 0; if($fotos){ for($i=0; $i<count($fotos);$i++){ $foto = $fotos[$i]["nome"]; $descricao = $fotos[$i]["descricao"]; ?>
                                        <div class="carousel-item <?= ($i==0?'active':'') ?>">
                                            <img class="d-block img-fluid" style="width: 715px; height: 470px;" src="<?= $foto;?>" alt="<?= utf8_encode($descricao);?>">
                                        </div>
                                        <?php $x++; }  } else { ?>
                                        <div class="carousel-item active">
                                            <img class="d-block img-fluid" style="width: 715px; height: 470px;" src="<?php echo $imagepath;?>indisponivel.jpg" alt="<?= utf8_encode($descricao);?>">
                                        </div>
                                    <?php } ?>
                                </div>
                                <a class="carousel-control-prev" href="#detalhes-imovel" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#detalhes-imovel" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div><!-- col 2 slide -->
                    </div><!-- row -->
                </div><!-- container -->
            </div><!-- dados do imovel -->
            <div class="descricao-localizacao">
                <div class="container">
                    <div class="row">
                        <div class="col descricao wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                            <h1 class="headline">
                                <i class="fa fa-pencil-square-o"></i>
                                Características
                            </h1>
                            <ul>
                                <?php

                                //print_r($caracsP); die;

                                for($i =0; $i < count($caracsP); $i++) {
                                    $carac = $caracsP[$i]['car_car_desPai'];
                                    $qtdCarac = "";
                                    if ($caracsP[$i]['car_car_tipPai'] == 2) { $qtdCarac = $caracsP[$i]['imc_qtd'];}
                                    ?>
                                    <li style="list-style:inside square !important; font-size:11px !important;">
                                        <?php

                                        if (!empty($caracsP[$i]['imc_qtd'])) {
                                            echo "<strong>".utf8_encode($carac)."</strong>".': '."<span>".$caracsP[$i]['imc_qtd']."</span>";
                                        } elseif (!empty($caracsP[$i]['imc_tip_con'])) {
                                            echo "<strong>".utf8_encode($carac)."</strong>".': '."<span>".$caracsP[$i]['imc_tip_con']."</span>";
                                        } elseif (!empty($caracsP[$i]['dca_des'])) {
                                            echo "<strong>".utf8_encode($carac)."</strong>".': '."<span>".utf8_encode($caracsP[$i]['dca_des'])."</span>";
                                        } elseif (!empty($caracsP[$i]['imc_des'])) {
                                            echo "<strong>".utf8_encode($carac)."</strong>".': '."<span>".utf8_encode($caracsP[$i]['imc_des'])."</span>";
                                        } elseif (!empty($caracsP[$i]['imc_val'])) {
                                            echo "<strong>".utf8_encode($carac)."</strong>".': '."<span> R$ ".number_format($caracsP[$i]['imc_val'],2,',','.')."</span>";
                                        } else {
                                            echo "<strong>".utf8_encode($carac)."</strong>";
                                        }

                                        ?>
                                    </li>
                                <?php }?>
                            </ul>

                        </div><!-- descrica do imovel -->

                        <div class="caracteristicas">
                            <div class="col icones">
                            <?php if ($imovel[0]['imo_finalidade'] == 1) { ?>
                                <ul class="nav">
                                    <li class="nav-item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                                        <i class="fa fa-hotel"></i><?= ($imovel[0]['caracsDestaque'][0]['qtd_quartos']?$imovel[0]['caracsDestaque'][0]['qtd_quartos']:'0'); ?> quartos
                                    </li>
                                    <li class="nav-item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                                        <i class="fa fa-bathtub"></i><?= ($imovel[0]['caracsDestaque'][0]['qtd_suites']?$imovel[0]['caracsDestaque'][0]['qtd_suites']:'0'); ?> suítes
                                    </li>
                                    <li class="nav-item wow fadeIn" data-wow-duration="1s" data-wow-delay="1.2s">
                                        <i class="fa fa-car"></i><?= ($imovel[0]['caracsDestaque'][0]['qtd_vagas']?$imovel[0]['caracsDestaque'][0]['qtd_vagas']:'0'); ?> vagas
                                    </li>
                                    <li class="nav-item wow fadeIn" data-wow-duration="1s" data-wow-delay="1.5s">
                                        <i class="fa fa-crop"></i><?= $imovel[0]["imo_val_are"]; ?> m2
                                    </li>
                                </ul>
                            <?php } else { ?>
                                <ul class="nav">
                                    <li class="nav-item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                                        <i class="fa fa-object-ungroup"></i><?= ($imovel[0]['imo_val_are']?$imovel[0]['imo_val_are']:'0'); ?> m2
                                    </li>
                                    <li class="nav-item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                                        <i class="fa fa-bathtub"></i><?= ($imovel[0]['caracsDestaque'][0]['qtd_banheiros']?$imovel[0]['caracsDestaque'][0]['qtd_banheiros']:'0'); ?> banheiros
                                    </li>
                                    <li class="nav-item wow fadeIn" data-wow-duration="1s" data-wow-delay="1.2s">
                                        <i class="fa fa-car"></i><?= ($imovel[0]['caracsDestaque'][0]['qtd_vagas']?$imovel[0]['caracsDestaque'][0]['qtd_vagas']:'0'); ?> vagas
                                    </li>
                                    <!-- <li class="nav-item wow fadeIn" data-wow-duration="1s" data-wow-delay="1.5s">
                                        <i class="fa fa-crop"></i><?= $imovel[0]["imo_val_are"]; ?> m2
                                    </li> -->
                                </ul>
                            <?php } ?>

                            </div>
                        </div><!-- principais caracteristicas -->
                    </div><!-- row -->

                    <div class="localizacao wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                        <h1 class="headline">
                            <i class="fa fa-map-marker"></i>
                            Localização
                        </h1>
                        <div class="mapa-localizacao">
                            <?php

                                $bairro   = $imovel[0]["imo_des_bai"];
                                $cidade   = $imovel[0]["imo_des_loc"];
                                $endereco = preg_replace('/^([\-\(\)]|.+\,\s+([0-9]+|\*+))(\s(.*|\(\)))$/','$1',$imovel[0]["imo_des_end"])." ".$bairro." ".$cidade;

                            ?>

                            <iframe id="enderecoMapa" name="enderecoMapa" width="100%" height="465px" style="margin-top:0px;" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?q=<?php echo $endereco ?>&spn=0.043344,0.063343&t=M&hl=pt&output=embed"></iframe><br />
                        </div>
                    </div><!-- localizacao -->
                </div><!-- container -->
            </div><!-- descricao e localizacao -->
        </div><!-- detalhes do imovel -->
    </section>
</main>

<script type="text/javascript">

    function validaInteresse() {
        nome  = 		  $("#nome");
        email = 		  $("#emailInteresse");
        fone = 		  	  $("#fone");
        codigo = 	      $("#codigo");
        mensagem =        $("#mensagem");

        if(!nome.val()) {
            alert("É necessário preencher o campo Nome");
            nome.focus();
            return false;
        }else if(!email.val()) {
            alert("É necessário preencher o campo Email");
            email.focus();
            return false;
        }else if(!fone.val()) {
            alert("É necessário preencher o campo Fone");
            fone.focus();
            return false;
        }else if(!mensagem.val()) {
            alert("É necessário preencher o campo Mensagem");
            mensagem.focus();
            return false;
        }
        document.frmInteresse.submit();
    }
</script>
