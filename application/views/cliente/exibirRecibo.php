<?php
include("application/helpers/classBarraC.php");
if($logo_bco == "images/LogUnibanco.jpg") {
   $aviso = "Instruções: Texto de Responsabilidade do Cedente";
}
$x = 0;
$z = 12;
for($y=0;$y < 4;$y++) {
  $lin_dig = $lin_dig.substr($dadosRec[$contador]["rec_des_cod_bar"],$x,$z);
  $x = $x + 12;
  $z = 12;
  $lin_dig = $lin_dig."&nbsp;&nbsp;";
}  

$html = '

<style type="text/css">
@media print {
	#boleto {
		width: auto;
	}
	
	#imprimir_botao {
		display: none;
	}
}
<!--
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.style2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
}
.style4 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; font-style: italic; }
.style6 {font-size: 10}
.style7 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-style: italic;
}
.style12 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; }
.style14 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; }
.style15 {font-family: Arial, Helvetica, sans-serif}
-->
</style>

'; 

?>

<div id="imprimir_botao" style="width: 210mm; padding-bottom: 5px;">
<input type="button" value="    Imprimir   " onclick="window.print();" style="float: right; font-size: 15px;" />
<div style="clear: both;"></div>
</div>

<?php $html .= '
<div style="width: 210mm;" id="boleto">
<table width="'.$width.'"  border="1" align="center" cellpadding="0" cellspacing="0" bordercolorlight="#999999">
  <tr>
    <td width="30%">
	    <div align="center"><img src="'.$logomarca.'" width="200" height="95"> </div></td>
    <td width="70%">
	  <table width="100%"  border="0">
		  <tr>
			<td><div align="center"><strong><span class="style1">RECIBO DE COBRAN&Ccedil;A </span></strong></div></td>
		  </tr>
		  <tr>
			<td>			  <div align="center"><span class="style2">
	          '.$endereco.'
			  </span></div>
			</td>
		  </tr>
		  <tr>
			<td>
			  <div align="center"><span class="style2">
	             '.$fones.'
		      </span></div>
			</td>
		  </tr>
	  </table>
	</td>
  </tr>
</table>
<br>
<table width="'.$width.'"  border="1" align="center" cellpadding="0" cellspacing="0" bordercolorlight="#999999">
  <tr height="23">
    <td colspan="6"><span class="style2">&nbsp;Inquilino / Comprador<br>&nbsp;'.utf8_encode($dadosRec[$contador]["rec_nom_inq"]).'</span></td>
    <td colspan="2"><span class="style2">&nbsp;CPF/CGC<br>&nbsp;'.utf8_encode($dadosRec[$contador]["rec_num_cpf_cgc_inq"]).'</span></td>
  </tr>
  <tr height="23">
    <td colspan="5"><span class="style2">&nbsp;Endere&ccedil;o<br>&nbsp;'.utf8_encode($dadosRec[$contador]["rec_des_end"]).'</span></td>
    <td colspan="3"><span class="style2">&nbsp;Bairro<br>&nbsp;'.utf8_encode($dadosRec[$contador]["rec_des_bai"]).'</span></td>
  </tr>
  <tr height="23">
    <td colspan="2"><span class="style2">&nbsp;N&uacute;mero<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'.utf8_encode($dadosRec[$contador]["rec_num"]).'</strong></span></td>
    <td width="13%"><span class="style2">&nbsp;Vencimento<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'.utf8_encode($dadosRec[$contador]["rec_dat_vct"]).'</strong></span></td>
    <td width="15%"><span class="style2">&nbsp;Dt. Emiss&atilde;o<br>&nbsp;&nbsp;&nbsp;&nbsp;'.utf8_encode($dadosRec[$contador]["rec_dat_emi"]).'</span></td>
    <td width="16%"><span class="style2">&nbsp;Car&ecirc;ncia at&eacute; dia<br>
    &nbsp;&nbsp;&nbsp;&nbsp;'.utf8_encode($dadosRec[$contador]["rec_dst_car"]).'</span></td>
    <td width="15%"><span class="style2">&nbsp;Reajuste<br>&nbsp;&nbsp;&nbsp;&nbsp;'.utf8_encode($dadosRec[$contador]["rec_dat_reaj"]).'</span></td>
    <td width="15%"><span class="style2">&nbsp;Mora / dia (R$)<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.number_format($dadosRec[$contador]["rec_val_jur_mora"],2,",",".").'</span></td>
    <td width="14%"><span class="style2">&nbsp;Multa (R$)<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.number_format($dadosRec[$contador]["rec_val_mul"],2,",",".").'</span></td>
  </tr>
  <tr height="9">
    <td width="6%" height="9"><div align="center"><span class="style2">Tipo</span></div></td>
    <td width="6%" colspan="5"><div align="center"><span class="style2">Descri&ccedil;&atilde;o</span></div></td>
    <td><div align="center"><span class="style2">Data</span></div></td>
	<td><div align="center"><span class="style2">Valor (R$) </span></div></td>
  </tr>
</table>
<table width="'.$width.'"  border="1" align="center"  bordercolor="#FFFFFF" bgcolor="#E8E8E8">
';
is_array($itensRec)?$quant=count($itensRec):$quant=0;
for($i=0;$i<$quant;$i++) {
  if(($itensRec[$i]["irc_tip"]) && ($itensRec[$i]["irc_des"]) && ($itensRec[$i]["irc_dat"]) && ($itensRec[$i]["irc_val"])) { 

  $html .= '
  <tr>
    <td width="6%"><div align="center"><span class="style2">
        '.utf8_encode($itensRec[$i]["irc_tip"]).'
    </span></div></td>
    <td width="65%"><div align="left"><span class="style2">
        '.utf8_encode($itensRec[$i]["irc_des"]).'
    </span></div></td>
    <td width="15%"><div align="center"><span class="style2">
        '.utf8_encode($itensRec[$i]["irc_dat"]).'
    </span></div></td>
    <td width="14%"><div align="right"><span class="style2">
        '.number_format($itensRec[$i]["irc_val"],2,",",".").'
    </span></div></td>
  </tr>
';
 } 
 else {
 $html .= '
  <tr>
    <td width="6%"><div align="center"><span class="style2">
     &nbsp;    
    </span></div></td>
    <td width="65%"><div align="center"><span class="style2">
        &nbsp;
    </span></div></td>
    <td width="15%"><div align="center"><span class="style2">
        &nbsp;
    </span></div></td>
    <td width="14%"><div align="right"><span class="style2">
        &nbsp;
    </span></div></td>
  </tr>
';
 }
}
$html .= '
</table>
<table width="'.$width.'"  border="1" align="center" cellpadding="0" cellspacing="0" bordercolorlight="#999999">
  <tr height="23">
    <td colspan="4"><span class="style4">&nbsp;&nbsp;<span class="style6">Este recibo n&atilde;o quita d&eacute;bitos anteriores</span> </span></td>
    <td width="19%"><span class="style2">&nbsp;Parcela base (R$)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      '.number_format($dadosRec[$contador]["rec_val_alu_base"],2,",",".").'
    </span></td>
    <td width="14%"><span class="style2">&nbsp;Desconto (R$)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      '.number_format($dadosRec[$contador]["rec_val_des"],2,",",".").'
    </span></td>
    <td width="19%" colspan="2"><span class="style2">&nbsp;Total (R$)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<strong>'.number_format($dadosRec[$contador]["rec_val_tot"],2,",",".").'</strong>
&nbsp;
    </span></td>
  </tr>
  <tr>
    <td colspan="6" rowspan="2">
	</td>
    <td height="23" colspan="2"><span class="style2">&nbsp;Encargos (R$)<br>&nbsp;</span></td>
  </tr>
    <tr>
    <td height="23" colspan="2"><span class="style2">&nbsp;Total a pagar (R$) <br>
      &nbsp;</span></td>
  </tr>
  <tr height="23">
    <td colspan="4"><span class="style2">&nbsp;Nome do Cedente<br>&nbsp;'.utf8_encode($dadosBlt["blt_nom_ced"]).'</span></td>
	<td colspan="2"><span class="style2">&nbsp;Ag&ecirc;ncia / C&oacute;digo do Cedente<br>&nbsp;'.utf8_encode($dadosBlt["blt_des_age_cod_ced"]).'</span></td>
	<td colspan="2"><span class="style2">&nbsp;Nosso n&uacute;mero<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.utf8_encode($dadosBlt["blt_des_nos_num"]).'</span></td>
  </tr>
  <tr height="13">
    <td colspan="8"><span class="style7">&nbsp;Via do sacado - autentica&ccedil;&atilde;o Mec&acirc;nica </span></td>
  </tr>
</table>
';
if($dadosBlt) {
	
$html .= '

  <table width="91%"  border="0" align="center">
  <tr>
    <td width="17%" height="18"><div align="center"><img src="'.$logo_bco.'" width="100" height="50"></div></td>
	<td width="2%"><div align="center"><img src="'.$local.'application/images/barra.jpg"></div></td>
    <td width="9%"><div align="center"><span class="style12">'.utf8_encode($dadosBlt["blt_cod_bco"]).'</span></div></td>
	<td width="2%"><div align="center"><img src="'.$local.'application/images/barra.jpg"></div></td>
	<td width="70%"><div align="left"><span class="style12">
        '.utf8_encode($dadosBlt["blt_des_lin_dig"]).' 
    </span></div></td>
  </tr>
</table>
<table width="'.$width.'"  border="1" align="center" cellpadding="0" cellspacing="0" bordercolorlight="#999999">
  <tr height="10">
    <td colspan="5">
		 <span class="style2">&nbsp;Local de Pagamento <br>
		 &nbsp;'.utf8_encode($dadosBlt["blt_des_loc_pag"]).'</span>
	 </td>
    <td colspan="2">
		<span class="style2">&nbsp;Vencimento<br>&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'.utf8_encode($dadosBlt["blt_des_vct"]).'</strong></span>
	 </td>
  </tr>
  <tr height="23">
    <td height="25" colspan="5"><span class="style2">&nbsp;Cedente<br>
    &nbsp;'.utf8_encode($dadosBlt["blt_nom_ced"]).'</span></td>
    <td colspan="2"><span class="style2">&nbsp;Ag&ecirc;ncia / c&oacute;digo do Cedente<br>&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;'.utf8_encode($dadosBlt["blt_des_age_cod_ced"]).'</span></td>
  </tr>
  <tr height="23">
    <td width="16%"><div align="center"><span class="style2">&nbsp;Data do documento<br>
    &nbsp;'.utf8_encode($dadosBlt["blt_dat_doc"]).'</span></div>	</td>
    <td width="14%"><div align="center"><span class="style2">&nbsp;N&ordm; do Documento<br>
    &nbsp;'.utf8_encode($dadosBlt["blt_num_doc"]).'</span></div>	</td>
    <td width="13%"><div align="center"><span class="style2">&nbsp;Esp&eacute;cie Doc.<br>
    &nbsp;
      '.utf8_encode($dadosBlt["blt_des_esp_doc"]).'
    </span></div></td>
    <td width="12%"><div align="center"><span class="style2">&nbsp;Aceite<br>
    &nbsp;
      '.utf8_encode($dadosBlt["blt_des_aceite"]).'
    </span></div></td>
    <td width="19%"><div align="center"><span class="style2">&nbsp;Data do Processamento<br>
    &nbsp;'.utf8_encode($dadosBlt["blt_dat_proc"]).'</span></div>	</td>
    <td colspan="2"><span class="style2">&nbsp;Nosso n&uacute;mero<br>&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;'.utf8_encode($dadosBlt["blt_des_nos_num"]).'</span></td>
  </tr>
  <tr height="23">
    <td>
	  <div align="center"><span class="style2">&nbsp;Uso do Banco<br>
	  &nbsp;
	    '.utf8_encode($dadosBlt["blt_des_uso_bco"]).'
	    </span>
	    </div></td>
	<td><div align="center"><span class="style2">Carteira<br>
	&nbsp;'.utf8_encode($dadosBlt["blt_des_cart"]).'</span></div></td>
    <td width="13%"><div align="center"><span class="style2">&nbsp;Esp&eacute;cie Moeda<br>
    &nbsp;'.utf8_encode($dadosBlt["blt_des_esp_moeda"]).'</span></div>	</td>
    <td width="12%"><div align="center"><span class="style2">&nbsp;Quantidade<br>
    &nbsp;</span></div></td>
	<td>
	   <div align="center"><span class="style2">&nbsp;Contrato<br>
	   &nbsp;
        <nobr>'.utf8_encode($dadosBlt["blt_des_ctr"]).'</nobr>
	     </span>
	     </div></td>
	<td colspan="2"><span class="style2">&nbsp;(=) Valor do documento<br>&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'.utf8_encode($dadosBlt["blt_val"]).'</strong></span></td>
  </tr>
  <tr>
    <td colspan="5" rowspan="5" valign="top">
	    <span class="style14">&nbsp;<strong>'.$aviso.'</strong>&nbsp;<br>&nbsp;Mensagens<br>
<br><br>
';
		is_array($dadosIbl)?$cont = count($dadosIbl):$cont=0;
		for($i=0;$i<$cont;$i++) {
$html .= '
		&nbsp;'.utf8_encode($dadosIbl[$i]["ibl_des_msg"]).'<br>
';
		}
$html .= '
		</span>
	</td>
    <td  height="23"colspan="2"><span class="style2">&nbsp;(-)Desconto<br>
    &nbsp;&nbsp;&nbsp;&nbsp;
      *********************</span></td>
  </tr>
  <tr>
    <td colspan="2" height="28"><span class="style2">&nbsp;(-) Outras Dedu&ccedil;&otilde;es / abatimento<br>
    &nbsp;&nbsp;&nbsp;**********************</span></td>
  </tr>
  <tr>
    <td colspan="2"  height="27"><span class="style2">&nbsp;(+)&nbsp;Mora / Multa /Juros<br>&nbsp;</span></td>
  </tr>
  <tr>
    <td colspan="2" height="27"><span class="style2">&nbsp;(+) Outros Acr&eacute;scimos<br>&nbsp;</span></td>
  </tr>
  <tr>
    <td width="26%" colspan="2" height="28"><span class="style2">&nbsp;(=) Valor Cobrado<br>&nbsp;</span></td>
  </tr>
  <tr height="23">
    <td colspan="7" valign="top"><span class="style2">&nbsp;Sacado&nbsp;&nbsp;'.utf8_encode($dadosBlt["blt_nom_sac"]).'<br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.utf8_encode($dadosBlt["blt_des_end"]).'<br>&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;'.utf8_encode($dadosBlt["blt_des_bai"]).' / '.utf8_encode($dadosBlt["blt_des_cid"]).' - '.utf8_encode($dadosBlt["blt_des_uf"]).' '.utf8_encode($dadosBlt["blt_des_cep"]).' </span></td>
  </tr>
</table>
<table width="'.$width.'"  border="0" align="center">
  <tr>
    <td width="55%" rowspan="2">
';
    $cod = new WBarCode($local);

$html .= $cod->gerarCode($dadosBlt["blt_cod_bar"]).'
	</td>
    <td width="30%"><table width="100%"  border="0">
  <tr>
    <td><div align="center"><strong><span class="style14">Ficha de Compensa&ccedil;&atilde;o </span></strong></div></td>
  </tr>
  <tr>
    <td>
	  <div align="center"><span class="style2">
	     <img src="'.$local.'/application/images/autenticacao.jpg" width="200" height="41"></span>          
	  </div>
	</td>
  </tr>
</table>
</td>
  </tr>
</table>
';
}

$html .= '
</div>

'; 

$mpdf->mirrorMargins = true;
$mpdf->SetDisplayMode('fullpage','two');
$mpdf->WriteHTML($html);
$mpdf->Output();

/*$dompdf->load_html($html);
$paper_orientation = 'portrait';
$customPaper = array(0,0,950,950);
$dompdf->set_paper($customPaper,$paper_orientation);
//$dompdf->set_paper('letter', 'portrait');
$dompdf->render();
$dompdf->stream("exemplo-01.pdf");*/
die;

?>

