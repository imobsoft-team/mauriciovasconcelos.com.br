<style type="text/css">
@media print {
	#pcconta {
		width: auto;
	}
	
	#imprimir {
		display: none;
	}
}
<!--
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
}
.style2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 9px;
}
.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
}
.style5 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16;
}
-->
</style>
<div id="pcconta" style="width: 210mm">
<table width="100%"  border="0" cellspacing="1">
<tr><td colspan="9" align="center" class="style4 style5">PRESTA&Ccedil;&Atilde;O DE CONTAS</td>
</tr>
  <tr>
    <td width="21%"><div align="center"><img width="230" height="100"  src="<?= $imagePath.'logotipo.png'?>"></div></td>
    <td width="51%"><span class="style1">
	   <strong>RAZ&Atilde;O SOCIAL:</strong>&nbsp;<?php echo $empresa?><BR>
	   <strong>ENDERE&Ccedil;O:</strong>&nbsp;<?php echo $endereco?>
      <strong>CPF/CNPJ:</strong>&nbsp;<?php echo $cnpj?></span></td>
    <td width="28%"><div align="right">
      <p class="style1">Impresso em <?php echo $data?></p>
      <p class="style1">&nbsp;</p>
    </div></td>
  </tr>
  <tr>
  </tr>
</table>

<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="290" height="5">
	   <span class="style1"><strong>&nbsp;PROPRIET&Aacute;RIO</strong><br>&nbsp;<?php echo $pc["pco_nom_prop"]?></span>	</td>
    <td width="118"><strong><span class="style1">&nbsp;CPF/CGC:</span></strong><br>
    <span class="style1">&nbsp;<?php echo $pc["pco_num_cpf_cgc"]?></span></td>
    <td width="146"><strong><span class="style1">&nbsp;VENCIMENTO</span></strong><br>
    <span class="style1">&nbsp;<?php echo $pc["pco_dat_vct"]?></span></td>
    <td width="102"><strong><span class="style1">&nbsp;N&Uacute;MERO</span></strong><br>
    <span class="style1">&nbsp;<?php echo $pc["pco_num"]?></span></td>
    <td width="67"><strong><span class="style1">&nbsp;EMISS&Atilde;O</span></strong><br>
    <span class="style1">&nbsp;<?php echo $pc["pco_dat_emi"]?></span></td>
  </tr>
  <tr>
    <td colspan="5"><strong><span class="style1">&nbsp;ENDERE&Ccedil;O</span></strong><br>
      <span class="style1">&nbsp;<?php echo $pc["pco_des_end"]?></span></td>
  </tr>
  <tr>
    <td><strong><span class="style1">&nbsp;BAIRRO</span></strong><br>
    <span class="style1">&nbsp;<?php echo $pc["pco_des_bai"]?></span></td>
    <td colspan="1"><strong><span class="style1">&nbsp;CEP</span></strong><br>
    <span class="style1">&nbsp;<?php echo $pc["pco_des_cep"]?></span></td>
    <td colspan="3"><strong><span class="style1">&nbsp;CIDADE/UF</span></strong><br>
    <span class="style1">&nbsp;<?php echo $pc["pco_des_cid"]?></span></td>
  </tr>
  <tr>
    <td height="26" colspan="3"><strong><span class="style1">&nbsp;FAVORECIDO</span></strong><br>
    <span class="style1">&nbsp;<?php echo $pc["pco_nom_fav"]?></span></td>
    <td colspan="2"><strong><span class="style1">&nbsp;CPF/CNPJ</span></strong><br>
    <span class="style1">&nbsp;</span></td>
  </tr>
</table>
<br>
<table width="100%"  border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="5"><div align="center"><strong><span class="style1">RELA&Ccedil;&Atilde;O DE IM&Oacute;VEIS </span></strong></div></td>
  </tr>
</table>
<br>
<table width="100%"  border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="7%"><div align="center"><span class="style2">C&Oacute;DIGO</span></div></td>
    <td width="31%"><div align="center"><span class="style2">ENDERE&Ccedil;O</span></div></td>
    <td width="11%"><div align="center"><span class="style2">ENTRADA</span></div></td>
    <td width="10%"><div align="center"><span class="style2">REAJUSTE</span></div></td>
    <td width="10%"><div align="center"><span class="style2">T&Eacute;RMINO</span></div></td>
    <td width="10%"><div align="center"><span class="style2">ALUGUEL</span></div></td>
    <td width="9%"><div align="center"><span class="style2">ULT. PAG </span></div></td>
    <td width="6%"><div align="center"><span class="style2">TIPO</span></div></td>
    <td width="6%"><div align="center"><span class="style2">SIT.</span></div></td>
  </tr>

<?php
  $valtot = 0;
  $qtdimo = count($ipc);
  for($i=0;$i<$qtdimo;$i++) {
?>
  <tr>
    <td width="7%"><div align="center"><span class="style1"><?php echo $ipc[$i]["ipc_cod_imo"]?></span></div></td>
    <td width="31%"><div align="left"><span class="style1">&nbsp;<?php echo $ipc[$i]["ipc_des_end"]?> </span></div></td>
    <td width="11%"><div align="center"><span class="style1"><?php echo $ipc[$i]["ipc_dat_ent"]?></span></div></td>
    <td width="10%"><div align="center"><span class="style1"><?php echo $ipc[$i]["ipc_dat_reaj"]?></span></div></td>
    <td width="10%"><div align="center"><span class="style1"><?php echo $ipc[$i]["ipc_dat_fim"]?></span></div></td>
    <td width="10%"><div align="right"><span class="style1"><?php echo number_format($ipc[$i]["ipc_val_alu"],2,",",".")?></span></div></td>
    <td width="9%"><div align="center"><span class="style1"><?php echo $ipc[$i]["ipc_des_ult_pgto"]?></span></div></td>
    <td width="6%"><div align="center"><span class="style1"><?php echo $ipc[$i]["ipc_des_tip"]?></span></div></td>
    <td width="6%"><div align="center"><span class="style1"><?php echo $ipc[$i]["ipc_des_sit"]?></span></div></td>
  </tr>
<?php
 $valtot = $valtot + $ipc[$i]["ipc_val_alu"];
 }
?>  
  <tr>
    <td colspan="7"><div align="right"><span class="style1">TOTAL ..............:</span></div></td>
    <td colspan="2"><div align="right"><span class="style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo number_format($valtot,2,",",".")?>
    </span></div></td>
  </tr>
  <tr>
    <td colspan="9"><div align="center"><strong><span class="style1">NCTR - N&atilde;o contratado / AVIS - &Agrave; vistoriar / DEV - Devolvido / VIS - Vistoriado / ALUG - Alugado / QUIT - Quitado </span></strong></div></td>
  </tr>
</table>
<br>
<table width="100%"  border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="9"><div align="center"><strong><span class="style1">DEMONSTRA&Ccedil;&Atilde;O DOS LAN&Ccedil;AMENTOS EFETUADOS </span></strong></div></td>
  </tr>
  <tr>
    <td width="6%"><div align="center"><span class="style2">C&Oacute;DIGO</span></div></td>
    <td width="11%"><div align="center"><span class="style1"><nobr><strong>CTR. LOCA&Ccedil;&Atilde;O</strong></nobr></span></div></td>
    <td width="32%"><div align="center"><span class="style2">INQUILINOS / OUTROS </span></div></td>
    <td width="5%"><div align="center"><span class="style2">M&Ecirc;S/ANO</span></div></td>
    <td width="7%"><div align="center"><span class="style2">D&Eacute;BITO</span></div></td>
    <td width="7%"><div align="center"><span class="style2">CR&Eacute;DITO</span></div></td>
    <td width="5%"><div align="center"><span class="style2">COMIS</span></div></td>
    <td width="7%"><div align="center"><span class="style2">SALDO</span></div></td>
    <td width="2%"><div align="center"><span class="style2">D/C</span></div></td>
  </tr>
<?php
$valorsal = 0;
$valordeb = 0;
$valorcred = 0;
$qtdlan   = count($lpc);
for($i=0;$i < $qtdlan;$i++) {
	if(!$lpc[$i]["lpc_des_tip_com"]) {
	?>
	  <tr>
		 <td width="6%"><div align="center"><span class="style1"><?php echo $lpc[$i]["lpc_cod_imo"]?></span></div></td>
		 <td width="32%" colspan="8"><span class="style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $lpc[$i]["lpc_des_inq_out"]?> </span></td>
	  </tr>
	<?php
	}
	else {
	?>
	  <tr>
		 <td width="6%"><div align="center"><span class="style1"><?php echo $lpc[$i]["lpc_cod_imo"]?></span></div></td>
		 <td width="11%"><span class="style1"><nobr><?php echo $lpc[$i]["lpc_des_ctr_loc"]?></nobr></strong></span></td>
		 <td width="32%"><span class="style1">&nbsp;&nbsp;<?php echo $lpc[$i]["lpc_des_inq_out"]?> </span></td>
		 <td width="5%"><div align="center"><span class="style1"><?php echo $lpc[$i]["lpc_des_mes_ano"]?></span></div></td>
		 <?php
		 if(number_format($lpc[$i]["lpc_val_deb"],2,",",".") != "0,00") {
		 ?>
		 <td width="7%"><div align="right"><span class="style1"><?php echo number_format($lpc[$i]["lpc_val_deb"],2,",",".")?></span></div></td>
		 <?php
		 }
		 else {
		 ?>
		 <td width="7%"><div align="right"><span class="style1">&nbsp;</span></div></td>
		 <?php
		 }
		 if(number_format($lpc[$i]["lpc_val_cre"],2,",",".") != "0,00") {
		 ?>
		 <td width="7%"><div align="right"><span class="style1"><?php echo number_format($lpc[$i]["lpc_val_cre"],2,",",".")?></span></div></td>
		 <?php
		 }
		 else {
		 ?>
		 <td width="7%"><div align="right"><span class="style1">&nbsp;</span></div></td>
		 <?php
		 }
		 ?>
		 <td width="5%"><div align="center"><span class="style1"><?php echo $lpc[$i]["lpc_des_tip_com"]?></span></div></td>
		 <?php
		 if(number_format($lpc[$i]["lpc_val_sal"],2,",",".") != "0,00") {
		 ?>
		 <td width="7%"><div align="right"><span class="style1"><?php echo number_format($lpc[$i]["lpc_val_sal"],2,",",".")?></span></div></td>
		 <?php
		 }
		 else {
		 ?>
		 <td width="7%"><div align="right"><span class="style1">&nbsp;</span></div></td>
		 <?php
		 }
		 ?>
		 <td width="2%"><div align="center"><span class="style1"><?php echo $lpc[$i]["lpc_des_tip_deb_cre"]?></span></div></td>
	  </tr>
	<?php
	$valordeb  = $valordeb + $lpc[$i]["lpc_val_deb"];
	$valorcred = $valorcred + $lpc[$i]["lpc_val_cre"];
	}
}
$valorsal  = $valorcred - $valordeb;
if($valorsal > 0) {
   $valdc = "C";
}
if($valorsal < 0) {
  $valdc = "D";
}
if($valorsal == 0) {
  $valdc = "-";
}
?>
  <tr>
    <td colspan="4"><div align="right"><span class="style1">TOTAL ............: </span></div></td>
    <td width="10%"><div align="right"><span class="style1"><?php echo number_format($valordeb,2,",",".")?></span></div></td>
    <td width="9%"><div align="right"><span class="style1"><?php echo number_format($valorcred,2,",",".")?></span></div></td>
	 <td><div align="center">-</div></td>
    <td><div align="right"><span class="style1"><?php echo number_format($valorsal,2,",",".")?></span></div></td>
	 <td><div align="center"><span class="style1"><?php echo $valdc?></div></td>
  </tr>
</table>
<br>
<table width="100%" height="293"  border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="34%"><div align="center"><strong><span class="style1">RESUMO</span></strong></div></td>
    <td width="66%"><div align="center"><strong><span class="style1">RECIBO</span></strong></div></td>
  </tr>
  <tr>
    <td valign="top">
	   <table width="100%"  border="0" cellspacing="1">
		  <tr>
			<td width="60%"><span class="style1">BASE</span></td>
			<td width="40%"><div align="right"><span class="style1"><?php echo number_format($pc["pco_val_bas_com"],2,",",".")?></span></div></td>
		  </tr>
		  <tr>
			<td><span class="style1">ALUGU&Eacute;IS CREDITADOS </span></td>
			<td><div align="right"><span class="style1"><?php echo number_format($pc["pco_val_alu_cred"],2,",",".")?></span></div></td>
		  </tr>
		  <tr>
			<td><span class="style1">(-) IRRF </span></td>
			<td><div align="right"><span class="style1"><?php echo number_format($pc["pco_val_irrf"],2,",",".")?></span></div></td>
		  </tr>
		  <tr>
			<td><span class="style1">(-) COMISS&Otilde;ES </span></td>
			<td><div align="right"><span class="style1"><?php echo number_format($pc["pco_val_com"],2,",",".")?></span></div></td>
		  </tr>
		  <tr>
			<td><span class="style1">(-) OUTROS D&Eacute;BITOS </span></td>
			<td><div align="right"><span class="style1"><?php echo number_format($pc["pco_val_deb"],2,",",".")?></span></div></td>
		  </tr>
		  <tr>
			<td><span class="style1">(+) OUTROS CR&Eacute;DITOS </span></td>
			<td><div align="right"><span class="style1"><?php echo number_format($pc["pco_val_cred"],2,",",".")?></span></div></td>
	      </tr>
       </table>
	</td>
    <td>
	   <table width="100%" height="101"  border="0" cellspacing="1">
		  <tr>
			<td colspan="2"><span class="style1">RECEBI(EMOS) &Agrave; IMPORT&Acirc;NCIA DE R$ &nbsp;&nbsp;&nbsp;&nbsp;*********** <?php echo number_format($pc["pco_val_pc"],2,",",".")?> REFERENTE AO L&Iacute;QUIDO DOS ALUGU&Eacute;IS ACIMA. </span></td>
		  </tr>
		  <tr>
			<td width="33%" height="47"><div align="center">___/____/_____</div></td>
			<td width="66%"><div align="center">
			<br><br>
			 ______________________________<br>
			  <span class="style2">ASSINATURA</span>
			        
			    </p>
			</div></td>
		  </tr>
	  </table>
	</td>
  </tr>
  <tr>
    <td height="50" valign="top">
	   <table width="100%" height="100"  border="0" cellspacing="1">
		  <tr valign="top">
			 <td width="51%"><span class="style1">TOTAL</span></td>
			 <td width="49%"><div align="right"><span class="style1"><?php echo number_format($pc["pco_val_brt"],2,",",".")?></span></div></td>
		  </tr>
		  <tr>
			 <td><span class="style1">(-) CPMF </span></td>
			 <td><div align="right"><span class="style1"><?php echo number_format($pc["pco_val_cpmf"],2,",",".")?></span></div></td>
		  </tr>
		  <tr>
			 <td><span class="style1">(-) DOC </span></td>
			 <td><div align="right"><span class="style1"><?php echo number_format($pc["pco_val_doc"],2,",",".")?></span></div></td>
		  </tr>
		  <tr>
			 <td><span class="style1">(-) TX </span></td>
			 <td><div align="right"><span class="style1"><?php echo number_format($pc["pco_val_tax_bca"],2,",",".")?></span></div></td>
		  </tr>
		  <tr>
			 <td><span class="style1">(+) ENC. SOCIAIS </span></td>
			 <td><div align="right"><span class="style1"></span></div></td>
		  </tr>
		  <tr>
			 <td height="20"><span class="style1">L&Iacute;QUIDO</span></td>
			 <td><div align="right"><span class="style1"><?php echo number_format($pc["pco_val_pc"],2,",",".")?></span></div></td>
		  </tr>
     </table>
</td>
    <td>
	  <table width="100%" height="130"  border="0" cellspacing="1">
  <tr>
    <td colspan="2"><span class="style1">TIPO DE PAGAMENTO: &nbsp;&nbsp;&nbsp;<?php echo $pc["pco_des_tip_pag"]?></span></td>
  </tr>
  <tr>
    <td colspan="2"><span class="style1">BANCO ..........: &nbsp;&nbsp;&nbsp;<?php echo $pc["pco_des_bco"]?> </span></td>
  </tr>
  <tr>
    <td colspan="2"><span class="style1">AG&Ecirc;NCIA ......:&nbsp;&nbsp;&nbsp; <?php echo $pc["pco_des_age"]?> </span></td>
    <td width="1%">&nbsp;</td>
  </tr>
  <tr>
    <td width="60%"><span class="style1">CONTA ..........:&nbsp;&nbsp;&nbsp; <?php echo $pc["pco_des_cta"]?> </span></td>
    <td width="39%"><span class="style1">COMPENSA&Ccedil;&Atilde;O ..................... </span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="21">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</td>
</tr>
</table>
<br>
<table border="0" cellspacing="1" width="100%">
<tr id="imprimir">
<td height="36" colspan="9">
  <div align="right"><input type="button" value="Imprimir" onClick="javascript: window.print()"></div>
</td>
<td width="52%">
  <div align="leftr"><input type="submit" value="Fechar" onClick="javascript: window.close()"></div>
</td>
</tr>
</table>
</div>
<script language="javascript">
<!--
  alert("Para imprimir esse documento clique no botï¿½o imprimir no final da pï¿½gina");
-->
</script>