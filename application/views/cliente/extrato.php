<style type="text/css">
<!--
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
}
.style2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
}
.style3 {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 9px;
}
.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
<table width="101%"  border="1"  cellpadding="0" cellspacing="0" bordercolorlight="#999999"align="center">
  <tr>
    <td width="12%">
	    <div align="center"><img src="<?php echo $logomarca?>" width="99" height="62">	       </div></td>
    <td width="88%">
	   <table width="102%"  border="0">
		  <tr>
			<td colspan="2"><div align="center"><span class="style1">EXTRATO DE IMPOSTO DE RENDA</span></div></td>
		  </tr>
		  <tr>
			<td width="72%"><div align="center"><span class="style2">&nbsp;
              <?php echo $dadosPes["pes_nom"]?>
		    </span></div></td>
		  </tr>
		  <tr>
			<td><div align="center">&nbsp;<?php echo $dadosPes["pes_des_end"]?>
		      </div></td>
		  </tr>
		  <tr>
			<td><div align="center" class="style2">&nbsp;<?php echo $dadosIrf["irf_per"]?>
		      </div></td>
		  </tr>
	   </table>
	</td>
  </tr>
</table>
<br>
<table width="101%"  border="0" align="center">
<?php $qtd = count($extratoImo);
 for($i=0;$i<$qtd;$i++) {
?>
  <tr>
    <td>
	   <table width="100%"  border="0">
		  <tr>
			<td width="31%"><span class="style2">IM&Oacute;VEL:</span></td>
			<td width="69%"><span class="style2">&nbsp;
		    <?php echo $extratoImo[$i]["ime_des_end"]?>
			</span></td>
		  </tr>
		  <tr>
			<td><span class="style2">CONTRATO DE LOCA&Ccedil;&Atilde;O: </span></td>
			<td><span class="style2">&nbsp;
		    <?php echo $extratoImo[$i]["ime_des_con"]?>
			</span></td>
		  </tr>
	   </table>
	</td>
  </tr>
  <tr>
    <td>
	   <table width="100%"  border="1" cellpadding="0" cellspacing="0" bordercolorlight="#999999">
		  <tr>
			<td width="8%"><div align="center"><span class="style2">M&Ecirc;S</span></div></td>
			<td width="8%"><div align="center"><span class="style2">M&Ecirc;S PG </span></div></td>
			<td width="9%"><div align="center"><span class="style2">ALUGUEL</span></div></td>
			<td width="7%"><div align="center"><span class="style3">TX ADM </span></div></td>
			<td width="6%"><div align="center"><span class="style2">%</span></div></td>
			<td width="5%"><div align="center"><span class="style2">IRRF</span></div></td>
			<td width="39%"><div align="center"><span class="style2">INQUILINO</span></div></td>
			<td width="18%"><span class="style2">CPF_CNPJ</span></td>
		  </tr>
		  <?php $quant = count($lancamentos[$i]);
		   for($cont=0;$cont<$quant;$cont++) {
		  ?>
		  <tr>
			<td><div align="center"><span class="style2">&nbsp;
              <?php echo $lancamentos[$i][$cont]["lim_mes"]?>
		    </span></div></td>
			<td><div align="center"><span class="style2">&nbsp;
              <?php echo $lancamentos[$i][$cont]["lim_mes_pgto"]?>
		    </span></div></td>
			<td><div align="right"><span class="style2">&nbsp;
              <?php echo number_format($lancamentos[$i][$cont]["lim_val_alu"],2,",",".")?>
		    </span></div></td>
			<td><div align="right"><span class="style2">&nbsp;
              <?php echo number_format($lancamentos[$i][$cont]["lim_val_tx_adm"],2,",",".")?>
		    </span></div></td>
			<td><div align="right"><span class="style2">&nbsp;
              <?php echo number_format($lancamentos[$i][$cont]["lim_tx_adm"],2,",",".")?>
		    </span></div></td>
			<td><div align="right"><span class="style2">&nbsp;
              <?php echo number_format($lancamentos[$i][$cont]["lim_val_irrf"],2,",",".")?>
		    </span></div></td>
			<td><span class="style2">&nbsp;
		    <?php echo $lancamentos[$i][$cont]["lim_nom_inq"]?>
			</span></td>
			<td><div align="center"><span class="style2">&nbsp;
              <?php echo $lancamentos[$i][$cont]["lim_cpf_cnpj_inq"]?>
		    </span></div></td>
		  </tr>
		  <?php }
		  ?>
	   </table>
	</td>
  </tr>
  <tr>
    <td>
	   <table width="100%"  border="0">
	   <?php $parc = count($resumoImo[$i]);
		for($id=0;$id<$parc;$id++) {
	   ?>
		  <tr>
			<td width="39%"><span class="style2">TOTAL DE ALUGU&Eacute;IS: </span></td>
			<td width="61%"><span class="style2">R$&nbsp;
		    <?php echo number_format($resumoImo[$i][$id]["rim_tot_alu"],2,",",".")?>
			</span></td>
		  </tr>
		  <tr>
			<td><span class="style2">TOTAL ( ALUGUEL + ENCARGOS + IPTU ) : </span></td>
			<td><span class="style2">R$&nbsp;
		    <?php echo number_format($resumoImo[$i][$id]["rim_tot_alu_enc_iptu"],2,",",".")?>
			</span></td>
		  </tr>
		  <tr>
			<td><span class="style2">TAXA DE ADM: </span></td>
			<td><span class="style2">R$&nbsp;
		    <?php echo number_format($resumoImo[$i][$id]["rim_tot_tx_adm"],2,",",".")?>
			</span></td>
		  </tr>
		  <tr>
			<td><span class="style2">IMPOSTO DE RENDA RETIDO NA FONTE: </span></td>
			<td><span class="style2">R$&nbsp;
		    <?php echo number_format($resumoImo[$i][$id]["rim_tot_irrf"],2,",",".")?>
			</span></td>
		  </tr>
		  <?php if($i < ($qtd-1)) {
		  ?>
		  <tr>
		    <td colspan="2">----------------------------------------------------------------------------------------------------------------------------</td>
		  </tr>
		  <?php }
		  ?>
		<?php }
		?>  
	   </table>
	</td>
  </tr>
<?php }
?>  
</table>
<br>
<table width="101%"  border="0" align="center">
  <tr>
    <td colspan="2"><div align="left"><strong><span class="style4">----------------------------------------------------------- RESUMO -------------------------------------------------------</span></strong><br>
    </div></td>
  </tr>
  <tr>
    <td width="35%"><br><span class="style2">&nbsp;TOTAL DE ALUGU&Eacute;IS: </span></td>
    <td width="65%"><br><span class="style2">R$&nbsp;
      <?php echo number_format($resumoGeral[0]["rge_tot_alu"],2,",",".")?>
    </span></td>
  </tr>
  <tr>
    <td><span class="style2">&nbsp;TOTAL ( ALUGUEL + ENCARGOS + IPTU ) : </span></td>
    <td><span class="style2">R$&nbsp;
      <?php echo number_format($resumoGeral[0]["rge_tot_alu_enc_iptu"],2,",",".")?>
    </span></td>
  </tr>
  <tr>
    <td><span class="style2">&nbsp;TOTAL DE IRRF:</span></td>
    <td><span class="style2">R$&nbsp;
      <?php echo number_format($resumoGeral[0]["rge_tot_irrf"],2,",",".")?>
    </span></td>
  </tr>
  <tr>
    <td><span class="style2">&nbsp;TAXA DE ADM (-) : </span></td>
    <td><span class="style2">R$&nbsp;
      <?php echo number_format($resumoGeral[0]["rge_tot_tx_adm"],2,",",".")?>
    </span></td>
  </tr>
  <tr>
    <td><span class="style2">&nbsp;L&Iacute;QUIDO:</span></td>
    <td><span class="style2">R$&nbsp;
      <?php echo number_format($resumoGeral[0]["rge_tot_liq"],2,",",".")?>
    </span></td>
  </tr>
</table>
<br>
<table width="101%"  border="0" align="center">
  <tr>
    <td><div align="right">
      <input type="button" value="Imprimir" onClick="javascript: window.print()">
    </div></td>
    <td><input type="button" value="Fechar" onClick="javascript: window.close()"></td>
  </tr>
</table>