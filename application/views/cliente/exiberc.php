<style type="text/css">
<!--
.style1 {font-family: Verdana, Arial, Helvetica, sans-serif}
.style2 {
	font-size: 14px;
	font-weight: bold;
}
.style3 {font-size: 9px}
.style4 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; }
-->
</style>
<table width="100%"  border="0" cellspacing="1">
<tr>
  <td colspan="9" align="center" class="style4 style1 style2">RESUMO DE CONTRATOS </td>
</tr>
  <tr>
    <td width="12%"><div align="center"><img src="<?php echo $logomarca?>"></div></td>
    <td width="50%" align="center"><span class="style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="style3"><strong>PROPRIET&Aacute;RIO(A):</strong>&nbsp;
      <?php echo $usuario["nome"]?>
    </span><BR>
    &nbsp;&nbsp;&nbsp;&nbsp;</span></td>
    <td width="38%"><div align="right">
      <p class="style4">Impresso em <?php echo $data?></p>
      <p class="style4">&nbsp;</p>
    </div></td>
  </tr>
  <tr>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="1">
<?php $valtot = 0;
  $qtdimo = count($rc);
  for($i=0;$i<$qtdimo;$i++) {
?>

<tr>
<td><br>
<table width="100%"  border="0" cellspacing="1">
  <tr>
    <td colspan="2"><div align="hight"><span class="style6"><span class="style4"><span class="style9"><strong><u>IM&Oacute;VEL:</u>&nbsp;
            <?php echo  $rc[$i]["imo_des"]?>
    </strong></span></div></td>
  </tr>
  <tr>
  <td width="97%">
  <table width="100%"  border="1" cellspacing="0" cellpadding="0" style="border-bottom:1 " bordercolor="#000000">
  <tr>
    <td width="26%"><div align="center"><span class="style6 style1 style3">INQUILINO</span></div></td>
    <td width="19%"><div align="center"><span class="style6 style1 style3">CONTRATO</span></div></td>
    <td width="8%"><div align="center"><span class="style6 style1 style3">IN&Iacute;CIO</span></div></td>
    <td width="8%"><div align="center"><span class="style6 style1 style3">T&Eacute;RMINO</span></div></td>
    <td width="9%"><div align="center"><span class="style4">PR&Oacute;X. REAJ</span></div></td>
    <td width="12%"><div align="center"><span class="style6 style1 style3">VAL ALUGUEL.</span></div></td>
    <td width="6%"><div align="center"><span class="style6 style1 style3">DESC</span></div></td>
    <td width="6%"><div align="center"><span class="style6 style1 style3">GARANT</span></div></td>
	<td width="6%"><div align="right"><span class="style6 style1 style3">IND&Iacute;CE</span></div></td>
  </tr>
</table>
</td>
</tr>
<tr>
<td>
<table width="101%"  border="0" cellspacing="1">
  <tr>
    <td width="26%"><div align="left" class="style7 style3"><span class="style1">&nbsp;<?php echo  $rc[$i]["pes_nom"]?> </span></div></td>
    <td width="19%"><div align="center" class="style6 style1 style3"><?php echo  $rc[$i]["inf_des_con"]?></div></td>
    <td width="8%"><div align="center"><span class="style4"><?php echo  $rc[$i]["inf_dat_ini"]?></span></div></td>
    <td width="8%"><div align="center"><span class="style4"><?php echo  $rc[$i]["inf_dat_fim"]?></span></div></td>
    <td width="9%"><div align="center"><span class="style4"><?php echo  $rc[$i]["inf_dat_reaj"]?></span></div></td>
    <td width="12%"><div align="right"><span class="style4"><?php echo $rc[$i]["inf_val_alu"]?></span></div></td>	
    <td width="6%"><div align="right"><span class="style4"><?php echo  $rc[$i]["inf_vald_desc"]?></span></div></td>
    <td width="6%"><div align="center"><span class="style4"><?php echo  $rc[$i]["inf_des_gar"]?></span></div></td>
	<td width="6%"><div align="center"><span class="style4"><?php echo  $rc[$i]["inf_des_ind"]?></span></div></td>
  
</table>
</td>
</tr>
</table>
</td>
</tr>
<?php $valp = $rc[$i]["inf_val_alu"];
 $valParc = number_format($valp,2,",",".");
 $valtot = $valtot + $valParc;
 }
?>  

<br><br>
<table width="100%"  border="0" cellspacing="1">
  <tr>
    <td width="89%"><div align="right" class="style3"><span class="style1"><br><br>TOTAL DE ALUGU&Eacute;IS............: </span></div></td>
    <td width="11%"><div align="right" class="style4"><span class="style1"><br><br><?php echo  number_format($valtot,2,",",".")?></span></div></td>
  </tr>
</table>
<br>
<br>
<table border="0" cellspacing="1" width="96%">
<tr>
<td height="36" colspan="9">
  <div align="right"><input type="button" value="Imprimir" onClick="javascript: window.print()"></div>
</td>
<td width="52%">
  <div align="leftr"><input type="submit" value="Fechar" onClick="javascript: window.close()"></div>
</td>
</tr>
</table>
