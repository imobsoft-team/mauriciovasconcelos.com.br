<div class="modal fade area-cliente modal-custom">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Area cleinte</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <span class="icon"><i class="fa fa-user-circle-o"></i></span>
                <form name="formLogin" id="formLogin" method="post" action="<?php echo $local?>index.php/cliente/arearestrita">
                    <input type="hidden" name="botao" id="botao">
                    <input type="hidden" name="comando" value="validarUsuario">
                    <input type="hidden" name="valida" value="1">
                    <div class="form-group">
                        <label for="user">Usuário</label>
                        <input type="text" class="form-control input-custom" name="login" id="login" maxlength="18" onKeyUp="FormataCpfCgc('login',18,event,'formLogin')" placeholder="Digite seu CPF/CNPJ" required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="senha">Senha</label>
                        <input type="password" class="form-control input-custom" name="senha_usu"  id="senha_usu" placeholder="Digite sua senha" required>
                    </div>
                    <div class="form-group">
                        <button onClick="javascript: validaLogin()" class="btn btn-secondary "><i class="fa fa-unlock"></i>
                            Login
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- modal area cliente -->