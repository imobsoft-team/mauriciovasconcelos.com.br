<div class="modal fade contatos modal-custom">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nossos contatos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text"><i class="fa fa-phone"></i><?= $telefone ?></p>
                <p class="text"><i class="fa fa-whatsapp"></i><?= $telefone2 ?></p>
                <p class="text"><i class="fa fa-envelope-o"></i><?= $email ?></p>
            </div>
        </div>
    </div>
</div><!-- modal contatos -->