<div class="modal fade cadastro-imovel modal-custom">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cadastre seu imóvel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col form-group">
                            <label for="nome">Nome</label>
                            <input type="text" class="form-control input-custom" id="nome" autofocus placeholder="Digite seu nome">
                        </div>
                        <div class="col form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control input-custom" id="email" placeholder="Digite seu email">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col form-group">
                            <label for="fone">Telefone</label>
                            <input type="tel" class="form-control input-custom" id="fone" placeholder="Digite seu contato">
                        </div>
                        <div class="col form-group">
                            <label for="tipo">Tipo de imóvel</label>
                            <select class="form-control input-custom" id="tipo">
                                <option>Selecione o tipo tipo de imóvel</option>
                                <option>Apartamento</option>
                                <option>Casa</option>
                                <option>Flat</option>
                                <option>Galpão</option>
                                <option>Loja</option>
                                <option>Prédio comercial</option>
                                <option>Quitinete</option>
                                <option>Terreno</option>
                                <option>Chácara/Sítio</option>
                                <option>Lote</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col form-group">
                            <label for="endereco">Endereço</label>
                            <input type="text" class="form-control input-custom" id="endereco" placeholder="Digite o endereço do imóvel">
                        </div>
                        <div class="col form-group">
                            <label for="cidade">Cidade/UF</label>
                            <input type="text" class="form-control input-custom" id="cidade" placeholder="Digite a cidade e estado do imóvel">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col form-group">
                            <label for="modalidade">Modalidade</label>
                            <select class="form-control input-custom" id="modalidade">
                                <option>Modalidade do imóvel</option>
                                <option>Aluguel</option>
                                <option>Venda</option>
                                <option>aluguel/Venda</option>
                            </select>
                        </div>
                        <div class="col form-group">
                            <label for="finalidade">Finalidade</label>
                            <select class="form-control input-custom" id="finalidade">
                                <option>Finalidade do imóvel</option>
                                <option>Comercial</option>
                                <option>Residencial</option>
                                <option>Comercial/Residencial</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col form-group text">
                            <label for="descricao">Descrição do imóvel</label>
                            <textarea class="form-control input-custom textarea-custom" id="descricao" placeholder="Descrição do imóvel"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col form-group text">
                            <label for="obs">Obervações</label>
                            <textarea class="form-control input-custom textarea-custom" id="obs" placeholder="Observações"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-secondary "><i class="fa fa-check"></i>
                            Cadastrar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- modal cadastre seu imovel -->