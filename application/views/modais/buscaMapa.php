<div class="modal fade busca-pelo-mapa modal-custom">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Busca pelo mapa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-1">
                        <form name="frmBuscaMap" id="frmBuscaMap" method="post" action="" onSubmit="return false;">
                            <input type="hidden" name="numresult" id="numresultMap" value="5" />
                            <input type="hidden" name="valorini" id="valoriniMap" value="" />
                            <input type="hidden" name="valorfim" id="valorfimMap" value="" />
                            <input type="hidden" name="valorBus" id="valorBusMap" value="0"/>
                            <input type="hidden" name="codigo" id="codigoMap" value=""/>
                            <input type="hidden" name="finalidade" id="finalidade" value="1"/>
                            <div class="form-group">
                                <select class="form-control input-custom" name="opcoes" id="opcoesMap" onChange="listarCidadesMap(this.value,'<?php echo $local?>')">
                                    <option>Finalidade</option>
                                    <option value="1">Alugar</option>
                                    <!--option value="2">Comprar</option-->
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="form-control input-custom" name="tipo" id="tipoMap">
                                    <option>Tipo de imóvel</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="form-control input-custom" name="cidade" id="cidadeMap" onchange="javascript: carregarBairrosMap('<?php echo $local?>')">
                                    <option>Cidade</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="form-control input-custom" name="bairro[]" id="bairroMap">
                                    <option>Bairro</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button onclick="buscaRapida2Map();" class="btn btn-secondary input-custom"><i class="fa fa-search"></i>
                                    Buscar
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="col-2">
                        <div id="cmdFormGMaps">

                            <div id="ProgressBar">

                                <div id="loaderData" ><img src="<?php echo $local; ?>application/images/maps/loading.gif" tppabs="<?php echo $local; ?>application/images/loading.gif" width="24" style="vertical-align:middle;" /> carregando dados...</div>
                                <div id="txtInfo" ></div>
                                <div id="barinfo" ><div id="txtBarInfo" ></div></div>
                            </div>
                            <div id="divMap" style="width:650px;height:330px;" ></div>
                        </div>

                        <div style=" height:20px; width:100%; color:#F00; margin:auto;" id="msgCli"></div>


                        <script type="text/javascript">
                            initializeMap();
                            //listarCidadesMap(1,'<?php echo $local?>');
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- modal busca pelo mapa -->

