<div class="modal fade documentos modal-custom">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Documentos disponíveis para download</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul>
                    <li><a href="<?= "http://". $_SERVER['SERVER_NAME'] ."/docs/DocsFicha.pdf"?>">Documentos para locação</a></li>
                    <li><a href="<?= "http://". $_SERVER['SERVER_NAME'] ."/docs/ComercialFiador.pdf"?>">Ficha cadastral comercial fiador</a></li>
                    <li><a href="<?= "http://". $_SERVER['SERVER_NAME'] ."/docs/ComercialTituloCapitalizacao.pdf"?>">Ficha cadastral comercial titulo de capitalização</a></li>
                    <li><a href="<?= "http://". $_SERVER['SERVER_NAME'] ."/docs/ResidencialFiador.pdf"?>">Ficha cadastral residencial fiador</a></li>
                    <li><a href="<?= "http://". $_SERVER['SERVER_NAME'] ."/docs/ResidencialTituloCapitalizacao.pdf"?>">Ficha cadastral residencial titulo de capitalização</a></li>
                </ul>
            </div>
        </div>
    </div>
</div><!-- modal area cliente -->