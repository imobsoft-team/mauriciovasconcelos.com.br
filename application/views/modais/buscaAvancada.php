<div class="modal fade busca-avancada modal-custom">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Busca avançada</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="frmBusca3" id="frmBusca3" method="post" action="<?php echo $local?>index.php/imoveis/buscaimovel">
                    <input type="hidden" name="numresult3" id="numresult3" value="5" />
                    <input type="hidden" name="valorini3" value="" />
                    <input type="hidden" name="valorfim3"  value="" />
                    <div class="row">
                        <div class="col form-group">
                            <label for="modalidade">Modalidade</label>
                            <select class="form-control input-custom" name="opcoes3" id="opcoes3" onChange="listarCidades3('1','<?php echo $local?>')">
                                    <option value="0">SELECIONE</option>
                                    <option value="1">LOCAÇÃO</option>
                                    <!--<option value="2">VENDA</option>-->
                                </select>
                        </div>
                        <div class="col form-group">
                            <label for="finalidade">Finalidade</label>
                            <select class="form-control input-custom" id="finalidade" name="finalidade">
                                <option value="0">TODAS</option>
                                <option value="1">Residencial</option>
                                <option value="2">Comercial</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col form-group">
                            <label for="tipo">Tipo de imóvel</label>
                            <select class="form-control input-custom" id="tipo3" name="tipo3">
                                <option value="0" selected>Tipo de imóvel</option>
                            </select>
                        </div>
                        <div class="col form-group">
                            <label for="cidade">Cidade</label>
                            <select class="form-control input-custom" name="cidade3" id="cidade3" onChange="javascript: carregarBairros3('<?php echo $local?>')">
                                <option value="0" selected>CIDADE</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col form-group">
                            <label for="bairro">Bairro</label>
                            <select class="form-control input-custom" style="height:75px;" name="bairro3[]" id="bairro3"  title="Para selecionar mais de um bairro: pressione a tecla CTRL e selecione com o mouse" >
                                <option>Selecione</option>
                            </select>
                        </div>
                        <div class="col form-group">
                            <label for="area">Área</label>
                            <select class="form-control input-custom" id="area" name="area">
                                <option value="0">SELECIONE</option>            
                                <option value="50-70">50 a 70m2</option>            
                                <option value="70-100">70 a 100m2</option>            
                                <option value="100-120">100 a 120m2</option>            
                                <option value="120-150">120 a 150m2</option>            
                                <option value="150-200">150 a 200m2</option>            
                                <option value="200">acima de 200m2</option> 
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col form-group">
                            <label for="valor">Valor</label>
                            <select class="form-control input-custom" id="valor" name="valor">
                                <option value="0">SELECIONE</option>            
                                <option value="250-500">R$ 250,00 a  R$ 500,00</option>            
                                <option value="500-1000">R$ 500,00 a  R$ 1.000,00</option>            
                                <option value="1000-1500">R$ 1.000,00 a  R$ 1.500,00</option>            
                                <option value="1500-2000">R$ 1.500,00 a  R$ 2.000,00</option>            
                                <option value="2000-3000">R$ 2.000,00 a  R$ 3.000,00</option>            
                                <option value="3000">acima de R$ 3.000,00</option>
                            </select>
                        </div>
                        <div class="col col-3 form-group">
                            <label for="suites">Suítes</label>
                            <select class="form-control input-custom" id="suites" name="suites">
                                <option value="0">SELECIONE</option>
                                <option value="1">1</option>            
                                <option value="2">2</option>            
                                <option value="3">3</option>            
                                <option value="4">4</option>            
                                <option value="5">5</option>
                            </select>
                        </div>
                        <div class="col col-3 form-group">
                            <label for="vagas">Vagas</label>
                            <select class="form-control input-custom" id="vagas" name="vagas">
                                <option value="0">SELECIONE</option>
                                <option value="1">1</option>            
                                <option value="2">2</option>            
                                <option value="3">3</option>            
                                <option value="4">4</option>            
                                <option value="5">5</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <button onClick="buscaRapida4('<?php echo $local?>')" class="btn btn-secondary "><i class="fa fa-search-plus"></i>
                            Buscar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- busca avancada -->