<div class="row" style="text-align:center;">
	<div class="sixteen columns">
		<h3 class="titulo">Nossos Parceiros</h3>
	</div>
</div>
<div class="row">
	<div class="sixteen columns">
		<?php 
				if (!empty($parceiros)) {
					foreach ($parceiros as $parceiro) { ?>
		<div class="four columns" style="text-align:center;">
			<img src="<?php echo $parceiro['imagem'];?>" style="width:220px; height:auto;" alt="<?php echo $parceiro['nome'];?>" title="<?php echo $parceiro['nome'];?>" />
		</div>
				<?php }} ?>
	</div>
</div>