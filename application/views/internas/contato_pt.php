<main>

    <section>
        <div class="section-contato">
            <div class="nossos-contatos">
                <div class="container">
                    <div class="header-contato wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                        <h1 class="headline"><i class="fa fa-phone"></i>
                            Nossos contatos
                        </h1>
                        <p class="text">
                            Fique à vontade para tirar dúvidas, dar sugestões ou criticar. Como você já sabe, nosso objetivo maior é atendê-lo, sempre, da melhor maneira possível. Caso prefira, entre em contato com nossa Central de Atendimento:
                        </p>
                        <ul class="nav">
                            <li class="nav-item">
                                Central de Atendimento: <?= $telefone ?> - <?= $email ?>
                            </li>
                        </ul>
                    </div>

                    <div class="header-contato wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                        <h1 class="headline"><i class="fa fa-envelope-o"></i>
                            Fale conosco
                        </h1>
                    </div>
                    <form>
                        <div class="row wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                            <div class="coluna">
                                <div class="form-group">
                                    <input type="text" class="form-control input-custom" placeholder="Nome">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control input-custom" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control input-custom" placeholder="Contato">
                                </div>
                            </div><!-- coluna 1 -->

                            <div class="coluna">
                                <!--<div class="form-group">
                                    <select class="form-control input-custom">
                                        <option>Selecione um Departamento</option>
                                        <option>Atendimento</option>
                                        <option>Locação</option>
                                        <option>Vendas</option>
                                        <option>Jurídico</option>

                                    </select>
                                </div>-->
                                <div class="form-group">
                                    <textarea class="form-control input-custom textarea-custom" placeholder="Mensagem"></textarea>
                                </div>
                                <div class="row btn-custom">
                                    <div class="form-group">
                                        <button onclick="alert('Contato enviado.')" class="btn btn-secondary input-custom"><i class="fa fa-send"></i>
                                            Enviar
                                        </button>
                                    </div>
                                </div>
                            </div><!-- coluna 2 -->
                        </div><!-- row -->
                    </form>
                </div><!-- container -->
            </div><!-- nossos contatos -->

            <div class="localizacao">
                <div class="container wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h1 class="headline">
                        <i class="fa fa-map-marker"></i>
                        Localização
                    </h1>
                    <div class="mapa-localizacao">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3981.57545663717!2d-40.35199144923228!3d-3.6836208973066307!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7eac6df7f9106e9%3A0x1388fbdcc8781b8c!2sR.+Cel.+Diogo+Gomes%2C+1032+-+Alto+da+Bras%C3%ADlia%2C+Sobral+-+CE%2C+62010-150!5e0!3m2!1spt-BR!2sbr!4v1514398518594" width="100%" height="465" frameborder="0" style="border:0" allowfullscreen></iframe>                   
                    </div>
                </div><!-- container -->
            </div><!-- localizacao -->


        </div><!-- section contato -->
    </section>
</main>