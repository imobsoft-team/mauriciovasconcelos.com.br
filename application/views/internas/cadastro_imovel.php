<?php
/**
 * Created by PhpStorm.
 * User: Diego Pedrosa
 * Date: 09/09/2015
 * Time: 16:16
 */

?>

<style>
    #cadastro {
        margin: 0 auto;
        text-align: center;
    }

</style>

<div class="row">
    <div id="cadastro" class="sixteen columns">
        <h3>Cadastro de Imóvel</h3>
        <form id="frmCad" name="frmCad" method="post" action="<?php echo $local?>index.php/internas/cadastroImovel" onsubmit="return false">
            <input type="hidden" name="comando" value="cadastrarImoveis">
            <div class="eight columns">
                <input style="width: 100%;" type="text" name="nome" id="nomeimo" placeholder="Nome"><br/>
                <input style="width: 100%;" type="text" name="fone1" id="fone1" class="fone" placeholder="Telefone"><br/>
                <input style="width: 100%;" type="text" name="fone2" id="fone2" class="fone" placeholder="Telefone 2"><br/>
                <input style="width: 100%;" type="text" name="email" id="emailimo" placeholder="Email"><br/>
                <input style="width: 100%;" type="text" name="cel" id="cel" class="fone" placeholder="Celular"><br/>
                <input style="width: 100%;" type="text" name="endereco" id="endereco" placeholder="Logradouro"><br/>
                <input style="width: 100%;" type="text" name="numero" id="numero" placeholder="Número"><br/>
                <input style="width: 100%;" type="text" name="cep" id="cep" placeholder="CEP"><br/>
            </div>
            <div class="eight columns">
                <input style="width: 100%;" type="text" name="complementoEnd" id="complementoEnd" placeholder="Complemento"><br/>
                <input style="width: 100%;" type="text" name="bairro" id="bairroimo" placeholder="Bairro"><br/>
                <select style="width: 100%;" name="uf">
                    <option value="AC">AC</option>
                    <option value="AL">AL</option>
                    <option value="AM">AM</option>
                    <option value="JC">JC</option>
                    <option value="BA">BA</option>
                    <option value="CE" selected="selected">CE</option>
                    <option value="CO">CO</option>
                    <option value="DF">DF</option>
                    <option value="ES">ES</option>
                    <option value="GO">GO</option>
                    <option value="MA">MA</option>
                    <option value="MG">MG</option>
                    <option value="MS">MS</option>
                    <option value="MT">MT</option>
                    <option value="PA">PA</option>
                    <option value="PB">PB</option>
                    <option value="PE">PE</option>
                    <option value="PI">PI</option>
                    <option value="PR">PR</option>
                    <option value="RJ">RJ</option>
                    <option value="RN" >RN</option>
                    <option value="RO">RO</option>
                    <option value="RR">RR</option>
                    <option value="RS">RS</option>
                    <option value="SC">SC</option>
                    <option value="SE">SE</option>
                    <option value="SP">SP</option>
                    <option value="TO">TO</option>
                </select><br/><br/>
                <input style="width: 100%;" type="text" name="cidade" id="cidadeimo" placeholder="Cidade"><br/>
                <select style="width: 100%;" name="finalidade">
                    <option value="1">Locação</option>
                    <option value="2">Venda</option>
                    <option value="3">Locação e Venda</option>
                </select><br/><br/>
                <textarea style="width: 100%; height: 250px;" name="resumo" id="resumo" placeholder="Resumo"></textarea>
                <div style="width: 350px; margin:0 auto;">
                    <button style="float: left; margin-left:60px;" onClick="javascript: validaCad()">Enviar</button>
                    <button style="float: left; margin-left:10px;" onClick="javascript: limpaForm()">Limpar</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        var maskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            options = {onKeyPress: function(val, e, field, options) {
                field.mask(maskBehavior.apply({}, arguments), options);
            }
            };

        $('.fone').mask(maskBehavior, options);
    });

</script>