<main>

    <section>
        <div class="section-busca container wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#busca-rapida" role="tab" ><i class="fa fa-search"></i>
                    Busca rápida
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#codigo" role="tab"><i class="fa fa-qrcode"></i>
                    Código
                </a>
            </li>
            <li class="nav-item hidden-xs-down">
                <a class="nav-link" href="#" data-toggle="modal" data-target=".busca-pelo-mapa"><i class="fa fa-map-marker"></i>
                    Pelo mapa
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-toggle="modal" data-target=".busca-avancada"><i class="fa fa-search-plus"></i>
                    Avançada
                </a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="busca-rapida" role="tabpanel">
                <form name="frmBusca2" id="frmBusca2" method="post" action="<?php echo $local?>index.php/imoveis/buscaimovel">
                    <input type="hidden" name="numresult2" id="numresult2" value="5" />
                    <input type="hidden" name="valorini2" id="valorini2" value="" />
                    <input type="hidden" name="valorfim2" id="valorfim2" value="" />
                    <input type="hidden" name="codigo2"  id="codigo2" value="" />
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <select class="form-control input-custom" name="opcoes2" id="opcoes2">
                                    <option value="0">FINALIDADE</option>
                                    <option value="1" selected>LOCAÇÃO</option>
                                    <!--<option value="2">VENDA</option>-->
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <select class="form-control input-custom" id="tipo2" name="tipo2">
                                    <option value="0" selected>Tipo de imóvel</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <select class="form-control input-custom" name="cidade2" id="cidade2" onChange="javascript: carregarBairros2('<?php echo $local?>')">
                                    <option value="0" selected>CIDADE</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <select class="form-control input-custom" style="height:75px;" name="bairro2[]" id="bairro2"  title="Para selecionar mais de um bairro: pressione a tecla CTRL e selecione com o mouse" >
                                    <option>Selecione</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <button class="btn btn-secondary input-custom" onClick="buscaRapida3('<?php echo $local?>')"><i class="fa fa-search"></i>
                                    Buscar
                                </button>
                            </div>
                        </div>
                    </div><!-- row -->
                </form>
            </div><!-- busca rapida -->

            <div class="tab-pane" id="codigo" role="tabpanel">
                <form name="frmBuscaCod" id="frmBuscaCod" method="post" action="">
                    <div class="row">
                        <div class="col input-codigo">
                            <div class="form-group">
                                <input type="text" name="codigo3" id="codigo3" class="form-control input-custom" placeholder="Digite o código do imóvel">
                            </div>
                        </div>
                        <div class="col btn-codigo">
                            <div class="form-group">
                                <button class="btn btn-secondary input-custom" onClick="buscarPorCodigo('<?php echo $local?>')"><i class="fa fa-search"></i>
                                    Buscar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- tab contant -->
        </div><!-- section busca -->
    </section>


    <div class="border-top-primary">
        <div class="border-top-right"></div>
    </div>

    <!-- Banner de destaques especiais -->
    <section>
        <?php include_once "application/views/principal/destaqueEspecial.php" ?>
    </section>


    <div class="border-bottom-primary">
        <div class="border-bottom-right"></div>
    </div>


    <!-- <section>
        <?php //include_once "application/helpers/banner.php" ?>
    </section> -->


    <div class="border-top-secondary">
        <div class="border-top-right"></div>
    </div>


    <section>
        <div class="locacao">
            <div class="container wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <div class="header wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s">
                    <div class="busca">
                        <h1 class="headline">Destaques de <span class="mark">locação</span></h1>
                        <form class="form-inline">
                            <div class="form-group">
                                <select id="tipo_destaque_residencial" class="form-control input-custom" onChange="getDestaquesTipo('<?php echo $local?>',this.value,'residencial')">
                                    <option>: : SELECIONE : :</option>
                                </select>
                            </div>
                        </form>
                        <div class="form-group">
                            <a href="<?= $local ?>index.php/imoveis/buscaRapida/aluguel/0/0/0/0/0/imoveis_aluguel.html" class="btn btn-primary input-custom">
                                Ver todos
                            </a>
                        </div>
                    </div>

                    <div class="controls">
                        <a class="control-custom btn btn-primary input-custom" href="#carousel-locacao" role="button" data-slide="prev">
                            <i class="fa fa-chevron-left"></i>
                            <span class="sr-only">Prev</span>
                        </a>
                        <a class="control-custom btn btn-primary input-custom" href="#carousel-locacao" role="button" data-slide="next">
                            <i class="fa fa-chevron-right"></i>
                            <span class="sr-only">Next</span>
                        </a>
                    </div><!-- controls -->

                </div>

                <div id="carousel-residencial">
                    <?php include_once "application/helpers/carrossel_destaques_locacao.php" ?>
                </div>

            </div><!-- container -->
        </div><!-- locacao -->
    </section>


    <div class="border-bottom-secondary">
        <div class="border-bottom-right"></div>
    </div>


    <section>
        <div class="venda">
            <div class="container wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <div class="header wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s">
                    <div class="busca">
                        <h1 class="headline">Destaques <span class="mark">Comerciais</span></h1>
                        <form class="form-inline">
                            <div class="form-group">
                                <select id="tipo_destaque_comercial" class="form-control input-custom" onChange="getDestaquesTipo('<?php echo $local?>',this.value,'comercial')">
                                    <option>: : SELECIONE : :</option>
                                </select>
                            </div>
                        </form>
                        <div class="form-group">
                            <a href="<?= $local ?>index.php/imoveis/buscaRapida/aluguel/0/0/0/0/0/imoveis_aluguel.html" class="btn btn-secondary input-custom">
                                Ver todos
                            </a>
                        </div>
                    </div>

                    <div class="controls">
                        <a class="control-custom btn btn-secondary input-custom" href="#carousel-venda" role="button" data-slide="prev">
                            <i class="fa fa-chevron-left"></i>
                            <span class="sr-only">Prev</span>
                        </a>
                        <a class="control-custom btn btn-secondary input-custom" href="#carousel-venda" role="button" data-slide="next">
                            <i class="fa fa-chevron-right"></i>
                            <span class="sr-only">Next</span>
                        </a>
                    </div><!-- controls -->

                </div>

                <div id="carousel-comercial">
                    <?php include_once "application/helpers/carrossel_destaques_venda.php" ?>
                </div>

            </div><!-- container -->
        </div><!-- venda -->
    </section>

    <section>
        <?php include_once "application/views/principal/servicos.php" ?>
    </section>

    <section>
        <?php include_once "application/views/principal/newsletter.php" ?>
    </section>
</main>




