<div class="imovel-destaque">
    <div class="container wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">

        <div id="carousel-imovel-destaque" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <?php foreach ($destaquesEspeciais as $destaqueEspecial) { ?>
                <div class="carousel-item <?php echo ($i==0?'active':''); $i++;?>">
                    <a href="<?php echo $local.'index.php/imoveis/detalhes/aluguel/'.$destaqueEspecial['tim_des'].'/'.$destaqueEspecial['imo_isn']?>"><img class="d-block mx-auto img-fluid" src="<?= $destaqueEspecial['foto']; ?>" style="width: 1138px; height: 481px;" alt="Imóvel destaque"></a>
                    <div class="carousel-caption">
                        <h3 class="end-imovel"><?= utf8_encode($destaqueEspecial['imo_des_end']. ' - ' .$destaqueEspecial['imo_des_bai']); ?></h3>
                        <p class="finalidade"><?= utf8_encode($destaqueEspecial['tim_des']) ?></p>
                        <p class="value">R$ <?= number_format($destaqueEspecial['imo_val_alu'], 2, ',', '.'); ?>
                            <span class="service">
                                <!--<a href=""><i class="fa fa-facebook-official"></i>
                                    Compartilhar
                                </a>-->
                                <a href="<?php echo $local.'index.php/imoveis/detalhes/aluguel/'.$destaqueEspecial['tim_des'].'/'.$destaqueEspecial['imo_isn']?>"><i class="fa fa-folder-open"></i>
                                    Ficha Completa
                                </a>
                            </span>
                        </p>
                        <ul class="nav">
                            <li class="nav-item"><i class="fa fa-hotel"></i><?php echo $destaqueEspecial['caracs'][0]['qtd_quartos'];?> quartos</li>
                            <li class="nav-item"><i class="fa fa-bathtub"></i><?php echo $destaqueEspecial['caracs'][0]['qtd_suites'];?> suítes</li>
                            <li class="nav-item"><i class="fa fa-car"></i><?php echo $destaqueEspecial['caracs'][0]['qtd_vagas'];?> vagas</li>
                            <li class="nav-item"><i class="fa fa-crop"></i><?php echo $destaqueEspecial['imo_val_are'];?>m2</li>
                        </ul>
                    </div>
                </div>
                <?php } ?>
            </div><!-- carousel inner -->
        </div><!-- carousel imovel destaque -->

        <a class="carousel-control-prev" href="#carousel-imovel-destaque" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-imovel-destaque" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div><!-- container -->
</div><!-- imovel destaque -->
