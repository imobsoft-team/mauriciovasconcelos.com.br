<div class="topo wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.3s">
    <div class="container">
        <div class="topo-itens">
            <div class="contato">
                <p>
                    <span class="offcanvas hidden-lg-up">
                        <a href="#" class="js-open-menu">
                            <i class="fa fa-bars"></i>
                        </a>
                    </span><!-- off canvas -->
                    <i class="fa fa-phone" data-toggle="modal" data-target=".contatos"></i> <span class="hidden-md-down"><?= $telefone ?></span>
                    <i class="fa fa-whatsapp" data-toggle="modal" data-target=".contatos"></i> <span class="hidden-md-down"><?= $telefone2 ?></span>
                    <i class="fa fa-envelope-o" data-toggle="modal" data-target=".contatos"></i> <span class="hidden-md-down"><?= $email ?></span>
                </p>
            </div><!-- contato -->
            <div class="areacliente">
                <a data-toggle="modal" data-target=".area-cliente" ><i class="fa fa-user-circle-o"></i><span class="hidden-xs-down">Área cliente</span></a>
            </div><!-- area cliente -->
        </div><!-- topo itens -->
    </div><!-- container -->
</div><!-- topo -->

<div class="header">
    <div class="container">

        <nav>
            <div class="nav-content">
                <div class="header-logo wow slideInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <a href="<?= $local ?>">
                        <img class="img-fluid" src="<?= $imagePath.$logomarca ?>" alt="Logo da Imobiliária">
                    </a>
                </div><!-- logo -->
                <ul class="nav wow fadeIn" data-wow-duration="1s" data-wow-delay="2s">
                    <li class="nav-item">
                        <a href="<?= $local ?>" class="nav-link active">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= $local ?>index.php/internas/empresa" class="nav-link">Imobiliária</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= $local ?>index.php/imoveis/buscaRapida/aluguel/0/0/0/0/0/imoveis_aluguel.html" class="nav-link">Aluguel</a>
                    </li>
                    <!--<li class="nav-item">
                        <a href="<?/*= $local */?>index.php/imoveis/buscaRapida/venda/0/0/0/0/0/imoveis_venda.html" class="nav-link">Venda</a>
                    </li>-->
                    <li class="nav-item">
                        <a href="<?= $local ?>index.php/internas/contato" class="nav-link">Contato</a>
                    </li>
                </ul>
                <!--<div class="header-areacleinte hidden-xs-down wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                    <img class="img-fluid" src="<?/*= $imagePath */?>/img-atendimentoOnline.png" alt="">
                </div>-->
            </div><!-- nav content -->
        </nav>
    </div><!-- container -->
</div><!-- header -->