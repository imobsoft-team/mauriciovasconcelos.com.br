<div class="servicos" style="background-color: #F5F1E6;">
    <div class="container">
        <div class="row">
        
            <div class="col cad-imovel wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                <article>
                    <a class="nav-link" data-toggle="modal" data-target=".cadastro-imovel">
                        <div class="servicos-header">
                            <i class="fa fa-laptop"></i>
                            <header>
                                <h2 class="headline">Cadastre seu imóvel</h2>
                            </header>
                        </div>
                        <div class="cad-block">
                            <p class="text-servico">
                                acesse aqui o formulário de cadastro e agilize o processo de venda ou locação do seu imóvel
                            </p>
                        </div>
                    </a>
                </article>
            </div>

            <div class="col boleto active wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                <article>
                    <a class="nav-link" data-toggle="modal" data-target=".area-cliente">
                        <div class="servicos-header">
                            <i class="fa fa-barcode"></i>
                            <header>
                                <h2 class="headline">2º via de boleto</h2>
                            </header>
                        </div>
                        <div class="cad-block">
                            <p class="text-servico">
                                Inquilino acesse este link para imprimir sua segunda via de boleto
                            </p>
                        </div>
                    </a>
                </article>
            </div>

            <div class="col extrato wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                <article>
                    <a class="nav-link" data-toggle="modal" data-target=".area-cliente">
                        <div class="servicos-header">
                            <i class="fa fa-dollar"></i>
                            <header>
                                <h2 class="headline">Extratos</h2>
                            </header>
                        </div>
                        <div class="cad-block">
                            <p class="text-servico">
                                Proprietário retire aqui sua prestação de contas ou seu extraro de IRRF
                            </p>
                        </div>
                    </a>
                </article>
            </div>

        </div><!-- row -->
    </div><!-- container -->
</div><!-- serviços -->