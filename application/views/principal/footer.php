<div class="footer wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
    <div class="container">
        <div class="row">
            <div class="col contato">
                <img class="img-fluid" src="<?= $imagePath.$logoFooter ?>" alt="Imobiliária">
                <ul class="nav">
                    <li class="nav-item" style="color:#fff;"><i class="fa fa-map-marker"></i><span class="endereco"><?= $endereco ?></span></li>
                    <li class="nav-item" style="color:#fff;"><i class="fa fa-phone"></i><?= $telefone ?></li>
                    <li class="nav-item" style="color:#fff;"><i class="fa fa-whatsapp"></i><?= $telefone2 ?></li>
                    <li class="nav-item" style="color:#fff;"><i class="fa fa-envelope-o"></i><?= $email ?></li>
                </ul>
            </div><!-- col -->
            <div class="col localizacao">
                <ul class="nav">
                    <li class="nav-item">
                        <a href="<?= $local ?>" class="nav-link active" style="color:#fff;">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= $local ?>index.php/internas/empresa" class="nav-link" style="color:#fff;">Imobiliária</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= $local ?>index.php/imoveis/buscaRapida/aluguel/0/0/0/0/0/imoveis_aluguel.html" class="nav-link" style="color:#fff;">Aluguel</a>
                    </li>
                    <!--<li class="nav-item">
                        <a href="<?/*= $local */?>index.php/imoveis/buscaRapida/venda/0/0/0/0/0/imoveis_venda.html" class="nav-link">Venda</a>
                    </li>-->
                    <li class="nav-item">
                        <a href="<?= $local ?>index.php/internas/contato" class="nav-link" style="color:#fff;">Contato</a>
                    </li>
                </ul>
                <div class="mapa-localizacao">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3981.57545663717!2d-40.35199144923228!3d-3.6836208973066307!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7eac6df7f9106e9%3A0x1388fbdcc8781b8c!2sR.+Cel.+Diogo+Gomes%2C+1032+-+Alto+da+Bras%C3%ADlia%2C+Sobral+-+CE%2C+62010-150!5e0!3m2!1spt-BR!2sbr!4v1514398518594" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>                   
                </div>
            </div><!-- col 2 -->
        </div><!-- row -->

        <div class="redes-sociais">
            <h2 class="headline" style="color:#fff;">Acompanhe nossos imóveis nas redes sociais</h2>
            <div class="icones">
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link" href=""><i class="fa fa-facebook" style="color:#b18a14"></i></a></li>
                    <li class="nav-item"><a class="nav-link" href=""><i class="fa fa-instagram" style="color:#b18a14"></i></a></li>
                    <li class="nav-item"><a class="nav-link" href=""><i class="fa fa-youtube" style="color:#b18a14"></i></a></li>
                    <li class="nav-item"><a class="nav-link" href=""><i class="fa fa-twitter" style="color:#b18a14"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="bottom-footer">
            <p style="color:#fff;"><i class="fa fa-copyright"></i> 2017 - Todos os direitos reservados</p>
            <p style="color:#fff;">Imobsoft - Software Imobiliários</p>
        </div>

    </div><!-- container -->
</div><!-- footer -->

<!-- MODAIS -->

<?php include_once "application/views/modais/contatos.php" ?>

<?php include_once "application/views/modais/areaCliente.php" ?>

<?php include_once "application/views/modais/buscaAvancada.php" ?>

<?php include_once "application/views/modais/cadastroImovel.php" ?>

<?php include_once "application/views/modais/documentos.php" ?>

<?php include_once "application/views/modais/buscaMapa.php" ?>