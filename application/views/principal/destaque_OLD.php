<?php //echo "<pre>"; print_r($destaques1); die; ?>
	<div style="padding-top:51px;"></div>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#imob-navbar" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand hidden-md hidden-sm hidden-lg" href="#">
            <img src="<?php echo $imagePath?>logoNavbar.png" alt="lotipo DMV Imóveis" title="DMV Imóveis">
          </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="imob-navbar">
          <ul class="nav navbar-nav">
			  <li><a href="<?php echo $local;?>index.php/internas/empresa">EMPRESA</a></li>
			  <li><a href="<?php echo $local;?>index.php/imoveis/buscaRapida/aluguel/0/0/0/0/0/imoveis.html">ALUGAR</a></li> 
			  <li><a href="<?php echo $local;?>index.php/imoveis/buscaRapida/venda/0/0/0/0/0/imoveis.html">COMPRAR</a></li>  
			  <li><a href="<?php echo $local;?>index.php/internas/servicos">SERVIÇOS</a></li>  
			  <li><a href="<?php echo $local;?>index.php/internas/contato">FALE CONOSCO</a></li>               
          </ul>
          <div class="imob-idioma">
            <!--img src="<?php echo $imagePath?>bra.png" alt="bandeira brasil" title="Traduzir para português">&nbsp;&nbsp;
            <img src="<?php echo $imagePath?>us.png" alt="bandeira USA" title="Translate to English"-->
            <!--a href="/novo/chat/client.php?locale=pt-br&amp;style=silver" class="btn imob-btn-chat hidden-sm hidden-xs">
              <img src="<?php echo $imagePath?>icon-headset.png" alt="ícone headset" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &amp;&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('/chat/client.php?locale=pt-br&amp;style=silver&amp;url='+escape(document.location.href)+'&amp;referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=640,height=480,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;">INICIAR CHAT
            </a-->
          </div>          
        </div><!-- /.navbar-collapse -->
      </div>
    </nav><!-- /.navbar -->
    <section class="imob-section">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-4 col-xs-5">
            <a href="#">
              <img class="img-responsive hidden-xs" src="<?php echo $imagePath?>logo-dmv.png" alt="logotipo dmv" title="DMV Imóveis">
            </a>
            <img class="visible-xs img-responsive" src="<?php echo $imagePath?>atendente.png" alt="">
          </div>
          <div class="imob-contato-header col-md-5 col-md-offset-4 col-sm-5 col-sm-offset-3 col-xs-7 ">
            <p id="imob-contato-whatsapp">
              <i class="fa fa-whatsapp hidden-sm hidden-md hidden-lg" aria-hidden="true"></i>
              <img class="hidden-xs" src="<?php echo $imagePath?>icon-whatsapp.png" alt="ícone WhatsApp"> +55 85 98204-0055
            </p>
            <p id="imob-faleconosco">
              Fale Conosco
            </p>
            <p id="imob-fone">
              <sup style="font-size: 0.5em">(85)</sup>3205-6000
            </p>
          </div>
        </div>
      </div><!-- /.container contatos -->
		
		<!-- CARREGA O FORMULÁRIO DE BUSCA -->
      <?php include "application/helpers/form-busca-alugar.php"; ?>

	  <?php if ($banners) { ?>
      <div class="container hidden-xs">
        <div class="row imob-row">
          <div id="imob-carousel-banner" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators imob-carousel-indicators">
			<?php for ($i=0; $i<count($banners); $i++) { ?>
              <li data-target="#imob-carousel-banner" data-slide-to="<?php echo $i?>" <?php echo ($i==0?'class="active"':''); ?>></li>
			<?php } ?>
            </ol>

            <!-- Wrapper for slides -->           
            <div class="carousel-inner" role="listbox" style="border: 10px solid #fff; margin:10px 0;">
			<?php $x = 0; foreach ($banners as $banner) { ?>
              <div class="item <?php echo ($x == 0 ? 'active':'');?>">
                <a href="<?php echo ($banner['link']?$banner['link']:'javascript:void(0)');?>" <?php echo ($banner['link']?'target="_blank"':'');?>><img src="<?php echo $banner['img'];?>" style="width:1118px;" alt="<?php echo $banner['nome'];?>"></a>                
              </div>
			<?php $x++; } ?>  
            </div>         
          </div>
        </div>
      </div><!-- /.container carousel banner -->
	  <?php } ?>

      <div class="container" style="margin-top:20px;">
        <div class="row imob-row imob-redes-banner">
          <ul>
            <li><a href="https://www.facebook.com/dmvimobiliaria/"><i class="fa fa-2x fa-facebook-square" aria-hidden="true"></i></a></li>
            <li><a href="https://www.instagram.com/dmvimoveis"><i class="fa fa-2x fa-instagram" aria-hidden="true"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCN325Ur-zhZPxUPYs-U0Mpg"><i class="fa fa-2x fa-youtube" aria-hidden="true"></i></a></li>
          </ul>
        </div>        
      </div>
    </section><!-- /.section header-->    

    <section class="imob-section-carousel">
  <div class="container">
   <div class="row imob-row">
      <div class="col-md-4 col-sm-4 col-xs-7 text-center imob-destLocacao">
        <h4>Destaque de <b>Locação</b></h4>
      </div>          
      <div class="col-md-8 col-sm-8 col-xs-5 text-right imob-busca-todos">
        <a href="<?php echo $local;?>index.php/imoveis/buscaRapida/aluguel/0/0/0/0/0/imoveis.html"><h4 class="hidden-xs">Realize uma busca de todos os imóveis</h4></a>
        <a href="<?php echo $local;?>index.php/imoveis/buscaRapida/aluguel/0/0/0/0/0/imoveis.html"><h4 class="visible-xs">Ver todos</h4></a>
      </div>
    </div>       
  </div>
  <div class="container" style="margin-bottom:60px;">
    <div class="row imob-row">          
      <div class="col-md-1 visible-lg" style="width:45px; padding: 0;">
        <a class="imob-left carousel-control" href="#imob-imovel-destaque1" role="button" data-slide="prev">
          <img src="<?php echo $imagePath?>btn-left.png" alt="Botão anterior">              
        </a>
      </div>

      <div id="imob-imovel-destaque1" class="col-md-11 carousel slide carousel-imovel-destaque" data-ride="carousel" data-type="multi" data-interval="5000" >  <!-- Wrapper for slides -->
        <div class="carousel-inner">
		<?php $i=0; foreach ($destaques1 as $destaque1) { ?>
          <div class="item <?php echo ($i==0?'active':''); $i++;?>">              
              <div class="col-md-3 col-sm-4 col-xs-12 imob-thumb">
                <div class="thumbnail">                      
                  <div class="col-md-12 col-xs-12 imob-thumbnail">
                    <a href="<?php echo $local.'index.php/imoveis/detalhes/aluguel/'.$destaque1['tim_des'].'/'.$destaque1['imo_isn']?>" ><img src="<?php echo $destaque1['foto']?>" style="width:229px; height:241px;" class="img-responsive" alt="Imóvel destaque"></a>
                    <h3 class="imob-bairro"><?php echo $destaque1['imo_des_bai']?></h3>
                    <p class="text-center col-md-5 col-xs-5"><i class="fa fa-2x fa-bed" aria-hidden="true"></i><br>DORMITÓRIOS: <?php echo ($destaque1['caracs'][0]['qtd_suites'] + $destaque1['caracs'][0]['qtd_quartos']);?></p>
                    <p class="text-center col-md-3 col-xs-3"><i class="fa fa-2x fa-car" aria-hidden="true"></i><br>VAGAS: <?php echo $destaque1['caracs'][0]['qtd_vagas']?></p>
                    <p class="text-center col-md-4 col-xs-4">
                      <img src="<?php echo $imagePath?>icon-trena.png" class="img-responsive" alt="Imóvel destaque">ÁREA: <?php echo $destaque1['imo_val_are']?>m²
                    </p>
                    <h3 class="col-md-12 col-xs-12 text-center imob-tipo-imovel"><?php echo $destaque1['tim_des']?></h3>
                    <p class="col-md-8 col-xs-8" style="font-size: 1em;">&nbsp;CÓDIGO: <?php echo $destaque1['imo_isn']?></p>
                    <p class="text-center col-md-2 col-xs-2 imob-rs-destaques"><a id="fbshare<?php echo $destaque1['imo_isn']?>" href="javascript:void(0)" data-link="<?php echo $local?>index.php/imoveis/detalhes/aluguel/<?php echo $destaque1['tim_des']?>/<?php echo $destaque1['imo_isn']?>" data-image="<?php echo $local.$destaque1['foto']?>" data-title="<?php echo utf8_encode($destaque1['imo_des_est']).' - '.utf8_encode($destaque1['tim_des'])?>" data-desc="<?php echo utf8_encode($destaque1['imo_des_est'])?>" onClick="btnShare(<?php echo $destaque1['imo_isn']?>)"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></p>
                    <p class="text-center col-md-2 col-xs-2 imob-rs-destaques"><a href="<?php echo $local.'index.php/imoveis/detalhes/aluguel/'.$destaque1['tim_des'].'/'.$destaque1['imo_isn']?>"><i class="fa fa-plus" aria-hidden="true"></i></a></p>
                  </div>
                </div>
              </div>
          </div>
		<?php } ?>	
        </div>
      </div>       

      <div class="col-md-1 visible-lg" style="width:45px; padding: 0;">
        <a class="imob-right carousel-control" href="#imob-imovel-destaque1" role="button" data-slide="next">
          <img class="img-reponsive" src="<?php echo $imagePath?>btn-right.png" alt="Botão próximo">              
        </a>
      </div>

      <div class="col-md-6 col-sm-6 col-xs-6 hidden-lg">
        <a class="imob-left-mobile carousel-control" href="#imob-imovel-destaque1" role="button" data-slide="prev">
          <img src="<?php echo $imagePath?>btn-left-mobile.png" alt="Botão anterior">              
        </a>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-6 hidden-lg">
        <a class="imob-right-mobile carousel-control" href="#imob-imovel-destaque1" role="button" data-slide="next">
          <img class="img-reponsive" src="<?php echo $imagePath?>btn-right-mobile.png" alt="Botão próximo">              
        </a>
      </div>
      
    </div>        
  </div><!-- /.container carousel destaques1 -->

  <div class="container" style="margin-bottom:50px;">
    <div class="row imob-row">          
      <div class="col-md-1 visible-lg" style="width:45px; padding: 0;">
        <a class="imob-left carousel-control" href="#imob-imovel-destaque2" role="button" data-slide="prev">
          <img src="<?php echo $imagePath?>btn-left.png" alt="Botão anterior">              
        </a>
      </div>

      <div id="imob-imovel-destaque2" class="col-md-11 carousel slide carousel-imovel-destaque" data-ride="carousel" data-type="multi" data-interval="5000" >  <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <?php $i=0; foreach ($destaques2 as $destaque2) { ?>
          <div class="item <?php echo ($i==0?'active':''); $i++;?>">              
              <div class="col-md-3 col-sm-4 col-xs-12 imob-thumb">
                <div class="thumbnail">                      
                  <div class="col-md-12 col-xs-12 imob-thumbnail">
                    <a href="<?php echo $local.'index.php/imoveis/detalhes/aluguel/'.$destaque2['tim_des'].'/'.$destaque2['imo_isn']?>" ><img src="<?php echo $destaque2['foto']?>" style="width:229px; height:241px;" class="img-responsive" alt="Imóvel destaque"></a>
                    <h3 class="imob-bairro"><?php echo $destaque2['imo_des_bai']?></h3>
                    <p class="text-center col-md-5 col-xs-5"><i class="fa fa-2x fa-bed" aria-hidden="true"></i><br>DORMITÓRIOS: <?php echo ($destaque2['caracs'][0]['qtd_suites'] + $destaque2['caracs'][0]['qtd_quartos']);?></p>
                    <p class="text-center col-md-3 col-xs-3"><i class="fa fa-2x fa-car" aria-hidden="true"></i><br>VAGAS: <?php echo $destaque2['caracs'][0]['qtd_vagas']?></p>
                    <p class="text-center col-md-4 col-xs-4">
                      <img src="<?php echo $imagePath?>icon-trena.png" class="img-responsive" alt="Imóvel destaque">ÁREA: <?php echo $destaque2['imo_val_are']?>m²
                    </p>
                    <h3 class="col-md-12 col-xs-12 text-center imob-tipo-imovel"><?php echo $destaque2['tim_des']?></h3>
                    <p class="col-md-8 col-xs-8" style="font-size: 1em;">&nbsp;CÓDIGO: <?php echo $destaque2['imo_isn']?></p>
                    <p class="text-center col-md-2 col-xs-2 imob-rs-destaques"><a id="fbshare<?php echo $destaque2['imo_isn']?>" href="javascript:void(0)" data-link="<?php echo $local?>index.php/imoveis/detalhes/aluguel/<?php echo $destaque2['tim_des']?>/<?php echo $destaque2['imo_isn']?>" data-image="<?php echo $local.$destaque2['foto']?>" data-title="<?php echo utf8_encode($destaque2['imo_des_est']).' - '.utf8_encode($destaque2['tim_des'])?>" data-desc="<?php echo utf8_encode($destaque2['imo_des_est'])?>" onClick="btnShare(<?php echo $destaque2['imo_isn']?>)"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></p>
                    <p class="text-center col-md-2 col-xs-2 imob-rs-destaques"><a href="<?php echo $local.'index.php/imoveis/detalhes/aluguel/'.$destaque2['tim_des'].'/'.$destaque2['imo_isn']?>"><i class="fa fa-plus" aria-hidden="true"></i></a></p>
                  </div>
                </div>
              </div>
          </div>
		<?php } ?> 
        </div>
      </div>       

      <div class="col-md-1 visible-lg" style="width:45px; padding: 0;">
        <a class="imob-right carousel-control" href="#imob-imovel-destaque2" role="button" data-slide="next">
          <img class="img-reponsive" src="<?php echo $imagePath?>btn-right.png" alt="Botão próximo">              
        </a>
      </div>

      <div class="col-md-6 col-sm-6 col-xs-6 hidden-lg">
        <a class="imob-left-mobile carousel-control" href="#imob-imovel-destaque2" role="button" data-slide="prev">
          <img src="<?php echo $imagePath?>btn-left-mobile.png" alt="Botão anterior">              
        </a>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-6 hidden-lg">
        <a class="imob-right-mobile carousel-control" href="#imob-imovel-destaque2" role="button" data-slide="next">
          <img class="img-reponsive" src="<?php echo $imagePath?>btn-right-mobile.png" alt="Botão próximo">              
        </a>
      </div>
      
    </div>        
  </div><!-- /.container carousel destaques2 -->

</section><!-- /.section destaques -->    

    <section class="imob-section-areacli">
    
       <div class="row imob-row">
         <div class="col-sm-6 col-md-6 col-lg-4 col-lg-offset-2" style="padding-left:0;">
            <img class="img-responsive pull-right" src="<?php echo $imagePath?>bg-area-cliente.png" alt="Área cliente">
          </div>
         <div class="col-sm -6 col-md-6 col-lg-6">
           <div class="col-sm-6 col-md-12 col-lg-10">
              <h3>Área restrita para<br>&nbsp;&nbsp;&nbsp;inquilinos e proprietários</h3>
              <form action="<?php echo $local?>index.php/cliente/arearestrita" name="formLogin" id="formLogin" method="post">
				  <input type="hidden" name="botao" id="botao">
				  <input type="hidden" name="comando" value="validarUsuario">
				  <input type="hidden" name="valida" value="1">
                <div class="form-group">                    
                    <input type="text" class="form-control input-areacli" placeholder="CPF/CNPJ" id="login" name="login" maxlength="18" onKeyUp="FormataCpfCgc('login',18,event,'formLogin')">
                  </div> 
                  <div class="form-group">                    
                    <input type="password" class="form-control input-areacli" placeholder="Senha" id="senha_usu" name="senha_usu">
                  </div>
                  <button type="button" class="btn imob-btn-areacli text-center" onClick="javascript: validaLogin()">ENTRAR</button> 
              </form>
              <p class="icon-areacliente"><i class="fa fa-2x fa-barcode" aria-hidden="true"></i></a></li>Inquilino efetue o login para retirar sua 2ª via de boleto</p>
              <p class="icon-areacliente"><i class="fa fa-2x fa-calculator" aria-hidden="true"></i></a></li>Proprietário efetue o login para retirar sua prestação de contas e sua declaração de IRRF</p>
           </div>
            
         </div>
       </div>
    </section><!-- /.section area cliente -->

    <section class="imob-section-redes-sociais">
      <div class="container">
        <div class="row imob-row">
          <div class="col-md-6 col-sm-6" style="margin-bottom: 20px;"> 
          	<h1 style="font-family: 'Calibri'; font-size: 2em;"><i class="fa fa-facebook-square" aria-hidden="true"></i> Facebook</h1>     
            <div class="fb-page" data-href="https://www.facebook.com/dmvimobiliaria" data-tabs="timeline" data-width="500" data-height="410" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/dmvimobiliaria" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/dmvimobiliaria">DMV Imóveis</a></blockquote></div>
          </div>
          <div class="col-md-6 col-sm-6"> 
          	<h1 style="font-family: 'Calibri'; font-size: 2em;"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</h1>   
            <!-- LightWidget WIDGET --><script src="//lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/cc3af95432575a578776524ef18501d0.html" id="lightwidget_cc3af95432" name="lightwidget_cc3af95432"  scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>         
          </div>
        </div>
      </div>
    </section><!-- /.section rede social -->    