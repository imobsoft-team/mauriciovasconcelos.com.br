<div class="newsletter wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
    <div class="container">
        <div class="block-newsletter">
            <h2 class="headline">Cadastre-se para receber nossa newsletter</h2>
            <form class="form-inline">
                <div class="form-group">
                    <input type="email" class="form-control input-custom">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary input-custom">
                        <i class="fa fa-envelope-o"></i>
                    </button>
                </div>
            </form>
        </div>
    </div><!-- container -->
</div><!-- newsletter-->
