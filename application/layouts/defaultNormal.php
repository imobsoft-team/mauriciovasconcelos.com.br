<!--
/**
 * Created by PhpStorm.
 * User: Diego Pedrosa
 * Date: 03/09/2015
 * Time: 10:27
 */
-->

<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Basic Page Needs
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title>{title_for_layout}</title>
    <meta name="description" content="{description_for_layout}">
    <meta name="author" content="Diego Pedrosa - Imobsoft Softwares Imobiliários">
	<meta http-equiv="Cache-Control" content="public, max-age=3600, must-revalidate" />
	
    {keys_for_layout}

    <!-- Mobile Specific Metas
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="stylesheet" href="<?php echo $local?>application/css/magnific-popup.css" type="text/css">
    {css_for_layout}


    <!-- Favicon
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="shortcut icon" href="<?php echo $local?>application/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo $local?>application/images/favicon.ico" type="image/x-icon">

</head>
<!--onLoad="popUpAviso()" colocar isso no Body para ativar o Banner inicial de avisos... -->
<body style="background-repeat:no-repeat;">
<a style="display:none;" id="popUpAviso" class="image-popup-no-margins" href="<?php echo $local?>application/images/folderferiado.jpg">
	<img src="<?php echo $local?>application/images/folderferiado.jpg" width="107" height="75">
</a>

<div id="fb-root"></div>
<script type="text/javascript">
window.fbAsyncInit = function(){
            FB.init({
                appId: '763003123810086', status: true, cookie: true, xfbml: true });
        };
        (function(d, debug){var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];if   (d.getElementById(id)) {return;}js = d.createElement('script'); js.id = id; js.async = true;js.src = "//connect.facebook.net/en_US/all" + (debug ? "/debug" : "") + ".js";ref.parentNode.insertBefore(js, ref);}(document, /*debug*/ false));
        function postToFeed(title, desc, url, image){
            var obj = {method: 'feed',link: url, picture: image,name: title,description: desc};
            function callback(response){}
            FB.ui(obj, callback);
        }
		
		
		function btnShare(id){
        elem = $('#fbshare'+id);
        postToFeed(elem.data('title'), elem.data('desc'), elem.data('link'), elem.data('image'));
		//alert(elem.data('title')+elem.data('desc')+elem.data('link')+elem.data('image'));
        return false;
    };
 </script>
 
<div id="google_translate_element" style="display:none;"></div> 
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<div id="Bavancada" style=" position:absolute;display:none;width:600px;height:auto;background:#FFFFFF;left:60%;margin-left:-400px;text-align:center" ></div>

<div id="paraMaps" style="position:absolute;display:none;width:980px;height:340px;background-image:url(<?php echo $local; ?>application/images/bg_mapa.jpg); background-position:left; background-repeat:no-repeat;left:60%;margin-left:-590px;text-align:center" ></div>
<!-- Primary Page Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="container">
    <div id="menu_slide"><a href="#" class="custom-menu-trigger-selector"><img src="<?php echo $imagepath;?>menu.png"/></a></div>
    <div class="row">
        <div class="sixteen columns">
            <div class="four columns" id="logo"><a href="<?php echo $local;?>"><img src="<?php echo $imagepath;?>logo.png"/></a></div>
            <div class="twelve columns">
                <div class="twelve columns" id="barra_menu">
                    <img src="<?php echo $imagepath;?>barra_menu.png"/>
                    <div id="menu">
                        <ul>
                            <li><a href="<?php echo $local;?>">Home</a><img src="<?php echo $imagepath;?>barra.jpg"/></li>
                            <li><a href="<?php echo $local;?>index.php/internas/pagina/empresa">Empresa</a><img src="<?php echo $imagepath;?>barra.jpg"/></li>
                            <li><a href="<?php echo $local;?>index.php/internas/pagina/servicos">Serviços</a><img src="<?php echo $imagepath;?>barra.jpg"/></li>
                            <li><a href="<?php echo $local;?>index.php/internas/pagina/inquilino">Inquilinos</a><img src="<?php echo $imagepath;?>barra.jpg"/></li>
                            <li><a href="<?php echo $local;?>index.php/internas/cadastro">Alugue Conosco</a><img src="<?php echo $imagepath;?>barra.jpg"/></li>
                            <li><a href="<?php echo $local;?>index.php/internas/cadastro">Venda Conosco</a></li>
                        </ul>
                    </div>
                    <div id="bandeiras" style="">
                        <div id="brasil" onclick="ChangeLang('pt')"><img src="<?php echo $imagepath;?>brasil.png"/> </div>
                        <div id="espanha" onclick="ChangeLang('es')"><img src="<?php echo $imagepath;?>espanha.png"/> </div>
                        <div id="eua" onclick="ChangeLang('en')"><img src="<?php echo $imagepath;?>eua.png"/> </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="four columns" id="telefone">
                    <span>Dúvidas?</span>
                    <a href="/chat/client.php?locale=pt-br&amp;style=silver" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &amp;&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('/chat/client.php?locale=pt-br&amp;style=silver&amp;url='+escape(document.location.href)+'&amp;referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=640,height=480,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;"><img src="<?php echo $local?>chat/b.php?i=mblue&amp;lang=pt-br"/></a>
                </div>
				
                <div class="tabs-guias-form eight columns">
                    <div class="tabs-form clearfix-form">
                        <ul>
                            <li><a class="active-tab-form" href="#" data-tab="tab1">BUSCA POR <span style="color: #ed3237; font-weight: bold;">ALUGUEL</span></a></li>
                            <li><a href="#" data-tab="tab2">BUSCA POR <span style="color: #ed3237; font-weight: bold;">VENDA</span></a></li>                               
                        </ul>
                    </div> <!-- \fim guias de navegacao -->

                    
                    <div class="tab1 tabs first-tab"><!-- formulario de pesquisa de aluguel -->
                        <div class="tmn-form-pesq">
                            <div id="buscalocacao">
                                <?php include("application/helpers/busca_locacao.php"); ?>
                            </div>
                        </div>
                    </div><!-- \formulario de pesquisa de aluguel -->


                    <div class="tab2 tabs"><!-- formulario de pesquisa de venda -->
                        <div class="tmn-form-pesq">
                            <div id="buscaVenda">
                                <?php include("application/helpers/busca_venda.php"); ?>
                            </div>
                        </div>
                    </div><!-- \formulario de pesquisa de venda -->                                         
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div id="botoes_home" class="sixteen columns">
            <div id="negocie" class="four columns">
				<a href="<?php echo $local;?>index.php/internas/cadastro">
					<div style="width:220px; height:61px; background-image:url('<?php echo $imagepath;?>negocie_old.jpg');padding-top:3px;">
						<!--span style="font-size:11px; font-family:Verdana;color:#000;font-weight:bolder;">CLIQUE AQUI<span-->
						<span style="font-weight:bold;color:#000000;letter-spacing:0pt;word-spacing:0pt;font-size:14px;text-align:left;font-family:verdana, sans-serif;line-height:1;">NEGOCIE SEU</span><span id="traduImovel" style="font-weight:bold;color:red;letter-spacing:2pt;word-spacing:-2pt;font-size:26px;text-align:left;font-family:verdana, sans-serif;line-height:1;display:block;">IMÓVEL</span>
					</div>
				</a>
            </div>
            <div id="area_restrita" class="four columns">
				<a href="<?php echo $local;?>index.php/cliente/logar">
					<div id="area_restritaDiv">
						<!--span style="font-size:11px; font-family:Verdana;color:#000;font-weight:bolder;">CLIQUE AQUI<span-->
						<span id="txtSpan" style="font-weight:bold;color:#000000;letter-spacing:0pt;word-spacing:0pt;font-size:8px;text-align:left;font-family:verdana, sans-serif;line-height:1;">INQUILINOS E PROPRIETÁRIOS</span>
						<span id="traduArea" style="font-weight:bold;color:#000;letter-spacing:-2pt;word-spacing:0pt;font-size:18px;text-align:left;font-family:verdana, sans-serif;line-height:1;">ÁREA</span><span id="traduRestrita" style="font-weight:bold;color:red;letter-spacing:-1pt;word-spacing:-2pt;font-size:18px;text-align:left;font-family:verdana, sans-serif;line-height:1;margin-left:10px;">RESTRITA</span>
					</div>
                </a>                
            </div>
            <div id="mapa" class="four columns">
				<a href="javascript:void(0)" onclick="carDiv('paraMaps','','<?php echo $local;?>','<?php echo $local;?>/index.php/ajax/desenhaMap','');">
					<div style="width:220px; height:57px; background-image:url('<?php echo $imagepath;?>mapa_old.jpg');padding-top:3px;">
						<!--span style="font-size:11px; font-family:Verdana;color:#000;font-weight:bolder;">CLIQUE AQUI<span-->
						<span style="font-weight:bold;color:#000000;letter-spacing:0pt;word-spacing:0pt;font-size:14px;text-align:left;font-family:verdana, sans-serif;line-height:1;">BUSQUE PELO</span>
						<span style="font-weight:bold;color:red;letter-spacing:2pt;word-spacing:-2pt;font-size:26px;text-align:left;font-family:verdana, sans-serif;line-height:1;display:block;">MAPA</span>
					</div>
				</a>
            </div>
            <div id="avancada" class="four columns">
				<a href="javascript:void(0)" onclick=" carDiv('Bavancada','','<?php echo $local?>','<?php echo $local?>index.php/ajax/avancada','')">
					<div style="width:220px; height:57px; background-image:url('<?php echo $imagepath;?>avancada_old.jpg');padding-top:3px;">
						<!--span style="font-size:11px; font-family:Verdana;color:#000;font-weight:bolder;">CLIQUE AQUI<span-->
						<span style="font-weight:bold;color:#000000;letter-spacing:0pt;word-spacing:0pt;font-size:10px;text-align:left;font-family:verdana, sans-serif;line-height:1;">FAÇA UMA</span>
						<span id="traduBusca" style="font-weight:bold;color:#000;letter-spacing:-1pt;word-spacing:0pt;font-size:16px;text-align:left;font-family:verdana, sans-serif;line-height:1;">BUSCA</span><span id="traduAvancada" style="font-weight:bold;color:red;letter-spacing:-1pt;word-spacing:-2pt;font-size:16px;text-align:left;font-family:verdana, sans-serif;line-height:1;margin-left:10px;">AVANÇADA</span>
					</div>
                </a>
            </div>
        </div>
    </div>

    {content_for_layout}

    <div id="fundo_rodape" class="row">

        <div class="row">
            <div class="sixteen columns">
                <img src="<?php echo $imagepath;?>barra_divisao.jpg"/>
            </div>
        </div>

        <div class="row">
            <div class="sixteen columns">
                <div class="four columns">
                    <div id="menu_rodape">
                        <p><a href="<?php echo $local;?>">Home</a></p>
                        <p><a href="<?php echo $local;?>index.php/internas/pagina/empresa">Empresa</a></p>
                        <p><a href="<?php echo $local;?>index.php/internas/pagina/servicos">Serviços</a></p>
                        <p class="ocultar"><a href="<?php echo $local;?>index.php/cliente/logar">Segunda via de boleto</a></p>
                        <p class="ocultar"><a href="<?php echo $local;?>index.php/cliente/logar">Prestação de contas</a></p>
                        <p><a href="<?php echo $local;?>index.php/internas/cadastro">Alugue Conosco</a></p>
                        <p><a href="<?php echo $local;?>index.php/internas/cadastro">Venda Conosco</a></p>
                    </div>
                    <div id="barra_rodape">
                        <img src="<?php echo $imagepath;?>barra_rodape.jpg"/>
                    </div>
                </div>
                <div class="four columns">
                    <div id="menu_rodape">
                        <p class="ocultar"><a href="javascript:void(0)" onclick="carDiv('paraMaps','','<?php echo $local;?>','<?php echo $local;?>/index.php/ajax/desenhaMap','');">Busca pelo mapa</a></p>
                        <p class="ocultar"><a href="javascript:void(0)" onclick=" carDiv('Bavancada','','<?php echo $local?>','<?php echo $local?>index.php/ajax/avancada','')">Busca avançada</a></p>
                        <!--p><a href="#">Newsletter</a></p-->
                        <p><a href="http://imobiliariatorresdemeloneto.blogspot.com.br/" target="_blank">Blogger</a></p>
                        <p><a href="<?php echo $local;?>index.php/internas/parceiros">Nossos parceiros</a></p>
                        <p class="ocultar"><a href="#">Fale Conosco</a></p>
                    </div>
                    <div id="barra_rodape">
                        <img src="<?php echo $imagepath;?>barra_rodape.jpg"/>
                    </div>
                </div>
                <div class="eight columns">
                    <img src="<?php echo $imagepath;?>endereco.png"/>
					<div style="position:absolute;margin: -60px 0 0 410px;">
						<a href="https://www.facebook.com/torres.d.neto?ref=ts&fref=ts" target="_blank"><img src="<?php echo $imagepath;?>facebookRodape.png"/></a>
						<a href="http://www.online-instagram.com/user/imobiliariatorresdemeloneto/2113802343" target="_blank"><img src="<?php echo $imagepath;?>instagramRodape.png"/>
					</div>
                    <a href="https://www.imobsoft.com.br" target="_blank"><img id="imobsoft" src="<?php echo $imagepath;?>imobsoft.png"/></a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="mascara"></div>
<nav id="custom-menu-selector" style="width: 300px; z-index: 1; display: none; background-color: #ED3237;">
    <ul>
        <li><a href="<?php echo $local;?>">Home</a></li>
        <li><a href="<?php echo $local;?>index.php/internas/pagina/empresa">Empresa</a></li>
        <li><a href="<?php echo $local;?>index.php/internas/pagina/servicos">Serviços</a></li>
        <li><a href="<?php echo $local;?>index.php/internas/pagina/inquilino">Inquilinos</a></li>
        <li><a href="<?php echo $local;?>index.php/internas/cadastro">Alugue Conosco</a></li>
        <li><a href="<?php echo $local;?>index.php/internas/cadastro">Venda Conosco</a></li>
    </ul>
    <div style="width:100%; height: 100px; margin-top:290px; background-color: #fff;"><img width="156" height="61" style="margin:15px 0 0 45px;" src="<?php echo $imagepath?>imobsoft.png"/> </div>
</nav>


    <!-- JAVASCRIPT
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->

    <script type="text/javascript">
        <!--
        var localGer = "<?php echo $local?>";
        -->
    </script>	

    <!-- Google Maps API
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->

    <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

    {js_for_layout}
	
	<script src="<?php echo $local?>application/js/jquery.magnific-popup.min.js"></script>
	
	<script type="text/javascript">
        $(document).ready(function(){
            $('#opcoes2 option').each(function(){
                if($(this).val() == '1'){
                    $(this).attr('selected',true);
                }
            });
			listarCidades2(1,'<?php echo $local?>');
			
			function verificaTraducao () {		
				if ($(".goog-te-menu-frame:eq(0)").contents().find("span:contains('en')")) {
					$("#bairroBuscaLoc").css("fontSize", "10px");
					$("#bairroBuscaVen").css("fontSize", "10px");
					$("#traduImovel").css("fontSize", "24px");
					$("#traduArea").css("fontSize", "15px");
					$("#traduRestrita").css("fontSize", "15px");
					$("#traduBusca").css("fontSize", "16px");
					$("#traduAvancada").css("fontSize", "15px");
				} else if ($(".goog-te-menu-frame:eq(0)").contents().find("span:contains('es')")) {
					$("#bairroBuscaLoc").css("fontSize", "16px");
					$("#bairroBuscaVen").css("fontSize", "16px");
					$("#traduImovel").css("fontSize", "20px");
					$("#traduArea").css("fontSize", "15px");
					$("#traduRestrita").css("fontSize", "14px");
					$("#traduBusca").css("fontSize", "13px");
					$("#traduAvancada").css("fontSize", "13px");
				} else if ($(".goog-te-menu-frame:eq(0)").contents().find("span:contains('pt')")) {
					$("#bairroBuscaLoc").css("fontSize", "16px");
					$("#bairroBuscaVen").css("fontSize", "16px");
					$("#traduImovel").css("fontSize", "26px");
					$("#traduArea").css("fontSize", "18px");
					$("#traduRestrita").css("fontSize", "18px");
					$("#traduBusca").css("fontSize", "16px");
					$("#traduAvancada").css("fontSize", "16px");
				}				
				
			}
			
			setTimeout(verificaTraducao,5000);
			
			jQuery(function($){
			  $('.tabs-form ul li a').click(function(){
				var a = $(this);
				var active_tab_class = 'active-tab-form';
				var the_tab = '.' + a.attr('data-tab');
				
				$('.tabs-form ul li a').removeClass(active_tab_class);
				a.addClass(active_tab_class);
				
				$('.tabs-guias-form .tabs').css({
				  'display' : 'none'
				});
				
				$(the_tab).show();
				
				return false;
			  });
			});
			
        });
    </script>

    <script type="text/javascript">

        function  chad(){
			$('#opcoes option').each(function(){
                if($(this).val() == '2'){
                    $(this).attr('selected',true);
					listarCidades(2,'<?php echo $local?>');
                }
            });
            
            $('#codigo').prop('disabled', false);
            $('#tipo').prop('disabled', false);
            $('#cidade').prop('disabled', false);
            $('#valorini').prop('disabled', false);
            $('#valorfim').prop('disabled', false);
            $('#bairro').prop('disabled', false);
        };

        setTimeout(chad, 5000);
		
		function popUpAviso() {
			if(document.getElementById('validaPopUp').value == 'true'){
				document.getElementById("popUpAviso").click();
			}
		}

    </script>

<script type="text/javascript">
    $(document).ready(function() {

        var jPM = $.jPanelMenu({
            menu: '#custom-menu-selector',
            trigger: '.custom-menu-trigger-selector'
        });

        jPM.on();

        $("#owl-locacao_1").owlCarousel({
            items : 4, //10 items above 1000px browser width
            itemsDesktop : [1000,4], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,2], // betweem 900px and 601px
            itemsTablet: [600,1], //2 items between 600 and 0
            itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
        });

        $("#owl-locacao_2").owlCarousel({
            items : 4, //10 items above 1000px browser width
            itemsDesktop : [1000,4], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,2], // betweem 900px and 601px
            itemsTablet: [600,1], //2 items between 600 and 0
            itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
        });

        $("#owl-venda_1").owlCarousel({
            items : 4, //10 items above 1000px browser width
            itemsDesktop : [1000,4], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,2], // betweem 900px and 601px
            itemsTablet: [600,1], //2 items between 600 and 0
            itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
        });

        $("#owl-venda_2").owlCarousel({
            items : 4, //10 items above 1000px browser width
            itemsDesktop : [1000,4], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,2], // betweem 900px and 601px
            itemsTablet: [600,1], //2 items between 600 and 0
            itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
        });
		
		$('.image-popup-no-margins').magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			closeBtnInside: false,
			fixedContentPos: true,
			mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
			image: {
				verticalFit: true
			},
			zoom: {
				enabled: true,
				duration: 300 // don't foget to change the duration also in CSS
			}
		});

    });
</script>


<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>

</html>
