<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{description_for_layout}">
    <meta name="author" content="Imobsoft Softwares Imobiliários">
    <meta http-equiv="Cache-Control" content="public, max-age=3600, must-revalidate" />

    {metatags_for_layout}

    <title>{title_for_layout}</title>

    <!-- Bootstrap -->
    {css_for_layout}

    <!-- JAVASCRIPT
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->

    <script type="text/javascript">
        <!--
        var localGer = "<?= $local ?>";
        -->
    </script>

    <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Favicon
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $local?>application/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $local?>application/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $local?>application/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $local?>application/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $local?>application/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $local?>application/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $local?>application/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $local?>application/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $local?>application/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo $local?>application/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $local?>application/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $local?>application/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $local?>application/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $local?>application/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

</head>

<body>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.11&appId=1549770892018915&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div id="google_translate_element" style="display:none;"></div>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<div id="google_translate_element" style="display:none;"></div>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<div id="Bavancada" style=" position:absolute;display:none;width:600px;height:auto;background:#FFFFFF;left:60%;margin-left:-400px;text-align:center" ></div>

<div id="paraMaps" style="position:absolute;display:none;width:980px;height:340px;background-image:url(<?php echo $local; ?>application/images/bg_mapa.jpg); background-position:left; background-repeat:no-repeat;left:60%;margin-left:-590px;text-align:center" ></div>

<?php include_once "application/views/principal/menu_responsivo.php" ?>

<header>
    <?php include_once "application/views/principal/header.php" ?>
</header>

{content_for_layout}

<footer>
    <?php include_once "application/views/principal/footer.php" ?>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script-->
<!-- Include all compiled plugins (below), or include individual files as needed -->

{js_for_layout}

<script type="text/javascript">
    $(document).ready(function() {
        $('#opcoes2 option').each(function(){
            if($(this).val() == '1'){
                $(this).attr('selected',true);
            }
        });

        listarCidades2(1,'<?php echo $local?>');
    });
</script>

</body>
</html>
