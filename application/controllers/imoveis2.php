<?php
class Imoveis extends Controller {
	public $layout = 'default';
	public $title = 'Torres de Melo Neto';
	
	public $css = array('jquery.lightbox-0.5','ajaxtabs','styleCarro','template','diapo');
	
	public $js = array('jquery.min','js','jquery.mobile-1.0rc2.customized.min','jquery.easing.1.3','global','internas','jquery.hoverIntent.minified','busca','diapo','jquery.easing.1.3','jquery.contentcarousel','jquery.maskedinput-1.1.1','jquery.innerfade','slider.js','tur.js','js','ajaxtabs','global','internas','swfobject','busca','jquery.min','js','jquery.mobile-1.0rc2.customized.min','jquery.easing.1.3','global','internas','jquery.hoverIntent.minified','busca','diapo','jquery.easing.1.3','jquery.contentcarousel','geraMaps','smartinfowindow');
	
	public $data = array('local'=>"../../../",'logomarca'=>'/application/images/logo.jpg','empresa'=>'Torres de Melo Neto','cnpj'=>'99.999.999/0001-99','fones'=>'(85) 4011.0800','endereco'=>'Rua Professor Dias da Rocha, 162 1� Andar - Meireles/Fortaleza/CE, Brasil.','sessao'=>'imoveisEqua','sessaoCli'=>'EquaCli','url'=>'http://torresdemeloneto.com.br/');
	public $emails = array('Loca��o|locacao@imobiliaria@torresdemeloneto.com.br','Vendas|imobiliaria@torresdemeloneto.com.br','Financeiro|imobiliaria@torresdemeloneto.com.br');
	public $configMail = array('protocol' => '0',
    'smtp_host' => '0',
    'smtp_port' => 0,
    'smtp_user' => '0',
    'smtp_pass' => '0',
    );
	public $description = '';
	public $keys = array();
	
	function Imoveis()
	{
		parent::Controller();
		if(!isset($_SESSION['idioma'])){
			session_register('idioma');
			$_SESSION['idioma'] = 1;
		}
		if(isset($_GET['idioma']) && is_numeric($_GET['idioma'])){
			switch($_GET['idioma']){
				case 2:
					$_SESSION['idioma'] = 2;
					break;
				case 3;
					$_SESSION['idioma'] = 3;
					break;
				default:
					$_SESSION['idioma'] = 1;
			}
		}
		if(!isset($_SESSION[''.$this->data['sessao']])){
			//session_register(''.$this->data['sessao']);
			$this->data['sessao'];
		}
		if(!isset($_SESSION['parametros'])){
			//session_register('parametros');
			$_SESSION['parametros']="";
		}
		if(!isset($_SESSION[''.$this->data['sessaoCli']])){
		$this->data['sessaoCli'];
			//session_register(''.$this->data['sessaoCli']);
		}
		//$logado = $_SESSION[''.$this->data['sessaoCli']];
		$logado = $this->data['sessaoCli'];
		$this->load->model('enquete');
		$this->data['pergunta'] = $this->enquete->getAsk($logado);
		$this->data['resp']="";
		if($this->data['pergunta']){
			$this->data['resp'] = $this->enquete->getResp($this->data['pergunta']["id"]);
		}
		if(!isset($_SESSION['origem'])){
			session_register('origem');
		}
		$_SESSION['origem']?$this->data['origem'] = $_SESSION['origem']:$this->data['origem'] = $this->data["local"];
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		//conta acesso ao site
		$this->load->model('tab_acs');
		$this->tab_acs->insertAcssite(session_id());
		//pega a tradu��o para a p�gina
		$this->load->model('utilidades');
		$this->data['tradutor'] = $this->utilidades->Tradutor($_SESSION['idioma'],$this->data['local']);
		//pega os imoveis para obama
		$this->load->model('imovelvenda');
		$this->load->model('lancamento');
		$imoveis = $this->imovelvenda->getDestaques(500);
		$lancs   = $this->lancamento->getDestaques(500);
		$banner ="";
		if(is_array($lancs) && is_array($imoveis)){
			$banner = array_merge($lancs,$imoveis);
		}else if(is_array($imoveis)){
			$banner = $imoveis;
		}else if(is_array($lancs)){
			$banner = $lancs;
		}
		$imoveis = "";
		is_array($banner)?$quant=count($banner):$quant=0;
		for($i=0;$i<$quant && $i<6;$i++){
			if($banner[$i]['imo_isn']>0){
				$imoveis[$i]['link'] = $this->data['local'].'index.php/imoveis/detalhes/venda/'.$banner[$i]['tim_des'].'/'.$banner[$i]['imo_isn'];
				$imoveis[$i]['texto'] = $banner[$i]['imo_des_est'];
				$imoveis[$i]['foto'] = $this->data['local'].$banner[$i]['foto'];
				$imoveis[$i]['tipo'] = $banner[$i]['tim_des'];
			}else {
				$imoveis[$i]['link'] = $this->data['local'].'index.php/imoveis/detalhes/empreendimento/'.$banner[$i]['tipo'].'/'.$banner[$i]['emp_isn'];
				$imoveis[$i]['texto'] = $banner[$i]['emp_des_txt'];
				$imoveis[$i]['foto'] = $this->data['local'].$banner[$i]['foto'];
				$imoveis[$i]['tipo'] = $banner[$i]['tipo'];
			}
		}
		$_SESSION["banners"] = $imoveis;
		$this->load->model('tab_banner');
		$this->data['banners'] = $this->tab_banner->buscarBannerAti();
		$this->data['temBanner'] = 0;
	}
	
	function index()
	{
		$this->load->helper('url');
		redirect('','refresh');
	}
	
	function Detalhes($tip,$tipo,$codigo){
		$view = 'exibe_detalhes';
		$this->output->cache(1);
		switch($tip){
			case "venda":
				$this->load->model('imovelvenda');
				$this->data['imovel'] = $this->imovelvenda->consultarImovel($codigo,200,$this->data["local"]);
				if($this->data['imovel']){
					//$this->title = '';
					$this->description = $this->data['imovel'][0]['imo_des_est'];
					array_push($this->keys,'VENDA',$this->data['imovel'][0]['tim_des']);
					$this->data['caracsP'] = $this->data['imovel'][0]['caracs'];
				}
				break;
			case "aluguel":
				$this->load->model('imovelloc');
				$this->data['imovel'] = $this->imovelloc->consultarImovel($codigo,200,$this->data["local"]);
				if($this->data['imovel']){
					//$this->title = '';
					$this->description = $this->data['imovel'][0]['imo_des_est'];
					array_push($this->keys,'ALUGUEL',$this->data['imovel'][0]['tim_des']);
					$this->data['caracsP'] = $this->data['imovel'][0]['caracs'];
				}
				break;
			case "empreendimento":
				$this->load->model('lancamento');
				$this->data['imovel'] = $this->lancamento->consultarImovel($codigo,200,$this->data["local"]);
				$view = 'exibe_empr';
				if($this->data['imovel']){
					//$this->title = '';
					$this->description = $this->data['imovel'][0]['emp_des_txt'];
					array_push($this->keys,'VENDA',$this->data['imovel'][0]['tipo']);
					$this->data['clas'] = $this->data['imovel'][0]["classes"];
				}
				break;
		}
		$this->data['tipo'] = $tip;
		$this->data['idm'] = $this->session->userdata('idioma');
		array_push($this->js,'global','venda','jq-corner','jquery.lightbox-0.5');
		$_SESSION['origem']?$this->data['origem'] = $_SESSION['origem']:$this->data['origem'] = $this->data["local"];
		if($this->data['imovel']){
			$this->data['imagem'] = $this->data['imovel'][0]['foto'];
			$this->data['fotos'] = $this->data['imovel'][0]['fotos'];
		}
		$this->load->view('imovel/'.$view,$this->data);
	}
	
	function localizacaoSatelite($tip,$codigo){
		switch($tip){
			case "venda":
				$this->load->model('imovelvenda');
				$imovel = $this->imovelvenda->consultarImovel($codigo,200,$this->data["local"]);
				break;
			case "aluguel":
				$this->load->model('imovelloc');
				$imovel = $this->imovelloc->consultarImovel($codigo,200,$this->data["local"]);
				break;
			case "empreendimento":
				$this->load->model('lancamento');
				$imovel = $this->lancamento->consultarImovel($codigo,200,$this->data["local"]);
				break;
		}
		$endereco = explode(",",$imovel[0]["imo_des_end"]);
		$endereco = $endereco[0];
		$bairro   = $imovel[0]["imo_des_bai"];
		$cidade   = $imovel[0]["imo_des_loc"];
		$endereco = preg_replace ("/R./", "", $endereco);
		$endereco = preg_replace ("/RUA./", "", $endereco);
		$endereco = preg_replace ("/AV./", "", $endereco);
		$endereco = preg_replace ("/AV /", "", $endereco);
		$endereco = preg_replace ("/./", "", $endereco);
		$endereco = preg_replace ("/-/", "", $endereco);
		
		$endereco = $endereco." ".$bairro." ".$cidade;
		
		$endereco = preg_replace ("/[����]/","a",$endereco);
		$endereco = preg_replace ("/[����]/","A",$endereco);
		$endereco = preg_replace  ("/[���]/","e",$endereco);
		$endereco = preg_replace ("/[���]/","E",$endereco);
		$endereco = preg_replace ("/[�����]/","o",$endereco);
		$endereco = preg_replace ("/[����]/","O",$endereco);
		$endereco = preg_replace ("/[���]/","u",$endereco);
		$endereco = preg_replace ("/[���]/","U",$endereco);
		$endereco = preg_replace ("/�/","c",$endereco);
		$endereco = preg_replace ("/�/","C",$endereco);
		header("Location: http://maps.google.com/maps?q=$endereco&spn=0.043344,0.063343&t=M&hl=pt");
	}
	
	function Indique($tip,$codigo){
		$this->title = "Indique para um amigo";
		$this->layout = "popup";
		$this->css = array('popup');
		$this->data['tipo'] = $tip;
		$this->data['codigo'] = $codigo;
		$this->load->helper('url');
		array_push($this->js,'venda');
		$this->data['submit'] = $this->data["local"]."index.php/imoveis/enviaAmigo/";
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->load->view('imovel/enviar_amigo',$this->data);
	}
	
	function Imprimir($tip,$codigo){
		switch($tip){
			case "venda":
				$this->load->model('imovelvenda');
				$this->data['imovel'] = $this->imovelvenda->consultarImovel($codigo,200,$this->data["local"]);
				break;
			case "aluguel":
				$this->load->model('imovelloc');
				$this->data['imovel'] = $this->imovelloc->consultarImovel($codigo,200,$this->data["local"]);
				break;
		}
		$this->data['caracsP'] = $this->data['imovel'][0]['caracs'];
		$this->title = "Impress�o do im�vel ";
		$this->layout = "popup";
		$this->css = array('imprimir');
		$this->data['tipo'] = $tip;
		array_push($this->js,'venda');
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->load->view('imovel/imprimir',$this->data);
	}
	
	function enviaAmigo(){
		$this->load->library('form_validation');
		switch($this->input->post('tipo')){
			case "venda":
				$this->load->model('imovelvenda');
				$imovel = $this->imovelvenda->consultarImovel($this->input->post('codigo'),200,$this->data["local"]);
				break;
			case "aluguel":
				$this->load->model('imovelloc');
				$imovel = $this->imovelloc->consultarImovel($this->input->post('codigo'),200,$this->data["local"]);
				break;
			case "empreendimento":
				$this->load->model('lancamento');
				$imovel = $this->lancamento->consultarImovel($this->input->post('codigo'),200,$this->data["local"]);
				break;
		}
		//desenhando email
	   if($this->input->post('tipo')=="empreendimento"){
	   		$msgEmail = $this->data['empresa']."<br>";
			$msgEmail = $msgEmail." ".$this->data['tradutor']['enviarAmigo'][9].": ".$this->data['endereco']."<br>";
			$msgEmail = $msgEmail." Fone(s): ".$this->data['fones']."<br><br>";
			$msgEmail = $msgEmail."<strong> ".$this->data['tradutor']['enviarAmigo'][10]." ".$this->input->post('nome')."</strong><br><br>";
			$msgEmail = $msgEmail." ".$this->data['tradutor']['enviarAmigo'][11].": ".$imovel[0]["imo_isn"]."<br> ".$this->data['tradutor']['enviarAmigo'][3].": ".$imovel[0]["imo_nom"]."<br> ".$this->data['tradutor']['enviarAmigo'][12].": ".$imovel[0]["imo_ano_entrega"]."<br> ".$this->data['tradutor']['enviarAmigo'][13].": ".$imovel[0]["imo_des_bai"]."<br><br>"; 
			$clas = $imovel[0]["classes"];
			$caract = "";
			if($clas){
				$msgEmail = $msgEmail."<strong> ".$this->data['tradutor']['enviarAmigo'][14].":</strong> <br><br>";
				foreach($clas as $clas){ 
					$crcPai = $clas["caracs"];
					if($crcPai){
						$caract.='<strong>'.$this->data['tradutor']['enviarAmigo'][15].' '.$clas['des'].'</strong><br /><br />';
						$qtd = count($crcPai);
						$ult = $qtd - 1;
						for($i=0;$i<$qtd;$i++) {
						   if(($crcPai[$i]["car_car_tipPai"]  == 1) && ($i < $ult)) {
							  $caract = $caract."".$crcPai[$i]["car_car_desPai"]."<br>";
						   }else if(($crcPai[$i]["car_car_tipPai"]  == 1) && ($i == $ult)) {
							  $caract = $caract."".$crcPai[$i]["car_car_desPai"];
						   }else if(($crcPai[$i]["car_car_tipPai"]  == 2) && ($i < $ult)) {
							  $caract = $caract."".$crcPai[$i]["imc_qtd"]." ".$crcPai[$i]["car_car_desPai"]."<br>";
						   }else if(($crcPai[$i]["car_car_tipPai"]  == 2) && ($i == $ult)) {
							  $caract = $caract."".$crcPai[$i]["imc_qtd"]." ".$crcPai[$i]["car_car_desPai"];
						   }else if(($crcPai[$i]["car_car_tipPai"]  == 3) && ($i < $ult)) {
							  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["dca_des"]."<br>";
						   }else if(($crcPai[$i]["car_car_tipPai"]  == 3) && ($i == $ult)) {
							  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["dca_des"];
						   }else if(($crcPai[$i]["car_car_tipPai"]  == 4) && ($i < $ult)) {
							  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["imc_des"]."<br>";
						   }else if(($crcPai[$i]["car_car_tipPai"]  == 4) && ($i == $ult)) {
							  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["imc_des"];
						   }else if(($crcPai[$i]["car_car_tipPai"]  == 5) && ($i < $ult) && ($crcPai[$i]["car_car_des_uni"] == "R$")) {
							  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["car_car_des_uni"]."&nbsp;".number_format($crcPai[$i]["imc_val"],2,",",".")."<br>";
						   }else if(($crcPai[$i]["car_car_tipPai"]  == 5) && ($i == $ult) && ($crcPai[$i]["car_car_des_uni"] == "R$")) {
							  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["car_car_des_uni"]."&nbsp;".number_format($crcPai[$i]["imc_val"],2,",",".");
						   }else if(($crcPai[$i]["car_car_tipPai"]  == 5) && ($i < $ult) && ($crcPai[$i]["car_car_des_uni"] != "R$")) {
							  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["imc_val"]."&nbsp;".$crcPai[$i]["car_car_des_uni"]."<br>";
							}else if(($crcPai[$i]["car_car_tipPai"]  == 5) && ($i == $ult) && ($crcPai[$i]["car_car_des_uni"] != "R$")) {
							  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["imc_val"]."&nbsp;".$crcPai[$i]["car_car_des_uni"];
							}
						}
						$caract.="<br /><br />";
					}
				}				
			}
	   }else {
		$crcPai = $imovel[0]['caracs'];
		$msgEmail = $this->data['empresa']."<br>";
		$msgEmail = $msgEmail." ".$this->data['tradutor']['enviarAmigo'][9].": ".$this->data['endereco']."<br>";
		$msgEmail = $msgEmail." Fone(s): ".$this->data['fones']."<br><br>";
		$msgEmail = $msgEmail."<strong> ".$this->data['tradutor']['enviarAmigo'][18]." ".$this->input->post('nome')."</strong><br><br>";
		$msgEmail = $msgEmail." ".$this->data['tradutor']['enviarAmigo'][11].": ".$imovel[0]["imo_isn"]."<br> ".$this->data['tradutor']['enviarAmigo'][16].": ".$imovel[0]["tim_des"]."<br> ".$this->data['tradutor']['enviarAmigo'][9].": ".$imovel[0]["imo_des_end"]."<br> ".$this->data['tradutor']['enviarAmigo'][13].": ".$imovel[0]["imo_des_bai"]."<br> Valor: R$ ".number_format($imovel[0]["imo_val_ven"],2,",",".")."<br> ".$this->data['tradutor']['enviarAmigo'][17].": ".$imovel[0]["imo_val_are"]. " M2 <br><br>";    
		$caract = "";
		if($crcPai){
			$msgEmail = $msgEmail."<strong> ".$this->data['tradutor']['enviarAmigo'][15].":</strong> <br><br>";
			$qtd = count($crcPai);
			$ult = $qtd - 1;
			for($i=0;$i<$qtd;$i++) {
			   if(($crcPai[$i]["car_car_tipPai"]  == 1) && ($i < $ult)) {
				  $caract = $caract."".$crcPai[$i]["car_car_desPai"]."<br>";
			   }else if(($crcPai[$i]["car_car_tipPai"]  == 1) && ($i == $ult)) {
				  $caract = $caract."".$crcPai[$i]["car_car_desPai"];
			   }else if(($crcPai[$i]["car_car_tipPai"]  == 2) && ($i < $ult)) {
				  $caract = $caract."".$crcPai[$i]["imc_qtd"]." ".$crcPai[$i]["car_car_desPai"]."<br>";
			   }else if(($crcPai[$i]["car_car_tipPai"]  == 2) && ($i == $ult)) {
				  $caract = $caract."".$crcPai[$i]["imc_qtd"]." ".$crcPai[$i]["car_car_desPai"];
			   }else if(($crcPai[$i]["car_car_tipPai"]  == 3) && ($i < $ult)) {
				  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["dca_des"]."<br>";
			   }else if(($crcPai[$i]["car_car_tipPai"]  == 3) && ($i == $ult)) {
				  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["dca_des"];
			   }else if(($crcPai[$i]["car_car_tipPai"]  == 4) && ($i < $ult)) {
				  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["imc_des"]."<br>";
			   }else if(($crcPai[$i]["car_car_tipPai"]  == 4) && ($i == $ult)) {
				  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["imc_des"];
			   }else if(($crcPai[$i]["car_car_tipPai"]  == 5) && ($i < $ult) && ($crcPai[$i]["car_car_des_uni"] == "R$")) {
				  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["car_car_des_uni"]."&nbsp;".number_format($crcPai[$i]["imc_val"],2,",",".")."<br>";
			   }else if(($crcPai[$i]["car_car_tipPai"]  == 5) && ($i == $ult) && ($crcPai[$i]["car_car_des_uni"] == "R$")) {
				  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["car_car_des_uni"]."&nbsp;".number_format($crcPai[$i]["imc_val"],2,",",".");
			   }else if(($crcPai[$i]["car_car_tipPai"]  == 5) && ($i < $ult) && ($crcPai[$i]["car_car_des_uni"] != "R$")) {
				  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["imc_val"]."&nbsp;".$crcPai[$i]["car_car_des_uni"]."<br>";
			    }else if(($crcPai[$i]["car_car_tipPai"]  == 5) && ($i == $ult) && ($crcPai[$i]["car_car_des_uni"] != "R$")) {
				  $caract = $caract."".$crcPai[$i]["car_car_desPai"].": ".$crcPai[$i]["imc_val"]."&nbsp;".$crcPai[$i]["car_car_des_uni"];
				}
			}
		}
	   }//fim da sele��o avulso lancamento	
		$msgEmail = $msgEmail." ".$caract;
		
		//fim do desenho
		$this->load->plugin('phpmailer');
		$mail=new PHPMailer();
		if($this->configMail['protocol']=="smtp"){
			$mail->IsSMTP();
			$mail->SMTPAuth   = true;
			$mail->SMTPSecure = "";
			$mail->Host       = $this->configMail['smtp_host'];
			$mail->Port       = $this->configMail['smtp_port'];
			$mail->Username   = $this->configMail['smtp_user'];  
			$mail->Password   = $this->configMail['smtp_pass'];            
		}else {
			$mail->IsMail();
		}	 
		$mail->From       = $this->input->post('email');
		$mail->FromName   = $this->input->post('nome');
		$mail->Subject    = "Imovel indicado";
		$mail->Body       = utf8_encode($msgEmail);
		//$mail->AltBody    = "This is the body when user views in plain text format"; 
		$mail->WordWrap   = 50;	 
		$mail->AddAddress($this->input->post('emailAmigo'));
		$mail->IsHTML(true);
		if($mail->send()){
			$v=1;
			switch($_SESSION['idioma']){
				case 2:
					$alerta="E-mail send successfully.";
					break;
				case 3:
					$alerta="correo electr�nico edo �xito.";
					break;
				default:
					$alerta="E-mail enviado com sucesso.";
			}
		}else {
			$v=0;
			switch($_SESSION['idioma']){
				case 2:
					$alerta="Send error, please try outher hour.";
					break;
				case 3:
					$alerta="Error en el env�o, por favor intente en unos minutos.";
					break;
				default:
					$alerta="Erro no envio, favor tentar em alguns minutos.";
			}			
		}
		header("Content-Type:application/xml; charset=UTF-8");
		print('<root><retorno><msg>'.$v.'</msg><alerta>'.utf8_encode($alerta).'</alerta></retorno></root>');
		die;

	}
	
	function buscaimovel(){

			
			
print_r($_SESSION['dados']);
			die;
			
			
	   if(isset($_POST["opcoes"])){
			$parametros = $_POST;
			$_SESSION['parametros'] = $_POST;
			if(!isset($parametros['bairro'])){
				$parametros['bairro'] = '';
			}
			switch($parametros['opcoes']){
				case 1:
					$this->load->model('imovelloc');
					$parametros['codigo']>0?$this->data["imoveis"]=$this->imovelloc->consultarImovel($parametros['codigo'],200,$this->data["local"]):$this->data["imoveis"] = $this->imovelloc->consultarImoveis($parametros['bairro'],$parametros['tipo'],$parametros['valorini'],$parametros['valorfim'],$parametros['cidade'],200,$this->data["local"]);
					$timnom = $this->imovelloc->buscaTipo($parametros['tipo']);
					$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
					break;
				case 2:
					$this->load->model('imovelvenda');
					$parametros['codigo']>0?$this->data["imoveis"]=$this->imovelvenda->consultarImovel($parametros['codigo'],200,$this->data["local"]):$this->data["imoveis"] = $this->imovelvenda->consultarImoveis($parametros['bairro'],$parametros['tipo'],$parametros['valorini'],$parametros['valorfim'],$parametros['cidade'],200,$this->data["local"]);
					$timnom = $this->imovelvenda->buscaTipo($parametros['tipo']);		
					$this->load->model('lancamento');
					$tipos = $this->lancamento->buscarEmpTipo($parametros['tipo']);
					$parametros['codigo']>0?$lancs=$this->lancamento->consultarImovel($parametros['codigo'],200,$this->data["local"]):$lancs = $this->lancamento->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],"","","","",$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);
					if(is_array($this->data["imoveis"]) && is_array($lancs)){				
						$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
					}else if(is_array($lancs)){
						$this->data["imoveis"] = $lancs;
					}
					$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
					for($i=0;$i<($tam-1);$i++){
					   for($j=$i+1;$j<$tam;$j++){
						   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
								$aux=$this->data["imoveis"][$i];
								$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
								$this->data["imoveis"][$j]=$aux;
						   }
					   
					   }
					
					}
					break;
			}
			$_SESSION[''.$this->data['sessao']] = $this->data["imoveis"];
			//cadastra a busca para estat�sticas				
			$this->tab_acs->cadastraBusc(session_id(),$parametros['bairro'],$timnom,$parametros['valorini'],$parametros['valorfim'],$parametros['cidade'],$parametros['opcoes']);
	    }else {
		   
			$this->data["imoveis"] =  $_SESSION[''.$this->data['sessao']];

			$parametros =  $_SESSION['parametros'];
		}
		if($this->data["imoveis"]){
			$this->data['atual'] = (!$this->uri->segment("3")) ? 1 : $this->uri->segment("3");
			$this->data['quant'] = $parametros["numresult"];		
			is_array($this->data["imoveis"])?$this->data["total"]=count($this->data["imoveis"]):$this->data["total"]=0;
			$this->data['pags'] = ceil($this->data["total"]/$this->data['quant']);
			$this->data['aux']=($this->data['atual']-1)*$this->data['quant'];
			$this->data['pagst'] = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
			if($this->data['atual']>=$this->data['pagst']+10){
				$this->data['pagst']+=10;
			}else if(($this->data['atual']==$this->data['pagst'] || $this->data['atual']==$this->data['pagst']-1) && $this->data['pagst']!=0){
				$this->data['pagst']-=10;
			}		
			if($this->data['pagst']>=10){
				$this->data['x']=0;
			}else {
				$this->data['x']=1;
			}
			($this->data['pagst']-1)>0?$this->data['limite'] = $this->data['pags']-$this->data['pagst']:$this->data["limite"] = $this->data['pags'];
			$this->data["limite"]<10?$this->data["limite"]=$this->data["limite"]:$this->data["limite"]=10;
			//calculo do �ltimo registro
			$this->data['ultimoreg'] = (ceil($this->data['pags']/10)-1)*10;
			$this->data['tip'] = $parametros['opcoes'];
			$this->data['gopag'] = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$this->data['avulsos'] = explode("-",$this->data['gopag']);
			$this->data['gopag2'] = (!$this->uri->segment("6")) ? 0 : $this->uri->segment("6");
			$this->data['avulsos2'] = explode("-",$this->data['gopag2']);
			$_SESSION['origem'] = $this->data['local'].'index.php/imoveis/buscaimovel/'.$this->data['atual']."/".$this->data['pagst']."/".$this->data['gopag']."/".$this->data['gopag2'];
			$this->data['origem'] = $_SESSION['origem'];
		}else {
			$this->data['gopag'] = 0;
			$this->data['gopag2'] = 0;
			$this->data['pags'] = 0;
			$this->data['atual'] = 0;
			$this->data['quant'] = 0;
			$this->data['tip'] = 1;
		}
		array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->view('imovel/exibe_imovel',$this->data);
	}
	
	function buscaRapida(){
		
	   if($this->uri->segment("9")){
	      $parametros['opcoes'] = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
		  $parametros['codigo']=0;
	   	  $parametros['cidade'] = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
	   	  $valores = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
		  $parametros['tipo'] = (!$this->uri->segment("6")) ? 0 : $this->uri->segment("6");
		  $parametros['timnom'] = (!$this->uri->segment("7")) ? "" : $this->uri->segment("7");
		  $parametros['bairro'] = (!$this->uri->segment("8")) ? 0 : $this->uri->segment("8");
		  $titl = (!$this->uri->segment("9")) ? 0 : $this->uri->segment("9");
		  $this->data['atual'] = (!$this->uri->segment("10")) ? 1 : $this->uri->segment("10");
		  $this->data['pagst'] = (!$this->uri->segment("11")) ? 0 : $this->uri->segment("11");
		  $this->data['gopag'] = (!$this->uri->segment("12")) ? 0 : $this->uri->segment("12");
		  $this->data['gopag2'] = (!$this->uri->segment("13")) ? 0 : $this->uri->segment("13");
		  $_SESSION['origem'] = $this->data['local'].'imoveis/buscaRapida/'.$parametros['opcoes']."/".$parametros['cidade']."/".$valores."/".$parametros['tipo']."/".$parametros['timnom']."/".$parametros['bairro'];
	   }else if($this->uri->segment("8")){
	      $parametros['opcoes'] = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
		  $parametros['codigo']=0;
	   	  $parametros['cidade'] = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
	   	  $valores = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
		  $parametros['tipo'] = (!$this->uri->segment("6")) ? 0 : $this->uri->segment("6");
		  $parametros['timnom'] = (!$this->uri->segment("7")) ? "" : $this->uri->segment("7");
		  $titl = (!$this->uri->segment("8")) ? 0 : $this->uri->segment("8");
		  $parametros['bairro'] = 0;
		  $this->data['atual'] = (!$this->uri->segment("9")) ? 1 : $this->uri->segment("9");
		  $this->data['pagst'] = (!$this->uri->segment("10")) ? 0 : $this->uri->segment("10");
		  $this->data['gopag'] = (!$this->uri->segment("11")) ? 0 : $this->uri->segment("11");
		  $this->data['gopag2'] = (!$this->uri->segment("12")) ? 0 : $this->uri->segment("12");
		  $_SESSION['origem'] = $this->data['local'].'imoveis/buscaRapida/'.$parametros['opcoes']."/".$parametros['cidade']."/".$valores."/".$parametros['tipo']."/".$parametros['timnom'];
	   }else if($this->uri->segment("6")){
		  $parametros['opcoes'] = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
	   	  $parametros['codigo']=0;
	   	  $parametros['cidade'] = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
	   	  $valores = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
		  $titl = (!$this->uri->segment("6")) ? 0 : $this->uri->segment("6");
		  $parametros['tipo'] = 0;
		  $parametros['timnom'] ="";
		  $parametros['bairro'] = 0;
		  $this->data['atual'] = (!$this->uri->segment("7")) ? 1 : $this->uri->segment("7");
		  $this->data['pagst'] = (!$this->uri->segment("8")) ? 0 : $this->uri->segment("8");
		  $this->data['gopag'] = (!$this->uri->segment("9")) ? 0 : $this->uri->segment("9");
		  $this->data['gopag2'] = (!$this->uri->segment("10")) ? 0 : $this->uri->segment("10");
		  $_SESSION['origem'] = $this->data['local'].'imoveis/buscaRapida/'.$parametros['opcoes']."/".$parametros['cidade']."/".$valores;
	   }else if($this->uri->segment("5")){
		  $parametros['opcoes'] = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
	   	  if($this->uri->segment("4")){
			if(is_numeric($this->uri->segment("4"))){
				$parametros['codigo']=$this->uri->segment("4");
				$parametros['cidade']=0;
			}else {
				$parametros['codigo']=0;
				$parametros['cidade']=$this->uri->segment("4");
			}
		  }else {
		  	$parametros['codigo']=0;
			$parametros['cidade']=0;
		  }
		  $titl = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
		  $valores = 0;
		  $parametros['tipo'] = 0;
		  $parametros['timnom'] ="";
		  $parametros['bairro'] = 0;
		  $this->data['atual'] = (!$this->uri->segment("6")) ? 1 : $this->uri->segment("6");
		  $this->data['pagst'] = (!$this->uri->segment("7")) ? 0 : $this->uri->segment("7");
		  $this->data['gopag'] = (!$this->uri->segment("8")) ? 0 : $this->uri->segment("8");
		  $this->data['gopag2'] = (!$this->uri->segment("9")) ? 0 : $this->uri->segment("9");
		  $_SESSION['origem'] = $this->data['local'].'imoveis/buscaRapida/'.$parametros['opcoes']."/".$this->uri->segment("4");
	   }else if($this->uri->segment("4")){
	   	  $parametros['opcoes'] = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
		  $titl = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
	   	  $parametros['codigo']=0;
		  $parametros['cidade']=0;
		  $valores = 0;
		  $parametros['tipo'] = 0;
		  $parametros['timnom'] ="";
		  $parametros['bairro'] = 0;
		  $this->data['atual'] = (!$this->uri->segment("5")) ? 1 : $this->uri->segment("5");
		  $this->data['pagst'] = (!$this->uri->segment("6")) ? 0 : $this->uri->segment("6");
		  $this->data['gopag'] = (!$this->uri->segment("7")) ? 0 : $this->uri->segment("7");
		  $this->data['gopag2'] = (!$this->uri->segment("8")) ? 0 : $this->uri->segment("8");
		  $_SESSION['origem'] = $this->data['local'].'imoveis/buscaRapida/'.$parametros['opcoes'];
	   }else {
	   	  $parametros['opcoes']=0;
		  $parametros['marcador']=0;
		  $this->data['atual']=0;
		  $this->data['pagst'] = 0;
		  $this->data['gopag'] = 0;
		  $this->data['gopag2'] = 0;
		  $_SESSION['origem'] = $this->data['local'];
		  $parametros['timnom'] ="";
		  $titl = 0;
	   }
	   if($valores){		  
		  if(strpos($valores, ' a ')>0){
			$valores2 = explode(' a ',$valores);
			$parametros['valorini'] = $valores2[0];
			$parametros['valorfim'] = $valores2[1];
		  }else if(eregi('ate ',$valores)){
			$valores2 = str_replace("ate ","",$valores);
			$parametros['valorini'] = "";
			$parametros['valorfim'] = $valores2;
		  }else if(eregi('a partir ',$valores)>0){
			$valores2 = str_replace("a partir ","",$valores);
			$parametros['valorini'] = $valores2;
			$parametros['valorfim'] = "";
		  }
	   }else {
	   	  $valores="";
		  $parametros['valorini'] = "";
		  $parametros['valorfim'] = "";
	   }
	   if($parametros['bairro']){
	   	 $parametros['bairro2'] = $parametros['bairro'];
		 $parametros['bairro'] = explode("-",$parametros['bairro']);
	   }else {
	   	 $parametros['bairro2'] ="";
	   }
	   
	   if(isset($_POST["numresult"])){ 
	   		$parametros['numresult'] = $_POST["numresult"];
			$this->data['quant'] = $_POST["numresult"];
		}else {
			$parametros['numresult'] = 5;
			$this->data['quant']=5;
		}
		$this->title = utf8_decode($parametros['cidade']." ".$parametros['opcoes']." ".$parametros['timnom']." ".$valores." ".$parametros['bairro2']);
		$this->description = utf8_decode($parametros['cidade']." ".$parametros['opcoes']." ".$parametros['timnom']." ".$valores." ".$parametros['bairro2']);
		array_push($this->keys,utf8_decode($parametros['cidade']),$parametros['opcoes'],utf8_decode($parametros['timnom']),$valores,utf8_decode($parametros['bairro2']));
	   
	   switch($parametros['opcoes']){
			case 'aluguel':
				$this->load->model('imovelloc');
				
				$parametros['codigo']>0?$this->data["imoveis"]=$this->imovelloc->consultarImovel($parametros['codigo'],200,$this->data["local"]):$this->data["imoveis"] = $this->imovelloc->consultarImoveis($parametros['bairro'],$parametros['tipo'],$parametros['valorini'],$parametros['valorfim'],$parametros['cidade'],200,$this->data["local"]);
				$timnom = $this->imovelloc->buscaTipo($parametros['tipo']);
				$this->data['tip'] = 1;
				
				break;
			case 'venda':
				$this->load->model('imovelvenda');
				$parametros['codigo']>0?$this->data["imoveis"]=$this->imovelvenda->consultarImovel($parametros['codigo'],200,$this->data["local"]):$this->data["imoveis"] = $this->imovelvenda->consultarImoveis($parametros['bairro'],$parametros['tipo'],$parametros['valorini'],$parametros['valorfim'],$parametros['cidade'],200,$this->data["local"]);
				$timnom = $this->imovelvenda->buscaTipo($parametros['tipo']);
				$this->data['tip'] = 2;	
				$this->load->model('lancamento');
				$tipos = $this->lancamento->buscarEmpTipo($parametros['tipo']);
				$parametros['codigo']>0?$lancs=$this->lancamento->consultarImovel($parametros['codigo'],200,$this->data["local"]):$lancs = $this->lancamento->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],"","","","",$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);
				if(is_array($this->data["imoveis"]) && is_array($lancs)){				
					$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
				}else if(is_array($lancs)){
					$this->data["imoveis"] = $lancs;
				}
				$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
					for($i=0;$i<($tam-1);$i++){
					   for($j=$i+1;$j<$tam;$j++){
						   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
								$aux=$this->data["imoveis"][$i];
								$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
								$this->data["imoveis"][$j]=$aux;
						   }
					   
					   }
					
					}
				break;
			case 'Avulsos':
				$this->load->model('imovelvenda');
				$parametros['codigo']>0?$this->data["imoveis"]=$this->imovelvenda->consultarImovel($parametros['codigo'],200,$this->data["local"]):$this->data["imoveis"] = $this->imovelvenda->consultarImoveis($parametros['bairro'],$parametros['tipo'],$parametros['valorini'],$parametros['valorfim'],$parametros['cidade'],200,$this->data["local"]);
				$timnom = $this->imovelvenda->buscaTipo($parametros['tipo']);
				$this->data['tip'] = 2;
				break;
			case 'Lancamentos':
				$this->load->model('lancamento');
				$this->load->model('imovelvenda');
				$tipos = $this->lancamento->buscarEmpTipo($parametros['tipo']);

				$parametros['codigo']>0?$this->data["imoveis"]=$this->lancamento->consultarImovel($parametros['codigo'],200,$this->data["local"]):$this->data["imoveis"] = $this->lancamento->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],"","","","",$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);
				$timnom = $this->imovelvenda->buscaTipo($parametros['tipo']);
				$this->data['tip'] = 2;
				break;
				
				
			case 'Construcao':
				$this->load->model('lancamento');
				$this->load->model('imovelvenda');
				$tipos = $this->lancamento->buscarEmpTipo($parametros['tipo']);
				
				
				$parametros['codigo']>0?$this->data["imoveis"]=$this->lancamento->consultarImovel($parametros['codigo'],200,$this->data["local"]):$this->data["imoveis"] = $this->lancamento->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],"","","","",$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);
				

				$timnom = $this->imovelvenda->buscaTipo($parametros['tipo']);
				$this->data['tip'] = 2;
				break;
				
				
			default:
				$this->data["imoveis"]="";
				break;
		}
		
	    $this->tab_acs->cadastraBusc(session_id(),$parametros['bairro'],$timnom,$parametros['valorini'],$parametros['valorfim'],$parametros['cidade'],$this->data['tip']);	
		$parametros['opcoes'] = $this->data['tip'];	
		$_SESSION[''.$this->data['sessao']] = $this->data["imoveis"];
		
		$_SESSION['parametros'] = $parametros;
		
		if($this->data["imoveis"]){	
			$_SESSION['origem'] = $this->data['local'].'index.php/imoveis/buscaimovel/'.$this->data['atual']."/".$this->data['pagst']."/".$this->data['gopag']."/".$this->data['gopag2'];	
			is_array($this->data["imoveis"])?$this->data["total"]=count($this->data["imoveis"]):$this->data["total"]=0;
			$this->data['pags'] = ceil($this->data["total"]/$this->data['quant']);
			$this->data['aux']=($this->data['atual']-1)*$this->data['quant'];
			if($this->data['atual']>=$this->data['pagst']+10){
				$this->data['pagst']+=10;
			}else if(($this->data['atual']==$this->data['pagst'] || $this->data['atual']==$this->data['pagst']-1) && $this->data['pagst']!=0){
				$this->data['pagst']-=10;
			}		
			if($this->data['pagst']>=10){
				$this->data['x']=0;
			}else {
				$this->data['x']=1;
			}
			($this->data['pagst']-1)>0?$this->data['limite'] = $this->data['pags']-$this->data['pagst']:$this->data["limite"] = $this->data['pags'];
			$this->data["limite"]<10?$this->data["limite"]=$this->data["limite"]:$this->data["limite"]=10;
			//calculo do �ltimo registro
			$this->data['ultimoreg'] = (ceil($this->data['pags']/10)-1)*10;
			$this->data['avulsos'] = explode("-",$this->data['gopag']);
			$this->data['avulsos2'] = explode("-",$this->data['gopag2']);
			$this->data['origem'] = $_SESSION['origem'];
		}else {
			$this->data['gopag'] = 0;
			$this->data['gopag2'] = 0;
			$this->data['pags'] = 0;
			$this->data['atual'] = 0;
			$this->data['quant'] = 0;
			$this->data['tip'] = 1;
			$_SESSION['origem'] = $this->data['local'].'index.php/imoveis/buscaimovel/'.$this->data['atual']."/".$this->data['pagst']."/".$this->data['gopag']."/".$this->data['gopag2'];
		}
		
		$_SESSION['dados'] = $this->data["imoveis"];
		
		array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		
		$this->load->view('imovel/exibe_imovel',$this->data);
	}
	
	function lista($tip,$idc,$idc2){
		$this->data['avulsos'] = array('0');
	    $this->data['avulsos2'] = array('0');
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		if($tip==1) $idc2="";
		$imoveis =  $_SESSION[''.$this->data['sessao']];
		$this->data["data"] = date("d-m-Y H:i:s");
		$this->title = "Impress�o do im�vel ";
		$this->layout = "popup";
		$this->css = array('imprimir');
		$avulso = array();
		$lanc = array();
		if(!$idc && !$idc2){
			$idc2 = "";
			$idc = "";
		}else if($idc && !$idc2){
			$idc2 = "";
			$idc = explode("-",$idc);
			@array_pop($idc);
		}else if(!$idc && $idc2){
			$idc = "";
			$idc2 = explode("-",$idc2);
			@array_pop($idc2);
		}else {
			$idc = explode("-",$idc);
			@array_pop($idc);
			$idc2 = explode("-",$idc2);
			@array_pop($idc2);
			
		}
		
		if(!is_array($idc) && !is_array($idc2)){
			if(is_array($imoveis)){
				foreach($imoveis as $imol){
					if(!empty($imol['imo_isn'])){
						array_push($avulso,$imol);
					}else if(!empty($imol['emp_isn'])){
						array_push($lanc,$imol);
					}
				}
			}
		}else if(is_array($idc) && !is_array($idc2)){
			if(is_array($imoveis)){
				foreach($imoveis as $imol){
					if(!empty($imol['imo_isn']) && is_numeric(array_search($imol['imo_isn'],$idc))){
						array_push($avulso,$imol);
					}
				}
			}
			$lanc="";
		}else if(!is_array($idc) && is_array($idc2)){
			if(is_array($imoveis)){
				foreach($imoveis as $imol){
					if(!empty($imol['emp_isn']) && is_numeric(array_search($imol['emp_isn'],$idc2))){
						array_push($lanc,$imol);
					}
				}
			}
			$avulso="";
		}else {
			if(is_array($imoveis)){
				foreach($imoveis as $imol){
					if(!empty($imol['imo_isn']) && is_numeric(array_search($imol['imo_isn'],$idc))){
						array_push($avulso,$imol);
					}else if(!empty($imol['emp_isn']) && is_numeric(array_search($imol['emp_isn'],$idc2))){
						array_push($lanc,$imol);
					}
				}
			}
			
		}
		if(@empty($avulso[0]['imo_isn'])){
			$avulso = "";
		}
		
		if(empty($lanc[0]['emp_isn'])){
			$lanc = "";
		}
		$this->data['avulso'] = $avulso;
		$this->data['lanc'] = $lanc;
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->data['tipo'] = $tip;
		$this->load->view('imovel/imprimiLista',$this->data);
	}
	
	function listaCaracteristica($tip,$idc,$idc2){
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		if($tip==1) $idc2="";
		$imoveis =  $_SESSION[''.$this->data['sessao']];
		$this->data["data"] = date("d-m-Y H:i:s");
		$this->title = "Impress�o do im�vel ";
		$this->layout = "popup";
		$this->css = array('imprimir');
		$avulso = array();
		$lanc = array();
		if(!$idc && !$idc2){
			$idc2 = "";
			$idc = "";
		}else if($idc && !$idc2){
			$idc2 = "";
			$idc = explode("-",$idc);
			@array_pop($idc);
		}else if(!$idc && $idc2){
			$idc = "";
			$idc2 = explode("-",$idc2);
			@array_pop($idc2);
		}else {
			$idc = explode("-",$idc);
			@array_pop($idc);
			$idc2 = explode("-",$idc2);
			@array_pop($idc2);
		}
		if(!is_array($idc) && !is_array($idc2)){
			if(is_array($imoveis)){
				foreach($imoveis as $imol){
					if(!empty($imol['imo_isn'])){
						array_push($avulso,$imol);
					}else if(!empty($imol['emp_isn'])){
						array_push($lanc,$imol);
					}
				}
			}
		}else if(is_array($idc) && !is_array($idc2)){
			if(is_array($imoveis)){
				foreach($imoveis as $imol){
					if(!empty($imol['imo_isn']) && is_numeric(array_search($imol['imo_isn'],$idc))){
						array_push($avulso,$imol);
					}
				}
			}
			$lanc="";
		}else if(!is_array($idc) && is_array($idc2)){
			if(is_array($imoveis)){
				foreach($imoveis as $imol){
					if(!empty($imol['emp_isn']) && is_numeric(array_search($imol['emp_isn'],$idc2))){
						array_push($lanc,$imol);
					}
				}
			}
			$avulso="";
		}else {
			if(is_array($imoveis)){
				foreach($imoveis as $imol){
					if(!empty($imol['imo_isn']) && is_numeric(array_search($imol['imo_isn'],$idc))){
						array_push($avulso,$imol);
					}else if(!empty($imol['emp_isn']) && is_numeric(array_search($imol['emp_isn'],$idc2))){
						array_push($lanc,$imol);
					}
				}
			}
			
		}
		if(@empty($avulso[0]['imo_isn'])){
			$avulso = "";
		}
		if(empty($lanc[0]['emp_isn'])){
			$lanc = "";
		}
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->data['tipo'] = $tip;		
		$this->data['avulso'] = $avulso;
		$this->data['lanc'] = $lanc;
		$this->load->view('imovel/imprimiListaCarac',$this->data);
	}
	
	function avancada(){
		$this->load->library('form_validation');
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		array_push($this->js,'global','busca');
		$this->data['submit'] = $this->data['local']."index.php/imoveis/buscaavancada";
		$this->load->view('imovel/imoveis_busca_avancada',$this->data);
	}
	
	function buscaavancada(){
	   $this->load->helper('url');	   
	   $this->data['local'] = site_url();
	   if(isset($_POST["opcoes"])){
			$parametros = $_POST;
			$_SESSION['parametros'] = $_POST;
			if(!isset($parametros['bairro'])){
				$parametros['bairro'] = '';
			}
			$cod_imoALT = array();			
			$cod_imoQTDE = array();			
			$cod_imoPOS = array();			
			$cod_imoVALOR =array();	
			$cod_imoALTLAN = "";			
			$cod_imoQTDELAN = "";			
			$cod_imoPOSLAN = "";			
			$cod_imoVALORLAN ="";			
			//filtra as caracter�sticas pai pra saber o tipo que ele procura
			$veri = array("c1"=>false,"c2"=>false,"c3"=>false,"c5"=>false);
			$pas="";
			if(is_array($parametros['carac'])){
				 //pega os tipos
				 $tipos = @array_keys($parametros["carac"]);
				 foreach($tipos as $t){
					$x=0;
					//pega os codigo da caracter�stica
					$codcars = @array_keys($parametros["carac"][$t]);
					foreach($parametros["carac"][$t] as $caracs){
						if($caracs){
							//identifica o tipo
							switch($t){
								case 1:
									//carrega os imo_isn pelo tipo
									array_push($cod_imoALT,$codcars[$x]);									
									$veri["c1"]=true;
									break;
								case 2:
									//carrega os imo_isn pelo tipo
									if($parametros['car'][$codcars[$x]]['inicio'] || $parametros['car'][$codcars[$x]]['fim']){
										$pas['id']  = $codcars[$x];
										$pas['ini'] = $parametros['car'][$codcars[$x]]['inicio'];
										$pas['fim'] = $parametros['car'][$codcars[$x]]['fim'];
										array_push($cod_imoQTDE,$pas);										
										$veri["c2"]=true;
									}
									break;
								case 3:
									array_push($cod_imoPOS,$caracs);									
									$veri["c3"]=true;
									break;
								case 5:
									if($parametros['car'][$codcars[$x]]['inicio'] || $parametros['car'][$codcars[$x]]['fim']){
										$pas['id']  = $codcars[$x];
										$pas['ini'] = $parametros['car'][$codcars[$x]]['inicio'];
										$pas['fim'] = $parametros['car'][$codcars[$x]]['fim'];
										array_push($cod_imoVALOR,$pas);										
										$veri["c5"]=true;
									}
									break;
							}
						}
						$x++;
					}
				 }				   
			 }
			 //pega os imoveis que cont�m as caracter�sticas
			 switch($parametros['opcoes']){
			 	case 1:
					$this->load->model('imovelloc');
					$imovel = new ImovelLoc();
					$imovelan = "";
					 if($veri["c1"]){
						$cod_imoALT = $this->imovelloc->carregarCodigosALT($cod_imoALT,$parametros['tipo'],$parametros['cidade']);
					 }
					 if($veri["c2"]){
						$cod_imoQTDE = $this->imovelloc->carregarCodigosQTDE($cod_imoQTDE,$parametros['tipo'],$parametros['cidade']);
					 }
					 if($veri["c3"]){
						$cod_imoPOS = $this->imovelloc->carregarCodigosPOS($cod_imoPOS,$parametros['tipo'],$parametros['cidade']);
					 }
					 if($veri["c5"]){
						$cod_imoVALOR = $this->imovelloc->carregarCodigosVALOR($cod_imoVALOR,$parametros['tipo'],$parametros['cidade']);
					 }					 
					break;
				case 2:
					$this->load->model('imovelvenda');
					$this->load->model('lancamento');
					$imovel = new imovelvenda();
					$imovelan = new lancamento();
					 if($veri["c1"]){
						$cod_imoALT = $this->imovelvenda->carregarCodigosALT($cod_imoALT,$parametros['tipo'],$parametros['cidade']);
						$cod_imoALTLAN = $this->lancamento->carregarCodigosALT($cod_imoALT,$parametros['tipo']);
					 }
					 if($veri["c2"]){
						$cod_imoQTDE = $this->imovelvenda->carregarCodigosQTDE($cod_imoQTDE,$parametros['tipo'],$parametros['cidade']);
						$cod_imoQTDELAN = $this->lancamento->carregarCodigosQTDE($cod_imoQTDE,$parametros['tipo']);
					 }
					 if($veri["c3"]){
						$cod_imoPOS = $this->imovelvenda->carregarCodigosPOS($cod_imoPOS,$parametros['tipo'],$parametros['cidade']);
						$cod_imoPOSLAN = $this->lancamento->carregarCodigosPOS($cod_imoPOS,$parametros['tipo']);
					 }
					 if($veri["c5"]){
						$cod_imoVALOR = $this->imovelvenda->carregarCodigosVALOR($cod_imoVALOR,$parametros['tipo'],$parametros['cidade']);
						$cod_imoVALORLAN = $this->lancamento->carregarCodigosVALOR($cod_imoVALOR,$parametros['tipo']);
					 }					 
					break;
				
			 }
			 //fim a captura01
			 //verificar se tem im�veis para as op��es escolhidas
			 $this->data["imoveis"]="";
			 $timnom="";
			  if(!$veri["c1"] && !$veri["c2"] && !$veri["c3"] && !$veri["c5"]){//0000
				$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
				$timnom = $imovel->buscaTipo($parametros['tipo']);
				if($parametros['opcoes']==2){
					$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
					$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);
					if(is_array($this->data["imoveis"]) && is_array($lancs)){				
						$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
					}else if(is_array($lancs)){
						$this->data["imoveis"] = $lancs;
					}
					$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
					for($i=0;$i<($tam-1);$i++){
					   for($j=$i+1;$j<$tam;$j++){
						   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
								$aux=$this->data["imoveis"][$i];
								$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
								$this->data["imoveis"][$j]=$aux;
						   }
					   
					   }
					
					}
				}
			 }else if(!$veri["c1"] && !$veri["c2"] && !$veri["c3"] && $veri["c5"]){//0001
				if($cod_imoVALOR){
				//inicio 0001
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoVALORLAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 0001
				 }	
			 }else if(!$veri["c1"] && !$veri["c2"] && $veri["c3"] && !$veri["c5"]){//0010
				if($cod_imoPOS){
					//inicio 
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoPOSLAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 
				 }	
			 }else if(!$veri["c1"] && !$veri["c2"] && $veri["c3"] && $veri["c5"]){//0011
				if($cod_imoPOS && $cod_imoVALOR){
					//inicio 
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoPOSLAN && $cod_imoVALORLAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 
				 }	
			 }else if(!$veri["c1"] && $veri["c2"] && !$veri["c3"] && !$veri["c5"]){//0100
				if($cod_imoQTDE){
					//inicio 
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoQTDELAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 
				 }	
			 }else if(!$veri["c1"] && $veri["c2"] && !$veri["c3"] && $veri["c5"]){//0101
				if($cod_imoQTDE && $cod_imoVALOR){
					//inicio 
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoQTDELAN && $cod_imoVALORLAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 
				 }	
			 }else if(!$veri["c1"] && $veri["c2"] && $veri["c3"] && !$veri["c5"]){//0110
				if($cod_imoQTDE && $cod_imoPOS){
					//inicio 
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoQTDELAN && $cod_imoPOSLAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 
				 }	
			 }else if(!$veri["c1"] && $veri["c2"] && $veri["c3"] && $veri["c5"]){//0111
				if($cod_imoQTDE && $cod_imoPOS && $cod_imoVALOR){
					//inicio 
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoQTDELAN && $cod_imoPOSLAN && $cod_imoVALORLAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 
				 }	
			 }else if($veri["c1"] && !$veri["c2"] && !$veri["c3"] && !$veri["c5"]){//1000
				if($cod_imoALT){
					//inicio 
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoALTLAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 
				 }	
			 }else if($veri["c1"] && !$veri["c2"] && !$veri["c3"] && $veri["c5"]){//1001
				if($cod_imoALT && $cod_imoVALOR){
					//inicio 
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoALTLAN && $cod_imoVALORLAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 
				 }	
			 }else if($veri["c1"] && !$veri["c2"] && $veri["c3"] && !$veri["c5"]){//1010
				if($cod_imoALT && $cod_imoPOS){
					//inicio 
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoALTLAN && $cod_imoPOSLAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 
				 }	
			 }else if($veri["c1"] && !$veri["c2"] && $veri["c3"] && $veri["c5"]){//1011
				if($cod_imoALT && $cod_imoPOS && $cod_imoVALOR){
					//inicio 
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoALTLAN && $cod_imoPOSLAN && $cod_imoVALORLAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 
				 }	
			 }else if($veri["c1"] && $veri["c2"] && !$veri["c3"] && !$veri["c5"]){//1100
				if($cod_imoALT && $cod_imoQTDE){
					//inicio 
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoALTLAN && $cod_imoQTDELAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 
				 }	
			 }else if($veri["c1"] && $veri["c2"] && !$veri["c3"] && $veri["c5"]){//1101
				if($cod_imoALT && $cod_imoQTDE && $cod_imoVALOR){
					//inicio 
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoALTLAN && $cod_imoQTDELAN && $cod_imoVALORLAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 
				 }	
			 }else if($veri["c1"] && $veri["c2"] && $veri["c3"] && !$veri["c5"]){//1110
				if($cod_imoALT && $cod_imoQTDE && $cod_imoPOS){
					//inicio 
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoALTLAN && $cod_imoQTDELAN && $cod_imoPOSLAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 
				 }	
			 }else if($veri["c1"] && $veri["c2"] && $veri["c3"] && $veri["c5"]){//1111
				if($cod_imoALT && $cod_imoQTDE && $cod_imoPOS && $cod_imoVALOR){
					//inicio 
					$this->data["imoveis"] = $imovel->carregarImoveisAVC($parametros['bairro'],$parametros['tipo'],$cod_imoVALOR,$cod_imoQTDE,$cod_imoALT,$cod_imoPOS,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],200,$this->data["local"]); 
					$timnom = $imovel->buscaTipo($parametros['tipo']);
					if($parametros['opcoes']==2 && $cod_imoALTLAN && $cod_imoQTDELAN && $cod_imoPOSLAN && $cod_imoVALORLAN){
						$tipos = $imovelan->buscarEmpTipo($parametros['tipo']);
						$lancs = $imovelan->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],$cod_imoVALORLAN,$cod_imoQTDELAN,$cod_imoALTLAN,$cod_imoPOSLAN,$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);					
						if(is_array($this->data["imoveis"]) && is_array($lancs)){				
							$lancs?$this->data["imoveis"] = array_merge($this->data["imoveis"],$lancs):$this->data["imoveis"]=$this->data["imoveis"];
						}else if(is_array($lancs)){
							$this->data["imoveis"] = $lancs;
						}
						$tam = $this->data["imoveis"]?count($this->data["imoveis"]):0;
						for($i=0;$i<($tam-1);$i++){
						   for($j=$i+1;$j<$tam;$j++){
							   if($this->data["imoveis"][$i]["imo_val_ven"]>$this->data["imoveis"][$j]["imo_val_ven"]){
									$aux=$this->data["imoveis"][$i];
									$this->data["imoveis"][$i]=$this->data["imoveis"][$j];
									$this->data["imoveis"][$j]=$aux;
							   }
						   
						   }
						
						}
					}
				//fim 
				 }	
			 }
			 //fim da captura 02
			//fim aqui
			$_SESSION[''.$this->data['sessao']] = $this->data["imoveis"];
			//cadastra a busca para estat�sticas				
			$this->tab_acs->cadastraBusc(session_id(),$parametros['bairro'],$timnom,$parametros['valorini'],$parametros['valorfim'],$parametros['cidade'],$parametros['opcoes']);
	    }else {
			$this->data["imoveis"] =  $_SESSION[''.$this->data['sessao']];
			$parametros =  $_SESSION['parametros'];
		}
		if($this->data["imoveis"]){
			$this->data['atual'] = (!$this->uri->segment("3")) ? 1 : $this->uri->segment("3");
			$this->data['quant'] = $parametros["numresult"];		
			is_array($this->data["imoveis"])?$this->data["total"]=count($this->data["imoveis"]):$this->data["total"]=0;
			$this->data['pags'] = ceil($this->data["total"]/$this->data['quant']);
			$this->data['aux']=($this->data['atual']-1)*$this->data['quant'];
			$this->data['pagst'] = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
			if($this->data['atual']>=$this->data['pagst']+10){
				$this->data['pagst']+=10;
			}else if(($this->data['atual']==$this->data['pagst'] || $this->data['atual']==$this->data['pagst']-1) && $this->data['pagst']!=0){
				$this->data['pagst']-=10;
			}		
			if($this->data['pagst']>=10){
				$this->data['x']=0;
			}else {
				$this->data['x']=1;
			}
			($this->data['pagst']-1)>0?$this->data['limite'] = $this->data['pags']-$this->data['pagst']:$this->data["limite"] = $this->data['pags'];
			$this->data["limite"]<10?$this->data["limite"]=$this->data["limite"]:$this->data["limite"]=10;
			if($this->data['pags']<=10){
			  $this->data['ultimoreg'] = 0;
			}else if($this->data['pags']>99){
			   $this->data['ultimoreg'] = $this->data['pags']-10;	
			}else {
			  $this->data['ultimoreg'] = substr($this->data['pags']-1,0,1).'0';  
			}
			$this->data['tip'] = $parametros['opcoes'];
			$this->data['gopag'] = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$this->data['avulsos'] = explode("-",$this->data['gopag']);
			$this->data['gopag2'] = (!$this->uri->segment("6")) ? 0 : $this->uri->segment("6");
			$this->data['avulsos2'] = explode("-",$this->data['gopag2']);
			$_SESSION['origem'] = $this->data['local'].'index.php/imoveis/buscaimovel/'.$this->data['atual']."/".$this->data['pagst']."/".$this->data['gopag']."/".$this->data['gopag2'];
			$this->data['origem'] = $_SESSION['origem'];
		}else {
			$this->data['gopag'] = 0;
			$this->data['gopag2'] = 0;
			$this->data['pags'] = 0;
			$this->data['atual'] = 0;
			$this->data['quant'] = 0;
			$this->data['tip'] = 1;
		}
		array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->view('imovel/exibe_imovel',$this->data);
	}
	
	function contrato($codigo){
		$this->load->helper('url');	   
		$this->title = "Roteiro de loca��o";
		$this->layout = "popup";
		$this->css = array('imprimir');
	    $this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->model('imovelloc');
		$this->data['imovel'] = $this->imovelloc->consultarImovel($codigo,200,$this->data["local"]);
		$this->load->view('imovel/roteiro_contr_loc',$this->data);
	}
	
	function formulariocontrato(){
		$this->load->helper('url');	 
		$this->load->library('form_validation'); 
		$this->title = "Roteiro de loca��o";
		$this->layout = "popup";
		$this->css = array('imprimir'); 
	    $this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->load->model('imovelloc');
		$this->data['imovel'] = $this->imovelloc->consultarImovel($this->input->post('codigo'),200,$this->data["local"]);
		if($this->input->post('confirm')==1){
			$this->data['pre']=$_POST;
		}else {
			$this->data['pre']="";
		}
		array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->input->post('pf')==1?$this->load->view('imovel/propostaFormInq',$this->data):$this->load->view('imovel/propostaFormInqPjr',$this->data);
	}
	
	function confirmInq(){
		$this->load->helper('url');	 
		$this->load->library('form_validation'); 
		$this->title = "Roteiro de loca��o";
		$this->layout = "popup";
		$this->css = array('imprimir'); 
	    $this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->load->model('imovelloc');
		if(isset($_POST['codImo'])){
			$this->data['imovel'] = $this->imovelloc->consultarImovel($this->input->post('codImo'),200,$this->data["local"]);
			$this->data['pre'] = $_POST;
			$this->data['indices'] = array_keys($_POST);
			switch($this->data['pre']['estadoCivilInq']){
				case 2:
					$this->data['ecv'] = 'Casado';
					break;
				case 3:
					$this->data['ecv'] = 'Divorciado';
					break;
				case 4:
					$this->data['ecv'] = 'Separado';
					break;
				case 5:
					$this->data['ecv'] = 'Vi�vo';
					break;
				default:
					$this->data['ecv'] = 'Solteiro';
					break;
			}
			switch($this->data['pre']['pgsimInq']){
				case 1;
					$this->data['pga'] = 'Sim';
					break;
				default:
					$this->data['pga'] = 'N�o';
					break;
			}
			switch($this->data['pre']['sexoInq']){
				case 1:
					$this->data['sexo'] = 'Masculino';
					break;
				default:
					$this->data['sexo'] = 'Feminino';
					break;
			}
			switch($this->data['pre']['rendimentoSimInq']){
				case 1:
					$this->data['rendimento'] = 'Sim';
					break;
				default:
					$this->data['rendimento'] = 'N�o';
					break;
			}
			switch($this->data['pre']['conjTrabSimInq']){
				case 1:
					$this->data['trab'] = 'Sim';
					break;
				default:
				    $this->data['trab'] = 'N�o';
					break;
			}
		}
		array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->view('imovel/confirm_propostaInq',$this->data);
	}
	
	function confirmInqPj(){
		$this->load->helper('url');	 
		$this->load->library('form_validation'); 
		$this->title = "Roteiro de loca��o Pessoa jur�dica";
		$this->layout = "popup";
		$this->css = array('imprimir'); 
	    $this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->load->model('imovelloc');
		if(isset($_POST['codigo'])){
			$this->data['imovel'] = $this->imovelloc->consultarImovel($this->input->post('codigo'),200,$this->data["local"]);
			$this->data['pre'] = $_POST;
			$this->data['indices'] = array_keys($_POST);
			switch($this->data['pre']['estadocivilSoc']){
				case 2:
					$this->data['ecv'] = 'Casado';
					break;
				case 3:
					$this->data['ecv'] = 'Divorciado';
					break;
				case 4:
					$this->data['ecv'] = 'Separado';
					break;
				case 5:
					$this->data['ecv'] = 'Vi�vo';
					break;
				default:
					$this->data['ecv'] = 'Solteiro';
					break;
			}
			switch($this->data['pre']['pgsim']){
				case 1;
					$this->data['paga'] = 'Sim';
					break;
				default:
					$this->data['paga'] = 'N�o';
					break;
			}
			switch($this->data['pre']['outRendSim']){
				case 1:
					$this->data['renda'] = 'Sim';
					break;
				default:
					$this->data['renda'] = 'N�o';
					break;
			}
			switch($this->data['pre']["tipoImovel"]){
				case 2:
					$this->data['tipo'] = "Alugada";
					break;
				case 3:
					$this->data['tipo'] = "Financiada";
					break;
				case 4:
					$this->data['tipo'] = "Com Pais";
					break;
				case 5:
					$this->data['tipo'] = "Com Parentes";
					break;
				case 6:
					$this->data['tipo'] = "Outros";
					break;
				default:
					$this->data['tipo'] = "Alugada";
					break;
			}
		}
		array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->view('imovel/confirm_propostaInqPjr',$this->data);
	}
	
	function cadastraInq(){
		$this->load->model('imovel');
		if(is_numeric($_POST["codImo"])){
			$pre = $_POST;
			($_POST["fi"]==1)?$pf = $this->imovel->cadastrarFiador($pre):$pf = $this->imovel->cadastrarPretendentePF($pre);
			if($pf>0){
				switch($pre['estadoCivilInq']){
					case 1:
						$estcon = "Solteiro";
						break;
					case 2:
						$estcon = "Casado";
						break;
					case 3:
						$estcon = "Divorciado";
						break;
					case 4:
						$estcon = "Separado";
						break;
					default:
						$estcon = "Vi�vo";
				 }
				 switch($pre['tipoImovel']){
					case 1:
						$resi = "Alugada";
						break;
					case 2:
						$resi = "Pr�pria";
						break;
					case 3:
						$resi = "Financiada";
						break;
					case 4:
						$resi = "Com Pais";
						break;
					case 5:
						$resi = "Com Parentes";
						break;
					default:
						$resi = "Outros";
				 }
				 switch($pre['sexoInq']){
					case 1:
						$sex = "Maculino";
						break;
					default:
						$sex = "Feminino";
				 }
				 $pre['pgsimInq']==1?$alugue = "Sim":$alugue = "N�o";
				 $pre['rendimentoSimInq']==1?$renda = "Sim":$renda = "N�o";
				 $pre['conjTrabSimInq']==1?$trab = "Sim":$trab = "N�o";
				 $pre['conta1']==1?$refe = "Conta Corrente":$refe = "Poupan�a";
				 $pre['conta2']==1?$contas = "Conta Corrente":$contas = "Poupan�a";
				 $campo = "";
				 if($_POST["fi"]==1){
					$cli = $this->imovel->buscarProposta($pre["ppc"]);
					$st = 3;
					$campo = '<tr>
						   <td colspan="6"> <b>Fiador do Cliente:  '.$cli["nome"].'</b>            
						   </td>
						 </tr>';	
				 }
				 ($_POST["fi"]==1)?$fiador='<span style="font-family: Verdana, Arial, Helvetica, sans-serif;font-size:9px;">CADASTRO FIADOR </span>':$fiador="CADASTRO DE PRETENDENTE";
				 ($_POST["fi"]==1)?$nom="Fiador":$nom="Candidato";
				 $email = '<table width="99%"  border="1" align="center" cellpadding="0" cellspacing="0" bordercolorlight="#999999">  <tr>    <td width="30%">	   <div align="center"><img src="'.$this->data['url'].'images/logo.jpg" width="90" height="49">      </div></td>    <td width="40%"><div align="center"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 12px;	font-weight: bold;">PROPOSTA PARA LOCA&Ccedil;&Atilde;O</span><br>        <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 12px;	font-weight: bold;">PESSOA F&Iacute;SICA</span>      <br>	    <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">'.$fiador.' </span> </div>	</td>	<td width="30%">	<table width="100%"  border="0">	  <tr>		<td height="26">		   <span style="font-size: xx-small; font-family: Verdana, Arial, Helvetica, sans-serif;"><strong>Tipo de Im&oacute;vel:</strong>	      '.$pre["tipoImo"].'</span>		</td>	  </tr>	  <tr>		<td>		   <span style="font-size: xx-small; font-family: Verdana, Arial, Helvetica, sans-serif;"><strong>Aluguel:</strong> R$	      &nbsp;'.number_format($pre["valorImo"],2,",",".").'</span>		</td>	  </tr>	</table>	</td>  </tr></table><br><table width="96%"  border="0" align="center" cellpadding="0" cellspacing="0" bordercolorlight="#999999">  <tr>    <td>	<fieldset>	   <div align="center">	     <legend title="APOIO"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 10px;	font-weight: bold;	color: #000000;">DADOS PESSOAIS </span></legend>        </div>	   <table width="100%"  border="1" cellpadding="0" cellspacing="0" bordercolorlight="#999999">         '.$campo.'		 <tr>           <td colspan="6"> <span style="font-size: xx-small; font-family: Verdana, Arial, Helvetica, sans-serif;">&nbsp;<strong>Imovel Pretendido:</strong> &nbsp;                 '.$pre["endImo"].'&nbsp;-&nbsp;<strong>Bairro:</strong>&nbsp;      '.$pre["bairroImo"].'             </span>                          </td>         </tr>         <tr>           <td width="12%" style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;" valign="middle"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;&nbsp;C&oacute;digo:</span>		      '.$pre["codImo"].'</td>           <td colspan="4" valign="middle"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;&nbsp;Nome do '.$nom.': </span> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: xx-small;	font-style: italic;">&nbsp;                 '.$pre['nomeInq'].'           </span> </td>           <td width="25%"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;&nbsp;Estado Civil &nbsp;                 '.$estcon.'                             </span></td>         </tr>         <tr>           <td height="4" width="12%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;CPF:&nbsp;                 '.$pre['cpfInq'].'           </span> </td>           <td colspan="4"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;&nbsp;Logradouro:</span> &nbsp;               '.$pre['ruaInq'].'  			   &nbsp;<span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;N�:</span>&nbsp;  			   '.$pre['numResInq'].'          </td>           <td> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Comp: &nbsp;                '.$pre['compEndInq'].'           </span> </td>         </tr>         <tr>           <td colspan="3"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Bairro:</span>                 '.$pre['bairroInq'].'           </span> </td>           <td colspan="2" style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;"> <span style="font-size: 10">&nbsp;</span><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">Cidade:                 '.$pre['cidadeInq'].'           </span> </td>           <td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Telefone:&nbsp;              '.$pre['fone1Inq'].'</td>         </tr>         <tr>           <td colspan="2"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">Telefone:</span>               '.$pre['fone2Inq'].'       </td>           <td width="15%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;UF:</span>              '.$pre['ufInq'].'</td>           <td width="19%" class="style9 style15"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;CEP:</span>&nbsp;             '.$pre['cepInq'].'         </td>           <td colspan="2"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Celular:              '.$pre['celularInq'].'            </span> </td>         </tr>         <tr>           <td colspan="2"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Nacionalidade</span>:               '.$pre['nacionalidadeInq'].'          </td>           <td colspan="2"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Naturalidade:</span> &nbsp;               '.$pre['naturalidadeInq'].'        </td>           <td colspan="2"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data Nascimento: </span>               '.$pre['datanascInq'].'           </td>         </tr>         <tr>		  <td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;" colspan="2">				  <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Tipo Im&oacute;vel:</span>				  &nbsp;				  '.$resi.'					   </td>           <td colspan="2">		      <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Tempo Moradia: &nbsp;              '.$pre['tempoMoradiaAntInq'].' </span>	<span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">Meses</span>  </td>		   <td colspan="2">		      <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">RG: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$pre['rgInq'].' Org.Exp: '.$pre['exprgInq'].'</span>				</td>         </tr>		 <tr>		   <td colspan="5" style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">		      &nbsp;Pai:&nbsp;&nbsp; '.$pre['paiInq'].'<br />			  &nbsp;M�e: '.$pre['maeInq'].'	   			  </td>		   <td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">		      &nbsp;Sexo:&nbsp;		      '.$sex.'				</td>		 </tr>         <tr>           <td width="12%" align="center"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Paga Aluguel: <br \>               '.$alugue.'</span> </td>           <td colspan="4"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Ao(A) Sr(A): &nbsp;                 '.$pre['nomepropInq'].'           </span>		   </td>		   <td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">		     &nbsp;Valor:&nbsp;		     '.$pre['valorImoInq'].'		   </td>         </tr>         <tr>           <td colspan="5"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Endere&ccedil;o: &nbsp;                 '.$pre['enderecopropInq'].'           </span> </td>           <td><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Telefone:                  '.$pre['fonepropInq'].'           </span></td>         </tr>         <tr>           <td colspan="6"> &nbsp;<span style="font-size: xx-small; font-family: Verdana, Arial, Helvetica, sans-serif;">Caso Negativo Explicar a Falta de Pagamento: &nbsp;                 '.$pre['explicacaoInq'].'           </span> </td>         </tr>       </table>	</fieldset>	</td>  </tr>  <tr><td></td></tr><tr><td></td></tr>  <tr>    <td>	   <fieldset>	      <div align="center">	        <legend title="APOIO"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 10px;	font-weight: bold;	color: #000000;">DADOS PROFISSIONAIS</span></legend>        </div>          <table width="100%"  border="1" cellspacing="0" cellpadding="0" bordercolorlight="#999999">            <tr>              <td width="21%"> <span style="font-size: xx-small; font-family: Verdana, Arial, Helvetica, sans-serif;">&nbsp;Profiss&atilde;</span><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">o:&nbsp;                    '.$pre['profissaoInq'].'              </span>               </td>              <td colspan="3"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Endere&ccedil;o Trabalho: &nbsp;                    '.$pre['endTrabInq'].'					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;N�&nbsp;'.$pre['numEndTrabInq'].'              </span> </td>            </tr>			<tr>			  <td class="style9 style15" colspan="2">			     &nbsp;<span style="font-size: 10px">Bairro:</span>'.$pre['bairroTrabInq'].'			  </td>			  <td>		         &nbsp;<span style="font-size: 12px; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 10px">Cidade:</span>'.$pre['cidadeTrabInq'].'</span>			  </td>			  <td>		        <span style="font-size: 12px; font-family: Verdana, Arial, Helvetica, sans-serif;">&nbsp;<span style="font-size: 10px">CEP:</span>'.$pre['cepTrabInq'].'	          </span>			  </td>			</tr>            <tr>              <td colspan="4" style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;"> <span style="font-size: xx-small; font-family: Verdana, Arial, Helvetica, sans-serif;">&nbsp;</span>Se Titular S&oacute;cio ou Diretor dar o Nome que est&aacute; no Alvar&aacute;:&nbsp;                     <span style="font-size: xx-small; font-family: Verdana, Arial, Helvetica, sans-serif;">                                     '.$pre['nomeDirSocInq'].'                 </span> </td>            </tr>            <tr>              <td colspan="3"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Nome da Firma: &nbsp;                    '.$pre['nomeFirmaInq'].'              </span> </td>              <td width="24%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Telefone: &nbsp;                    '.$pre['foneFirmaInq'].'              </span> </td>            </tr>            <tr>              <td colspan="2"> <span style="font-size: xx-small; font-family: Verdana, Arial, Helvetica, sans-serif;">&nbsp;Fun&ccedil;&atilde;o: &nbsp;                    '.$pre['funcaoInq'].'              </span> </td>              <td width="29%"> <span style="font-size: xx-small; font-family: Verdana, Arial, Helvetica, sans-serif;">&nbsp;Tempo de Trabalho:                     '.$pre['tempoServicoInq'].'              </span> </td>              <td> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Sal&aacute;rio Atual:                    '.$pre['salarioAtualInq'].'              </span> </td>            </tr>            <tr>              <td align="center"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Outras Rendas:<br>'.$renda.'</span></td>              <td width="26%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Tipo: &nbsp;                    '.$pre['tipoRendimentoInq'].'              </span> </td>              <td> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Fonte Pag: &nbsp;                    '.$pre['fontePagadoraInq'].'              </span> </td>              <td> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Valor: &nbsp;                    '.$pre['valorRendimentoInq'].'              </span> </td>            </tr>            <tr>              <td colspan="3"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Endere&ccedil;o: &nbsp;                    '.$pre['endOutRendimentoInq'].'              </span> </td>              <td> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Telefone: &nbsp;                    '.$pre['foneOutRendimentoInq'].'              </span> </td>            </tr>        </table>	   </fieldset>	</td>  </tr>  <tr>    <td>	  <fieldset>	       <div align="center">	         <legend title="APOIO"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 10px;	font-weight: bold;	color: #000000;">DADOS DO C&Ocirc;NJUGUE </span></legend>        </div>	       <table width="100%"  border="1" cellspacing="0" cellpadding="0" bordercolorlight="#999999">			  <tr>				<td colspan="3">			       <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Nome do C&ocirc;njugue:				   &nbsp;				   '.$pre['nomeConjInq'].'				   </span>				</td>				<td width="22%">			       <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;CPF:			    &nbsp;			    '.$pre['cpfConjInq'].' 			    </span>				</td>			  </tr>			  <tr>			     <td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">				    &nbsp;Data de Nascimento:&nbsp;				    '.$pre['dtnascConjInq'].'				 </td>				 <td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;" colspan="3">				    &nbsp;RG:&nbsp;				    '.$pre['rgConjInq'].'					&nbsp;&nbsp;&nbsp;&nbsp;Org.Exp:&nbsp;'.$pre['orgexpConjInq'].'				 </td>			  </tr>			  <tr>			    <td colspan="2" style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">				   &nbsp;Naturalidade:&nbsp;				   '.$pre['natConjInq'].'				</td>				<td colspan="2" style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">				   &nbsp;Nacionalidade:&nbsp;				   '.$pre['nacConjInq'].'				</td>			  </tr>			  <tr>				<td width="28%" align="center">			       <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Trabalha:			    &nbsp;			   '.$trab.'</span><td colspan="3">				   <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Nome da Firma:				   &nbsp;				   '.$pre['nomeFirmaConjInq'].'</span>				</td>			  </tr>			  <tr>				<td colspan="4">			       <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Endere&ccedil;o Trabalho:			    &nbsp;			    '.$pre['endTrabConjInq'].'</span>				</td>			  </tr>			  <tr>				<td>			       <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Telefone:			    &nbsp;			    '.$pre['foneConjInq'].'			    </span>				</td>				<td width="22%">			       <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Celular:			    &nbsp;			    '.$pre['celularConjInq'].' 			    </span>				</td>				<td width="28%">			       <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Fun&ccedil;&atilde;o:			    &nbsp;			    '.$pre['funcaoConjInq'].'			    </span>				</td>				<td>			       <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Sal&aacute;rio:			    &nbsp;			    '.$pre['salarioConjInq'].'			    </span>				</td>			  </tr>		</table> 	  </fieldset>	</td>  </tr>  <tr><td></td></tr>  <tr><td></td></tr>  <tr>    <td>	  <fieldset>	     <legend title="APOIO"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 10px;	font-weight: bold;	color: #000000;">REFER�NCIAS</span></legend>	     <table width="100%"  border="1" cellspacing="0" cellpadding="0" bordercolorlight="#999999">			  <tr>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;" colspan="3">&nbsp;Refer&ecirc;ncias Pessoais </td>			  </tr>			  <tr>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;" colspan="2">&nbsp;Nome:&nbsp;'.$pre['nomeRefInq'].'</td>				<td width="26%" style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Telefone:&nbsp;'.$pre['foneRefInq'].'</td>			  </tr>			  <tr>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;" colspan="2">&nbsp;Nome:&nbsp;'.$pre['nomeRef2Inq'].'</td>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Telefone:&nbsp;'.$pre['foneRef2Inq'].'</td>			  </tr>			  <tr>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;" colspan="3">&nbsp;Refer&ecirc;ncias Comerciais</td>			  </tr>			  <tr>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;" colspan="2">&nbsp;Nome:&nbsp;'.$pre['nomeRefComInq'].'</td>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Telefone:&nbsp;'.$pre['foneRefComInq'].'</td>			  </tr>			  <tr>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;" colspan="3">&nbsp;Refer&ecirc;ncias Imobili&aacute;rias </td>			  </tr>			  <tr>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;" colspan="2">&nbsp;Nome:&nbsp;'.$pre['nomeRefImobInq'].'</td>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Telefone:&nbsp;'.$pre['foneRefImobInq'].'</td>			  </tr>			  <tr>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;" colspan="3">&nbsp;Refer&ecirc;ncias Banc&aacute;rias</td>			  </tr>			  <tr>				<td width="45%" style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;" align="center">'.$refe.' '.$pre['numCcPoup1'].'</td>				<td width="29%" style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Ag&ecirc;ncias:&nbsp;'.$pre['agencia1'].'</td>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Banco:&nbsp;'.$pre['banco1'].'</td>			  </tr>			  <tr>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;" align="center">'.$contas.' '.$pre['numCcPoup2'].'</td>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Ag&ecirc;ncias:&nbsp;'.$pre['agencia2'].'</td>				<td style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Banco:&nbsp;'.$pre['banco2'].'</td>			  </tr>		</table>	  </fieldset>	</td>  </tr>  <tr>    <td>	  <fieldset>	     <legend title="APOIO"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 10px;	font-weight: bold;	color: #000000;">DADOS  PATRIMONIAIS</span></legend>		 <table width="100%"  border="0" cellspacing="0" cellpadding="0">			  <tr>				<td>				  <fieldset>				     <legend title="APOIO"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 10px;	font-weight: bold;	color: #000000;">IMOV�IS</span></legend>				     <table width="100%"  border="1" cellspacing="0" cellpadding="0" bordercolorlight="#999999">                       <tr>                         <td height="35" colspan="4"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Endere&ccedil;o: &nbsp;                               '.$pre['endImoPatrimonioInq'].'                         </span> </td>                       </tr>                       <tr>                         <td width="33%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Valor: &nbsp;                               '.$pre['valorImoPatrInq'].'                         </span> </td>                         <td width="39%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Numero Reg / Matricula: &nbsp;                               '.$pre['numregmatImoPatrInq'].'                         </span> </td>                         <td> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Zona: &nbsp;                               '.$pre['zonaImoPatrInq'].'                         </span> </td>                       </tr>                       <tr>                         <td height="35" colspan="4"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Endere&ccedil;o: &nbsp;                               '.$pre['endImoPatrimonio2Inq'].'                         </span> </td>                       </tr>                       <tr>                         <td width="33%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Valor: &nbsp;                               '.$pre['valorImoPatr2Inq'].'                         </span> </td>                         <td width="39%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Numero Reg / Matricula: &nbsp;                               '.$pre['numregmatImoPatr2Inq'].'                         </span> </td>                         <td> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Zona: &nbsp;                               '.$pre['zonaImoPatr2Inq'].'                         </span> </td>                       </tr>                     </table>				  </fieldset>				</td>			  </tr>			  <tr>				<td>				  <fieldset>				     <legend title="APOIO"><span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 10px;	font-weight: bold;	color: #000000;">VE�CULOS</span></legend>				     <table width="100%"  border="1" cellspacing="0" cellpadding="0" bordercolorlight="#999999">                       <tr>                         <td width="29%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Marca: &nbsp;                               '.$pre['marcaVeiculoInq'].'                         </span> </td>                         <td width="21%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Placa: &nbsp;                               '.$pre['placaVeiculoInq'].'                         </span> </td>                         <td width="20%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Ano: &nbsp;                              '.$pre['anoVeiculoInq'].'                         </span> </td>                         <td width="30%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Valor: &nbsp;                               '.$pre['valorVeiculoInq'].'                         </span> </td>                       </tr>                       <tr>                         <td width="29%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Marca: &nbsp;                               '.$pre['marcaVeiculo2Inq'].'                         </span> </td>                         <td width="21%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Placa: &nbsp;                               '.$pre['placaVeiculo2Inq'].'                         </span> </td>                         <td width="20%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Ano: &nbsp;                              '.$pre['anoVeiculo2Inq'].'                         </span> </td>                         <td width="30%"> <span style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;">&nbsp;Valor: &nbsp;                               '.$pre['valorVeiculo2Inq'].'                         </span> </td>                       </tr>                     </table>				  </fieldset>				</td>			  </tr>		 </table>	  </fieldset>	</td>  </tr></table><br></table>';
				$this->load->plugin('phpmailer');
				$mail=new PHPMailer();
				if($this->configMail['protocol']=="smtp"){
					$mail->IsSMTP();
					$mail->SMTPAuth   = true;
					$mail->SMTPSecure = "";
					$mail->Host       = $this->configMail['smtp_host'];
					$mail->Port       = $this->configMail['smtp_port'];
					$mail->Username   = $this->configMail['smtp_user'];  
					$mail->Password   = $this->configMail['smtp_pass'];            
				}else {
					$mail->IsMail();
				}	
				($_POST["fi"]==1)?$subject="Fiador do im�vel c�digo ".str_pad($pre['codigo'],4,"0","STR_PAD_LEFT"):$subject="Pretedente im�vel c�digo ".str_pad($pre['codigo'],4,"0","STR_PAD_LEFT");
				$destino = explode('|',$this->emails[0]);	 
				$mail->From       = $destino[1];
				$mail->FromName   = $pre['nomeInq'];
				$mail->Subject    = utf8_encode($subject);
				$mail->Body       = utf8_encode($email);
				//$mail->AltBody    = "This is the body when user views in plain text format"; 
				$mail->WordWrap   = 50;	 
				$mail->AddAddress($destino[1]);
				$mail->IsHTML(true);
				$mail->send();
			}else {
				print('<script type="text/javascript">alert("Erro no cadastro, Favor tentar em alguns minutos!"); window.close(); </script>');
			}//fim do cadastro pf
			$this->load->helper('url');
			if($_POST["fi"]==1){ 
				print('<script type="text/javascript">alert("Cadastro realizado com sucesso!\nObrigado pela prefer�ncia."); window.close(); </script>');
			}else {
				print('<script type="text/javascript">alert("Cadastro realizado com sucesso!"); window.location = "'.site_url().'index.php/imoveis/formulariofiador/'.$_POST["codImo"].'/'.$pf.'"; </script>');
			}
			 
		}else {
			print('<script type="text/javascript">alert("Imovel n�o localizado!"); window.close(); </script>');
		}		
		die;
	}
	
	function cadastraInqPj(){
		$this->load->model('imovel');
		if(is_numeric($_POST["codigo"])){
			$pre = $_POST;
			$pf = $this->imovel->cadastrarPretendentePJR($pre);
			if($pf>0){
				switch($pre['estadocivilSoc']){
					case 1:
						$estcon = "Solteiro";
						break;
					case 2:
						$estcon = "Casado";
						break;
					case 3:
						$estcon = "Divorciado";
						break;
					case 4:
						$estcon = "Separado";
						break;
					default:
						$estcon = "Vi�vo";
				 }
				 switch($pre['tipoImovel']){
					case 1:
						$resi = "Alugada";
						break;
					case 2:
						$resi = "Pr�pria";
						break;
					case 3:
						$resi = "Financiada";
						break;
					case 4:
						$resi = "Com Pais";
						break;
					case 5:
						$resi = "Com Parentes";
						break;
					default:
						$resi = "Outros";
				 }
				 $pre['pgsim']==1?$alugue = "Sim":$alugue = "N�o";
 				 $pre['outRendSim']==1?$renda = "Sim":$renda = "N�o";
 				 $pre['conta']==1?$refe = "Conta Corrente":$refe = "Poupan�a";
 				 $pre['contaEmp']==1?$contas = "Conta Corrente":$contas = "Poupan�a";
				 $email = '<table width="98%"  border="1" align="center" cellpadding="0" cellspacing="0" bordercolorlight="#999999">  <tr>    <td width="25%">	   <div align="center"><img src="'.$this->data['url'].'images/logo.jpg" width="90" height="49">        </div></td>    <td width="38%"><div align="center"><span class="style1">	     PROPOSTA PARA LOCA&Ccedil;&Atilde;O<br>	     PESSOA JUR&Iacute;DICA<br>    </span><span class="style2">CADASTRO DE PRETENDENTE</span>         </div></td>    <td width="37%">	  <table width="100%"  border="0">		  <tr>			<td width="56%"><div align="right"><span class="style1"> Tipo de Im&oacute;vel: </span>		    </div></td>			<td width="44%">   			   <span class="style4">			   '.$pre['tipoImo'].'			   			   </span></td>		  </tr>		  <tr>			<td class="style1">			   <div align="right">Aluguel: </span>			   </div></td>			<td>   			   <span class="style4">			   R$&nbsp;'.number_format($pre['valorImo'],2,",",".").'			   			   </span></td>		  </tr>    </table>   </td>  </tr></table><br><table width="100%"  border="0" align="center">  <tr>    <td>	   <fieldset>	      <legend title="APOIO"><span class="style6">DADOS DO IM�VEL</span></legend>		     <table width="100%"  border="1" cellpadding="0" cellspacing="0" bordercolorlight="#999999">				  <tr>					<td><span class="style8">				     Imovel pretendido:&nbsp;</span><span class="style2">'.$pre['endImoPretendido'].' 			          </span><span class="style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;					  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bairro:</span>&nbsp;<span class="style2">			          			          '.$pre['bairroImoPretendido'].'</span>				    </td>				  </tr>			</table>	   </fieldset>	</td>  </tr>  <tr>    <td>	   <fieldset>	      <legend title="APOIO"><span class="style6">DADOS DA EMPRESA</span></legend>		  <table width="100%"  border="1" cellpadding="0" cellspacing="0" bordercolorlight="#999999">			  <tr>				<td colspan="4"><span class="style2">&nbsp;Nome da Empresa:&nbsp;				'.$pre['nomeEmp'].'</span></td>			  </tr>			  <tr>				<td width="27%" class="style2">&nbsp;Tel.1:&nbsp;'.$pre['fone1'].'</td>				<td width="27%" class="style2">&nbsp;Tel.2:&nbsp;'.$pre['fone2'].'</td>				<td colspan="2" class="style2">&nbsp;E-mail:&nbsp;'.$pre['emailEmp'].'</td>			  </tr>			  <tr>				<td class="style2" colspan="2">&nbsp;Data da Constitui&ccedil;&atilde;o:&nbsp;&nbsp;&nbsp;&nbsp;'.$pre['dataConst'].' </td>				<td colspan="2" class="style2">&nbsp;CGC/CNPJ:&nbsp;&nbsp;&nbsp;&nbsp;'.$pre['cgccnpj'].'</td>			  </tr>			  <tr>				<td class="style2" colspan="2">&nbsp;Inscri&ccedil;&atilde;o Estadual:&nbsp;&nbsp;&nbsp;&nbsp;'.$pre['inscEst'].' </td>				<td class="style2" colspan="2">&nbsp;Registro Jucec N&ordm;:&nbsp;'.$pre['numregjucec'].' </td>			  </tr>			  <tr>				<td class="style2" colspan="2">&nbsp;Tipo de Sociedade:&nbsp;'.$pre['tiposociedade'].' </td>				<td class="style2" colspan="2">&nbsp;Setor de Atividade:&nbsp;'.$pre['setorAtiv'].'</td>			  </tr>			  <tr>			    <td colspan="4" class="style2">				   &nbsp;Endere&ccedil;o da Sede da Empresa:&nbsp;'.$pre['endEmp'].'				   &nbsp;&nbsp;&nbsp;N�:&nbsp;'.$pre['numEndEmp'].'				</td>			  </tr>			  <tr>				<td class="style2">&nbsp;Bairro:&nbsp;'.$pre['bairroEmp'].'</td>				<td class="style2">&nbsp;Cidade:&nbsp;'.$pre['cidadeEmp'].'</td>				<td width="12%" class="style2">				  &nbsp;UF:&nbsp;				  '.$pre['ufEmp'].'</option>					</td>				<td width="34%" class="style2">&nbsp;CEP:&nbsp;'.$pre['cepEmp'].'</td>			  </tr>			  <tr>				<td class="style2" colspan="4">&nbsp;Nome do S&oacute;cio Representante da Empresa:&nbsp;'.$pre['socioEmp'].' </td>			  </tr>			  <tr>				<td class="style2">&nbsp;Tel.Resid:&nbsp;'.$pre['telResid'].' </td>				<td class="style2">&nbsp;Celular:&nbsp;'.$pre['celular'].'</td>				<td class="style2" colspan="2">&nbsp;E-mail:&nbsp;'.$pre['email'].'</td>			  </tr>			  <tr>				<td class="style2">&nbsp;Nascimento:&nbsp;'.$pre['datanasc'].'</td>				<td class="style2" colspan="2">&nbsp;Identidade N&ordm;:&nbsp;'.$pre['rg'].' </td>				<td class="style2">&nbsp;Expedido por:&nbsp;'.$pre['rgExp'].' </td>			  </tr>			  <tr>				<td class="style2">&nbsp;CPF:&nbsp;'.$pre['cpfSoc'].'</td>				<td colspan="2" class="style2">&nbsp;Naturalidade:&nbsp;'.$pre['naturalidade'].'</td>				<td class="style2">&nbsp;Nacionalidade:&nbsp;'.$pre['nacionalidade'].'</td>			  </tr>			  <tr>				<td class="style2" colspan="3">&nbsp;Endere&ccedil;o Atual:&nbsp;'.$pre['endAtual'].' </td>				<td class="style2">				   &nbsp;Estado Civil:&nbsp;				  '.$estcon.'					</td>			  </tr>			  <tr>				<td class="style2">&nbsp;Bairro:&nbsp;'.$pre['bairroSoc'].'</td>				<td class="style2">&nbsp;Cidade:&nbsp;'.$pre['cidadeSoc'].'</td>				<td class="style2">				   &nbsp;UF:&nbsp;				  '.$pre['ufSoc'].'					</td>				<td class="style2">			    &nbsp;CEP:&nbsp;'.$pre['cepSoc'].'</td>			  </tr>			  <tr>				<td class="style2" colspan="2">&nbsp;Tempo de Residencia Atual:&nbsp;'.$pre['tempoResidAtual'].' </td>				<td class="style2">				  &nbsp;Resid&ecirc;ncia:&nbsp;				  '.$resi.'					</td>				<td class="style2">&nbsp;Valor:&nbsp;'.$pre['valorImoSoc'].'</td>			  </tr>         <tr>           <td width="12%" align="center">		       <span class="style9">&nbsp;&nbsp;</span><span class="style2">Paga Aluguel:</span><br>               '.$alugue.' </td>           <td colspan="3">		         <span class="style9">&nbsp;</span><span class="style2">Ao(A) Sr(A):</span>				 <span class="style9"> &nbsp;'.$pre['nomeprop'].'</span>		   </td>         </tr>         <tr>           <td colspan="3">		       <span class="style9">&nbsp;</span><span class="style2">Endere&ccedil;o:</span>			   <span class="style9"> &nbsp;'.$pre['enderecoprop'].'</span>		   </td>           <td>		      <span class="style9">&nbsp;</span><span class="style2">Telefone:</span>			  <span class="style9">'.$pre['foneprop'].'			  </span>		   </td>         </tr>         <tr>           <td colspan="6"> &nbsp;<span class="style2">Caso Negativo Explicar a Falta de Pagamento:</span><span class="style9"> &nbsp;                '.$pre['explicacao'].'           </span> </td>         </tr>			  <tr>				<td class="style2" align="center">				  &nbsp;Outros Rendimentos<br>			    '.$renda.'		</td>				<td class="style2" colspan="2">&nbsp;Fonte Pagadora:&nbsp;'.$pre['fontePag'].' </td>				<td class="style2">&nbsp;Valor:&nbsp;'.$pre['valorOutRend'].'</td>			  </tr>		</table>	   </fieldset>	</td>  </tr>  <tr>    <td>	  <fieldset>	     <legend class="style6" title="APOIO">REFER�NCIAS</legend>		 <table width="100%"  border="1" cellpadding="0" cellspacing="0" bordercolorlight="#999999">			  <tr>				<td class="style2" colspan="3">&nbsp;Refer&ecirc;ncias Pessoais do S&oacute;cio Representante </td>			  </tr>			  <tr>				<td class="style2" colspan="2">&nbsp;Nome:&nbsp;'.$pre['nomeRefPes1'].'</td>				<td width="26%" class="style2">&nbsp;Telefone:&nbsp;'.$pre['foneRefPes1'].'</td>			  </tr>			  <tr>				<td class="style2" colspan="2">&nbsp;Nome:&nbsp;'.$pre['nomeRefPes2'].'</td>				<td class="style2">&nbsp;Telefone:&nbsp;'.$pre['foneRefPes2'].'</td>			  </tr>			  <tr>				<td class="style2" colspan="3">&nbsp;Refer&ecirc;ncias Comerciais do S&oacute;cio Representante </td>			  </tr>			  <tr>				<td class="style2" colspan="2">&nbsp;Nome:&nbsp;'.$pre['nomeRefCom'].'</td>				<td class="style2">&nbsp;Telefone:&nbsp;'.$pre['foneRefCom'].'</td>			  </tr>			  <tr>				<td class="style2" colspan="3">&nbsp;Refer&ecirc;ncias Imobili&aacute;rias do S&oacute;cio Representante </td>			  </tr>			  <tr>				<td class="style2" colspan="2">&nbsp;Nome:&nbsp;'.$pre['nomeRefImob'].'</td>				<td class="style2">&nbsp;Telefone:&nbsp;'.$pre['foneRefImob'].'</td>			  </tr>			  <tr>				<td class="style2" colspan="3">&nbsp;Refer&ecirc;ncias Banc&aacute;rias do S&oacute;cio Representante </td>			  </tr>			  <tr>				<td width="45%" class="style2" align="center">'.$refe.'&nbsp;'.$pre['numCcPoup'].'</td>				<td width="29%" class="style2">&nbsp;Ag&ecirc;ncias:&nbsp;'.$pre['agencia'].'</td>				<td class="style2">&nbsp;Banco:&nbsp;'.$pre['banco'].'</td>			  </tr>			  <tr>				<td class="style2" colspan="3">&nbsp;Refer&ecirc;ncias Pessoais da Empresa </td>			  </tr>			  <tr>				<td class="style2" colspan="2">&nbsp;Nome:&nbsp;'.$pre['nomeRefEmpPes1'].'</td>				<td class="style2">&nbsp;Telefone:&nbsp;'.$pre['foneRefEmpPes1'].'</td>			  </tr>			  <tr>				<td class="style2" colspan="2">&nbsp;Nome:&nbsp;'.$pre['nomeRefEmpPes2'].'</td>				<td class="style2">&nbsp;Telefone:&nbsp;'.$pre['foneRefEmpPes2'].'</td>			  </tr>			  <tr>				<td class="style2" colspan="3">&nbsp;Refer&ecirc;ncias Comerciais da Empresa </td>			  </tr>			  <tr>				<td class="style2" colspan="2">&nbsp;Nome:&nbsp;'.$pre['nomeRefEmpCom1'].'</td>				<td class="style2">&nbsp;Telefone:&nbsp;'.$pre['foneRefEmpCom1'].'</td>			  </tr>			  <tr>				<td class="style2" colspan="2">&nbsp;Nome:&nbsp;'.$pre['nomeRefEmpCom2'].'</td>				<td class="style2">&nbsp;Telefone:&nbsp;'.$pre['foneRefEmpCom2'].'</td>			  </tr>			  <tr>				<td class="style2" colspan="3">&nbsp;Refer&ecirc;ncias Banc&aacute;rias da Empresa</td>			  </tr>			  <tr>				<td class="style2" align="center">'.$contas.'  '.$pre['contaPoupEmp'].'</td>				<td class="style2">&nbsp;Ag&ecirc;ncias:&nbsp;'.$pre['agenciaEmp'].'</td>				<td class="style2">&nbsp;Banco:&nbsp;'.$pre['bancoEmp'].'</td>			  </tr>		</table>	  </fieldset>	</td>  </tr>  <tr>    <td>	  <fieldset>	    <legend class="style6" title="APOIO">BENS</legend>		<table width="100%"  border="1" cellpadding="0" cellspacing="0" bordercolorlight="#999999">		  <tr>			<td class="style2" colspan="3"><strong>Im&oacute;vel</strong></td>		  </tr>		  <tr>			<td class="style2" colspan="2">&nbsp;Endere&ccedil;o:&nbsp;'.$pre['endBensImo'].'</td>			<td width="25%" class="style2">&nbsp;Valor:&nbsp;'.$pre['valorBensImo'].'</td>		  </tr>		  <tr>			<td width="27%" class="style2">&nbsp;Matr&iacute;cula N&ordm;&nbsp;'.$pre['matBensImo'].' </td>			<td class="style2" colspan="2">&nbsp;Zona:&nbsp;'.$pre['zonaBensImo'].'</td>		  </tr>		  <tr>			<td class="style2" colspan="3">&nbsp;<strong>Imovel</strong></td>		  </tr>		  <tr>			<td class="style2" colspan="2">&nbsp;Endere&ccedil;o:&nbsp;'.$pre['endBensImo2'].'</td>			<td class="style2">&nbsp;Valor:&nbsp;'.$pre['valorBensImo2'].'</td>		  </tr>		  <tr>			<td class="style2">&nbsp;Matr&iacute;cula N&ordm;&nbsp;'.$pre['matBensImo2'].' </td>			<td width="48%" class="style2">&nbsp;Zona:&nbsp;'.$pre['zonaBensImo2'].'</td>			<td>&nbsp;</td>		  </tr>		  <tr>			<td class="style2" colspan="3"><strong>&nbsp;Ve&iacute;culos</strong></td>		  </tr>		  <tr>			<td class="style2">&nbsp;Marca:&nbsp;'.$pre['marcaBensVei1'].'</td>			<td class="style2">			   &nbsp;Ano:&nbsp;'.$pre['anoBensVei1'].'			   &nbsp;&nbsp;Valor:&nbsp;'.$pre['valorBensVei1'].'			</td>			<td class="style2">&nbsp;Placa:&nbsp;'.$pre['placaBensVei1'].'</td>		  </tr>		  <tr>			<td class="style2">&nbsp;Marca:&nbsp;'.$pre['marcaBensVei2'].'</td>			<td class="style2">			   &nbsp;Ano:&nbsp;'.$pre['anoBensVei2'].'			   &nbsp;&nbsp;Valor:&nbsp;'.$pre['valorBensVei2'].'			</td>			<td class="style2">&nbsp;Placa:&nbsp;'.$pre['placaBensVei2'].'</td>		  </tr>		  <tr>			<td class="style2" colspan="2">&nbsp;Outros:&nbsp;'.$pre['outrosBens'].'</td>			<td class="style2">&nbsp;Valor:&nbsp;'.$pre['valorOutros'].'</td>		  </tr>		</table>	  </fieldset>	</td>  </tr></table>';
				$this->load->plugin('phpmailer');
				$mail=new PHPMailer();
				if($this->configMail['protocol']=="smtp"){
					$mail->IsSMTP();
					$mail->SMTPAuth   = true;
					$mail->SMTPSecure = "";
					$mail->Host       = $this->configMail['smtp_host'];
					$mail->Port       = $this->configMail['smtp_port'];
					$mail->Username   = $this->configMail['smtp_user'];  
					$mail->Password   = $this->configMail['smtp_pass'];            
				}else {
					$mail->IsMail();
				}	
				$destino = explode('|',$this->emails[0]);	 
				$mail->From       = $destino[1];
				$mail->FromName   = $pre['nomeEmp'];
				$mail->Subject    = utf8_encode("Pretedente im�vel c�digo ".str_pad($pre['codigo'],4,"0","STR_PAD_LEFT"));
				$mail->Body       = utf8_encode($email);
				//$mail->AltBody    = "This is the body when user views in plain text format"; 
				$mail->WordWrap   = 50;	 
				$mail->AddAddress($destino[1]);
				$mail->IsHTML(true);
				$mail->send();
			}else {
				print('<script type="text/javascript">alert("Erro no cadastro, Favor tentar em alguns minutos!"); window.close(); </script>');
			}//fim do cadastro pf
			$this->load->helper('url');
			print('<script type="text/javascript">alert("Cadastro realizado com sucesso!"); window.location = "'.site_url().'index.php/imoveis/formulariofiador/'.$_POST["codigo"].'/'.$pf.'"; </script>');
			 
		}else {
			print('<script type="text/javascript">alert("Imovel n�o localizado!"); window.close(); </script>');
		}		
		die;
	}
	 
	function formulariofiador(){
		$this->load->helper('url');	 
		$this->load->library('form_validation'); 
		$this->title = "Roteiro de loca��o";
		$this->layout = "popup";
		$this->css = array('imprimir'); 
	    $this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->data['ppc']="";
		$codigo = (!$this->uri->segment("3")) ? $_POST["codigo"] : $this->uri->segment("3");
		if(is_numeric($codigo)){
			$this->data['ppc'] = (!$this->uri->segment("4")) ? $_POST["ppc"] : $this->uri->segment("4");
			$this->load->model('imovelloc');
			$this->data['imovel'] = $this->imovelloc->consultarImovel($codigo,200,$this->data["local"]);
		}
		if($this->input->post('confirm')==1){
			$this->data['pre']=$_POST;
		}else {
			$this->data['pre']="";
		}
		array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->view('imovel/propostaFormFiador',$this->data);
	}
	
	function confirmFiador(){
		$this->load->helper('url');	 
		$this->load->library('form_validation'); 
		$this->title = "Roteiro de loca��o";
		$this->layout = "popup";
		$this->css = array('imprimir'); 
	    $this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->load->model('imovelloc');
		if(isset($_POST['codImo'])){
			$this->data['imovel'] = $this->imovelloc->consultarImovel($this->input->post('codImo'),200,$this->data["local"]);
			$this->data['pre'] = $_POST;
			$this->data['indices'] = array_keys($_POST);			
			switch($this->data['pre']['estadoCivilInq']){
				case 2:
					$this->data['ecv'] = 'Casado';
					break;
				case 3:
					$this->data['ecv'] = 'Divorciado';
					break;
				case 4:
					$this->data['ecv'] = 'Separado';
					break;
				case 5:
					$this->data['ecv'] = 'Vi�vo';
					break;
				default:
					$this->data['ecv'] = 'Solteiro';
					break;
			}
			switch($this->data['pre']['pgsimInq']){
				case 1;
					$this->data['pga'] = 'Sim';
					break;
				default:
					$this->data['pga'] = 'N�o';
					break;
			}
			switch($this->data['pre']['sexoInq']){
				case 1:
					$this->data['sexo'] = 'Masculino';
					break;
				default:
					$this->data['sexo'] = 'Feminino';
					break;
			}
			switch($this->data['pre']['rendimentoSimInq']){
				case 1:
					$this->data['rendimento'] = 'Sim';
					break;
				default:
					$this->data['rendimento'] = 'N�o';
					break;
			}
			switch($this->data['pre']['conjTrabSimInq']){
				case 1:
					$this->data['trab'] = 'Sim';
					break;
				default:
				   $this->data['trab'] = 'N�o';
					break;
			}
		}
		array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->view('imovel/confirmPropostaFiador',$this->data);
	}
}
/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>