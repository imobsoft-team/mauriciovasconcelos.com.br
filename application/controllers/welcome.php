<?php
class Welcome extends Controller {
    public $layout = 'default';
    public $title =  'Mauricio Vasconcelos';
    public $css = array('style','geraMaps');
    
    public $js = array('main','app','ajax','js','geraMaps');

    public $metatags = array (
        '<meta property="og:url" content="http://www.mauriciovasconcelos.com.br">',
        '<meta property="og:title" content="Mauricio Vasconcelos">',
        '<meta property="og:description" content="Os melhores imóveis de Fortaleza você encontra aqui.">',
        '<meta property="og:image" content="http://www.mauriciovasconcelos.com.br/application/images/logomarca.png">',
        '<meta property="og:type" content="other">'
    );
    
    public $data = array(
        'local'=>"../../../",
        'logomarca'=>'logotipo.png',
        'logoFooter' => 'logoFooter.png',
        'empresa'=>'Mauricio Vasconcelos',
        'cnpj'=>'26.214.153/0001-25',
        'telefone'=>'(88) 99208-8406',
        'telefone2' => '',
        'email' => 'contato@mauriciovasconcelos.com.br',
        'endereco'=>'Rua Coronel Diogo Gomes, 1032 - Centro - Sobral - CE, Brasil.',
        'sessaoCli'=>'EquaCli',
        'ogMetaTag' => [
            'url' => 'http://www.mauriciovasconcelos.com.br',
            'description' => 'A melhor imobiliária de Sobral',
            'image' => 'logo.jpg',
        ],
    );

    public $description = 'imóveis para venda e aluguel';
    public $keys = array();





	function Welcome()
	{
        parent::Controller();	
        
        if(!isset($_SESSION[''.$this->data['sessao']])){
			//session_register(''.$this->data['sessao']);
			$this->data['sessao'];
		}
		if(!isset($_SESSION['parametros'])){
			//session_register('parametros');
			$_SESSION['parametros']="";
		}
		if(!isset($_SESSION[''.$this->data['sessaoCli']])){
		$this->data['sessaoCli'];
			//session_register(''.$this->data['sessaoCli']);
		}
	
		if(!isset($_SESSION['origem'])){
			//session_register('origem');
			$_SESSION['origem'];
		}
		$_SESSION['origem']?$this->data['origem'] = $_SESSION['origem']:$this->data['origem'] = $this->data["local"];

		$this->load->helper('url');
		$this->data['local'] = site_url();

        //Caminho da pasta de imagens
        $this->data['imagePath'] = site_url().'application/images/';

        $this->load->model('imovelloc');
        $this->load->model('imovelvenda');

        //$this->data['destaquesEspeciais'] = $this->imovelloc->getDestaquesEspeciais(600, '0');
        $this->data['destaquesEspeciais'] = $this->imovelloc->getDestaques(600, '1');
        $this->data['destaques'] = $this->imovelloc->getDestaquesTipo(600, 0, 'residencial');
        $this->data['destaquesCom'] = $this->imovelloc->getDestaquesTipo(600, 0, 'comercial');


        @shuffle($this->data['destaquesEspeciais']);
        @shuffle($this->data['destaques']);
        @shuffle($this->data['destaquesCom']);
    }

    function index()
	{
		$this->load->view('principal/destaque',$this->data);
	}
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>