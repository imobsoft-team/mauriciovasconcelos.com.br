<?php
class Cliente extends Controller {
	public $layout = 'default';
    public $title =  'Mauricio Vasconcelos';
    public $css = array('style','geraMaps');

    public $js = array('main','app','ajax','js','geraMaps','cliente');

    public $data = array(
        'local'=>"",
        'logomarca'=>'logotipo.png',
        'logoFooter' => 'logoFooter.png',
        'empresa'=>'Mauricio Vasconcelos',
        'cnpj'=>'26.214.153/0001-25',
        'telefone'=>'(88) 99208-8406',
        'telefone2' => '',
        'email' => 'contato@mauriciovasconcelos.com.br',
        'endereco'=>'Rua Coronel Diogo Gomes, 1032 - Centro - Sobral - CE, Brasil.',
        'sessaoCli'=>'EquaCli',
        'ogMetaTag' => [
            'url' => 'http://www.mauriciovasconcelos.com.br',
            'description' => 'A melhor imobiliária de Sobral',
            'image' => 'logo.jpg',
        ],
    );

	public $configMail = array('protocol' => '0',
		'smtp_host' => '0',
		'smtp_port' =>  0,
		'smtp_user' => '0',
		'smtp_pass' => '0',
	);
    public $emails = array('Locacação|locacao@mauriciovasconcelos.com.br','Vendas|vendas@mauriciovasconcelos.com.br','Financeiro|imobiliaria@mauriciovasconcelos.com.br');
    public $email1 = 'contato@mauriciovasconcelos.com.br';
    public $email2 = 'contato@mauriciovasconcelos.com.br';
	public $description = '';
	public $keys = array();
	public $mail = '';
	
	function Cliente()
	{
		parent::Controller();
		if(!isset($_SESSION['idioma'])){
			//////session_register('idioma');
			$_SESSION['idioma'] = 1;
		}
		if(isset($_GET['idioma']) && is_numeric($_GET['idioma'])){
			switch($_GET['idioma']){
				case 2:
					$_SESSION['idioma'] = 2;
					break;
				case 3;
					$_SESSION['idioma'] = 3;
					break;
				default:
					$_SESSION['idioma'] = 1;
			}
		}
		if(!isset($_SESSION[''.$this->data['sessaoCli']])){
			
			$this->data['sessaoCli'];
		}
		$logado = $this->data['sessaoCli'];
		$this->load->model('enquete');
		$this->data['pergunta'] = $this->enquete->getAsk($logado);
		$this->data['resp']="";
		if($this->data['pergunta']){
			$this->data['resp'] = $this->enquete->getResp($this->data['pergunta']["id"]);
		}
		if(!isset($_SESSION['origem'])){
			////session_register('origem');
		}
		$_SESSION['origem']?$this->data['origem'] = $_SESSION['origem']:$this->data['origem'] = $this->data["local"];
		//conta acesso ao site
		$this->load->model('tab_acs');
		$this->tab_acs->insertAcssite(session_id());
		$this->load->helper('url');
		$this->data['local'] = site_url();
		//Caminho da pasta de imagens
		$this->data['imagePath'] = site_url().'application/images/';
		//pega a tradu��o para a p�gina
		$this->load->model('utilidades');
		$this->data['tradutor'] = $this->utilidades->Tradutor($_SESSION['idioma'],$this->data['local']);
		//pega os imoveis para obama
		$this->load->model('imovelvenda');
		$this->load->model('lancamento');
		$imoveis = $this->imovelvenda->getDestaques(500);
		$lancs   = $this->lancamento->getDestaques(500);
	}
	
	function index()
	{
		$this->load->helper('url');
		redirect('','refresh');
	}
	
	function logar(){
		$this->load->view('cliente/areaRes',$this->data);	
	}
	
	function Arearestrita(){
		$this->load->library('form_validation');
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		$this->load->model('usuario');
		if(isset($_POST["login"])){
			$this->data['usuario'] = $this->usuario->validaUsuario($this->input->post('login'),$this->input->post('senha_usu'));
			$_SESSION[''.$this->data['sessaoCli']] = $this->data['usuario'];
		}else {
			$this->data['usuario'] = $_SESSION[''.$this->data['sessaoCli']];
		}
		$this->data['dadosRec']="";
		$this->data['pcn']="";
		$this->data['pessoa']="";
		$this->data['dadosIRF']="";
		$this->data['resumoarr']="";
		if($this->data['usuario']){
			$this->load->model('recibo');
			$this->data['dadosRec'] = $this->recibo->carregarRecibos($this->data['usuario']['id']);
			$this->load->model('pc');
			$this->data['pcn'] = $this->pc->carregaD($this->data['usuario']['id']);

			if ($this->data['pcn'] == false) {
                $this->data['pcn'] = $this->pc->carregaD2($this->data['usuario']['id']);
            }

			$this->load->model('irf');
			$this->data['pessoa'] = $this->irf->carregarDadosPessoais($this->data['usuario']['id']);
			$this->data['dadosIRF'] = $this->irf->carregarDados($this->data['usuario']['id']);
			$this->load->model('resumocontrato');
			$this->data['resumoarr'] = $this->resumocontrato->carregaDados($this->data['usuario']['id']);
		}
		if($this->uri->segment("3") && $this->uri->segment("3")=="deslogar"){
			$_SESSION[''.$this->data['sessaoCli']] = "";
			header("Location: ".site_url());
		}else {			
			$this->load->view('cliente/exibepcrec',$this->data);
		}
	}
	
	function exibepc($numpc)
        {
		$this->title = "Prestação de contas";
		$this->layout = "popup";
		$this->css = array('popup');
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->data['lpc'] = "";
		$this->data['ipc'] ="";
		$this->data['pc'] = "";
		//if($_SESSION[''.$this->data['sessaoCli']])
                //{
                    $this->load->model('pc');
					
                    $this->data['pc'] = $this->pc->carregaDados($numpc);
					
					if ($this->data['pc'] == false) {
						$this->data['pc'] = $this->pc->carregaDados2($numpc);
						//echo "<pre>"; print_r($this->data['pc']); die;
					} else {
						$this->data['pc']['pc2_id'] = false;
					}
					
                    if($this->data['pc']['pc2_id'] == false) {
                        
						$this->data['lpc'] = $this->pc->carregaLancamentos($this->data['pc']["pco_id"]);
                        $this->data['ipc'] = $this->pc->carregaImoveis($this->data['pc']["pco_id"]);
						
						$this->data['data'] = date("d-m-Y H:i:s");
						$this->load->view('cliente/exibepc',$this->data);
						
                    } else if ($this->data['pc']['pc2_id'] > 0){
						
						$this->data['lpc'] = $this->pc->carregaLancamentos2($this->data['pc']["pco_id"]);
                        $this->data['ipc'] = $this->pc->carregaImoveis2($this->data['pc']["pco_id"]);
						
						$this->data['data'] = date("d-m-Y H:i:s");
						$this->load->view('cliente/exibepc2',$this->data);
						
					} else {
						echo 'Não foi encontrado o documento para o código informado'; exit();
					}
		//}
		
	}
	
	function resumocontrato($pes){
		$this->title = "Resumo do contrato";
		$this->layout = "popup";
		$this->css = array('popup');
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->data['data'] = date("d-m-Y H:i:s");
		if($_SESSION[''.$this->data['sessaoCli']]){
			$this->load->model('resumocontrato');
			$this->data['rc'] = $this->resumocontrato->carregaDados($pes);
			$this->data['usuario'] = $_SESSION[''.$this->data['sessaoCli']];
		}else {
			$this->data['rc'] ="";
		}
		$this->load->view('cliente/exiberc',$this->data);
	}
	
	function extrato($pes){
		$this->title = "Extrato de imposto de renda";
		$this->layout = "popup";
		$this->css = array('popup');
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->data['data'] = date("d-m-Y H:i:s");
		$this->data['dadosPes']="";
		$this->data['dadosIrf']="";
		$this->data['extratoImo']="";
		$this->data['lancamentos']="";
		$this->data['resumoImo']="";
		$this->data['resumoGeral']="";
		//if($_SESSION[''.$this->data['sessaoCli']]){
			$this->load->model('irf');
			$this->data['dadosPes'] = $this->irf->carregarDadosPessoais($pes);
			$this->data['dadosIrf'] = $this->irf->carregarDados($pes);
			if($this->data['dadosIrf']){
				$this->data['extratoImo'] = $this->irf->carregarImoExt($this->data['dadosIrf']["irf_isn"]);
				$this->data['resumoGeral'] = $this->irf->carregarResumoGeral($this->data['dadosIrf']["irf_isn"]);	
			}
			is_array($this->data['extratoImo'])?$quant=count($this->data['extratoImo']):$quant=0;
			for($i=0;$i<$quant;$i++) {
				$this->data['lancamentos'][$i] = $this->irf->carregarLancamento($this->data['extratoImo'][$i]["ime_isn"]);
				$this->data['resumoImo'][$i] = $this->irf->carregarResumoImo($this->data['extratoImo'][$i]["ime_isn"]); 
			}
			$this->data['usuario'] = $_SESSION[''.$this->data['sessaoCli']];
		//}
		$this->load->view('cliente/extrato',$this->data);
	}
	
	function recibo($pes,$pos,$rec)
        {
		$this->title = "Recibo de cobrançaa";
		$this->layout = "popup";
		$this->css = array('popup');
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
        $this->data['logomarca'] = $this->data['local'].'application/images/'.$this->data['logomarca'];
		$this->data['data'] = date("d-m-Y H:i:s");
		$this->data['dadosRec']="";
		$this->data['itensRec']="";
		$this->data['dadosBlt']="";
		$this->data['cod_bco']="";
		$this->data['dadosIbl']="";
		$this->data['logo_bco']="";
		$this->data['contador']="";
		$this->data['lin_dig']= "";
		$this->data['aviso']= "";
		
		// Inclui o arquivo dompdf_config localizado na pasta dompdf dentro de Plugins
        require("plugins/mpdf/mpdf.php");
		$this->data['mpdf'] = new mPDF('C');
		
		//if($_SESSION[''.$this->data['sessaoCli']])
         //{
			$this->load->model('recibo');
			$this->data['dadosRec'] = $this->recibo->carregarDados($rec);
			$this->data['itensRec'] = $this->recibo->carregarItens($rec);
			
				if(!empty($this->data['itensRec'])) {
				$this->data['dadosBlt'] = $this->recibo->carregarDadosBlt($rec);
				if($this->data['dadosBlt'])
							{
					  $this->data['cod_bco']  = substr($this->data['dadosBlt']["blt_cod_bco"],0,3);
					  $this->data['dadosIbl'] = $this->recibo->carregarDadosIbl($this->data['dadosBlt']["blt_isn"]);
					  switch($this->data['cod_bco'])
									  {
						case 409:
							$this->data['logo_bco'] = $this->data['local']."application/images/LogUnibanco.jpg";
							break;
						case 001:
							$this->data['logo_bco'] = $this->data['local']."application/images/LogBB.jpg";
							break;
						case 104:
							$this->data['logo_bco'] = $this->data['local']."application/images/LogCaixa3.jpg";
							break;
						case 004:
							$this->data['logo_bco'] = $this->data['local']."application/images/LogoBNB.jpg";
							break;
						case 237:
							$this->data['logo_bco'] = $this->data['local']."application/images/logo_bradesco.gif";
							break;
						case 356:
							$this->data['logo_bco'] = $this->data['local']."application/images/LogBancoReal.jpg";
							break;
						case 422:
							$this->data['logo_bco'] = $this->data['local']."application/images/LogoSafra.jpg";
						case 341:
							$this->data['logo_bco'] = $this->data['local']."application/images/LogItau.jpg";
							break;
					  }
				}
				$this->data['contador'] = $pos;
				@$this->data['usuario']  = $_SESSION[''.$this->data['sessaoCli']];
			
				$this->data['width'] = '100%'; 
				$this->load->view('cliente/exibirRecibo',$this->data);
			} else {
				echo 'N�o foi encontrado o documento para o codigo informado'; exit();
			}
		
		//}
			
			
	}
	
	function segundaviaboleto($rec){
		if($rec){
			$rec=base64_decode($rec);
			$this->title = "Recibo de cobran�a";
			$this->layout = "popup";
			$this->css = array('popup');
			$this->load->helper('url');	   
			$this->data['local'] = site_url();
			$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
			$this->data['data'] = date("d-m-Y H:i:s");
			$this->data['dadosRec']="";
			$this->data['itensRec']="";
			$this->data['dadosBlt']="";
			$this->data['cod_bco']="";
			$this->data['dadosIbl']="";
			$this->data['logo_bco']="";
			$this->data['contador']="";
			$this->data['lin_dig']= "";
			$this->data['aviso']= "";			
			$this->load->model('recibo');
			$this->data['dadosRec'] = $this->recibo->carregarDados2($rec);
			$this->data['itensRec'] = $this->recibo->carregarItens($rec);
			$this->data['dadosBlt'] = $this->recibo->carregarDadosBlt($rec);
			if($this->data['dadosBlt']){
				  $this->data['cod_bco']  = substr($this->data['dadosBlt']["blt_cod_bco"],0,3);
				  $this->data['dadosIbl'] = $this->recibo->carregarDadosIbl($this->data['dadosBlt']["blt_isn"]);
				  switch($this->data['cod_bco']){
					case 409:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogUnibanco.jpg";
						break;
					case 001:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogBB.jpg";
						break;
					case 104:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogCaixa3.jpg";
						break;
					case 004:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogoBNB.jpg";
						break;
					case 237:
						$this->data['logo_bco'] = $this->data['local']."application/images/logo_bradesco.gif";
						break;
					case 356:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogBancoReal.jpg";
						break;
					case 422:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogoSafra.jpg";
					case 341:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogItau.jpg";
						break;
				  }
			}
			$this->data['contador'] = 0;
			$this->data['width'] = '760';			
		}
		$this->load->view('cliente/exibirRecibo',$this->data);
	}
	
	function descadastrarnews(){
		$cliente = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
		$this->load->model('tab_email');
		$cliente = $this->tab_email->buscarEmail(base64_decode($cliente));
		$this->data['msg'] = 'Seu email não se encontra cadastrado em nosso banco de dados.';
		if($cliente){
			$this->tab_email->excluirItens($cliente["id"]);
			$this->data['msg'] = 'Seu cadastro foiexclu�do de nosso banco de dados, Se desejar voltar a receber nossas not�cias, Favor recedastre seu email em nossa Newsletter.';
		}
		$this->load->view('cliente/descNews',$this->data);
		
	}
	
}
/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>