<?php
class Ajax extends Controller {
    public $layout = 'ajax';
    public $title = 'Mota da Costa';
    public $css = '';
    public $js = '';
    public $data = array('sessaoCli'=>'EquaCli','local' => '../../../');
    public $description = '';
    public $keys = array();

    function Ajax()
    {
        parent::Controller();
        if(!isset($_SESSION[''.$this->data['sessaoCli']])){
            ////session_register(''.$this->data['sessaoCli']);
            $this->data['sessaoCli'];
        }
        $this->load->helper('url');
        $this->data['local'] = site_url();
        //pega a tradução para a página
        $this->load->model('utilidades');
        $this->data['tradutor'] = $this->utilidades->Tradutor($_SESSION['idioma'],$this->data['local']);
        $this->data['temBanner'] = 0;
        $this->data['imagePath'] = site_url().'application/images/';

       /* //Carrega o Model de Cookies e cria o Objeto para utilização dos imóveis Favoritos
        $this->data['cookie'] = $this->load->model('cookie');*/
    }

    function index()
    {
        die;
    }

    function consultarCidade(){

        $this->load->library('form_validation');
        switch($this->input->post('opcao')){
            case 1:
                $this->load->model('imovelloc');
                $cidades = "";
                $cidades = $this->imovelloc->consultarCidades();
                break;
            case 2:
                $this->load->model('imovelvenda');
                $cidades = "";
                $cidades = $this->imovelvenda->consultarCidades();
                break;
        }
        switch($_SESSION['idioma']){
            case 2:
                $html = "Select one city"."|"."0".",";
                break;
            case 3:
                $html = "Seleccione una ciudad"."|"."0".",";
                break;
            default:
                $html = "Cidade"."|"."0".",";
        }
        is_array($cidades)?$quant=count($cidades):$quant=0;
        for($i=0; $i<$quant; $i++){
            if($i < (count($cidades)-1)){
                $html.=$cidades[$i]["imo_des_loc"]."|".$cidades[$i]["imo_des_loc"].",";
            } else {
                $html.=$cidades[$i]["imo_des_loc"]."|".$cidades[$i]["imo_des_loc"];
            }
        }
        $this->data['html'] = utf8_encode($html);
        $this->load->view('principal/servidor',$this->data);


    }

    function msn(){
        $html = "";
        switch($this->input->post('msn')){
            case 1:
                $html = '<iframe src="http://settings.messenger.live.com/Conversation/IMMe.aspx?invitee=737d0db17dc0958@apps.messenger.live.com&mkt=pt-br" width="300" height="300" style="border: solid 1px black; width: 300px; height: 300px;" frameborder="0" scrolling="no"></iframe>';
                break;
            case 2:
                $html = '<iframe src="http://settings.messenger.live.com/Conversation/IMMe.aspx?invitee=5a78cd40dcb8e674@apps.messenger.live.com&mkt=pt-br" width="300" height="300" style="border: solid 1px black; width: 300px; height: 300px;" frameborder="0" scrolling="no"></iframe>';
                break;
            case 3:
                $html = '<iframe src="http://settings.messenger.live.com/Conversation/IMMe.aspx?invitee=86c13dec256c0a06@apps.messenger.live.com&mkt=pt-br" width="300" height="300" style="border: solid 1px black; width: 300px; height: 300px;" frameborder="0" scrolling="no"></iframe>';
                break;
            case 4:
                $html = '<iframe src="http://settings.messenger.live.com/Conversation/IMMe.aspx?invitee=8d5415e48c34037f@apps.messenger.live.com&mkt=pt-br" width="300" height="300" style="border: solid 1px black; width: 300px; height: 300px;" frameborder="0" scrolling="no"></iframe>';
                break;
            case 5:
                $html = '<iframe src="http://settings.messenger.live.com/Conversation/IMMe.aspx?invitee=eea72baf6a3c690b@apps.messenger.live.com&mkt=pt-br" width="300" height="300" style="border: solid 1px black; width: 300px; height: 300px;" frameborder="0" scrolling="no"></iframe>';
                break;
            case 6:
                $html = '<iframe src="http://settings.messenger.live.com/Conversation/IMMe.aspx?invitee=d40692234b418342@apps.messenger.live.com&mkt=pt-br" width="300" height="300" style="border: solid 1px black; width: 300px; height: 300px;" frameborder="0" scrolling="no"></iframe>';
                break;
            case 7:
                $html = '<iframe src="http://settings.messenger.live.com/Conversation/IMMe.aspx?invitee=c2567daf18b901e9@apps.messenger.live.com&mkt=pt-br" width="300" height="300" style="border: solid 1px black; width: 300px; height: 300px;" frameborder="0" scrolling="no"></iframe>';
                break;

        }
        $this->data['html'] = utf8_encode($html);
        $this->load->view('principal/servidor',$this->data);
    }

    function consultarTipo(){
        $this->load->library('form_validation');
        switch($this->input->post('opcao')){
            case 1:
                $this->load->model('imovelloc');
                $tipos = array();
                $tipos = $this->imovelloc->consultarTipo();
                break;
            case 2:
                $this->load->model('imovelvenda');
                $tipos = array();
                $tipos = $this->imovelvenda->consultarTipo();
                break;
        }
        switch($_SESSION['idioma']){
            case 2:
                $html = "Select"."|"."0".",";
                break;
            case 3:
                $html = "Seleccione"."|"."0".",";
                break;
            default:
                $html = "Tipo"."|"."0".",";
        }

        $conversao = array('á' => 'a','à' => 'a','ã' => 'a','â' => 'a', 'é' => 'e',
            'ê' => 'e', 'í' => 'i', 'ï'=>'i', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', "ö"=>"o",
            'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'ñ'=>'n', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
            'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ï'=>'I', "Ö"=>"O", 'Ó' => 'O',
            'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'Ç' =>'C', 'Ñ'=>'N');

        for($i=0; $i<count($tipos); $i++){
            if($i < (count($tipos)-1)){
                $tipos[$i]["tim_des"] = strtr($tipos[$i]["tim_des"], $conversao);
                $html.=$tipos[$i]["tim_des"]."|".$tipos[$i]["tim_id"].",";
            }else{
                $tipos[$i]["tim_des"] = strtr($tipos[$i]["tim_des"], $conversao);
                $html.=$tipos[$i]["tim_des"]."|".$tipos[$i]["tim_id"];
            }
        }
        $this->data['html'] = utf8_encode($html);
        $this->load->view('principal/servidor',$this->data);
    }

    function consultarBairros(){
        $this->load->library('form_validation');
        switch($this->input->post('opcao')){
            case 1:
                $this->load->model('imovelloc');
                $bairros = array();
                $bairros = $this->imovelloc->consultarBairros($this->input->post('cidade'));
                break;
            case 2:
                $this->load->model('imovelvenda');
                $bairros = array();
                $bairros = $this->imovelvenda->consultarBairros($this->input->post('cidade'));
                break;
        }
        switch($_SESSION['idioma']){
            case 2:
                $html = "ALL"."|"." ".",";
                break;
            case 3:
                $html = "TODOS"."|"." ".",";
                break;
            default:
                $html = "TODOS"."|"." ".",";
        }
        for($i=0; $i<count($bairros); $i++){
            if($i < (count($bairros)-1)){
                $html.=$bairros[$i]["imo_des_bai"]."|".$bairros[$i]["imo_des_bai"].",";
            }else{
                $html.=$bairros[$i]["imo_des_bai"]."|".$bairros[$i]["imo_des_bai"];
            }
        }
        $this->data['html'] = utf8_encode($html);
        $this->load->view('principal/servidor',$this->data);
    }

    function buscavop(){
        $this->load->library('form_validation');
        switch($this->input->post('opcao')){
            case 1:
                $this->load->model('imovelloc');
                $opcs   = array();
                $opcs  = $this->imovelloc->consultarTipo();
                $cidades = array();
                $cidades = $this->imovelloc->consultarCidades();
                break;
            case 2:
                $this->load->model('imovelvenda');
                $opcs   = array();
                $opcs  = $this->imovelvenda->consultarTipo();
                $cidades = array();
                $cidades = $this->imovelvenda->consultarCidades();
                break;
        }
        $compl="";
        if(is_array($opcs) && is_array($cidades)){
            switch($_SESSION['idioma']){
                case 2:
                    $compl='<cid>Select|0</cid>';
                    break;
                case 3:
                    $compl='<cid>Seleccione|0</cid>';
                    break;
                default:
                    $compl='<cid>Selecione|0</cid>';
            }
            foreach($cidades as $cid){
                $compl.='<cid>'.utf8_encode($cid["imo_des_loc"])."|".utf8_encode($cid["imo_des_loc"]).'</cid>';
            }
            $compl.='<op>Selecione|0</op>';
            foreach($opcs as $op){
                $compl.='<op>'.utf8_encode($op["tim_des"])."|".utf8_encode($op["tim_id"]).'</op>';
            }
            $v = 1;
            $alerta = "Carregando...";
        }else {
            $v = 0;
            $alerta = "Sem cidades e opções cadastradas.";
        }
        header("Content-Type:application/xml; charset=UTF-8");
        print('<root><retorno><msg>'.$v.'</msg><alerta>'.utf8_encode($alerta).'</alerta>'.$compl.'</retorno></root>');
        die;
    }


    function desenhaMap(){
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->data['local'] = site_url();
        header("Content-Type:text/html; charset=iso-8859-1");
        $this->load->view('imovel/maps',$this->data);
    }

    function desenhacaracs(){
        $this->load->library('form_validation');
        switch($this->input->post('opcao')){
            case 1:
                $this->load->model('imovelloc');
                $caracsAv = array();
                $caracsAv = $this->imovelloc->carregaCaracteristicasAv($this->input->post('tipo'));
                break;
            case 2:
                $this->load->model('imovelvenda');
                $caracsAv = array();
                $caracsAv = $this->imovelvenda->carregaCaracteristicasAv($this->input->post('tipo'));
                break;
        }
        $html="";
        if(is_array($caracsAv)){
            if(!empty($caracsAv[0]["car_tip"])){
                $html='<table width="250" border="0" align="center">
					  <tr>
						<td class="novo_destaq"><div align="center"><span class="busca-label">: : ';

                switch($_SESSION['idioma']){
                    case 2:
                        $html.='CHOOSE THE FEATURES OF YOUR PROPERTY';
                        break;
                    case 3:
                        $html.='Escoja las características de su propiedad';
                        break;
                    default:
                        $html.='ESCOLHA	AS CARACTER&Iacute;STICAS DO SEU IM&Oacute;VEL';
                }
                $html.=' : : </span> <br />
						</div></td>
					  </tr>
				  </table><table width="250"  border="0" align="center" cellpadding="2" cellspacing="0">';
                $qtd = count($caracsAv);
                for($i=0;$i<$qtd;$i++) {
                    $html.='<input type="hidden" name="caracteristica_desc[]" value="'.$caracsAv[$i]["car_des"].'">';
                    switch($caracsAv[$i]["car_tip"]){
                        case 1:
                            $html.='<tr><td width="102" class="busca-label">'.$caracsAv[$i]["car_des"].'</td><td class="busca-label" colspan="4">SIM<input type="checkbox" name="carac[1]['.$caracsAv[$i]["car_isn"].']" value="1"></td></tr>';
                            break;
                        case 2:
                            $html.='<tr><td width="102" class="busca-label">'.$caracsAv[$i]["car_des"].'</td><td colspan="4"><table border="0" cellpadding="2" cellspacing="0" width="47%"><tr><td width="36%"><input name="car['.$caracsAv[$i]["car_isn"].'][inicio]" type="text"  onKeyPress="javascript:  return validaNumeros(this,event)" size="12" maxlength="8"></td><td width="14%" align="center" class="busca-label" >a </td><td width="36%"><input name="car['.$caracsAv[$i]["car_isn"].'][fim]" type="text"  onKeyPress="javascript:  return validaNumeros(this,event)" size="12" maxlength="8"><input type="hidden" name="carac[2]['.$caracsAv[$i]["car_isn"].']" value="1"/></td></tr></table></td></tr>';
                            break;
                        case 3:
                            is_array($caracsAv[$i]["val_pre"])?$quant=count($caracsAv[$i]["val_pre"]):$quant = 0;
                            if($quant>0){
                                $html.='<tr><td class="busca-label">'.$caracsAv[$i]["car_des"].'</td><td><select name="carac[3]['.$caracsAv[$i]["car_isn"].']" ><option value="0">TODOS</option>';
                                for($cont=0;$cont<$quant;$cont++) {
                                    $html.='<option value="'.$caracsAv[$i]["val_pre"][$cont]["dca_isn"].'">'.$caracsAv[$i]["val_pre"][$cont]["dca_des"].'</option>';
                                }
                                $html.='</select></td></tr>';
                            }
                            break;
                        case 5:
                            $html.='<tr><td class="busca-label">'.$caracsAv[$i]["car_des"].'</td><td colspan="2"><table width="85%" height="26" border="0" align="left" cellpadding="2" cellspacing="0"><tr><td width="36%"><input name="car['.$caracsAv[$i]["car_isn"].'][inicio]" type="text"  onKeyPress="javascript:  return validaNumeros(this,event)"  size="12" maxlength="8"></td><td width="7%" align="center" class="busca-label">a </td><td width="36%"><input name="car['.$caracsAv[$i]["car_isn"].'][fim]" type="text"  onKeyPress="javascript:  return validaNumeros(this,event)"  size="12" maxlength="8"><input type="hidden" name="carac[5]['.$caracsAv[$i]["car_isn"].']" value="1" /></td></tr></table></td> <td width="114"></td></tr>';
                            break;
                    }
                }
            }
        }else {
            $html = 'SEM CARACTERÍSTICAS PARA O TIPO ESCOLHIDO<br /><input type="hidden" name="carac" value="0" />';
        }
        $this->data['html'] = utf8_encode($html);
        $this->load->view('principal/servidor',$this->data);
    }


    function callMap(){
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->data['local'] = site_url();
        //$this->data['submit'] = $this->data['local']."index.php/imoveis/buscaavancada";
        header("Content-Type:text/html; charset=iso-8859-1");
        $this->load->view('internas/mapa',$this->data);
    }


    function votoenquete(){
        $this->load->model('enquete');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->data['local'] = site_url();
        $pergunta = $this->input->post('enquete_pergunta',TRUE);
        $resposta = $this->input->post('enquete_resposta',TRUE);
        $comando  = $this->input->post('comando',TRUE);
        $st = $this->enquete->setVotar($pergunta);
        $html = '<div style="width:100%; height:57px; clear:both;"></div>
  <div style="width:190px; height:auto; clear:both; color:#CC0000; font:Arial, Helvetica, sans-serif; font-size:10px; text-align:left;margin-left:10px">';
        if(($st == 0) && ($comando == "votar")) { // VOTO PARA ENQUETE JA CADASTRADO NO COOMPUTADOR
            $html.= 'Já houve um voto nesta  enquete neste computador.<br />
			Agradecemos sua participação.';
        }else if(($comando == "votar") && ($st != 0)) {
            $this->enquete->setResult($pergunta,$resposta); // CADASTRA O VOTO DA ENQUETE
            $html.= 'Obrigado!<br>Seu voto foi confirmado!<br />';
            $exib = $this->enquete->getExib($pergunta);
            if($exib == 1) {
                $per = $this->enquete->getPerg($pergunta);
                if(!$per) {
                    $html.='Enquete não encontrada!<br />';
                }else {
                    $resp = $this->enquete->getResp($pergunta);
                    if(!$resp) {
                        $html.='Nenhuma resposta cadastrada para a enquete<br />';
                    }else {
                        $html.=substr($per,0,35).'<br /><br />';
                        $respondida = $this->enquete->getResultResp($pergunta);
                        for($i=0;$i < count($resp);$i++) {
                            $res[$i]["qtd"] = $this->enquete->getAns($resp[$i]["id"],$pergunta);
                            if($respondida != 0){
                                $percentual  =  ($res[$i]["qtd"] / $respondida) * 100;
                            }
                            if($resp[$i]["des"]) {
                                $html.=substr($resp[$i]["des"],0,35).'&nbsp;<img src ="images/legenda_cor<?php echo $i+1?>.gif" border="0" width="'.(round($percentual) * 1.4).'" height="12" align="absmiddle">&nbsp;&nbsp;'.round($percentual).' %<br /><br />';

                            }else {
                                $html.='<br />Nenhum voto cadastrado!<br />';
                            }
                        }//fim do for
                    }//fim do resp
                }//fim do per
            }
        }
        $html.='</div>';
        $this->data['html'] = utf8_encode($html);
        $this->load->view('principal/servidor',$this->data);
    }

    function avancada(){
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->data['local'] = site_url();
        $this->data['submit'] = $this->data['local']."index.php/imoveis/buscaavancada";
        header("Content-Type:text/html; charset=iso-8859-1");
        $this->load->view('imovel/imoveis_busca_avancada',$this->data);
    }



    function buscanoMapa(){
        $valBusca="";
        if($this->uri->segment("11")){
            $parametros['opcoes'] = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
            $parametros['codigo']=0;
            $parametros['cidade'] = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
            $valores = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
            $parametros['tipo'] = (!$this->uri->segment("6")) ? 0 : $this->uri->segment("6");
            $parametros['timnom'] = (!$this->uri->segment("7")) ? "" : $this->uri->segment("7");
            $parametros['bairro'] = (!$this->uri->segment("8")) ? 0 : $this->uri->segment("8");
            $valBusca = (!$this->uri->segment("9")) ? 0 : $this->uri->segment("9");
            $parametros['finalidade'] = (!$this->uri->segment("10")) ? 0 : $this->uri->segment("10");
            $titl = (!$this->uri->segment("11")) ? 0 : $this->uri->segment("11");
            $this->data['atual'] = (!$this->uri->segment("12")) ? 1 : $this->uri->segment("12");
            $this->data['pagst'] = (!$this->uri->segment("13")) ? 0 : $this->uri->segment("13");
            $this->data['gopag'] = (!$this->uri->segment("14")) ? 0 : $this->uri->segment("14");
            $this->data['gopag2'] = (!$this->uri->segment("15")) ? 0 : $this->uri->segment("15");
        }else if($this->uri->segment("10")){
            $parametros['opcoes'] = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
            $parametros['codigo']=0;
            $parametros['cidade'] = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
            $valores = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
            $parametros['tipo'] = (!$this->uri->segment("6")) ? 0 : $this->uri->segment("6");
            $parametros['timnom'] = (!$this->uri->segment("7")) ? "" : $this->uri->segment("7");
            $parametros['bairro'] = 0;
            $valBusca = (!$this->uri->segment("8")) ? 0 : $this->uri->segment("8");
            $parametros['finalidade'] = (!$this->uri->segment("9")) ? 0 : $this->uri->segment("9");
            $titl = (!$this->uri->segment("10")) ? 0 : $this->uri->segment("10");
            $this->data['atual'] = (!$this->uri->segment("11")) ? 1 : $this->uri->segment("11");
            $this->data['pagst'] = (!$this->uri->segment("12")) ? 0 : $this->uri->segment("12");
            $this->data['gopag'] = (!$this->uri->segment("13")) ? 0 : $this->uri->segment("13");
            $this->data['gopag2'] = (!$this->uri->segment("14")) ? 0 : $this->uri->segment("14");
        }else if($this->uri->segment("8")){
            $parametros['opcoes'] = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
            $parametros['codigo']=0;
            $parametros['cidade'] = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
            $valores = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
            $parametros['tipo'] = (!$this->uri->segment("6")) ? 0 : $this->uri->segment("6");
            $parametros['timnom'] = (!$this->uri->segment("7")) ? "" : $this->uri->segment("7");
            $titl = (!$this->uri->segment("8")) ? 0 : $this->uri->segment("8");
            $parametros['bairro'] = 0;
            $parametros['finalidade'] = 0;
            $this->data['atual'] = (!$this->uri->segment("9")) ? 1 : $this->uri->segment("9");
            $this->data['pagst'] = (!$this->uri->segment("10")) ? 0 : $this->uri->segment("10");
            $this->data['gopag'] = (!$this->uri->segment("11")) ? 0 : $this->uri->segment("11");
            $this->data['gopag2'] = (!$this->uri->segment("12")) ? 0 : $this->uri->segment("12");
        }else if($this->uri->segment("6")){
            $parametros['opcoes'] = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
            $parametros['codigo']=0;
            $parametros['cidade'] = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
            $valores = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
            $titl = (!$this->uri->segment("6")) ? 0 : $this->uri->segment("6");
            $parametros['tipo'] = 0;
            $parametros['timnom'] ="";
            $parametros['bairro'] = 0;
            $parametros['finalidade'] = 0;
            $this->data['atual'] = (!$this->uri->segment("7")) ? 1 : $this->uri->segment("7");
            $this->data['pagst'] = (!$this->uri->segment("8")) ? 0 : $this->uri->segment("8");
            $this->data['gopag'] = (!$this->uri->segment("9")) ? 0 : $this->uri->segment("9");
            $this->data['gopag2'] = (!$this->uri->segment("10")) ? 0 : $this->uri->segment("10");
        }else if($this->uri->segment("5")){
            $parametros['opcoes'] = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
            if($this->uri->segment("4")){
                if(is_numeric($this->uri->segment("4"))){
                    $parametros['codigo']=$this->uri->segment("4");
                    $parametros['cidade']=0;
                }else {
                    $parametros['codigo']=0;
                    $parametros['cidade']=$this->uri->segment("4");
                }
            }else {
                $parametros['codigo']=0;
                $parametros['cidade']=0;
            }
            $titl = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
            $valores = 0;
            $parametros['finalidade'] = 0;
            $parametros['tipo'] = 0;
            $parametros['timnom'] ="";
            $parametros['bairro'] = 0;
            $this->data['atual'] = (!$this->uri->segment("6")) ? 1 : $this->uri->segment("6");
            $this->data['pagst'] = (!$this->uri->segment("7")) ? 0 : $this->uri->segment("7");
            $this->data['gopag'] = (!$this->uri->segment("8")) ? 0 : $this->uri->segment("8");
            $this->data['gopag2'] = (!$this->uri->segment("9")) ? 0 : $this->uri->segment("9");
        }else if($this->uri->segment("4")){
            $parametros['opcoes'] = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
            $titl = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
            $parametros['codigo']=0;
            $parametros['cidade']=0;
            $valores = 0;
            $parametros['tipo'] = 0;
            $parametros['finalidade'] = 0;
            $parametros['timnom'] ="";
            $parametros['bairro'] = 0;
            $this->data['atual'] = (!$this->uri->segment("5")) ? 1 : $this->uri->segment("5");
            $this->data['pagst'] = (!$this->uri->segment("6")) ? 0 : $this->uri->segment("6");
            $this->data['gopag'] = (!$this->uri->segment("7")) ? 0 : $this->uri->segment("7");
            $this->data['gopag2'] = (!$this->uri->segment("8")) ? 0 : $this->uri->segment("8");
        }else {
            $parametros['opcoes']=0;
            $parametros['marcador']=0;
            $this->data['atual']=0;
            $this->data['pagst'] = 0;
            $this->data['gopag'] = 0;
            $this->data['gopag2'] = 0;
            $parametros['finalidade'] = 0;
            $_SESSION['origem'] = $this->data['local'];
            $parametros['timnom'] ="";
            $titl = 0;
        }
        if($valBusca){
            $valores2 = explode('|',$valBusca);
            if($valores2[0]==0){
                $parametros['valorini'] = "";
                if(@$valores2[1])
                    $parametros['valorfim'] = $valores2[1];
                else
                    $parametros['valorfim'] = "";
            }else if($valores2[1]==0){
                $parametros['valorini'] = $valores2[0];
                $parametros['valorfim'] = "";
            }else{
                $parametros['valorini'] = $valores2[0];
                $parametros['valorfim'] = $valores2[1];
            }
        }else {
            $valores="";
            $parametros['valorini'] = "";
            $parametros['valorfim'] = "";
        }
        if($parametros['bairro']){
            $parametros['bairro2'] = $parametros['bairro'];
            $parametros['bairro'] = explode("-",$parametros['bairro']);
        }else {
            $parametros['bairro2'] ="";
        }
        if(isset($_POST["numresult"])){
            $parametros['numresult'] = $_POST["numresult"];
            $this->data['quant'] = $_POST["numresult"];
        }else {
            $parametros['numresult'] = 5;
            $this->data['quant']=5;
        }
        switch($parametros['opcoes']){
            case 'aluguel':
                $this->load->model('imovelloc');
                $parametros['codigo']>0?$this->data["imoveis"]=$this->imovelloc->consultarImovel($parametros['codigo'],400,$this->data["local"]):$this->data["imoveis"] = $this->imovelloc->consultarImoveis($parametros['bairro'],$parametros['tipo'],$parametros['valorini'],$parametros['valorfim'],$parametros['cidade'],400,$this->data["local"],"",$parametros['finalidade']);

                $timnom = $this->imovelloc->buscaTipo($parametros['tipo']);
                $this->data['tip'] = 1;
                break;
            case 'venda':
                $this->load->model('imovelvenda');
                $parametros['codigo']>0?$this->data["imoveis"]=$this->imovelvenda->consultarImovel($parametros['codigo'],200,$this->data["local"]):$this->data["imoveis"] = $this->imovelvenda->consultarImoveis($parametros['bairro'],$parametros['tipo'],$parametros['valorini'],$parametros['valorfim'],$parametros['cidade'],200,$this->data["local"],"");
                $timnom = $this->imovelvenda->buscaTipo($parametros['tipo']);
                $this->data['tip'] = 2;
                break;
            case 'Lancamentos':
                $this->load->model('lancamento');
                $tipos = $this->lancamento->buscarEmpTipo($parametros['tipo']);
                $parametros['codigo']>0?$this->data["imoveis"]=$this->lancamento->consultarImovel($parametros['codigo'],200,$this->data["local"]):$this->data["imoveis"] = $this->lancamento->carregarLancAVC($parametros['bairro'],$tipos,$parametros['tipo'],"","","","",$parametros['cidade'],$parametros['valorini'],$parametros['valorfim'],0,"EMP_ISN","",200,$this->data["local"]);
                $this->data['tip'] = 3;
                break;
            default:
                $this->data["imoveis"]="";
                break;
        }
        $parametros['opcoes'] = $this->data['tip'];
        $html ='{"msg":"0"}';
        if($this->data["imoveis"]){
            $html ='{"msg":"1","men":[';
            foreach($this->data["imoveis"] as $imo){
                //pega variáveis de acordo com a pesquisa
                if($this->data['tip']==1 || $this->data['tip']==2){
                    $bai=$imo["imo_des_bai"];
                    $codimo=$imo["imo_isn"];
                    $endereco = explode(",",$imo["imo_des_end"]);
                    $this->data['tip']==1?$tip = "aluguel":$tip = "venda";
                    if($this->data['tip']==1){
                        $imo["imo_val_alu"]>0?$val=number_format($imo["imo_val_alu"],2,",","."):$val='consulte-nos';
                        $val = "Aluguel R$".$val;
                    }else {
                        $imo["imo_val_ven"]>0?$val=number_format($imo["imo_val_ven"],2,",","."):$val='consulte-nos';
                        $val = "Venda R$".$val;
                    }
                }else if($this->data['tip']==3){
                    $bai=$imo["emp_des_bai"];
                    $codimo=$imo["emp_isn"];
                    $endereco=explode(",",$imo["emp_des_end"]);
                    $tip = "empreendimento";
                    $imo["imo_val_ven"]>0?$val=number_format($imo["imo_val_ven"],2,",","."):$val='consulte-nos';
                    $val = "Apartir R$".$val;
                }
                //trata o endereço para a busca
                $endereco = $endereco[0];
                $cidade   = $imo["imo_des_loc"];

                $endereco = preg_replace("/R./", "", $endereco);
                $endereco = preg_replace("/RUA./", "", $endereco);
                $endereco = preg_replace("/AV./", "", $endereco);
                $endereco = preg_replace("/AV /", "", $endereco);
                $endereco = preg_replace("/./", "", $endereco);
                $endereco = preg_replace("/-/", "", $endereco);


                if($this->data['tip']==2){
                    $endereco = explode("n&deg;",$endereco);
                    $endereco = $endereco[0]." ".$bai." ".$cidade;
                }else {
                    $endereco = $endereco." ".$bai." ".$cidade;
                }
                $endereco = preg_replace("/[áàâãª]/","a",$endereco);
                $endereco = preg_replace("/[ÁÀÂÃ]/","A",$endereco);
                $endereco = preg_replace("/[éèê]/","e",$endereco);
                $endereco = preg_replace("/[ÉÈÊ]/","E",$endereco);
                $endereco = preg_replace("/[óòôõº]/","o",$endereco);
                $endereco = preg_replace("/[ÓÒÔÕ]/","O",$endereco);
                $endereco = preg_replace("/[úùû]/","u",$endereco);
                $endereco = preg_replace("/[ÚÙÛ]/","U",$endereco);
                $endereco = preg_replace("/ç/","c",$endereco);
                $endereco = preg_replace("/Ç/","C",$endereco);



                //trata as características do imóvel
                $crcPai = $imo["caracs"];
                $quantidade = count($crcPai);
                $ult = $quantidade - 1;
                $result="";
                for($cont=0;$cont<$quantidade;$cont++) {
                    if(($crcPai[$cont]["car_car_tipPai"] == 1) && ($cont < $ult)) {
                        $result = $result."".$crcPai[$cont]["car_car_desPai"].", ";
                    }else if(($crcPai[$cont]["car_car_tipPai"] == 1) && ($cont == $ult)) {
                        $result = $result."".$crcPai[$cont]["car_car_desPai"];
                    }else if(($crcPai[$cont]["car_car_tipPai"] == 2) && ($cont < $ult)) {
                        $result = $result."".$crcPai[$cont]["imc_qtd"]." ".$crcPai[$cont]["car_car_desPai"].", ";
                    }else if(($crcPai[$cont]["car_car_tipPai"] == 2) && ($cont == $ult)) {
                        $result = $result."".$crcPai[$cont]["imc_qtd"]." ".$crcPai[$cont]["car_car_desPai"];
                    }else if(($crcPai[$cont]["car_car_tipPai"] == 3) && ($cont < $ult)) {
                        $result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["dca_des"].", ";
                    }else if(($crcPai[$cont]["car_car_tipPai"] == 3) && ($cont == $ult)) {
                        $result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["dca_des"];
                    }else if(($crcPai[$cont]["car_car_tipPai"] == 4) && ($cont < $ult)) {
                        $result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["imc_des"].", ";
                    }else if(($crcPai[$cont]["car_car_tipPai"] == 4) && ($cont == $ult)) {
                        $result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["imc_des"];
                    }else if(($crcPai[$cont]["car_car_tipPai"] == 5) && ($cont < $ult) && ($crcPai[$cont]["car_car_des_uni"] == "R$")) {
                        $result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["car_car_des_uni"]." ".number_format($crcPai[$cont]["imc_val"],2,",",".").", ";
                    }else if(($crcPai[$cont]["car_car_tipPai"] == 5) && ($cont == $ult) && ($crcPai[$cont]["car_car_des_uni"] == "R$")) {
                        $result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["car_car_des_uni"]." ".number_format($crcPai[$cont]["imc_val"],2,",",".");
                    }else if(($crcPai[$cont]["car_car_tipPai"] == 5) && ($cont < $ult) && ($crcPai[$cont]["car_car_des_uni"] != "R$")) {
                        $result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["imc_val"]." ".$crcPai[$cont]["car_car_des_uni"].", ";
                    }else if(($crcPai[$cont]["car_car_tipPai"] == 5) && ($cont == $ult) && ($crcPai[$cont]["car_car_des_uni"] != "R$")) {
                        $result = $result."".$crcPai[$cont]["car_car_desPai"].": ".$crcPai[$cont]["imc_val"]." ".$crcPai[$cont]["car_car_des_uni"];
                    }
                }
                //fim do tratamento de características
                //trata o link pro imóvel
                $link = $this->data["local"]."index.php/imoveis/detalhes/".$tip."/".str_replace("/","_",$imo["tim_des"])."/".$codimo;
                $html.=str_replace("n&deg;"," ",'"'.$imo["foto"].'|'.$codimo.'|'.$imo["tim_des"].'|'.$bai.'|'.$endereco.'|'.substr($result,0,40).'...|'.$val.'|'.$link.'|'.$endereco.'|'.$cidade).'",';
            }
            $html.='""]}';
        }
        header("Content-Type:text/html; charset=utf-8");
        $this->data['html'] = utf8_encode($html);
        $this->load->view('principal/servidor',$this->data);
    }

    function getDestaquesTipo ($tim_isn, $finalidade) {

        $this->load->model('imovelloc');

        if ($finalidade == "residencial") {
            $destaques = $this->imovelloc->getDestaquesTipo(600, $tim_isn, 'residencial');
        } else {
            $destaques = $this->imovelloc->getDestaquesTipo(600, $tim_isn, 'comercial');
        }

        $i=0;

        if ($finalidade == "residencial") {

            echo '<div id="carousel-locacao" data-type="multi" data-interval="5000" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">';

            if ($destaques) {

                $total = count($destaques) / 3;

                $y = 0;

                for ($i = 0; $i < $total; $i++) {
                    if ($i == 0) {
                        echo '
					   <div class="carousel-item active">';
                        $i++;
                    } else {
                        echo '
					   <div class="carousel-item">';
                        $i++;
                    }

                    for ($x = 0; $x < 3; $x++) {
                        echo ' 
                           <div class="item-card" style="height:470px; width:358px;">
                                <div class="card" style="height:440px; width:358px;">
                                    <ul class="nav">
                                        <li class="nav-item">
                                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u='.$local.'index.php/imoveis/detalhes/aluguel/'.utf8_encode($destaques[$y]['tim_des']).'/'.$destaques[$y]['imo_isn'].'" class="fb-xfbml-parse-ignore fb-share-button nav-link" data-href="'.$local.'index.php/imoveis/detalhes/aluguel/'.$destaques[$y]['tim_des'].'/'.$destaques[$y]['imo_isn'].'" data-layout="button_count" data-mobile_iframe="true" title="Compartilhar"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <!--<li class="nav-item">
                                            <a href="" class="nav-link" title="Localização"><i class="fa fa-map-marker"></i></a>
                                        </li>-->
                                        <li class="nav-item">
                                            <a href="'.$local.'index.php/imoveis/detalhes/aluguel/'.$destaques[$y]['tim_des'].'/'.$destaques[$y]['imo_isn'].'" class="nav-link" title="Detalhes"><i class="fa fa-eye"></i></a>
                                        </li>
                                    </ul>
                                    <img class="card-img-top img-fluid" src="'.$destaques[$y]['foto'].'" style="height: 223px; width: 355px;" alt="Imóvel para locação">
                                    <div class="card-block">
                                        <h4 class="card-title">'.utf8_encode($destaques[$y]['tim_des']).'</h4>
                                        <p class="card-text">
                                            <span class="endereco">'.utf8_encode($destaques[$y]['imo_des_end'].' - '.$destaques[$y]['imo_des_bai']).'</span><br>
                                            <span class="text-destaque">'.utf8_encode(substr($destaques[$y]['imo_des_txt_dest'],0,50)).'...</span><br>
                                            <span class="valor">R$ '.number_format($destaques[$y]['imo_val_alu'],2 ,',' ,'.').'</span>
                                        </p>
                                    </div>
                                    <div class="card-footer">
                                        <span><i class="fa fa-hotel"></i>'.$destaques[$y]['caracs'][0]['qtd_quartos'].' quartos</span>
                                        <span><i class="fa fa-bathtub"></i>'.$destaques[$y]['caracs'][0]['qtd_suites'].' suítes</span>
                                        <span><i class="fa fa-car"></i>'.$destaques[$y]['caracs'][0]['qtd_vagas'].' vagas</span>
                                    </div>
                                </div>
                            </div>';

                        $y++;
                    }
                    //fechamento do item
                    echo '</div>';
                }

                echo '</div>
				</div>';

            } else {
                echo '<h3>Sem imóveis de destaque</h3>';
            }

        } else {

            echo '<div id="carousel-venda" data-type="multi" data-interval="5000" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">';

            if ($destaques) {

                $total = count($destaques) / 3;

                $y = 0;

                for ($i = 0; $i < $total; $i++) {
                    if ($i == 0) {
                        echo '
					   <div class="carousel-item active">';
                        $i++;
                    } else {
                        echo '
					   <div class="carousel-item">';
                        $i++;
                    }

                    for ($x = 0; $x < 3; $x++) {
                        echo ' 
                           <div class="item-card" style="height:470px; width:358px;">
                                <div class="card" style="height:440px; width:358px;">
                                    <ul class="nav">
                                        <li class="nav-item">
                                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u='.$local.'index.php/imoveis/detalhes/aluguel/'.utf8_encode($destaques[$y]['tim_des']).'/'.$destaques[$y]['imo_isn'].'" class="fb-xfbml-parse-ignore fb-share-button nav-link" data-href="'.$local.'index.php/imoveis/detalhes/aluguel/'.$destaques[$y]['tim_des'].'/'.$destaques[$y]['imo_isn'].'" data-layout="button_count" data-mobile_iframe="true" title="Compartilhar"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <!--<li class="nav-item">
                                            <a href="" class="nav-link" title="Localização"><i class="fa fa-map-marker"></i></a>
                                        </li>-->
                                        <li class="nav-item">
                                            <a href="'.$local.'index.php/imoveis/detalhes/aluguel/'.$destaques[$y]['tim_des'].'/'.$destaques[$y]['imo_isn'].'" class="nav-link" title="Detalhes"><i class="fa fa-eye"></i></a>
                                        </li>
                                    </ul>
                                    <img class="card-img-top img-fluid" src="'.$destaques[$y]['foto'].'" style="height: 223px; width: 355px;" alt="Imóvel para locação">
                                    <div class="card-block">
                                        <h4 class="card-title">'.utf8_encode($destaques[$y]['tim_des']).'</h4>
                                        <p class="card-text">
                                            <span class="endereco">'.utf8_encode($destaques[$y]['imo_des_end'].' - '.$destaques[$y]['imo_des_bai']).'</span><br>
                                            <span class="text-destaque">'.utf8_encode(substr($destaques[$y]['imo_des_txt_dest'],0,50)).'...</span><br>
                                            <span class="valor">R$ '.number_format($destaques[$y]['imo_val_alu'],2 ,',' ,'.').'</span>
                                        </p>
                                    </div>
                                    <div class="card-footer">
                                        <span><i class="fa fa-hotel"></i>'.$destaques[$y]['caracs'][0]['qtd_quartos'].' quartos</span>
                                        <span><i class="fa fa-bathtub"></i>'.$destaques[$y]['caracs'][0]['qtd_suites'].' suítes</span>
                                        <span><i class="fa fa-car"></i>'.$destaques[$y]['caracs'][0]['qtd_vagas'].' vagas</span>
                                    </div>
                                </div>
                            </div>';

                        $y++;
                    }
                    //fechamento do item
                    echo '</div>';
                }

                echo '</div>
				</div>';

            } else {
                echo '<h3>Sem imóveis de destaque</h3>';
            }
            
        }
    }

    function getTipos ($modalidade, $finalidade) {

        if ($modalidade == 'alugar'){
            $this->load->model('imovelloc');
            $tipos = $this->imovelloc->consultarTipos($finalidade);
            $tipos = json_encode($tipos, JSON_UNESCAPED_UNICODE);
            echo $tipos;
        } else {
            $this->load->model('imovelvenda');
            $tipos = $this->imovelvenda->consultarTipo();
            $tipos = json_encode($tipos, JSON_UNESCAPED_UNICODE);
            echo $tipos;
        }

    }

    /*function favoritarImovel ($imo_isn) {
        $this->load->model('cookie');
        echo $this->cookie->setaImoveisCookie($imo_isn);
    }

    function favoritos () {
        $this->load->model('imovelloc');
        $imoveis = $this->imovelloc->consultarFavoritos($_COOKIE['favoritos']);

        if ($_COOKIE['favoritos'] != '') {
            foreach ($imoveis as $favorito) {
                echo '
					<div class="thumbnail col-md-4 col-sm-6 thumbnaul-result-busca">                      
				  <div class="col-md-12 col-sm-12 col-xs-12 imob-result-busc">
					<a href="'.$this->data['local'].'index.php/imoveis/detalhes/aluguel/'.$favorito['tim_des'].'/'.$favorito['imo_isn'].'" ><img src="'.$favorito['foto'].'" class="img-responsive" alt="Imóvel destaque"></a>	                
					<p class="result-tipo col-md-12">'.$favorito['tim_des'].'</p>         
					<h3 class="imob-bairro">'.$favorito['imo_des_bai'].'</h3>
					<div class="imob-valor-carousel col-md-12 col-sm-12 col-xs-12">                     
					  <h3 class="cifrao-carousel col-md-2 col-sm-2 col-xs-2">R$</h3>
					  <h3 class="col-md-10 col-sm-10 col-xs-10 text-center valor-carousel">'.number_format($favorito['imo_val_alu'],2,',','.').'</h3>	                  
					</div> 
					<div class="cod-mais-detalhes result-tipo-det col-md-12 col-sm-12 col-xs-12">
					  <p class="result-tipo col-lg-6 col-md-6 col-sm-6 col-xs-6">CÓDIGO: '.$favorito['cod_esp'].'</p>
					  <p class="result-icon-det text-right col-lg-6 col-md-6 col-sm-6 col-xs-6"> 
						<a href=""><i class="fa fa-2x fa-facebook-official" aria-hidden="true" title="Compartilhar"></i></a>
						<a href=""><i class="fa fa-2x fa-instagram" aria-hidden="true" title="Publicar"></i></a>
						<a href="'.$this->data['local'].'index.php/imoveis/detalhes/aluguel/'.$favorito['tim_des'].'/'.$favorito['imo_isn'].'"><i class="fa fa-2x fa-plus" aria-hidden="true" title="Mais detalhes"></i></a>
						<a href="#" onclick="excluirFavorito(\''.$this->data['local'].'\',\''.$favorito['imo_isn'].'\')"><i class="fa fa-2x fa-heart-o" aria-hidden="true" title="Excluir Favorito"></i></a>
					  </p>	                  
					</div>		
				  </div>
				</div>
				';
            }
        } else {
            echo '<h3>Sem imóveis favoritos.</h3>';
        }

    }

    function excluirFavorito ($imo_isn) {
        $this->load->model('cookie');
        echo $this->cookie->deletaImovelCookie($imo_isn);
    }*/

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>