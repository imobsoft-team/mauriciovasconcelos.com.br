<?php
class Internas extends Controller {
    public $layout = 'internas';
    public $title =  'Mauricio Vasconcelos';
	public $css = array('style','geraMaps');
	
	public $js = array('main','app','ajax','js','geraMaps');
    
    public $data = array(
        'local'=>"../../../",
        'logomarca'=>'logotipo.png',
        'logoFooter' => 'logoFooter.png',
        'empresa'=>'Mauricio Vasconcelos',
        'cnpj'=>'26.214.153/0001-25',
        'telefone'=>'(88) 99208-8406',
        'telefone2' => '',
        'email' => 'contato@mauriciovasconcelos.com.br',
        'endereco'=>'Rua Coronel Diogo Gomes, 1032 - Centro - Sobral - CE, Brasil.',
        'sessaoCli'=>'EquaCli',
        'ogMetaTag' => [
            'url' => 'http://www.mauriciovasconcelos.com.br',
            'description' => 'A melhor imobiliária de Sobral',
            'image' => 'logo.jpg',
        ],
    );
	public $configMail = array('protocol' => '0',
    'smtp_host' => '0',
    'smtp_port' =>  0,
    'smtp_user' => '0',
    'smtp_pass' => '0',
    );
    public $emails = array('Locacação|locacao@mauriciovasconcelos.com.br','Vendas|vendas@mauriciovasconcelos.com.br','Financeiro|imobiliaria@mauriciovasconcelos.com.br');
    public $email1 = 'contato@mauriciovasconcelos.com.br';
    public $email2 = 'contato@mauriciovasconcelos.com.br';
    public $description = '';
    public $keys = array();
    public $mail = '';

    function Internas()
    {
        parent::Controller();

        /************************************
         ** DADOS PARA ENVIO DE EMAIL
         *************************************/
        // Inclui o arquivo class.phpmailer.php localizado na pasta phpmailer
        require("plugins/phpmailer/PHPMailerAutoload.php");

        // Inicia a classe PHPMailer
        $this->mail = new PHPMailer();

        // Define os dados do servidor e tipo de conexão
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $this->mail->IsSMTP(); // Define que a mensagem será SMTP
        $this->mail->Host = "smtp.gmail.com"; // Endereço do servidor SMTP
        $this->mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
        $this->mail->Port = '465'; // Usa autenticação SMTP? (opcional)
        $this->mail->SMTPSecure = 'ssl'; // Usa autenticação SMTP? (opcional)
        $this->mail->Username = 'imobsoft@imobsoft.com.br'; // Usuário do servidor SMTP
        $this->mail->Password = '3m4bs4ft'; // Senha do servidor SMTP

        /************************************
         ** FIM DADOS PARA ENVIO DE EMAIL
         *************************************/

		if(!isset($_SESSION['idioma'])){
			//session_register('idioma');
			$_SESSION['idioma'] = 1;
		}
		if(isset($_GET['idioma']) && is_numeric($_GET['idioma'])){
			switch($_GET['idioma']){
				case 2:
					$_SESSION['idioma'] = 2;
					break;
				case 3;
					$_SESSION['idioma'] = 3;
					break;
				default:
					$_SESSION['idioma'] = 1;
			}
		}
		if(!isset($_SESSION[''.$this->data['sessao']])){
			////session_register(''.$this->data['sessao']);
			$this->data['sessao'];
		}
		if(!isset($_SESSION['parametros'])){
		   
			////session_register('parametros');
		}
		if(!isset($_SESSION[''.$this->data['sessaoCli']])){
			////session_register(''.$this->data['sessaoCli']);
			$this->data['sessaoCli'];
		}
		$logado = $this->data['sessaoCli'];
		$this->load->model('enquete');
		$this->data['pergunta'] = $this->enquete->getAsk($logado);
		$this->data['resp']="";
		if($this->data['pergunta']){
			$this->data['resp'] = $this->enquete->getResp($this->data['pergunta']["id"]);
		}
		if(!isset($_SESSION['origem'])){
			////session_register('origem');
			$_SESSION['origem'];
		}
		$_SESSION['origem']?$this->data['origem'] = $_SESSION['origem']:$this->data['origem'] = $this->data["local"];
		//conta acesso ao site
		$this->load->model('tab_acs');
		$this->tab_acs->insertAcssite(session_id());
		$this->load->helper('url');
		$this->data['local'] = site_url();
        //Caminho da pasta de imagens
        $this->data['imagePath'] = site_url().'application/images/';
		//pega a tradu��o para a p�gina
		$this->load->model('utilidades');
		$this->data['tradutor'] = $this->utilidades->Tradutor($_SESSION['idioma'],$this->data['local']);
		//pega os imoveis para obama
		$this->load->model('imovelvenda');
		$this->load->model('lancamento');
		$imoveis = $this->imovelvenda->getDestaques(500);
		$lancs   = $this->lancamento->getDestaques(500);

	}
	
	function index()
	{
		$this->load->helper('url');
		redirect('','refresh');
	}
	
	function pagina($pag){
		$this->load->model('tab_textos');
		switch($pag){
			case "empresa":
				$tex = $this->tab_textos->getPagina(1);
				break;
			case "servicos":
				$tex = $this->tab_textos->getPagina(2);
				break;
			case "inquilino":
				$tex = $this->tab_textos->getPagina(3);
				break;
			case "proprietario":
				$tex = $this->tab_textos->getPagina(4);
				break;
			case "simuladores":
				$tex = $this->tab_textos->getPagina(5);
				break;
			case "lancamentos":
				$tex = $this->tab_textos->getPagina(6);
				break;
				
		}
		$this->data['men'] = "";
		$idm = $_SESSION['idioma'];
		if($tex){
			switch($idm){
				case 2:
					$tit = $tex->TEXTO_NOME_ING;
					$men = $tex->TEXTO_DES_ING;
					break;
				case 3:
					$tit = $tex->TEXTO_NOME_ESP;
					$men = $tex->TEXTO_DES_ESP;
					break;
				default:
					$tit = $tex->TEXTO_NOME;
					$men = $tex->TEXTO_DES;
					break;
			}
			/*$men = str_replace("<h3>","<h3 style=\"color: #000080;\">",$men);
			$men = str_replace("class=\"Title\"","style=\"font-weight: bold;font-size: 18px;color: #cc3300;\"",$men);
			$men = str_replace("class=\"Code\"","style=\"border: #8b4513 1px solid;padding-right: 5px;padding-left: 5px;color: #000066;font-family: 'Courier New' , Monospace;background-color: #ff9933;\"",$men);*/
			//inverte o htmlentities no banco
			$this->data['men'] = html_entity_decode($men);
		}
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->view('internas/text',$this->data);
	}
	
	function indicadores(){
		$this->load->model('tab_cota');
		$this->data['dados1'] = $this->tab_cota->getDados1();
		$this->data['dados2'] = $this->tab_cota->getDados2();
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->view('internas/cotacoes',$this->data);
	}
	
	function empresa(){
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		$this->load->view('internas/empresa',$this->data);
	}
	
	function servicos(){
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		$this->load->view('internas/empresa',$this->data);
	}
	
	function noticias(){
		$this->load->model('tab_not');
		$this->load->library('pagination');
		$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
		if(!$busca){
			$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
		}
		$ordem = (!$this->uri->segment("4")) ? 'NOT_ISN ASC' : $this->uri->segment("4");
		$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
		$config['base_url']   = $this->data['local'].'index.php/internas/noticias/'.$busca.'/'.$ordem.'/';
		if(empty($busca)) { $busca=""; }
		$config['total_rows'] = $this->tab_not->contaRegistros($busca);
		$config['per_page']   = 10;
		$config['first_link'] = 'Primeiro';
		$config['last_link']  = '�ltimo';
		$config['next_link']  = 'Pr�ximo';
		$config['prev_link']  = 'Anterior';
		$config['uri_segment'] = 5;
		$this->pagination->initialize($config); 
		$this->data["paginacao"] =  $this->pagination->create_links();
		$this->data['noticias'] = $this->tab_not->getNoticias(10,$inicio,$busca,$ordem);
		//array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->view('internas/noticias',$this->data);
	}
	
	function videos(){
		$this->load->model('videos');
		$this->load->library('pagination');
		$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
		if(!$busca){
			$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
		}
		$ordem = (!$this->uri->segment("4")) ? 'ID_VID ASC' : $this->uri->segment("4");
		$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
		$config['base_url']   = $this->data['local'].'index.php/internas/videos/'.$busca.'/'.$ordem.'/';
		if(empty($busca)) { $busca=""; }
		$config['total_rows'] = $this->videos->contaRegistrosVideos($busca);
		$config['per_page']   = 10;
		$config['first_link'] = 'Primeiro';
		$config['last_link']  = '�ltimo';
		$config['next_link']  = 'Pr�ximo';
		$config['prev_link']  = 'Anterior';
		$config['uri_segment'] = 5;
		$this->pagination->initialize($config); 
		$this->data["paginacao"] =  $this->pagination->create_links();
		$this->data['videos'] = $this->videos->getVideos(10,$inicio,$busca,$ordem);
		//array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->view('internas/videos',$this->data);
	}
	
	function noticia($id){
		$this->load->model('tab_not');
		$this->data['noticia'] = $this->tab_not->buscarNoticia($id);
		$this->load->view('internas/noticia',$this->data);
	}
	
	
	function newsletter(){
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		array_push($this->js,'global','internas');
		$this->load->view('internas/news_letter',$this->data);
	}
	
	function cadastrarNews(){
		$this->load->model('usuario');
		$this->load->library('form_validation');
		$this->load->helper('url');
		if($this->input->post('nome',TRUE) && $this->input->post('email',TRUE)){
			$msg = $this->usuario->cadastrarNews($this->input->post('nome',TRUE),$this->input->post('email',TRUE),$this->input->post('pes_tipo',TRUE));
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'"; </script>');
		}else {
			//redirect('internas/newsletter','refresh');
			redirect('welcome/index','refresh');
		}
		die;
		
	}
	
	function cadastroImovel(){
		$this->load->model('imovel');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$nome        = $this->input->post("nome",TRUE); 
		$fone1       = $this->input->post("fone1",TRUE);
		$fone2       = $this->input->post("fone2",TRUE);
		$email       = $this->input->post("email",TRUE);
		$cel    	 = $this->input->post("cel",TRUE);
		$end         = $this->input->post("endereco",TRUE);
		$comp        = $this->input->post("complementoEnd",TRUE);
		$numero      = $this->input->post("numero",TRUE);
		$cep         = $this->input->post("cep",TRUE);
		$uf          = $this->input->post("uf",TRUE);
		$bairro      = $this->input->post("bairro",TRUE);
		$cidade      = $this->input->post("cidade",TRUE);
		$fin         = $this->input->post("finalidade",TRUE);
		$resumo      = $this->input->post("resumo",TRUE);

        // Define o remetente
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $this->mail->From = $email; // Seu e-mail
        $this->mail->FromName = utf8_decode($nome); // Seu nome

        // Define os destinatário(s)
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        if ($fin == 1) {
            $finalidade = 'Locação';
            $this->mail->AddAddress($this->email1, 'Locacao');
        } elseif ($fin == 2) {
            $finalidade = 'Venda';
            $this->mail->AddAddress($this->email2, 'Vendas');
        } else {
            $finalidade = 'Locação e Venda';
            $this->mail->AddAddress($this->email1, 'Locacao');
            $this->mail->AddAddress($this->email2, 'Vendas');
        }

        //$mail->AddAddress('ciclano@site.net');
        //$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
        //$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta

        // Define os dados técnicos da Mensagem
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $this->mail->IsHTML(true); // Define que o e-mail será enviado como HTML
        //$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)

        // Define a mensagem (Texto e Assunto)
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $this->mail->Subject  = "Imovel cadastrado no site - Dados cliente"; // Assunto da mensagem
        $mensagem = "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Nome: ".utf8_decode($nome)."</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Email: ". $email."</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Telefone: ".$fone1."</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Telefone 2: ".$fone2."</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Celular: ".$cel."</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Endereco: ". utf8_decode($end).' '. $numero . "</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Complemento: ". utf8_decode($comp). "</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Bairro: ".utf8_decode($bairro)."</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Cidade: ".utf8_decode($cidade)."</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Estado: ".utf8_decode($uf)."</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Finalidade: ".utf8_decode($finalidade)."</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Resumo: ".utf8_decode($resumo)."</p>";
        $this->mail->Body = $mensagem;
        $this->mail->AltBody = "Este é o corpo da mensagem de teste, em Texto Plano! \r\n :)";

        // Define os anexos (opcional)
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        //$mail->AddAttachment("c:/temp/documento.pdf", "novo_nome.pdf");  // Insere um anexo

        // Envia o e-mail
        $enviado = $this->mail->Send();

        // Limpa os destinatários e os anexos
        $this->mail->ClearAllRecipients();
        $this->mail->ClearAttachments();

        // Exibe uma mensagem de resultado
        if ($enviado) {
            //echo "<script>alert('E-mail enviado com sucesso!')</script>";

            $msg = "E-mail enviado com sucesso. ";
        } else {
            //echo "Não foi possível enviar o e-mail.<br /><br />";
            //echo "<b>Informações do erro:</b> <br />" . $mail->ErrorInfo;

            $msg = "Um erro ocorreu, favor entrar em contato com o suporte e informar o erro: " . $this->mail->ErrorInfo;
        }
	
			$msg .= $this->imovel->cadastrarImovel($nome,$fone1,$fone2,$email,$cel,$end,$comp,$numero,$cep,$uf,$bairro,$cidade,$fin,$resumo);
			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/cadastro"; </script>');
		die;	
	}
	
	function contato(){
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		array_push($this->js,'global','internas');
		$this->data['emails'] = $this->emails;
		$this->load->view('internas/contato_pt',$this->data);
	}
	
	
	function trabalhe(){
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		array_push($this->js,'global','internas');
		$this->data['emails'] = $this->emails;
		$this->load->view('internas/trabalhe_conosco',$this->data);
	}
	
	
	function solicitacao(){
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		array_push($this->js,'global','internas');
		$this->data['emails'] = $this->emails;
		$this->load->view('internas/atualizarDados',$this->data);
	}
	
	function pesquisa(){
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		array_push($this->js,'global','internas');
		$this->data['emails'] = $this->emails;
		$this->load->view('internas/pesquisa',$this->data);
	}
	
	
	
	
	function solicitacaoEnv(){
		$this->load->library('form_validation');
			$this->load->helper('url');
			
			$tipo_msg    = $this->input->post("tipo_sol",TRUE); 
			$nome        = $this->input->post("nome",TRUE);
			$mailC        = $this->input->post("mail",TRUE);
			$fone        = $this->input->post("fone",TRUE);
			$depart      = $this->input->post("depto",TRUE);
			$msg         = $this->input->post("msg",TRUE);

			$destino  = explode('|',$this->emails[0]);
			
				$msgEmail = $this->data['empresa']."<br>";
				$msgEmail = $msgEmail." <strong>Tipo: ".$tipo_msg."</strong><br>";
				$msgEmail = $msgEmail."<strong> Nome: ".$nome."</strong><br>";
				$msgEmail = $msgEmail."<strong> E-mail: ".$mailC."</strong><br>";
				$msgEmail = $msgEmail."<strong> Fone: ".$fone."</strong><br>";
				$msgEmail = $msgEmail."<strong> Departamento: ".$depart."</strong><br>";
				$msgEmail = $msgEmail."<strong> Mensagem: ".$msg."</strong><br>";
				$this->load->plugin('phpmailer');
				$mail=new PHPMailer();
				if($this->configMail['protocol']=="smtp"){
					$mail->IsSMTP();
					$mail->SMTPAuth   = true;
					$mail->SMTPSecure = "";
					$mail->Host       = $this->configMail['smtp_host'];
					$mail->Port       = $this->configMail['smtp_port'];
					$mail->Username   = $this->configMail['smtp_user'];  
					$mail->Password   = $this->configMail['smtp_pass'];            
				}else {
					$mail->IsMail();
				}		 
				$mail->From       = $mailC;
				$mail->FromName   = $nome;
				$mail->Subject    = "Atualiza��o de Dados";
				$mail->Body       = utf8_encode($msgEmail);
				//$mail->AltBody    = "This is the body when user views in plain text format"; 
				$mail->WordWrap   = 50;	 
				$mail->AddAddress($destino[1]);
				$mail->IsHTML(true);
				$mail->send();
				$msg = "Enviado com sucesseo!";
	
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/solicitacao"; </script>');
		
		die;
	}
	
	

	
	function pesquisaEnv(){
		$this->load->library('form_validation');

			$this->load->helper('url');
			
			$tipo_msg    = $this->input->post("tipo_msg",TRUE); 
			$nome        = $this->input->post("nome",TRUE);
			$mailC        = $this->input->post("mail",TRUE);
			$fone        = $this->input->post("fone",TRUE);
			$atendente   = $this->input->post("atendente",TRUE);
			$depart      = $this->input->post("depto",TRUE);
			$msg         = $this->input->post("msg",TRUE);
			
			
			$destino  = explode('|',$this->emails[0]);
			
				$msgEmail = $this->data['empresa']."<br>";
				$msgEmail = $msgEmail." <strong>Tipo: ".$tipo_msg."</strong><br>";
				$msgEmail = $msgEmail."<strong> Nome: ".$nome."</strong><br>";
				$msgEmail = $msgEmail."<strong> E-mail: ".$mailC."</strong><br>";
				$msgEmail = $msgEmail."<strong> Fone: ".$fone."</strong><br>";
				$msgEmail = $msgEmail."<strong> Atendente: ".$atendente."</strong><br>";
				$msgEmail = $msgEmail."<strong> Departamento: ".$depart."</strong><br>";
				$msgEmail = $msgEmail."<strong> Mensagem: ".$msg."</strong><br>";
				$this->load->plugin('phpmailer');
				$mail = new PHPMailer();
				if($this->configMail['protocol']=="smtp"){
					$mail->IsSMTP();
					$mail->SMTPAuth   = true;
					$mail->SMTPSecure = "";
					$mail->Host       = $this->configMail['smtp_host'];
					$mail->Port       = $this->configMail['smtp_port'];
					$mail->Username   = $this->configMail['smtp_user'];  
					$mail->Password   = $this->configMail['smtp_pass'];            
				}else {
					$mail->IsMail();
				}		 
				$mail->From       = $mailC;
				$mail->FromName   = $nome;
				$mail->Subject    = "Pesquisa de Satisfa��o";
				$mail->Body       = utf8_encode($msgEmail);
				//$mail->AltBody    = "This is the body when user views in plain text format"; 
				$mail->WordWrap   = 50;	 
				$mail->AddAddress($destino[1]);
				$mail->IsHTML(true);
				$mail->send();
				$msg="Enviado com sucesseo!";
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/pesquisa"; </script>');
		
		die;
	}
	
	
	
	function cadastro(){
		array_push($this->js,'global','internas','cadastroImo');
		$this->load->view('internas/cadastro_imovel',$this->data);
	}
	
	function enviacontato(){
		$this->load->model('imovel');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$nome        = $this->input->post("nome",TRUE); 
		$fone1       = $this->input->post("fone1",TRUE);
		$fone2       = $this->input->post("fone2",TRUE);
		$email       = $this->input->post("email",TRUE);
		$cel    	 = $this->input->post("cel",TRUE);
		$depto       = $this->input->post("depto",TRUE);
		$obs         = $this->input->post("mensagem",TRUE);
		

        // Define o remetente
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $this->mail->From = $email; // Seu e-mail
        $this->mail->FromName = utf8_decode($nome); // Seu nome

        // Define os destinatário(s)
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $this->mail->AddAddress($depto, 'Site DMV');

        //$mail->AddAddress('ciclano@site.net');
        //$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
        //$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta

        // Define os dados técnicos da Mensagem
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $this->mail->IsHTML(true); // Define que o e-mail será enviado como HTML
        //$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)

        // Define a mensagem (Texto e Assunto)
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $this->mail->Subject  = "Contato do Site - Dados cliente"; // Assunto da mensagem
        $mensagem = "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Nome: ".utf8_decode($nome)."</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Email: ". $email."</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Telefone: ".$fone1."</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Telefone 2: ".$fone2."</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Celular: ".$cel."</p>";
        $mensagem .= "<p style='font-family: Cambria, Hoefler Text, Liberation Serif, Times, Times New Roman, serif; font-size: 16px;'>Endereco: ". utf8_decode($obs)."</p>";

        $this->mail->Body = $mensagem;
        $this->mail->AltBody = "Este é o corpo da mensagem de teste, em Texto Plano! \r\n :)";

        // Define os anexos (opcional)
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        //$mail->AddAttachment("c:/temp/documento.pdf", "novo_nome.pdf");  // Insere um anexo

        // Envia o e-mail
        $enviado = $this->mail->Send();

        // Limpa os destinatários e os anexos
        $this->mail->ClearAllRecipients();
        $this->mail->ClearAttachments();

        // Exibe uma mensagem de resultado
        if ($enviado) {
            //echo "<script>alert('E-mail enviado com sucesso!')</script>";

            $msg = "E-mail enviado com sucesso. ";
        } else {
            //echo "Não foi possível enviar o e-mail.<br /><br />";
            //echo "<b>Informações do erro:</b> <br />" . $mail->ErrorInfo;

            $msg = "Um erro ocorreu, favor entrar em contato com o suporte e informar o erro: " . $this->mail->ErrorInfo;
        }
			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/contato"; </script>');
		die;
	}
	
	function atendimento(){
		$this->title = "Atendimento on-line";
		$this->layout = "popup";
		$this->css = array('popup');
		$this->load->helper('url');
		array_push($this->js,'venda');
		$this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->data['msn'] = 'http://settings.messenger.live.com/Conversation/IMMe.aspx?invitee=e1a3afec7cddd916@apps.messenger.live.com&mkt=pt-br';
		$this->load->view('internas/msn',$this->data);
	}
	
	function p1(){
		$this->layout = 'popup';
		$this->css = '';
		$this->js = '';
		$this->load->view('principal/external1',$this->data);
	}
	
	function p2(){
		$this->layout = 'popup';
		$this->css = '';
		$this->js = '';
		$this->load->view('principal/external2',$this->data);
	}
	
	function parceiros () {	
		$this->load->model('tab_par');
		$this->data['parceiros'] = $this->tab_par->getParceiros();
		$this->load->view('internas/parceiros', $this->data);
	}
	
}
/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>