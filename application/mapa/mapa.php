<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>WhatsNearby, a useful plugin</title>
		<script src="example/js/es5-shim.min.js" type="text/javascript"></script>
		<script src="example/js/es5-sham.min.js" type="text/javascript"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
		<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAQaDEmFgvceuv1xoSUR-Fk4-ui_e7kFEU&sensor=false&libraries=places"></script>
		<script src="source/WhatsNearby.js" type="text/javascript"></script>
		<?php session_start();?>
		<script type="text/javascript">
		
			$(document).ready(function(){
				$("#wn2").whatsnearby({ 
					zoom:16,
					width:"100%",
					address: "<?php echo utf8_encode($_SESSION['enderecoPlaces']);?>",
					placesRadius: 1000,
					placesTypes: [
						'restaurant', 
						//'cafe', 
						'gym',
						'pharmacy',
						'hospital',
						'grocery_or_supermarket',
						'bakery',
						'school',
						'shopping_mall',
						'church',
						'university',
						'bank',
					],
					mainMarkerIcon: "images/marcador.png",
					placesTypesIcon: [
						'images/restaurant.png',
						//'images/coffee.png',
						'images/gym.png',
						'images/drugstore.png',
						'images/hospital.png',
						'images/supermarket.png',
						'images/bread.png',
						'images/school.png',
						'images/shopping.png',
						'images/church.png',
						'images/highschool.png',
						'images/bank.png',
					],
				});
			});
		</script>
	</head>
	<body>
				<div id="wn2">
					<div class="infowindow-markup" style="width:200px; height:50px;">
						<strong>{{name}}</strong><br/>
						{{vicinity}}
					</div>
				</div>
	</body>
</html>