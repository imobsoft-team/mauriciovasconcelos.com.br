function mudaCampos(formulario,acao){
	var action;
	action = acao == '0'?true:false;
	for (i = 0; i < formulario.length; i++)
	{
		formulario.elements[i].disabled = action;	
	}
}

function FormataCpfCgc(campo,tammax,teclapres,frmName) {
	if (frmName == undefined) frmName = "form";
	nmCampo = eval("document."+frmName+"."+campo);
	if (nmCampo) {
		vr = limpaCampo(nmCampo.value);
		if (vr.length <= 11) {
			FormataCpf(campo,tammax,teclapres,frmName)
		} else {
			FormataCgc(campo,tammax,teclapres,frmName)
		}
	}
}

function FormataCpf(campo,tammax,teclapres,frmName) {
	if (frmName == undefined) frmName = "form";
	nmCampo = eval("document."+frmName+"."+campo);
	if (nmCampo) {
		var tecla = teclapres.keyCode;
		vr = limpaCampo(nmCampo.value);
		tam = vr.length + 1;

		if (tam > 3) {
			vr = vr.substr(0, 3) + '.' + vr.substr(3);
			if (tam > 6)
				vr = vr.substr(0, 7) + '.' + vr.substr(7);
				if (tam > 9)
					vr = vr.substr(0, 11) + '-' + vr.substr(11);
		}
  	nmCampo.value = vr;
	}
}

function FormataCgc(campo,tammax,teclapres,frmName) {
	if (frmName == undefined) frmName = "form";
	nmCampo = eval("document."+frmName+"."+campo);
	if (nmCampo) {
		var tecla = teclapres.keyCode;
		vr = limpaCampo(nmCampo.value);
		tam = vr.length + 1;

		if (tam > 2) {
			vr = vr.substr(0, 2) + '.' + vr.substr(2);
			if (tam > 5)
				vr = vr.substr(0, 6) + '.' + vr.substr(6);
				if (tam > 8)
					vr = vr.substr(0, 10) + '/' + vr.substr(10);
					if (tam > 12)
						vr = vr.substr(0, 15) + '-' + vr.substr(15);
		}
  	nmCampo.value = vr;
	}
}
function limpaCampo(valor) {
// Retira os simbolos de um campo, retornando apenas os seus digitos
	var numeros = "0123456789";
	var cont = 0;
	while (cont < valor.length)
	  {
	  	if (numeros.indexOf(valor.charAt(cont)) < 0)
				valor = valor.substr(0, cont) + valor.substr(cont + 1);
			else
				cont++;
	  }
	return valor;
}

function validaNumeros(campo, event){
  var BACKSPACE= 8;
  var key;
  var tecla;
	
  CheckTAB=9;
  if (navigator.appName.indexOf("Netscape")!= -1) 
	tecla= event.which;
  else
	tecla= event.keyCode;
	 
  key = String.fromCharCode( tecla);
	
  if (tecla == 13) return false;
	 
  if (tecla == BACKSPACE) return true;
  
  if (tecla == CheckTAB) return true;
  
   if (tecla == 0) return true;
	
  return (numerico(key));
} // fim da funcao validaTecla 

function numerico(caractere)
{
  var strValidos = "0123456789,";
  if (strValidos.indexOf(caractere) == -1) return false;
  return true;
}

function carDivMap(id,barra,local,url,parametros){
	
	$('#mascara').append('<div id="mask" style="position:absolute;left:0;top:0;z-index:9000;background-color:#FFFFFF;display:none;"></div>');
	scrollP  = scrollPagina();
	tamanhoP = tamanhoPagina();
	var winH = tamanhoP[3];
	var winW = tamanhoP[2];
	var maskHeight = tamanhoP[1];
	var maskWidth  = tamanhoP[0];
	$('#mask').css({'width':maskWidth,'height':maskHeight});
	$('#mask').css({ opacity:0.5,background:"#000000" });
	$('#mask').css("z-index","900");
	$("#mask").fadeIn("fast");
	$("#"+id).css("z-index","1000");
	if(barra ==1){
		$("#"+id).css("top", (parseInt(scrollP[1] + (tamanhoP[3] / 10) - $("#"+id).width())));
	}else {
		$("#"+id).css("top", (parseInt(scrollP[1] + (tamanhoP[3] / 10))));
	}
	//$("#"+id).draggable({ handle: "#"+barra });
	$("#"+id).fadeIn("fast");
	
	//$("#"+id).css("display","block");
	$('#mask').click(function () {
		fechar(id);	
		$("#"+id).css("display","none");
	});	
	$("#"+id).html("<img src='"+local+"application/images/loading.gif' /><label style='font-family:Arial, Helvetica, sans-serif;color:#FF0000;font-size:12px;font-weight:bold'>Carregando...</label>");
	ajaxHtml(id,url,parametros);
	
}



function validaNumeros2(campo, event){
  var BACKSPACE= 8;
  var key;
  var tecla;
	
  CheckTAB=9;
  if (navigator.appName.indexOf("Netscape")!= -1) 
	tecla= event.which;
  else
	tecla= event.keyCode;
	 
  key = String.fromCharCode( tecla);
	
  if (tecla == 13) return false;
	 
  if (tecla == BACKSPACE) return true;
  
  if (tecla == CheckTAB) return true;
  
   if (tecla == 0) return true;
	
  return (numerico2(key));
} // fim da funcao validaTecla 

function numerico2(caractere)
{
  var strValidos = "0123456789";
  if (strValidos.indexOf(caractere) == -1) return false;
  return true;
}

//da receita federal
function validaCPF(obj, str){
var numero;
var digito = new Array(10); // array para os d�gitos do CPF.
var aux = 0; // �ndice para a string num.
var posicao
var i
var soma
var dv
var dvInformado;

if(obj != null)
{
str = obj.value;
}

//numero = _extraiNumero(str);

// Retira os d�gitos formatadores de CPF '.' e '-', caso existam.
if (str.length > 0){
while ((str.indexOf('.') != -1) || (str.indexOf('-') != -1))
{
if (str.indexOf('.') != -1)
{
aux = str.indexOf('.');
str = str.substr(0, aux) + str.substr(aux+1, str.length-1);
}
if (str.indexOf('-') != -1)
{
aux = str.indexOf('-');
str = str.substr(0, aux) + str.substr(aux+1, str.length-1);
}
} //while
} //if

//verifica CPFs manjados
switch (str) {
case '0':
case '00':
case '000':
case '0000':
case '00000':
case '000000':
case '0000000':
case '00000000':
case '000000000':
case '0000000000':
case '00000000000':
case '11111111111':
case '22222222222':
case '33333333333':
case '44444444444':
case '55555555555':
case '66666666666':
case '77777777777':
case '88888888888':
case '99999999999':
return false;
}

// In�cio da valida��o do CPF.
/* Retira do n�mero informado os dois �ltimos d�gitos */
dvInformado = str.substr(9,2);
/* Desmembra o n�mero do CPF no array digito */
for (i=0; i<=8; i++)
{
digito[i] = str.substr(i,1);
}
/* Calcula o valor do 10o. digito de verifica��o */
posicao = 10;
soma = 0;
for (i=0; i<=8; i++)
{
soma = soma + digito[i] * posicao;
posicao--;
}
digito[9] = soma % 11;
if (digito[9] < 2)
{
digito[9] = 0;
}
else
{
digito[9] = 11 - digito[9];
}
/* Calcula o valor do 11o. digito de verifica��o */
posicao = 11;
soma = 0;
for (i=0; i<=9; i++)
{
soma = soma + digito[i] * posicao;
posicao--;
}
digito[10] = soma % 11;
if (digito[10] < 2)
{
digito[10] = 0;
}
else
{
digito[10] = 11 - digito[10];
}
dv = digito[9] * 10 + digito[10];
/* Verifica se o DV calculado � igual ao informado */
if(dv != dvInformado)
{
return false;
}
else
{
return true;
}
}

function valida_cnpj(cnpj){
      var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
      digitos_iguais = 1;
//adicionado para tirar os digitos		
if (cnpj.length > 0){
while ((cnpj.indexOf('.') != -1) || (cnpj.indexOf('-') != -1) || (cnpj.indexOf('/') != -1))
{
if (cnpj.indexOf('.') != -1)
{
aux = cnpj.indexOf('.');
cnpj = cnpj.substr(0, aux) + cnpj.substr(aux+1, cnpj.length-1);
}
if (cnpj.indexOf('-') != -1)
{
aux = cnpj.indexOf('-');
cnpj = cnpj.substr(0, aux) + cnpj.substr(aux+1, cnpj.length-1);
}
if (cnpj.indexOf('/') != -1)
{
aux = cnpj.indexOf('/');
cnpj = cnpj.substr(0, aux) + cnpj.substr(aux+1, cnpj.length-1);
}
} //while
} //if
		
		
      if (cnpj.length < 14 && cnpj.length < 15)
            return false;
      for (i = 0; i < cnpj.length - 1; i++)
            if (cnpj.charAt(i) != cnpj.charAt(i + 1)){
                  digitos_iguais = 0;
                  break;
             }
      if (!digitos_iguais){
            tamanho = cnpj.length - 2
            numeros = cnpj.substring(0,tamanho);
            digitos = cnpj.substring(tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (i = tamanho; i >= 1; i--)
                  {
                  soma += numeros.charAt(tamanho - i) * pos--;
                  if (pos < 2)
                        pos = 9;
                  }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0))
                  return false;
            tamanho = tamanho + 1;
            numeros = cnpj.substring(0,tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (i = tamanho; i >= 1; i--)
                  {
                  soma += numeros.charAt(tamanho - i) * pos--;
                  if (pos < 2)
                        pos = 9;
                  }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1))
                  return false;
            return true;
            }
      else
            return false;
} 

function validaEmailBO(email) {
		if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.val()))) {
			return false;
		}
		return true;
}

function enterloga(oEvent,funcao){
  var oEvent = (oEvent)? oEvent : event;
  var oTarget =(oEvent.target)? oEvent.target : oEvent.srcElement;
  if(oEvent.keyCode==13){
    //oEvent.keyCode = 9;
	 eval(funcao+";");
  }else if(oTarget.type=="text" && oEvent.keyCode==13){
    //return false;
    //oEvent.keyCode = 9;
	 eval(funcao+";");
  }else if (oTarget.type=="radio" && oEvent.keyCode==13){
    //oEvent.keyCode = 9;
	 eval(funcao+";");
  }
}

function ajaxHtml(local,servidor,param){
	$.ajax({
							 type: "POST",
							 url: servidor,
							 dataType: "html",
							 data: param,
							 success: function(msg){
								 if(msg){									 
									 $("#carreg").css("display","none");
									 $('#'+local).html(msg);
									
									 									
								 }else{									 
									 alert("Sem resposta do servidor!");
									 
								 }
							 },
			 error: function(){	
			 			 alert("Erro no servidor de envio, favor contactar o suporte!");			 				
						 
						 $("#mask4").fadeOut("slow");
						 
			 },
							 beforeSend: function(){
									
									
								}
	});	
}

function ajaxXml(destino,servidor,param,form,frase,antes,erro){
	if(form){
		form_string = $("#"+form).serialize();
		eval("form = document."+form+";");
	}else{
		form_string = "";
	}
	$.ajax({
							 type: "POST",
							 url: servidor,
							 dataType: "xml",
							 data: param+"&"+form_string,
							 success: function(msg){
								  var retorno = msg.getElementsByTagName('retorno')[0];
								 if(retorno.getElementsByTagName('msg')[0].firstChild.nodeValue == '1'){	
									 if(frase == 1){	 
										alert(retorno.getElementsByTagName('alerta')[0].firstChild.nodeValue);				 				
										eval(destino);
										
									 }else{
										eval(destino);
									 }									 
									 if(form){
									 	mudaCampos(form,1);
									 }
			
								 }else{									 
									 alert(retorno.getElementsByTagName('alerta')[0].firstChild.nodeValue);
									 eval(erro);						 							 
									 if(form){
									 	mudaCampos(form,1);
									 }
									 
								 }
							 },
			 error: function(){	
			 			 alert("Erro no servidor de envio, favor contactar o suporte!");			 				
						 if(form){
						 	mudaCampos(form,1);
						 }
						 
			 },
			 beforeSend: function(){
				if(form){
					mudaCampos(form,0);
				}
				eval(antes);									
			}
	});	
}

function formataValorMonetario(campooriginal,decimais){
		  if(campooriginal.value){
		    var posicaoPontoDecimal;
		    var campo = '';
		    var resultado = '';
		    var pos,sep,dec;
		
		    //Retira possiveis separadores de milhar
		    for (pos=0; pos < campooriginal.value.length; pos ++)
		    {
			  if (campooriginal.value.charAt(pos)!='.')
				campo = campo + campooriginal.value.charAt(pos);
		    }     
		
		    //Formata valor monet�rio com decimais
		    posicaoPontoDecimal = campo.indexOf(',');
		    if (posicaoPontoDecimal != -1)
		    {
			  sep = 0;
			  for (pos=posicaoPontoDecimal-1;pos >= 0;pos--)
			  {
				sep ++;
				if (sep > 3)
				{
				   //resultado = '.' + resultado;
				   sep = 1;
				}
		
				resultado = campo.charAt(pos) + resultado;   
			  }
		
			  // Trata parte decimal
			  if (parseInt(decimais) > 0 )
			  {
				 resultado = resultado + ',';
			  
				 pos=posicaoPontoDecimal+1;
				 for (dec = 1;dec <= parseInt(decimais); dec++)
				 {
				   if (pos < campo.length)
				   {
					  resultado = resultado + campo.charAt(pos);
					  pos++;
				   }
				   else
					  resultado = resultado + '0';   
				 }
		
			  } // trata decimais
		   }
		   // Trata valor monet�rio sem decimais
		   else
		   {
			  sep = 0;
			  for (pos=campo.length-1;pos >= 0;pos--)
			  {
				sep ++;
				if (sep > 3)
				{
				  // resultado = '.' + resultado;
				   sep = 1;
				}
				resultado = campo.charAt(pos) + resultado;   
			  }
			  // Trata parte decimal
			  if (parseInt(decimais) > 0 )
			  {
				 resultado = resultado + ',';
				 for (dec = 1;dec <= parseInt(decimais); dec++)
				 {
					  resultado = resultado + '0';   
				 }
			  } // trata decimais
		   }
		   campooriginal.value = resultado;
		}
}

function Maiusculas(id){
	$("#"+id).val($("#"+id).val().toUpperCase());
	
}

function tamanhoPagina() {
			var xScroll, yScroll;
			if (window.innerHeight && window.scrollMaxY) {	
				xScroll = window.innerWidth + window.scrollMaxX;
				yScroll = window.innerHeight + window.scrollMaxY;
			} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
				xScroll = document.body.scrollWidth;
				yScroll = document.body.scrollHeight;
			} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
				xScroll = document.body.offsetWidth;
				yScroll = document.body.offsetHeight;
			}
			var windowWidth, windowHeight;
			if (self.innerHeight) {	// all except Explorer
				if(document.documentElement.clientWidth){
					windowWidth = document.documentElement.clientWidth; 
				} else {
					windowWidth = self.innerWidth;
				}
				windowHeight = self.innerHeight;
			} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
				windowWidth = document.documentElement.clientWidth;
				windowHeight = document.documentElement.clientHeight;
			} else if (document.body) { // other Explorers
				windowWidth = document.body.clientWidth;
				windowHeight = document.body.clientHeight;
			}	
			// for small pages with total height less then height of the viewport
			if(yScroll < windowHeight){
				pageHeight = windowHeight;
			} else { 
				pageHeight = yScroll;
			}
			// for small pages with total width less then width of the viewport
			if(xScroll < windowWidth){	
				pageWidth = xScroll;		
			} else {
				pageWidth = windowWidth;
			}
			arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight);
			return arrayPageSize;
};
		
function scrollPagina() {
			var xScroll, yScroll;
			if (self.pageYOffset) {
				yScroll = self.pageYOffset;
				xScroll = self.pageXOffset;
			} else if (document.documentElement && document.documentElement.scrollTop) {	 // Explorer 6 Strict
				yScroll = document.documentElement.scrollTop;
				xScroll = document.documentElement.scrollLeft;
			} else if (document.body) {// all other Explorers
				yScroll = document.body.scrollTop;
				xScroll = document.body.scrollLeft;	
			}
			arrayPageScroll = new Array(xScroll,yScroll);
			return arrayPageScroll;
};

function listarCidades(opcao,local){
	if(opcao>0){
	campo_select = document.getElementById("cidade");
	$.ajax({
					 type: "POST",
					 url: local+"index.php/ajax/consultarCidade",
					 dataType: "html",
					 data: "opcao="+opcao,
					 success: function(msg){
						 if(msg){
							document.getElementById("bairro").length = 0;								
							document.getElementById("bairro").options[0] = new Option("Bairro","0");
							campo_select.options.length = 0;
							results = msg.split(",");	 
							for( i = 0; i < results.length; i++ ){ 
							  string = results[i].split( "|" );
							  campo_select.options[i] = new Option( string[0], string[1] );	
							}
							mudaCampos(document.frmBusca,0);
							carregarTipoDeImoveis(local);		 									 
				
						 }else{
							alert("Erro na aplica��o, favor tentar em alguns minutos!");
							document.getElementById("bairro").length = 0;								
							document.getElementById("bairro").options[0] = new Option("Bairro","0");
							campo_select.options.length = 0;
							document.getElementById("cidade").options[0] = new Option("Bairro","0");
							mudaCampos(document.frmBusca,1);			 												
										 
						 }
					 },
					 beforeSend: function(){												
						document.getElementById("cidade").length = 0;
						document.getElementById("cidade").options[0] = new Option("carregando...","0");
						document.getElementById("bairro").length = 0;
						document.getElementById("bairro").options[0] = new Option("carregando...","0");
						mudaCampos(document.frmBusca,0);													
										
					 }
	});
  }
}


function listarCidades2(opcao,local){
	if(opcao>0){
	campo_select = document.getElementById("cidade2");
	$.ajax({
					 type: "POST",
					 url: local+"index.php/ajax/consultarCidade",
					 dataType: "html",
					 data: "opcao="+opcao,
					 success: function(msg){
						 if(msg){
							document.getElementById("bairro2").length = 0;								
							document.getElementById("bairro2").options[0] = new Option("Bairro","0");
							campo_select.options.length = 0;
							results = msg.split(",");	 
							for( i = 0; i < results.length; i++ ){ 
							  string = results[i].split( "|" );
							  campo_select.options[i] = new Option( string[0], string[1] );	
							}
							mudaCampos(document.frmBusca2,0);
							carregarTipoDeImoveis2(local);		 									 
				
						 }else{
							alert("Erro na aplica��o, favor tentar em alguns minutos!");
							document.getElementById("bairro2").length = 0;								
							document.getElementById("bairro2").options[0] = new Option("Bairro","0");
							campo_select.options.length = 0;
							document.getElementById("cidade2").options[0] = new Option("Bairro","0");
							mudaCampos(document.frmBusca2,1);			 												
										 
						 }
					 },
					 beforeSend: function(){												
						document.getElementById("cidade2").length = 0;
						document.getElementById("cidade2").options[0] = new Option("carregando...","0");
						document.getElementById("bairro2").length = 0;
						document.getElementById("bairro2").options[0] = new Option("carregando...","0");
						mudaCampos(document.frmBusca2,0);													
										
					 }
	});
  }
}

function listarCidades3(opcao,local){
	if(opcao>0){
	campo_select = document.getElementById("cidade3");
	$.ajax({
		type: "POST",
		url: local+"index.php/ajax/consultarCidade",
		dataType: "html",
		data: "opcao="+opcao,
		success: function(msg){
			if(msg){
			document.getElementById("bairro3").length = 0;								
			document.getElementById("bairro3").options[0] = new Option("Bairro","0");
			campo_select.options.length = 0;
			results = msg.split(",");	 
			for( i = 0; i < results.length; i++ ){ 
				string = results[i].split( "|" );
				campo_select.options[i] = new Option( string[0], string[1] );	
			}
			mudaCampos(document.frmBusca3,0);
			carregarTipoDeImoveis3(local);		 									 
			}else{
			alert("Erro na aplicacao, favor tentar em alguns minutos!");
			document.getElementById("bairro3").length = 0;								
			document.getElementById("bairro3").options[0] = new Option("Bairro","0");
			campo_select.options.length = 0;
			document.getElementById("cidade3").options[0] = new Option("Bairro","0");
			mudaCampos(document.frmBusca3,1);			 												
							
			}
		},
		beforeSend: function(){												
			document.getElementById("cidade3").length = 0;
			document.getElementById("cidade3").options[0] = new Option("carregando...","0");
			document.getElementById("bairro3").length = 0;
			document.getElementById("bairro3").options[0] = new Option("carregando...","0");
			mudaCampos(document.frmBusca3,0);																
		}
	});
  }
}


function carregarTipoDeImoveis(local){
  opcao  = document.getElementById('opcoes').value;
  campo_select = document.getElementById("tipo");	
  $.ajax({
		 type: "POST",
		 url:  local+"index.php/ajax/consultarTipo",
		 dataType: "html",
		 data: "opcao="+opcao,
		 success: function(msg){
			 if(msg){																	
				campo_select.options.length = 0;
				results = msg.split(",");	 
				for( i = 0; i < results.length; i++ ){ 
				  string = results[i].split( "|" );
				  campo_select.options[i] = new Option( string[0], string[1] );
				}
				mudaCampos(document.frmBusca,1); 	 	 									 
	
			 }else{
				alert("Erro na aplica��o, favor tentar em alguns minutos!");																		
				campo_select.options.length = 0;
				document.getElementById("tipo").options[0] = new Option("Escolha uma op��o","0");
				mudaCampos(document.frmBusca,1);			 												
							 
			 }
		 },
		 beforeSend: function(){
			campo_select.options.length = 0;												
			document.getElementById("tipo").options[0] = new Option("Carregando...","0");							
			mudaCampos(document.frmBusca,0);													
							
		 }
	});			
	
}


function carregarTipoDeImoveis2(local){
  opcao  = document.getElementById('opcoes2').value;
  campo_select = document.getElementById("tipo2");	
  $.ajax({
		 type: "POST",
		 url:  local+"index.php/ajax/consultarTipo",
		 dataType: "html",
		 data: "opcao="+opcao,
		 success: function(msg){
			 if(msg){																	
				campo_select.options.length = 0;
				results = msg.split(",");	 
				for( i = 0; i < results.length; i++ ){ 
				  string = results[i].split( "|" );
				  campo_select.options[i] = new Option( string[0], string[1] );
				}
				mudaCampos(document.frmBusca2,1); 	 	 									 
	
			 }else{
				alert("Erro na aplica��o, favor tentar em alguns minutos!");																		
				campo_select.options.length = 0;
				document.getElementById("tipo2").options[0] = new Option("Escolha uma op��o","0");
				mudaCampos(document.frmBusca2,1);			 												
							 
			 }
		 },
		 beforeSend: function(){
			campo_select.options.length = 0;												
			document.getElementById("tipo2").options[0] = new Option("Carregando...","0");							
			mudaCampos(document.frmBusca2,0);													
							
		 }
	});			
	
}

function carregarTipoDeImoveis3(local){
	opcao  = document.getElementById('opcoes3').value;
	campo_select = document.getElementById("tipo3");	
	$.ajax({
		   type: "POST",
		   url:  local+"index.php/ajax/consultarTipo",
		   dataType: "html",
		   data: "opcao="+opcao,
		   success: function(msg){
			   if(msg){																	
				  campo_select.options.length = 0;
				  results = msg.split(",");	 
				  for( i = 0; i < results.length; i++ ){ 
					string = results[i].split( "|" );
					campo_select.options[i] = new Option( string[0], string[1] );
				  }
				  mudaCampos(document.frmBusca3,1); 	 	 									 
	  
			   }else{
				  alert("Erro na aplicacao, favor tentar em alguns minutos!");																		
				  campo_select.options.length = 0;
				  document.getElementById("tipo3").options[0] = new Option("Escolha uma opcao","0");
				  mudaCampos(document.frmBusca3,1);			 												
							   
			   }
		   },
		   beforeSend: function(){
			  campo_select.options.length = 0;												
			  document.getElementById("tipo3").options[0] = new Option("Carregando...","0");							
			  mudaCampos(document.frmBusca3,0);													
							  
		   }
	  });			
	  
  }



function carregarBairros(local){
  opcao  = document.getElementById('opcoes').value;
  cidade = document.getElementById('cidade').value;
  if(opcao>0 && opcao<3 && cidade){
  	campo_select = document.getElementById("bairro");		
	$.ajax({
					type: "POST",
					url:  local+"index.php/ajax/consultarBairros",
					dataType: "html",
					 data: "opcao="+opcao+"&cidade="+cidade,
					 success: function(msg){
						 if(msg){																	
							campo_select.options.length = 0;
							results = msg.split(",");	 
							for( i = 0; i < results.length; i++ ){ 
							  string = results[i].split( "|" );
							  campo_select.options[i] = new Option( string[0], string[1] );
							  if(i==0){
							  	//campo_select.options[i].selected = true;
							  }
							}
							mudaCampos(document.frmBusca,1);								
				
						 }else{
							alert("Erro na aplica��o, favor tentar em alguns minutos!");																		
							campo_select.options.length = 0;
							campo_select.options[0] = new Option("Cidade","0");
							mudaCampos(document.frmBusca,1);			 												
										 
						 }
					 },
					 beforeSend: function(){
						campo_select.options.length = 0;												
						campo_select.options[0] = new Option("Carregando...","0");							
						mudaCampos(document.frmBusca,0);													
										
					 }
			});	
	}		
}




function carregarBairros2(local){
  opcao  = document.getElementById('opcoes2').value;
  cidade = document.getElementById('cidade2').value;
  if(opcao>0 && opcao<3 && cidade){
  	campo_select = document.getElementById("bairro2");		
	$.ajax({
					type: "POST",
					url:  local+"index.php/ajax/consultarBairros",
					dataType: "html",
					 data: "opcao="+opcao+"&cidade="+cidade,
					 success: function(msg){
						 if(msg){																	
							campo_select.options.length = 0;
							results = msg.split(",");	 
							for( i = 0; i < results.length; i++ ){ 
							  string = results[i].split( "|" );
							  campo_select.options[i] = new Option( string[0], string[1] );
							  if(i==0){
							  	//campo_select.options[i].selected = true;
							  }
							}
							mudaCampos(document.frmBusca2,1);
							/*$('#bairro2').multiselect({
								  //includeSelectAllOption: true,
								  //selectAllValue: 'select-all-value'
							});*/
				
						 }else{
							alert("Erro na aplica��o, favor tentar em alguns minutos!");																		
							campo_select.options.length = 0;
							campo_select.options[0] = new Option("Cidade","0");
							mudaCampos(document.frmBusca2,1);			 												
										 
						 }
					 },
					 beforeSend: function(){
						campo_select.options.length = 0;												
						campo_select.options[0] = new Option("Carregando...","0");							
						mudaCampos(document.frmBusca2,0);													
										
					 }
			});	
	}		
}

function carregarBairros3(local){
	opcao  = document.getElementById('opcoes3').value;
	cidade = document.getElementById('cidade3').value;
	if(opcao>0 && opcao<3 && cidade){
		campo_select = document.getElementById("bairro3");		
	  $.ajax({
					  type: "POST",
					  url:  local+"index.php/ajax/consultarBairros",
					  dataType: "html",
					   data: "opcao="+opcao+"&cidade="+cidade,
					   success: function(msg){
						   if(msg){																	
							  campo_select.options.length = 0;
							  results = msg.split(",");	 
							  for( i = 0; i < results.length; i++ ){ 
								string = results[i].split( "|" );
								campo_select.options[i] = new Option( string[0], string[1] );
								if(i==0){
									//campo_select.options[i].selected = true;
								}
							  }
							  mudaCampos(document.frmBusca3,1);
							  /*$('#bairro2').multiselect({
									//includeSelectAllOption: true,
									//selectAllValue: 'select-all-value'
							  });*/
				  
						   }else{
							  alert("Erro na aplicacao, favor tentar em alguns minutos!");																		
							  campo_select.options.length = 0;
							  campo_select.options[0] = new Option("Cidade","0");
							  mudaCampos(document.frmBusca3,1);			 												
										   
						   }
					   },
					   beforeSend: function(){
						  campo_select.options.length = 0;												
						  campo_select.options[0] = new Option("Carregando...","0");							
						  mudaCampos(document.frmBusca3,0);													
										  
					   }
			  });	
	  }		
  }



function buscaRapida(){
	 op = document.frmBusca.opcoes.value;
	 vali = document.frmBusca.valorini.value;
	 valf = document.frmBusca.valorfim.value;
	 tipo = document.frmBusca.tipo.value;
	 codigo = document.frmBusca.codigo.value;
	 cidade = document.frmBusca.cidade.value;
	 bairro = document.frmBusca.bairro.value;
	 if(codigo && op>0){
		 $("#frmBusca").submit();
	 }else {
		 if((op == 0)) {
			alert("� necess�rio escolher uma pretens�o!");
			document.frmBusca.opcoes.focus();
			return false;
		 }else if(tipo == 0){
			alert("� necess�rio escolher um tipo de imovel!");
			document.frmBusca.tipo.focus();
			return false;
		 }else if(cidade == 0){
			alert("� necess�rio escolher uma cidade!");
			document.frmBusca.cidade.focus();
			return false;
		 }else {
			$("#frmBusca").submit();		
		 }
	 }
}

function buscaRapida2(local){

	 op = document.frmBusca.opcoes.value;
	 vali = document.frmBusca.valorini.value;
	 valf = document.frmBusca.valorfim.value;
	 tipo = document.frmBusca.tipo.value;
	 tipoNom = $("#tipo option").filter(":selected").text();
	 codigo = document.frmBusca.codigo.value;
	 cidade = document.frmBusca.cidade.value;
	 bairro = document.frmBusca.bairro.value;
	 if(codigo && op>0){
		 if(op==1) op="aluguel"; else op="venda";
		 document.frmBusca.action = local+"index.php/imoveis/buscaRapida/"+op+"/"+codigo+"/imoveis"+op+"codigo"+codigo+".html";
		 $("#frmBusca").submit();
	 }else {
		 if((op == 0)) {
			alert("� necess�rio escolher uma pretens�o!");
			document.frmBusca.opcoes.focus();
			return false;
		 }else if(tipo == 0){
			alert("� necess�rio escolher um tipo de imovel!");
			document.frmBusca.tipo.focus();
			return false;
		 }else if(cidade == 0){
			alert("� necess�rio escolher uma cidade!");
			document.frmBusca.cidade.focus();
			return false;
		 }else {
			if(op==1) op="aluguel"; else op="venda";
			if(vali) vali = vali.replace(",","."); else vali=0;
			if(valf) valf = valf.replace(",","."); else valf=0;
			if(valf>0 && vali>0){
				valores="/"+vali+" a "+valf+"/";
				valstr = +vali+"a"+valf;
			}else if(valf>0){
				valores="/ate "+valf+"/";
				valstr = "ate"+valf;
			}else if(vali>0){
				valores="/a partir "+vali+"/";
				valstr = "apartir"+vali;
			}else {
				valores="/0/";
				valstr = "";
			}
			if($("#bairro option").filter(":selected").text().indexOf("TODOS")!=-1){
				 pagina="/imoveis"+tipoNom+op+cidade+valstr+".html";
				 document.frmBusca.action = local+"index.php/imoveis/buscaRapida/"+op+"/"+cidade+valores+tipo+"/"+tipoNom+pagina;
				 $("#frmBusca").submit();
			}else {
				var bairro="";	
				var arr = new Array;
				$("#bairro option").filter(":selected").each(function (){
					if(this.selected){
						if(this.value!=0) arr.push(this.value);
					}
				});
				for(i=0;i<arr.length;i++){
					if(i==(arr.length-1))
						bairro+=arr[i];
					else 
						bairro+=arr[i]+"-";
				}
				 pagina="/imoveis"+tipoNom+op+cidade+valstr+bairro+".html";
				 document.frmBusca.action = local+"index.php/imoveis/buscaRapida/"+op+"/"+cidade+valores+tipo+"/"+tipoNom+"/"+bairro+pagina;
				 $("#frmBusca").submit();
			}			
		 }
	 }
}

function buscarPorCodigo(local) {
    op = document.frmBusca2.opcoes2.value;
    codigo = document.frmBuscaCod.codigo3.value;
    if (codigo && op > 0) {
        if (op == 1) op = "aluguel"; else op = "venda";
        document.frmBuscaCod.action = local + "index.php/imoveis/buscaRapida/" + op + "/" + codigo + "/imoveis" + op + "codigo" + codigo + ".html";
        $("#frmBuscaCod").submit();
    }
}


function buscaRapida3(local){
	 op   = document.frmBusca2.opcoes2.value;
	 vali = document.frmBusca2.valorini2.value;
	 valf = document.frmBusca2.valorfim2.value;
	 tipo = document.frmBusca2.tipo2.value;
	 tipoNom = $("#tipo2 option").filter(":selected").text();
	 codigo = document.frmBusca2.codigo2.value;
	 cidade = document.frmBusca2.cidade2.value;
	 bairro = document.frmBusca2.bairro2.value;
	 if(codigo && op>0){
		 if(op==1) op="aluguel"; else op="venda";
		 document.frmBusca2.action = local+"index.php/imoveis/buscaRapida/"+op+"/"+codigo+"/imoveis"+op+"codigo"+codigo+".html";
		 $("#frmBusca2").submit();
	 }else {
		 if((op == 0)) {
			alert("E necessario escolher uma pretensao!");
			document.frmBusca2.opcoes2.focus();
			return false;
		 }else if(tipo == 0){
			alert("E necessario escolher um tipo de imovel!");
			document.frmBusca2.tipo2.focus();
			return false;
		 }else if(cidade == 0){
			alert("E necessario escolher uma cidade!");
			document.frmBusca2.cidade2.focus();
			return false;
		 }else {
			if(op==1) op="aluguel"; else op="venda";
			if(vali) vali = vali.replace(",","."); else vali=0;
			if(valf) valf = valf.replace(",","."); else valf=0;
			if(valf>0 && vali>0){
				valores="/"+vali+" a "+valf+"/";
				valstr = +vali+"a"+valf;
			}else if(valf>0){
				valores="/ate "+valf+"/";
				valstr = "ate"+valf;
			}else if(vali>0){
				valores="/a partir "+vali+"/";
				valstr = "apartir"+vali;
			}else {
				valores="/0/";
				valstr = "";
			}
			if($("#bairro2 option").filter(":selected").text().indexOf("TODOS")!=-1){
				 pagina="/imoveis"+tipoNom+op+cidade+valstr+".html";
				 document.frmBusca2.action = local+"index.php/imoveis/buscaRapida/"+op+"/"+cidade+valores+tipo+"/"+tipoNom+pagina;
				 $("#frmBusca2").submit();
			}else {
				var bairro="";	
				var arr = new Array;
				$("#bairro2 option").filter(":selected").each(function (){
					if(this.selected){
						if(this.value!=0) arr.push(this.value);
					}
				});
				for(i=0;i<arr.length;i++){
					if(i==(arr.length-1))
						bairro+=arr[i];
					else 
						bairro+=arr[i]+"-";
				}
				 pagina="/imoveis"+tipoNom+op+cidade+valstr+bairro+".html";
				 document.frmBusca2.action = local+"index.php/imoveis/buscaRapida/"+op+"/"+cidade+valores+tipo+"/"+tipoNom+"/"+bairro+pagina;
				 $("#frmBusca2").submit();
			}			
		 }
	 }
}

function buscaRapida4(local){
	op   = document.frmBusca3.opcoes3.value;
	vali = document.frmBusca3.valorini3.value;
	valf = document.frmBusca3.valorfim3.value;
	tipo = document.frmBusca3.tipo3.value;
	tipoNom = $("#tipo3 option").filter(":selected").text();
	cidade = document.frmBusca3.cidade3.value;
	bairro = document.frmBusca3.bairro3.value;

	if((op == 0)) {
		alert("E necessario escolher uma pretensao!");
		document.frmBusca3.opcoes3.focus();
		return false;
	}else if(tipo == 0){
		alert("E necessario escolher um tipo de imovel!");
		document.frmBusca3.tipo3.focus();
		return false;
	}else if(cidade == 0){
		alert("E necessario escolher uma cidade!");
		document.frmBusca3.cidade3.focus();
		return false;
	}else {
		if(op==1) op="aluguel"; else op="venda";
		if(vali) vali = vali.replace(",","."); else vali=0;
		if(valf) valf = valf.replace(",","."); else valf=0;
		if(valf>0 && vali>0){
			valores="/"+vali+" a "+valf+"/";
			valstr = +vali+"a"+valf;
		}else if(valf>0){
			valores="/ate "+valf+"/";
			valstr = "ate"+valf;
		}else if(vali>0){
			valores="/a partir "+vali+"/";
			valstr = "apartir"+vali;
		}else {
			valores="/0/";
			valstr = "";
		}
		if($("#bairro3 option").filter(":selected").text().indexOf("TODOS")!=-1){
			pagina="/imoveis"+tipoNom+op+cidade+valstr+".html";
			document.frmBusca3.action = local+"index.php/imoveis/buscaRapida/"+op+"/"+cidade+valores+tipo+"/"+tipoNom+pagina;
			$("#frmBusca3").submit();
		}else {
			var bairro="";	
			var arr = new Array;
			$("#bairro3 option").filter(":selected").each(function (){
				if(this.selected){
					if(this.value!=0) arr.push(this.value);
				}
			});
			for(i=0;i<arr.length;i++){
				if(i==(arr.length-1))
					bairro+=arr[i];
				else 
					bairro+=arr[i]+"-";
			}
			pagina="/imoveis"+tipoNom+op+cidade+valstr+bairro+".html";
			document.frmBusca3.action = local+"index.php/imoveis/buscaRapida/"+op+"/"+cidade+valores+tipo+"/"+tipoNom+"/"+bairro+pagina;
			$("#frmBusca3").submit();
		}			
	}
}

function validaLogin() {
  bot = document.getElementById("botao").value;
  login = document.getElementById("login").value;
  senha = document.getElementById("senha_usu").value;
  nSenha = fncReplace(senha,"'","");
  document.getElementById("senha_usu").value = nSenha;
  
  if(login.length == 14){
	  document.getElementById("botao").value = 1;
	  bot = document.getElementById("botao").value;
  }else if(login.length == 18){
  	  document.getElementById("botao").value = 2;
	  bot = document.getElementById("botao").value;
  }else {
  	 bot = 0;
  }
  
  if(login == "") {
     alert("� necess�rio preencher o campo Login do usu�rio");
	 return document.getElementById("login").focus();
  }else if(senha == "") {
     alert("� necess�rio preencher o campo Senha do usu�rio");
	 return document.getElementById("senha_usu").focus();
  }else if((bot == 1) && (login.length != 14)) {
     alert("CPF inv�lido");
	  return document.getElementById("login").focus();
  }else if((bot == 2) && (login.length != 18)) {
     alert("CNPJ inv�lido");
	  return document.getElementById("login").focus();
  }else if(!bot){
  	alert("Campo login inv�lido!");
	return document.getElementById("login").focus();
  }else {
  
     document.formLogin.submit();
  }
} 

function fncReplace(str,find,repla)
{
    var newStr = ""
    
    for(i=0;i<=str.length-1;i++)
    {
 strValue = str.substring(i,i+1)

 if (strValue == find)
     newStr += repla
 else
     newStr += str.substring(i,i+1)
    }
    return newStr
}

function validarVoto(local) {
   val = document.frmVoto.valor.value;
	if(!val) {
	   alert("Voc� deve escolher uma op��o antes de votar");
	}else {
		form = document.frmVoto;
		form_string = $("#frmVoto").serialize();		   
			$.ajax({
			 type: "POST",
			 url: local+"index.php/ajax/votoenquete",
			 dataType: "html",
			 data: form_string,
			 success: function(msg){
				 if(msg){		 
					 $("#local").html(msg);
														
				 }else{
					 alert("Erro na a��o, Favor tentar em alguns minutos!");						 
					 
				 }
			 },
			 beforeSend: function(){
					$("#local").html('<div style="width:100%; height:57px; clear:both;"></div><div style="float:left; margin-top:10px; margin-left:58px;"><img src="'+local+'application/images/load.gif"></div></div><div style="width:100%; height:30px; text-align:center; font-family:Arial, Helvetica, sans-serif; font-size:10px;color: #a52828; font-weight: bold;">Votando...</div>');
					
				}
		});	
	}
}

function voltar(origem){
	window.location = origem;
}

function retira_acentos(palavra) {  
   com_acento = '����������������������������������������������';  
   sem_acento = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC';  
   nova='';  
   for(i=0;i<palavra.length;i++) {  
      if (com_acento.search(palavra.substr(i,1))>=0) {  
        nova+=sem_acento.substr(com_acento.search(palavra.substr(i,1)),1);  
	  }else {  
		nova+=palavra.substr(i,1);  
	  }  
   }  
  return nova;  
}  
//busca avancada
function carDiv(id,barra,local,url,parametros){
	$('#mascara').append('<div id="mask" style="position:absolute;left:0;top:0;z-index:9000;background-color:#FFFFFF;display:none;"></div>');
	scrollP  = scrollPagina();
	tamanhoP = tamanhoPagina();
	var winH = tamanhoP[3];
	var winW = tamanhoP[2];
	var maskHeight = tamanhoP[1];
	var maskWidth  = tamanhoP[0];
	$('#mask').css({'width':maskWidth,'height':maskHeight});
	$('#mask').css({ opacity:0.5,background:"#000000" });
	$('#mask').css("z-index","900");
	$("#mask").fadeIn("fast");
	$("#"+id).css("z-index","1000");
	if(barra ==1){
		$("#"+id).css("top", (parseInt(scrollP[1] + (tamanhoP[3] / 10) - $("#"+id).width())));
	}else {
		$("#"+id).css("top", (parseInt(scrollP[1] + (tamanhoP[3] / 10))));
	}
	//$("#"+id).draggable({ handle: "#"+barra });
	$("#"+id).fadeIn("fast");
	
	//$("#"+id).css("display","block");
	$('#mask').click(function () {
		fechar(id);	
		$("#"+id).css("display","none");
	});	
	$("#"+id).html("<img src='"+local+"application/images/loading.gif' /><label style='font-family:Arial, Helvetica, sans-serif;color:#FF0000;font-size:12px;font-weight:bold'>Carregando...</label>");
	ajaxHtml(id,url,parametros);
	
}

function fechar(id){
	$("#"+id).css("display","none");
	$("#mask").remove();
}


function carDivMap(id,barra,local,url,parametros){
	
	$('#mascara').append('<div id="mask" style="position:absolute;left:0;top:0;z-index:9000;background-color:#FFFFFF;display:none;"></div>');
	scrollP  = scrollPagina();
	tamanhoP = tamanhoPagina();
	var winH = tamanhoP[3];
	var winW = tamanhoP[2];
	var maskHeight = tamanhoP[1];
	var maskWidth  = tamanhoP[0];
	$('#mask').css({'width':maskWidth,'height':maskHeight});
	$('#mask').css({ opacity:0.5,background:"#000000" });
	$('#mask').css("z-index","900");
	$("#mask").fadeIn("fast");
	$("#"+id).css("z-index","1000");
	if(barra ==1){
		$("#"+id).css("top", (parseInt(scrollP[1] + (tamanhoP[3] / 10) - $("#"+id).width())));
	}else {
		$("#"+id).css("top", (parseInt(scrollP[1] + (tamanhoP[3] / 10))));
	}
	//$("#"+id).draggable({ handle: "#"+barra });
	$("#"+id).fadeIn("fast");
	
	//$("#"+id).css("display","block");
	$('#mask').click(function () {
		fechar(id);	
		$("#"+id).css("display","none");
	});	
	$("#"+id).html("<img src='"+local+"application/images/loading.gif' /><label style='font-family:Arial, Helvetica, sans-serif;color:#FF0000;font-size:12px;font-weight:bold'>Carregando...</label>");
	ajaxHtml(id,url,parametros);
	
}

function fechar(id){
	$("#"+id).css("display","none");
	$("#mask").remove();
}

function listarCidadesMap(opcao,local){	
	if(opcao>0){
	campo_select = document.getElementById("cidadeMap");
	$.ajax({
					 type: "POST",
					 url: local+"index.php/ajax/consultarCidade",
					 dataType: "html",
					 data: "opcao="+opcao,
					 success: function(msg){
						 if(msg){
							document.getElementById("bairroMap").length = 0;								
							document.getElementById("bairroMap").options[0] = new Option("Cidade","0");
							campo_select.options.length = 0;
							results = msg.split(",");	 
							for( i = 0; i < results.length; i++ ){ 
							  string = results[i].split( "|" );
							  campo_select.options[i] = new Option( string[0], string[1] );	
							}
							mudaCampos(document.frmBuscaMap,0);
							carregarTipoDeImoveisMap(local);		 									 
				
						 }else{
							alert("Erro na aplica��o, favor tentar em alguns minutos!");
							document.getElementById("bairroMap").length = 0;								
							document.getElementById("bairroMap").options[0] = new Option("Cidade","0");
							campo_select.options.length = 0;
							document.getElementById("cidadeMap").options[0] = new Option("Cidade","0");
							mudaCampos(document.frmBuscaMap,1);			 												
										 
						 }
					 },
					 beforeSend: function(){												
						document.getElementById("cidadeMap").length = 0;
						document.getElementById("cidadeMap").options[0] = new Option("carregando...","0");
						document.getElementById("bairroMap").length = 0;
						document.getElementById("bairroMap").options[0] = new Option("carregando...","0");
						mudaCampos(document.frmBuscaMap,0);													
										
					 }
	});
  }
}

function carregarTipoDeImoveisMap(local){
  opcao  = document.getElementById('opcoesMap').value;
  campo_select = document.getElementById("tipoMap");	
  $.ajax({
		 type: "POST",
		 url:  local+"index.php/ajax/consultarTipo",
		 dataType: "html",
		 data: "opcao="+opcao,
		 success: function(msg){
			 if(msg){																	
				campo_select.options.length = 0;
				results = msg.split(",");	 
				for( i = 0; i < results.length; i++ ){ 
				  string = results[i].split( "|" );
				  campo_select.options[i] = new Option( string[0], string[1] );
				}
				mudaCampos(document.frmBuscaMap,1); 	 	 									 
	
			 }else{
				alert("Erro na aplica��o, favor tentar em alguns minutos!");																		
				campo_select.options.length = 0;
				document.getElementById("tipoMap").options[0] = new Option("Escolha uma op��o","0");
				mudaCampos(document.frmBuscaMap,1);			 												
							 
			 }
		 },
		 beforeSend: function(){
			campo_select.options.length = 0;												
			document.getElementById("tipoMap").options[0] = new Option("Carregando...","0");							
			mudaCampos(document.frmBuscaMap,0);													
							
		 }
	});			
	
}

function carregarBairrosMap(local){
  opcao  = document.getElementById('opcoesMap').value;
  cidade = document.getElementById('cidadeMap').value;
  if(opcao>0 && opcao<3 && cidade){
  	campo_select = document.getElementById("bairroMap");		
	$.ajax({
					type: "POST",
					url:  local+"index.php/ajax/consultarBairros",
					dataType: "html",
					 data: "opcao="+opcao+"&cidade="+cidade,
					 success: function(msg){
						 if(msg){																	
							campo_select.options.length = 0;
							results = msg.split(",");	 
							for( i = 0; i < results.length; i++ ){ 
							  string = results[i].split( "|" );
							  campo_select.options[i] = new Option( string[0], string[1] );
							  if(i==0){
							  	campo_select.options[i].selected = true;
							  }
							}
							mudaCampos(document.frmBuscaMap,1); 	 	 									 
				
						 }else{
							alert("Erro na aplica��o, favor tentar em alguns minutos!");																		
							campo_select.options.length = 0;
							campo_select.options[0] = new Option("Cidade","0");
							mudaCampos(document.frmBuscaMap,1);			 												
										 
						 }
					 },
					 beforeSend: function(){
						campo_select.options.length = 0;												
						campo_select.options[0] = new Option("Carregando...","0");							
						mudaCampos(document.frmBuscaMap,0);													
										
					 }
			});	
	}		
}


function buscaRapida2Map(){
	valBusca = document.frmBuscaMap.valorBusMap.value;
	 op = document.frmBuscaMap.opcoesMap.value;
	 vali = document.frmBuscaMap.valoriniMap.value;
	 valf = document.frmBuscaMap.valorfimMap.value;
	 tipo = document.frmBuscaMap.tipoMap.value;
	 tipoNom = $("#tipoMap option").filter(":selected").text();
	 //retira a barra inversa do tipo
	 tipoNom = tipoNom.replace("/","|");
	 codigo = document.frmBuscaMap.codigoMap.value;
	 cidade = document.frmBuscaMap.cidadeMap.value;
	 bairro = document.frmBuscaMap.bairroMap.value;
	 finalidade = document.frmBuscaMap.finalidade.value;
	 if(codigo && op>0){
		 if(op==1) op="aluguel"; else op="venda";
		 loadDadosImoveis(localGer+"index.php/ajax/buscanoMapa/"+op+"/"+codigo+"/imoveis+"+op+"+codigo+"+codigo+".html");
	 }else {
		 if((op == 0)) {
			alert("� necess�rio escolher uma pretens�o!");
			document.frmBuscaMap.opcoesMap.focus();
			return false;
		 }else if(tipo == 0){
			alert("� necess�rio escolher um tipo de imovel!");
			document.frmBuscaMap.tipoMap.focus();
			return false;
		 }else if(cidade == 0){
			alert("� necess�rio escolher uma cidade!");
			document.frmBuscaMap.cidadeMap.focus();
			return false;
		 }else {
			if(op==1) op="aluguel"; else op="venda";
			if(vali) vali = vali.replace(",","."); else vali=0;
			if(valf) valf = valf.replace(",","."); else valf=0;
			if(valf>0 && vali>0){
				valores="/"+vali+" a "+valf+"/";
				valstr = "+"+vali+"+a+"+valf;
			}else if(valf>0){
				valores="/ate "+valf+"/";
				valstr = "+ate+"+valf;
			}else if(vali>0){
				valores="/a partir "+vali+"/";
				valstr = "+a+partir+"+vali;
			}else {
				valores="/0/";
				valstr = "";
			}
			if($("#bairroMap option").filter(":selected").text().indexOf("TODOS")!=-1){
				
				 pagina="/imoveis+"+tipoNom+"+"+op+"+"+cidade+valstr+".html";
				 loadDadosImoveis(localGer+"index.php/ajax/buscanoMapa/"+op+"/"+cidade+valores+tipo+"/"+tipoNom+"/"+valBusca+"/"+finalidade+pagina);
			}else {
				var bairro="";	
				var arr = new Array;
				$("#bairroMap option").filter(":selected").each(function (){
					if(this.selected){
						if(this.value!=0) arr.push(this.value);
					}
				});
				for(i=0;i<arr.length;i++){
					if(i==(arr.length-1))
						bairro+=arr[i];
					else 
						bairro+=arr[i]+"-";
				}
				 pagina="/imoveis+"+tipoNom+"+"+op+"+"+cidade+valstr+"+"+bairro+".html";
				 loadDadosImoveis(localGer+"index.php/ajax/buscanoMapa/"+op+"/"+cidade+valores+tipo+"/"+tipoNom+"/"+bairro+"/"+valBusca+"/"+finalidade+pagina);
			}			
		 }
	 }
}

function validaInteresse() {
    nome  = 	$("#nome");
    //email =	    $("#email1");
    fone =      $("#fone");
    //celular =   $("#celular");
    mensagem =  $("#mensagem");

    if(!nome.val()) {
        alert("É necessário preencher o campo Nome");
        nome.focus();
        return false;
    /*}else if(!email.val()) {
        alert("É necessário preencher o campo Email");
        email.focus();
        return false;*/
    }else if(!fone.val()) {
        alert("É necessário preencher o campo Fone");
        fone.focus();
        return false;
    /*}else if(!celular.val()) {
        alert("É necessário preencher o campo Celular");
        celular.focus();
        return false;*/
    }else if(!mensagem.val()) {
        alert("É necessário preencher o campo Mensagem");
        mensagem.focus();
        return false;
    }
    document.frmInteresse.submit();
}