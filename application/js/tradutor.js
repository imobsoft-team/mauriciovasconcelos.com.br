function ChangeLang(a) {
	var b, elemento = "";
	if (document.createEvent) {
		var c = document.createEvent("HTMLEvents");
		c.initEvent("click", true, true)
	}
	if (a == 'pt') {
		elemento = $(".goog-te-banner-frame:eq(0)").contents().find("button[id*='restore']");
		$("#bairroBuscaLoc").css("fontSize", "16px");
		$("#bairroBuscaVen").css("fontSize", "16px");
		$("#traduImovel").css("fontSize", "26px");
		$("#traduArea").css("fontSize", "18px");
		$("#traduRestrita").css("fontSize", "18px");
		$("#traduBusca").css("fontSize", "16px");
		$("#traduAvancada").css("fontSize", "16px");
	} else {
		switch (a) {
		case 'de':
			b = "alem";
			break;
		case 'es':
			b = "espanhol";
			$("#bairroBuscaLoc").css("fontSize", "16px");
			$("#bairroBuscaVen").css("fontSize", "16px");
			$("#traduImovel").css("fontSize", "20px");
			$("#traduArea").css("fontSize", "15px");
			$("#traduRestrita").css("fontSize", "14px");
			$("#traduBusca").css("fontSize", "14px");
			$("#traduAvancada").css("fontSize", "14px");
			break;
		case 'fr':
			b = "fran";
			break;
		case 'en':
			b = "ing";
			$("#bairroBuscaLoc").css("fontSize", "10px");
			$("#bairroBuscaVen").css("fontSize", "10px");
			$("#traduImovel").css("fontSize", "24px");
			$("#traduArea").css("fontSize", "15px");
			$("#traduRestrita").css("fontSize", "15px");
			$("#traduBusca").css("fontSize", "16px");
			$("#traduAvancada").css("fontSize", "15px");
			break;
		case 'it':
			b = "italiano";
			break
		}
		elemento = $(".goog-te-menu-frame:eq(0)").contents().find("span:contains('" + b + "')");
	}
	if (elemento.length > 0) {
		if (document.createEvent) {
			elemento[0].dispatchEvent(c)
		} else {
			elemento[0].click()
		}
	}
}
function googleTranslateElementInit() {
	new google.translate.TranslateElement({
		pageLanguage: 'pt',
		autoDisplay: false,
		includedLanguages: 'de,es,fr,en,it',
		layout: google.translate.TranslateElement.InlineLayout.SIMPLE
	},
	'google_translate_element');
}