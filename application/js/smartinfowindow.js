function SmartInfoWindow(t) {
    google.maps.OverlayView.call(this), this.latlng_ = t.position, this.content_ = t.content, this.map_ = t.map, this.height_ = 205, this.width_ = 224, this.size_ = new google.maps.Size(this.height_, this.width_), this.offsetVertical_ = -this.height_, this.offsetHorizontal_ = 0, this.panned_ = !1, this.setMap(this.map_);
    var i = this;
    google.maps.event.addListener(this.map_, "bounds_changed", function() {
        i.draw()
    })
}
SmartInfoWindow.prototype = new google.maps.OverlayView, SmartInfoWindow.prototype.onRemove = function() {
    this.div_ && (this.div_.parentNode.removeChild(this.div_), this.div_ = null)
}, SmartInfoWindow.prototype.onAdd = function() {
    this.createElement()
}, SmartInfoWindow.prototype.draw = function() {
    if (this.getProjection()) {
        var t = this.getProjection().fromLatLngToDivPixel(this.latlng_),
            i = this.getProjection().fromLatLngToDivPixel(this.map_.getCenter()),
            e = new google.maps.Point(this.map_.getDiv().offsetWidth / 2, this.map_.getDiv().offsetHeight / 2),
            n = -i.x + e.x,
            o = -i.y + e.y;
        if (t) {
            var a = this.getBestAlignment(),
                s = 0,
                h = 0,
                l = 0;
            switch (a) {
                case SmartInfoWindow.Align.ABOVE:
                    this.width_ = 224, this.height_ = 205, image = "infobox_above.png", this.offsetX_ = -(this.width_ / 2), this.offsetY_ = -this.height_;
                    break;
                case SmartInfoWindow.Align.BELOW:
                    this.width_ = 224, this.height_ = 205, image = "infobox_below.png", this.offsetX_ = -(this.width_ / 2), this.offsetY_ = -15, s = 0;
                    break;
                case SmartInfoWindow.Align.LEFT:
                    this.width_ = 224, this.height_ = 205, image = "infobox_left.png", this.offsetX_ = -this.width_ - 12, this.offsetY_ = -(this.height_ / 2) - 12, l = 0;
                    break;
                case SmartInfoWindow.Align.RIGHT:
                    image = "infobox_right.png", this.width_ = 224, this.height_ = 205, this.offsetX_ = 12, this.offsetY_ = -(this.height_ / 2) - 12, h = 0, l = 0
            }
            this.div_.style.width = this.width_ + "px", this.div_.style.height = this.height_ + "px", this.div_.style.left = t.x + this.offsetX_ + n + "px", this.div_.style.top = t.y + this.offsetY_ + o + "px", this.div_.style.background = 'url("' + localGer + "application/images/maps/" + image + '")', this.div_.style.display = "block", this.wrapperDiv_.style.width = this.width_ - l + "px", this.wrapperDiv_.style.height = this.height_ + "px", this.wrapperDiv_.style.marginTop = s + "px", this.wrapperDiv_.style.marginLeft = h + "px", this.wrapperDiv_.style.overflow = "hidden", this.panned_ || (this.panned_ = !0, this.maybePanMap())
        }
    }
}, SmartInfoWindow.prototype.createElement = function() {
    function t(t) {
        return function() {
            t.setMap(null)
        }
    }
    var i = this.getPanes(),
        e = this.div_;
    if (e) e.parentNode != i.floatPane && (e.parentNode.removeChild(e), i.floatPane.appendChild(e));
    else {
        e = this.div_ = document.createElement("div"), e.style.border = "0px none", e.style.position = "absolute", e.style.overflow = "hidden", e.style.zIndex = 2545217445;
        var n = this.wrapperDiv_ = document.createElement("div"),
            o = document.createElement("div");
        "string" == typeof this.content_ ? o.innerHTML = this.content_ : o.appendChild(this.content_);
        var a = document.createElement("div");
        a.style.textAlign = "left", a.style.position = "absolute";
        var s = document.createElement("img");
        s.src = localGer + "application/images/maps/fechar.gif", s.style.width = "14px", s.style.height = "13px", s.style.marginLeft = "20px", s.style.marginRight = "0px", s.style.marginTop = "20px", s.style.marginBottom = "0px", s.style.cursor = "pointer", a.appendChild(s), google.maps.event.addDomListener(s, "click", t(this)), n.appendChild(a), n.appendChild(o), e.appendChild(n), e.style.display = "none", document.getElementById("divMap").appendChild(e)
    }
}, SmartInfoWindow.mouseFilter = function(t) {
    t.returnValue = "true", t.handled = !0
}, SmartInfoWindow.prototype.close = function() {
    this.setMap(null)
}, SmartInfoWindow.prototype.maybePanMap = function() {
    var t = this.map_,
        i = this.getProjection(),
        e = t.getBounds();
    if (e) {
        var n = this.width_,
            o = this.height_,
            a = this.offsetX_,
            s = this.offsetY_,
            h = i.fromLatLngToDivPixel(this.latlng_),
            l = new google.maps.Point(h.x + a + 20, h.y + s + o),
            r = new google.maps.Point(h.x + a + n, h.y + s),
            g = i.fromDivPixelToLatLng(l),
            d = i.fromDivPixelToLatLng(r);
        t.getBounds().contains(d) && t.getBounds().contains(g) || t.panToBounds(new google.maps.LatLngBounds(g, d))
    }
}, SmartInfoWindow.Align = {
    ABOVE: 0,
    LEFT: 1,
    RIGHT: 2,
    BELOW: 3
}, SmartInfoWindow.prototype.getBestAlignment = function() {
    var t = SmartInfoWindow.Align.LEFT,
        i = 0;
    for (var e in SmartInfoWindow.Align) {
        var e = SmartInfoWindow.Align[e],
            n = this.getPanValue(e);
        n > i && (i = n, t = e)
    }
    return t
}, SmartInfoWindow.prototype.getPanValue = function(t) {
    var i, e = (new google.maps.Size(this.map_.getDiv().offsetWidth, this.map_.getDiv().offsetHeight), this.map_.getBounds());
    switch (t) {
        case SmartInfoWindow.Align.ABOVE:
            i = new google.maps.LatLng(e.getNorthEast().lat(), this.latlng_.lng());
            break;
        case SmartInfoWindow.Align.BELOW:
            i = new google.maps.LatLng(e.getSouthWest().lat(), this.latlng_.lng());
            break;
        case SmartInfoWindow.Align.RIGHT:
            i = new google.maps.LatLng(this.latlng_.lat(), e.getNorthEast().lng());
            break;
        case SmartInfoWindow.Align.LEFT:
            i = new google.maps.LatLng(this.latlng_.lat(), e.getSouthWest().lng())
    }
    var n = SmartInfoWindow.distHaversine(this.latlng_.lat(), this.latlng_.lng(), i.lat(), i.lng());
    return n
}, SmartInfoWindow.toRad = function(t) {
    return t * Math.PI / 180
}, SmartInfoWindow.distHaversine = function(t, i, e, n) {
    var o = 6371,
        a = SmartInfoWindow.toRad(e - t),
        s = SmartInfoWindow.toRad(n - i);
    t = SmartInfoWindow.toRad(t), e = SmartInfoWindow.toRad(e);
    var h = Math.sin(a / 2) * Math.sin(a / 2) + Math.cos(t) * Math.cos(e) * Math.sin(s / 2) * Math.sin(s / 2),
        l = 2 * Math.atan2(Math.sqrt(h), Math.sqrt(1 - h)),
        r = o * l;
    return r
};