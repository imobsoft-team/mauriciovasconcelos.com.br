var address = '-3.716521,-38.551369';
var briba_icon_image=""+localGer+"application/images/maps/gmaps_marcador.png";

var locations = new Array();
var locationsDataInfo = new Array();

var map;
var markers = [];
var geocoder;

var AddressLatLng;
var AddressConteudo="";
var MarkersCount=0;
var MarkersSucesso=true;
var interval=null;
errosMap = 0;
chamaAlerta=true;
//inicia o mapa
function initializeMap()
{
	var myLatlng = new google.maps.LatLng(-3.716521,-38.551369);
	var myOptions = {
		zoom: 12,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	map = new google.maps.Map(document.getElementById("divMap"), myOptions);
}
//busca os im�veis no banco de dados
function loadDadosImoveis(urlAjax)
{
	
	
	cidade="";
	errosMap=0;
	$("#msgCli").html("");
	locations = new Array();
	locationsDataInfo = new Array();
	MarkersSucesso=true;
	$("#loaderData").css("display","block");
	$("#ProgressBar").css("display","block");
	var url = "";
	$.getJSON(urlAjax, {acao:"envMenJSON",variavel:"teste" } , function(json){ 
		if(json.msg == '1'){
			$.each(json.men,function (i,id){
				if(id){
					string = id.split("|");
					locationsDataInfo.push("<div class=boxInfoWindow ><img style='height: 1px;' src='"+localGer+"application/images/maps/spacer.gif' width=156 height=1 ><br><img class='imgImovel' src='"+string[0]+"' width=70 height=70 align=left />C&oacute;digo: <b>"+string[1]+"</b><br><b>"+string[2]+"</b><br><b>"+string[3]+"</b><br>"+string[4]+"<div style='padding:3px;background-color:#EEEEEE;width:156px;height:35px;overflow:hidden;'>"+string[5]+"</div>"+string[6]+"<a href='"+string[7].replace("_","|")+"'><img class='imgDetalhes' src='"+localGer+"application/images/maps/bt_detalhes.png' border=0 /></a></div>");
					locations.push(string[8]);
					if(!cidade)
						cidade = string[9];
				}					
			});
			$("#loaderData").css("display","none");		
			
			
			buscanoMapa(cidade);	
		}else {
			$("#ProgressBar").css("display","none");
			//centralizaMapa();
			initializeMap();
			alert("Sem resultado para a pesquisa!");
		}
	});
}

//centraliza o mapa
function centralizaMapa(){
	map.setCenter(markers[markers.length-1].getPosition());
}
//inicializa pesquisa no mapa
function buscanoMapa(cidade)
{

	MarkersCount = 0;
	chamaAlerta=true;
	markers = [];
	var myLatlng = new google.maps.LatLng(-3.716521,-38.551369);
	var myOptions = {
		zoom: 12,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	map = new google.maps.Map(document.getElementById("divMap"), myOptions);
	geocoder = new google.maps.Geocoder();
	//centraliza o mapa na cidade
	geocoder.geocode( { 'address': cidade}, function(results2, status2) {
      if (status2 == google.maps.GeocoderStatus.OK) {
        map.setCenter(results2[0].geometry.location);
      }
    });
	$("#ProgressBar").css("display","block");
	interval = setInterval("setaMarcadores()",300); 
}
//seta os marcadores para a contagem
function setaMarcadores()
{    
	endereco = locations[MarkersCount];
	conteudo = locationsDataInfo[MarkersCount];
	
	
	AddressConteudo = conteudo;
	if(endereco){
		geocoder.geocode({'address': endereco,'partialmatch': true}, GeocodeResult);
	}else {
		clearInterval(interval);
		$("#ProgressBar").css("display","none");
		if(!MarkersSucesso){
			//alert("Alguns im�veis n�o poderam ser localizados no mapa!");
			if(errosMap>=MarkersCount){
				$("#msgCli").html("nenhum imóvel pode ser localizado no mapa!");
			}else {
				$("#msgCli").html("Alguns imóveis não poderam ser localizados no mapa!");
			}
		}
	}
}
//trata o resultado no mapa
function GeocodeResult(results, status)
{	
	if (status == 'OK' && results.length > 0)
	{
		var content = AddressConteudo;
		//map.fitBounds(results[0].geometry.viewport);
		AddressLatLng = results[0].geometry.location;		
		var marker = new google.maps.Marker({
			position: AddressLatLng, 
			map: map,
			icon: briba_icon_image
		}); 
		markers.push(marker);
		var infowindow = new google.maps.InfoWindow(
		  { content: content,
			size: new google.maps.Size(50,50)
		});
		google.maps.event.addListener(marker, 'click', function(e) {
			var infobox = new SmartInfoWindow({position: marker.getPosition(), map: map, content: content});
		});
	}else{
		MarkersSucesso=false;
		errosMap+=1;
	}
	if(MarkersCount<(locations.length-1))
	{
	    
		MarkersCount++;
		var PercentProgress = Math.round((100/locations.length)*MarkersCount);	
		$('#txtInfo').html(PercentProgress+"% &nbsp;-&nbsp; "+MarkersCount+' de '+locations.length);
		$('#txtBarInfo').html(PercentProgress+"% &nbsp;-&nbsp; "+MarkersCount+' de '+locations.length);
		$('#barinfo').css("width",PercentProgress.toString()+"%");
	}else if(MarkersCount>=(locations.length-2)){
		clearInterval(interval);
		$("#ProgressBar").css("display","none");
		if(!MarkersSucesso && chamaAlerta){
			chamaAlerta=false;
			if(errosMap>=MarkersCount){
				alert("nenhum imóvel pode ser localizado no mapa!");
				$("#msgCli").html("nenhum imóvel pode ser localizado no mapa!");
			}else {
				alert("Alguns imóveis não puderam ser localizados no mapa!");
				$("#msgCli").html("Alguns imóveis não puderam ser localizados no mapa!");
			}
		}
	}
	
}