function getTipos (local, modalidade, finalidade) {
    $.ajax({
        url:local+'index.php/ajax/getTipos/'+modalidade+'/'+finalidade,
        type: 'POST',
        data: modalidade,
        dataType: 'json',
        beforeSend: function () {
        //$('body').addClass("loading");
        },
        complete: function () {
        //$('body').removeClass("loading");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Algo de errado ocorreu. Favor verifique no console do navegador.');
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        },
        success: function(data){
			
            if (finalidade == 1) {
				
				for (var i=0; i<data.length; i++) {
					$('#tipo_destaque_residencial').append($('<option>', {
							value: data[i].tim_id,
							text : data[i].tim_des
                    }));
				}
				
            } else {
				
				for (var i=0; i<data.length; i++) {
					$('#tipo_destaque_comercial').append($('<option>', {
							value: data[i].tim_id,
							text : data[i].tim_des
                    }));
				}

            }
        }
    })
}

function getDestaquesTipo (local, tim_isn, finalidade) {
    $.ajax({
        url:local+'index.php/ajax/getDestaquesTipo/'+tim_isn+'/'+finalidade,
        type: 'POST',
        data: tim_isn,
        dataType: 'html',
        beforeSend: function () {
            $('body').addClass("loading");
        },
        complete: function () {
            $('body').removeClass("loading");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Algo de errado ocorreu. Favor verifique no console do navegador.');
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        },
        success: function(data){
            $('#carousel-'+finalidade).html('<br>');
            $('#carousel-'+finalidade).html(data);
        }
    })
}

function favoritarImovel (local, imo_isn) {
    $.ajax({
        url:local+'index.php/ajax/favoritarImovel/'+imo_isn,
        type: 'POST',
        data: imo_isn,
        dataType: 'html',
        // beforeSend: function () {
        // $('body').addClass("loading");
        // },
        // complete: function () {
        // $('body').removeClass("loading");
        // },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Algo de errado ocorreu. Favor verifique no console do navegador.');
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        },
        success: function(data){
            //console.log(data);
            $().toastmessage('showToast', {
                text     : data,
                sticky   : false,
                type     : 'success',
                position : 'top-right',
                inEffectDuration:  600,
                stayTime:         3000,
            });
        }
    })
}

function favoritos (local) {
    $.ajax({
        url:local+'index.php/ajax/favoritos/',
        type: 'POST',
        data: local,
        dataType: 'html',
        // beforeSend: function () {
        // $('body').addClass("loading");
        // },
        // complete: function () {
        // $('body').removeClass("loading");
        // },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Algo de errado ocorreu. Favor verifique no console do navegador.');
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        },
        success: function(data){
            //console.log(data);
            //$('#listaFavoritos').html('<div></div>');
            $('#listaFavoritos').html(data);
        }
    })
}

function excluirFavorito (local, imo_isn) {
    $.ajax({
        url:local+'index.php/ajax/excluirFavorito/'+imo_isn,
        type: 'POST',
        data: imo_isn,
        dataType: 'html',
        // beforeSend: function () {
        // $('body').addClass("loading");
        // },
        // complete: function () {
        // $('body').removeClass("loading");
        // },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Algo de errado ocorreu. Favor verifique no console do navegador.');
            console.log('jqXHR:');
            console.log(jqXHR);
            console.log('textStatus:');
            console.log(textStatus);
            console.log('errorThrown:');
            console.log(errorThrown);
        },
        success: function(data){
            //console.log(data);
            $().toastmessage('showToast', {
                text     : 'Favorito Excluído',
                sticky   : false,
                type     : 'notice',
                position : 'top-right',
                inEffectDuration:  600,
                stayTime:         3000,
            });
            favoritos(local);
        }
    })
}