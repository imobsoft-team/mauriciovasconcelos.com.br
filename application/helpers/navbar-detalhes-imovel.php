  <nav class="pushy pushy-left">
      <ul>              
          <li class="text-right imob-close-menu"><a href="#" ><i class="fa fa-times" aria-hidden="true" title="Sair"></i></a></li>
          <li><a href="<?php echo $local;?>index.php/internas/empresa">EMPRESA</a></li>
          <li><a href="<?php echo $local;?>index.php/imoveis/buscaRapida/aluguel/0/0/0/0/0/imoveis.html">ALUGAR</a></li> 
          <li><a href="<?php echo $local;?>index.php/imoveis/buscaRapida/venda/0/0/0/0/0/imoveis.html">COMPRAR</a></li>  
          <li><a href="<?php echo $local;?>index.php/internas/servicos">SERVIÇOS</a></li>  
          <li><a href="<?php echo $local;?>index.php/internas/contato">FALE CONOSCO</a></li>                                      
      </ul>
      <!--ul class="imob-idioma-pushy">               
          <li><a><img src="<?php echo $imagePath; ?>bra.png" alt="bandeira brasil" title="Traduzir para português"></a></li>           
          <li><a><img src="<?php echo $imagePath; ?>us.png" alt="bandeira USA" title="Translate to English"></a></li>                 
      </ul-->
  </nav>

	<div class="site-overlay"></div><!-- menu pushy -->
    <nav class="navbar navbar-default">
      <div class="container" style="padding-left: 0; padding-right: 0;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header col-md-5">          
          <a class="navbar-brand" href="<?php echo $local;?>">
            <img class="img-responsive" src="<?php echo $imagePath; ?>logoNavbar.png" alt="lotipo DMV Imóveis" title="DMV Imóveis">
          </a>
          <ul class="nav navbar-nav imob-navbar-det-imovel">
            <li><a class="imob-btn-menu-pushy col-xs-2"><i class="fa fa-bars" aria-hidden="true" title="Menu"></i></a></li>
            <li>
            	<a class="col-xs-2 imob-btn-collapse" data-toggle="collapse" data-target="#imob-collapse" aria-expanded="false"><i class="fa fa-search" aria-hidden="true" title="Realizar busca de imóvel"></i></a>
            </li>
            <!--li><a class="col-xs-2 hidden-xs" href=""><img src="<?php echo $imagePath; ?>icon-headset.png" alt="Iniciar chat" title="Iniciar chat"></a></li-->
            <!--li><a class="col-xs-2" href=""><i class="fa fa-print" aria-hidden="true" title="Imprimir resultado"></i></a></li-->
          </ul>
        </div>
        <div class="imob-navbar-det-contato col-md-7 hidden-sm hidden-xs">
           <ul class="nav navbar-nav pull-right">
            <li class="nav-det-whats"><i class="fa fa-whatsapp" aria-hidden="true"></i> +55 85 98204-0055</li>
            <li class="nav-det-phone">
            	<i class="fa fa-phone" aria-hidden="true" style="font-size:0.9em;"></i><sup style="font-size: 0.5em; top:-12px; right:-6px;">(85)</sup> 3205-6000
           	</li>
           </ul>
        </div>        
      </div>
    </nav><!-- /.navbar -->

    