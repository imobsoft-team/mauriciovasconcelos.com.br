<?php
/**
 * Created by PhpStorm.
 * User: Diego Pedrosa
 * Date: 05/09/2015
 * Time: 09:19
 */
?>

<form name="frmBusca" id="frmBusca" method="post" action="<?php echo $local?>index.php/imoveis/buscaimovel">
    <input type="hidden" name="numresult" id="numresult" value="5" />
    <table>
        <tr>
            <td><label>Código:</label></td>
            <td><input type="text" name="codigo" id="codigo" onKeyPress="this.value = this.value.toUpperCase()"/></td>
        </tr>
        <tr>
            <td>
                <div style="display:none">
                    <label style="margin-right:6px"><?php echo $tradutor['buscaRapida'][9]?></label>
                    <select name="opcoes" id="opcoes" onChange="listarCidades(this.value,'<?php echo $local?>')">
                        <option value="0" selected="selected"><?php echo $tradutor['buscaRapida'][8]?></option>
                        <!--<option value="1"><?php echo $tradutor['buscaRapida'][6]?></option>-->
                        <option value="2"><?php echo $tradutor['buscaRapida'][7]?></option>
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <td><label>Tipo:</label></td>
            <td>
                <select name="tipo" id="tipo">
                    <option value="0" selected="selected">
                        <?php echo utf8_encode($tradutor['buscaRapida'][9])?>
                    </option>
                </select>
            </td>
        </tr>
        <tr>
            <td><label>Cidade:</label></td>
            <td>
                <select name="cidade" id="cidade" onChange="javascript: carregarBairros('<?php echo $local?>')">
                    <option value="0" selected="selected"><?php echo utf8_encode($tradutor['buscaRapida'][9])?></option>
                </select>
            </td>
        </tr>
        <tr>
            <td><label>Valor:</label></td>
            <td>
                <div><input type="text" style="width:75px; float:left;"  id="valorini" name="valorini"  size="5" onKeyPress="javascript: return validaNumeros2(this,event)"/></div>
                <div style="float:left;">/</div>
                <div><input type="text" style="width:75px; float:left;" id="valorfim" name="valorfim"  size="5" onKeyPress="javascript: return validaNumeros2(this,event)"/></div>
            </td>
        </tr>
        <tr>
            <td><label id="bairroBuscaVen">Bairro:</label></td>
            <td>
                <select style="height:75px;" name="bairro[]" id="bairro" multiple="multiple" title="Para selecionar mais de um bairro: pressione a tecla CTRL e selecione com o mouse" >
                    <option>Selecione</option>
                </select>
            </td>
        </tr>
    </table>
    <div class="btn" style="margin-left: 65px; margin-top:-20px; width:50px; height: 25px; cursor:pointer;" onClick="buscaRapida2('<?php echo $local?>')">Buscar</div>
</form>