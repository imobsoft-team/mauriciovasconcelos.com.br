<div id="carousel-venda" data-type="multi" data-interval="5000" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">

        <?php $i=0; foreach ($destaquesCom as $destaque) { ?>
            <div class="carousel-item <?php echo ($i==0?'active':''); $i++;?>">
                <div class="item-card" style="height:470px; width:358px;">
                    <div class="card" style="height:440px; width:358px;">
                        <ul class="nav">
                            <li class="nav-item">
                                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $local?>index.php/imoveis/detalhes/aluguel/<?php echo utf8_encode($destaque['tim_des'])?>/<?php echo $destaque['imo_isn']?>" class="fb-xfbml-parse-ignore fb-share-button nav-link" data-href="<?php echo $local?>index.php/imoveis/detalhes/aluguel/<?php echo $destaque['tim_des']?>/<?php echo $destaque['imo_isn']?>" data-layout="button_count" data-mobile_iframe="true" title="Compartilhar"><i class="fa fa-facebook"></i></a>
                            </li>
                            <!--<li class="nav-item">
                                <a href="" class="nav-link" title="Localização"><i class="fa fa-map-marker"></i></a>
                            </li>-->
                            <li class="nav-item">
                                <a href="<?php echo $local.'index.php/imoveis/detalhes/aluguel/'.$destaque['tim_des'].'/'.$destaque['imo_isn']?>" class="nav-link" title="Detalhes"><i class="fa fa-eye"></i></a>
                            </li>
                        </ul>
                        <img class="card-img-top img-fluid" src="<?php echo $destaque['foto']?>" style="height: 223px; width: 355px;" alt="Imóvel para locação">
                        <div class="card-block">
                            <h4 class="card-title"><?php echo utf8_encode($destaque['tim_des'])?></h4>
                            <p class="card-text">
                                <span class="endereco"><?php echo utf8_encode($destaque['imo_des_end']. ' - ' .$destaque['imo_des_bai'])?></span><br>
                                <span class="text-destaque"><?php echo utf8_encode(substr($destaque['imo_des_txt_dest'],0,50)).'...'?></span><br>
                                <span class="valor">R$ <?php echo number_format($destaque['imo_val_alu'],2 ,',' ,'.')?></span>
                            </p>
                        </div>
                        <div class="card-footer">
                            <span><i class="fa fa-hotel"></i><?php echo $destaque['caracs'][0]['qtd_quartos'];?> quartos</span>
                            <span><i class="fa fa-bathtub"></i><?php echo $destaque['caracs'][0]['qtd_suites']?> suítes</span>
                            <span><i class="fa fa-car"></i><?php echo $destaque['caracs'][0]['qtd_vagas']?> vagas</span>
                        </div>
                    </div>
                </div>
            </div><!-- item active -->
        <?php } ?>

    </div><!-- carousel inner -->

</div><!-- carousel locacao -->