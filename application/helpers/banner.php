<div class="banner-destaque">
    <div class="container wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
        <div>
            <h1 class="headline wow slideInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                Nossos Destaques
            </h1>
        </div>

        <div id="carousel-banner-destaque"  class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel-banner-destaque" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-banner-destaque" data-slide-to="1"></li>
                <li data-target="#carousel-banner-destaque" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img class="img-fluid" src="<?= $imagePath ?>/img-banner.jpg" alt="Banner destaque">
                </div>
                <div class="carousel-item">
                    <img class="img-fluid" src="<?= $imagePath ?>/img-banner.jpg" alt="Banner destaque">
                </div>
                <div class="carousel-item">
                    <img class="img-fluid" src="<?= $imagePath ?>/img-banner.jpg" alt="Banner destaque">
                </div>
            </div>
        </div><!-- carousel banner destaque -->

    </div><!-- container -->
</div><!-- banner destaque -->


<!--<div id="banner" style="z-index:-0; margin-top:70px;<?php /*echo $temBanner==0?"display:none":""*/?>">
<?php /*if($banners){*/?>
    <div class="pix_diapo" style="margin-bottom:-50px;">
      <?php /* foreach($banners as $banner){*/?>
        <div data-thumb="<?php /*echo $local*/?><?php /*echo $banner["img"] */?>">
                 <img src="<?php /*echo $local*/?><?php /*echo $banner["img"] */?>" width="980" height="280">
        </div>
      <?php /*}*/?>
    </div>
    <div class="limp"></div>
<?php /*}*/?>
</div>-->