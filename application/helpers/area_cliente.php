﻿<div class="col-md-4 text-left">
<h1 class="text-left">Área Cliente</h1>
<div class="row">
  <div class="col-md-12">
	<ul class="media-list">
	  <li class="media">
		<!-- Button trigger modal >-->
		<a class="pull-left imob-formatacao-model" data-toggle="modal" data-target="#pcModal"><img class="media-object" src="<?php echo $local;?>application/images/pc.png" height="64" width="64"></a>

		<div class="media-body">
		  <h4 class="media-heading">Proprietário</h4>
		  <p>Retire aqui sua Prestação de contas e declaração de IRRF.</p>
		</div>
	  </li>
	  <li class="media">
		<!-- Button trigger modal >-->
		<a class="pull-left imob-formatacao-model" data-toggle="modal" data-target="#recModal"><img class="media-object" src="<?php echo $local;?>application/images/boleto2.png" height="64" width="64"></a>

		<div class="media-body">
		  <h4 class="media-heading">Inquilino</h4>
		  <p>Retire aqui sua segunda via de boleto.</p>
		</div>
	  </li>
	</ul>
  </div>
</div>
</div>
