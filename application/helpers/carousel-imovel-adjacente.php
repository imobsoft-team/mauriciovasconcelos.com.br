<section class="imob-section-carousel">
  <div class="container">
   <div class="row imob-row">
      <div class="col-md-4 col-sm-4 col-xs-7 text-center imob-destLocacao">
        <h4>Imóveis <b>Próximos</b></h4>
      </div>          
      <div class="col-md-8 col-sm-8 col-xs-5 text-right imob-busca-todos">
        <!--a href=""><h4 class="hidden-xs">Realize uma busca de todos os imóveis</h4></a-->
        <a href=""><h4 class="visible-xs">Ver todos</h4></a>
      </div>
    </div>       
  </div>
  <div class="container" style="margin-bottom:60px;">
    <div class="row imob-row">          
      <div class="col-md-1 visible-lg" style="width:45px; padding: 0;">
        <a class="imob-left carousel-control" href="#imob-imovel-adjacente" role="button" data-slide="prev">
          <img src="<?php echo $imagePath;?>btn-left.png" alt="Botão anterior">              
        </a>
      </div>

      <div id="imob-imovel-adjacente" class="col-md-11 carousel slide carousel-imovel-destaque" data-ride="carousel" data-type="multi" data-interval="4000" >  <!-- Wrapper for slides -->
        <div class="carousel-inner">
		<?php $y = 0; foreach ($imoveisPrx as $proximos) { ?>
          <div class="item <?php echo ($y == 0?'active':''); ?>">              
              <div class="col-md-3 col-sm-4 col-xs-12 imob-thumb">
                <div class="thumbnail">                      
                  <div class="col-md-12 col-xs-12 imob-thumbnail">
                    <a href="<?php echo $local.'index.php/imoveis/detalhes/aluguel/'.$proximos['tim_des'].'/'.$proximos['imo_isn']; ?>" ><img src="<?php echo $proximos['foto'];?>" style="width:255px; height:187px;" class="img-responsive" alt="Imóvel"></a>
                    <h3 class="imob-bairro"><?php echo $proximos['imo_des_bai']; ?></h3>
                    <p class="text-center col-md-5 col-xs-5"><i class="fa fa-2x fa-bed" aria-hidden="true"></i><br>DORMITÓRIOS: <?php echo ($proximos['caracs'][0]['qtd_suites'] + $proximos['caracs'][0]['qtd_quartos']);?></p>
                    <p class="text-center col-md-3 col-xs-3"><i class="fa fa-2x fa-car" aria-hidden="true"></i><br>VAGAS: <?php echo $proximos['caracs'][0]['qtd_vagas']?></p>
                    <p class="text-center col-md-4 col-xs-4">
                      <img src="<?php echo $imagePath?>icon-trena.png" class="img-responsive" alt="Imóvel destaque">ÁREA: <?php echo $proximos['imo_val_are']?>m²
                    </p>
                    <h3 class="col-md-12 col-xs-12 text-center imob-tipo-imovel"><?php echo $proximos['tim_des']; ?></h3>
                    <p class="col-md-8 col-xs-8" style="font-size: 1em;">&nbsp;CÓDIGO: #<?php echo $proximos['imo_isn']; ?></p>
                    <p class="text-center col-md-2 col-xs-2 imob-rs-destaques"><a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a></p>
                    <p class="text-center col-md-2 col-xs-2 imob-rs-destaques"><a href="<?php echo $local.'index.php/imoveis/detalhes/aluguel/'.$proximos['tim_des'].'/'.$proximos['imo_isn']; ?>"><i class="fa fa-plus" aria-hidden="true"></i></a></p>
                  </div>
                </div>
              </div>
          </div> 
		<?php $y++; } ?>
        </div>
      </div>       

      <div class="col-md-1 visible-lg" style="width:45px; padding: 0;">
        <a class="imob-right carousel-control" href="#imob-imovel-adjacente" role="button" data-slide="next">
          <img class="img-reponsive" src="<?php echo $imagePath;?>btn-right.png" alt="Botão próximo">              
        </a>
      </div>

      <div class="col-md-6 col-sm-6 col-xs-6 hidden-lg">
        <a class="imob-left-mobile carousel-control" href="#imob-imovel-adjacente" role="button" data-slide="prev">
          <img src="<?php echo $imagePath;?>btn-left-mobile.png" alt="Botão anterior">              
        </a>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-6 hidden-lg">
        <a class="imob-right-mobile carousel-control" href="#imob-imovel-adjacente" role="button" data-slide="next">
          <img class="img-reponsive" src="<?php echo $imagePath;?>btn-right-mobile.png" alt="Botão próximo">              
        </a>
      </div>
      
    </div>        
  </div><!-- /.container carousel destaques1 -->
  
</section><!-- /.section destaques -->