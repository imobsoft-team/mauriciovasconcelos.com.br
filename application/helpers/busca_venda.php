<?php
/**
 * Created by PhpStorm.
 * User: Diego Pedrosa
 * Date: 05/09/2015
 * Time: 09:19
 */
?>


<form name="frmBusca" id="frmBusca" method="post" action="<?php echo $local?>index.php/imoveis/buscaimovel">
    <input type="hidden" name="numresult" id="numresult" value="5" />
    <input type="hidden" name="valorini" id="" value="" />
    <input type="hidden" name="valorfim" id="" value="" />

    <div style="display:none">
        <label style="margin-right:6px"><?php echo $tradutor['buscaRapida'][9]?></label>
        <select name="opcoes" id="opcoes" onChange="listarCidades(this.value,'<?php echo $local?>')">
            <option value="0" selected="selected"><?php echo $tradutor['buscaRapida'][8]?></option>
            <!--<option value="1"><?php echo $tradutor['buscaRapida'][6]?></option>-->
            <option value="2"><?php echo $tradutor['buscaRapida'][7]?></option>
        </select>
    </div>

    <div class="tmn-form-pesquisa">
        <select name="tipo" id="tipo">
            <option value="0" selected="selected">
                Tipo
            </option>
        </select>
    </div>
    
    <div class="tmn-form-pesquisa">
        <select name="cidade" id="cidade" onChange="javascript: carregarBairros('<?php echo $local?>')">
            <option value="0" selected="selected"><?php echo utf8_encode($tradutor['buscaRapida'][9])?></option>
        </select>
    </div>

    <div class="tmn-form-pesquisa">
        <select name="bairro[]" id="bairro" class="sumoselectVen" multiple="multiple" title="Para selecionar mais de um bairro: pressione a tecla CTRL e selecione com o mouse" >
            <option>Bairro</option>
        </select>
    </div>

    <div class="tmn-form-pesquisa">
        <select name="area" id="area">
            <option value="0">Área</option>            
            <option value="50-70">50 a 70m2</option>            
            <option value="70-100">70 a 100m2</option>            
            <option value="100-120">100 a 120m2</option>            
            <option value="120-150">120 a 150m2</option>            
            <option value="150-200">150 a 200m2</option>            
            <option value="200">acima de 200m2</option>
        </select>        
    </div>

    <div class="tmn-form-pesquisa">
        <select id="valor" name="valor">
            <option value="0">Valor</option>
			<option value="50000-100000">R$ 50.000,00 a  R$ 100.000,00</option>            
            <option value="100000-200000">R$ 100.000,00 a  R$ 200.000,00</option>            
            <option value="200000-300000">R$ 200.000,00 a  R$ 300.000,00</option>            
            <option value="300000-400000">R$ 300.000,00 a  R$ 400.000,00</option>            
            <option value="400000-500000">R$ 400.000,00 a  R$ 500.000,00</option>            
            <option value="500000">acima de R$ 500.000,00</option> 
        </select>        
    </div>

    <div class="tmn-form-pesquisa">
        <select id="suite" name="suites">
            <option value="0">Suíte</option>
			<option value="1">1</option>            
            <option value="2">2</option>            
            <option value="3">3</option>            
            <option value="4">4</option>            
            <option value="5">5</option>
        </select>        
    </div>

    <div class="tmn-form-pesquisa">
        <select id="vagas" name="vagas">
            <option value="0">Vagas</option>
			<option value="1">1</option>            
            <option value="2">2</option>            
            <option value="3">3</option>            
            <option value="4">4</option>            
            <option value="5">5</option>
        </select>        
    </div>

    <div class="tmn-form-pesquisa">
        <input type="text" name="codigo" id="codigo" onKeyPress="this.value = this.value.toUpperCase()" placeholder="Código" />
    </div>

    <div class="tmn-form-pesquisa">
        <span style="background-color: #ed3237; font-size: 15px; margin-left: 15px; color:#fff; margin-bottom: 1rem; border: 1px solid #bbb; border-radius: 4px; box-sizing: border-box; cursor: pointer;display: inline-block; font-weight: 600;height: 38px;letter-spacing: 0.1rem;line-height: 38px;padding: 0 30px;text-align: center;text-decoration: none;text-transform: uppercase;white-space: nowrap;" onClick="buscaRapida2('<?php echo $local?>')" class="tmn-btn" name="btn-enviar" id="btn-enviar">Buscar</span>
    </div>
</form>