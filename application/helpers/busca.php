﻿<div class="col-md-8">
	<h1>Encontre seu Imóvel</h1>
	<div class="row">
		<form name="frmBusca" id="frmBusca" method="post" action="<?php echo $local?>index.php/imoveis/buscaimovel" role="form">
		  <input type="hidden" name="numresult" id="numresult" value="5" />
		  <input type="hidden" id="valorfim" name="valorfim" />
		  <input type="hidden" id="valorini" name="valorini" />
		  <input type="hidden" id="dte" name="dte" value="0"/>
		  <div class="col-md-3 col-sm-3">
			  <div class="form-group">
				<label class="control-label" for="opcoes">Finalidade</label>
				<select class="form-control stylized-form" name="opcoes" id="opcoes"> <!-- onChange="listarCidades(this.value,'<?php echo $local?>')" -->
					<option value="0"><?php echo $tradutor['buscaRapida'][8]?></option>
					<option value="1" selected="selected"><?php echo $tradutor['buscaRapida'][6]?></option>
        			<!--option value="2"><?php echo $tradutor['buscaRapida'][7]?></option-->
				</select>
			  </div>
			  <div class="form-group">
				<label class="control-label" for="cidade">Cidade</label>
				<select class="form-control stylized-form" name="cidade" id="cidade" onChange="javascript: carregarBairros('<?php echo $local?>')">
				  <option value="0" selected="selected"><?php echo utf8_encode($tradutor['buscaRapida'][9])?></option>
				</select>
			  </div>
		  </div>
		  <div class="col-md-3 col-sm-3">
			  <div class="form-group">
				<label class="control-label" for="tipo">Tipo</label>
				<select class="form-control stylized-form" name="tipo" id="tipo">
				  <option value="0" selected="selected"><?php echo $tradutor['buscaRapida'][19]?></option>
				</select>
			  </div>
			  <div class="form-group">
				<label class="control-label" for="codigo">Código</label>
				<input type="text" class="form-control stylized-form" id="codigo" name="codigo">
			  </div>
		  </div>
		  <div class="col-md-3 col-sm-3">
		  <div class="form-group">
				<label class="control-label" for="bairro">Bairro</label>
				<select class="form-control stylized-form" name="bairro[]" id="bairro" style="height:108px;" multiple>
				  <option>Escolha o bairro</option>
				</select>
			  </div>
		  </div>
		  <div class="col-md-3 col-sm-3">
			<a href="javascript:void(0);" class="btn btn-danger imob-btn" onClick="buscaRapida2('<?php echo $local?>')">Buscar</a>
		  </div>
	  </form>
	</div>
</div>