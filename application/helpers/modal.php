﻿<!-- Modal de Área Restira de Proprietários -->
<div class="modal fade" id="pcModal" tabindex="-1" role="dialog" aria-labelledby="pcModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h3 id="pcModalLabel"><i class="fa fa-unlock-alt"></i>&nbsp;Área Restrita aos Proprietários</h3>
	  </div>
	  <div class="modal-body">
		<form class="form-horizontal" action="<?php echo $local?>index.php/cliente/arearestrita" name="formLogin" id="formLogin" method="post">
			<input type="hidden" name="botao" id="botao">
			<input type="hidden" name="comando" value="validarUsuario">
			<input type="hidden" name="valida" value="1">
		  <div class="form-group">
			<label for="inpuTextPc" class="col-sm-2 control-label">Usuário</label>
			<div class="col-sm-10">
			  <input type="text" class="form-control" name="login" id="login" maxlength="18" onKeyUp="FormataCpfCgc('login',18,event,'formLogin')" placeholder="Digite seu CPF/CNPJ">
			</div>
		  </div>
		  <div class="form-group">
			<label for="inputPasswordPc" class="col-sm-2 control-label">Senha</label>
			<div class="col-sm-10">
			  <input type="password" class="form-control" name="senha_usu"  id="senha_usu" placeholder="Digite sua senha">
			</div>
		  </div>
		  <!--div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			  <div class="checkbox">
				<label>
				  <input type="checkbox"> Lembrar usuário e senha
				</label>
			  </div>
			</div>
		  </div-->
		  <div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			  <button type="button" onClick="javascript: validaLogin()" class="btn btn-primary">Login</button>
			</div>
		  </div>
		</form>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>      
	  </div>
	</div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Modal de Área Restira de Inquilinos -->
<div class="modal fade" id="recModal" tabindex="-1" role="dialog" aria-labelledby="recModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h3 id="recModalLabel"><i class="fa fa-unlock-alt"></i>&nbsp;Área Restrita aos Inquilinos</h3>
	  </div>
	  <div class="modal-body">
		<form class="form-horizontal" action="<?php echo $local?>index.php/cliente/arearestrita" name="formLoginInq" id="formLoginInq" method="post">
			<input type="hidden" name="botao" id="botao">
			<input type="hidden" name="comando" value="validarUsuario">
			<input type="hidden" name="valida" value="1">
		  <div class="form-group">
			<label for="inpuTextPc" class="col-sm-2 control-label">Usuário</label>
			<div class="col-sm-10">
			  <input type="text" class="form-control" name="login" id="loginInq" maxlength="18" onKeyUp="FormataCpfCgc('loginInq',18,event,'formLoginInq')" placeholder="Digite seu CPF/CNPJ">
			</div>
		  </div>
		  <div class="form-group">
			<label for="inputPasswordPc" class="col-sm-2 control-label">Senha</label>
			<div class="col-sm-10">
			  <input type="password" class="form-control" name="senha_usu"  id="senha_usuInq" placeholder="Digite sua senha">
			</div>
		  </div>
		  <!--div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			  <div class="checkbox">
				<label>
				  <input type="checkbox"> Lembrar usuário e senha
				</label>
			  </div>
			</div>
		  </div-->
		  <div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			  <button type="button" onClick="javascript: validaLoginInq()" class="btn btn-primary">Login</button>
			</div>
		  </div>
		</form>
	  </div>
   </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal de Índices de Reajuste  -->
<div class="modal fade bs-example-modal-lg" id="indiceModal" tabindex="-1" role="dialog" aria-labelledby="indiceModalLabel">
  <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h3 id="indiceModalLabel"><i class="glyphicon glyphicon-globe"></i>&nbsp;Índices de Reajuste</h3>
	  </div><!-- /.modal-header -->
	  <div class="modal-body">
			<iframe id="iframeIndices" src="http://www.debit.com.br/aluguel10.php" width="100%" height="600px" frameborder="0" style="border:0" allowfullscreen></iframe>
	  </div><!-- /.modal-body -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal de Localização do Imóvel no Mapa no Banner Principal -->
<div class="modal fade" id="mapaModal" tabindex="-1" role="dialog" aria-labelledby="mapaModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h3 id="mapaModalLabel"><i class="glyphicon glyphicon-globe"></i>&nbsp;Localização do Imóvel no Mapa</h3>
	  </div><!-- /.modal-header -->
		<div class="modal-body">
			<iframe id="iframeMapa" src="" width="550" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	  </div><!-- /.modal-body -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal de Cadastro de Imóvel  -->
<div class="modal fade bs-example-modal-lg" id="cadastroModal" tabindex="-1" role="dialog" aria-labelledby="cadastroModalLabel">
  <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h3 id="cadastroModalLabel"><i class="glyphicon glyphicon-pencil"></i>&nbsp;Cadastro de Imóvel</h3>
	  </div><!-- /.modal-header -->
	  <div class="modal-body">
			<form class="form-horizontal" role="form">
              <div class="form-group">
                <div class="col-sm-2">
                  <label for="nome" class="control-label">Nome</label>
                </div>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="nome" placeholder="Digite seu nome">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-2">
                  <label for="telefone" class="control-label">Telefone</label>
                </div>
                <div class="col-sm-10">
                  <input type="text" class="form-control fixo" id="telefone" placeholder="Digite seu telefone">
                </div>
              </div>
			  <div class="form-group">
                <div class="col-sm-2">
                  <label for="celular" class="control-label">Celular</label>
                </div>
                <div class="col-sm-10">
                  <input type="text" class="form-control celular" id="celular" placeholder="Digite seu celular">
                </div>
              </div>
			  <div class="form-group">
                <div class="col-sm-2">
                  <label for="quantidade" class="control-label">Quantidade de imóveis</label>
                </div>
                <div class="col-sm-10">
                  <select class="form-control" id="quantidade">
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
					<option>5</option>
					<option>Entre 5 e 10</option>
					<option>mais de 10</option>
				  </select>
                </div>
              </div>
              <div class="form-group imob-textarea-padding">
               <div class="col-sm-2">
                  <label for="imob-textarea" class="control-label">Resumo do(s) imóvel(eis):</label>
                </div>
                <div class="col-sm-10">
                  <textarea class="form-control" id="imob-textarea" rows="10" placeholder="Digite aqui um resumo sobre o(s) imóvel(eis)."></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Enviar</button>
				   <button type="reset" class="btn btn-danger">Limpar</button>
                </div>
              </div>
            </form>
	  </div><!-- /.modal-body -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" >
	$(function() {
		 $(".fixo").mask("(99) 9999-9999");
		 $(".celular").mask("(99) 99999-9999");
	});
</script>
