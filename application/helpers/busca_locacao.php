<?php
/**
 * Created by PhpStorm.
 * User: Diego Pedrosa
 * Date: 05/09/2015
 * Time: 09:19
 */
?>


<form name="frmBusca2" id="frmBusca2" method="post" action="<?php echo $local?>index.php/imoveis/buscaimovel">
    <input type="hidden" name="numresult2" id="numresult2" value="5" />
    <input type="hidden" name="valorini2" value="" />
    <input type="hidden" name="valorfim2"  value="" />

    <div style="display:none">
        <label style="margin-right:6px"><?php echo $tradutor['buscaRapida'][9]?></label>
        <select name="opcoes2" id="opcoes2" onLoad="listarCidades2(this.value,'<?php echo $local?>')">
            <option value="0" selected="selected"><?php echo $tradutor['buscaRapida'][8]?></option>
            <option value="1"><?php echo $tradutor['buscaRapida'][6]?></option>
            <!--<option value="2"><?php echo $tradutor['buscaRapida'][7]?></option>-->
        </select>
    </div>

    <div class="tmn-form-pesquisa">
        <select name="tipo2" id="tipo2">
            <option value="0" selected="selected">
                Tipo
            </option>
        </select>
    </div>
    
    <div class="tmn-form-pesquisa">
        <select name="cidade2" id="cidade2" onChange="javascript: carregarBairros2('<?php echo $local?>')">
            <option value="0" selected="selected"><?php echo utf8_encode($tradutor['buscaRapida'][9])?></option>
        </select>
    </div>

    <div class="tmn-form-pesquisa">
        <select name="bairro2[]" class="sumoselectLoc" id="bairro2" multiple="multiple" title="Para selecionar mais de um bairro: pressione a tecla CTRL e selecione com o mouse" >
            <option>Bairro</option>
        </select>
    </div>

    <div class="tmn-form-pesquisa">
        <select name="area" id="area">
            <option value="0">Área</option>            
            <option value="50-70">50 a 70m2</option>            
            <option value="70-100">70 a 100m2</option>            
            <option value="100-120">100 a 120m2</option>            
            <option value="120-150">120 a 150m2</option>            
            <option value="150-200">150 a 200m2</option>            
            <option value="200">acima de 200m2</option>            
        </select>        
    </div>

    <div class="tmn-form-pesquisa">
        <select id="valor" name="valor">
            <option value="0">Valor</option>            
            <option value="250-500">R$ 250,00 a  R$ 500,00</option>            
            <option value="500-1000">R$ 500,00 a  R$ 1.000,00</option>            
            <option value="1000-1500">R$ 1.000,00 a  R$ 1.500,00</option>            
            <option value="1500-2000">R$ 1.500,00 a  R$ 2.000,00</option>            
            <option value="2000-3000">R$ 2.000,00 a  R$ 3.000,00</option>            
            <option value="3000">acima de R$ 3.000,00</option>            
        </select>        
    </div>

    <div class="tmn-form-pesquisa">
        <select id="suite" name="suites">
            <option value="0">Suíte</option>
			<option value="1">1</option>            
            <option value="2">2</option>            
            <option value="3">3</option>            
            <option value="4">4</option>            
            <option value="5">5</option>          
        </select>        
    </div>

    <div class="tmn-form-pesquisa">
        <select id="vagas" name="vagas">
            <option value="0">Vagas</option>
			<option value="1">1</option>            
            <option value="2">2</option>            
            <option value="3">3</option>            
            <option value="4">4</option>            
            <option value="5">5</option>          
        </select>        
    </div>

    <div class="tmn-form-pesquisa">
        <input type="text" name="codigo2" id="codigo2" onKeyPress="this.value = this.value.toUpperCase()" placeholder="Código" />
    </div>

    <div class="tmn-form-pesquisa">
        <button style="background-color: #ed3237; font-size: 15px; margin-left: 15px; color:#fff;" onClick="buscaRapida3('<?php echo $local?>')" class="tmn-btn" name="btn-enviar" id="btn-enviar">Buscar</button>
    </div>
</form>