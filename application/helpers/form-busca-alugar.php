<div class="container">
  <div class="imob-busca">
    <div class="row imob-row">
      <div class="col-md-3 col-sm-4 col-xs-6 text-center imob-busAluga">
        <h4>Busca para alugar</h4>
      </div>
      <div class="col-md-6 hidden-sm hidden-xs imob-busca-map">
        <a href="javascript:void(0)" onclick="carDiv('paraMaps','','<?php echo $local;?>','<?php echo $local;?>/index.php/ajax/desenhaMap','');"><h4>Realize uma busca pelo mapa <i class="fa fa-map-marker" aria-hidden="true"></i></h4></a>
      </div>
      <div class="col-md-3 col-sm-8 col-xs-6 text-right imob-quero-comprar">
        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/comprar'; ?>"><h4>Quero comprar <i class="fa fa-share" aria-hidden="true"></i></h4></a>
      </div>
    </div>

    <div class="row imob-row">
      <form name="frmBusca2" id="frmBusca2" class="imob-form" method="post" action="<?php echo $local?>index.php/imoveis/buscaimovel">
		<input type="hidden" name="numresult2" id="numresult2" value="9" />
		<input type="hidden" name="valorini2" value="" />
		<input type="hidden" name="valorfim2"  value="" />
		<div style="display:none">
        <label style="margin-right:6px"><?php echo $tradutor['buscaRapida'][9]?></label>
        <select name="opcoes2" id="opcoes2">
            <option value="0" selected="selected"><?php echo $tradutor['buscaRapida'][8]?></option>
            <option value="1">Alugar</option>
            <!--<option value="2"><?php echo $tradutor['buscaRapida'][7]?></option>-->
        </select>
    </div>
        <div class="col-md-10 col-sm-12">                
          <div class="col-md-3 col-sm-6">
            <div class="form-group">
				<select class="form-control" name="tipo2" id="tipo2">
					<option value="0" selected="selected">
						TIPO
					</option>
				</select>
            </div>
            <div class="form-group">                  
				<select class="form-control" name="cidade2" id="cidade2" onChange="carregarBairros2('<?php echo $local?>')">
					<option value="0" selected="selected">CIDADE</option>
				</select>
            </div> 
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="form-group">
				<select name="bairro2[]" class="form-control" id="bairro2" multiple="multiple" style="height:35px;">
					<option>BAIRRO</option>
				</select>
            </div>
            <div class="form-group">                  
				<select class="form-control" id="suite" name="suites">
					<option value="0">Suíte</option>
					<option value="1">1</option>            
					<option value="2">2</option>            
					<option value="3">3</option>            
					<option value="4">4</option>            
					<option value="5">5</option>          
				</select>
            </div> 
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="form-group">
				<select class="form-control" id="vagas" name="vagas">
					<option value="0">Vagas</option>
					<option value="1">1</option>            
					<option value="2">2</option>            
					<option value="3">3</option>            
					<option value="4">4</option>            
					<option value="5">5</option>          
				</select> 
            </div>
            <div class="form-group">                  
				<select class="form-control" name="area" id="area">
					<option value="0">Área</option>            
					<option value="50-70">50 a 70m2</option>            
					<option value="70-100">70 a 100m2</option>            
					<option value="100-120">100 a 120m2</option>            
					<option value="120-150">120 a 150m2</option>            
					<option value="150-200">150 a 200m2</option>            
					<option value="200">acima de 200m2</option>            
				</select>
            </div> 
          </div> 
          <div class="col-md-3 col-sm-6">
            <div class="form-group">
				<select class="form-control" id="valor" name="valor">
					<option value="0">Valor</option>            
					<option value="250-500">R$ 250,00 a  R$ 500,00</option>            
					<option value="500-1000">R$ 500,00 a  R$ 1.000,00</option>            
					<option value="1000-1500">R$ 1.000,00 a  R$ 1.500,00</option>            
					<option value="1500-2000">R$ 1.500,00 a  R$ 2.000,00</option>            
					<option value="2000-3000">R$ 2.000,00 a  R$ 3.000,00</option>            
					<option value="3000">acima de R$ 3.000,00</option>            
				</select>
            </div>
            <div class="form-group">                    
              <input class="form-control" type="text" name="codigo2" id="codigo2" onKeyPress="this.value = this.value.toUpperCase()" placeholder="CÓDIGO" />
            </div> 
          </div>
        </div>
        <div class="col-md-2">
          <div class="col-md-12 col-sm-3">
            <div class="form-group">
              <button type="button" onClick="buscaRapida3('<?php echo $local?>')" class="btn imob-btn-form text-center">
                <i class="fa fa-search hidden-xs hidden-sm" aria-hidden="true" style="font-size:1.9em; display: block;"></i>
                <i class="fa fa-search hidden-md hidden-lg" aria-hidden="true" style="display: inline"></i> BUSCAR
              </button>
            </div>
          </div>
        </div>                
      </form>            
    </div><!-- /.form de busca -->
  </div>
</div><!-- /.container de busca alugar -->