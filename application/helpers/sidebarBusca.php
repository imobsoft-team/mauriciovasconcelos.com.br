<div class="col busca wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
    <form class="form" name="frmBusca2" id="frmBusca2" method="post" action="<?php echo $local?>index.php/imoveis/buscaimovel">
        <input type="hidden" name="numresult2" id="numresult2" value="5" />
        <input type="hidden" name="valorini2" id="valorini2" value="" />
        <input type="hidden" name="valorfim2" id="valorfim2" value="" />
        <input type="hidden" name="codigo2"  id="codigo2" value="" />
        <div class="form-group">
            <select class="form-control input-custom" name="opcoes2" id="opcoes2" onLoad="listarCidades2(this.value,'<?php echo $local?>')">
                <option value="0" selected>FINALIDADE</option>
                <option value="1">LOCAÇÃO</option>
                <!--<option value="2">VENDA</option>-->
            </select>
        </div>
        <div class="form-group">
            <select class="form-control input-custom" id="tipo2" name="tipo2">
                <option value="0" selected>Tipo de imóvel</option>
            </select>
        </div>
        <div class="form-group">
            <select class="form-control input-custom" name="cidade2" id="cidade2" onChange="javascript: carregarBairros2('<?php echo $local?>')">
                <option value="0" selected>CIDADE</option>
            </select>
        </div>
        <div class="form-group">
            <select class="form-control input-custom" style="height:75px;" name="bairro2[]" id="bairro2"  title="Para selecionar mais de um bairro: pressione a tecla CTRL e selecione com o mouse" >
                <option>Selecione</option>
            </select>
        </div>
        <div class="form-group">
            <button class="btn btn-secondary input-custom" onClick="buscaRapida3('<?php echo $local?>')"><i class="fa fa-search"></i>
                Buscar
            </button>
        </div>
    </form>
</div><!-- col 1 busca -->