<?php
require_once('conexao.php');
 class Irf extends Conexao{
    function Irf() {
		parent::Conexao();
	}	
	function carregarDados($cod) {
	   $dados = "";
	   $sql = "SELECT * FROM TAB_IRF WHERE PES_ISN=$cod";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
	   	    $dados["irf_isn"]     = odbc_result($res,"IRF_ISN"); 	   
			$dados["irf_per"]     = odbc_result($res,"IRF_PER"); 
	   }	 
	   odbc_close($conn);		
	   return $dados;
	}
	function carregarImoExt($codigo) { 
	   $dados = "";
	   $i = 0;
	   $sql = "SELECT * FROM TAB_IME WHERE IRF_ISN=$codigo";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		  return false;
	   }
	   while(@odbc_fetch_row($res)) {
	   		$dados[$i]["ime_isn"]         = odbc_result($res,"IME_ISN"); 	   
			$dados[$i]["ime_des_end"]     = odbc_result($res,"IME_DES_END"); 
			$dados[$i]["ime_des_con"]     = odbc_result($res,"IME_DES_CON"); 
			$i++;
	   }	 
	   odbc_close($conn);		
	   return $dados;
	}

	function carregarDadosPessoais($codigo) {
	   $dados = "";
	   $sql = "SELECT * FROM TAB_PES WHERE PES_ISN=$codigo";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
	   	    $dados["pes_nom"]             = odbc_result($res,"PES_NOM"); 	   
			$dados["pes_des_end"]         = odbc_result($res,"PES_DES_END"); 
			$dados["pes_num_cpf_cgc"]     = odbc_result($res,"PES_NUM_CPF_CGC"); 
	   }	 
	   odbc_close($conn);		
	   return $dados;
	}

	function carregarLancamento($codigo) { 
	   $dados = "";
	   $i = 0;
	   $sql = "SELECT * FROM TAB_LIM WHERE IME_ISN=$codigo";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
	   	    $dados[$i]["ime_isn"]           = odbc_result($res,"IME_ISN"); 	   
			$dados[$i]["lim_isn"]           = odbc_result($res,"LIM_ISN"); 	   
			$dados[$i]["lim_mes"]            = odbc_result($res,"LIM_MES"); 
			$dados[$i]["lim_mes_pgto"]      = odbc_result($res,"LIM_MES_PGTO"); 
			$dados[$i]["lim_val_alu"]       = odbc_result($res,"LIM_VAL_ALU"); 
			$dados[$i]["lim_val_tx_adm"]    = odbc_result($res,"LIM_VAL_TX_ADM"); 
			$dados[$i]["lim_tx_adm"]        = odbc_result($res,"LIM_TX_ADM"); 
			$dados[$i]["lim_val_irrf"]      = odbc_result($res,"LIM_VAL_IRRF"); 
			$dados[$i]["lim_nom_inq"]       = odbc_result($res,"LIM_NOM_INQ"); 
			$dados[$i]["lim_cpf_cnpj_inq"]  = odbc_result($res,"LIM_CPF_CNPJ_INQ"); 
			$i++;
	   }	 
	   odbc_close($conn);		
	   return $dados;
	}
	function carregarResumoImo($codigo) {
	   $dados = "";
	   $i = 0;
	   $sql = "SELECT * FROM TAB_RIM WHERE IME_ISN=$codigo";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
	   	    $dados[$i]["ime_isn"]               = odbc_result($res,"IME_ISN"); 	   
			$dados[$i]["rim_tot_alu"]           = odbc_result($res,"RIM_TOT_ALU"); 	   
			$dados[$i]["rim_tot_alu_enc_iptu"]  = odbc_result($res,"RIM_TOT_ALU_ENC_IPTU"); 
			$dados[$i]["rim_tot_tx_adm"]        = odbc_result($res,"RIM_TOT_TX_ADM"); 
			$dados[$i]["rim_tot_irrf"]       = odbc_result($res,"RIM_TOT_IRRF"); 
			$i++;
	   }	 
	   odbc_close($conn);		
	   return $dados;
	}
	function carregarResumoGeral($codigo) {
	   $dados = "";
	   $i = 0;
	   $sql = "SELECT * FROM TAB_RGE WHERE IRF_ISN=$codigo";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$dados[$i]["rge_tot_alu"]           = odbc_result($res,"RGE_TOT_ALU"); 	   
			$dados[$i]["rge_tot_alu_enc_iptu"]  = odbc_result($res,"RGE_TOT_ALU_ENC_IPTU"); 
			$dados[$i]["rge_tot_irrf"]          = odbc_result($res,"RGE_TOT_IRRF"); 
			$dados[$i]["rge_tot_tx_adm"]        = odbc_result($res,"RGE_TOT_TX_ADM"); 
			$dados[$i]["rge_tot_liq"]           = odbc_result($res,"RGE_TOT_LIQ"); 
			$i++;
	   }	 
	   odbc_close($conn);		
	   return $dados;
	}
	function consultarIrf($nome) {
	   $con = new Conexao(); 
	   $dados = array();
	   $sql = "SELECT P.PES_ISN,P.PES_NOM AS NOME,I.IRF_PER AS PERIODO FROM TAB_PES P,TAB_IRF I WHERE I.PES_ISN = P.PES_ISN
		       AND P.PES_NOM LIKE '$nome%'";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
	   }
	   $i = 0;
	   while(odbc_fetch_row($res)) {
			$dados[$i]["nome"]           = odbc_result($res,"NOME"); 	   
			$dados[$i]["periodo"]        = odbc_result($res,"PERIODO"); 
			$i++;
	   }	 
	   odbc_close($conn);		
	   return $dados;
	}
 }
?>