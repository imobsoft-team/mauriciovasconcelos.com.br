<?php
class Internas extends Controller {
	public $layout = 'default';
	public $title = '::: Equatorial Im�veis:::';
	public $css = array('default','jquery.lightbox-0.5','ajaxtabs');
	public $js = array('jquery-1.3.2.min','jquery.maskedinput-1.1.1','jquery.innerfade','slider.js','tur.js','js','ajaxtabs','global','internas','swfobject','busca');
	public $data = array('local'=>"../../../",'logomarca'=>'application/images/logo.jpg','empresa'=>'Equatorial Im�veis','cnpj'=>'99.999.999/0001-99','fones'=>'(85) 3244 7464','endereco'=>'Rua Dr. Jos� Louren�o, 870, Sala 105, Meireles.','sessao'=>'imoveisEqua','sessaoCli'=>'EquaCli');
	public $configMail = array('protocol' => '0',
    'smtp_host' => '0',
    'smtp_port' => 0,
    'smtp_user' => '0',
    'smtp_pass' => '0',
    );
	public $emails = array('Loca��o|locacao@equatorialimoveis.com.br','Vendas|vendas@equatorialimoveis.com.br','Financeiro|financeiro@equatorialimoveis.com.br');
	public $description = '';
	public $keys = array();
	
	function Internas()
	{
		parent::Controller();
		if(!isset($_SESSION['idioma'])){
			session_register('idioma');
			$_SESSION['idioma'] = 1;
		}
		if(isset($_GET['idioma']) && is_numeric($_GET['idioma'])){
			switch($_GET['idioma']){
				case 2:
					$_SESSION['idioma'] = 2;
					break;
				case 3;
					$_SESSION['idioma'] = 3;
					break;
				default:
					$_SESSION['idioma'] = 1;
			}
		}
		if(!isset($_SESSION[''.$this->data['sessao']])){
			session_register(''.$this->data['sessao']);
		}
		if(!isset($_SESSION['parametros'])){
			session_register('parametros');
		}
		if(!isset($_SESSION[''.$this->data['sessaoCli']])){
			session_register(''.$this->data['sessaoCli']);
		}
		$logado = $_SESSION[''.$this->data['sessaoCli']];
		$this->load->model('enquete');
		$this->data['pergunta'] = $this->enquete->getAsk($logado);
		$this->data['resp']="";
		if($this->data['pergunta']){
			$this->data['resp'] = $this->enquete->getResp($this->data['pergunta']["id"]);
		}
		if(!isset($_SESSION['origem'])){
			session_register('origem');
		}
		$_SESSION['origem']?$this->data['origem'] = $_SESSION['origem']:$this->data['origem'] = $this->data["local"];
		//conta acesso ao site
		$this->load->model('tab_acs');
		$this->tab_acs->insertAcssite(session_id());
		$this->load->helper('url');
		$this->data['local'] = site_url();
		//pega a tradu��o para a p�gina
		$this->load->model('utilidades');
		$this->data['tradutor'] = $this->utilidades->Tradutor($_SESSION['idioma'],$this->data['local']);
		//pega os imoveis para obama
		$this->load->model('imovelvenda');
		$this->load->model('lancamento');
		$imoveis = $this->imovelvenda->getDestaques(500);
		$lancs   = $this->lancamento->getDestaques(500);
		$banner ="";
		if(is_array($lancs) && is_array($imoveis)){
			$banner = array_merge($lancs,$imoveis);
		}else if(is_array($imoveis)){
			$banner = $imoveis;
		}else if(is_array($lancs)){
			$banner = $lancs;
		}
		$imoveis = "";
		for($i=0;$i<count($banner) && $i<6;$i++){
			if($banner[$i]['imo_isn']>0){
				$imoveis[$i]['link'] = $this->data['local'].'index.php/imoveis/detalhes/venda/'.$banner[$i]['tim_des'].'/'.$banner[$i]['imo_isn'];
				$imoveis[$i]['texto'] = $banner[$i]['imo_des_est'];
				$imoveis[$i]['foto'] = $this->data['local'].$banner[$i]['foto'];
				$imoveis[$i]['tipo'] = $banner[$i]['tim_des'];
			}else {
				$imoveis[$i]['link'] = $this->data['local'].'index.php/imoveis/detalhes/empreendimento/'.$banner[$i]['tipo'].'/'.$banner[$i]['emp_isn'];
				$imoveis[$i]['texto'] = $banner[$i]['emp_des_txt'];
				$imoveis[$i]['foto'] = $this->data['local'].$banner[$i]['foto'];
				$imoveis[$i]['tipo'] = $banner[$i]['tipo'];
			}
		}
		$_SESSION["banners"] = $imoveis;
	}
	
	function index()
	{
		$this->load->helper('url');
		redirect('','refresh');
	}
	
	function pagina($pag){
		$this->load->model('tab_textos');
		switch($pag){
			case "empresa":
				$tex = $this->tab_textos->getPagina(1);
				break;
			case "servicos":
				$tex = $this->tab_textos->getPagina(2);
				break;
			case "aluguel":
				$tex = $this->tab_textos->getPagina(4);
				break;
			case "venda":
				$tex = $this->tab_textos->getPagina(3);
				break;
			case "parceiros":
				$tex = $this->tab_textos->getPagina(4);
				break;
			case "simuladores":
				$tex = $this->tab_textos->getPagina(5);
				break;
			case "lancamentos":
				$tex = $this->tab_textos->getPagina(6);
				break;
				
		}
		$this->data['men'] = "";
		$idm = $_SESSION['idioma'];
		if($tex){
			switch($idm){
				case 2:
					$tit = $tex->TEXTO_NOME_ING;
					$men = $tex->TEXTO_DES_ING;
					break;
				case 3:
					$tit = $tex->TEXTO_NOME_ESP;
					$men = $tex->TEXTO_DES_ESP;
					break;
				default:
					$tit = $tex->TEXTO_NOME;
					$men = $tex->TEXTO_DES;
					break;
			}
			/*$men = str_replace("<h3>","<h3 style=\"color: #000080;\">",$men);
			$men = str_replace("class=\"Title\"","style=\"font-weight: bold;font-size: 18px;color: #cc3300;\"",$men);
			$men = str_replace("class=\"Code\"","style=\"border: #8b4513 1px solid;padding-right: 5px;padding-left: 5px;color: #000066;font-family: 'Courier New' , Monospace;background-color: #ff9933;\"",$men);*/
			//inverte o htmlentities no banco
			$this->data['men'] = html_entity_decode($men);
		}
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->view('internas/text',$this->data);
	}
	
	function indicadores(){
		$this->load->model('tab_cota');
		$this->data['dados1'] = $this->tab_cota->getDados1();
		$this->data['dados2'] = $this->tab_cota->getDados2();
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->view('internas/cotacoes',$this->data);
	}
	
	function noticias(){
		$this->load->model('tab_not');
		$this->load->library('pagination');
		$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
		if(!$busca){
			$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
		}
		$ordem = (!$this->uri->segment("4")) ? 'NOT_ISN ASC' : $this->uri->segment("4");
		$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
		$config['base_url']   = $this->data['local'].'index.php/internas/noticias/'.$busca.'/'.$ordem.'/';
		if(empty($busca)) { $busca=""; }
		$config['total_rows'] = $this->tab_not->contaRegistros($busca);
		$config['per_page']   = 10;
		$config['first_link'] = 'Primeiro';
		$config['last_link']  = '�ltimo';
		$config['next_link']  = 'Pr�ximo';
		$config['prev_link']  = 'Anterior';
		$config['uri_segment'] = 5;
		$this->pagination->initialize($config); 
		$this->data["paginacao"] =  $this->pagination->create_links();
		$this->data['noticias'] = $this->tab_not->getNoticias(10,$inicio,$busca,$ordem);
		//array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->view('internas/noticias',$this->data);
	}
	
	function videos(){
		$this->load->model('videos');
		$this->load->library('pagination');
		$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
		if(!$busca){
			$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
		}
		$ordem = (!$this->uri->segment("4")) ? 'ID_VID ASC' : $this->uri->segment("4");
		$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
		$config['base_url']   = $this->data['local'].'index.php/internas/videos/'.$busca.'/'.$ordem.'/';
		if(empty($busca)) { $busca=""; }
		$config['total_rows'] = $this->videos->contaRegistrosVideos($busca);
		$config['per_page']   = 10;
		$config['first_link'] = 'Primeiro';
		$config['last_link']  = '�ltimo';
		$config['next_link']  = 'Pr�ximo';
		$config['prev_link']  = 'Anterior';
		$config['uri_segment'] = 5;
		$this->pagination->initialize($config); 
		$this->data["paginacao"] =  $this->pagination->create_links();
		$this->data['videos'] = $this->videos->getVideos(10,$inicio,$busca,$ordem);
		//array_push($this->js,'global','venda','busca','jq-corner','jquery.lightbox-0.5');
		$this->load->view('internas/videos',$this->data);
	}
	
	function noticia($id){
		$this->load->model('tab_not');
		$this->data['noticia'] = $this->tab_not->buscarNoticia($id);
		$this->load->view('internas/noticia',$this->data);
	}
	
	
	function newsletter(){
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		array_push($this->js,'global','internas');
		$this->load->view('internas/news_letter',$this->data);
	}
	
	function cadastrarNews(){
		$this->load->model('usuario');
		$this->load->library('form_validation');
		$this->load->helper('url');
		if($this->input->post('nome',TRUE) && $this->input->post('email',TRUE)){
			$msg = $this->usuario->cadastrarNews($this->input->post('nome',TRUE),$this->input->post('email',TRUE),$this->input->post('pes_tipo',TRUE));
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/newsletter"; </script>');
		}else {
			redirect('internas/newsletter','refresh');
		}
		die;
		
	}
	
	function cadastroImovel(){
			$this->load->model('imovel');
			$this->load->library('form_validation');
			$this->load->helper('url');
			$nome        = $this->input->post("nome",TRUE); 
			$fone1       = $this->input->post("fone1",TRUE);
			$fone2       = $this->input->post("fone2",TRUE);
			$email       = $this->input->post("email",TRUE);
			$cel    	 = $this->input->post("cel",TRUE);
			$end         = $this->input->post("endereco",TRUE);
			$comp        = $this->input->post("complementoEnd",TRUE);
			$numero      = $this->input->post("numero",TRUE);
			$cep         = $this->input->post("cep",TRUE);
			$uf          = $this->input->post("uf",TRUE);
			$bairro      = $this->input->post("bairro",TRUE);
			$cidade      = $this->input->post("cidade",TRUE);
			$fin         = $this->input->post("finalidade",TRUE);
			$resumo      = $this->input->post("resumo",TRUE);
	
			$msg = $this->imovel->cadastrarImovel($nome,$fone1,$fone2,$email,$cel,$end,$comp,$numero,$cep,$uf,$bairro,$cidade,$fin,$resumo);
			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/cadastro"; </script>');
		die;	
	}
	
	function contato(){
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		array_push($this->js,'global','internas');
		$this->data['emails'] = $this->emails;
		$this->load->view('internas/contato_pt',$this->data);
	}
	
		function cadastro(){
		array_push($this->js,'global','internas','cadastroImo');
		$this->load->view('internas/cadastro_imovel',$this->data);
	}
	
	function enviacontato(){
		$this->load->library('form_validation');
		if($this->input->post("depto") && $this->input->post("mail")){
			$this->load->model('usuario');
			$this->load->helper('url');
			$depto    = $this->input->post("depto",TRUE); 
			$nome     = $this->input->post("nome",TRUE);
			$email    = $this->input->post("mail",TRUE);
			$fone     = $this->input->post("fone",TRUE);
			$msgs     = $this->input->post("msg",TRUE);
			$destino  = explode('|',$this->emails[$depto-1]);
			$msg = $this->usuario->cadastrarContato(0,$nome,$email,$fone,$msgs,$depto);
			if($msg=="Operacao relizada com sucesso"){
				$msgEmail = $this->data['empresa']."<br>";
				$msgEmail = $msgEmail." Endere�o: ".$this->data['endereco']."<br>";
				$msgEmail = $msgEmail." Fone(s): ".$this->data['fones']."<br><br>";
				$msgEmail = $msgEmail."<strong> Nome: ".$nome."</strong><br>";
				$msgEmail = $msgEmail."<strong> E-mail: ".$email."</strong><br>";
				$msgEmail = $msgEmail."<strong> Fone: ".$fone."</strong><br>";
				$msgEmail = $msgEmail."<strong> Mensagem: ".$msgs."</strong><br>";
				$this->load->plugin('phpmailer');
				$mail=new PHPMailer();
				if($this->configMail['protocol']=="smtp"){
					$mail->IsSMTP();
					$mail->SMTPAuth   = true;
					$mail->SMTPSecure = "";
					$mail->Host       = $this->configMail['smtp_host'];
					$mail->Port       = $this->configMail['smtp_port'];
					$mail->Username   = $this->configMail['smtp_user'];  
					$mail->Password   = $this->configMail['smtp_pass'];            
				}else {
					$mail->IsMail();
				}		 
				$mail->From       = $email;
				$mail->FromName   = $nome;
				$mail->Subject    = "Envio de contato";
				$mail->Body       = utf8_encode($msgEmail);
				//$mail->AltBody    = "This is the body when user views in plain text format"; 
				$mail->WordWrap   = 50;	 
				$mail->AddAddress($destino[1]);
				$mail->IsHTML(true);
				$mail->send();
			}
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/contato"; </script>');
		}else {
			redirect('internas/contato','refresh');
		}
		die;
	}
	
	function atendimento(){
		$this->title = "Atendimento on-line";
		$this->layout = "popup";
		$this->css = array('popup');
		$this->load->helper('url');
		array_push($this->js,'venda');
		$this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->data['msn'] = 'http://settings.messenger.live.com/Conversation/IMMe.aspx?invitee=e1a3afec7cddd916@apps.messenger.live.com&mkt=pt-br';
		$this->load->view('internas/msn',$this->data);
	}
	
	function p1(){
		$this->layout = 'popup';
		$this->css = '';
		$this->js = '';
		$this->load->view('principal/external1',$this->data);
	}
	
	function p2(){
		$this->layout = 'popup';
		$this->css = '';
		$this->js = '';
		$this->load->view('principal/external2',$this->data);
	}
	
}
/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>