<?php
class Welcome extends Controller {
	public $layout = 'default';
	public $title = '::: Equatorial Im�veis:::';
	public $css = array('default','jquery.lightbox-0.5','ajaxtabs','slider');
	public $js = array('jquery-1.3.2.min','js','ajaxtabs','easySlider1.5','global','internas','swfobject','busca');
	public $data = array('local'=>"",'sessaoCli'=>'EquaCli');
	public $description = 'im�veis para venda e aluguel';
	public $keys = array();

	function Welcome()
	{
		parent::Controller();	
		if(!isset($_SESSION['idioma'])){
			session_register('idioma');
			$_SESSION['idioma'] = 1;
		}
		if(isset($_GET['idioma']) && is_numeric($_GET['idioma'])){
			switch($_GET['idioma']){
				case 2:
					$_SESSION['idioma'] = 2;
					break;
				case 3;
					$_SESSION['idioma'] = 3;
					break;
				default:
					$_SESSION['idioma'] = 1;
			}
		}
		$_SESSION['origem']='';
		if(!isset($_SESSION[''.$this->data['sessaoCli']])){
			session_register(''.$this->data['sessaoCli']);
		}
		$logado = $_SESSION[''.$this->data['sessaoCli']];
		$this->load->model('enquete');
		$this->data['pergunta'] = $this->enquete->getAsk($logado);
		$this->data['resp']="";
		if($this->data['pergunta']){
			$this->data['resp'] = $this->enquete->getResp($this->data['pergunta']["id"]);
		}
		if(!isset($_SESSION['origem'])){
			session_register('origem');
		}
		$_SESSION['origem']?$this->data['origem'] = $_SESSION['origem']:$this->data['origem'] = $this->data["local"];
		//conta acesso ao site
		$this->load->model('tab_acs');
		$this->tab_acs->insertAcssite(session_id());
		$this->load->helper('url');
		$this->data['local'] = site_url();
		//pega a tradu��o para a p�gina
		$this->load->model('utilidades');
		$this->data['tradutor'] = $this->utilidades->Tradutor($_SESSION['idioma'],$this->data['local']);
		//pega os imoveis para obama
		$this->load->model('imovelvenda');
		$this->load->model('lancamento');
		$imoveis = $this->imovelvenda->getDestaques(500);
		$lancs   = $this->lancamento->getDestaques(500);
		$banner ="";
		if(is_array($lancs) && is_array($imoveis)){
			$banner = array_merge($lancs,$imoveis);
		}else if(is_array($imoveis)){
			$banner = $imoveis;
		}else if(is_array($lancs)){
			$banner = $lancs;
		}
		$imoveis = "";
		for($i=0;$i<count($banner) && $i<6;$i++){
			if($banner[$i]['imo_isn']>0){
				$imoveis[$i]['link'] = $this->data['local'].'index.php/imoveis/detalhes/venda/'.$banner[$i]['tim_des'].'/'.$banner[$i]['imo_isn'];
				$imoveis[$i]['texto'] = $banner[$i]['imo_des_est'];
				$imoveis[$i]['foto'] = $this->data['local'].$banner[$i]['foto'];
				$imoveis[$i]['tipo'] = $banner[$i]['tim_des'];
			}else {
				$imoveis[$i]['link'] = $this->data['local'].'index.php/imoveis/detalhes/empreendimento/'.$banner[$i]['tipo'].'/'.$banner[$i]['emp_isn'];
				$imoveis[$i]['texto'] = $banner[$i]['emp_des_txt'];
				$imoveis[$i]['foto'] = $this->data['local'].$banner[$i]['foto'];
				$imoveis[$i]['tipo'] = $banner[$i]['tipo'];
			}
		}
		$_SESSION["banners"] = $imoveis;
	}
	
	function index()
	{
		$this->load->model('imovelvenda');
		$this->load->model('imovelloc');
		$this->load->model('lancamento');
		$this->data['destaqueComp'] = $this->imovelvenda->getDestaques(200);
		$this->data['destaque'] = $this->imovelloc->getDestaques(200);
		$this->data['emprf'] = $this->lancamento->getDestaques(200);
		$this->data['idm'] = $this->session->userdata('idioma');
		@shuffle($this->data['emprf']);
		@shuffle($this->data['destaque']);
		@shuffle($this->data['destaqueComp']);
		$this->load->view('principal/destaque',$this->data);
	}
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>