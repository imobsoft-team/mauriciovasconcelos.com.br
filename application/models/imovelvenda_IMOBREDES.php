<?php
require_once('conexaoimobsale.php');
class ImovelVenda extends Conexaoimobsale{	 
	 private $imobredes;
	 function ImovelVenda(){
	 	parent::ConexaoImobsale();
		$this->imobredes = $this->load->database('imobredes', TRUE);
	 }
	 function getDestaques($tam=300,$tipoImo="IMO.IMO_TIP_IMO = 0 or IMO.IMO_TIP_IMO is null"){
		$this->imobredes = $this->load->database('imobredes', TRUE);
		$idm = $_SESSION['idioma'];;	   
		if($idm == 2) {
		   $textDestq = "IMO_DES_TXT_DEST_ING";
		}else if($idm == 3){
		   $textDestq = "IMO_DES_TXT_DEST_ESP";
		}else {
		   $textDestq = "IMO_DES_TXT_DEST";
		}
		$ass = $this->getASS_ISN();
		$sql = "SELECT IMO.IMO_ISN,IMO.IMO_DES_REF,IMO.IMO_DES_END,IMO.IMO_DES_BAI,IMO.IMO_VAL_ARE_TOT,
		IMO.IMO_COD_IMO_SIS,IMO.IMO_VAL_VEN,IMO.IMO_DES_TXT_DEST,
		IMO.IMO_DES_TXT_DEST_ING,IMO.IMO_DES_TXT_DEST_ESP,IMO.IMO_TIP_EXIB_PRECO_SITE,TIM.TIM_DES,
		(SELECT LOC.LOC_NOM FROM tab_loc LOC WHERE LOC.LOC_ISN = IMO.LOC_ISN) AS CIDADE,
		(SELECT EST.EST_SIG FROM tab_est EST WHERE IMO.EST_ISN = EST.EST_ISN) AS ESTADO  FROM tab_imo IMO INNER JOIN tab_tim TIM ON
		TIM.TIM_ISN = IMO.TIM_ISN WHERE IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0
		AND IMO.ASS_ISN = $ass AND IMO.IMO_TIP_DEST = 1 AND IMO.IMO_TIP_DEST_PRIM_PAG = 1
		AND ($tipoImo)
		ORDER BY IMO.IMO_ISN DESC LIMIT 10";
		
		
	    $i = 0;
	    $codigo = "";	
	    $res = $this->imobredes->query($sql);
	    if($res->num_rows() < 1){
		   return false;
	    }
	    foreach($res->result() as $array){
			$codigo[$i]["imo_isn"]            = $array->IMO_ISN;
			$codigo[$i]["imo_ref"]            = $array->IMO_DES_REF;
			$codigo[$i]["tim_des"]            = $array->TIM_DES;
			$codigo[$i]["imo_cod_sis"]        = $array->IMO_COD_IMO_SIS; 
			$codigo[$i]["imo_des_end"]        = $array->IMO_DES_END; 
			$codigo[$i]["imo_des_bai"]        = $array->IMO_DES_BAI; 
			$codigo[$i]["imo_val_are"]        = $array->IMO_VAL_ARE_TOT; 
			$codigo[$i]["imo_cidade"]         = $array->CIDADE; 
			$codigo[$i]["imo_estado"]         = $array->ESTADO; 
			$codigo[$i]["finalidade"]         = 'VENDA'; 
			$codigo[$i]["detalhes"]    		  = 'index.php/imoveis/detalhes/venda/'.$codigo[$i]["tim_des"].'/'.$codigo[$i]["imo_isn"].'/';
			
			$codigo[$i]["tim_des"]            = utf8_decode($array->TIM_DES); 	   
			if($array->IMO_TIP_EXIB_PRECO_SITE==1){				   
				$codigo[$i]["imo_val"]  = $array->IMO_VAL_VEN;
			}else {
				$codigo[$i]["imo_val"]  = "Consulte-nos"; 
			} 
			if($idm == 2) {
			   $codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST_ING);
			}else if($idm == 3){
			   $codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST_ESP);
			}else {
			   $codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST);
			}
			if(!$codigo[$i]["imo_des_est"]){
				$codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST);
			}
			
		 $i++; 
	   }
	   is_array($codigo)?$quant=count($codigo):$quant=0;
	   
	   for($x=0;$x<$quant;$x++){		 	
			$codigo[$x]["foto"] = $this->buscarFoto($codigo[$x]["imo_isn"],$this->CaminhoFoto(),$tam);
	   }
	   return $codigo;		
	
	 }
	 
	 function getDestaquesResidencial($tam=300,$tipoImo="IMO.IMO_TIP_IMO = 0 or IMO.IMO_TIP_IMO is null"){
		$this->imobredes = $this->load->database('imobredes', TRUE);
		$idm = $_SESSION['idioma'];;	   
		if($idm == 2) {
		   $textDestq = "IMO_DES_TXT_DEST_ING";
		}else if($idm == 3){
		   $textDestq = "IMO_DES_TXT_DEST_ESP";
		}else {
		   $textDestq = "IMO_DES_TXT_DEST";
		}
		$ass = $this->getASS_ISN();
		$sql = "SELECT IMO.IMO_ISN,IMO.IMO_DES_REF,IMO.IMO_DES_END,IMO.IMO_DES_BAI,IMO.IMO_VAL_ARE_TOT,
		IMO.IMO_COD_IMO_SIS,IMO.IMO_VAL_VEN,IMO.IMO_DES_TXT_DEST,
		IMO.IMO_DES_TXT_DEST_ING,IMO.IMO_DES_TXT_DEST_ESP,IMO.IMO_TIP_EXIB_PRECO_SITE,TIM.TIM_DES,
		(SELECT LOC.LOC_NOM FROM tab_loc LOC WHERE LOC.LOC_ISN = IMO.LOC_ISN) AS CIDADE,
		(SELECT EST.EST_SIG FROM tab_est EST WHERE IMO.EST_ISN = EST.EST_ISN) AS ESTADO  FROM tab_imo IMO INNER JOIN tab_tim TIM ON
		TIM.TIM_ISN = IMO.TIM_ISN WHERE IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0
		AND IMO.ASS_ISN = $ass AND IMO.IMO_TIP_DEST = 1 AND IMO.IMO_TIP_DEST_PRIM_PAG = 1
		AND $tipoImo
		AND IMO.IMO_TIP_FIN = 3
		ORDER BY IMO.IMO_ISN DESC LIMIT 10";
		
		
	    $i = 0;
	    $codigo = "";	
	    $res = $this->imobredes->query($sql);
	    if($res->num_rows() < 1){
		   return false;
	    }
	    foreach($res->result() as $array){
			$codigo[$i]["imo_isn"]            = $array->IMO_ISN;
			$codigo[$i]["imo_ref"]            = $array->IMO_DES_REF;
			$codigo[$i]["tim_des"]            = $array->TIM_DES;
			$codigo[$i]["imo_cod_sis"]        = $array->IMO_COD_IMO_SIS; 
			$codigo[$i]["imo_des_end"]        = $array->IMO_DES_END;
			$codigo[$i]["imo_val_are"]        = $array->IMO_VAL_ARE_TOT;			
			$codigo[$i]["imo_des_bai"]        = $array->IMO_DES_BAI; 
			$codigo[$i]["imo_cidade"]         = $array->CIDADE; 
			$codigo[$i]["imo_estado"]         = $array->ESTADO; 
			
			$codigo[$i]["tim_des"]            = utf8_decode($array->TIM_DES); 	   
			if($array->IMO_TIP_EXIB_PRECO_SITE==1){				   
				$codigo[$i]["imo_val"]  = $array->IMO_VAL_VEN;
			}else {
				$codigo[$i]["imo_val"]  = "Consulte-nos"; 
			} 
			if($idm == 2) {
			   $codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST_ING);
			}else if($idm == 3){
			   $codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST_ESP);
			}else {
			   $codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST);
			}
			if(!$codigo[$i]["imo_des_est"]){
				$codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST);
			}
			
		 $i++; 
	   }
	   is_array($codigo)?$quant=count($codigo):$quant=0;
	   
	   for($x=0;$x<$quant;$x++){		 	
			$codigo[$x]["foto"] = $this->buscarFoto($codigo[$x]["imo_isn"],$this->CaminhoFoto(),$tam);
	   }
	   return $codigo;		
	
	 }
	 
	 function getDestaquesComercial($tam=300,$tipoImo="IMO.IMO_TIP_IMO = 0 or IMO.IMO_TIP_IMO is null"){
		$this->imobredes = $this->load->database('imobredes', TRUE);
		$idm = $_SESSION['idioma'];;	   
		if($idm == 2) {
		   $textDestq = "IMO_DES_TXT_DEST_ING";
		}else if($idm == 3){
		   $textDestq = "IMO_DES_TXT_DEST_ESP";
		}else {
		   $textDestq = "IMO_DES_TXT_DEST";
		}
		$ass = $this->getASS_ISN();
		$sql = "SELECT IMO.IMO_ISN,IMO.IMO_DES_REF,IMO.IMO_DES_END,IMO.IMO_DES_BAI,IMO.IMO_VAL_ARE_TOT,
		IMO.IMO_COD_IMO_SIS,IMO.IMO_VAL_VEN,IMO.IMO_DES_TXT_DEST,
		IMO.IMO_DES_TXT_DEST_ING,IMO.IMO_DES_TXT_DEST_ESP,IMO.IMO_TIP_EXIB_PRECO_SITE,TIM.TIM_DES,
		(SELECT LOC.LOC_NOM FROM tab_loc LOC WHERE LOC.LOC_ISN = IMO.LOC_ISN) AS CIDADE,
		(SELECT EST.EST_SIG FROM tab_est EST WHERE IMO.EST_ISN = EST.EST_ISN) AS ESTADO  FROM tab_imo IMO INNER JOIN tab_tim TIM ON
		TIM.TIM_ISN = IMO.TIM_ISN WHERE IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0
		AND IMO.ASS_ISN = $ass AND IMO.IMO_TIP_DEST = 1 AND IMO.IMO_TIP_DEST_PRIM_PAG = 1
		AND $tipoImo
		AND IMO.IMO_TIP_FIN = 1
		ORDER BY IMO.IMO_ISN DESC LIMIT 10";
		
	    $i = 0;
	    $codigo = "";	
	    $res = $this->imobredes->query($sql);
	    if($res->num_rows() < 1){
			
		   return $codigo = 'deu zerado';
	    }
	    foreach($res->result() as $array){
			$codigo[$i]["imo_isn"]            = $array->IMO_ISN;
			$codigo[$i]["imo_ref"]            = $array->IMO_DES_REF;
			$codigo[$i]["tim_des"]            = $array->TIM_DES;
			$codigo[$i]["imo_cod_sis"]        = $array->IMO_COD_IMO_SIS; 
			$codigo[$i]["imo_des_end"]        = $array->IMO_DES_END; 
			$codigo[$i]["imo_des_bai"]        = $array->IMO_DES_BAI; 
			$codigo[$i]["imo_val_are"]        = $array->IMO_VAL_ARE_TOT;
			$codigo[$i]["imo_cidade"]         = $array->CIDADE; 
			$codigo[$i]["imo_estado"]         = $array->ESTADO; 
			
			$codigo[$i]["tim_des"]            = utf8_decode($array->TIM_DES); 	   
			if($array->IMO_TIP_EXIB_PRECO_SITE==1){				   
				$codigo[$i]["imo_val"]  = $array->IMO_VAL_VEN;
			}else {
				$codigo[$i]["imo_val"]  = "Consulte-nos"; 
			} 
			if($idm == 2) {
			   $codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST_ING);
			}else if($idm == 3){
			   $codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST_ESP);
			}else {
			   $codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST);
			}
			if(!$codigo[$i]["imo_des_est"]){
				$codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST);
			}
			
		 $i++; 
	   }
	   is_array($codigo)?$quant=count($codigo):$quant=0;
	   
	   for($x=0;$x<$quant;$x++){		 	
			$codigo[$x]["foto"] = $this->buscarFoto($codigo[$x]["imo_isn"],$this->CaminhoFoto(),$tam);
	   }
	   
	   return $codigo;		

	}
	 
	 function getBanners($tam=300){
		$idm = $_SESSION['idioma'];	   
		if($idm == 2) {
		   $textDestq = "IMO_DES_TXT_DEST_ING";
		}else if($idm == 3){
		   $textDestq = "IMO_DES_TXT_DEST_ESP";
		}else {
		   $textDestq = "IMO_DES_TXT_DEST";
		}
		$ass = $this->getASS_ISN();
		$sql = "SELECT IMO.IMO_ISN,IMO.IMO_DES_REF,IMO.IMO_DES_BAI,IMO.IMO_COD_IMO_SIS,IMO.IMO_VAL_VEN,IMO.$textDestq,IMO.IMO_DES_TXT_DEST,IMO.IMO_TIP_EXIB_PRECO_SITE,TIM.TIM_DES FROM tab_imo IMO INNER JOIN tab_tim TIM ON TIM.TIM_ISN = IMO.TIM_ISN INNER JOIN tab_imi IMI ON IMI.IMO_ISN = IMO.IMO_ISN WHERE IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND IMO.ASS_ISN = $ass AND IMO.IMO_TIP_DEST = 1 AND IMO.IMO_TIP_DEST_PRIM_PAG = 1 AND IMI.IMI_TIP_DES = 1 AND IMO.IMO_TIP_IMO = 1 ORDER BY IMO.IMO_ISN LIMIT 15";
	    $i = 0;
	    $codigo = "";	
	    $res = $this->imobredes->query($sql);
	    if($res->num_rows() < 1){
		   return false;
	    }
	    foreach($res->result() as $array){
			$codigo[$i]["imo_isn"]            = $array->IMO_ISN;
			$codigo[$i]["imo_ref"]            = $array->IMO_DES_REF;
			$codigo[$i]["imo_cod_sis"]        = $array->IMO_COD_IMO_SIS; 
			$codigo[$i]["imo_des_bai"]        = $array->IMO_DES_BAI; 
			$codigo[$i]["tim_des"]            = utf8_decode($array->TIM_DES); 	   
			if($array->IMO_TIP_EXIB_PRECO_SITE==1){				   
				$codigo[$i]["imo_val"]  = $array->IMO_VAL_VEN;
			}else {
				$codigo[$i]["imo_val"]  = "Consulte-nos"; 
			} 
			if($idm == 2) {
			   $codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST_ING);
			}else if($idm == 3){
			   $codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST_ESP);
			}else {
			   $codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST);
			}
			if(!$codigo[$i]["imo_des_est"]){
				$codigo[$i]["imo_des_est"]    = utf8_decode($array->IMO_DES_TXT_DEST);
			} 		 			
		 $i++; 
	   }
	   is_array($codigo)?$quant=count($codigo):$quant=0;
	   
	   for($x=0;$x<$quant;$x++){		 	
			$codigo[$x]["foto"] = $this->buscarFoto($codigo[$x]["imo_isn"],$this->CaminhoFoto(),$tam);
	   }
	   return $codigo;		
	
	 }
	 
	 function buscaTipo($isn) {
	    $idm = $_SESSION['idioma'];	   
		if($idm == 2) {
		   $textDestq = "TIM_DES_ING";
		}else if($idm == 3){
		   $textDestq = "TIM_DES_ESP";
		}else {
		   $textDestq = "TIM_DES";
		}
	   $tipoesp = "";
	   $ass = $this->getASS_ISN();
	   $sql = "SELECT * FROM tab_tim WHERE TIM_ISN = $isn AND (ASS_ISN IS NULL OR ASS_ISN = $ass) ";
	   $res = $this->imobredes->query($sql);
	   if($res->num_rows() < 1){
		 return false;
	   }
	   foreach($res->result() as $array){        	   
			if($idm == 2) {
			   $tipoesp  = utf8_decode($array->TIM_DES_ING);
			}else if($idm == 3){
			   $tipoesp  = utf8_decode($array->TIM_DES_ESP);
			}else {
			   $tipoesp  = utf8_decode($array->TIM_DES);
			}
			if(!$tipoesp){
				$tipoesp  = utf8_decode($array->TIM_DES); 
			}
	   }
	   return $tipoesp;	 
	 }
	 
	 public function buscarFoto($imo_isn,$caminho,$tamanho=130,$local=''){
		$ass = $this->getASS_ISN();
		$sql = "SELECT * FROM tab_imi IMI
					INNER JOIN tab_imo IMO ON IMI.IMO_ISN = IMO.IMO_ISN
					WHERE IMO.ASS_ISN = $ass AND IMI.IMO_ISN = $imo_isn AND IMI.IMI_TIP_DES = 1";
		$res = $this->imobredes->query($sql);
	    if($res->num_rows() < 1){
		   $foto = $local.'application/helpers/inc_thumb.php?img=../images/indisponivel.jpg&size=200';
		   return $foto;
	    }
		foreach($res->result() as $array){
			$foto = 'http://www.imobredes.com.br/inc_thumb.php?img='.$array->IMI_DES_ARQ.'&size='.$tamanho; 
		}
		
		return $foto;
	}
	
	 function consultarTipo() {
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descTip = "TIM_DES_ING";
	   }else if($idm == 3){
	   	  $descTip = "TIM_DES_ESP";
	   }else {
	      $descTip = "TIM_DES";
	   }
	   $i = 0;
	   $tipoesp = "";
	   $ass = $this->getASS_ISN();
	   $sql = "SELECT T.* FROM tab_tim T 
			   INNER JOIN tab_tia TI ON T.TIM_ISN = TI.TIM_ISN
			   WHERE TI.ASS_ISN = $ass ORDER BY T.TIM_DES";
	   $res = $this->imobredes->query($sql);
	   if($res->num_rows() < 1){
		   return false;
	   }
	   foreach($res->result() as $array){
         	$tipoesp[$i]["tim_id"]        = $array->TIM_ISN;
			if($idm == 2) {
			   $tipoesp[$i]["tim_des"]  = utf8_decode($array->TIM_DES_ING);
			}else if($idm == 3){
			   $tipoesp[$i]["tim_des"]  = utf8_decode($array->TIM_DES_ESP);
			}else {
			   $tipoesp[$i]["tim_des"]  = utf8_decode($array->TIM_DES);
			}
			if(!$tipoesp[$i]["tim_des"]){
				$tipoesp[$i]["tim_des"]  = utf8_decode($array->TIM_DES); 
			}
			$i++; 
	   }
	   return $tipoesp;
	 }
	 
	 function consultarBairros($cid) {
	   $i = 0;
	   $bairro = "";
	   $ass = $this->getASS_ISN();
	  $sql = "SELECT DISTINCT IMO.IMO_DES_BAI FROM tab_imo IMO
	   		   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   WHERE IMO.ASS_ISN = $ass AND LOC.LOC_NOM LIKE '$cid' AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 ORDER BY IMO.IMO_DES_BAI";
	   $res = $this->imobredes->query($sql);
	   if($res->num_rows() < 1){
		   return false;
	   }
	   foreach($res->result() as $array){
			$bairro[$i]["imo_des_bai"]           = utf8_decode($array->IMO_DES_BAI); 	   
            $i++; 
	   }
	   return $bairro;
	 }
	 
	 function consultarCidades() {
	   $i = 0;
	   $cidade = "";
	   $ass = $this->getASS_ISN();
	   $sql = "SELECT DISTINCT LOC.LOC_NOM FROM tab_loc LOC
	   		   INNER JOIN tab_imo IMO ON LOC.LOC_ISN = IMO.LOC_ISN
			   WHERE IMO.ASS_ISN = $ass AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0
			   ORDER BY LOC.LOC_NOM";
	   $res = $this->imobredes->query($sql);
	   if($res->num_rows() < 1){
		   return false;
	   }
	   foreach($res->result() as $array){
			$cidade[$i]["imo_des_loc"]           = utf8_decode($array->LOC_NOM); 	   
            $i++; 
	   }	
	   return $cidade;
	 }
	 function carregarCodigosPOS($id,$tipo,$cidade) {
	   $ret="";
	   if(is_array($id)){	
		   $codimo = "";
		   $ass = $con->getASS_ISN();
		   $sql = "SELECT IMC.IMO_ISN FROM tab_imc IMC 
				   INNER JOIN tab_imo IMO ON IMC.IMO_ISN = IMO.IMO_ISN
				   INNER JOIN tab_loc ON LOC.LOC_ISN = IMO.LOC_ISN
				   WHERE IMO.TIM_ISN = $tipo AND LOC.LOC_NOM = '$cidade' AND IMO.ASS_ISN = $ass ";
		   foreach($id as $i){
		   		if($i){
					$sql.=" AND IMC.IMO_ISN IN (SELECT DISTINCT(IMC2.IMO_ISN) FROM tab_imc IMC2 INNER JOIN tab_imo IMO2 ON IMC2.IMO_ISN = IMO2.IMO_ISN WHERE IMO2.ASS_ISN = $ass AND IMC2.DCA_ISN = $i) ";
				}
		   }
		   $sql.=" AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 GROUP BY IMC.IMO_ISN ORDER BY IMC.IMO_ISN ";
		   $res = $this->imobredes->query($sql);
		   if($res->num_rows() < 1){
			   return false;
		   }
		   $ret="";
		   foreach($res->result() as $array){
				$ret.=$array->IMO_ISN.','; 	   
		   }
		   if($ret) { $ret.="0"; } 
	   }
	   return $ret;
	}
	 
	 function consultarImovel($codigo,$tam=300,$local) {
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descTip = "TIM_DES_ING";
	   }else if($idm == 3){
	   	  $descTip = "TIM_DES_ESP";
	   }else {
	      $descTip = "TIM_DES";
	   }
	   $i = 0;
	   $imovel = "";
	   $ass = $this->getASS_ISN();
	   $param = $this->getParam();
	   $sql = "SELECT TIM.TIM_DES,TIM.TIM_DES_ESP,TIM.TIM_DES_ING,LOC.LOC_NOM,IMO.* FROM tab_imo IMO
	   		   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
	           WHERE IMO.ASS_ISN = $ass AND IMO.IMO_ISN = $codigo
			   AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 
			   ORDER BY IMO.IMO_ISN";
	   $res = $this->imobredes->query($sql);
	   if($res->num_rows() < 1){
		   return false;
	   }
	   foreach($res->result() as $array){
			$imovel[$i]["imo_isn"]           = $array->IMO_ISN; 
			$imovel[$i]["emp_isn"]           = 0;
			$imovel[$i]["imo_tip_imo"]       = $array->IMO_TIP_IMO;
			$imovel[$i]["imo_ref"]           = $array->IMO_DES_REF;
			$imovel[$i]["imo_cod_sis"]       = $array->IMO_COD_IMO_SIS;	   
			$imovel[$i]["imo_des_bai"]       = utf8_decode($array->IMO_DES_BAI); 
			if($idm == 2) {
			   $imovel[$i]["imo_des_txt"]  = utf8_decode($array->IMO_DES_TXT_ING);
			}else if($idm == 3){
			   $imovel[$i]["imo_des_txt"]  = utf8_decode($array->IMO_DES_TXT_ESP);
			}else {
			   $imovel[$i]["imo_des_txt"]  = utf8_decode($array->IMO_DES_TXT);
			}
			if(!$imovel[$i]["imo_des_txt"]){
				$imovel[$i]["imo_des_txt"]  = utf8_decode($array->IMO_DES_TXT);
			}
			if($array->IMO_TIP_EXC==1){
				if($param["PAA_TIP_EXIB_END_IMO"]==1){	   
					if($param["PAA_TIP_EXIB_NUM_END_IMO"]==1){
						$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." n&deg; ".$array->IMO_NUM_END." - ".$array->LOC_NOM);
					}else {
						$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." - ".$array->LOC_NOM);
					}
				}else {
					$imovel[$i]["imo_des_end"]       = "Consulte nossos corretores";
				}	
			}else {
				$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." n&deg; ".$array->IMO_NUM_END." - ".$array->LOC_NOM);
			}
			$imovel[$i]["imo_des_loc"]       = utf8_decode($array->LOC_NOM);	   
			if($array->IMO_TIP_EXIB_PRECO_SITE==1){				   
				$imovel[$i]["imo_val_ven"]       = $array->IMO_VAL_VEN; 
			}else {
				$imovel[$i]["imo_val_ven"]       = "Consulte nossos corretores"; 
			} 
			$imovel[$i]["imo_val_are"]       = $array->IMO_VAL_ARE_TOT;
			$imovel[$i]["imo_val_are_priv"]  = $array->IMO_VAL_ARE_PRIV;
			$imovel[$i]["imo_val_frente"]    = $array->IMO_VAL_MED_FRENTE;
			$imovel[$i]["imo_val_fundo"]     = $array->IMO_VAL_MED_FUNDOS;
			$imovel[$i]["imo_val_sal_dev"]   = $array->IMO_VAL_SAL_DEV;
			$imovel[$i]["imo_val_agio"]      = $array->IMO_VAL_AGIO;
			$imovel[$i]["imo_val_prest"]     = $array->IMO_VAL_PARC;
			$imovel[$i]["imo_qtd_prest"]     = $array->IMO_QTD_PARC;
			$imovel[$i]["imo_ano_const"]     = $array->IMO_ANO_CONST;
			$imovel[$i]["imo_tip_ocup"]      = $array->IMO_TIP_OCUP; 
			$imovel[$i]["imo_tip_usar"]      = 0; 
			//$imovel[$i]["imo_dest_det"]      = 0; 
			$imovel[$i]["imo_dest_det"]      = utf8_decode($array->IMO_DES_TXT); 
			$imovel[$i]["imo_dest_det_2"]    = 0; 
			$imovel[$i]["imo_des_est"]       = 0; 	   			
			if($imovel[$i]["imo_tip_ocup"] == 0) {
			   $imovel[$i]["imo_tipo_ocupacao"] = "NAO";
			}else if($imovel[$i]["imo_tip_ocup"] == 1) {
			   $imovel[$i]["imo_tipo_ocupacao"] = "SIM";
			}
			if($idm == 2) {
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES_ING);
			}else if($idm == 3){
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES_ESP);
			}else {
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES);
			}
			if(!$imovel[$i]["tim_des"]){
				$imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES);	
			}
					  
         $i++; 
	   }
	   is_array($imovel)?$quant=count($imovel):$quant=0;
	   for($x=0;$x<$quant;$x++){
	   		$imovel[$x]["caracs"] = $this->consultarCaracteristicaPai($codigo);
			$imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
			$imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam*2,$local);
	   }	
	   return $imovel;
	}
	
	function consultarImovel2($codigo,$tam=300,$local) {
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descTip = "TIM_DES_ING";
	   }else if($idm == 3){
	   	  $descTip = "TIM_DES_ESP";
	   }else {
	      $descTip = "TIM_DES";
	   }
	   $i = 0;
	   $imovel = "";
	   $ass = $this->getASS_ISN();
	   $param = $this->getParam();
	   if(is_numeric($codigo)){ $codigo=(int)$codigo; }
	   $sql = "SELECT TIM.TIM_DES,TIM.TIM_DES_ESP,TIM.TIM_DES_ING,LOC.LOC_NOM,IMO.* FROM tab_imo IMO
	   		   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
	           WHERE IMO.ASS_ISN = $ass AND (IMO.IMO_ISN = '$codigo' OR IMO.IMO_DES_REF = '$codigo' OR IMO_COD_IMO_SIS='$codigo') AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 
			   ORDER BY IMO.IMO_ISN";
	   $res = $this->imobredes->query($sql);
	   if($res->num_rows() < 1){
		   return false;
	   }
	   foreach($res->result() as $array){
			$imovel[$i]["imo_isn"]           = $array->IMO_ISN; 
			$imovel[$i]["emp_isn"]           = 0;
			$imovel[$i]["imo_tip_imo"]       = $array->IMO_TIP_IMO;
			$imovel[$i]["imo_ref"]           = $array->IMO_DES_REF;
			$imovel[$i]["imo_cod_sis"]       = $array->IMO_COD_IMO_SIS;	   
			$imovel[$i]["imo_des_bai"]       = utf8_decode($array->IMO_DES_BAI); 
			if($array->IMO_TIP_EXC==1){
				if($param["PAA_TIP_EXIB_END_IMO"]==1){	   
					if($param["PAA_TIP_EXIB_NUM_END_IMO"]==1){
						$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." n&deg; ".$array->IMO_NUM_END." - ".$array->LOC_NOM);
					}else {
						$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." - ".$array->LOC_NOM);
					}
				}else {
					$imovel[$i]["imo_des_end"]       = "Consulte nossos corretores";
				}	
			}else {
				$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." n&deg; ".$array->IMO_NUM_END." - ".$array->LOC_NOM);
			}
			$imovel[$i]["imo_des_loc"]       = utf8_decode($array->LOC_NOM);	   
			if($array->IMO_TIP_EXIB_PRECO_SITE==1){				   
				$imovel[$i]["imo_val_ven"]       = $array->IMO_VAL_VEN; 
			}else {
				$imovel[$i]["imo_val_ven"]       = "Consulte nossos corretores"; 
			} 
			$imovel[$i]["imo_val_are"]       = $array->IMO_VAL_ARE_TOT;
			$imovel[$i]["imo_val_are_priv"]  = $array->IMO_VAL_ARE_PRIV;
			$imovel[$i]["imo_val_frente"]    = $array->IMO_VAL_MED_FRENTE;
			$imovel[$i]["imo_val_fundo"]     = $array->IMO_VAL_MED_FUNDOS;
			$imovel[$i]["imo_val_sal_dev"]   = $array->IMO_VAL_SAL_DEV;
			$imovel[$i]["imo_val_agio"]      = $array->IMO_VAL_AGIO;
			$imovel[$i]["imo_val_prest"]     = $array->IMO_VAL_PARC;
			$imovel[$i]["imo_qtd_prest"]     = $array->IMO_QTD_PARC;
			$imovel[$i]["imo_ano_const"]     = $array->IMO_ANO_CONST;
			$imovel[$i]["imo_tip_ocup"]      = $array->IMO_TIP_OCUP; 
			$imovel[$i]["imo_tip_usar"]      = 0; 
			$imovel[$i]["imo_dest_det"]      = 0; 
			$imovel[$i]["imo_dest_det_2"]    = 0; 
			$imovel[$i]["imo_des_est"]       = 0; 	   			
			if($imovel[$i]["imo_tip_ocup"] == 0) {
			   $imovel[$i]["imo_tipo_ocupacao"] = "NAO";
			}else if($imovel[$i]["imo_tip_ocup"] == 1) {
			   $imovel[$i]["imo_tipo_ocupacao"] = "SIM";
			}
			if($idm == 2) {
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES_ING);
			}else if($idm == 3){
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES_ESP);
			}else {
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES);
			}
			if(!$imovel[$i]["tim_des"]){
				$imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES);	
			}			  
         $i++; 
	   }
	   is_array($imovel)?$quant=count($imovel):$quant=0;
	   for($x=0;$x<$quant;$x++){
	   		$imovel[$x]["caracs"] = $this->consultarCaracteristicaPai($imovel[$x]["imo_isn"]);
			$imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
			$imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam*2,$local);
	   }	
	   return $imovel;
	}
	
	public function buscarFoto2($imo_isn,$caminho,$tamanho=300,$local=''){
		$ass = $this->getASS_ISN();
		$sql = "SELECT * FROM tab_imi IMI
				INNER JOIN tab_imo IMO ON IMI.IMO_ISN = IMO.IMO_ISN
				WHERE IMO.ASS_ISN = $ass AND IMI.IMO_ISN = $imo_isn AND IMI.IMI_TIP_DES = 1";
		$res = $this->imobredes->query($sql);
		$foto ="";
	    if($res->num_rows() < 1){
		   $sql = "SELECT * FROM tab_imi IMI
				   INNER JOIN tab_imo IMO ON IMI.IMO_ISN = IMO.IMO_ISN
				   WHERE IMO.ASS_ISN = $ass AND IMI.IMO_ISN = $imo_isn ORDER BY IMI.IMI_SEQ ";
		   $res = $this->imobredes->query($sql);
		   if($res->num_rows() < 1){
		   		$foto = $local.'application/helpers/inc_thumb.php?img=../images/indisponivel.jpg&size=200';
				return $foto;
		   }
		   foreach($res->result() as $array){
				$foto = 'http://www.imobredes.com.br/inc_thumb.php?img='.$array->IMI_DES_ARQ.'&size='.$tamanho;
		   }	
			
	    }
		foreach($res->result() as $array){
			$foto = 'http://www.imobredes.com.br/inc_thumb.php?img='.$array->IMI_DES_ARQ.'&size='.$tamanho;
		}
		if(empty($foto)){
			$foto = $local.'application/helpers/inc_thumb.php?img=../images/indisponivel.jpg&size=200';
		}		
		return $foto;
	}
	
	public function carregarFotos($cod,$caminho,$tamanho=130,$local='') {
	   $idm = $_SESSION['idioma'];
	   $ass = $this->getASS_ISN();
	   $sql = "SELECT * FROM tab_imi IMI
			   INNER JOIN tab_imo IMO ON IMI.IMO_ISN = IMO.IMO_ISN
			   WHERE IMO.ASS_ISN = $ass AND IMI.IMO_ISN = $cod ORDER BY IMI.IMI_SEQ";
	   $i = 0;	   
	   $res = $this->imobredes->query($sql);	
	   if($res->num_rows() < 1){
		   return false;
	   }
	   $img = "";
	   foreach($res->result() as $array){
			if($idm == 2) {
			   $img[$i]["descricao"] = $array->IMI_DES_ING;
			}else if($idm == 3){
			   $img[$i]["descricao"] = $array->IMI_DES_ESP;
			}else {
			   $img[$i]["descricao"] = $array->IMI_DES;
			}
			if(!$img[$i]["descricao"]){
				$img[$i]["descricao"] = $array->IMI_DES;
			}   
			$img[$i]["nome"] = 'http://www.imobredes.com.br/verificafoto2.php?img='.$array->IMI_DES_ARQ.'&size='.$tamanho; 
			$img[$i]["codigo"]    = $array->IMI_ISN; 
			$i++;
	   }
	   return $img;
	 }
	
	 function consultarImoveis($bairro,$tipo,$valini,$valfim,$cidade,$tam=300,$local,$dte="") {
	   $idm = $_SESSION['idioma'];
	   $br = "";
	   if(is_array($bairro)){
			$cont = count($bairro);
			if($bairro[0] == "TODOS" || $bairro[0]==" "){
				$br="";
			}else {		
				for($i=0;$i<$cont;$i++) {		  
				   if($cont == 1) {
					  $br = "AND IMO.IMO_DES_BAI='".$bairro[$i]."'";
				   }else if($cont > 1) {
					 if($i==0){
						$br.= " AND ( IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }else if($i == ($cont - 1)) {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' ) ";
					 }else {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }
				   }
				   
				}
			}
	   }else {
	   	  $br = "";
	   }
	   $bairro = $br;
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descTip = "TIM_DES_ING";
	   }else if($idm == 3){
	   	  $descTip = "TIM_DES_ESP";
	   }else {
	      $descTip = "TIM_DES";
	   }
	   $i = 0;
	   $imovel = "";
	   $ass = $this->getASS_ISN();
	   $param = $this->getParam();
	   if(empty($cidade) && !empty($tipo)){
	   	    $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST , IMO.IMO_TIP_EXIB_PRECO_SITE 
			   FROM tab_imo IMO
			   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
			   WHERE IMO.ASS_ISN = $ass AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND TIM.TIM_ISN = $tipo $bairro ";
	   }else  if(empty($cidade) && empty($tipo)){
		   $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST , IMO.IMO_TIP_EXIB_PRECO_SITE 
				   FROM tab_imo IMO
				   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   	   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
				   WHERE IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND IMO.ASS_ISN = $ass $bairro ";
		}else if(!empty($cidade) && empty($tipo)){
	   	    $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST , IMO.IMO_TIP_EXIB_PRECO_SITE 
			   FROM tab_imo IMO
			   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
			   WHERE IMO.ASS_ISN = $ass AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND LOC.LOC_NOM = '$cidade' $bairro ";
	   }else{
			$sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST, IMO.IMO_TIP_EXIB_PRECO_SITE 
				   FROM tab_imo IMO
				   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   	   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
				   WHERE IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND IMO.ASS_ISN = $ass AND LOC.LOC_NOM = '$cidade' 
				   AND TIM.TIM_ISN = $tipo $bairro ";
		}  
		if($valini) { 
	      $valini = str_replace(",",".",$valini);
		  $sql = $sql."AND IMO.IMO_VAL_VEN >= $valini ";
		}	 
		if($valfim) { 
		  $valfim = str_replace(",",".",$valfim);
	      $sql = $sql."AND IMO.IMO_VAL_VEN <= $valfim ";
		}	 
		if($dte>0){
			$sql = $sql." AND DTE_ISN = $dte ";
		}
		$sql = $sql."ORDER BY IMO.IMO_VAL_VEN";
	    $res = $this->imobredes->query($sql);	
	    if($res->num_rows() < 1){
		   return false;
	    }
	    foreach($res->result() as $array){
			$imovel[$i]["imo_isn"]           = $array->IMO_ISN; 
			$imovel[$i]["imo_ref"]           = $array->IMO_DES_REF;
			$imovel[$i]["imo_tip_imo"]       = $array->IMO_TIP_IMO;
			$imovel[$i]["imo_cod_sis"]       = $array->IMO_COD_IMO_SIS;	   
			$imovel[$i]["imo_des_bai"]       = utf8_decode($array->IMO_DES_BAI); 
			$imovel[$i]["imo_des_txt_dest"]       = utf8_decode($array->IMO_DES_TXT_DEST);
			if($array->IMO_TIP_EXC==1){
				if($param["PAA_TIP_EXIB_END_IMO"]==1){	   
					if($param["PAA_TIP_EXIB_NUM_END_IMO"]==1){
						$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." n&deg;".$array->IMO_NUM_END." - ".$array->LOC_NOM);
					}else {
						$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." - ".$array->LOC_NOM);
					}
				}else {
					$imovel[$i]["imo_des_end"]       = "Consulte nossos corretores";
				}
			}else {
				$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." n&deg;".$array->IMO_NUM_END." - ".$array->LOC_NOM);
			}
			$imovel[$i]["imo_des_loc"]       = $array->LOC_NOM; 	   
			if($array->IMO_TIP_EXIB_PRECO_SITE==1){				   
				$imovel[$i]["imo_val_ven"]       = $array->IMO_VAL_VEN; 
			}else {
				$imovel[$i]["imo_val_ven"]       = "Consulte nossos corretores"; 
			} 	   
			$imovel[$i]["imo_val_are"]       = $array->IMO_VAL_ARE_TOT;
			$imovel[$i]["imo_val_are_priv"]  = $array->IMO_VAL_ARE_PRIV;
			$imovel[$i]["imo_val_frente"]    = $array->IMO_VAL_MED_FRENTE;
			$imovel[$i]["imo_val_fundo"]     = $array->IMO_VAL_MED_FUNDOS;
			$imovel[$i]["imo_val_sal_dev"]   = $array->IMO_VAL_SAL_DEV;
			$imovel[$i]["imo_val_agio"]      = $array->IMO_VAL_AGIO;
			$imovel[$i]["imo_val_prest"]     = $array->IMO_VAL_PREST;
			$imovel[$i]["imo_qtd_prest"]     = $array->IMO_QTD_PREST;
			$imovel[$i]["imo_ano_const"]     = $array->IMO_ANO_CONST;
			$imovel[$i]["imo_tip_ocup"]      = $array->IMO_TIP_OCUP;
			if($imovel[$i]["imo_tip_ocup"] == 0) {
			   $imovel[$i]["imo_tipo_ocupacao"] = "NAO";
			}else if($imovel[$i]["imo_tip_ocup"] == 1) {
			   $imovel[$i]["imo_tipo_ocupacao"] = "SIM";
			}
			if($idm == 2) {
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES_ING);
			}else if($idm == 3){
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES_ESP);
			}else {
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES);
			}
			if(!$imovel[$i]["tim_des"]){
				$imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES);	
			}    
            $i++; 
	   }
	   for($x=0;$x<count($imovel);$x++){
			$imovel[$x]["caracs"] = $this->consultarCaracteristicaPai($imovel[$x]["imo_isn"]);
			$imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
			$imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam*2,$local);
	   }		
	   return $imovel;
	}
	
	function consultarImoveisUsados($bairro,$tipo,$valini,$valfim,$cidade,$tam=300,$local) {
	   $idm = $_SESSION['idioma'];
	   $br = "";
	   if(is_array($bairro)){
			$cont = count($bairro);
			if($bairro[0] == "TODOS" || $bairro[0]==" "){
				$br="";
			}else {		
				for($i=0;$i<$cont;$i++) {		  
				   if($cont == 1) {
					  $br = "AND IMO.IMO_DES_BAI='".$bairro[$i]."'";
				   }else if($cont > 1) {
					 if($i==0){
						$br.= " AND ( IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }else if($i == ($cont - 1)) {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' ) ";
					 }else {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }
				   }
				   
				}
			}
	   }else {
	   	  $br = "";
	   }
	   $bairro = $br;
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descTip = "TIM_DES_ING";
	   }else if($idm == 3){
	   	  $descTip = "TIM_DES_ESP";
	   }else {
	      $descTip = "TIM_DES";
	   }
	   $i = 0;
	   $imovel = "";
	   $ass = $this->getASS_ISN();
	   $param = $this->getParam();
	   if(empty($cidade) && !empty($tipo)){
	   	    $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST , IMO.IMO_TIP_EXIB_PRECO_SITE 
			   FROM tab_imo IMO
			   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
			   WHERE IMO.ASS_ISN = $ass AND (IMO.IMO_TIP_IMO = 0 OR IMO.IMO_TIP_IMO IS NULL) AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND TIM.TIM_ISN = $tipo $bairro ";
	   }else  if(empty($cidade) && empty($tipo)){
		   $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST , IMO.IMO_TIP_EXIB_PRECO_SITE 
				   FROM tab_imo IMO
				   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   	   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
				   WHERE (IMO.IMO_TIP_IMO = 0 OR IMO.IMO_TIP_IMO IS NULL) AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND IMO.ASS_ISN = $ass $bairro ";
		}else if(!empty($cidade) && empty($tipo)){
	   	    $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST , IMO.IMO_TIP_EXIB_PRECO_SITE 
			   FROM tab_imo IMO
			   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
			   WHERE IMO.ASS_ISN = $ass AND (IMO.IMO_TIP_IMO = 0 OR IMO.IMO_TIP_IMO IS NULL) AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND LOC.LOC_NOM = '$cidade' $bairro ";
	   }else{
			$sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST, IMO.IMO_TIP_EXIB_PRECO_SITE 
				   FROM tab_imo IMO
				   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   	   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
				   WHERE (IMO.IMO_TIP_IMO = 0 OR IMO.IMO_TIP_IMO IS NULL) AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND IMO.ASS_ISN = $ass AND LOC.LOC_NOM = '$cidade' 
				   AND TIM.TIM_ISN = $tipo $bairro ";
		}  
		if($valini) { 
	      $valini = str_replace(",",".",$valini);
		  $sql = $sql."AND IMO.IMO_VAL_VEN >= $valini ";
		}	 
		if($valfim) { 
		  $valfim = str_replace(",",".",$valfim);
	      $sql = $sql."AND IMO.IMO_VAL_VEN <= $valfim ";
		}	 
		$sql = $sql."ORDER BY IMO.IMO_VAL_VEN";
	    $res = $this->imobredes->query($sql);	
	    if($res->num_rows() < 1){
		   return false;
	    }
	    foreach($res->result() as $array){
			$imovel[$i]["imo_isn"]           = $array->IMO_ISN; 
			$imovel[$i]["imo_ref"]           = $array->IMO_DES_REF;
			$imovel[$i]["imo_tip_imo"]       = $array->IMO_TIP_IMO;
			$imovel[$i]["imo_cod_sis"]       = $array->IMO_COD_IMO_SIS;	   
			$imovel[$i]["imo_des_bai"]       = utf8_decode($array->IMO_DES_BAI); 
			if($array->IMO_TIP_EXC==1){
				if($param["PAA_TIP_EXIB_END_IMO"]==1){	   
					if($param["PAA_TIP_EXIB_NUM_END_IMO"]==1){
						$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." n&deg;".$array->IMO_NUM_END." - ".$array->LOC_NOM);
					}else {
						$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." - ".$array->LOC_NOM);
					}
				}else {
					$imovel[$i]["imo_des_end"]       = "Consulte nossos corretores";
				}
			}else {
				$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." n&deg;".$array->IMO_NUM_END." - ".$array->LOC_NOM);
			}
			$imovel[$i]["imo_des_loc"]       = $array->LOC_NOM; 	   
			if($array->IMO_TIP_EXIB_PRECO_SITE==1){				   
				$imovel[$i]["imo_val_ven"]       = $array->IMO_VAL_VEN; 
			}else {
				$imovel[$i]["imo_val_ven"]       = "Consulte nossos corretores"; 
			} 	   
			$imovel[$i]["imo_val_are"]       = $array->IMO_VAL_ARE_TOT;
			$imovel[$i]["imo_val_are_priv"]  = $array->IMO_VAL_ARE_PRIV;
			$imovel[$i]["imo_val_frente"]    = $array->IMO_VAL_MED_FRENTE;
			$imovel[$i]["imo_val_fundo"]     = $array->IMO_VAL_MED_FUNDOS;
			$imovel[$i]["imo_val_sal_dev"]   = $array->IMO_VAL_SAL_DEV;
			$imovel[$i]["imo_val_agio"]      = $array->IMO_VAL_AGIO;
			$imovel[$i]["imo_val_prest"]     = $array->IMO_VAL_PREST;
			$imovel[$i]["imo_qtd_prest"]     = $array->IMO_QTD_PREST;
			$imovel[$i]["imo_ano_const"]     = $array->IMO_ANO_CONST;
			$imovel[$i]["imo_tip_ocup"]      = $array->IMO_TIP_OCUP;
			if($imovel[$i]["imo_tip_ocup"] == 0) {
			   $imovel[$i]["imo_tipo_ocupacao"] = "NAO";
			}else if($imovel[$i]["imo_tip_ocup"] == 1) {
			   $imovel[$i]["imo_tipo_ocupacao"] = "SIM";
			}
			if($idm == 2) {
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES_ING);
			}else if($idm == 3){
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES_ESP);
			}else {
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES);
			}
			if(!$imovel[$i]["tim_des"]){
				$imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES);	
			}    
            $i++; 
	   }
	   for($x=0;$x<count($imovel);$x++){
			$imovel[$x]["caracs"] = $this->consultarCaracteristicaPai($imovel[$x]["imo_isn"]);
			$imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
			$imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam*2,$local);
	   }		
	   return $imovel;
	}
	
	function consultarImoveisLancs($bairro,$tipo,$valini,$valfim,$cidade,$tam=300,$local) {
	   $idm = $_SESSION['idioma'];
	   $br = "";
	   if(is_array($bairro)){
			$cont = count($bairro);
			if($bairro[0] == "TODOS" || $bairro[0]==" "){
				$br="";
			}else {		
				for($i=0;$i<$cont;$i++) {		  
				   if($cont == 1) {
					  $br = "AND IMO.IMO_DES_BAI='".$bairro[$i]."'";
				   }else if($cont > 1) {
					 if($i==0){
						$br.= " AND ( IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }else if($i == ($cont - 1)) {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' ) ";
					 }else {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }
				   }
				   
				}
			}
	   }else {
	   	  $br = "";
	   }
	   $bairro = $br;
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descTip = "TIM_DES_ING";
	   }else if($idm == 3){
	   	  $descTip = "TIM_DES_ESP";
	   }else {
	      $descTip = "TIM_DES";
	   }
	   $i = 0;
	   $imovel = "";
	   $ass = $this->getASS_ISN();
	   $param = $this->getParam();
	   if(empty($cidade) && !empty($tipo)){
	   	    $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST , IMO.IMO_TIP_EXIB_PRECO_SITE 
			   FROM tab_imo IMO
			   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
			   WHERE IMO.ASS_ISN = $ass AND IMO.IMO_TIP_IMO = 1 AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND TIM.TIM_ISN = $tipo $bairro ";
	   }else  if(empty($cidade) && empty($tipo)){
		   $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST , IMO.IMO_TIP_EXIB_PRECO_SITE 
				   FROM tab_imo IMO
				   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   	   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
				   WHERE IMO.IMO_TIP_IMO = 1 AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND IMO.ASS_ISN = $ass $bairro ";
		}else if(!empty($cidade) && empty($tipo)){
	   	    $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST , IMO.IMO_TIP_EXIB_PRECO_SITE 
			   FROM tab_imo IMO
			   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
			   WHERE IMO.ASS_ISN = $ass  AND IMO.IMO_TIP_IMO = 1 AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND LOC.LOC_NOM = '$cidade' $bairro ";
	   }else{
			$sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST, IMO.IMO_TIP_EXIB_PRECO_SITE 
				   FROM tab_imo IMO
				   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   	   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
				   WHERE IMO.IMO_TIP_IMO = 1 AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND IMO.ASS_ISN = $ass AND LOC.LOC_NOM = '$cidade' 
				   AND TIM.TIM_ISN = $tipo $bairro ";
		}  
		if($valini) { 
	      $valini = str_replace(",",".",$valini);
		  $sql = $sql."AND IMO.IMO_VAL_VEN >= $valini ";
		}	 
		if($valfim) { 
		  $valfim = str_replace(",",".",$valfim);
	      $sql = $sql."AND IMO.IMO_VAL_VEN <= $valfim ";
		}	 
		$sql = $sql."ORDER BY IMO.IMO_VAL_VEN";
	    $res = $this->imobredes->query($sql);	
	    if($res->num_rows() < 1){
		   return false;
	    }
	    foreach($res->result() as $array){
			$imovel[$i]["imo_isn"]           = $array->IMO_ISN; 
			$imovel[$i]["imo_ref"]           = $array->IMO_DES_REF;
			$imovel[$i]["imo_tip_imo"]       = $array->IMO_TIP_IMO;
			$imovel[$i]["imo_cod_sis"]       = $array->IMO_COD_IMO_SIS;	   
			$imovel[$i]["imo_des_bai"]       = utf8_decode($array->IMO_DES_BAI); 
			if($array->IMO_TIP_EXC==1){
				if($param["PAA_TIP_EXIB_END_IMO"]==1){	   
					if($param["PAA_TIP_EXIB_NUM_END_IMO"]==1){
						$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." n&deg;".$array->IMO_NUM_END." - ".$array->LOC_NOM);
					}else {
						$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." - ".$array->LOC_NOM);
					}
				}else {
					$imovel[$i]["imo_des_end"]       = "Consulte nossos corretores";
				}
			}else {
				$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." n&deg;".$array->IMO_NUM_END." - ".$array->LOC_NOM);
			}
			$imovel[$i]["imo_des_loc"]       = $array->LOC_NOM; 	   
			if($array->IMO_TIP_EXIB_PRECO_SITE==1){				   
				$imovel[$i]["imo_val_ven"]       = $array->IMO_VAL_VEN; 
			}else {
				$imovel[$i]["imo_val_ven"]       = "Consulte nossos corretores"; 
			} 	   
			$imovel[$i]["imo_val_are"]       = $array->IMO_VAL_ARE_TOT;
			$imovel[$i]["imo_val_are_priv"]  = $array->IMO_VAL_ARE_PRIV;
			$imovel[$i]["imo_val_frente"]    = $array->IMO_VAL_MED_FRENTE;
			$imovel[$i]["imo_val_fundo"]     = $array->IMO_VAL_MED_FUNDOS;
			$imovel[$i]["imo_val_sal_dev"]   = $array->IMO_VAL_SAL_DEV;
			$imovel[$i]["imo_val_agio"]      = $array->IMO_VAL_AGIO;
			$imovel[$i]["imo_val_prest"]     = $array->IMO_VAL_PREST;
			$imovel[$i]["imo_qtd_prest"]     = $array->IMO_QTD_PREST;
			$imovel[$i]["imo_ano_const"]     = $array->IMO_ANO_CONST;
			$imovel[$i]["imo_tip_ocup"]      = $array->IMO_TIP_OCUP;
			if($imovel[$i]["imo_tip_ocup"] == 0) {
			   $imovel[$i]["imo_tipo_ocupacao"] = "NAO";
			}else if($imovel[$i]["imo_tip_ocup"] == 1) {
			   $imovel[$i]["imo_tipo_ocupacao"] = "SIM";
			}
			if($idm == 2) {
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES_ING);
			}else if($idm == 3){
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES_ESP);
			}else {
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES);
			}
			if(!$imovel[$i]["tim_des"]){
				$imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES);	
			}    
            $i++; 
	   }
	   for($x=0;$x<count($imovel);$x++){
			$imovel[$x]["caracs"] = $this->consultarCaracteristicaPai($imovel[$x]["imo_isn"]);
			$imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
			$imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam*2,$local);
	   }		
	   return $imovel;
	}
	
	function consultarImoveisDestaques($bairro,$tipo,$valini,$valfim,$cidade,$tam=300,$local) {
	   $idm = $_SESSION['idioma'];
	   $br = "";
	   if(is_array($bairro)){
			$cont = count($bairro);
			if($bairro[0] == "TODOS" || $bairro[0]==" "){
				$br="";
			}else {		
				for($i=0;$i<$cont;$i++) {		  
				   if($cont == 1) {
					  $br = "AND IMO.IMO_DES_BAI='".$bairro[$i]."'";
				   }else if($cont > 1) {
					 if($i==0){
						$br.= " AND ( IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }else if($i == ($cont - 1)) {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' ) ";
					 }else {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }
				   }
				   
				}
			}
	   }else {
	   	  $br = "";
	   }
	   $bairro = $br;
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descTip = "TIM_DES_ING";
	   }else if($idm == 3){
	   	  $descTip = "TIM_DES_ESP";
	   }else {
	      $descTip = "TIM_DES";
	   }
	   $i = 0;
	   $imovel = "";
	   $ass = $this->getASS_ISN();
	   $param = $this->getParam();
	   if(empty($cidade) && !empty($tipo)){
	   	    $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST , IMO.IMO_TIP_EXIB_PRECO_SITE 
			   FROM tab_imo IMO
			   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
			   WHERE IMO.ASS_ISN = $ass AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND IMO.IMO_TIP_DEST = 1 AND IMO.IMO_TIP_DEST_PRIM_PAG = 1 AND TIM.TIM_ISN = $tipo $bairro ";
	   }else  if(empty($cidade) && empty($tipo)){
		   $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST , IMO.IMO_TIP_EXIB_PRECO_SITE 
				   FROM tab_imo IMO
				   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   	   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
				   WHERE IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND IMO.IMO_TIP_DEST = 1 AND IMO.IMO_TIP_DEST_PRIM_PAG = 1 AND IMO.ASS_ISN = $ass $bairro ";
		}else if(!empty($cidade) && empty($tipo)){
	   	    $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST , IMO.IMO_TIP_EXIB_PRECO_SITE 
			   FROM tab_imo IMO
			   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
			   WHERE IMO.ASS_ISN = $ass AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND IMO.IMO_TIP_DEST = 1 AND IMO.IMO_TIP_DEST_PRIM_PAG = 1 AND LOC.LOC_NOM = '$cidade' $bairro ";
	   }else{
			$sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_REF,IMO_COD_IMO_SIS,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_VAL_MED_FUNDOS,IMO_NUM_END,IMO_TIP_EXC,IMO.IMO_TIP_IMO,
			   IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$descTip,
			   IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST, IMO.IMO_TIP_EXIB_PRECO_SITE 
				   FROM tab_imo IMO
				   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   	   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
				   WHERE IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND IMO.IMO_TIP_DEST = 1 AND IMO.IMO_TIP_DEST_PRIM_PAG = 1 AND IMO.ASS_ISN = $ass AND LOC.LOC_NOM = '$cidade' 
				   AND TIM.TIM_ISN = $tipo $bairro ";
		}  
		if($valini) { 
	      $valini = str_replace(",",".",$valini);
		  $sql = $sql."AND IMO.IMO_VAL_VEN >= $valini ";
		}	 
		if($valfim) { 
		  $valfim = str_replace(",",".",$valfim);
	      $sql = $sql."AND IMO.IMO_VAL_VEN <= $valfim ";
		}	 
		$sql = $sql."ORDER BY IMO.IMO_VAL_VEN";
	    $res = $this->imobredes->query($sql);	
	    if($res->num_rows() < 1){
		   return false;
	    }
	    foreach($res->result() as $array){
			$imovel[$i]["imo_isn"]           = $array->IMO_ISN; 
			$imovel[$i]["imo_ref"]           = $array->IMO_DES_REF;
			$imovel[$i]["imo_tip_imo"]       = $array->IMO_TIP_IMO;
			$imovel[$i]["imo_cod_sis"]       = $array->IMO_COD_IMO_SIS;	   
			$imovel[$i]["imo_des_bai"]       = utf8_decode($array->IMO_DES_BAI); 
			if($array->IMO_TIP_EXC==1){
				if($param["PAA_TIP_EXIB_END_IMO"]==1){	   
					if($param["PAA_TIP_EXIB_NUM_END_IMO"]==1){
						$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." n&deg;".$array->IMO_NUM_END." - ".$array->LOC_NOM);
					}else {
						$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." - ".$array->LOC_NOM);
					}
				}else {
					$imovel[$i]["imo_des_end"]       = "Consulte nossos corretores";
				}
			}else {
				$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." n&deg;".$array->IMO_NUM_END." - ".$array->LOC_NOM);
			}
			$imovel[$i]["imo_des_loc"]       = $array->LOC_NOM; 	   
			if($array->IMO_TIP_EXIB_PRECO_SITE==1){				   
				$imovel[$i]["imo_val_ven"]       = $array->IMO_VAL_VEN; 
			}else {
				$imovel[$i]["imo_val_ven"]       = "Consulte nossos corretores"; 
			} 	   
			$imovel[$i]["imo_val_are"]       = $array->IMO_VAL_ARE_TOT;
			$imovel[$i]["imo_val_are_priv"]  = $array->IMO_VAL_ARE_PRIV;
			$imovel[$i]["imo_val_frente"]    = $array->IMO_VAL_MED_FRENTE;
			$imovel[$i]["imo_val_fundo"]     = $array->IMO_VAL_MED_FUNDOS;
			$imovel[$i]["imo_val_sal_dev"]   = $array->IMO_VAL_SAL_DEV;
			$imovel[$i]["imo_val_agio"]      = $array->IMO_VAL_AGIO;
			$imovel[$i]["imo_val_prest"]     = $array->IMO_VAL_PREST;
			$imovel[$i]["imo_qtd_prest"]     = $array->IMO_QTD_PREST;
			$imovel[$i]["imo_ano_const"]     = $array->IMO_ANO_CONST;
			$imovel[$i]["imo_tip_ocup"]      = $array->IMO_TIP_OCUP;
			if($imovel[$i]["imo_tip_ocup"] == 0) {
			   $imovel[$i]["imo_tipo_ocupacao"] = "NAO";
			}else if($imovel[$i]["imo_tip_ocup"] == 1) {
			   $imovel[$i]["imo_tipo_ocupacao"] = "SIM";
			}
			if($idm == 2) {
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES_ING);
			}else if($idm == 3){
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES_ESP);
			}else {
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES);
			}
			if(!$imovel[$i]["tim_des"]){
				$imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES);	
			}    
            $i++; 
	   }
	   for($x=0;$x<count($imovel);$x++){
			$imovel[$x]["caracs"] = $this->consultarCaracteristicaPai($imovel[$x]["imo_isn"]);
			$imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
			$imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam*2,$local);
	   }		
	   return $imovel;
	}
	
	 function carregarCodigosALT($id,$tipo,$cidade) {
	   $ret="";
	   if(is_array($id)){
		   $codimo = "";
		   $ass = $this->getASS_ISN();
		   $sql = "SELECT IMC.IMO_ISN  FROM tab_imc IMC
		   		   INNER JOIN tab_imo IMO ON IMO.IMO_ISN = IMC.IMO_ISN
				   WHERE IMO.ASS_ISN = $ass AND IMO.TIM_ISN = $tipo ";
		   foreach($id as $i){
		   		if($i){
					$sql.=" AND IMC.IMO_ISN IN (SELECT DISTINCT(IMC2.IMO_ISN) FROM tab_imc IMC2 INNER JOIN tab_imo IMO2 ON IMC2.IMO_ISN = IMO2.IMO_ISN WHERE IMO2.ASS_ISN = $ass AND IMC2.CAR_ISN = $i) ";
				}
		   }
		   $sql.=" AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 GROUP BY IMC.IMO_ISN";
		   $res = $this->imobredes->query($sql);	
		   if($res->num_rows() < 1){
			   return false;
		   }
		   $ret="";
		   foreach($res->result() as $array){
				$ret.=$array->IMO_ISN.','; 	   
		   }
		   if($ret) { $ret.="0"; }
	   }	
	   return $ret;
	}
	
	 function carregarCodigosQTDE($array,$tipo,$cidade) {
	   $ret="";
	   if(is_array($array)){
		  $codimo = "";
		  $ass = $this->getASS_ISN();
		  $sql = "SELECT IMC.IMO_ISN FROM tab_imc IMC 
					INNER JOIN tab_imo IMO ON IMC.IMO_ISN = IMO.IMO_ISN
					WHERE IMO.ASS_ISN = $ass AND IMO.TIM_ISN = $tipo ";
		   foreach($array as $ar){
		   		if($ar){
					  $sql.=" AND IMC.IMO_ISN IN ( SELECT DISTINCT(IMC2.IMO_ISN) FROM tab_imc IMC2 INNER JOIN tab_imo IMO2 ON IMC2.IMO_ISN = IMO2.IMO_ISN WHERE IMO2.ASS_ISN = $ass AND IMC2.CAR_ISN = ".$ar['id']." ";
					 if(!$ar['ini'] && $ar['fim']) {
						$sql.= " AND IMC2.IMC_QTD <= ".$ar['fim']." ) ";
					 }else if($ar['ini'] && !$ar['fim']) {
						$sql.= "AND IMC2.IMC_QTD >= ".$ar['ini']." ) ";
					 }else if($array[0]['ini'] && $ar['fim']) {
						$sql.= " AND IMC2.IMC_QTD >= ".$ar['ini']." AND IMC2.IMC_QTD <= ".$ar['fim']." ) ";
					 }
				}
		   }
		   $sql.=" AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 GROUP BY IMC.IMO_ISN ORDER BY IMC.IMO_ISN ";
		   $res = $this->imobredes->query($sql);	
		   if($res->num_rows() < 1){
			   return false;
		   }
		   $ret="";
		   foreach($res->result() as $array){
				$ret.=$array->IMO_ISN.','; 	   
		   }
		   if($ret) { $ret.="0"; }
	   }
	   return $ret;
	}
	
	 function carregarCodigosVALOR($array,$tipo,$cidade) {
	   $ret="";
	   if(is_array($array)){
		   $codimo = "";
			$sql = "SELECT IMC.IMO_ISN  FROM tab_imc IMC
		   		   INNER JOIN tab_imo IMO ON IMO.IMO_ISN = IMC.IMO_ISN
				   WHERE IMO.ASS_ISN = $ass AND IMO.TIM_ISN = $tipo ";
		   foreach($array as $ar){
		   		if($ar){
					 $sql.=" AND IMC.IMO_ISN IN ( SELECT DISTINCT(IMC2.IMO_ISN) FROM tab_imc IMC2 INNER JOIN tab_imo IMO2 ON IMC2.IMO_ISN = IMO2.IMO_ISN WHERE IMO2.ASS_ISN = $ass AND IMC2.CAR_ISN = ".$ar['id']." ";
					 if(!$ar['ini'] && $ar['fim']) {
						$sql.= " AND IMC2.IMC_VAL <= ".$ar['fim']." ) ";
					 }else if($ar['ini'] && !$ar['fim']) {
						$sql.= "AND IMC2.IMC_VAL >= ".$ar['ini']." ) ";
					 }else if($array[0]['ini'] && $ar['fim']) {
						$sql.= " AND IMC2.IMC_VAL >= ".$ar['ini']." AND IMC2.IMC_VAL <= ".$ar['fim']." ) ";
					 }
				}
		   }
		   $sql.=" AND IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 GROUP BY IMC.IMO_ISN ";
		   $res = $this->imobredes->query($sql);	
		   if($res->num_rows() < 1){
			   return false;
		   }
		   $ret="";
		   foreach($res->result() as $array){
				$ret.=$array->IMO_ISN.','; 	   
		   }
		   if($ret) { $ret.="0"; }
	   }
	   return $ret;
	}
	
	 function carregarImoveisAVC($bairro,$tipo,$codigoVal,$codigoQtde,$codigoAlt,$codigoPos,$cidade,$valini,$valfim,$tam=300,$local) {
       $idm = $_SESSION['idioma'];
	   if($valini) { $valini=str_replace(",",".",$valini); }
	   if($valfim) { $valfim=str_replace(",",".",$valfim); }
	   if($idm == 2) {
	      $desTp = "TIM_DES_ING";
	   }else if($idm == 3){
	   	  $desTp = "TIM_DES_ESP";
	   }else {
	      $desTp = "TIM_DES";
	   } 
	   $br = "";
	   if(is_array($bairro)){
			$cont = count($bairro);
			if($bairro[0] == "TODOS" || $bairro[0]==" "){
				$br="";
			}else {		
				for($i=0;$i<$cont;$i++) {		  
				   if($cont == 1) {
					  $br = "AND IMO.IMO_DES_BAI='".$bairro[$i]."'";
				   }else if($cont > 1) {
					 if($i==0){
						$br.= " AND ( IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }else if($i == ($cont - 1)) {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' ) ";
					 }else {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }
				   }
				   
				}
			}
	   }else {
	   	  $br = "";
	   }
	   $bairro = $br;
	   $imovel = "";
	   $ass = $this->getASS_ISN();
	   $param = $this->getParam();
	   $sql = "SELECT IMO.IMO_ISN,IMO_DES_BAI,IMO.IMO_COD_IMO_SIS,IMO.IMO_DES_REF,IMO_VAL_ARE_PRIV,IMO_VAL_MED_FRENTE,IMO_NUM_END,IMO_VAL_MED_FUNDOS,IMO_VAL_AGIO,IMO_ANO_CONST,IMO_TIP_OCUP,IMO_DES_END,IMO_VAL_VEN,LOC_NOM,IMO_VAL_ARE_TOT,TIM_DES,$desTp,IMO_TIP_EXIB_PRECO_SITE,IMO_VAL_SAL_DEV,IMO_VAL_PARC IMO_VAL_PREST,IMO_QTD_PARC IMO_QTD_PREST,IMO_TIP_EXC,IMO_TIP_IMO 
				   FROM tab_imo IMO
				   INNER JOIN tab_loc LOC ON IMO.LOC_ISN = LOC.LOC_ISN
			   	   INNER JOIN tab_tim TIM ON IMO.TIM_ISN = TIM.TIM_ISN
       WHERE IMO.IMO_TIP_EXIB_SITE = 1 AND IMO.IMO_TIP_BLOQ = 0 AND IMO.ASS_ISN = $ass AND TIM.TIM_ISN = $tipo $bairro";
	   if(($valini > 0) && ($valfim > 0)) {
	      $sql = $sql."AND IMO.IMO_VAL_VEN >= $valini AND IMO.IMO_VAL_VEN <= $valfim ";
	   }else if(($valini > 0) && ($valfim == 0)) {
	      $sql = $sql."AND IMO.IMO_VAL_VEN >= $valini";
	   }else if(($valini == 0) && ($valfim > 0)) {
	      $sql = $sql."AND IMO.IMO_VAL_VEN <= $valfim";
	   }else if(($valini == 0) && ($valfim == 0)) {
	      $sql = $sql;
	   }
	   if($codigoVal) {
		  $sql = $sql." AND IMO_ISN IN (".$codigoVal.")";
	   }
	   if($codigoQtde) {
		   $sql = $sql." AND IMO_ISN IN (".$codigoQtde.")";
		}
	   if($codigoAlt) {
		  $sql = $sql." AND IMO_ISN IN (".$codigoAlt.")";
	   }
	   if($codigoPos) {
		    $sql = $sql." AND IMO_ISN IN (".$codigoPos.")";
	   }
	   $sql = $sql."  AND LOC.LOC_NOM = '$cidade' GROUP BY IMO.IMO_ISN ORDER BY IMO.IMO_VAL_VEN";
	   $res = $this->imobredes->query($sql);	
	   if($res->num_rows() < 1){
		   return false;
	   }
	   $i = 0;
	   foreach($res->result() as $array){
			$imovel[$i]["imo_isn"]           = $array->IMO_ISN; 
			$imovel[$i]["imo_ref"]           = $array->IMO_DES_REF;
			$imovel[$i]["imo_cod_sis"]       = $array->IMO_COD_IMO_SIS;		   
			$imovel[$i]["imo_des_bai"]       = utf8_decode($array->IMO_DES_BAI);
			$imovel[$i]["imo_tip_imo"]       = $array->IMO_TIP_IMO;
			if($array->IMO_TIP_EXC==1){
				if($param["PAA_TIP_EXIB_END_IMO"]==1){	   
					if($param["PAA_TIP_EXIB_NUM_END_IMO"]==1){
						$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." n&deg;".$array->IMO_NUM_END." - ".$array->LOC_NOM);
					}else {
						$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." - ".$array->LOC_NOM);
					}
				}else {
					$imovel[$i]["imo_des_end"]       = "Consulte nossos corretores";
				}
			}else {
				$imovel[$i]["imo_des_end"]       = utf8_decode($array->IMO_DES_END." n&deg;".$array->IMO_NUM_END." - ".$array->LOC_NOM);
			} 	   
			if($array->IMO_TIP_EXIB_PRECO_SITE==1){				   
				$imovel[$i]["imo_val_ven"]  = $array->IMO_VAL_VEN; 
			}else {
				$imovel[$i]["imo_val_ven"]  = "Entre em contato para mais detalhes"; 
			}		 	   
			$imovel[$i]["imo_val_are"]       = $array->IMO_VAL_ARE_TOT;
			$imovel[$i]["imo_val_are_priv"]  = $array->IMO_VAL_ARE_PRIV;
			$imovel[$i]["imo_val_frente"]    = $array->IMO_VAL_MED_FRENTE;
			$imovel[$i]["imo_val_fundo"]     = $array->IMO_VAL_MED_FUNDOS;
			$imovel[$i]["imo_val_sal_dev"]   = $array->IMO_VAL_SAL_DEV;
			$imovel[$i]["imo_val_agio"]      = $array->IMO_VAL_AGIO;
			$imovel[$i]["imo_val_prest"]     = $array->IMO_VAL_PREST;
			$imovel[$i]["imo_qtd_prest"]     = $array->IMO_QTD_PREST;
			$imovel[$i]["imo_ano_const"]     = $array->IMO_ANO_CONST;
			$imovel[$i]["imo_tip_ocup"]      = $array->IMO_TIP_OCUP;
			if($imovel[$i]["imo_tip_ocup"] == 0) {
			   $imovel[$i]["imo_tipo_ocupacao"] = "NAO";
			}else if($imovel[$i]["imo_tip_ocup"] == 1) {
			   $imovel[$i]["imo_tipo_ocupacao"] = "SIM";
			}
			if($idm == 2) {
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES_ING);
			}else if($idm == 3){
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES_ESP);
			}else {
			   $imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES);
			}
			if(!$imovel[$i]["tim_des"]){
				$imovel[$i]["tim_des"] = utf8_decode($array->TIM_DES);	
			}     
         $i++; 
	   }
	   is_array($imovel)?$quant=count($imovel):$quant=0;
	   for($x=0;$x<$quant;$x++){
			$imovel[$x]["caracs"] = $this->consultarCaracteristicaPai($imovel[$x]["imo_isn"]);
			$imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
			$imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam*2,$local);
	   }	
	   return $imovel;
	}
	
	 function consultarCaracteristicaPai($codigo) {
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descCarDes = "DSC_DES_ING"; 
	   }else if($idm == 3){
	   	  $descCarDes = "DSC_DES_ESP"; 
	   }else {
	      $descCarDes = "DSC_DES"; 
	   }  
	   $i = 0;
	   $caracPai = "";
	   $caracPaiVal = "";
	   $ass = $this->getASS_ISN();
	   $sql = "SELECT IMC.*, DSC.DSC_DES,DSC.$descCarDes,CAR.CAR_TIP, CAR.CAR_DES_UNI,CAR.CAR_ISN 
	   		   FROM tab_imc IMC
               INNER JOIN tab_car CAR ON IMC.CAR_ISN = CAR.CAR_ISN
			   INNER JOIN tab_dsc DSC ON DSC.DSC_ISN = CAR.DSC_ISN
               WHERE (CAR.ASS_ISN = $ass OR CAR.ASS_ISN IS NULL) AND IMC.IMO_ISN = $codigo 
			   AND (CAR.CAR_ISN_PAI = 0 OR CAR.CAR_ISN_PAI IS NULL) ORDER BY DSC.DSC_DES";
	   $res = $this->imobredes->query($sql);
	   if($res->num_rows() < 1){
		    return false;
	   }
	   $carsFilha = "";
	   foreach($res->result() as $array){
	   		$caracPai[$i]["imc_isn"]           = $array->IMC_ISN; 	   
			$caracPai[$i]["imc_car_isn"]       = $array->CAR_ISN; 
			$caracPai[$i]["car_car_tip"]       = $array->CAR_TIP;
			$caracPai[$i]["car_car_des_uni"]   = $array->CAR_DES_UNI; 
			$caracPai[$i]["imc_qtd"]           = $array->IMC_QTD;
			$caracPai[$i]["imc_des"]           = utf8_decode($array->IMC_DES);
			$caracPai[$i]["imc_val"]           = $array->IMC_VAL;
			if($caracPai[$i]["car_car_tip"]==1 && $array->IMC_VAL_BOL==1){
				$caracPai[$i]["imc_tip_con"]       = $array->IMC_VAL_BOL;
			}else {
				$caracPai[$i]["imc_tip_con"]       = 0;
			}
			$caracPai[$i]["imc_dca_isn"]       = $array->DCA_ISN;
			if($idm == 2) {
			   $caracPai[$i]["car_car_des"] = utf8_decode($array->DSC_DES_ING);
			}else if($idm == 3){
			   $caracPai[$i]["car_car_des"] = utf8_decode($array->DSC_DES_ESP);
			}else {
			   $caracPai[$i]["car_car_des"] = utf8_decode($array->DSC_DES);
			}
			if(!$caracPai[$i]["car_car_des"]){
				$caracPai[$i]["car_car_des"] = utf8_decode($array->DSC_DES);
			} 
			$i++;
		}	
		for($i=0;$i<count($caracPai);$i++) {
			if($caracPai[$i]["car_car_tip"] == 1) {
			   if($caracPai[$i]["imc_tip_con"] == 0) {
			      $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			      $caracPaiVal[$i]["imc_tip_con"] = "NAO";
			   }else if($caracPai[$i]["imc_tip_con"] == 1) {
			      $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			      $caracPaiVal[$i]["imc_tip_con"] = "SIM";
			   }
			}else if($caracPai[$i]["car_car_tip"] == 2) {
			   $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			   $caracPaiVal[$i]["imc_qtd"] = $caracPai[$i]["imc_qtd"];
			}else if($caracPai[$i]["car_car_tip"] == 3) {
			      $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			      $caracPaiVal[$i]["dca_des"]      = $this->dcaValor($caracPai[$i]["imc_dca_isn"]);
			}else if($caracPai[$i]["car_car_tip"] == 4) {
			   $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			   $caracPaiVal[$i]["imc_des"] = $caracPai[$i]["imc_des"];
			}else if($caracPai[$i]["car_car_tip"] == 5) {
			   $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			   $caracPaiVal[$i]["imc_val"] = $caracPai[$i]["imc_val"];
			   $caracPaiVal[$i]["car_car_des_uni"] = $caracPai[$i]["car_car_des_uni"] ;
			}
            $caracPaiVal[$i]["car_car_tipPai"] = $caracPai[$i]["car_car_tip"]; 
			$caracPaiVal[$i]["imo_obs"] = $this->obsValor($codigo); 
		}
		for($i=0;$i < count($caracPai);$i++) {
		   $caracPai[$i]["imc_isn"] = $caracPai[$i]["imc_isn"];
			$caracPai[$i]["imc_car_isn"] = $caracPai[$i]["imc_car_isn"];
			$caracPai[$i]["car_car_tip"] = $caracPai[$i]["car_car_tip"];
			$caracPai[$i]["car_car_des"] = $caracPai[$i]["car_car_des"];
			$caracPai[$i]["car_car_des_uni"] = $caracPai[$i]["car_car_des_uni"];
			$caracPai[$i]["imc_qtd"] = $caracPai[$i]["imc_qtd"];
			$caracPai[$i]["imc_des"]  = $caracPai[$i]["imc_des"];
			$caracPai[$i]["imc_val"]  = $caracPai[$i]["imc_val"];
			$caracPai[$i]["imc_tip_con"] = $caracPai[$i]["imc_tip_con"]; 
			$caracPai[$i]["imc_dca_isn"] = $caracPai[$i]["imc_dca_isn"];
		    $caracPaiVal[$i]["caracteristicas"]  = $this->consultarCaracteristicaFilha($codigo,$caracPai[$i]["imc_car_isn"]);
		}
	   return $caracPaiVal;
	}
	
	function dcaValor($numero) {
	    $ass = $this->getASS_ISN();
	    $sql = "SELECT DCA.DCA_DES FROM tab_dca DCA WHERE DCA.DCA_ISN = $numero AND (ASS_ISN = $ass OR ASS_ISN IS NULL)";
	   $res = $this->imobredes->query($sql);
	   if($res->num_rows() < 1){
		    return false;
	   }
	   foreach($res->result() as $array){
		  $dca_des  = utf8_decode($array->DCA_DES);
	   }
	   return $dca_des;
	}
	
	function obsValor($numero) {
	   $idm = $_SESSION['idioma'];
	   $ass = $this->getASS_ISN();
	   $sql = "SELECT IMO_DES_TXT_DEST,IMO_DES_TXT_DEST_ING,IMO_DES_TXT_DEST_ESP FROM tab_imo WHERE IMO_ISN = $numero AND ASS_ISN = $ass";
	   $res = $this->imobredes->query($sql);
	   if($res->num_rows() < 1){
		    return false;
	   }
	   $obs_des="";
	   foreach($res->result() as $array){
		  	if($idm == 2) {
			   $obs_des  = utf8_decode($array->IMO_DES_TXT_DEST_ING);
			}else if($idm == 3){
			   $obs_des  = utf8_decode($array->IMO_DES_TXT_DEST_ESP);
			}else {
			   $obs_des  = utf8_decode($array->IMO_DES_TXT_DEST);
			}
			if(!$obs_des){
				$obs_des  = utf8_decode($array->IMO_DES_TXT_DEST);
			} 
	   }		
	   return $obs_des;
	}
	
	function consultarCaracteristicaFilha($codigo,$car_isn) {
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descCarDes = "DSC_DES_ING"; 
	   }else if($idm == 3){
	   	  $descCarDes = "DSC_DES_ESP"; 
	   }else {
	      $descCarDes = "DSC_DES"; 
	   }  
	   $caracFilha = "";
	   $caracFilhaVal = "";
	   $carsPai = "";
	   $i = 0;
	   $ass = $this->getASS_ISN();
	   $sql = "SELECT IMC.*, DSC.DSC_DES,DSC.$descCarDes,CAR.CAR_TIP, CAR.CAR_DES_UNI,CAR.CAR_ISN 
	   		   FROM tab_imc IMC
               INNER JOIN tab_car CAR ON IMC.CAR_ISN = CAR.CAR_ISN
			   INNER JOIN tab_dsc DSC ON DSC.DSC_ISN = CAR.DSC_ISN
               WHERE (CAR.ASS_ISN = $ass OR CAR.ASS_ISN IS NULL) AND IMC.IMO_ISN = $codigo 
			   AND CAR.CAR_ISN_PAI = $car_isn ORDER BY IMC.IMC_NUM_SEQ,IMC.IMC_ISN";
	   $res = $this->imobredes->query($sql);
	   if($res->num_rows() < 1){
		    return false;
	   }	   
	   foreach($res->result() as $array){			
			$caracFilha[$i]["imc_isn"]           = $array->IMC_ISN; 	   
			$caracFilha[$i]["imc_car_isn"]       = $array->CAR_ISN;
			$caracFilha[$i]["car_car_tip"]       = $array->CAR_TIP; 	   
			if($caracFilha[$i]["car_car_tip"]==1 && $array->IMC_VAL_BOL==1){
				$caracFilha[$i]["imc_tip_con"]   = $array->IMC_VAL_BOL;
			}else {
				$caracFilha[$i]["imc_tip_con"]   = 0;
			}    	   
			$caracFilha[$i]["imc_qtd"]           = $array->IMC_QTD; 	   
			$caracFilha[$i]["imc_val"]           = $array->IMC_VAL; 	   
			$caracFilha[$i]["imc_des"]           = utf8_decode($array->IMC_DES); 	   
			$caracFilha[$i]["imc_num_seq"]       = $array->IMC_NUM_SEQ; 	   
			$caracFilha[$i]["dca_isn"]           = $array->DCA_ISN; 
			if($idm == 2) {
			   $caracFilha[$i]["car_car_des"] = utf8_decode($array->DSC_DES_ING);
			}else if($idm == 3){
			   $caracFilha[$i]["car_car_des"] = utf8_decode($array->DSC_DES_ESP);
			}else {
			   $caracFilha[$i]["car_car_des"] = utf8_decode($array->DSC_DES);
			}
			if(!$caracFilha[$i]["car_car_des"]){
				$caracFilha[$i]["car_car_des"] = utf8_decode($array->DSC_DES);
			} 				
			$i++;
	   }	
		for($i=0;$i<count($caracFilha);$i++) {
			if($caracFilha[$i]["car_car_tip"] == 1) {
			   $caracFilha[$i]["imc_tip_con"];
			   if($caracFilha[$i]["imc_tip_con"] == 0) {
				  $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
				  $caracFilhaVal[$i]["imc_tip_con"] = "NAO";
			   }else if($caracFilha[$i]["imc_tip_con"] == 1) {
				  $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
				  $caracFilhaVal[$i]["imc_tip_con"] = "SIM";
			   }
			}else if($caracFilha[$i]["car_car_tip"] == 2) {
			   $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
			   $caracFilhaVal[$i]["imc_qtd"] = $caracFilha[$i]["imc_qtd"];
			}else if($caracFilha[$i]["car_car_tip"] == 3) {
				  $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
				  $caracFilhaVal[$i]["dca_des"]      = $this->dcaValor($caracFilha[$i]["dca_isn"]);
			}else if($caracFilha[$i]["car_car_tip"] == 4) {
			   $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
			   $caracFilhaVal[$i]["imc_des"] = $caracFilha[$i]["imc_des"];
			}else if($caracFilha[$i]["car_car_tip"] == 5) {
			   $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
			   $caracFilhaVal[$i]["imc_val"] = $caracFilha[$i]["imc_val"];
			}
			$caracFilhaVal[$i]["imc_num_seq"] = $caracFilha[$i]["imc_num_seq"]; 
			$caracFilhaVal[$i]["car_car_tipFilha"] = $caracFilha[$i]["car_car_tip"]; 
		}
	   return $caracFilhaVal;
	}
	
	function carregaCaracteristicasAv($tip) {
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descCar = "DSC.DSC_DES_ING";
	   }else if($idm == 3){
	   	  $descCar = "DSC.DSC_DES_ESP";
	   }else {
	      $descCar = "DSC.DSC_DES";
	   }
	   $i = 0;
	   $imovel = "";
	   $ass = $this->getASS_ISN();
	   $sql = "SELECT DISTINCT DSC.DSC_DES,$descCar,CAR.CAR_ISN,CAR.CAR_TIP FROM tab_car CAR
	   		   INNER JOIN tab_dsc DSC ON CAR.DSC_ISN = DSC.DSC_ISN 
	   		   WHERE (CAR.ASS_ISN IS NULL OR CAR.ASS_ISN = $ass) AND CAR.CAR_TIP <> 4 AND (CAR.CAR_ISN_PAI = 0 OR CAR.CAR_ISN_PAI IS NULL) AND CAR.TIM_ISN = $tip ORDER BY DSC.DSC_DES";
	   $res = $this->imobredes->query($sql);
	   if($res->num_rows() < 1){
		    return false;
	   }	   
	   foreach($res->result() as $array){
			$imovel[$i]["car_isn"]           = $array->CAR_ISN;	   
			$imovel[$i]["car_tip"]           = $array->CAR_TIP;
			if($idm == 2) {
			   $imovel[$i]["car_des"] = $array->DSC_DES_ING;
			}else if($idm == 3){
			   $imovel[$i]["car_des"] = $array->DSC_DES_ESP;
			}else {
			   $imovel[$i]["car_des"] = $array->DSC_DES;
			}
			if(!$imovel[$i]["car_des"]){
				$imovel[$i]["car_des"] = $array->DSC_DES;
			}  
            $i++; 
	   }
	   $valpre = "";
	   is_array($imovel)?$quant=count($imovel):$quant=0;
	   for($cont=0;$cont<$quant;$cont++) {
	      if($imovel[$cont]["car_tip"] == 3) {
		     $valpre = $this->obterValPre($imovel[$cont]["car_isn"]);  
			 $imovel[$cont]["val_pre"] = $valpre;
		  }	 
	   }
	   return $imovel;
	}
	function obterValPre($num) {
	   $i = 0;
	   $valores = "";
	   $ass = $this->getASS_ISN();
	   $sql = "SELECT DCA_ISN,DCA_DES 
	   FROM tab_dca WHERE (ASS_ISN = $ass OR ASS_ISN IS NULL) AND CAR_ISN = $num ORDER BY DCA_DES";
	   $res = $this->imobredes->query($sql);
	   if($res->num_rows() < 1){
		    return false;
	   }	   
	   foreach($res->result() as $array){
			$valores[$i]["dca_isn"]           = $array->DCA_ISN; 	   
			$valores[$i]["dca_des"]           = $array->DCA_DES; 	   
            $i++; 
	   }
	   return $valores;
	}
   function carregarCodigos() {
       $idm = $_SESSION['idioma'];	   
	   if($idm == 2) {
	      $textDestq = "IMO_DES_TXT_DEST_ING";
	   }else if($idm == 3){
	   	  $textDestq = "IMO_DES_TXT_DEST_ESP";
	   }else {
	      $textDestq = "IMO_DES_TXT_DEST";
	   }
	   $con = new ConexaoImobsale(); 
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT IMO_ISN,$textDestq,IMO_DES_TXT_DEST
	   FROM tab_imo WHERE IMO_TIP_DEST = 1 AND IMO_TIP_DEST_PRIM_PAG = 1   ORDER BY IMO_ISN";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]            = odbc_result($res,"IMO_ISN"); 	   
			$codigo[$i]["imo_des_est"]        = odbc_result($res,$textDestq); 
			if(empty($codigo[$i]["imo_des_est"])){
				$codigo[$i]["imo_des_est"]    = odbc_result($res,"IMO_DES_TXT_DEST"); 
			}	   
         $i++; 
	   }
	   @odbc_close($conn);		
	   return $codigo;
   }

   function carregarTodosCodigos() {
       $idm = $_SESSION['idioma'];
	    if($idm == 2) {
	      $textDestq = "IMO_DES_TXT_DEST_ING";
	   }else if($idm == 3){
	   	  $textDestq = "IMO_DES_TXT_DEST_ESP";
	   }else {
	      $textDestq = "IMO_DES_TXT_DEST";
	   }
	   $con = new ConexaoImobsale(); 
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT IMO_ISN,IMO_DES_TXT_DEST,$descDes 
	   FROM tab_imo WHERE IMO_TIP_DEST = 1 ORDER BY IMO_ISN";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]            = odbc_result($res,"IMO_ISN"); 	   
			$codigo[$i]["imo_des_est"]        = odbc_result($res,$descDes); 
			if(empty($codigo[$i]["imo_des_est"])){
				$codigo[$i]["imo_des_est"]    = odbc_result($res,"IMO_DES_TXT_DEST"); 
			}		   
         $i++; 
	   }
	   @odbc_close($conn);		
	   return $codigo;
   }


   function carregarDestaquesCapa($cod) {
	   $con = new ConexaoImobsale();  
	   //$i = 0;
	   $destaque = array();
	   $sql = "SELECT IMI_NOM  FROM tab_imi WHERE IMO_ISN = $cod AND IMI_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$destaque["imi_nom"] = odbc_result($res,"IMI_NOM"); 	   
	   }
	   @odbc_close($conn);		
	   return $destaque;
   }
   function carregarDestaques($cod) {
	   $con = new ConexaoImobsale();  
	   //$i = 0;
	   $destaque = array();
	   $sql = "SELECT IMI_NOM  FROM tab_imi WHERE IMO_ISN = $cod AND IMI_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$destaque["imi_nom"] = odbc_result($res,"IMI_NOM"); 	   
	   }
	   @odbc_close($conn);		
	   return $destaque;
   }
 function carregarCodigosAdm() {
	   $con = new ConexaoImobsale();  
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT IMO.IMO_ISN,IMO.IMO_DES_END,IMO.IMO_TIP_DEST,
	           IMO.IMO_TIP_DEST_PRIM_PAG,TIM.TIM_DES 
	           FROM tab_imo IMO,tab_tim TIM WHERE IMO.TIM_ISN = TIM.TIM_ISN 
			   ORDER BY IMO_ISN";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]              = odbc_result($res,"IMO_ISN"); 	   
			$codigo[$i]["imo_des_end"]          = odbc_result($res,"IMO_DES_END"); 	   
			$codigo[$i]["imo_tip_dest"]         = odbc_result($res,"IMO_TIP_DEST"); 	   
			$codigo[$i]["imo_tip_dest_prim"]    = odbc_result($res,"IMO_TIP_DEST_PRIM_PAG"); 				            $codigo[$i]["tipo"]                 = odbc_result($res,"TIM_DES");
			$i++; 
	   }
	   for($x=0;$x < count($codigo);$x++) {
			$imovel[$x]["imo_isn"]              = $codigo[$x]["imo_isn"]; 	   
			$imovel[$x]["imo_des_end"]          = $codigo[$x]["imo_des_end"]; 	   
			$imovel[$x]["imo_tip_dest"]         = $codigo[$x]["imo_tip_dest"]; 	   
			$imovel[$x]["imo_tip_dest_prim"]    = $codigo[$x]["imo_tip_dest_prim"]; 				            $imovel[$x]["tipo"]                 = $codigo[$x]["tipo"];
            $imovel[$x]["foto"]                 = $this->carregarFotoCod($codigo[$x]["imo_isn"]);
	   }
	   @odbc_close($conn);		
	   return $imovel;
 }
 function carregarCodigosAdmEsp($cod) {
	   $con = new ConexaoImobsale();  
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT IMO.IMO_ISN,IMO.IMO_DES_END,IMO.IMO_TIP_DEST,
	           IMO.IMO_TIP_DEST_PRIM_PAG,TIM.TIM_DES 
	           FROM tab_imo IMO,tab_tim TIM WHERE IMO.TIM_ISN = TIM.TIM_ISN 
			   AND IMO.IMO_ISN = $cod";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]              = odbc_result($res,"IMO_ISN"); 	   
			$codigo[$i]["imo_des_end"]          = odbc_result($res,"IMO_DES_END"); 	   
			$codigo[$i]["imo_tip_dest"]         = odbc_result($res,"IMO_TIP_DEST"); 	   
			$codigo[$i]["imo_tip_dest_prim"]    = odbc_result($res,"IMO_TIP_DEST_PRIM_PAG"); 	            $codigo[$i]["tipo"]                 = odbc_result($res,"TIM_DES");
            $i++; 
	   }
	   for($x=0;$x < count($codigo);$x++) {
			$imovel[$x]["imo_isn"]              = $codigo[$x]["imo_isn"]; 	   
			$imovel[$x]["imo_des_end"]          = $codigo[$x]["imo_des_end"]; 	   
			$imovel[$x]["imo_tip_dest"]         = $codigo[$x]["imo_tip_dest"]; 	   
			$imovel[$x]["imo_tip_dest_prim"]    = $codigo[$x]["imo_tip_dest_prim"]; 				            $imovel[$x]["tipo"]                 = $codigo[$x]["tipo"];
            $imovel[$x]["foto"]                 = $this->carregarFotoCod($codigo[$x]["imo_isn"]);
	   }
	   @odbc_close($conn);		
	   return $imovel;
 }
   function carregarFotoCod($cod) {
	   $con = new ConexaoImobsale();  
	   $codigo = array();
	   $sql = "SELECT IMI_NOM 
	   FROM tab_imi WHERE IMO_ISN = $cod AND
	   IMI_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$img  =  odbc_result($res,"IMI_NOM"); 	   
	   }
	   @odbc_close($conn);		
	   return $img;
   }
 function buscarImagensAdm($cod) {
	   $con = new ConexaoImobsale();  
	   $codigo = array();
	   $i = 0;
	   $sql = "SELECT IMI_NOM 
	   FROM tab_imi WHERE IMO_ISN = $cod"; 
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$img[$i]["nome"]  =  odbc_result($res,"IMI_NOM"); 	   
			$i++;
	   }
	   @odbc_close($conn);
	   for($x=0;$x < count($img);$x++) {
	       $imagem[$x]["nome"]     =  $img[$x]["nome"];
		   if($imagem[$x]["nome"] == $this->carregarFotoCod($cod)) {
		      $imagem[$x]["destaque"] =  $imagem[$x]["nome"];
		   }
	   }
	   return $imagem;
  }
  function alterarImagemAdm($cod,$nom) {
	   $con = new ConexaoImobsale();  
	   $sql = "UPDATE tab_imi SET IMI_TIP_DEST = 0 
	           WHERE IMO_ISN = $cod AND IMI_TIP_DEST = 1"; 
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   $sql2 = "UPDATE tab_imi SET IMI_TIP_DEST = 1
	            WHERE IMO_ISN = $cod AND IMI_NOM = '$nom'";
	   $res = @odbc_exec($conn,$sql2);
	   @odbc_close($conn);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
  function setarDestaque($cod) {
	   $con = new ConexaoImobsale();  
	   $sql = "UPDATE tab_imo SET IMO_TIP_DEST = 1
	          WHERE IMO_ISN = $cod";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
  function setarDestaquePrim($cod) {
	   $con = new ConexaoImobsale();  
	   $sql = "UPDATE tab_imo SET IMO_TIP_DEST = 1,
	           IMO_TIP_DEST_PRIM_PAG = 1 
	           WHERE IMO_ISN = $cod";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
  function retirarDestaque($cod) {
	   $con = new ConexaoImobsale();  
	   $sql = "UPDATE tab_imo SET IMO_TIP_DEST = 0
	           WHERE IMO_ISN = $cod AND IMO_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
  function retirarDestaquePrim($cod) {
	   $con = new ConexaoImobsale();  
	   $sql = "UPDATE tab_imo SET IMO_TIP_DEST_PRIM_PAG = 0
	           WHERE IMO_ISN = $cod AND IMO_TIP_DEST_PRIM_PAG = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
  
  function getParam(){
	$ass = $this->getASS_ISN();
	$sql = "SELECT * FROM tab_paa WHERE ASS_ISN = $ass";
    $res = $this->imobredes->query($sql);
	if($res->num_rows() < 1){
		return false;
	}
	foreach($res->result() as $array){
   		$params["ass"] = $array->ASS_ISN;
		$params["isn"] = $array->PAA_ISN;
		$params["PAA_TIP_EXIB_IMO_EXC"] = $array->PAA_TIP_EXIB_IMO_EXC;
		$params["PAA_TIP_EXIB_END_IMO"] = $array->PAA_TIP_EXIB_END_IMO;
		$params["PAA_TIP_EXIB_MARCA"] = $array->PAA_TIP_EXIB_MARCA;
		$params["PAA_TIP_EXIB_NUM_END_IMO"] = $array->PAA_TIP_EXIB_NUM_END_IMO;
		$params["PAA_TIP_EXIB_SITE"] = $array->PAA_TIP_EXIB_SITE;
		$params["PAA_TIP_EXIB_SITE_PAR"] = $array->PAA_TIP_EXIB_SITE_PAR;
		$params["PAA_TIP_EXIB_PORTAL"] = $array->PAA_TIP_EXIB_PORTAL;
	}   
    return $params;	
  }
  
  public function getAss(){
	$ass = $this->getASS_ISN();
	$sql = "SELECT * FROM tab_ass WHERE ASS_ISN = $ass";
	$res = $this->imobredes->query($sql);
	if($res->num_rows() < 1){
		return false;
	}
	$params="";
	foreach($res->result() as $array){	   
		 $params["ASS_ISN"] = $array->ASS_ISN;
		 $params["ASS_ISN"] = $array->ASS_ISN;
		 $params["ASS_TIP_PES"] = $array->ASS_TIP_PES;
		 $params["ASS_DES_CPF_CNPJ"] = $array->ASS_DES_CPF_CNPJ;
		 $params["ASS_NOM"] = $array->ASS_NOM;
		 $params["ASS_DES_MAIL"] = $array->ASS_DES_MAIL;
		 $params["ASS_DES_MAIL_GER"] = $array->ASS_DES_MAIL_GER;
		 $params["ASS_DES_MAIL_DIR"] = $array->ASS_DES_MAIL_DIR;
		 $params["ASS_DES_MAIL_DEP"] = $array->ASS_DES_MAIL_DEP;
		 $params["ASS_UTIL_MAIL"] = $array->ASS_UTIL_MAIL;
		 $params["ASS_DES_END"] = $array->ASS_DES_END;
		 $params["ASS_NUM_END"] = $array->ASS_NUM_END;
		 $params["ASS_DES_END_COM"] = $array->ASS_DES_END_COM;
		 $params["ASS_NUM_FON_1"] = $array->ASS_NUM_FON_1;
		 $params["ASS_NUM_FON_2"] = $array->ASS_NUM_FON_2;
		 $params["ASS_NUM_FON_3"] = $array->ASS_NUM_FON_3;
		 $params["ASS_NUM_FON_4"] = $array->ASS_NUM_FON_4;
		 $params["ASS_NUM_FON_5"] = $array->ASS_NUM_FON_5;
		 $params["ASS_TXT_DES"] = $array->ASS_TXT_DES;
		 $params["ASS_IMG"] = $array->ASS_IMG;
		 $params["ASS_TIP_LIB"] = $array->ASS_TIP_LIB;
		 $params["LOC_ISN"] = $array->LOC_ISN;
		 $params["BAI_ISN"] = $array->BAI_ISN;
		 $params["BAI_DES"] = $array->BAI_DES;
		 $params["ASS_DAT_CAD"] = $array->ASS_DAT_CAD;
		 $params["ASS_DAT_ATU"] = $array->ASS_DAT_ATU;
		 $params["ASS_DES_CEP"] = $array->ASS_DES_CEP;
		 $params["ASS_TIP_REF"] = $array->ASS_TIP_REF;
	}
	return $params;
  }
	 
}
?>