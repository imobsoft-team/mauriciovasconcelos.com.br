<?php
class Enquete extends Model{
	function Enquete(){
		parent::Model();
	}
	
	function getAsk($st){
		$sql = "SELECT ENQ_PER_ISN,ENQ_PER_DES,ENQ_PER_EXIB_PARC, ENQ_PER_TIPO
	       FROM tab_enq_per WHERE ENQ_PER_DAT_FIM >= DATE(NOW())";
		$res = $this->db->query($sql);
		$per = "";
		if($res->num_rows() > 0){
			foreach($res->result() as $dados){
				$per=array('id'=>$dados->ENQ_PER_ISN,'des'=>$dados->ENQ_PER_DES,'exib'=>$dados->ENQ_PER_EXIB_PARC,'tipo'=>$dados->ENQ_PER_TIPO);
			}
			
		}
		if($per){
			switch($per["tipo"]){
				case 1:
					$st?$per=$per:$per="";
					break;
				case 2:
					$st?$per="":$per=$per;
					break;
			}
		}
		return $per;
	}
	
	function getResp($cod){
		$sql = "SELECT * FROM tab_enq_resp WHERE ENQ_PER_ISN = $cod";
		$res = $this->db->query($sql);
		$resp= "";
		if($res->num_rows() > 0){
			foreach($res->result() as $dados){
				$resp[]=array('id'=>$dados->ENQ_RESP_ISN,'des'=>htmlentities($dados->ENQ_RESP_DES,ENT_QUOTES));
			}			
		}
		return $resp;
	}
	
	function setVotar($per) {
		error_reporting(134);
		if ($_COOKIE["Ask"] == $per) {
			 setcookie("Ask",$per, time()+3600000); // selecionar o nome do cokie
			 $st = 0; 
		}else {
			 setcookie("Ask",$per, time()+3600000);  //Criar o valor 1
			 $st = 1;
		}
		return $st;
	}
	
	function setResult($idP,$idR) {   
		$sql = "INSERT INTO tab_enq_res (ENQ_RES_PER,ENQ_RES_RESP)
			   VALUES ($idP,$idR)";
		$res = $this->db->query($sql);
		$msg = 1;
		return $msg;
	} 
	
	function getExib($cod) {	 
	   $sql = "SELECT ENQ_PER_EXIB_PARC FROM tab_enq_per WHERE ENQ_PER_ISN = $cod";
	   $res = $this->db->query($sql);
	   $exib = "";
	   if($res->num_rows() > 0){
			foreach($res->result() as $dados){
			   $exib   =   $dados->ENQ_PER_EXIB_PARC;
			}
	   }		
	   return $exib;
	}
	
	function getPerg($cod) {	 
	   $sql = "SELECT ENQ_PER_DES FROM tab_enq_per WHERE ENQ_PER_ISN = $cod";
	   $res = $this->db->query($sql);
	   $des="";
	   if($res->num_rows() > 0){
			foreach($res->result() as $dados){
			   $des    =   $dados->ENQ_PER_DES;
			}
	   }		
	   return $des;
	}
	
	function getResultResp($pergunta) {   
		$sql = "SELECT COUNT(ENQ_RES_ISN) AS QUANTIDADE FROM tab_enq_res WHERE ENQ_RES_PER = $pergunta"; 
		$qtd=0;
		$res = $this->db->query($sql);
		if($res->num_rows() > 0){
			foreach($res->result() as $dados){
			   $qtd   =   $dados->QUANTIDADE;
		    }
		}
		return $qtd;
	}
	
	function getAns($codr,$codp) {   
		$sql = "SELECT COUNT(ENQ_RES_ISN) AS QUANTIDADE FROM tab_enq_res WHERE ENQ_RES_RESP = $codr 
			   AND ENQ_RES_PER = $codp";
		$res = $this->db->query($sql);
		$qtd=0;
		if($res->num_rows() > 0){
			foreach($res->result() as $dados){
			   $qtd   =   $dados->QUANTIDADE;
		    }
		 }
		return $qtd;
	}

}
?>