<?php
require_once('conexaoimobsale.php');
  class Lancamento extends Conexaoimobsale{
	 function Lancamento(){
	 	parent::ConexaoImobsale();
	 }	 
	 function getDestaques($tam=130){
		$idm = $_SESSION['idioma'];	   
		if($idm == 2) {
			$textDestq = "EMP_DES_TXT_DEST_ING";
		}else if($idm == 3){
			$textDestq = "EMP_DES_TXT_DEST_ESP";
		}else {
			$textDestq = "EMP_DES_TXT_DEST";
		}
		$sql = "SELECT E.EMP_ISN,E.$textDestq, E.EMP_DES,E.EMP_DES_END,E.EMP_DES_BAI,E.EMP_MES_ANO_ENT,E.EMP_DES_TXT_DEST FROM TAB_EMP E WHERE EMP_TIP_DEST = 1 AND EMP_TIP_DEST_PRIM = 1 ORDER BY EMP_ISN";
	    $i = 0;
	    $emp = "";
	    $conn=$this->connDb();	
	    $res = @odbc_exec($conn,$sql);
	    if (@odbc_error($res)) { 
		   return false;
	    }
	    while(@odbc_fetch_row($res)) {
			$emp[$i]["emp_isn"]              = odbc_result($res,"EMP_ISN"); 	   
			$emp[$i]["emp_des"]              = odbc_result($res,"EMP_DES"); 	   
			$emp[$i]["emp_des_end"]          = odbc_result($res,"EMP_DES_END"); 	   
			$emp[$i]["emp_des_bai"]          = odbc_result($res,"EMP_DES_BAI"); 	   
			$emp[$i]["emp_mes_ano_ent"]      = odbc_result($res,"EMP_MES_ANO_ENT");
			$emp[$i]["imo_isn"]              = 0; 
			$emp[$i]["emp_des_txt"]          = odbc_result($res,$textDestq);
			if(empty($emp[$i]["emp_des_txt"])){
				$emp[$i]["emp_des_txt"]      = odbc_result($res,"EMP_DES_TXT_DEST");
			}
			
		    $i++; 
	   }
	   is_array($emp)?$quant=count($emp):$quant=0;
	   for($x=0;$x<$quant;$x++){		 	
			$emp[$x]["foto"] = $this->buscarFoto($emp[$x]["emp_isn"],$this->CaminhoFoto(),$tam);
			$emp[$x]["tipo"] = $this->getTipoEmp($emp[$x]["emp_isn"]);
			$emp[$x]["val"]  = $this->getMenorPreco($emp[$x]["emp_isn"]);
	   }
	   @odbc_close($conn);		
	   return $emp;		
	
	 }
	 
	 
	  function getDestaquesCon($tam=130){
		$idm = $_SESSION['idioma'];	   
		if($idm == 2) {
			$textDestq = "EMP_DES_TXT_DEST_ING";
		}else if($idm == 3){
			$textDestq = "EMP_DES_TXT_DEST_ESP";
		}else {
			$textDestq = "EMP_DES_TXT_DEST";
		}
		$sql = "SELECT E.EMP_ISN,E.$textDestq, E.EMP_DES,E.EMP_DES_END,E.EMP_DES_BAI,E.EMP_MES_ANO_ENT,E.EMP_DES_TXT_DEST FROM TAB_EMP E WHERE EMP_TIP_DEST = 1 AND EMP_TIP_DEST_PRIM = 1 AND EMP_COD_FASE = 3 ORDER BY EMP_ISN";
	    $i = 0;
	    $emp = "";
	    $conn=$this->connDb();	
	    $res = @odbc_exec($conn,$sql);
	    if (@odbc_error($res)) { 
		   return false;
	    }
	    while(@odbc_fetch_row($res)) {
			$emp[$i]["emp_isn"]              = odbc_result($res,"EMP_ISN"); 	   
			$emp[$i]["emp_des"]              = odbc_result($res,"EMP_DES"); 	   
			$emp[$i]["emp_des_end"]          = odbc_result($res,"EMP_DES_END"); 	   
			$emp[$i]["emp_des_bai"]          = odbc_result($res,"EMP_DES_BAI"); 	   
			$emp[$i]["emp_mes_ano_ent"]      = odbc_result($res,"EMP_MES_ANO_ENT");
			$emp[$i]["imo_isn"]              = 0; 
			$emp[$i]["emp_des_txt"]          = odbc_result($res,$textDestq);
			if(empty($emp[$i]["emp_des_txt"])){
				$emp[$i]["emp_des_txt"]      = odbc_result($res,"EMP_DES_TXT_DEST");
			}
			
		    $i++; 
	   }
	   is_array($emp)?$quant=count($emp):$quant=0;
	   for($x=0;$x<$quant;$x++){		 	
			$emp[$x]["foto"] = $this->buscarFoto($emp[$x]["emp_isn"],$this->CaminhoFoto(),$tam);
			$emp[$x]["tipo"] = $this->getTipoEmp($emp[$x]["emp_isn"]);
			$emp[$x]["val"]  = $this->getMenorPreco($emp[$x]["emp_isn"]);
	   }
	   @odbc_close($conn);		
	   return $emp;		
	
	 }
	 
	 
	 
	 
	 function getMenorPreco($emp){
		$sql = "SELECT CLA_VAL FROM TAB_CLA WHERE EMP_ISN = $emp ORDER BY CLA_VAL ASC";
		$i = 0;
	    $emp = "";
	    $conn=$this->connDb();	
	    $res = @odbc_exec($conn,$sql);
		if(@odbc_fetch_row($res)) {
			$emp             = odbc_result($res,"CLA_VAL"); 	
		
	    }
	    @odbc_close($conn);		
	    return $emp;
	}	
	 
	 public function buscarFoto($imo_isn,$caminho,$tamanho=130){
		$sql = "SELECT * FROM TAB_IME WHERE EMP_ISN = $imo_isn AND IME_TIP_DEST = 1";
		$conn2=$this->connDb();	
		$res2 = @odbc_exec($conn2,$sql);
		if (@odbc_error($res2)) { 
			return false;
		}		
		@odbc_fetch_row($res2);
		if(@file_exists($caminho.odbc_result($res2,"IME_NOM")) && odbc_result($res2,"IME_NOM")){
			$foto = 'application/helpers/inc_thumb.php?img=../../'.$caminho.odbc_result($res2,"IME_NOM").'&size='.$tamanho; 
		}		    
		if(empty($foto)){
			$foto = 'application/helpers/inc_thumb.php?img=../images/indisponivel.jpg&size=180';
		} 
		@odbc_close($conn2);		
		return $foto;
	 }
	 function todosEmpr() {
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descDes = "EMP_DES_TXT_DEST_ING";
	   }else if($idm == 3) {
	      $descDes = "EMP_DES_TXT_DEST_ESP";
	   }else {
	      $descDes = "EMP_DES_TXT_DEST";
	   }
	   $con = new ConexaoImobsale(); 
	   $i = 0;
	   $sql = "SELECT EMP_ISN,EMP_DES_TXT_DEST,$descDes FROM TAB_EMP WHERE EMP_TIP_DEST = 1 ORDER BY EMP_ISN";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) {
		   return false;
	   } 
	   while(@odbc_fetch_row($res)) {
         $empr[$i]["emp_id"]  = odbc_result($res,"EMP_ISN"); 	   
			$empr[$i]["emp_txt"] = odbc_result($res,$descDes); 
		if(empty($empr[$i]["emp_txt"])){
			$empr[$i]["emp_txt"] = odbc_result($res,"EMP_DES_TXT_DEST");
		}	   
         $i++; 
	   }
		for($x=0;$x < count($empr);$x++) {
		   $empr[$x]["foto"] = $this->carregarFotosDest($empr[$x][emp_id]);  
		}
	   @odbc_close($conn);		
	   return $empr;
	 }

	 function consultarTipo() {
	   $idm = $_SESSION['idioma'];	   
	   if($idm == 2) {
	      $descTipo = "TIM_DES_ING";
	   }else if($idm == 3) {
	      $descTipo = "TIM_DES_ESP";
	   }else {
	      $descTipo = "TIM_DES";
	   }
	   $con = new ConexaoImobsale(); 
	   $tipoesp = array();
	   $sql = "SELECT * FROM TAB_TIM ORDER BY TIM_DES";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) {
		   return false;
	   } 
		$i = 0;
	   while(@odbc_fetch_row($res)) {
		 $tipo[$i]["tim_des"]       = odbc_result($res,$descTipo); 
         $tipo[$i]["tim_id"]        = odbc_result($res,"TIM_ISN");
		 if(empty($tipo[$i]["tim_des"])){
		 	$tipo[$i]["tim_des"] = odbc_result($res,"TIM_DES");
		 } 	   
			$i++;
	   }
	   @odbc_close($conn);	
	   return $tipo;
	 }

	 function buscarClasseEsp($valini,$valfim,$area,$tipo,$cod) {
	   $idm = $_SESSION['idioma'];	   
	   if($idm == 2) {
	      $descTipo = "TIM_DES_ING";
	   }else if($idm == 3) {
	      $descTipo = "TIM_DES_ESP";
	   }else {
	      $descTipo = "TIM_DES";
	   }
	   $tipoesp = array();
	   if($tipo){
	   		$sql = "SELECT CLA.CLA_ISN,CLA.CLA_VAL_ARE_PRIV,CLA.CLA_VAL_ARE_TOT,CLA.CLA_DES,
		       CLA.CLA_VAL,CLA.CLA_VAL_SIN,CLA.CLA_VAL_PCT_SIN,CLA.CLA_QTD_BAL,CLA.CLA_VAL_BAL,
               CLA.CLA_QTD_PAR, TIM_DES,$descTipo,CLA.CLA_VAL_PAR
               FROM TAB_CLA CLA,TAB_TIM TIM
               WHERE TIM.TIM_ISN = CLA.TIM_ISN AND TIM.TIM_ISN = $tipo AND CLA.EMP_ISN = $cod ";
	   }else {
	   	    $sql = "SELECT CLA.CLA_ISN,CLA.CLA_VAL_ARE_PRIV,CLA.CLA_VAL_ARE_TOT,CLA.CLA_DES,
		       CLA.CLA_VAL,CLA.CLA_VAL_SIN,CLA.CLA_VAL_PCT_SIN,CLA.CLA_QTD_BAL,CLA.CLA_VAL_BAL,
               CLA.CLA_QTD_PAR, TIM_DES,$descTipo,CLA.CLA_VAL_PAR
               FROM TAB_CLA CLA,TAB_TIM TIM
               WHERE TIM.TIM_ISN = CLA.TIM_ISN AND CLA.EMP_ISN = $cod ";
	   }
	   $valini?$valini=str_replace(",",".",$valini):$valini='';
	   $valfim?$valfim=str_replace(",",".",$valfim):$valfim='';
	   if(($valini > 0) && ($valfim > 0)) {
	      $sql = $sql."AND CLA.CLA_VAL >= $valini AND CLA.CLA_VAL <= $valfim ";
	   }else if(($valini > 0) && ($valfim == 0)) {
	      $sql = $sql."AND CLA.CLA_VAL >= $valini";
	   }else if(($valini == 0) && ($valfim > 0)) {
	      $sql = $sql."AND CLA.CLA_VAL <= $valfim";
	   }else if(($valini == 0) && ($valfim == 0)) {
	      $sql = $sql;
	   }
	   if($area) {
	      $sql = $sql."AND CLA.CLA_VAL_ARE_TOT = $area";
	   }
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) {
		   return false;
	   } 
		$i = 0;
		$classe = "";
	   if(@odbc_fetch_row($res)) {
			$classe["cla_isn"]           = odbc_result($res,"CLA_ISN"); 	   
			$classe["cla_des"]           = odbc_result($res,"CLA_DES"); 	   
			$classe["cla_val"]           = odbc_result($res,"CLA_VAL"); 	   
			$classe["cla_val_sin"]       = odbc_result($res,"CLA_VAL_SIN"); 	   
			$classe["cla_val_pct_sin"]   = odbc_result($res,"CLA_VAL_PCT_SIN"); 	   
			$classe["cla_val_are_tot"]   = odbc_result($res,"CLA_VAL_ARE_TOT");
			$classe["cla_val_are_priv"]  = odbc_result($res,"CLA_VAL_ARE_PRIV");
			$classe["cla_qtd_bal"]       = odbc_result($res,"CLA_QTD_BAL");
			$classe["cla_val_bal"]       = odbc_result($res,"CLA_VAL_BAL");
			$classe["cla_qtd_par"]       = odbc_result($res,"CLA_QTD_PAR");
			$classe["tim_des"]           = odbc_result($res,$descTipo);
			$classe["cla_val_par"]       = odbc_result($res,"CLA_VAL_PAR");
			if(empty($classe["tim_des"])){
				$classe["tim_des"] = odbc_result($res,"TIM_DES");
			}
			$i++;
	   }
	   @odbc_close($conn);	
	   return $classe;
	 }

	 function buscarUnidade($cod) {
	   $div = array();
	   $sql = "SELECT DIV_VAL_INI,DIV_QTD_UNI,UNI_NUM FROM TAB_DIV D,TAB_UNI U
		        WHERE D.DIV_ISN = U.DIV_ISN AND D.CLA_ISN = $cod ORDER BY DIV_VAL_INI DESC";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) {
		   return false;
	   } 
		$i = 0;
	   while(@odbc_fetch_row($res)) {
         $div[$i]["div_val_ini"]       = odbc_result($res,"DIV_VAL_INI"); 	   
         $div[$i]["div_qtd_uni"]       = odbc_result($res,"DIV_QTD_UNI"); 	   
         $div[$i]["div_num"]           = odbc_result($res,"UNI_NUM"); 	   
			$i++;
	   }
	   @odbc_close($conn);	
	   return $div;
	 }
	 
	 function buscarUnidadeGeral($classes) {
	   $div = array();
	   $codigos = array();
	   foreach($classes as $cl){
	   		array_push($codigos,$cl["id"]);
	   }
	   $codigos = implode(",",$codigos);
	   $sql = "SELECT DIV_VAL_INI,DIV_QTD_UNI,UNI_NUM FROM TAB_DIV D,TAB_UNI U
		        WHERE D.DIV_ISN = U.DIV_ISN AND D.CLA_ISN IN ($codigos) ORDER BY DIV_VAL_INI DESC";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) {
		   return false;
	   } 
	   $i = 0;
	   while(@odbc_fetch_row($res)) {
         $div[$i]["div_val_ini"]       = odbc_result($res,"DIV_VAL_INI"); 	   
         $div[$i]["div_qtd_uni"]       = odbc_result($res,"DIV_QTD_UNI"); 	   
         $div[$i]["div_num"]           = odbc_result($res,"UNI_NUM"); 	   
		 $i++;
	   }
	   @odbc_close($conn);	
	   return $div;
	 }

	 function buscarFirstUnidade($cod) {
	   $con = new ConexaoImobsale(); 
	   $div = array();
	   $sql = "SELECT U.UNI_NUM FROM TAB_UNI U,TAB_CLA C,TAB_DIV D WHERE U.DIV_ISN = D.DIV_ISN AND D.CLA_ISN = C.CLA_ISN AND C.CLA_ISN = $cod";
		$conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) {
		   return false;
	   } 
	   if(odbc_fetch_row($res)) {
         $div["div_num"] = odbc_result($res,"UNI_NUM"); 	   
	   }
	   @odbc_close($conn);	
	   return $div;
	 }

	 function carregarFotosDest($cod) {
	   $img = array();
	   $con = new ConexaoImobsale(); 
	   $i = 0;
	   $sql = "SELECT IME_NOM FROM TAB_IME  WHERE EMP_ISN = $cod AND IME_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) {
		   return false;
	   } 
	   while(@odbc_fetch_row($res)) {
			$img = odbc_result($res,"IME_NOM"); 
	   }
	   @odbc_close($conn);		
	   return $img;
	 }
	 function consultarBairros($cid) {
	   $con = new ConexaoImobsale(); 
	   $i = 0;
	   $bairro = array();
	   $sql = "SELECT DISTINCT(EMP_DES_BAI) FROM TAB_EMP WHERE EMP_DES_LOC = '$cid' ORDER BY EMP_DES_BAI";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$bairro[$i]["imo_des_bai"]           = odbc_result($res,"EMP_DES_BAI"); 	   
         $i++; 
	   }
	   @odbc_close($conn);	
	   return $bairro;
	 }
	 
	 function consultarCidades() {
	   $con = new ConexaoImobsale(); 
	   $i = 0;
	   $cidade = array();
	   $sql = "SELECT DISTINCT EMP_DES_LOC FROM TAB_EMP ORDER BY EMP_DES_LOC";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$cidade[$i]["emp_des_loc"] = odbc_result($res,"EMP_DES_LOC"); 	   
         $i++; 
	   }
	   @odbc_close($conn);		
	   return $cidade;
	 }
	 function consultarId($loc) {
	   $con = new ConexaoImobsale(); 
	   $i = 0;
	   $cidade = array();
	   $sql = "SELECT EMP_ISN FROM TAB_EMP WHERE EMP_DES_LOC = '$loc'";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$id[$i]["emp_isn"] = odbc_result($res,"EMP_ISN"); 	   
         $i++; 
	   }
	   @odbc_close($conn);		
	   return $id;
	 }
	 
	 function consultarImovel($codigo,$tam=300,$local) {
	   $i = 0;
	   $imovel = array();
	   $sql = "SELECT E.EMP_ISN,E.EMP_DES,E.EMP_MES_ANO_ENT,E.EMP_DES_BAI,E.EMP_DES_TXT_DEST
		       FROM TAB_EMP E WHERE E.EMP_ISN = $codigo";
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   $imovel = "";
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["imo_isn"]           = odbc_result($res,"EMP_ISN"); 
			$imovel[$i]["emp_isn"]           = odbc_result($res,"EMP_ISN"); 	   
			$imovel[$i]["imo_des_bai"]       = odbc_result($res,"EMP_DES_BAI"); 
			$imovel[$i]["emp_des_bai"]       = odbc_result($res,"EMP_DES_BAI"); 	   
			$imovel[$i]["imo_nom"]           = odbc_result($res,"EMP_DES");
			$imovel[$i]["emp_des"]           = odbc_result($res,"EMP_DES"); 	   
			$imovel[$i]["imo_ano_entrega"]   = odbc_result($res,"EMP_MES_ANO_ENT");
			$imovel[$i]["emp_mes_ano_ent"]   = odbc_result($res,"EMP_MES_ANO_ENT");
			$imovel[$i]["emp_des_txt"]       = odbc_result($res,"EMP_DES_TXT_DEST");
			$imovel[$i]["imo_tip_usar"]      = 0;
			//$imovel[$i]["imo_dest_det"]      = odbc_result($res,"EMP_DES_TXT_DET");
			//$imovel[$i]["imo_dest_det_2"]    = odbc_result($res,"EMP_DES_TXT_DET_2");
         	$i++; 
	   }
	   is_array($imovel)?$quant=count($imovel):$quant=0;
	   for($x=0;$x<$quant;$x++){
			$imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
			$imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam*2,$local);
			$imovel[$x]["classes"]  = $this->buscarClasseEmp($imovel[$x]["imo_isn"]);
			if($imovel[$x]["classes"]){
				$imovel[$x]["tipo"]  = $this->consultarDesTipo($imovel[$x]["classes"][0]["tim_isn"]);
				$imovel[$x]["imo_val_ven"]  = $this->menorpreco($imovel[$x]["classes"][0]["id"]);
			}	
			$imovel[$x]["tim_des"] = $imovel[$x]["tipo"];		
	   }
	   @odbc_close($conn);	
	   return $imovel;
	}
	
	public function buscarFoto2($imo_isn,$caminho,$tamanho=300,$local=''){
		$sql = "SELECT * FROM TAB_IME WHERE EMP_ISN = $imo_isn AND IME_TIP_DEST = 1";
		$conn=$this->connDb();	
		$res = @odbc_exec($conn,$sql);
		if (@odbc_error($res)) { 
			return false;
		}		
		@odbc_fetch_row($res);
		if(@file_exists($caminho.odbc_result($res,"IME_NOM")) && odbc_result($res,"IME_NOM")){
			$foto = $local.'application/helpers/inc_thumb.php?img=../../'.$caminho.odbc_result($res,"IME_NOM").'&size='.$tamanho;
		}		
		if(empty($foto)){
			$sql = "SELECT * FROM TAB_IME WHERE EMP_ISN = $imo_isn ORDER BY IME_NUM ASC";
			$res = @odbc_exec($conn,$sql);
			if (@odbc_error($res)) { 
				return false;
			}
			while(@odbc_fetch_row($res)) {
			 if(@file_exists($caminho.odbc_result($res,"IME_NOM")) && odbc_result($res,"IME_NOM")){ 	
				$foto = $local.'application/helpers/inc_thumb.php?img=../../'.$caminho.odbc_result($res,"IME_NOM").'&size='.$tamanho;
				break;
			  }
			}
		}		
		if(empty($foto)){
			$foto = $local.'application/helpers/inc_thumb.php?img=../images/indisponivel.jpg&size=180';
		} 
		@odbc_close($conn);		
		return $foto;
	}
	
	public function carregarFotos($cod,$caminho,$tamanho=130,$local='') {
	   $sql = "SELECT * FROM TAB_IME WHERE EMP_ISN = $cod ORDER BY IME_NUM";
	   $i = 0;	   
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) {
		   return false;
	   } 
	   $img = "";
	   while(@odbc_fetch_row($res)) {
		 if(@file_exists($caminho.odbc_result($res,"IME_NOM")) && odbc_result($res,"IME_NOM")){
			$img[$i]["descricao"] = odbc_result($res,"IME_DES");	   
			$img[$i]["nome"] = $local.'application/helpers/inc_thumb.php?img=../../'.$caminho.odbc_result($res,"IME_NOM").'&size='.$tamanho;
			$img[$i]["codigo"]    = odbc_result($res,"IME_ISN");
			$i++;
		 }
	   }
	   @odbc_close($conn);
	   return $img;
	 }
	 
	function carregarCodigosPOS($id,$tipo) {
	   $ret="";
	   if(is_array($id)){
		   $codcla = "";
		   $sql = "SELECT CLA.EMP_ISN FROM TAB_CLA CLA 
					   TAB_CCL CCL ON CCL.CLA_ISN = CLA.CLA_ISN 
					   WHERE CLA.TIM_ISN = $tipo ";
		   foreach($id as $i){
				if($i){
					$sql.=" AND CLA.EMP_ISN IN (SELECT DISTINCT(CLA2.EMP_ISN) FROM TAB_CLA CLA2 INNER JOIN TAB_CCL CCL2 ON CLA2.CLA_ISN = CCL2.CLA_ISN WHERE CCL2.DCA_ISN = $i) ";
				}
		   }
		   $sql.=" GROUP BY CLA.EMP_ISN ORDER BY CLA.EMP_ISN";
		   $conn=$this->connDb();	
		   $res = odbc_exec($conn,$sql);
		   if (@odbc_error($res)) { 
			   return false;
		   }
		   $ret="";
		   while(@odbc_fetch_row($res)) {
				$ret.=odbc_result($res,"EMP_ISN").','; 	   
		   }
		   if($ret) { $ret.="0"; } 
		   @odbc_close($conn);
	   }
	   return $ret;
	}
	
	 function carregarCodigosALT($id,$tipo) { 
	   $ret="";
	   if(is_array($id)){
		   $codcla = "";
		   $sql = "SELECT CLA.EMP_ISN FROM TAB_CLA CLA 
		   		   TAB_CCL CCL ON CCL.CLA_ISN = CLA.CLA_ISN 
				   WHERE CLA.TIM_ISN = $tipo ";
		   foreach($id as $i){
		   		if($i){
					$sql.=" AND CLA.EMP_ISN IN (SELECT DISTINCT(CLA2.EMP_ISN) FROM TAB_CLA CLA2 INNER JOIN TAB_CCL CCL2 ON CLA2.CLA_ISN = CCL2.CLA_ISN WHERE CCL2.CAR_ISN = $i) ";
				}
		   }
		   $sql.=" GROUP BY CLA.EMP_ISN ORDER BY CLA.EMP_ISN";
		   $conn=$this->connDb();	
		   $res = odbc_exec($conn,$sql);
		   if (@odbc_error($res)) { 
			   return false;
		   }
		   $ret="";
		   while(@odbc_fetch_row($res)) {
				$ret.=odbc_result($res,"EMP_ISN").','; 	   
		   }
		   if($ret) { $ret.="0"; } 
		   @odbc_close($conn);
	   }	
	   return $ret;
	}
	
	 function carregarCodigosQTDE($array,$tipo) {
	   $ret="";
	   if(is_array($array)){
		   $codcla = "";	   
		   $sql = "SELECT CLA.EMP_ISN FROM TAB_CLA CLA 
		   		   TAB_CCL CCL ON CCL.CLA_ISN = CLA.CLA_ISN 
				   WHERE CLA.TIM_ISN = $tipo ";
		   foreach($array as $ar){
		   		if($ar){
					 $sql.=" AND CLA.EMP_ISN IN ( SELECT DISTINCT(CLA2.EMP_ISN) FROM TAB_CLA CLA2 INNER JOIN TAB_CCL CCL2 ON CLA2.CLA_ISN = CCL2.CLA_ISN WHERE CCL2.CAR_ISN = ".$ar['id']." ";
					 if(!$ar['ini'] && $ar['fim']) {
						$sql.= " AND CCL2.CCL_QTD <= ".$ar['fim']." ) ";
					 }else if($ar['ini'] && !$ar['fim']) {
						$sql.= "AND CCL2.CCL_QTD >= ".$ar['ini']." ) ";
					 }else if($array[0]['ini'] && $ar['fim']) {
						$sql.= " AND CCL2.CCL_QTD >= ".$ar['ini']." AND CCL2.CCL_QTD <= ".$ar['fim']." ) ";
					 }
				}
		   }
		   $sql.=" GROUP BY CLA.EMP_ISN ORDER BY CLA.EMP_ISN ";
		   $conn=$this->connDb();	
		   $res = odbc_exec($conn,$sql);
		   if (@odbc_error($res)) { 
			   return false;
		   }
		   $ret="";
		   while(@odbc_fetch_row($res)) {
				$ret.=odbc_result($res,"EMP_ISN").','; 	   
		   }
		   if($ret) { $ret.="0"; } 
		   @odbc_close($conn);
	   }
	   return $ret;
	}
	
	function carregarCodigosVALOR($array,$tipo) {
	   $ret="";
	   if(is_array($array)){
		   $codcla = "";	   
		   $sql = "SELECT CLA.EMP_ISN FROM TAB_CLA CLA 
		   		   TAB_CCL CCL ON CCL.CLA_ISN = CLA.CLA_ISN 
				   WHERE CLA.TIM_ISN = $tipo ";
		   foreach($array as $ar){
		   		if($ar){
					 $sql.=" AND CLA.EMP_ISN IN ( SELECT DISTINCT(CLA2.EMP_ISN) FROM TAB_CLA CLA2 INNER JOIN TAB_CCL CCL2 ON CLA2.CLA_ISN = CCL2.CLA_ISN WHERE CCL2.CAR_ISN = ".$ar['id']." ";
					 if(!$ar['ini'] && $ar['fim']) {
						$sql.= " AND CCL2.CCL_VAL <= ".$ar['fim']." ) ";
					 }else if($ar['ini'] && !$ar['fim']) {
						$sql.= "AND CCL2.CCL_VAL >= ".$ar['ini']." ) ";
					 }else if($array[0]['ini'] && $ar['fim']) {
						$sql.= " AND CCL2.CCL_VAL >= ".$ar['ini']." AND CCL2.CCL_VAL <= ".$ar['fim']." ) ";
					 }
				}
		   }
		   $sql.=" GROUP BY CLA.EMP_ISN ORDER BY CLA.EMP_ISN ";
		   $conn=$this->connDb();	
		   $res = odbc_exec($conn,$sql);
		   if (@odbc_error($res)) { 
			   return false;
		   }
		   $ret="";
		   while(@odbc_fetch_row($res)) {
				$ret.=odbc_result($res,"EMP_ISN").','; 	   
		   }
		   if($ret) { $ret.="0"; } 
		   @odbc_close($conn);
	   }
	   return $ret;
	}
	
	 function carregarLancAVC($bairro,$tipos,$tipo,$codigoVal,$codigoQtde,$codigoAlt,$codigoPos,$cidade,$valini,$valfim,$area,$ordem,$area2,$tam=300,$local) {
	   $classe = "";	   
	   $br = "";
	   if($valini) { $valini=str_replace(",",".",$valini); }
	   if($valfim) { $valfim=str_replace(",",".",$valfim); }
	   if($area)   { $area=str_replace(",",".",$area); }
	   if($area2)  { $area2=str_replace(",",".",$area2); }
	   if(is_array($bairro)){
			$cont = count($bairro);
			if($bairro[0] == "TODOS" || $bairro[0] == " "){
				$br="1 = 1";
			}else {
				for($i=0;$i<$cont;$i++) {		  
				   if($cont == 1) {
					  $br = "E.EMP_DES_BAI='".$bairro[$i]."'";
				   }else if($cont > 1) {
					 if($i==0){
						$br = $br." ((E.EMP_DES_BAI='".$bairro[$i]."') ";
					 }else if($i == ($cont - 1)) {
						$br = $br." OR (E.EMP_DES_BAI='".$bairro[$i]."'))";
					 }else {
						$br = $br." OR (E.EMP_DES_BAI='".$bairro[$i]."')";
					 }
				   }
				   
				}
			}
	   }else {
	   	  $br = "1 = 1";
	   }
	   $bairro = $br;  
		if($ordem == "tim_des"){
			$sql = "SELECT E.EMP_ISN, E.EMP_DES,E.EMP_DES_END,
              E.EMP_DES_BAI,E.EMP_MES_ANO_ENT
              FROM (TAB_EMP E 
				  INNER JOIN TAB_CLA C ON E.EMP_ISN = C.EMP_ISN)
				  INNER JOIN TAB_TIM T ON T.TIM_ISN = C.TIM_ISN   
				  WHERE $bairro";
		
		}else if($ordem == "imo_val_are_priv"){
			
			$sql = "SELECT E.EMP_ISN, E.EMP_DES,E.EMP_DES_END,
              E.EMP_DES_BAI,E.EMP_MES_ANO_ENT,C.CLA_VAL_ARE_TOT
              FROM TAB_EMP E 
				  INNER JOIN TAB_CLA C ON E.EMP_ISN = C.EMP_ISN
				  WHERE $bairro ";
		
		}else {
			$sql = "SELECT E.EMP_ISN, E.EMP_DES,E.EMP_DES_END,
              E.EMP_DES_BAI,E.EMP_MES_ANO_ENT
              FROM TAB_EMP E WHERE $bairro ";
		}				
		$ctipo = "";		
		if($tipos && $bairro) {  
			$Ctipo = implode(",",$tipos);
			$sql = $sql." AND E.EMP_ISN IN (".$Ctipo.")";  
		}else if($tipos && !$bairro) {  
			$Ctipo = implode(",",$tipos);
			$sql = $sql." E.EMP_ISN IN (".$Ctipo.")";  
		}
	   if(is_array($codigoVal)) {
		   $str = implode(",",$codigoVal);
		   $sql = $sql." AND E.EMP_ISN IN (".$str.")";
	   }else if($codigoVal){
	   	   $sql = $sql." AND E.EMP_ISN IN (".$codigoVal.")";
	   }		
	   if(is_array($codigoQtde)) {
		   $str = implode(",",$codigoQtde);
		   $sql = $sql." AND E.EMP_ISN IN (".$str.")";
	   }else if($codigoQtde){
	   	   $sql = $sql." AND E.EMP_ISN IN (".$codigoQtde.")";
	   }
	   if(is_array($codigoAlt)) {
	      $str = implode(",",$codigoAlt);
		  $sql = $sql." AND E.EMP_ISN IN (".$str.")";
	   }else if($codigoAlt){
	   	   $sql = $sql." AND E.EMP_ISN IN (".$codigoAlt.")";
	   }
	   if(is_array($codigoPos)) {
			$str = implode(",",$codigoPos);
		    $sql = $sql." AND IMO_ISN IN (".$str.")";
	   }else if($codigoPos){
	   	   $sql = $sql." AND E.EMP_ISN IN (".$codigoPos.")";
	   }  
	   if($cidade){
	   	$sql.=" AND E.EMP_DES_LOC = '$cidade' ";
	   }
		if($ordem == "tim_des"){
			$sql = $sql."GROUP BY E.EMP_ISN, E.EMP_DES,E.EMP_DES_END,
              E.EMP_DES_BAI,E.EMP_MES_ANO_ENT,T.TIM_DES ORDER BY T.TIM_DES ";
		}else if($ordem == "imo_val_are_priv"){
			$sql = $sql."GROUP BY E.EMP_ISN, E.EMP_DES,E.EMP_DES_END,
              E.EMP_DES_BAI,E.EMP_MES_ANO_ENT,C.CLA_VAL_ARE_TOT ORDER BY C.CLA_VAL_ARE_TOT";
		}else {
			$sql = $sql." ORDER BY E.$ordem ";
		}
		$conn=$this->connDb();			
	    $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   $i = 0;
	   $emprnd="";
	   while(@odbc_fetch_row($res)) {
			$emprnd[$i]["emp_isn"]           = odbc_result($res,"EMP_ISN"); 	   
			$emprnd[$i]["emp_des"]           = odbc_result($res,"EMP_DES"); 	   
			$emprnd[$i]["emp_des_end"]       = odbc_result($res,"EMP_DES_END"); 	   
			$emprnd[$i]["emp_des_bai"]       = odbc_result($res,"EMP_DES_BAI"); 	   
			$emprnd[$i]["emp_mes_ano_ent"]   = odbc_result($res,"EMP_MES_ANO_ENT");
         	$i++; 
	   }
		$emprnd?$qtd = count($emprnd):$qtd=0;
		for($x=0;$x < $qtd;$x++) {
			$emprnd[$x]["foto"] = $this->buscarFoto2($emprnd[$x]["emp_isn"],$this->CaminhoFoto(),$tam,$local);
			$emprnd[$x]["fotos"] = $this->carregarFotos($emprnd[$x]["emp_isn"],$this->CaminhoFoto(),$tam*2,$local);
		}
		$cls = "";
		$y=0;
		for($x=0;$x < $qtd;$x++) {
		   $cl = $this->buscarClasseEsp($valini,$valfim,$area,$tipo,$emprnd[$x]["emp_isn"],$area2);
			if($cl) {
				$cls[$y]["cla_isn"]           = $cl["cla_isn"]; 	   
				$cls[$y]["cla_des"]           = $cl["cla_des"]; 	   
				$cls[$y]["cla_val"]           = $cl["cla_val"]; 	   
				$cls[$y]["cla_val_sin"]       = $cl["cla_val_sin"]; 	   
				$cls[$y]["cla_val_pct_sin"]   = $cl["cla_val_pct_sin"]; 	   
				$cls[$y]["cla_val_are_tot"]   = $cl["cla_val_are_tot"];
				$cls[$y]["cla_val_are_priv"]  = $cl["cla_val_are_priv"];
				$cls[$y]["cla_qtd_bal"]       = $cl["cla_qtd_bal"];
				$cls[$y]["cla_val_bal"]       = $cl["cla_val_bal"];
				$cls[$y]["cla_qtd_par"]       = $cl["cla_qtd_par"];
				$cls[$y]["tim_des"]           = $cl["tim_des"];
				$cls[$y]["cla_val_par"]       = $cl["cla_val_par"];
				$cls[$y]["emp_isn"]           = $emprnd[$x]["emp_isn"]; 	   
				$cls[$y]["emp_des"]           = $emprnd[$x]["emp_des"]; 	   
				$cls[$y]["emp_des_end"]       = $emprnd[$x]["emp_des_end"]; 	   
				$cls[$y]["emp_des_bai"]       = $emprnd[$x]["emp_des_bai"]; 	   
				$cls[$y]["emp_mes_ano_ent"]   = $emprnd[$x]["emp_mes_ano_ent"];
				$cls[$y]["imo_val_ven"]       = $this->menorpreco($cl["cla_isn"]); 
				$cls[$y]["foto"]              = $emprnd[$x]["foto"];
				$cls[$y]["fotos"]             = $emprnd[$x]["fotos"];
				$cls[$y]["caracs"]   		  = $this->consultarCaracteristicaPai($cl["cla_isn"]);
				$y++;
			}
			
		}
	   @odbc_close($conn);
	   return $cls;
	}
	
	function menorpreco($cod) {
	   $div = "";
	   $sql = "SELECT TOP 1 DIV_VAL_INI,DIV_QTD_UNI,UNI_NUM FROM TAB_DIV D,TAB_UNI U
		        WHERE D.DIV_ISN = U.DIV_ISN AND D.CLA_ISN = $cod ORDER BY DIV_VAL_INI DESC";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) {
		   return false;
	   } 
	   @odbc_fetch_row($res);
       $div = @odbc_result($res,"DIV_VAL_INI"); 	   	   
	   @odbc_close($conn);	
	   return $div;
	 }
	
	function buscarEmpreendimento($cod) {
	   $con = new ConexaoImobsale(); 
	   $emp = array();
	   $sql = "SELECT E.EMP_ISN, E.EMP_DES, E.EMP_DES_END, E.EMP_DES_BAI, E.EMP_MES_ANO_ENT, E.EMP_DES_TXT_DEST
             FROM TAB_EMP E 
			 WHERE EMP_TIP_DEST = 1 
			 AND EMP_TIP_DEST_PRIM = 1
			 AND EMP_ISN = $cod 
			 ORDER BY EMP_ISN";
	   //echo $sql;exit;
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
		$i = 0;
	   while(@odbc_fetch_row($res)) {
			$emp[$i]["emp_isn"]              = odbc_result($res,"EMP_ISN"); 	   
			$emp[$i]["emp_des"]              = odbc_result($res,"EMP_DES"); 	   
			$emp[$i]["emp_des_end"]          = odbc_result($res,"EMP_DES_END"); 	   
			$emp[$i]["emp_des_bai"]          = odbc_result($res,"EMP_DES_BAI"); 	   
			$emp[$i]["emp_mes_ano_ent"]      = odbc_result($res,"EMP_MES_ANO_ENT");
			$emp[$i]["emp_des_txt"]          = odbc_result($res,"EMP_DES_TXT_DEST");
         $i++; 
	   }
	   @odbc_close($conn);		
	   return $emp;
	}

	 function buscarEmp($cod) {
	   $con = new ConexaoImobsale(); 
	   $sql = "SELECT IME_NOM FROM TAB_IME WHERE EMP_ISN = $cod";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   if(odbc_fetch_row($res)) {
			$ime_nom   = odbc_result($res,"IME_NOM");
	   }
		else {
		   $ime_nom   = "imgindisponivel.gif";
		}
	   @odbc_close($conn);		
	   return $ime_nom;
	}

	 function buscarEmpTipo($tipo) {
		$sql = "SELECT DISTINCT(CLA.EMP_ISN) FROM TAB_CLA CLA,TAB_TIM TIM WHERE TIM.TIM_ISN = CLA.TIM_ISN AND TIM.TIM_ISN = $tipo ORDER BY EMP_ISN";
		
		
		
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
		$i = 0;
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   $cod="";
	   while(@odbc_fetch_row($res)) {
			$cod[$i]   = odbc_result($res,"EMP_ISN");
			$i++;
	   }
	   @odbc_close($conn);		
	   return $cod;
	}
	

	 function buscarEmpr($cod) {
	   $con = new ConexaoImobsale(); 
	   $sql = "SELECT E.EMP_ISN,E.EMP_DES,E.EMP_MES_ANO_ENT,E.EMP_DES_BAI
		       FROM TAB_EMP E WHERE E.EMP_ISN = ".$cod;
			 
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
		$emp = array();  
		$i = 0;
	   while(@odbc_fetch_row($res)) {
			$emp[$i]["emp_isn"]            = odbc_result($res,"EMP_ISN");
			$emp[$i]["emp_des"]            = odbc_result($res,"EMP_DES");
			$emp[$i]["emp_mes_ano_ent"]    = odbc_result($res,"EMP_MES_ANO_ENT");
			$emp[$i]["emp_des_bai"]        = odbc_result($res,"EMP_DES_BAI");
			$emp[$i]["emp_tip_usar"]       = odbc_result($res,"EMP_TIP_USAR_TXT_DET");
			$emp[$i]["emp_des_txt_det"]    = odbc_result($res,"EMP_DES_TXT_DET");
			$emp[$i]["emp_des_txt_det_2"]  = odbc_result($res,"EMP_DES_TXT_DET_2");
	   }
		/*for($x=0;$x < 1;$x++) {
			$emprd[$x]["emp_isn"]           = $emp[$x]["emp_isn"];
			$emprd[$x]["emp_des"]           = $emp[$x]["emp_des"];
			$emprd[$x]["emp_mes_ano_ent"]   = $emp[$x]["emp_mes_ano_ent"];
			$emprd[$x]["emp_des_bai"]       = $emp[$x]["emp_des_bai"];
			//$emprd[$x]["ime_nom"]           = $this->buscarEmp($emprd[$x]["emp_isn"]);
		}*/
	   @odbc_close($conn);		
	   return $emp;
	}

	 function buscarEmps() {
       $idm = 1;
	   if($idm == 2) {
	      $textDestq = "EMP_DES_TXT_DEST_ING";
	   }else if($idm == 3){
	   	  $textDestq = "EMP_DES_TXT_DEST_ESP";
	   }else {
	      $textDestq = "EMP_DES_TXT_DEST";
	   }
	   $con = new ConexaoImobsale(); 
	   $emp = array();
	   $sql = "SELECT E.EMP_ISN, E.EMP_DES,E.EMP_DES_END,E.EMP_DES_BAI,E.EMP_MES_ANO_ENT,E.EMP_DES_TXT_DEST
             FROM TAB_EMP E WHERE EMP_TIP_DEST = 1 AND EMP_TIP_DEST_PRIM = 1 ORDER BY EMP_ISN";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
		$i = 0;
	   while(@odbc_fetch_row($res)) {
			$emp[$i]["emp_isn"]              = odbc_result($res,"EMP_ISN"); 	   
			$emp[$i]["emp_des"]              = odbc_result($res,"EMP_DES"); 	   
			$emp[$i]["emp_des_end"]          = odbc_result($res,"EMP_DES_END"); 	   
			$emp[$i]["emp_des_bai"]          = odbc_result($res,"EMP_DES_BAI"); 	   
			$emp[$i]["emp_mes_ano_ent"]      = odbc_result($res,"EMP_MES_ANO_ENT");
			$emp[$i]["emp_des_txt"]          = odbc_result($res,$textDestq);
			if(empty($emp[$i]["emp_des_txt"])){
				$emp[$i]["emp_des_txt"]          = odbc_result($res,"EMP_DES_TXT_DEST");
			}
         $i++; 
	   }
	   @odbc_close($conn);		
	   return $emp;
	}

	 function buscarImg($cod) {
	   $con = new ConexaoImobsale(); 
	   $sql = "SELECT IME_NOM FROM TAB_IME WHERE EMP_ISN = $cod AND IME_TIP_DEST = 1";
		$conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
		$i = 0;
	   while(@odbc_fetch_row($res)) {
			$img["ime_nom"] = odbc_result($res,"IME_NOM"); 	   
			$i++;
	   }
	   @odbc_close($conn);		
	   return $img;
	}
	
	 function consultarCaracteristicaPai($codigo) {
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descCarDes = "CAR_DES_ING"; 
	   }else if($idm == 3){
	   	  $descCarDes = "CAR_DES_ESP"; 
	   }else {
	      $descCarDes = "CAR_DES"; 
	   }  
	   $i = 0;
	   $caracPai = array();
	   $caracPaiVal = array();
	   $sql = "SELECT CCL.*, CAR.CAR_DES,CAR.$descCarDes,CAR.CAR_TIP, CAR.CAR_DES_UNI FROM TAB_CCL CCL
				  LEFT JOIN TAB_CAR CAR ON CAR.CAR_ISN = CCL.CAR_ISN
				  WHERE CCL.CLA_ISN = $codigo AND
				  CAR.CAR_ISN_PAI = 0 
				  ORDER BY CAR.CAR_DES";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		  return false;
	   }
	   $carsFilha = array();
	   while(@odbc_fetch_row($res)) {
	   		$caracPai[$i]["imc_isn"]           = odbc_result($res,"CCL_ISN"); 	   
			$caracPai[$i]["imc_car_isn"]       = odbc_result($res,"CAR_ISN"); 
			$caracPai[$i]["car_car_tip"]       = odbc_result($res,"CAR_TIP"); 	   
			$caracPai[$i]["car_car_des"]       = odbc_result($res,$descCarDes); 
			$caracPai[$i]["car_car_des_uni"]   = odbc_result($res,"CAR_DES_UNI"); 
			$caracPai[$i]["imc_qtd"]           = odbc_result($res,"CCL_QTD");
			$caracPai[$i]["imc_des"]           = odbc_result($res,"CCL_DES");
			$caracPai[$i]["imc_val"]           = odbc_result($res,"CCL_VAL");
			$caracPai[$i]["imc_tip_con"]       = odbc_result($res,"CCL_TIP_CON");
			$caracPai[$i]["imc_dca_isn"]       = odbc_result($res,"DCA_ISN");
			if(empty($caracPai[$i]["car_car_des"])){
				$caracPai[$i]["car_car_des"] = odbc_result($res,"CAR_DES"); 
			}
			$i++;
		}	
		for($i=0;$i<count($caracPai);$i++) {
			if($caracPai[$i]["car_car_tip"] == 1) {
			   if($caracPai[$i]["imc_tip_con"] == 0) {
			      $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			      $caracPaiVal[$i]["imc_tip_con"] = "NAO";
			   }else if($caracPai[$i]["imc_tip_con"] == 1) {
			      $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			      $caracPaiVal[$i]["imc_tip_con"] = "SIM";
			   }
			}else if($caracPai[$i]["car_car_tip"] == 2) {
			   $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			   $caracPaiVal[$i]["imc_qtd"] = $caracPai[$i]["imc_qtd"];
			}else if($caracPai[$i]["car_car_tip"] == 3) {
			      $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			      $caracPaiVal[$i]["dca_des"]      = $this->dcaValor($caracPai[$i]["imc_dca_isn"]);
			}else if($caracPai[$i]["car_car_tip"] == 4) {
			   $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			   $caracPaiVal[$i]["imc_des"] = $caracPai[$i]["imc_des"];
			}else if($caracPai[$i]["car_car_tip"] == 5) {
			   $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			   $caracPaiVal[$i]["imc_val"] = $caracPai[$i]["imc_val"];
			   $caracPaiVal[$i]["car_car_des_uni"] = $caracPai[$i]["car_car_des_uni"] ;
			}
            $caracPaiVal[$i]["car_car_tipPai"] = $caracPai[$i]["car_car_tip"]; 
			$caracPaiVal[$i]["imo_obs"] = $this->obsValor($codigo); 
		}
		for($i=0;$i < count($caracPai);$i++) {
		   $caracPai[$i]["imc_isn"] = $caracPai[$i]["imc_isn"];
			$caracPai[$i]["imc_car_isn"] = $caracPai[$i]["imc_car_isn"];
			$caracPai[$i]["car_car_tip"] = $caracPai[$i]["car_car_tip"];
			$caracPai[$i]["car_car_des"] = $caracPai[$i]["car_car_des"];
			$caracPai[$i]["car_car_des_uni"] = $caracPai[$i]["car_car_des_uni"];
			$caracPai[$i]["imc_qtd"] = $caracPai[$i]["imc_qtd"];
			$caracPai[$i]["imc_des"]  = $caracPai[$i]["imc_des"];
			$caracPai[$i]["imc_val"]  = $caracPai[$i]["imc_val"];
			$caracPai[$i]["imc_tip_con"] = $caracPai[$i]["imc_tip_con"]; 
			$caracPai[$i]["imc_dca_isn"] = $caracPai[$i]["imc_dca_isn"];
		    $caracPaiVal[$i]["caracteristicas"]  = $this->consultarCaracteristicaFilha($codigo,$caracPai[$i]["imc_car_isn"]);
		}	
	   @odbc_close($conn);		
	   return $caracPaiVal;
	}
	
	function dcaValor($numero) {
	   $sql = "SELECT DCA.DCA_DES FROM TAB_DCA DCA WHERE DCA.DCA_ISN = ".$numero;
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
		  $dca_des  = odbc_result($res,"DCA_DES");
	   }   
	   @odbc_close($conn);		
	   return $dca_des;
	}
	
	function obsValor($numero) {
	   $sql = "SELECT IMO_OBS FROM TAB_IMO WHERE IMO_ISN = ".$numero;
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   $obs_des = "";
	   while(@odbc_fetch_row($res)) {
		  $obs_des  = odbc_result($res,"IMO_OBS");
	   }	   
	   @odbc_close($conn);		
	   return $obs_des;
	}
	
	function consultarCaracteristicaFilha($codigo,$car_isn) {
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descCarDes = "CAR_DES_ING"; 
	   }else if($idm == 3){
	   	  $descCarDes = "CAR_DES_ESP"; 
	   }else {
	      $descCarDes = "CAR_DES"; 
	   }  
	   $caracFilha = array();
	   $caracFilhaVal = array();
	   $carsPai = array();
	   $i = 0;
	   $sql = "SELECT CAR.CAR_DES,CAR.$descCarDes,CAR.CAR_TIP, CCL.*  
			   FROM TAB_CCL CCL
			   LEFT JOIN TAB_CAR CAR ON CAR.CAR_ISN = CCL.CAR_ISN 
			   WHERE CCL.CLA_ISN = $codigo AND
			   CCL.CAR_ISN IN (SELECT CAR_ISN FROM TAB_CAR WHERE CAR_ISN_PAI =".$car_isn.")ORDER BY CCL_NUM_SEQ";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		  return false;
	   }
	   while(@odbc_fetch_row($res)) {			
			$caracFilha[$i]["imc_isn"]           = odbc_result($res,"CCL_ISN"); 	   
			$caracFilha[$i]["imc_car_isn"]       = odbc_result($res,"CAR_ISN"); 	   
			$caracFilha[$i]["car_car_des"]       = odbc_result($res,$descCarDes); 	   
			$caracFilha[$i]["car_car_tip"]       = odbc_result($res,"CAR_TIP"); 	   
			$caracFilha[$i]["imc_tip_con"]       = odbc_result($res,"CCL_TIP_CON"); 	   
			$caracFilha[$i]["imc_qtd"]           = odbc_result($res,"CCL_QTD"); 	   
			$caracFilha[$i]["imc_val"]           = odbc_result($res,"CCL_VAL"); 	   
			$caracFilha[$i]["imc_des"]           = odbc_result($res,"CCL_DES"); 	   
			$caracFilha[$i]["imc_num_seq"]       = odbc_result($res,"CCL_NUM_SEQ"); 	   
			$caracFilha[$i]["dca_isn"]           = odbc_result($res,"DCA_ISN"); 
			if(empty($caracFilha[$i]["car_car_des"])){
				$caracFilha[$i]["car_car_des"]  = odbc_result($res,"CAR_DES");
			}	
			$i++;
	   }	
		for($i=0;$i<count($caracFilha);$i++) {
			if($caracFilha[$i]["car_car_tip"] == 1) {
			   $caracFilha[$i]["imc_tip_con"];
			   if($caracFilha[$i]["imc_tip_con"] == 0) {
				  $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
				  $caracFilhaVal[$i]["imc_tip_con"] = "NAO";
			   }else if($caracFilha[$i]["imc_tip_con"] == 1) {
				  $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
				  $caracFilhaVal[$i]["imc_tip_con"] = "SIM";
			   }
			}else if($caracFilha[$i]["car_car_tip"] == 2) {
			   $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
			   $caracFilhaVal[$i]["imc_qtd"] = $caracFilha[$i]["imc_qtd"];
			}else if($caracFilha[$i]["car_car_tip"] == 3) {
				  $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
				  $caracFilhaVal[$i]["dca_des"]      = $this->dcaValor($caracFilha[$i]["dca_isn"]);
			}else if($caracFilha[$i]["car_car_tip"] == 4) {
			   $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
			   $caracFilhaVal[$i]["imc_des"] = $caracFilha[$i]["imc_des"];
			}else if($caracFilha[$i]["car_car_tip"] == 5) {
			   $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
			   $caracFilhaVal[$i]["imc_val"] = $caracFilha[$i]["imc_val"];
			}
			$caracFilhaVal[$i]["imc_num_seq"] = $caracFilha[$i]["imc_num_seq"]; 
			$caracFilhaVal[$i]["car_car_tipFilha"] = $caracFilha[$i]["car_car_tip"]; 
		}
	   @odbc_close($conn);		
	   return $caracFilhaVal;
	}
	function buscarClasseEmp($cod) {
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
		  $descTip = "TIM_DES_ING";
		}else if($idm == 3) {
		   $descTip = "TIM_DES_ESP";
		}else {
		   $descTip = "TIM_DES";
		}
	   $sql = "SELECT C.*, T.TIM_DES,T.$descTip FROM TAB_CLA C, TAB_TIM T WHERE C.TIM_ISN = T.TIM_ISN AND C.EMP_ISN = $cod ORDER BY C.CLA_ISN";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
		$i = 0;
	   while(@odbc_fetch_row($res)) {
		  $cls[$i]["id"]          = odbc_result($res,"CLA_ISN");
		  $cls[$i]["des"]         = odbc_result($res,"CLA_DES");
		  $cls[$i]["tim_isn"]     = odbc_result($res,"TIM_ISN");
		  $cls[$i]["val"]         = odbc_result($res,"CLA_VAL");
		  $cls[$i]["val_are_tot"] = odbc_result($res,"CLA_VAL_ARE_TOT");
		  $cls[$i]["val_are_priv"]= odbc_result($res,"CLA_VAL_ARE_PRIV");
		  $cls[$i]["val_sin"]     = odbc_result($res,"CLA_VAL_SIN");
		  $cls[$i]["qtd_bal"]     = odbc_result($res,"CLA_QTD_BAL");
		  $cls[$i]["val_bal"]     = odbc_result($res,"CLA_VAL_BAL");
		  $cls[$i]["qtd_par"]     = odbc_result($res,"CLA_QTD_PAR");
		  $cls[$i]["val_par"]     = odbc_result($res,"CLA_VAL_PAR");
		  $cls[$i]["tim_des"]     = odbc_result($res,$descTip);
		  $cls[$i]["cla_mes_ano_ent"] = odbc_result($res,"CLA_MES_ANO_ENT");
		  if(empty($cls[$i]["tim_des"])){
		  	$cls[$i]["tim_des"] = odbc_result($res,"TIM_DES");
		  }
		  $cls[$i]["caracs"]   = $this->consultarCaracteristicaPai($cls[$i]["id"]);
		  $cls[$i]["unidades"] = $this->buscarUnidade($cls[$i]["id"]);
		  $i++;
	   }
	   
	   @odbc_close($conn);		
	   return $cls;
	}
	
	function getTipoEmp($cod) {
	   $sql = "SELECT TOP 1 T.TIM_DES FROM TAB_CLA C, TAB_TIM T WHERE C.TIM_ISN = T.TIM_ISN AND C.EMP_ISN = $cod ORDER BY C.CLA_ISN";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   @odbc_fetch_row($res);
	   $cls = odbc_result($res,"TIM_DES");
	   @odbc_close($conn);		
	   return $cls;
	}

	 function buscarCaracteristicaPai() {
      $crPai = new Carac_Pai_Lanc();
	   $caracPai = array();
	   $imoveis = $_SESSION["arrayImoveisMAN"];
	   $qtd = count($imoveis);
	   for($i=0;$i<$qtd;$i++) {
	     $caracPai[$i] = $crPai->carregarCaracteristicaPai($imoveis[$i][imo_isn]);
	   } 
	   return $caracPai;
	}

	 function carregarCaracteristicaPai($codigo) {
       $crPai = new Carac_Pai_Lanc();
	   $caracPai = array();
	   $caracPai = $crPai->carregarCaracteristicaPai($codigo);
	   return $caracPai;
	}
	
	function carregaCaracteristicasAv($tip) {
	   $idm = $_SESSION['idioma'];
	   if($idm == 1) {
	      $descCar = "CAR_DES";
	   }
	   if($idm == 2) {
	      $descCar = "CAR_DES_ING";
	   }
	   $con = new ConexaoImobsale(); 
	   $i = 0;
	   $imovel = array();
	   $sql = "SELECT DISTINCT $descCar,CAR_ISN,CAR_TIP 
	   FROM TAB_CAR WHERE CAR_TIP_NET_PESQ = 1 AND CAR_TIP <> 4	 AND CAR_ISN_PAI = 0 AND TIM_ISN = $tip ORDER BY $descCar";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["car_isn"]           = odbc_result($res,"CAR_ISN"); 	   
			$imovel[$i]["car_des"]           = odbc_result($res,$descCar); 	   
			$imovel[$i]["car_tip"]           = odbc_result($res,"CAR_TIP"); 	   
         $i++; 
	   }
	   $valpre = array();
	   for($cont=0;$cont<count($imovel);$cont++) {
	      if($imovel[$cont]["car_tip"] == 3) {
		     $valpre = $this->obterValPre($imovel[$cont][car_isn]);  
			 $imovel[$cont]["val_pre"] = $valpre;
		  }	 
	   }
	   @odbc_close($conn);		
		return $imovel;
	}
	function obterValPre($num) {
	   $con = new ConexaoImobsale(); 
	   $i = 0;
	   $valores = array();
	   $sql = "SELECT DCA_ISN,DCA_DES 
	   FROM TAB_DCA WHERE CAR_ISN = $num   ORDER BY DCA_DES";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$valores[$i]["dca_isn"]           = odbc_result($res,"DCA_ISN"); 	   
			$valores[$i]["dca_des"]           = odbc_result($res,"DCA_DES"); 	   
            $i++; 
	   }
	   @odbc_close($conn);		
	   return $valores;
	}
   function carregarCodigos() {
	   $con = new ConexaoImobsale(); 
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT IMO_ISN,IMO_DES_TXT_DEST 
	   FROM TAB_IMO WHERE IMO_TIP_DEST = 1 AND IMO_TIP_DEST_PRIM_PAG = 1   ORDER BY IMO_ISN";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]            = odbc_result($res,"IMO_ISN"); 	   
			$codigo[$i]["imo_des_est"]        = odbc_result($res,"IMO_DES_TXT_DEST"); 	   
         $i++; 
	   }
	   @odbc_close($conn);		
	   return $codigo;
   }

   function carregarTodosCodigos() {
	   $con = new ConexaoImobsale(); 
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT IMO_ISN,IMO_DES_TXT_DEST 
	   FROM TAB_IMO WHERE IMO_TIP_DEST = 1 ORDER BY IMO_ISN";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]            = odbc_result($res,"IMO_ISN"); 	   
			$codigo[$i]["imo_des_est"]        = odbc_result($res,"IMO_DES_TXT_DEST"); 	   
         $i++; 
	   }
	   @odbc_close($conn);		
	   return $codigo;
   }

   function consultarDesTipo($cod) {
       $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descTipo = "TIM_DES_ING";
	   }else if($idm == 3) {
	      $descTipo = "TIM_DES_ESP";
	   }else {
	      $descTipo = "TIM_DES";
	   }
	   $sql = "SELECT TIM_DES,$descTipo FROM TAB_TIM WHERE TIM_ISN = $cod";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   $tipo="";
	   while(@odbc_fetch_row($res)) {
			$tipo= odbc_result($res,$descTipo); 
			if(empty($tipo)){
				$tipo= odbc_result($res,"TIM_DES");
			}	   
	   }
	   @odbc_close($conn);		
	   return $tipo;
   }

   function carregarDestaquesCapa($cod) {
	   $con = new ConexaoImobsale();  
	   //$i = 0;
	   $destaque = array();
	   $sql = "SELECT IMI_NOM  FROM TAB_IMI WHERE IMO_ISN = $cod AND IMI_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$destaque["imi_nom"] = odbc_result($res,"IMI_NOM"); 	   
	   }
	   @odbc_close($conn);		
	   return $destaque;
   }
   function carregarDestaques($cod) {
	   $con = new ConexaoImobsale();  
	   //$i = 0;
	   $destaque = array();
	   $sql = "SELECT IMI_NOM  FROM TAB_IMI WHERE IMO_ISN = $cod AND IMI_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$destaque["imi_nom"] = odbc_result($res,"IMI_NOM"); 	   
	   }
	   @odbc_close($conn);		
	   return $destaque;
   }
 function carregarCodigosAdm() {
	   $con = new ConexaoImobsale();  
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT EMP_ISN,EMP_DES_END,EMP_TIP_DEST,
	           EMP_TIP_DEST_PRIM 
	           FROM TAB_EMP
			   ORDER BY EMP_ISN";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["emp_isn"]              = odbc_result($res,"EMP_ISN"); 	   
			$codigo[$i]["emp_des_end"]          = odbc_result($res,"EMP_DES_END"); 	   
			$codigo[$i]["emp_tip_dest"]         = odbc_result($res,"EMP_TIP_DEST"); 	   
			$codigo[$i]["emp_tip_dest_prim"]    = odbc_result($res,"EMP_TIP_DEST_PRIM");
			$i++; 
	   }
	   for($x=0;$x < count($codigo);$x++) {
			$imovel[$x]["imo_isn"]              = $codigo[$x]["emp_isn"]; 	   
			$imovel[$x]["imo_des_end"]          = $codigo[$x]["emp_des_end"]; 	   
			$imovel[$x]["imo_tip_dest"]         = $codigo[$x]["emp_tip_dest"]; 	   
			$imovel[$x]["imo_tip_dest_prim"]    = $codigo[$x]["emp_tip_dest_prim"]; 				            $imovel[$x]["foto"]                 = $this->carregarFotoCod($codigo[$x]["emp_isn"]);
	   }
	   @odbc_close($conn);		
	   return $imovel;
 }
 function carregarCodigosAdmEsp($cod) {
	   $con = new ConexaoImobsale();  
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT EMP_ISN,EMP_DES_END,EMP_TIP_DEST,
	           EMP_TIP_DEST_PRIM
	           FROM TAB_EMP 
			   WHERE EMP_ISN = $cod";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["emp_isn"]              = odbc_result($res,"EMP_ISN"); 	   
			$codigo[$i]["emp_des_end"]          = odbc_result($res,"EMP_DES_END"); 	   
			$codigo[$i]["emp_tip_dest"]         = odbc_result($res,"EMP_TIP_DEST"); 	   
			$codigo[$i]["emp_tip_dest_prim"]    = odbc_result($res,"EMP_TIP_DEST_PRIM"); 	            $i++; 
	   }
	   for($x=0;$x < count($codigo);$x++) {
			$imovel[$x]["imo_isn"]              = $codigo[$x]["emp_isn"]; 	   
			$imovel[$x]["imo_des_end"]          = $codigo[$x]["emp_des_end"]; 	   
			$imovel[$x]["imo_tip_dest"]         = $codigo[$x]["emp_tip_dest"]; 	   
			$imovel[$x]["imo_tip_dest_prim"]    = $codigo[$x]["emp_tip_dest_prim"]; 				            $imovel[$x]["foto"]                 = $this->carregarFotoCod($codigo[$x]["emp_isn"]);
	   }
	   @odbc_close($conn);		
	   return $imovel;
 }
   function carregarFotoCod($cod) {
	   $con = new ConexaoImobsale();  
	   $codigo = array();
	   $sql = "SELECT IME_NOM 
	   FROM TAB_IME WHERE EMP_ISN = $cod AND
	   IME_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$img  =  odbc_result($res,"IME_NOM"); 	   
	   }
	   @odbc_close($conn);		
	   return $img;
   }
 function buscarImagensAdm($cod) {
	   $con = new ConexaoImobsale();  
	   $codigo = array();
	   $i = 0;
	   $sql = "SELECT IME_NOM 
	   FROM TAB_IME WHERE EMP_ISN = $cod"; 
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$img[$i]["nome"]  =  odbc_result($res,"IME_NOM"); 	   
			$i++;
	   }
	   @odbc_close($conn);
	   for($x=0;$x < count($img);$x++) {
	       $imagem[$x]["nome"]     =  $img[$x]["nome"];
		   $destaque               =  $this->carregarFotoCod($cod);        
		   if($imagem[$x]["nome"] == $destaque) {
		      $imagem[$x]["destaque"] =  $imagem[$x]["nome"];
		   }
	   }
	   return $imagem;
  }
  function alterarImagemAdm($cod,$nom) {
	   $con = new ConexaoImobsale();  
	   $sql = "UPDATE TAB_IME SET IME_TIP_DEST = 0 
	           WHERE EMP_ISN = $cod AND IME_TIP_DEST = 1"; 
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   $sql2 = "UPDATE TAB_IME SET IME_TIP_DEST = 1
	            WHERE EMP_ISN = $cod AND IME_NOM = '$nom'";
	   $res = odbc_exec($conn,$sql2);
	   @odbc_close($conn);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
  function setarDestaque($cod) {
	   $con = new ConexaoImobsale();  
	   $sql = "UPDATE TAB_EMP SET EMP_TIP_DEST = 1
	          WHERE EMP_ISN = $cod";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
  function setarDestaquePrim($cod) {
	   $con = new ConexaoImobsale();  
	   $sql = "UPDATE TAB_EMP SET EMP_TIP_DEST = 1,
	           EMP_TIP_DEST_PRIM = 1 
	           WHERE EMP_ISN = $cod";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
  function retirarDestaque($cod) {
	   $con = new ConexaoImobsale();  
	   $sql = "UPDATE TAB_EMP SET EMP_TIP_DEST = 0
	           WHERE EMP_ISN = $cod AND EMP_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
  function retirarDestaquePrim($cod) {
	   $con = new ConexaoImobsale();  
	   $sql = "UPDATE TAB_EMP SET EMP_TIP_DEST_PRIM = 0
	           WHERE EMP_ISN = $cod AND EMP_TIP_DEST_PRIM = 1";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
}
?>