<?php
class Tab_cota extends Model{
	function Tab_cota(){
		parent::Model();
	}
	
	function getDados1(){
		$this->db->where('COTA_NUM', 1);
		$query = $this->db->get('tab_cota');
		$row ="";
		if($query->num_rows() > 0){
			foreach($query->result() as $dados){
				$row[]=array('id'=>$dados->COTA_ISN,'num'=>$dados->COTA_NUM,'mes'=>$dados->COTA_MES,'ano'=>$dados->COTA_ANO,'val'=>$dados->COTA_VAL);
			}
			
		}
		return $row;
	}
	
	function getDados2(){
		$this->db->where('COTA_NUM', 2);
		$query = $this->db->get('tab_cota');
		$row ="";
		if($query->num_rows() > 0){
			foreach($query->result() as $dados){
				$row[]=array('id'=>$dados->COTA_ISN,'num'=>$dados->COTA_NUM,'mes'=>$dados->COTA_MES,'ano'=>$dados->COTA_ANO,'val'=>$dados->COTA_VAL);
			}
			
		}
		return $row;
	}

}
?>