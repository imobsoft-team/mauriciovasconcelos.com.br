<?php class ImovelPC {
     var $ipc_Isn          = null;
	 var $pco_Isn          = null;
	 var $ipc_Num_Seq      = null;
	 var $ipc_Cod_Imo      = null;
	 var $ipc_Des_End      = null;
	 var $ipc_Dat_Ent      = null;
	 var $ipc_Dat_Reaj     = null;
	 var $ipc_Dat_Fim      = null;
	 var $ipc_Val_Alu      = null;
	 var $ipc_Des_Ult_Pgto = null;
	 var $ipc_Des_Tip      = null;
	 var $ipc_Des_Sit      = null;
	 
	 function ImovelPC() {}
	 
	 function setIpc_Isn($num) {
	    $this->ipc_Isn = $num;
	 }
	 function getIpc_Isn() {
	    return $this->ipc_Isn;
	 }
	 function setPco_Isn($numr) {
	    $this->pco_Isn = $numr;
	 }
	 function getPco_Isn() {
	    return $this->pco_Isn;
	 }
	 function setIpc_Num_Seq($ns) {
	    $this->ipc_Num_Seq = $ns;
	 }
	 function getIpc_Num_Seq() {
	    return $this->ipc_Num_Seq;
	 }
	 function setIpc_Cod_Imo($cod) {
	    $this->ipc_Cod_Imo = $cod;
	 }
	 function getIpc_Cod_Imo() {
	    return $this->ipc_Cod_Imo;
	 }
	 function setIpc_Des_End($end) {
	    $this->ipc_Des_End = $end;
	 }
	 function getIpc_Des_End() {
	    return $this->ipc_Des_End;
	 }
	 function setIpc_Dat_Ent($date) {
	    $this->ipc_Dat_Ent = $date;
	 }
	 function getIpc_Dat_Ent() {
	    return $this->ipc_Dat_Ent;
	 }
	 function setIpc_Dat_Reaj($dater) {
	    $this->ipc_Dat_Reaj = $dater;
	 }
	 function getIpc_Dat_Reaj() {
	    return $this->ipc_Dat_Reaj;
	 }
	 function setIpc_Dat_Fim($datef) {
	    $this->ipc_Dat_Fim = $datef;
	 }
	 function getIpc_Dat_Fim() {
	    return $this->ipc_Dat_Fim;
	 }
	 function setIpc_Val_Alu($valal) {
	    $this->ipc_Val_Alu = $valal;
	 }
	 function getIpc_Val_Alu() {
	    return $this->ipc_Val_Alu;
	 }
	 function setIpc_Des_Ult_Pgto($up) {
	    $this->ipc_Des_Ult_Pgto = $up;
	 }
	 function getIpc_Des_Ult_Pgto() {
	    return $this->ipc_Des_Ult_Pgto;
	 }
	 function setIpc_Des_Tip($tp) {
	    $this->ipc_Des_Tip = $tp;
	 }
	 function getIpc_Des_Tip() {
	    return $this->ipc_Des_Tip;
	 }
	 function setIpc_Des_Sit($sit) {
	    $this->ipc_Des_Sit = $sit;
	 }
	 function getIpc_Des_Sit() {
	    return $this->ipc_Des_Sit;
	 }

  }
?>