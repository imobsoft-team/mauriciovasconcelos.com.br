<?php class LancamentoPC {
     var $lpc_Isn             =  null;
	 var $pco_Isn             =  null;
	 var $lpc_Num_Seq         = null;
	 var $lpc_Cod_Imo         = null;
	 var $lpc_Des_Ctr_Loc     = null;
	 var $lpc_Des_Inq_Out     = null;
	 var $lpc_Des_Mes_Ano     = null;
	 var $lpc_Val_Deb         = null;
	 var $lpc_Val_Cre         = null;
	 var $lpc_Des_Tip_Com     = null;
	 var $lpc_Val_Sal         = null;
	 var $lpc_Des_Tip_Deb_Cre = null;
	 var $lpc_Des_Tip_Pag     = null;
	 
	 function LancamentoPC() {}
	 
	 function setLpc_Isn($num) {
	    $this->lpc_Isn = $num;
	 }
	 
	 function getLpc_Isn() {
	    return $this->lpc_Isn;
	 }
	 function setPco_Isn($numr) {
	    $this->pco_Isn = $numr;
	 }
	 
	 function getPco_Isn() {
	    return $this->pco_Isn;
	 }
	 function setLpc_Num_Seq($seq) {
	    $this->lpc_Num_Seq = $seq;
	 }
	 
	 function getLpc_Num_Seq() {
	    return $this->lpc_Num_Seq;
	 }
	 function setLpc_Cod_Imo($cod) {
	    $this->lpc_Cod_Imo = $cod;
	 }
	 
	 function getLpc_Cod_Imo() {
	    return $this->lpc_Cod_Imo;
	 }

	 function setLpc_Des_Ctr_Loc($cont) {
	    $this->lpc_Des_Ctr_Loc = $cont;
	 }
	 
	 function getLpc_Des_Ctr_Loc() {
	    return $this->lpc_Des_Ctr_Loc;
	 }
	 function setLpc_Des_Inq_Out($inq) {
	    $this->lpc_Des_Inq_Out = $inq;
	 }
	 
	 function getLpc_Des_Inq_Out() {
	    return $this->lpc_Inq_Out;
	 }
	 function setLpc_Des_Mes_Ano($dat) {
	    $this->lpc_Des_Mes_Ano = $dat;
	 }
	 
	 function getLpcDes_Mes_Ano() {
	    return $this->lpcDes_Mes_Ano;
	 }
	 function setLpc_Val_Deb($val) {
	    $this->lpc_Val_Deb = $val;
	 }
	 
	 function getLpc_Val_Deb() {
	    return $this->lpc_Val_Deb;
	 }
	 function setLpc_Val_Cre($valc) {
	    $this->lpc_Val_Cre = $valc;
	 }
	 
	 function getLpc_Val_Cre() {
	    return $this->lpc_Val_Cre;
	 }
	 function setLpc_Des_Tip_Com($desc) {
	    $this->lpc_Des_Tip_Com = $desc;
	 }
	 
	 function getLpc_Des_Tip_Com() {
	    return $this->lpc_Des_Tip_Com;
	 }
	 function setLpc_Val_Sal($sal) {
	    $this->lpc_Val_Sal = $sal;
	 }
	 
	 function getLpc_Val_Sal() {
	    return $this->lpc_Val_Sal; 
	 }
	 function setLpc_Des_Tip_Deb_Cre($dtdc) {
	    $this->lpc_Des_Tip_Deb_Cre = $dtdc;
	 }
	 
	 function getLpc_Des_Tip_Deb_Cre() {
	    return $this->lpc_Des_Tip_Deb_Cre; 
	 }
	 function setLpc_Des_Tip_Pag($tp) {
	    $this->lpc_Des_Tip_Pag = $tp;
	 }
	 
	 function getLpc_Des_Tip_Pag() {
	    return $this->lpc_Des_Tip_Pag; 
	 }

  }
?>