<?php
class Cliente extends Controller {
	public $layout = 'default';
	public $title = '::: Equatorial Im�veis:::';
	public $css = array('default','jquery.lightbox-0.5','global','ajaxtabs');
	public $js = array('jquery-1.3.2.min','jquery.maskedinput-1.1.1','jquery.innerfade','js','cliente','ajaxtabs','global','internas','swfobject','busca');
	public $data = array('local'=>"../../../",'logomarca'=>'/application/images/logo.jpg','empresa'=>'Equatorial Im�veis','cnpj'=>'99.999.999/0001-99','fones'=>'(85) 3244 7464','endereco'=>'Rua Dr. Jos� Louren�o, 870, Sala 105, Meireles.','sessaoCli'=>'EquaCli');
	public $configMail = array('protocol' => '0',
    'smtp_host' => '0',
    'smtp_port' => 0,
    'smtp_user' => '0',
    'smtp_pass' => '0',
    );
	public $description = '';
	public $keys = array();
	
	function Cliente()
	{
		parent::Controller();
		if(!isset($_SESSION['idioma'])){
			session_register('idioma');
			$_SESSION['idioma'] = 1;
		}
		if(isset($_GET['idioma']) && is_numeric($_GET['idioma'])){
			switch($_GET['idioma']){
				case 2:
					$_SESSION['idioma'] = 2;
					break;
				case 3;
					$_SESSION['idioma'] = 3;
					break;
				default:
					$_SESSION['idioma'] = 1;
			}
		}
		if(!isset($_SESSION[''.$this->data['sessaoCli']])){
			session_register(''.$this->data['sessaoCli']);
		}
		$logado = $_SESSION[''.$this->data['sessaoCli']];
		$this->load->model('enquete');
		$this->data['pergunta'] = $this->enquete->getAsk($logado);
		$this->data['resp']="";
		if($this->data['pergunta']){
			$this->data['resp'] = $this->enquete->getResp($this->data['pergunta']["id"]);
		}
		if(!isset($_SESSION['origem'])){
			session_register('origem');
		}
		$_SESSION['origem']?$this->data['origem'] = $_SESSION['origem']:$this->data['origem'] = $this->data["local"];
		//conta acesso ao site
		$this->load->model('tab_acs');
		$this->tab_acs->insertAcssite(session_id());
		$this->load->helper('url');
		$this->data['local'] = site_url();
		//pega a tradu��o para a p�gina
		$this->load->model('utilidades');
		$this->data['tradutor'] = $this->utilidades->Tradutor($_SESSION['idioma'],$this->data['local']);
		//pega os imoveis para obama
		$this->load->model('imovelvenda');
		$this->load->model('lancamento');
		$imoveis = $this->imovelvenda->getDestaques(500);
		$lancs   = $this->lancamento->getDestaques(500);
		$banner ="";
		if(is_array($lancs) && is_array($imoveis)){
			$banner = array_merge($lancs,$imoveis);
		}else if(is_array($imoveis)){
			$banner = $imoveis;
		}else if(is_array($lancs)){
			$banner = $lancs;
		}
		$imoveis = "";
		for($i=0;$i<count($banner) && $i<6;$i++){
			if($banner[$i]['imo_isn']>0){
				$imoveis[$i]['link'] = $this->data['local'].'index.php/imoveis/detalhes/venda/'.$banner[$i]['tim_des'].'/'.$banner[$i]['imo_isn'];
				$imoveis[$i]['texto'] = $banner[$i]['imo_des_est'];
				$imoveis[$i]['foto'] = $this->data['local'].$banner[$i]['foto'];
				$imoveis[$i]['tipo'] = $banner[$i]['tim_des'];
			}else {
				$imoveis[$i]['link'] = $this->data['local'].'index.php/imoveis/detalhes/empreendimento/'.$banner[$i]['tipo'].'/'.$banner[$i]['emp_isn'];
				$imoveis[$i]['texto'] = $banner[$i]['emp_des_txt'];
				$imoveis[$i]['foto'] = $this->data['local'].$banner[$i]['foto'];
				$imoveis[$i]['tipo'] = $banner[$i]['tipo'];
			}
		}
		$_SESSION["banners"] = $imoveis;
	}
	
	function index()
	{
		$this->load->helper('url');
		redirect('','refresh');
	}
	
	function logar(){
		$this->load->view('cliente/areaRes',$this->data);	
	}
	
	function Arearestrita(){
		$this->load->library('form_validation');
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		$this->load->model('usuario');
		if(isset($_POST["login"])){
			$this->data['usuario'] = $this->usuario->validaUsuario($this->input->post('login'),$this->input->post('senha_usu'));
			$_SESSION[''.$this->data['sessaoCli']] = $this->data['usuario'];
		}else {
			$this->data['usuario'] = $_SESSION[''.$this->data['sessaoCli']];
		}
		$this->data['dadosRec']="";
		$this->data['pcn']="";
		$this->data['pessoa']="";
		$this->data['dadosIRF']="";
		$this->data['resumoarr']="";
		if($this->data['usuario']){
			$this->load->model('recibo');
			$this->data['dadosRec'] = $this->recibo->carregarDados($this->data['usuario']['id']);
			$this->load->model('pc');
			$this->data['pcn'] = $this->pc->carregaD($this->data['usuario']['id']);
			$this->load->model('irf');
			$this->data['pessoa'] = $this->irf->carregarDadosPessoais($this->data['usuario']['id']);
			$this->data['dadosIRF'] = $this->irf->carregarDados($this->data['usuario']['id']);
			$this->load->model('resumocontrato');
			$this->data['resumoarr'] = $this->resumocontrato->carregaDados($this->data['usuario']['id']);
		}
		if($this->uri->segment("3") && $this->uri->segment("3")=="deslogar"){
			$_SESSION[''.$this->data['sessaoCli']] = "";
			header("Location: ".site_url());
		}else {			
			$this->load->view('cliente/exibepcrec',$this->data);
		}
	}
	
	function exibepc($numpc){
		$this->title = "Presta��o de contas";
		$this->layout = "popup";
		$this->css = array('popup');
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->data['lpc'] = "";
		$this->data['ipc'] ="";
		$this->data['pc'] = "";
		if($_SESSION[''.$this->data['sessaoCli']]){
			$this->load->model('pc');
			$this->data['pc'] = $this->pc->carregaDados($numpc);
			if($this->data['pc']){
				$this->data['lpc'] = $this->pc->carregaLancamentos($this->data['pc']["pco_id"]);
				$this->data['ipc'] = $this->pc->carregaImoveis($this->data['pc']["pco_id"]);
			}
		}
		$this->data['data'] = date("d-m-Y H:i:s");
		$this->load->view('cliente/exibepc',$this->data);
	}
	
	function resumocontrato($pes){
		$this->title = "Resumo do contrato";
		$this->layout = "popup";
		$this->css = array('popup');
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->data['data'] = date("d-m-Y H:i:s");
		if($_SESSION[''.$this->data['sessaoCli']]){
			$this->load->model('resumocontrato');
			$this->data['rc'] = $this->resumocontrato->carregaDados($pes);
			$this->data['usuario'] = $_SESSION[''.$this->data['sessaoCli']];
		}else {
			$this->data['rc'] ="";
		}
		$this->load->view('cliente/exiberc',$this->data);
	}
	
	function extrato($pes){
		$this->title = "Extrato de imposto de renda";
		$this->layout = "popup";
		$this->css = array('popup');
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->data['data'] = date("d-m-Y H:i:s");
		$this->data['dadosPes']="";
		$this->data['dadosIrf']="";
		$this->data['extratoImo']="";
		$this->data['lancamentos']="";
		$this->data['resumoImo']="";
		$this->data['resumoGeral']="";
		if($_SESSION[''.$this->data['sessaoCli']]){
			$this->load->model('irf');
			$this->data['dadosPes'] = $this->irf->carregarDadosPessoais($pes);
			$this->data['dadosIrf'] = $this->irf->carregarDados($pes);
			if($this->data['dadosIrf']){
				$this->data['extratoImo'] = $this->irf->carregarImoExt($this->data['dadosIrf']["irf_isn"]);
				$this->data['resumoGeral'] = $this->irf->carregarResumoGeral($this->data['dadosIrf']["irf_isn"]);	
			}
			is_array($this->data['extratoImo'])?$quant=count($this->data['extratoImo']):$quant=0;
			for($i=0;$i<$quant;$i++) {
				$this->data['lancamentos'][$i] = $this->irf->carregarLancamento($this->data['extratoImo'][$i]["ime_isn"]);
				$this->data['resumoImo'][$i] = $this->irf->carregarResumoImo($this->data['extratoImo'][$i]["ime_isn"]); 
			}
			$this->data['usuario'] = $_SESSION[''.$this->data['sessaoCli']];
		}
		$this->load->view('cliente/extrato',$this->data);
	}
	
	function recibo($pes,$pos,$rec){
		$this->title = "Recibo de cobran�a";
		$this->layout = "popup";
		$this->css = array('popup');
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
		$this->data['data'] = date("d-m-Y H:i:s");
		$this->data['dadosRec']="";
		$this->data['itensRec']="";
		$this->data['dadosBlt']="";
		$this->data['cod_bco']="";
		$this->data['dadosIbl']="";
		$this->data['logo_bco']="";
		$this->data['contador']="";
		$this->data['lin_dig']= "";
		$this->data['aviso']= "";
		if($_SESSION[''.$this->data['sessaoCli']]){
			$this->load->model('recibo');
			$this->data['dadosRec'] = $this->recibo->carregarDados($pes);
			$this->data['itensRec'] = $this->recibo->carregarItens($rec);
			$this->data['dadosBlt'] = $this->recibo->carregarDadosBlt($rec);
			if($this->data['dadosBlt']){
				  $this->data['cod_bco']  = substr($this->data['dadosBlt']["blt_cod_bco"],0,3);
				  $this->data['dadosIbl'] = $this->recibo->carregarDadosIbl($this->data['dadosBlt']["blt_isn"]);
				  switch($this->data['cod_bco']){
				  	case 409:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogUnibanco.jpg";
						break;
					case 001:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogBB.jpg";
						break;
					case 104:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogCaixa3.jpg";
						break;
					case 004:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogoBNB.jpg";
						break;
					case 237:
						$this->data['logo_bco'] = $this->data['local']."application/images/logo_bradesco.gif";
						break;
					case 356:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogBancoReal.jpg";
						break;
					case 422:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogoSafra.jpg";
					case 341:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogItau.jpg";
						break;
				  }
			}
			$this->data['contador'] = $pos;
			$this->data['usuario']  = $_SESSION[''.$this->data['sessaoCli']];
		}
		$this->data['width'] = '100%'; 
		$this->load->view('cliente/exibirRecibo',$this->data);
	}
	
	function segundaviaboleto($rec){
		if($rec){
			$rec=base64_decode($rec);
			$this->title = "Recibo de cobran�a";
			$this->layout = "popup";
			$this->css = array('popup');
			$this->load->helper('url');	   
			$this->data['local'] = site_url();
			$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
			$this->data['data'] = date("d-m-Y H:i:s");
			$this->data['dadosRec']="";
			$this->data['itensRec']="";
			$this->data['dadosBlt']="";
			$this->data['cod_bco']="";
			$this->data['dadosIbl']="";
			$this->data['logo_bco']="";
			$this->data['contador']="";
			$this->data['lin_dig']= "";
			$this->data['aviso']= "";			
			$this->load->model('recibo');
			$this->data['dadosRec'] = $this->recibo->carregarDados2($rec);
			$this->data['itensRec'] = $this->recibo->carregarItens($rec);
			$this->data['dadosBlt'] = $this->recibo->carregarDadosBlt($rec);
			if($this->data['dadosBlt']){
				  $this->data['cod_bco']  = substr($this->data['dadosBlt']["blt_cod_bco"],0,3);
				  $this->data['dadosIbl'] = $this->recibo->carregarDadosIbl($this->data['dadosBlt']["blt_isn"]);
				  switch($this->data['cod_bco']){
					case 409:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogUnibanco.jpg";
						break;
					case 001:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogBB.jpg";
						break;
					case 104:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogCaixa3.jpg";
						break;
					case 004:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogoBNB.jpg";
						break;
					case 237:
						$this->data['logo_bco'] = $this->data['local']."application/images/logo_bradesco.gif";
						break;
					case 356:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogBancoReal.jpg";
						break;
					case 422:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogoSafra.jpg";
					case 341:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogItau.jpg";
						break;
				  }
			}
			$this->data['contador'] = 0;
			$this->data['width'] = '760';			
		}
		$this->load->view('cliente/exibirRecibo',$this->data);
	}
	
	function descadastrarnews(){
		$cliente = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
		$this->load->model('tab_email');
		$cliente = $this->tab_email->buscarEmail(base64_decode($cliente));
		$this->data['msg'] = 'Seu email n�o se encontra cadastrado em nosso banco de dados.';
		if($cliente){
			$this->tab_email->excluirItens($cliente["id"]);
			$this->data['msg'] = 'Seu cadastro foiexclu�do de nosso banco de dados, Se desejar voltar a receber nossas not�cias, Favor recedastre seu email em nossa Newsletter.';
		}
		$this->load->view('cliente/descNews',$this->data);
		
	}
	
}
/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>