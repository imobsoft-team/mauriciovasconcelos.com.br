<?php
require_once('conexao.php');
  class Usuario extends Conexao{
     var $id = null;
	 var $nome = null;
	 
	 function Usuario() { 
	 	parent::Conexao();
	 }
	 
	 function setId($num) {
	    $this->id = $num;    
	 } 
	 function getId() {
	    return $this->id;
	 }

	 function setNome($nome) {
	    $this->nome = $nome;    
	 } 
	 function getNome() {
	    return $this->nome;
	 }
	function validaUsuario($num,$senha) {
       $sql = "SELECT * FROM tab_pes WHERE PES_NUM_CPF_CGC ='$num' AND PES_DES_SEN = '$senha'";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") {
		   return false;
	   }
	   $usu  =  "";
	   while(@odbc_fetch_row($res)) {
			$usu['id']   = odbc_result($res,"PES_ISN"); 
			$usu['nome'] = odbc_result($res,"PES_NOM");
	   } 
	   odbc_close($conn);	
	   return $usu;
	 }
     function cadastrarNews($nome,$email,$pes_tipo) {
	     $data = date("d-m-Y H:i:s");	     
		 $sql2 = "SELECT NWS_ISN FROM tab_news WHERE NWS_DES_MAIL='$email'";
		 $res2 = $this->db->query($sql2);
	     if($res2->num_rows() ==0){
			  $this->db->set('NWS_DES_MAIL', $email);
			  $this->db->set('NWS_NOM', $nome);
			  $this->db->set('NWS_DAT', $data);
			  $this->db->set('NWS_STA', 1);
			  /*$this->db->set('NWS_GRUPO', $pes_tipo);*/
			  $this->db->set('NWS_GRUPO', 3);
			  $this->db->insert('tab_news');
			  if($this->db->insert_id() > 0){			 
				 $msg = "OPERA��O REALIZADA COM SUCESSO!";
				 return $msg;
			  }else {
			  	 $msg = "Erro no cadastro!";
				 return $msg;
			  }
		 }else {
               $msg = "USUARIO JA CADASTRADO";			  
			   return $msg;
		 }
	  }
	  function cadastrarContato($tipo_cli,$nome,$email,$fone,$msgs,$depto) {
		 $data = date("Y-m-d H:i:s");	     
		 $sql = "INSERT INTO tab_cnt (CNT_DAT,CNT_NOM,CNT_MAIL,CNT_FONE,CNT_MSG,CNT_DEPTO,CNT_STA)
		         VALUES('$data','$nome','$email','$fone','$msgs',$depto,$tipo_cli)";
	     $res = $this->db->query($sql);
	     if($this->db->affected_rows()>0){ 
		     $msg = "Operacao relizada com sucesso";			  
		 }else {
		  	  $msg = "Operacao n�o relizada";
		 }
		 return $msg;
	  }
	  function consultarTMSG() {
	     $idm = $_SESSION["idiomaHOU"];
		 if($idm == 1) {
		    $descTp = "TPM_DES";
		 }
		 if($idm == 2) {
		    $descTp = "TPM_DES_ING";
		 }
	     $tipo = array();
	     
		  $sql = "SELECT * FROM tab_tpm ORDER BY $descTp";
		  
		  $res = @mysql_query($sql);
		  if(@mysql_num_rows($res)>0){
			  $i = 0;
			  while($array = mysql_fetch_array($res)) {
				 $tipo[$i][tpm_isn] = $array["TPM_ISN"]; 
				 $tipo[$i][tpm_des] = $array[$descTp];
				 $tipo[$i][tpm_tem_atd] = $array["TPM_TEM_ATD"];
				 $i++;
			  } 
		  }
		  
		  return $tipo;
	  }

	  function consultarTPSOL() {
	     $idm = $_SESSION["idiomaHOU"];
		 if($idm == 1) {
		    $descSol = "TPS_DES";
		 }
		 if($idm == 2) {
		    $descSol = "TPS_DES_ING";
		 }
	     $tipo = array();
	     
		  $sql = "SELECT * FROM tab_tps ORDER BY $descSol";
		  
		  $res = @mysql_query($sql);
		  if(@mysql_num_rows($res)>0){
			  $i = 0;
			  while($array = mysql_fetch_array($res)) {
				 $tipo[$i][tps_isn] = $array["TPS_ISN"]; 
				 $tipo[$i][tps_des] = $array[$descSol];
				 $i++;
			  } 
		  }
		  return $tipo;
	  }

	  function cadastrarContatoOuvi($tipo_msg,$nome,$email,$fone,$atend,$msg,$depto) {
	     
		  $data = date("d-m-Y H:i:s");
		  $sql = "INSERT INTO tab_ouv (TPM_ISN,OUV_DAT,OUV_NOM,OUV_MAIL,OUV_FONE,OUV_NOM_ATD,OUV_MSG,OUV_STA,OUV_DEPTO)
		         VALUES ($tipo_msg,'$data','$nome','$email','$fone','$atend','$msg',1,$depto)";
	     	
	     $res = @mysql_query($sql);
	     if(@mysql_affected_rows()>0){		 
		      $sql2 = "SELECT TPM_DES FROM tab_tpm WHERE TPM_ISN = $tipo_msg";			  
			  $res2  = @mysql_query($sql2);
			  if(@mysql_num_rows($res2)>0){
				  while($array = mysql_fetch_array($res2)) {
					 $tmsg = $array["TPM_DES"];
				  }
			  }
			  return $tmsg;
		  }else {
		  		return false;
		  }
	  } 	  

	  function cadastrarSolicitacao($tipo_sol,$nome,$email,$fone,$msg,$depto) {	     
		  $data = date("d-m-Y H:i:s");
		  $sql = "INSERT INTO tab_sol (TPS_ISN,SOL_NOM,SOL_MAIL,SOL_FONE,SOL_MSG,SOL_STA,SOL_DAT,SOL_DEPTO)
		         VALUES ($tipo_sol,'$nome','$email','$fone','$msg',1,'$data',$depto)";
	     	
	     $res = @mysql_query($sql);
	     if(@mysql_affected_rows()>0){
		     $sql2 = "SELECT TPS_DES FROM tab_tps WHERE TPS_ISN = $tipo_sol";			  
			  $res2  = @mysql_query($sql2);
			  if(@mysql_num_rows($res2)>0){
				  while($array = mysql_fetch_array($res2)) {
					 $tmsg = $array["TPS_DES"];
				  }
			  }
			  return $tmsg;
		  }else {
		  	return false;
		  }
	  } 	  
  }
?>