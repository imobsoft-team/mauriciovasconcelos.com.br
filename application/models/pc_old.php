<?php
require_once('conexao.php');
 class PC extends Conexao{
    function PC(){
		parent::Conexao();
	}
	
	function carregaD($numpc) {
	   $pcexp = "";
	   $i = 0;
	   $sql = "SELECT PCO_ISN, PCO_NUM, PCO_DAT_VCT FROM TAB_PCO WHERE PES_ISN =".$numpc;
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") {
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
            $pcexp[$i]["pco_id"]           = odbc_result($res,"PCO_ISN"); 	   
			$pcexp[$i]["pco_num"]          = odbc_result($res,"PCO_NUM"); 
			$pcexp[$i]["pco_dat_vct"]      = odbc_result($res,"PCO_DAT_VCT");
            $i++;
	   }
	   odbc_close($conn);		
	   return $pcexp;
	}

	function carregaDados($num) {
	   $pcexp = "";
	   $sql = "SELECT * FROM TAB_PCO WHERE PCO_ISN =".$num;
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") {
		  return false;
	   }
	   while(@odbc_fetch_row($res)) {
            $pcexp["pco_id"]           = odbc_result($res,"PCO_ISN"); 	   
			$pcexp["pco_num"]          = odbc_result($res,"PCO_NUM"); 
			$pcexp["pco_num_cpf_cgc"]  = odbc_result($res,"PCO_NUM_CPF_CGC_PROP"); 
			$pcexp["pco_dat_vct"]      = odbc_result($res,"PCO_DAT_VCT");
			$pcexp["pes_id"]           = odbc_result($res,"PES_ISN"); 
			$pcexp["pco_nom_prop"]     = odbc_result($res,"PCO_NOM_PROP");
			$pcexp["pco_dat_emi"]      = odbc_result($res,"PCO_DAT_EMI"); 
			$pcexp["pco_des_end"]      = odbc_result($res,"PCO_DES_END"); 
			$pcexp["pco_des_bai"]      = odbc_result($res,"PCO_DES_BAI"); 
			$pcexp["pco_des_cid"]      = odbc_result($res,"PCO_DES_CID"); 
			$pcexp["pco_des_uf"]       = odbc_result($res,"PCO_DES_UF"); 
			$pcexp["pco_des_cep"]      = odbc_result($res,"PCO_DES_CEP"); 
			$pcexp["pco_nom_fav"]      = odbc_result($res,"PCO_NOM_FAV"); 
			$pcexp["pco_val_bas_com"]  = odbc_result($res,"PCO_VAL_BAS_COM"); 
			$pcexp["pco_val_alu_cred"] = odbc_result($res,"PCO_VAL_ALU_CRED"); 
			$pcexp["pco_val_irrf"]     = odbc_result($res,"PCO_VAL_IRRF"); 
			$pcexp["pco_val_com"]      = odbc_result($res,"PCO_VAL_COM"); 
			$pcexp["pco_val_deb"]      = odbc_result($res,"PCO_VAL_DEB"); 
			$pcexp["pco_val_cred"]     = odbc_result($res,"PCO_VAL_CRED"); 
			$pcexp["pco_val_cpmf"]     = odbc_result($res,"PCO_VAL_CPMF"); 
			$pcexp["pco_val_doc"]      = odbc_result($res,"PCO_VAL_DOC"); 
			$pcexp["pco_val_tax_bca"]  = odbc_result($res,"PCO_VAL_TAX_BCA"); 
			$pcexp["pco_val_brt"]      = odbc_result($res,"PCO_VAL_BRT"); 
			$pcexp["pco_val_pc"]       = odbc_result($res,"PCO_VAL_PC"); 
			$pcexp["pco_des_tip_pag"]  = odbc_result($res,"PCO_DES_TIP_PAG"); 
			$pcexp["pco_des_bco"]      = odbc_result($res,"PCO_DES_BCO"); 
			$pcexp["pco_des_age"]      = odbc_result($res,"PCO_DES_AGE"); 
			$pcexp["pco_des_cta"]      = odbc_result($res,"PCO_DES_CTA"); 

	   }
	   odbc_close($conn);		
	   return $pcexp;
	}
	
	function carregaLancamentos($isn) {
	   $lc = "";
	   $i = 0;
	   $sql = "SELECT * FROM TAB_LPC WHERE PCO_ISN =".$isn;
   	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") {
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
            $lc[$i]["lpc_id"]              = odbc_result($res,"LPC_ISN"); 	   
			$lc[$i]["pco_id"]              = odbc_result($res,"PCO_ISN"); 
			$lc[$i]["lpc_num_seq"]         = odbc_result($res,"LPC_NUM_SEQ"); 
			$lc[$i]["lpc_des_ctr_loc"]     = odbc_result($res,"LPC_DES_CTR_LOC");
			$lc[$i]["lpc_des_inq_out"]     = odbc_result($res,"LPC_DES_INQ_OUT"); 
			$lc[$i]["lpc_des_mes_ano"]     = odbc_result($res,"LPC_DES_MES_ANO");
			$lc[$i]["lpc_val_deb"]         = odbc_result($res,"LPC_VAL_DEB"); 
			$lc[$i]["lpc_val_cre"]         = odbc_result($res,"LPC_VAL_CRE"); 
			$lc[$i]["lpc_des_tip_com"]     = odbc_result($res,"LPC_DES_TIP_COM"); 
			$lc[$i]["lpc_val_sal"]         = odbc_result($res,"LPC_VAL_SAL"); 
			$lc[$i]["lpc_des_tip_deb_cre"] = odbc_result($res,"LPC_DES_TIP_DEB_CRE"); 
			$lc[$i]["lpc_des_tip_pag"]     = odbc_result($res,"LPC_DES_TIP_PAG"); 
			$lc[$i]["lpc_cod_imo"]         = odbc_result($res,"LPC_COD_IMO");
			$i++;
	   } 
	   odbc_close($conn);	
	   return $lc;
	}
	
	function carregaImoveis($isn) {	
	   $imo = "";
	   $i = 0;
	   $sql = "SELECT * FROM TAB_IPC WHERE PCO_ISN =".$isn;
  	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") {
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
         	$imo[$i]["ipc_id"]           = odbc_result($res,"IPC_ISN"); 	   
			$imo[$i]["pco_id"]           = odbc_result($res,"PCO_ISN"); 
			$imo[$i]["ipc_num_seq"]      = odbc_result($res,"IPC_NUM_SEQ"); 
         	$imo[$i]["ipc_cod_imo"]      = odbc_result($res,"IPC_COD_IMO"); 			
			$imo[$i]["ipc_des_end"]      = odbc_result($res,"IPC_DES_END");
			$imo[$i]["ipc_dat_ent"]      = odbc_result($res,"IPC_DAT_ENT"); 
			$imo[$i]["ipc_dat_reaj"]     = odbc_result($res,"IPC_DAT_REAJ");
			$imo[$i]["ipc_dat_fim"]      = odbc_result($res,"IPC_DAT_FIM"); 
			$imo[$i]["ipc_val_alu"]      = odbc_result($res,"IPC_VAL_ALU"); 
			$imo[$i]["ipc_des_ult_pgto"] = odbc_result($res,"IPC_DES_ULT_PGTO"); 
			$imo[$i]["ipc_des_tip"]      = odbc_result($res,"IPC_DES_TIP"); 
			$imo[$i]["ipc_des_sit"]      = odbc_result($res,"IPC_DES_SIT"); 
			$i++;
	   } 
	   odbc_close($conn);	
	   return $imo;
	}
	 function carregarDadosPC($nome,$num) {
	   $con = new Conexao(); 
	   $dadosPCO = array();
	   $i = 0;
	   $sql = "SELECT PCO_ISN,PCO_NOM_PROP,PCO_NUM,PCO_DAT_VCT,PCO_VAL_PC FROM TAB_PCO WHERE 1=1 "; 
		if($nome) {
		   $sql = $sql."AND PCO_NOM_PROP LIKE '$nome%' ";
		}
		if($num) {
		   $sql = $sql."AND PCO_NUM = '$num' ";
		}
		$sql = $sql."ORDER BY PCO_NOM_PROP";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
	   }
	   while(odbc_fetch_row($res)) {
			$dadosPCO[$i]["pco_isn"]                = odbc_result($res,"PCO_ISN"); 	   
			$dadosPCO[$i]["pco_nom_prop"]           = odbc_result($res,"PCO_NOM_PROP"); 	   
			$dadosPCO[$i]["pco_num"]                = odbc_result($res,"PCO_NUM"); 	   
			$dadosPCO[$i]["pco_dat_vct"]            = odbc_result($res,"PCO_DAT_VCT"); 	   
			$dadosPCO[$i]["pco_val_pc"]            = odbc_result($res,"PCO_VAL_PC"); 	   
			$i++;   
	   }
	   odbc_close($conn);		
	   return $dadosPCO; 	   
	}
 }
?>