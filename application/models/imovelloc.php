<?php
require_once('conexao.php');
 class ImovelLoc extends Conexao {
     function ImovelLoc(){
	 	parent::Conexao();
	 }
	 function getDestaques($tam=130){
		$idm = $_SESSION['idioma'];	   
		if($idm == 2) {
		   $textDestq = "IMO_DES_TXT_DEST_ING";
		}else if($idm == 3){
		   $textDestq = "IMO_DES_TXT_DEST_ESP";
		}else {
		   $textDestq = "IMO_DES_TXT_DEST";
		}
		$sql = "SELECT TOP 16 IMO.IMO_ISN,IMO_COD_ESP,IMO.IMO_VAL_ALU,IMO.IMO_VAL_ARE,IMO.IMO_DES_TXT_DEST, IMO.IMO_DES_BAI, IMO.IMO_DES_END, TIM.TIM_DES, IMI.IMI_NOM FROM TAB_IMO IMO, TAB_TIM TIM, TAB_IMI IMI WHERE IMO.IMO_TIP_DEST = 1 AND IMO.IMO_TIP_DEST_PRIM_PAG = 1 AND TIM.TIM_ISN = IMO.TIM_ISN AND IMI.IMO_ISN = IMO.IMO_ISN AND IMI.IMI_TIP_DEST = 1 ORDER BY RND(IMO.IMO_ISN)";
	    $i = 0;
	    $codigo = "";
	    $conn=$this->connDb();	
	    $res = @odbc_exec($conn,$sql);
	    if (@odbc_error($res)) {
		   return false;
	    }
	    while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]        = odbc_result($res,"IMO_ISN"); 
			$codigo[$i]["cod_esp"]            = odbc_result($res,"IMO_COD_ESP"); 	   
			$codigo[$i]["imo_des_txt_dest"]    = odbc_result($res,"IMO_DES_TXT_DEST");
			$codigo[$i]["tim_des"]        = odbc_result($res,"TIM_DES");
			$codigo[$i]["imo_des_bai"]    = odbc_result($res,"IMO_DES_BAI");	
			$codigo[$i]["imo_des_end"]    = odbc_result($res,"IMO_DES_END"); 
			$codigo[$i]["imi_nom"]        = odbc_result($res,"IMI_NOM");  
			$codigo[$i]["imo_val_alu"]    = odbc_result($res,"IMO_VAL_ALU");
            $codigo[$i]["imo_val_are"]    = odbc_result($res,"IMO_VAL_ARE");
		 	$i++; 
	   }
	   is_array($codigo)?$quant=count($codigo):$quant=0;
	   for($x=0;$x<$quant;$x++){
           $codigo[$x]["caracs"] = $this->consultarCaracteristicaPaiDestaque($codigo[$x]["imo_isn"]);
           $codigo[$x]["foto"] = $this->buscarFoto($codigo[$x]["imo_isn"],$this->CaminhoFoto(),$tam);
	   }
	   @odbc_close($conn);		
	   return $codigo;		
	
	  }

     function getDestaquesTipo($tam=130, $tim_isn, $finalidade){
         $idm = $_SESSION['idioma'];
         if($idm == 2) {
             $textDestq = "IMO_DES_TXT_DEST_ING";
         }else if($idm == 3){
             $textDestq = "IMO_DES_TXT_DEST_ESP";
         }else {
             $textDestq = "IMO_DES_TXT_DEST";
         }

         if($finalidade == 'residencial') {
             $finalidade = 1;
         } else {
             $finalidade = 2;
         }

         if ($tim_isn == 0) {
			 $sql = "SELECT TOP 16 IMO.IMO_ISN,IMO_COD_ESP,IMO.IMO_VAL_ALU,IMO.IMO_VAL_ARE,IMO.IMO_DES_TXT_DEST, IMO.IMO_DES_BAI, IMO.IMO_DES_LOC, IMO.IMO_DES_END, TIM.TIM_DES, IMI.IMI_NOM FROM TAB_IMO IMO, TAB_TIM TIM, TAB_IMI IMI WHERE IMO.IMO_TIP_DEST = 1 AND IMO.IMO_TIP_DEST_PRIM_PAG = 1 AND TIM.TIM_ISN = IMO.TIM_ISN AND IMI.IMO_ISN = IMO.IMO_ISN AND IMI.IMI_TIP_DEST = 1 AND IMO_FINALIDADE = $finalidade ORDER BY RND(IMO.IMO_ISN)";
         } else {
             $sql = "SELECT TOP 16 IMO.IMO_ISN,IMO_COD_ESP,IMO.IMO_VAL_ALU,IMO.IMO_VAL_ARE,IMO.IMO_DES_TXT_DEST, IMO.IMO_DES_BAI, IMO.IMO_DES_LOC, IMO.IMO_DES_END, TIM.TIM_DES, IMI.IMI_NOM FROM TAB_IMO IMO, TAB_TIM TIM, TAB_IMI IMI WHERE IMO.IMO_TIP_DEST = 1 AND IMO.IMO_TIP_DEST_PRIM_PAG = 1 AND TIM.TIM_ISN = IMO.TIM_ISN AND IMI.IMO_ISN = IMO.IMO_ISN AND IMI.IMI_TIP_DEST = 1 AND IMO.TIM_ISN = $tim_isn AND IMO_FINALIDADE = $finalidade ORDER BY RND(IMO.IMO_ISN)";
         }


         $i = 0;
         $codigo = "";
         $conn=$this->connDb();
         $res = @odbc_exec($conn,$sql);
         if (@odbc_error($res)) {
             return false;
         }
         while(@odbc_fetch_row($res)) {
             $codigo[$i]["imo_isn"]        = odbc_result($res,"IMO_ISN");
             $codigo[$i]["cod_esp"]        = odbc_result($res,"IMO_COD_ESP");
             $codigo[$i]["imo_des_est"]    = odbc_result($res,"IMO_DES_TXT_DEST");
             $codigo[$i]["tim_des"]        = odbc_result($res,"TIM_DES");
             $codigo[$i]["imo_des_bai"]    = odbc_result($res,"IMO_DES_BAI");
             $codigo[$i]["imo_des_loc"]    = odbc_result($res,"IMO_DES_LOC");
             $codigo[$i]["imo_des_end"]    = odbc_result($res,"IMO_DES_END");
             $codigo[$i]["imi_nom"]        = odbc_result($res,"IMI_NOM");
             $codigo[$i]["imo_val_alu"]    = odbc_result($res,"IMO_VAL_ALU");
             $codigo[$i]["imo_val_are"]    = odbc_result($res,"IMO_VAL_ARE");
             $i++;
         }
         is_array($codigo)?$quant=count($codigo):$quant=0;
         for($x=0;$x<$quant;$x++){
             $codigo[$x]["caracs"] = $this->consultarCaracteristicaPaiDestaque($codigo[$x]["imo_isn"]);
             $codigo[$x]["foto"] = $this->buscarFoto($codigo[$x]["imo_isn"],$this->CaminhoFoto(),$tam);
         }
         @odbc_close($conn);
         return $codigo;

     }

     function getDestaquesEspeciais($tam=600){
         $idm = $_SESSION['idioma'];
         if($idm == 2) {
             $textDestq = "IMO_DES_TXT_DEST_ING";
         }else if($idm == 3){
             $textDestq = "IMO_DES_TXT_DEST_ESP";
         }else {
             $textDestq = "IMO_DES_TXT_DEST";
         }
         $sql = "SELECT TOP 6 IMO.IMO_ISN,IMO_COD_ESP,IMO.IMO_VAL_ALU,IMO.IMO_VAL_ARE,IMO.IMO_DES_TXT_DEST, IMO.IMO_DES_BAI, IMO.IMO_DES_LOC, IMO.IMO_DES_END, TIM.TIM_DES, IMI.IMI_NOM FROM TAB_IMO IMO, TAB_TIM TIM, TAB_IMI IMI WHERE IMO.IMO_TIP_DEST = 1 AND IMO.IMO_TIP_DEST_PRIM_PAG = 1 AND TIM.TIM_ISN = IMO.TIM_ISN AND IMI.IMO_ISN = IMO.IMO_ISN AND IMI.IMI_TIP_DEST = 1 AND DTE_ISN IS NOT NULL ORDER BY RND(IMO.IMO_ISN)";
         $i = 0;
         $codigo = "";
         $conn=$this->connDb();
         $res = @odbc_exec($conn,$sql);
         if (@odbc_error($res)) {
             return false;
         }
         while(@odbc_fetch_row($res)) {
             $codigo[$i]["imo_isn"]        = odbc_result($res,"IMO_ISN");
             $codigo[$i]["cod_esp"]            = odbc_result($res,"IMO_COD_ESP");
             $codigo[$i]["imo_des_est"]    = odbc_result($res,"IMO_DES_TXT_DEST");
             $codigo[$i]["tim_des"]        = odbc_result($res,"TIM_DES");
             $codigo[$i]["imo_des_bai"]    = odbc_result($res,"IMO_DES_BAI");
             $codigo[$i]["imo_des_loc"]    = odbc_result($res,"IMO_DES_LOC");
             $codigo[$i]["imo_des_end"]    = odbc_result($res,"IMO_DES_END");
             $codigo[$i]["imi_nom"]        = odbc_result($res,"IMI_NOM");
             $codigo[$i]["imo_val_alu"]    = odbc_result($res,"IMO_VAL_ALU");
             $codigo[$i]["imo_val_are"]    = odbc_result($res,"IMO_VAL_ARE");
             $i++;
         }
         is_array($codigo)?$quant=count($codigo):$quant=0;
         for($x=0;$x<$quant;$x++){
             $codigo[$x]["caracs"] = $this->consultarCaracteristicaPaiDestaque($codigo[$x]["imo_isn"]);
             $codigo[$x]["foto"] = $this->buscarFoto($codigo[$x]["imo_isn"],$this->CaminhoFoto(),$tam);
         }
         @odbc_close($conn);

         return $codigo;

     }

     function consultarTipos($finalidade) {
         $i = 0;
         $tipoesp = array();
         //Query para trazer somente os Tipos de imóvel que tem destaques ativos.
         $sql = "SELECT DISTINCT TIM_ISN, TIM_DES FROM TAB_TIM WHERE TIM_ISN IN (SELECT TIM_ISN FROM TAB_IMO WHERE IMO_TIP_DEST = 1 AND IMO_TIP_DEST_PRIM_PAG = 1 AND IMO_FINALIDADE = $finalidade)";
         $conn=$this->connDb();
         $res = @odbc_exec($conn,$sql);
         if (@odbc_error($res)) {
             return false;
         }
         while(@odbc_fetch_row($res)) {
             $tipoesp[$i]["tim_id"]           = odbc_result($res,"TIM_ISN");
             $tipoesp[$i]["tim_des"]          = utf8_encode(odbc_result($res,"TIM_DES"));
             $i++;
         }
         @odbc_close($conn);
         return $tipoesp;

     }
	  
	  function consultarImoveisRegiao($bairro,$tam,$codigo,$local) {
	
		$i = 0;
		$imoveisPrx = "";
		$sql = "SELECT * FROM TAB_IMO IMO,TAB_TIM TIM
		WHERE TIM.TIM_ISN = IMO.TIM_ISN AND NOT IMO.IMO_ISN = $codigo AND IMO_DES_BAI = '$bairro'
		ORDER BY IMO_ISN";
	
		$conn=$this->connDb();
		$res = @odbc_exec($conn,$sql);
		if (@odbc_error($res)) {
		return false;
		}
		while(@odbc_fetch_row($res)) {
		$imoveisPrx[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN");
		$imoveisPrx[$i]["tim_des"]           = odbc_result($res,"TIM_DES");
		$imoveisPrx[$i]["cod_esp"]           = odbc_result($res,"IMO_COD_ESP");
		$imoveisPrx[$i]["imo_des_bai"]       = odbc_result($res,"IMO_DES_BAI");
		$imoveisPrx[$i]["imo_des_loc"]       = odbc_result($res,"IMO_DES_LOC");
		$imoveisPrx[$i]["imo_des_end"]       = odbc_result($res,"IMO_DES_END");
		$imoveisPrx[$i]["imo_val_alu"]       = odbc_result($res,"IMO_VAL_ALU");
	
		$i++;
	
	
		}
		is_array($imoveisPrx)?$quant=count($imoveisPrx):$quant=0;
		for($x=0;$x<$quant;$x++){
		$imoveisPrx[$x]["caracs"] = $this->consultarCaracteristicaPaiDestaque($imoveisPrx[$x]["imo_isn"]);
		$imoveisPrx[$x]["foto"] = $this->buscarFoto2($imoveisPrx[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
		$imoveisPrx[$x]["fotos"] = $this->carregarFotos($imoveisPrx[$x]["imo_isn"],$this->CaminhoFoto(),$tam*2,$local);
		}
				@odbc_close($conn);
				//echo "<pre>"; print_r($imoveisPrx); echo "</pre>";
		return $imoveisPrx;
	}

     function consultarCaracteristicaPaiDestaque($codigo) {
         $idm = $_SESSION['idioma'];
         if($idm == 2) {
             $descCarDes = "CAR_DES_ING";
         }else if($idm == 3){
             $descCarDes = "CAR_DES_ESP";
         }else {
             $descCarDes = "CAR_DES";
         }
         $i = 0;
         $caracPai = array();
         $caracDestaque = array();
         $sql = "SELECT IMC.*, CAR.CAR_DES,CAR.CAR_TIP, CAR.CAR_DES_UNI FROM TAB_IMC IMC
              LEFT JOIN TAB_CAR CAR ON CAR.CAR_ISN = IMC.CAR_ISN
              WHERE IMC.IMO_ISN = $codigo AND
              CAR.CAR_ISN_PAI = 0
              ORDER BY CAR.CAR_DES";
         $conn=$this->connDb();
         $res = odbc_exec($conn,$sql);
         if (@odbc_error($res)) {
             return false;
         }
		 
		 $caracDestaque[0]["qtd_suites"] = 0;
		 
         while(@odbc_fetch_row($res)) {

             $caracPai[$i]["imc_qtd"]           = odbc_result($res,"IMC_QTD");

             if(empty($caracPai[$i]["car_car_des"])){
                 $caracPai[$i]["car_car_des"] = odbc_result($res,"CAR_DES");
             }
             if($caracPai[$i]["car_car_des"] == "QUARTO(S)"){
                 $caracDestaque[0]["qtd_quartos"] = $caracPai[$i]["imc_qtd"];
             }
             if($caracPai[$i]["car_car_des"] == "VAGAS ESTACIONAMENTO" || $caracPai[$i]["car_car_des"] == "GARAGEM" || $caracPai[$i]["car_car_des"] == "VAGAS GARAGEM" || $caracPai[$i]["car_car_des"] == "VAGAS"){
                 $caracDestaque[0]["qtd_vagas"] = $caracPai[$i]["imc_qtd"];
             }
			 
			$conversao = array('á' => 'a','à' => 'a','ã' => 'a','â' => 'a', 'é' => 'e',
			 'ê' => 'e', 'í' => 'i', 'ï'=>'i', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', "ö"=>"o",
			 'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'ñ'=>'n', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
			 'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ï'=>'I', "Ö"=>"O", 'Ó' => 'O',
			 'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'Ç' =>'C', 'N'=>'Ñ');
			$suites = strtr(utf8_encode($caracPai[$i]["car_car_des"]), $conversao);
			
             if($suites == "SUITES"){
                 $caracDestaque[0]["qtd_suites"] = $caracPai[$i]["imc_qtd"];
			 }
			 
			 if($suites == "BANHEIRO SOCIAL"){
				$caracDestaque[0]["qtd_banheiros"] = $caracPai[$i]["imc_qtd"];
			 }
             $i++;
         }
         @odbc_close($conn);
         return $caracDestaque;
     }
	  
	   function getDestaquesAc($tam=130){
		$idm = $_SESSION['idioma'];	   
		if($idm == 2) {
		   $textDestq = "IMO_DES_TXT_DEST_ING";
		}else if($idm == 3){
		   $textDestq = "IMO_DES_TXT_DEST_ESP";
		}else {
		   $textDestq = "IMO_DES_TXT_DEST";
		}
		$sql = "SELECT IMO.IMO_ISN,IMO_COD_ESP,IMO.IMO_VAL_ALU,IMO.IMO_DES_TXT_DEST, IMO.IMO_DES_BAI, IMO.IMO_DES_END, TIM.TIM_DES, IMI.IMI_NOM,DTE.DTE_DES FROM TAB_IMO IMO, TAB_TIM TIM, TAB_IMI IMI,TAB_DTE DTE WHERE IMO.IMO_TIP_DEST = 1 AND IMO.IMO_TIP_DEST_PRIM_PAG = 1 AND TIM.TIM_ISN = IMO.TIM_ISN AND IMI.IMO_ISN = IMO.IMO_ISN AND IMI.IMI_TIP_DEST = 1 AND IMO.DTE_ISN=DTE.DTE_ISN AND DTE_TIP_EXIB_PRIM_PAG = 1 ORDER BY IMO.IMO_ISN";
	    $i = 0;
	    $codigo = "";
	    $conn=$this->connDb();	
	    $res = @odbc_exec($conn,$sql);
	    if (@odbc_error($res)) {
		   return false;
	    }
	    while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]        = odbc_result($res,"IMO_ISN"); 
			$codigo[$i]["cod_esp"]        = odbc_result($res,"IMO_COD_ESP"); 	   
			$codigo[$i]["imo_des_est"]    = odbc_result($res,"IMO_DES_TXT_DEST"); 
			$codigo[$i]["tim_des"]        = odbc_result($res,"TIM_DES");
			$codigo[$i]["imo_des_bai"]    = odbc_result($res,"IMO_DES_BAI");	
			$codigo[$i]["imo_des_end"]    = odbc_result($res,"IMO_DES_END"); 
			$codigo[$i]["imi_nom"]        = odbc_result($res,"IMI_NOM");  
			$codigo[$i]["imo_val_alu"]    = odbc_result($res,"IMO_VAL_ALU");
			$codigo[$i]["dte_des"]        = odbc_result($res,"DTE_DES");
			$codigo[$i]["tip"]            =1;
						
		 	$i++; 
	   }
	   is_array($codigo)?$quant=count($codigo):$quant=0;
	   for($x=0;$x<$quant;$x++){		 	
			$codigo[$x]["foto"] = $this->buscarFotoAc($codigo[$x]["imo_isn"],$this->CaminhoFoto(),$tam);
	   }
	   @odbc_close($conn);		
	   return $codigo;		
	
	  }
	 
	  public function buscarFoto($imo_isn,$caminho,$tamanho=130){
		$sql = "SELECT *  FROM TAB_IMI WHERE IMO_ISN = $imo_isn AND IMI_TIP_DEST = 1";
		$conn2=$this->connDb();	
		$res2 = @odbc_exec($conn2,$sql);
		if (@odbc_error($res2)) { 
			return false;
		}		
		@odbc_fetch_row($res2);
		if(@file_exists($caminho.odbc_result($res2,"IMI_NOM")) && odbc_result($res2,"IMI_NOM")){
			$foto = 'application/helpers/inc_thumb.php?img=../../'.$caminho.odbc_result($res2,"IMI_NOM").'&size=1200'; 
		}		    
		if(empty($foto)){
			$foto = 'application/helpers/inc_thumb.php?img=../images/indisponivel.jpg&size=300';
		} 
		@odbc_close($conn2);		
		return $foto;
	 }
	 
	  public function buscarFotoAc($imo_isn,$caminho,$tamanho=130,$local=''){
		$sql = "SELECT *  FROM TAB_IMI WHERE IMO_ISN = $imo_isn AND IMI_TIP_DEST = 1";
		$con = new Conexao();
                $conn2=$con->connDb();	
		$res2 = @odbc_exec($conn2,$sql);
		if (@odbc_error($res2)) { 
			return false;
		}		
		@odbc_fetch_row($res2);
		if(@file_exists($caminho.odbc_result($res2,"IMI_NOM")) && odbc_result($res2,"IMI_NOM")){
			$foto = $local.'application/helpers/inc_thumb.php?img=../../'.$caminho.odbc_result($res2,"IMI_NOM").'&size=1200'; 
		}		    
		if(empty($foto)){
			$foto = $local.'application/helpers/inc_thumb.php?img=../images/indisponivel.jpg&size=1200';
		} 
		@odbc_close($conn2);		
		return $foto;
	}
	 
	 function consultarCidades() { 
		   $i = 0;
		   $cidade = array();
		   $sql = "SELECT DISTINCT IMO_DES_LOC FROM TAB_IMO ORDER BY IMO_DES_LOC";
		   $conn=$this->connDb();	
		   $res = @odbc_exec($conn,$sql);
		   if (@odbc_error($res)) { 
			   return false;
		   }
		   while(@odbc_fetch_row($res)) {
				$cidade[$i]["imo_des_loc"]           = odbc_result($res,"IMO_DES_LOC"); 	   
				$i++; 
		   }
		   @odbc_close($conn);		
		   return $cidade;
	 }
	 
	function consultarCidAlu(){
		$con = new Conexao();
		$conn=$con->connDb();
		
		$sql ="SELECT DISTINCT(IMO_DES_LOC) FROM TAB_IMO";
		
		$res = @odbc_exec($conn,$sql);
		
		if (@odbc_error($res)) { 
		   return false;
	   }
		
		$bai = array();
		$i=0; 
		while(@odbc_fetch_row($res)) {
			$imo[$i]["imo_des_loc"] = odbc_result($res,"IMO_DES_LOC"); 	   
			$i++;
	   }
	   @odbc_close($conn);		
	   return $imo;
	
	}


	 function consultarTipo() {
	   $i = 0;
	   $tipoesp = array();
	   $sql = "SELECT * FROM TAB_TIM ORDER BY TIM_DES";
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) {
		   return false;
	   } 
	   while(@odbc_fetch_row($res)) {
         $tipoesp[$i]["tim_id"]           = odbc_result($res,"TIM_ISN"); 	   
			$tipoesp[$i]["tim_des"]          = odbc_result($res,"TIM_DES"); 
            $i++; 
	   }
	   @odbc_close($conn);		
	   return $tipoesp;
	 
	 }
	 
	  function buscaTipo($isn) {
	   $tipoesp = "";
	   $sql = "SELECT * FROM TAB_TIM WHERE TIM_ISN = $isn";
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) {
		   return false;
	   } 
	   while(@odbc_fetch_row($res)) {           	   
			$tipoesp  = odbc_result($res,"TIM_DES"); 
	   }
	   @odbc_close($conn);		
	   return $tipoesp;	 
	 }

	 function consultarBairros() {
	   $num_arg = func_num_args();	
	   $i = 0;
	   $bairro = array();
	   if($num_arg == 1){
	   		$cid = func_get_arg(0);
			$sql = "SELECT DISTINCT IMO_DES_BAI FROM TAB_IMO WHERE IMO_DES_LOC = '$cid' ORDER BY IMO_DES_BAI";
	   }else{
	   		$sql = "SELECT DISTINCT IMO_DES_BAI FROM TAB_IMO ORDER BY IMO_DES_BAI";
	   }
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$bairro[$i]["imo_des_bai"]           = odbc_result($res,"IMO_DES_BAI"); 	   
            $i++; 
	   }
	   @odbc_close($conn);		
	   return $bairro;
	 
	 }

     function consultarImoveis($bairro,$tipo,$valini,$valfim,$cidade,$tam=300,$local,$filtros) {
		 //echo "<pre>"; print_r($filtros); die;
		 
		 if (empty($filtros['area'])) {
            $filtros['area'] = 0;
        }

        if (empty($filtros['valor'])) {
            $filtros['valor'] = 0;
        }

        if (empty($filtros['suites'])) {
            $filtros['suites'] = 0;
        }

        if (empty($filtros['vagas'])) {
            $filtros['vagas'] = 0;
        }
		
		if (empty($filtros['finalidade'])) {
            $filtros['finalidade'] = 0;
        }

         $br = "";
         if(is_array($bairro)){
             $cont = count($bairro);
             if($bairro[0] == "TODOS"){
                 $br="";
             }else {
                 for($i=0;$i<$cont;$i++) {
                     if($bairro[$i]){
                         if($cont == 1) {
                             $br = "AND IMO.IMO_DES_BAI='".$bairro[$i]."'";
                         }else if($cont > 1) {
                             if($i==0){
                                 $br.= " AND ( IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
                             }else if($i == ($cont - 1)) {
                                 $br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' ) ";
                             }else {
                                 $br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
                             }
                         }
                     }
                 }
             }
         }else {
             $br = "";
         }
         $bairro = $br;
         $i = 0;
         $imovel = array();
         if(!empty($cidade) && !empty($tipo)){
             $sql = "SELECT IMO.IMO_ISN,IMO_COD_ESP, IMO.IMO_DES_BAI, IMO.IMO_DES_TXT_DEST,IMO.IMO_DES_LOC, IMO.IMO_DES_END, IMO.IMO_VAL_ALU, IMO.IMO_VAL_ARE, TIM.TIM_DES 
				   FROM TAB_IMO IMO, TAB_TIM TIM 
				   WHERE TIM.TIM_ISN = IMO.TIM_ISN 
				   AND TIM.TIM_ISN = $tipo 
				   $bairro AND IMO.IMO_DES_LOC = '$cidade' ";
         }else if(empty($cidade) && !empty($tipo)){
             $sql = "SELECT IMO.IMO_ISN,IMO_COD_ESP, IMO.IMO_DES_BAI, IMO.IMO_DES_TXT_DEST,IMO.IMO_DES_LOC, IMO.IMO_DES_END, IMO.IMO_VAL_ALU, IMO.IMO_VAL_ARE, TIM.TIM_DES 
				   FROM TAB_IMO IMO, TAB_TIM TIM 
				   WHERE TIM.TIM_ISN = IMO.TIM_ISN 
				   AND TIM.TIM_ISN = $tipo 
				   $bairro ";
         }else {
             $sql = "SELECT IMO.IMO_ISN,IMO_COD_ESP, IMO.IMO_DES_BAI, IMO.IMO_DES_TXT_DEST,IMO.IMO_DES_LOC, IMO.IMO_DES_END, IMO.IMO_VAL_ALU, IMO.IMO_VAL_ARE, TIM.TIM_DES 
				   FROM TAB_IMO IMO, TAB_TIM TIM 
				   WHERE TIM.TIM_ISN = IMO.TIM_ISN $bairro ";
         }

         /*if($valini) {
           $valini = str_replace(",",".",$valini);
           $sql = $sql."AND IMO.IMO_VAL_ALU >= $valini ";
         }
         if($valfim) {
           $valfim = str_replace(",",".",$valfim);
           $sql = $sql."AND IMO.IMO_VAL_ALU <= $valfim ";
         }	*/

         //FILTRA PELO TAMANHO DA ÁREA
         if ($filtros['area'] != 0) {
             $area = explode('-',$filtros['area']);
             if(!$area[1]) {
                 $sql = $sql."AND IMO.IMO_VAL_ARE >= {$area[0]} ";
             } else {
                 $sql = $sql."AND IMO.IMO_VAL_ARE >= {$area[0]} AND IMO.IMO_VAL_ARE <= {$area[1]} ";
             }
         }

         //FILTRA PELO VALOR
         if ($filtros['valor'] != 0) {
             $valor = explode('-',$filtros['valor']);
             if(!$valor[1]) {
                 $sql = $sql."AND IMO.IMO_VAL_ALU >= {$valor[0]} ";
             } else {
                 $sql = $sql."AND IMO.IMO_VAL_ALU >= {$valor[0]} AND IMO.IMO_VAL_ALU <= {$valor[1]} ";
             }
		 }
		 
		 if ($filtros['finalidade'] != 0) {
		 	$sql = $sql."AND IMO_FINALIDADE = {$filtros['finalidade']} ";
		 }

         $sql = $sql."ORDER BY IMO.IMO_VAL_ALU";
         //print_r($sql); die;
         $conn=$this->connDb();
         $res = @odbc_exec($conn,$sql);
         if (@odbc_error($res)) {
             return false;
         }
         while(@odbc_fetch_row($res)) {
             $imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN");
             $imovel[$i]["cod_esp"]           = odbc_result($res,"IMO_COD_ESP");
             $imovel[$i]["imo_des_bai"]       = odbc_result($res,"IMO_DES_BAI");
             $imovel[$i]["imo_des_end"]       = odbc_result($res,"IMO_DES_END");
             $imovel[$i]["imo_val_alu"]       = odbc_result($res,"IMO_VAL_ALU");
             $imovel[$i]["imo_val_are"]       = odbc_result($res,"IMO_VAL_ARE");
             $imovel[$i]["imo_des_loc"]       = odbc_result($res,"IMO_DES_LOC");
             $imovel[$i]["imo_des_txt_dest"]  = odbc_result($res,"IMO_DES_TXT_DEST");
             $imovel[$i]["tim_des"]           = odbc_result($res,"TIM_DES");

             $i++;
         }
         for($x=0;$x<count($imovel);$x++){
             $imovel[$x]["caracsDestaque"] = $this->consultarCaracteristicaPaiDestaque($imovel[$x]["imo_isn"]);
             $imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
             $imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam*2,$local);
         }
         @odbc_close($conn);

         $total_imoveis = count($imovel);
         //print_r($total_imoveis); die;

         //FILTRA PELA QUANTIDADE DE SUITES
         if($filtros['suites'] != 0) {
             for($x=0;$x<$total_imoveis;$x++) {
                 if($imovel[$x]['caracsDestaque'][0]['qtd_suites'] < $filtros['suites']) {
                     unset($imovel[$x]);
                 }
             }
         }

         //FILTRA PELA QUANTIDADE DE VAGAS DE GARAGEM
         if($filtros['vagas'] != 0) {
             for($x=0;$x<$total_imoveis;$x++) {
                 if($imovel[$x]['caracsDestaque'][0]['qtd_vagas'] < $filtros['vagas']) {
                     unset($imovel[$x]);
                 }
             }
         }

         //REARRANJA O ARRAY COM OS IMÓVEIS
         $imovel = array_values($imovel);
         //echo "<pre>"; print_r($imovel); die;
         return $imovel;
     }
	
	function consultarImoveisPag($bairro,$tipo,$valini,$valfim,$cidade,$tam=300,$local,$quant,$marcador,$ordem='IMO.IMO_VAL_ALU') {
	   $br = "";
	   if(is_array($bairro)){
			$cont = count($bairro);
			if($bairro[0] == "TODOS" || $bairro[0]==" "){
				$br="";
			}else {		
				for($i=0;$i<$cont;$i++) {		  
				   if($cont == 1) {
					  $br = "AND IMO.IMO_DES_BAI='".$bairro[$i]."'";
				   }else if($cont > 1) {
					 if($i==0){
						$br.= " AND ( IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }else if($i == ($cont - 1)) {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' ) ";
					 }else {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }
				   }
				   
				}
			}
	   }else {
	   	  $br = "";
	   }
	   $bairro = $br;
	   $i = 0;
	   $imovel = array();
	   if(!empty($cidade)){
			$sql = "SELECT IMO.IMO_ISN,IMO_COD_ESP, IMO.IMO_DES_BAI, IMO.IMO_DES_TXT_DEST,IMO.IMO_DES_LOC, IMO.IMO_DES_END, IMO.IMO_VAL_ALU, IMO.IMO_VAL_ARE, TIM.TIM_DES 
				   FROM TAB_IMO IMO, TAB_TIM TIM 
				   WHERE TIM.TIM_ISN = IMO.TIM_ISN 
				   AND TIM.TIM_ISN = $tipo 
				   $bairro AND IMO.IMO_DES_LOC = '$cidade' ";
		}else if(empty($cidade) && !empty($tipo)){
			$sql = "SELECT IMO.IMO_ISN,IMO_COD_ESP, IMO.IMO_DES_BAI, IMO.IMO_DES_TXT_DEST,IMO.IMO_DES_LOC, IMO.IMO_DES_END, IMO.IMO_VAL_ALU, IMO.IMO_VAL_ARE, TIM.TIM_DES 
				   FROM TAB_IMO IMO, TAB_TIM TIM 
				   WHERE TIM.TIM_ISN = IMO.TIM_ISN 
				   AND TIM.TIM_ISN = $tipo 
				   $bairro ";
		}else {
			$sql = "SELECT IMO.IMO_ISN,IMO_COD_ESP, IMO.IMO_DES_BAI, IMO.IMO_DES_TXT_DEST,IMO.IMO_DES_LOC, IMO.IMO_DES_END, IMO.IMO_VAL_ALU, IMO.IMO_VAL_ARE, TIM.TIM_DES 
				   FROM TAB_IMO IMO, TAB_TIM TIM 
				   WHERE TIM.TIM_ISN = IMO.TIM_ISN $bairro ";
		}		
			   
		if($valini) { 
	      $valini = str_replace(",",".",$valini);
		  $sql = $sql."AND IMO.IMO_VAL_ALU >= $valini ";
		}	
		if($valfim) { 
	      $valfim = str_replace(",",".",$valfim);
		  $sql = $sql."AND IMO.IMO_VAL_ALU <= $valfim ";  
		}		
		if($marcador){
			$sql = $sql." AND $ordem > $marcador";
		}
	   $sql = $sql." ORDER BY $ordem";
	   $conn=$this->connDb();
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
		  if($i<5){
			$imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 
			$imovel[$i]["cod_esp"]           = odbc_result($res,"IMO_COD_ESP");	   
			$imovel[$i]["imo_des_bai"]       = odbc_result($res,"IMO_DES_BAI"); 	   
			$imovel[$i]["imo_des_end"]       = odbc_result($res,"IMO_DES_END"); 	   
			$imovel[$i]["imo_val_alu"]       = odbc_result($res,"IMO_VAL_ALU"); 	   
			$imovel[$i]["imo_val_are"]       = odbc_result($res,"IMO_VAL_ARE");
			$imovel[$i]["imo_des_loc"]       = odbc_result($res,"IMO_DES_LOC");
			$imovel[$i]["imo_des_txt_dest"]  = odbc_result($res,"IMO_DES_TXT_DEST"); 	   
			$imovel[$i]["tim_des"]           = odbc_result($res,"TIM_DES"); 
		  }else {
		  	break;
		  }	   
            $i++; 
	   }
	   for($x=0;$x<count($imovel);$x++){
			$imovel[$x]["caracs"] = $this->consultarCaracteristicaPai($imovel[$x]["imo_isn"]);
			$imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
			$imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
	   }
	   @odbc_close($conn);		
	   return $imovel;
	}
	
	function quantReg($bairro,$tipo,$valini,$valfim,$cidade) {
	   $br = "";
	   if(is_array($bairro)){
			$cont = count($bairro);
			if($bairro[0] == "TODOS" || $bairro[0]==" "){
				$br="";
			}else {		
				for($i=0;$i<$cont;$i++) {		  
				   if($cont == 1) {
					  $br = "AND IMO.IMO_DES_BAI='".$bairro[$i]."'";
				   }else if($cont > 1) {
					 if($i==0){
						$br.= " AND ( IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }else if($i == ($cont - 1)) {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' ) ";
					 }else {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }
				   }
				   
				}
			}
	   }else {
	   	  $br = "";
	   }
	   $bairro = $br;
	   $i = 0;
	   $imovel = "";
	   if(!empty($cidade)){
			$sql = "SELECT COUNT(*) AS quant FROM TAB_IMO IMO, TAB_TIM TIM 
				   WHERE TIM.TIM_ISN = IMO.TIM_ISN 
				   AND TIM.TIM_ISN = $tipo 
				   $bairro AND IMO.IMO_DES_LOC = '$cidade' ";
		}else if(empty($cidade) && !empty($tipo)){
			$sql = "SELECT COUNT(*) AS quant FROM TAB_IMO IMO, TAB_TIM TIM 
				   WHERE TIM.TIM_ISN = IMO.TIM_ISN 
				   AND TIM.TIM_ISN = $tipo 
				   $bairro ";
		}else {
			$sql = "SELECT COUNT(*) AS quant FROM TAB_IMO IMO, TAB_TIM TIM 
				   WHERE TIM.TIM_ISN = IMO.TIM_ISN $bairro ";
		}		
			   
		if($valini) { 
	      $valini = str_replace(",",".",$valini);
		  $sql = $sql."AND IMO.IMO_VAL_ALU >= $valini ";
		}	
		if($valfim) { 
	      $valfim = str_replace(",",".",$valfim);
		  $sql = $sql."AND IMO.IMO_VAL_ALU <= $valfim ";  
		}
	   
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return 0;
	   }
	   @odbc_fetch_row($res);
	   $imovel = odbc_result($res,"quant");  
	   @odbc_close($conn);		
	   return $imovel;
	}

	 function carregarDados($codigo) {
	   $con = new Conexao(); 
	   $i = 0;
	   $imovel = array();
	   $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_END,IMO_VAL_ALU,TIM_DES 
	   FROM TAB_IMO IMO, TAB_TIM TIM WHERE TIM.TIM_ISN = IMO.TIM_ISN 
	   AND IMO.IMO_ISN = $codigo
	   ORDER BY IMO_ISN";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
			$imovel[$i]["imo_des_bai"]       = odbc_result($res,"IMO_DES_BAI"); 	   
			$imovel[$i]["imo_des_end"]       = odbc_result($res,"IMO_DES_END"); 	   
			$imovel[$i]["imo_val_alu"]       = odbc_result($res,"IMO_VAL_ALU"); 	   
			$imovel[$i]["tim_des"]           = odbc_result($res,"TIM_DES"); 	   
            $i++; 
	   }
	   @odbc_close($conn);		
	   return $imovel;
	}

	 function carregarImoveisAVC($bairro,$tipo,$codigoVal,$codigoQtde,$codigoAlt,$codigoPos,$cidade,$valini,$valfim,$tam=300,$local) {
	   $imovel = "";
	    $br = "";
	   if($valini) { $valini=str_replace(",",".",$valini); }
	   if($valfim) { $valfim=str_replace(",",".",$valfim); }
	   if(is_array($bairro)){
			$cont = count($bairro);
			if($bairro[0] == "TODOS" || $bairro[0]==" "){
				$br="";
			}else {		
				for($i=0;$i<$cont;$i++) {		  
				   if($cont == 1) {
					  $br = "AND IMO.IMO_DES_BAI='".$bairro[$i]."'";
				   }else if($cont > 1) {
					 if($i==0){
						$br.= " AND ( IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }else if($i == ($cont - 1)) {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' ) ";
					 }else {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }
				   }
				   
				}
			}
	   }else {
	   	  $br = "";
	   }	
	   $bairro = $br;   
	   $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_END,IMO_VAL_ALU,IMO_VAL_ARE,TIM_DES 
	           FROM TAB_IMO IMO LEFT JOIN TAB_TIM TIM ON TIM.TIM_ISN = IMO.TIM_ISN 
			   WHERE TIM.TIM_ISN = $tipo $bairro";
		   
	   if(($valini > 0) && ($valfim > 0)) {
	      $sql = $sql."AND IMO.IMO_VAL_ALU >= $valini AND IMO.IMO_VAL_ALU <= $valfim ";
	   }else if(($valini > 0) && ($valfim == 0)) {
	      $sql = $sql."AND IMO.IMO_VAL_ALU >= $valini";
	   }else if(($valini == 0) && ($valfim > 0)) {
	      $sql = $sql."AND IMO.IMO_VAL_ALU <= $valfim";
	   }else if(($valini == 0) && ($valfim == 0)) {
	      $sql = $sql;
	   }
	   
	   if($codigoVal) {
		  $sql = $sql." AND IMO_ISN IN (".$codigoVal.")";
	   }
	   if($codigoQtde) {
		   $sql = $sql." AND IMO_ISN IN (".$codigoQtde.")";
		}
	   if($codigoAlt) {
		  $sql = $sql." AND IMO_ISN IN (".$codigoAlt.")";
	   }
	   if($codigoPos) {
		    $sql = $sql." AND IMO_ISN IN (".$codigoPos.")";
	   }
	   $sql = $sql."AND IMO.IMO_DES_LOC = '". $cidade."' ORDER BY IMO_VAL_ALU";
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   $i = 0;
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
			$imovel[$i]["imo_des_bai"]       = odbc_result($res,"IMO_DES_BAI"); 	   
			$imovel[$i]["imo_des_end"]       = odbc_result($res,"IMO_DES_END"); 	   
			$imovel[$i]["imo_val_alu"]       = odbc_result($res,"IMO_VAL_ALU"); 	   
			$imovel[$i]["imo_val_are"]       = odbc_result($res,"IMO_VAL_ARE"); 	   
			$imovel[$i]["tim_des"]           = odbc_result($res,"TIM_DES"); 	   
            $i++; 
	   }
	   @odbc_close($conn);
	   is_array($imovel)?$quant=count($imovel):$quant=0;
	   for($x=0;$x<$quant;$x++){
			$imovel[$x]["caracs"] = $this->consultarCaracteristicaPai($imovel[$x]["imo_isn"]);
			$imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
			$imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam*2,$local);
	   }		
	   return $imovel;
	}
	 function carregarCodigosBR($bairro) {
	   $con = new Conexao(); 
	   $i = 0;
	   $codimo = array();
	   if($bairro == "") {
	      $sql = "SELECT IMO_ISN   FROM TAB_IMO IMO  ORDER BY IMO_ISN";
	   }
	   else {
	      $sql = "SELECT IMO_ISN   FROM TAB_IMO IMO WHERE  $bairro 
	              ORDER BY IMO_ISN";
	   }
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codimo[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
            $i++; 
	   }
	   @odbc_close($conn);		
	   return $codimo;
	}

	 function carregarCodigosTP($tipo) {
	   $con = new Conexao(); 
	   $i = 0;
	   $codimo = array();
       $sql = "SELECT IMO_ISN  FROM TAB_IMO WHERE TIM_ISN = $tipo
	           ORDER BY IMO_ISN";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codimo[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
            $i++; 
	   }
	   @odbc_close($conn);		
	   return $codimo;
	}

	 function carregarCodigosALT($id,$tipo,$cidade) {
	   $ret="";
	   if(is_array($id)){
		   $codimo = "";
		   $sql = "SELECT IMC.IMO_ISN  FROM TAB_IMC IMC
		   		   INNER JOIN TAB_IMO IMO ON IMO.IMO_ISN = IMC.IMO_ISN
				   WHERE IMO.TIM_ISN = $tipo AND IMO.IMO_DES_LOC = '$cidade' ";
		   foreach($id as $i){
		   		if($i){
					$sql.=" AND IMC.IMO_ISN IN (SELECT DISTINCT(IMC2.IMO_ISN) FROM tab_imc IMC2 INNER JOIN TAB_IMO IMO2 ON IMC2.IMO_ISN = IMO2.IMO_ISN WHERE IMC2.CAR_ISN = $i) ";
				}
		   }
		   $sql.=" GROUP BY IMC.IMO_ISN ORDER BY IMC.IMO_ISN";
		   $conn=$this->connDb();	
		   $res = @odbc_exec($conn,$sql);
		   if (@odbc_error($res)) { 
			  return false;
		   }
		   $ret="";
		   while(@odbc_fetch_row($res)) {
			  $ret.=odbc_result($res,"IMO_ISN").',';	   
		   }
		  @odbc_close($conn);
		  if($ret) { $ret.="0"; } 
	  }		
	  return $ret;
	 
	}

	 function carregarCodigosQTDE($array,$tipo,$cidade) {
	   $ret="";
	   if(is_array($array)){
		   $codimo = "";	   
		   $sql = "SELECT IMC.IMO_ISN  FROM TAB_IMC IMC
		   		   INNER JOIN TAB_IMO IMO ON IMO.IMO_ISN = IMC.IMO_ISN
				   WHERE IMO.TIM_ISN = $tipo AND IMO.IMO_DES_LOC = '$cidade' ";
		   foreach($array as $ar){
		   		if($ar){
					 $sql.=" AND IMC.IMO_ISN IN ( SELECT DISTINCT(IMC2.IMO_ISN) FROM tab_imc IMC2 INNER JOIN TAB_IMO IMO2 ON IMC2.IMO_ISN = IMO2.IMO_ISN WHERE IMC2.CAR_ISN = ".$ar['id']." ";
					 if(!$ar['ini'] && $ar['fim']) {
						$sql.= " AND IMC2.IMC_QTD <= ".$ar['fim']." ) ";
					 }else if($ar['ini'] && !$ar['fim']) {
						$sql.= "AND IMC2.IMC_QTD >= ".$ar['ini']." ) ";
					 }else if($array[0]['ini'] && $ar['fim']) {
						$sql.= " AND IMC2.IMC_QTD >= ".$ar['ini']." AND IMC2.IMC_QTD <= ".$ar['fim']." ) ";
					 }
				}
		   }
		   $sql.=" GROUP BY IMC.IMO_ISN ORDER BY IMC.IMO_ISN ";
		   $conn=$this->connDb();	
		   $res = @odbc_exec($conn,$sql);
		   if (@odbc_error($res)) { 
			   return false;
		   }
		   $ret="";
		   while(@odbc_fetch_row($res)) {
			  $ret.=odbc_result($res,"IMO_ISN").',';	   
		   }
		  @odbc_close($conn);
		  if($ret) { $ret.="0"; } 
	   }
	   return $ret;
	}
	
	function carregarCodigosPOS($id,$tipo,$cidade) {
	   $ret="";
	   if(is_array($id)){	   
		   $codimo = "";
		    $sql = "SELECT IMC.IMO_ISN  FROM TAB_IMC IMC
		   		   INNER JOIN TAB_IMO IMO ON IMO.IMO_ISN = IMC.IMO_ISN
				   WHERE IMO.TIM_ISN = $tipo AND IMO.IMO_DES_LOC = '$cidade' ";
		   foreach($id as $i){
		   		if($i){
					$sql.=" AND IMC.IMO_ISN IN (SELECT DISTINCT(IMC2.IMO_ISN) FROM tab_imc IMC2 INNER JOIN TAB_IMO IMO2 ON IMC2.IMO_ISN = IMO2.IMO_ISN WHERE IMC2.DCA_ISN = $i) ";
				}
		   }
		   $sql.=" GROUP BY IMC.IMO_ISN ORDER BY IMC.IMO_ISN";
		   $conn=$this->connDb();	
		   $res = @odbc_exec($conn,$sql);
		   if (@odbc_error($res)) { 
			   return false;
		   }
		  $ret="";
		   while(@odbc_fetch_row($res)) {
			  $ret.=odbc_result($res,"IMO_ISN").',';	   
		   }
		  @odbc_close($conn);
		  if($ret) { $ret.="0"; } 
	   }
	   return $ret;
	}

	 function carregarCodigosVALOR($array,$tipo,$cidade) {
	   $ret="";
	   if(is_array($array)){	   
		   $codimo = "";	   
		   $sql = "SELECT IMC.IMO_ISN  FROM TAB_IMC IMC
		   		   INNER JOIN TAB_IMO IMO ON IMO.IMO_ISN = IMC.IMO_ISN
				   WHERE IMO.TIM_ISN = $tipo AND IMO.IMO_DES_LOC = '$cidade' ";
		   foreach($array as $ar){
		   		if($ar){
					 $sql.=" AND IMC.IMO_ISN IN ( SELECT DISTINCT(IMC2.IMO_ISN) FROM tab_imc IMC2 INNER JOIN TAB_IMO IMO2 ON IMC2.IMO_ISN = IMO2.IMO_ISN WHERE IMC2.CAR_ISN = ".$ar['id']." ";
					 if(!$ar['ini'] && $ar['fim']) {
						$sql.= " AND IMC2.IMC_VAL <= ".$ar['fim']." ) ";
					 }else if($ar['ini'] && !$ar['fim']) {
						$sql.= "AND IMC2.IMC_VAL >= ".$ar['ini']." ) ";
					 }else if($array[0]['ini'] && $ar['fim']) {
						$sql.= " AND IMC2.IMC_VAL >= ".$ar['ini']." AND IMC2.IMC_VAL <= ".$ar['fim']." ) ";
					 }
				}
		   }
		   $sql.=" GROUP BY IMC.IMO_ISN ORDER BY IMC.IMO_ISN ";
		   $conn=$this->connDb();	
		   $res = @odbc_exec($conn,$sql);
		   if (@odbc_error($res)) { 
			   return false;
		   }
		   $ret="";
		   while(@odbc_fetch_row($res)) {
			  $ret.=odbc_result($res,"IMO_ISN").',';	   
		   }
		  @odbc_close($conn);
		  if($ret) { $ret.="0"; } 
	   }
	   return $ret;
	}
	
	public function buscarFoto2($imo_isn,$caminho,$tamanho=300,$local=''){
		$sql = "SELECT *  FROM TAB_IMI WHERE IMO_ISN = $imo_isn AND IMI_TIP_DEST = 1";
		$conn=$this->connDb();	
		$res = @odbc_exec($conn,$sql);
		if (@odbc_error($res)) { 
			return false;
		}		
		@odbc_fetch_row($res);
		if(@file_exists($caminho.odbc_result($res,"IMI_NOM")) && odbc_result($res,"IMI_NOM")){
			$foto = $local.'application/helpers/inc_thumb.php?img=../../'.$caminho.odbc_result($res,"IMI_NOM").'&size=1200';
		}		
		if(empty($foto)){
			$sql = "SELECT * FROM TAB_IMI WHERE IMO_ISN = ".$imo_isn." ORDER BY IMI_NUM ASC ";
			$res = @odbc_exec($conn,$sql);
			if (@odbc_error($res)) { 
				return false;
			}
			while(@odbc_fetch_row($res)) {
			 if(@file_exists($caminho.odbc_result($res,"IMI_NOM")) && odbc_result($res,"IMI_NOM")){ 	
				$foto = $local.'application/helpers/inc_thumb.php?img=../../'.$caminho.odbc_result($res,"IMI_NOM").'&size=1200';
				break;
			  }
			}
		}		
		if(empty($foto)){
			$foto = $local.'application/helpers/inc_thumb.php?img=../images/indisponivel.jpg&size=1200';
		} 
		@odbc_close($conn);		
		return $foto;
	}
	
	public function carregarFotos($cod,$caminho,$tamanho=130,$local='') {
	   $sql = "SELECT * FROM TAB_IMI  WHERE IMO_ISN = $cod ORDER BY IMI_NUM";
	   $i = 0;	   
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) {
		   return false;
	   } 
	   $img = "";
	   while(@odbc_fetch_row($res)) {
		 if(@file_exists($caminho.odbc_result($res,"IMI_NOM")) && odbc_result($res,"IMI_NOM")){
			$img[$i]["descricao"] = odbc_result($res,"IMI_DES"); 	   
			$img[$i]["nome"] = $local.'application/helpers/inc_thumb.php?img=../../'.$caminho.odbc_result($res,"IMI_NOM").'&size=1200'; 
			$img[$i]["codigo"]    = odbc_result($res,"IMI_ISN"); 
			$i++; 
		 }
	   }
	   @odbc_close($conn);
	   return $img;
	 }

	 public function carregarFotosIndiqueAmigo($cod,$caminho='') {
		 $sql = "SELECT * FROM TAB_IMI  WHERE IMO_ISN = $cod ORDER BY IMI_NUM";
		 $caminho = $this->CaminhoFoto();
		 $caminho2 = str_replace('..','',$caminho);

		 if ($_SERVER['IMOB_ENV'] == 'prod') {
			 $caminhoFoto = $_SERVER['DOCUMENT_ROOT'].$caminho2;
		 } else {
			 $caminhoFoto = $_SERVER['DOCUMENT_ROOT'].'/dmvimoveis.com.br'.$caminho2;
		 }

		 $i = 0;
		 $conn=$this->connDb();
		 $res = @odbc_exec($conn,$sql);
		 if (@odbc_error($res)) {
			 return false;
		 }
		 $img = "";
		 while(@odbc_fetch_row($res)) {
			 if(@file_exists($caminho.odbc_result($res,"IMI_NOM")) && odbc_result($res,"IMI_NOM")){
				 $img[$i]["descricao"] = odbc_result($res,"IMI_DES");
				 $img[$i]["nome"]	   = odbc_result($res,"IMI_NOM");
				 $img[$i]["arquivo"]   = $caminhoFoto.odbc_result($res,"IMI_NOM");
				 $i++;
			 }
		 }
		 @odbc_close($conn);
		 return $img;
	 }

	 function consultarImovel($codigo,$tam=300,$local){
	   $idm = $_SESSION['idioma'];
	   if(is_numeric($codigo)){
	   		$sql = "SELECT IMO.*,TIM.TIM_DES 
	   		   FROM TAB_IMO IMO, TAB_TIM TIM 
			   WHERE TIM.TIM_ISN = IMO.TIM_ISN 
			   AND IMO.IMO_ISN = $codigo	   
			   ORDER BY IMO.IMO_ISN";
		}else {
			$sql = "SELECT IMO.*,TIM.TIM_DES 
	   		   FROM TAB_IMO IMO, TAB_TIM TIM 
			   WHERE TIM.TIM_ISN = IMO.TIM_ISN 
			   AND IMO.IMO_COD_ESP = '$codigo'	   
			   ORDER BY IMO.IMO_ISN";
		}
	   $i = 0;
	   $imovel = array();
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 
			$imovel[$i]["cod_esp"]           = odbc_result($res,"IMO_COD_ESP"); 	   
			$imovel[$i]["imo_des_bai"]       = odbc_result($res,"IMO_DES_BAI"); 	   
			$imovel[$i]["imo_des_end"]       = odbc_result($res,"IMO_DES_END");
			$imovel[$i]["imo_des_loc"]       = odbc_result($res,"IMO_DES_LOC");	   
			$imovel[$i]["imo_val_ven"]       = odbc_result($res,"IMO_VAL_ALU");
			$imovel[$i]["imo_val_alu"]       = odbc_result($res,"IMO_VAL_ALU"); 	   
			$imovel[$i]["imo_val_are"]       = odbc_result($res,"IMO_VAL_ARE");
			$imovel[$i]["imo_val_are_priv"]  = odbc_result($res,"IMO_VAL_ARE");
			$imovel[$i]["imo_tip_usar"]      = odbc_result($res,"IMO_TIP_USAR_TXT_DET"); 
			$imovel[$i]["imo_dest_det"]      = odbc_result($res,"IMO_DES_TXT_DET"); 
			$imovel[$i]["imo_dest_det_2"]    = odbc_result($res,"IMO_DES_TXT_DET_2");
			$imovel[$i]["imo_des_est"]       = odbc_result($res,"IMO_DES_TXT_DEST"); 
            $imovel[$i]["imo_finalidade"]    = odbc_result($res,"IMO_FINALIDADE");
			$imovel[$i]["tim_des"] = odbc_result($res,"TIM_DES");
            $i++; 
	   }
	   for($x=0;$x<count($imovel);$x++){
	   		$imovel[$x]["caracs"] = $this->consultarCaracteristicaPai($codigo);
	   		$imovel[$x]["caracsDestaque"] = $this->consultarCaracteristicaPaiDestaque($codigo);
			$imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
			$imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam*2,$local);
	   }
	   @odbc_close($conn);		
	   return $imovel;
	}

	 function consultarCaracteristicaPai($codigo) {
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descCarDes = "CAR_DES_ING"; 
	   }else if($idm == 3){
	   	  $descCarDes = "CAR_DES_ESP"; 
	   }else {
	      $descCarDes = "CAR_DES"; 
	   }  
	   $descCarDes = "CAR_DES"; 
	   $i = 0;
	   $caracPai = array();
	   $caracPaiVal = array();
	   $sql = "SELECT IMC.*, CAR.CAR_DES,CAR.$descCarDes,CAR.CAR_TIP, CAR.CAR_DES_UNI FROM TAB_IMC IMC
              LEFT JOIN TAB_CAR CAR ON CAR.CAR_ISN = IMC.CAR_ISN
              WHERE IMC.IMO_ISN = $codigo AND
              CAR.CAR_ISN_PAI = 0 
              ORDER BY CAR.CAR_DES";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		  return false;
	   }
	   $carsFilha = array();
	   while(@odbc_fetch_row($res)) {
	   		$caracPai[$i]["imc_isn"]           = odbc_result($res,"IMC_ISN"); 	   
			$caracPai[$i]["imc_car_isn"]       = odbc_result($res,"CAR_ISN"); 
			$caracPai[$i]["car_car_tip"]       = odbc_result($res,"CAR_TIP"); 	   
			$caracPai[$i]["car_car_des"]       = odbc_result($res,$descCarDes); 
			$caracPai[$i]["car_car_des_uni"]   = odbc_result($res,"CAR_DES_UNI"); 
			$caracPai[$i]["imc_qtd"]           = odbc_result($res,"IMC_QTD");
			$caracPai[$i]["imc_des"]           = odbc_result($res,"IMC_DES");
			$caracPai[$i]["imc_val"]           = odbc_result($res,"IMC_VAL");
			$caracPai[$i]["imc_tip_con"]       = odbc_result($res,"IMC_TIP_CON");
			$caracPai[$i]["imc_dca_isn"]       = odbc_result($res,"DCA_ISN");
			if(empty($caracPai[$i]["car_car_des"])){
				$caracPai[$i]["car_car_des"] = odbc_result($res,"CAR_DES"); 
			}
			$i++;
		}	
		for($i=0;$i<count($caracPai);$i++) {
			if($caracPai[$i]["car_car_tip"] == 1) {
			   if($caracPai[$i]["imc_tip_con"] == 0) {
			      $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			      $caracPaiVal[$i]["imc_tip_con"] = "NAO";
			   }else if($caracPai[$i]["imc_tip_con"] == 1) {
			      $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			      $caracPaiVal[$i]["imc_tip_con"] = "SIM";
			   }
			}else if($caracPai[$i]["car_car_tip"] == 2) {
			   $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			   $caracPaiVal[$i]["imc_qtd"] = $caracPai[$i]["imc_qtd"];
			}else if($caracPai[$i]["car_car_tip"] == 3) {
			      $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			      $caracPaiVal[$i]["dca_des"]      = $this->dcaValor($caracPai[$i]["imc_dca_isn"]);
			}else if($caracPai[$i]["car_car_tip"] == 4) {
			   $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			   $caracPaiVal[$i]["imc_des"] = $caracPai[$i]["imc_des"];
			}else if($caracPai[$i]["car_car_tip"] == 5) {
			   $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			   $caracPaiVal[$i]["imc_val"] = $caracPai[$i]["imc_val"];
			   $caracPaiVal[$i]["car_car_des_uni"] = $caracPai[$i]["car_car_des_uni"] ;
			}
            $caracPaiVal[$i]["car_car_tipPai"] = $caracPai[$i]["car_car_tip"]; 
			$caracPaiVal[$i]["imo_obs"] = $this->obsValor($codigo); 
		}
		for($i=0;$i < count($caracPai);$i++) {
		   $caracPai[$i]["imc_isn"] = $caracPai[$i]["imc_isn"];
			$caracPai[$i]["imc_car_isn"] = $caracPai[$i]["imc_car_isn"];
			$caracPai[$i]["car_car_tip"] = $caracPai[$i]["car_car_tip"];
			$caracPai[$i]["car_car_des"] = $caracPai[$i]["car_car_des"];
			$caracPai[$i]["car_car_des_uni"] = $caracPai[$i]["car_car_des_uni"];
			$caracPai[$i]["imc_qtd"] = $caracPai[$i]["imc_qtd"];
			$caracPai[$i]["imc_des"]  = $caracPai[$i]["imc_des"];
			$caracPai[$i]["imc_val"]  = $caracPai[$i]["imc_val"];
			$caracPai[$i]["imc_tip_con"] = $caracPai[$i]["imc_tip_con"]; 
			$caracPai[$i]["imc_dca_isn"] = $caracPai[$i]["imc_dca_isn"];
		    $caracPaiVal[$i]["caracteristicas"]  = $this->consultarCaracteristicaFilha($codigo,$caracPai[$i]["imc_car_isn"]);
		}	
	   @odbc_close($conn);		
	   return $caracPaiVal;
	 }
	 
	 function dcaValor($numero) {
	   $sql = "SELECT DCA.DCA_DES FROM TAB_DCA DCA WHERE DCA.DCA_ISN = ".$numero;
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
		  $dca_des  = odbc_result($res,"DCA_DES");
	   }   
	   @odbc_close($conn);		
	   return $dca_des;
	}
	
	function obsValor($numero) {
	   $sql = "SELECT IMO_OBS FROM TAB_IMO WHERE IMO_ISN = ".$numero;
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
		  $obs_des  = odbc_result($res,"IMO_OBS");
	   }	   
	   @odbc_close($conn);		
	   return $obs_des;
	}
	
	function consultarCaracteristicaFilha($codigo,$car_isn) {
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descCarDes = "CAR_DES_ING"; 
	   }else if($idm == 3){
	   	  $descCarDes = "CAR_DES_ESP"; 
	   }else {
	      $descCarDes = "CAR_DES"; 
	   }  
	   $descCarDes = "CAR_DES"; 
	   $caracFilha = array();
	   $caracFilhaVal = array();
	   $carsPai = array();
	   $i = 0;
	   $sql = "SELECT CAR.CAR_DES,CAR.$descCarDes,CAR.CAR_TIP, IMC.*  
			   FROM TAB_IMC IMC
			   LEFT JOIN TAB_CAR CAR ON CAR.CAR_ISN = IMC.CAR_ISN 
			   WHERE IMC.IMO_ISN = $codigo AND
			   IMC.CAR_ISN IN (SELECT CAR_ISN FROM TAB_CAR WHERE CAR_ISN_PAI =".$car_isn.")ORDER BY IMC_NUM_SEQ";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		  return false;
	   }
	   while(@odbc_fetch_row($res)) {			
			$caracFilha[$i]["imc_isn"]           = odbc_result($res,"IMC_ISN"); 	   
			$caracFilha[$i]["imc_car_isn"]       = odbc_result($res,"CAR_ISN"); 	   
			$caracFilha[$i]["car_car_des"]       = odbc_result($res,$descCarDes); 	   
			$caracFilha[$i]["car_car_tip"]       = odbc_result($res,"CAR_TIP"); 	   
			$caracFilha[$i]["imc_tip_con"]       = odbc_result($res,"IMC_TIP_CON"); 	   
			$caracFilha[$i]["imc_qtd"]           = odbc_result($res,"IMC_QTD"); 	   
			$caracFilha[$i]["imc_val"]           = odbc_result($res,"IMC_VAL"); 	   
			$caracFilha[$i]["imc_des"]           = odbc_result($res,"IMC_DES"); 	   
			$caracFilha[$i]["imc_num_seq"]       = odbc_result($res,"IMC_NUM_SEQ"); 	   
			$caracFilha[$i]["dca_isn"]           = odbc_result($res,"DCA_ISN"); 
			if(empty($caracFilha[$i]["car_car_des"])){
				$caracFilha[$i]["car_car_des"]  = odbc_result($res,"CAR_DES");
			}	
			$i++;
	   }	
		for($i=0;$i<count($caracFilha);$i++) {
			if($caracFilha[$i]["car_car_tip"] == 1) {
			   $caracFilha[$i]["imc_tip_con"];
			   if($caracFilha[$i]["imc_tip_con"] == 0) {
				  $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
				  $caracFilhaVal[$i]["imc_tip_con"] = "NAO";
			   }else if($caracFilha[$i]["imc_tip_con"] == 1) {
				  $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
				  $caracFilhaVal[$i]["imc_tip_con"] = "SIM";
			   }
			}else if($caracFilha[$i]["car_car_tip"] == 2) {
			   $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
			   $caracFilhaVal[$i]["imc_qtd"] = $caracFilha[$i]["imc_qtd"];
			}else if($caracFilha[$i]["car_car_tip"] == 3) {
				  $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
				  $caracFilhaVal[$i]["dca_des"]      = $this->dcaValor($caracFilha[$i]["dca_isn"]);
			}else if($caracFilha[$i]["car_car_tip"] == 4) {
			   $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
			   $caracFilhaVal[$i]["imc_des"] = $caracFilha[$i]["imc_des"];
			}else if($caracFilha[$i]["car_car_tip"] == 5) {
			   $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
			   $caracFilhaVal[$i]["imc_val"] = $caracFilha[$i]["imc_val"];
			}
			$caracFilhaVal[$i]["imc_num_seq"] = $caracFilha[$i]["imc_num_seq"]; 
			$caracFilhaVal[$i]["car_car_tipFilha"] = $caracFilha[$i]["car_car_tip"]; 
		}
	   @odbc_close($conn);		
	   return $caracFilhaVal;
	 }
	
	 function buscarCaracteristicaPai() {
       $crPai = new Carac_Pai();
	   $caracPai = array();
	   $imoveis = $_SESSION["arrayImoveisIMF"];
	   $qtd = count($imoveis);
	   for($i=0;$i<$qtd;$i++) {
	     $caracPai[$i] = $crPai->carregarCaracteristicaPai($imoveis[$i][imo_isn]);
	   } 
	   return $caracPai;
	}
	
	function buscarCaracteristicaPaiDetal() {
       $crPai = new Carac_Pai();
	   $caracPai = array();
	   $imoveis = $_SESSION["arrayImoveisHOU"];
	   $qtd = count($imoveis);
	   if(!is_array($_SESSION["idchkCHOU"])){
	   		for($i=0;$i<$qtd;$i++) {
			  $caracPai[$i] = $crPai->carregarCaracteristicaPai($imoveis[$i][imo_isn]);
		    } 
	   }else {
	   		$idcs = implode(",",$_SESSION["idchkCHOU"]);
	   		for($i=0;$i<$qtd;$i++) {
			  if(eregi($imoveis[$i]['imo_isn'],$idcs)){
			  		$caracPai[$i] = $crPai->carregarCaracteristicaPai($imoveis[$i][imo_isn]);
			  }
		    } 			
	   }
	   
	   return $caracPai;
	}

	 function carregarCaracteristicaPai($codigo) {
       $crPai = new Carac_Pai();
	   $caracPai = array();
	   $caracPai = $crPai->carregarCaracteristicaPai($codigo);
	   return $caracPai;
	}

	function carregaCaracteristicasAv($tip) {
	   $i = 0;
	   $imovel = "";
	   $sql = "SELECT DISTINCT CAR_DES,CAR_ISN,CAR_TIP,CAR_DES_UNI
	   FROM TAB_CAR WHERE CAR_TIP_NET_PESQ = 1 AND CAR_TIP <> 4	 AND CAR_ISN_PAI = 0 AND TIM_ISN = $tip ORDER BY CAR_DES";
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["car_isn"]           = odbc_result($res,"CAR_ISN"); 	   
			$imovel[$i]["car_des"]           = odbc_result($res,"CAR_DES"); 	   
			$imovel[$i]["car_tip"]           = odbc_result($res,"CAR_TIP"); 	   
			$imovel[$i]["car_des_uni"]       = odbc_result($res,"CAR_DES_UNI"); 	   
         $i++; 
	   }
	   $valpre = "";
	   is_array($imovel)?$quant=count($imovel):$quant=0;
	   for($cont=0;$cont<$quant;$cont++) {
	      if($imovel[$cont]["car_tip"] == 3) {
		     $valpre = $this->obterValPre($imovel[$cont]["car_isn"]);  
			 $imovel[$cont]["val_pre"] = $valpre;
		  }	 
	   }
	   @odbc_close($conn);		
	   return $imovel;
	}

	function obterValPre($num) {
	   $i = 0;
	   $valores = "";
	   $sql = "SELECT DCA_ISN,DCA_DES 
	   FROM TAB_DCA WHERE CAR_ISN = $num   ORDER BY DCA_DES";
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$valores[$i]["dca_isn"]           = odbc_result($res,"DCA_ISN"); 	   
			$valores[$i]["dca_des"]           = odbc_result($res,"DCA_DES"); 	   
            $i++; 
	   }
	   @odbc_close($conn);		
	   return $valores;
	}
   function carregarCodigos() {
	   $con = new Conexao(); 
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT IMO.IMO_ISN, IMO.IMO_DES_TXT_DEST, IMO.IMO_DES_BAI, IMO.IMO_DES_END, TIM.TIM_DES, IMI.IMI_NOM 
	   		   FROM TAB_IMO IMO, TAB_TIM TIM, TAB_IMI IMI 
			   WHERE IMO.IMO_TIP_DEST = 1 
			   AND IMO.IMO_TIP_DEST_PRIM_PAG = 1
			   AND TIM.TIM_ISN = IMO.TIM_ISN
			   AND IMI.IMO_ISN = IMO.IMO_ISN
			   AND IMI.IMI_TIP_DEST = 1   
			   ORDER BY IMO.IMO_ISN";
	   //echo "<pre>$sql</pre>";exit;
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]        = odbc_result($res,"IMO_ISN"); 	   
			$codigo[$i]["imo_des_est"]    = odbc_result($res,"IMO_DES_TXT_DEST"); 
			$codigo[$i]["tim_des"]        = odbc_result($res,"TIM_DES");
			$codigo[$i]["imo_des_bai"]        = odbc_result($res,"IMO_DES_BAI");	
			$codigo[$i]["imo_des_end"]        = odbc_result($res,"IMO_DES_END"); 
			$codigo[$i]["imi_nom"]        = odbc_result($res,"IMI_NOM");  
         $i++; 
	   }
	   @odbc_close($conn);		
	   return $codigo;
   }

   function carregarTodosCodigos() {
	   $con = new Conexao(); 
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT IMO_ISN,IMO_DES_TXT_DEST 
	   FROM TAB_IMO WHERE IMO_TIP_DEST = 1 ORDER BY IMO_ISN";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]            = odbc_result($res,"IMO_ISN"); 	   
			$codigo[$i]["imo_des_est"]        = odbc_result($res,"IMO_DES_TXT_DEST"); 	   
         $i++; 
	   }
	   @odbc_close($conn);		
	   return $codigo;
   }

   function carregarDestaquesCapa($cod) {
	   $con = new Conexao(); 
	   //$i = 0;
	   $destaque = array();
	   $sql = "SELECT IMI_NOM  FROM TAB_IMI WHERE IMO_ISN = $cod AND IMI_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$destaque["imi_nom"] = odbc_result($res,"IMI_NOM"); 	   
	   }
	   @odbc_close($conn);		
	   return $destaque;
   }

   function carregarDestaques($cod) {
	   $con = new Conexao(); 
	   //$i = 0;
	   $destaque = array();
	   $sql = "SELECT IMI_NOM  FROM TAB_IMI WHERE IMO_ISN = $cod AND IMI_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$destaque["imi_nom"] = odbc_result($res,"IMI_NOM"); 	   
	   }
	   @odbc_close($conn);		
	   return $destaque;
   }
   
   	 function consultarFotoDestaque($codigo,$bairro,$cidade,$tipo,$valini,$valfim) {
	   $con = new Conexao(); 
	   $i = 0;
	   $imovel = array();
	   $sql = "SELECT  IMO.IMO_ISN,  IMO.IMO_DES_BAI,  IMO.IMO_DES_END,  IMO.IMO_VAL_ALU,  IMO.IMO_VAL_ARE, TIM.TIM_DES, IMI.IMI_NOM 
	   		   FROM TAB_IMO IMO, TAB_TIM TIM, TAB_IMI IMI
			   WHERE TIM.TIM_ISN = IMO.TIM_ISN 
	   		   AND TIM.TIM_ISN = $tipo 
			   $bairro 
			   AND IMO.IMO_DES_LOC = '$cidade' 
			   AND IMI.IMO_ISN = IMO.IMO_ISN
			   AND IMI.IMI_TIP_DEST = 1 ";
			   
		if($valini) { 
	      $sql = $sql."AND IMO.IMO_VAL_ALU >= $valini ";
		}	
		if($valfim) { 
	      $sql = $sql."AND IMO.IMO_VAL_ALU <= $valfim ";  
		}	
	   $sql = $sql."ORDER BY IMO.IMO_VAL_ALU";
	   //echo $sql;exit;
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
			$imovel[$i]["imi_nom"]           = odbc_result($res,"IMI_NOM");	   
            $i++; 
	   }
	   @odbc_close($conn);		
	   return $imovel;
	}
	
	function consultarFotoDestaquePorCodigo($codigo) {
	   $con = new Conexao(); 
	   $i = 0;
	   $imovel = array();
	   $sql = "SELECT  IMI.IMO_ISN, IMI.IMI_NOM 
	   		   FROM TAB_IMI IMI
			   WHERE IMI.IMO_ISN = $codigo
			   AND IMI.IMI_TIP_DEST = 1 ";
			   
		//echo $sql;exit;
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
			$imovel[$i]["imi_nom"]           = odbc_result($res,"IMI_NOM");	   
            $i++; 
	   }
	   @odbc_close($conn);		
	   return $imovel;
	}

	function consultarCroqui($cod) {
	  $con = new Conexao();
	  $imo = array();
	  $sql = "SELECT IMO_DES_END,IMO_DES_LOG_DIR1,IMO_DES_LOG_DIR2,IMO_DES_LOG_ESQ1,IMO_DES_LOG_ESQ2,
	          IMO_DES_LOG_TRAS,IMO_DES_REF_1,IMO_DES_REF_2 FROM TAB_IMO WHERE IMO_ISN = $cod";
		$conn=$con->connDb();			 
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imo["imo_log_pr"] = odbc_result($res,"IMO_DES_END"); 	   
			$imo["imo_log_dir1"] = odbc_result($res,"IMO_DES_LOG_DIR1"); 	   
			$imo["imo_log_dir2"] = odbc_result($res,"IMO_DES_LOG_DIR2"); 	   
			$imo["imo_log_esq1"] = odbc_result($res,"IMO_DES_LOG_ESQ1"); 	   
			$imo["imo_log_esq2"] = odbc_result($res,"IMO_DES_LOG_ESQ2"); 	   
			$imo["imo_log_tras"] = odbc_result($res,"IMO_DES_LOG_TRAS"); 	   
			$imo["imo_des_ref1"] = odbc_result($res,"IMO_DES_REF_1"); 	   
			$imo["imo_des_ref2"] = odbc_result($res,"IMO_DES_REF_2"); 	   
	   }
	   @odbc_close($conn);		
	   return $imo;
	}
 function carregarCodigosAdm($end=0, $num=0) {
	   $con = new Conexao(); 
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT IMO.IMO_ISN,IMO.IMO_DES_END,IMO.IMO_TIP_DEST,
	           IMO.IMO_TIP_DEST_PRIM_PAG,TIM.TIM_DES 
	           FROM TAB_IMO IMO,TAB_TIM TIM WHERE IMO.TIM_ISN = TIM.TIM_ISN ";
	   if($num){
	   $sql = $sql."AND IMO_DES_END LIKE '%$end%' ";
	   }
	   if($num){
	   $sql = $sql."AND IMO.IMO_ISN = $end ";
	   }
	   $sql = $sql."ORDER BY IMO_ISN";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]              = odbc_result($res,"IMO_ISN"); 	   
			$codigo[$i]["imo_des_end"]          = odbc_result($res,"IMO_DES_END"); 	   
			$codigo[$i]["imo_tip_dest"]         = odbc_result($res,"IMO_TIP_DEST"); 	   
			$codigo[$i]["imo_tip_dest_prim"]    = odbc_result($res,"IMO_TIP_DEST_PRIM_PAG"); 	
			$codigo[$i]["tipo"]                 = odbc_result($res,"TIM_DES");
			$i++; 
	   }
	   for($x=0;$x < count($codigo);$x++) {
			$imovel[$x]["imo_isn"]              = $codigo[$x]["imo_isn"]; 	   
			$imovel[$x]["imo_des_end"]          = $codigo[$x]["imo_des_end"]; 	   
			$imovel[$x]["imo_tip_dest"]         = $codigo[$x]["imo_tip_dest"]; 	   
			$imovel[$x]["imo_tip_dest_prim"]    = $codigo[$x]["imo_tip_dest_prim"]; 				            $imovel[$x]["tipo"]                 = $codigo[$x]["tipo"];
            $imovel[$x]["foto"]                 = $this->carregarFotoCod($codigo[$x]["imo_isn"]);
	   }
	   @odbc_close($conn);		
	   return $imovel;
 }
 function carregarCodigosAdmEsp($cod) {
	   $con = new Conexao(); 
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT IMO.IMO_ISN,IMO.IMO_DES_END,IMO.IMO_TIP_DEST,
	           IMO.IMO_TIP_DEST_PRIM_PAG,TIM.TIM_DES 
	           FROM TAB_IMO IMO,TAB_TIM TIM WHERE IMO.TIM_ISN = TIM.TIM_ISN 
			   AND IMO.IMO_ISN = $cod";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]              = odbc_result($res,"IMO_ISN"); 	   
			$codigo[$i]["imo_des_end"]          = odbc_result($res,"IMO_DES_END"); 	   
			$codigo[$i]["imo_tip_dest"]         = odbc_result($res,"IMO_TIP_DEST"); 	   
			$codigo[$i]["imo_tip_dest_prim"]    = odbc_result($res,"IMO_TIP_DEST_PRIM_PAG"); 	            $codigo[$i]["tipo"]                 = odbc_result($res,"TIM_DES");
            $i++; 
	   }
	   for($x=0;$x < count($codigo);$x++) {
			$imovel[$x]["imo_isn"]              = $codigo[$x]["imo_isn"]; 	   
			$imovel[$x]["imo_des_end"]          = $codigo[$x]["imo_des_end"]; 	   
			$imovel[$x]["imo_tip_dest"]         = $codigo[$x]["imo_tip_dest"]; 	   
			$imovel[$x]["imo_tip_dest_prim"]    = $codigo[$x]["imo_tip_dest_prim"]; 				            $imovel[$x]["tipo"]                 = $codigo[$x]["tipo"];
            $imovel[$x]["foto"]                 = $this->carregarFotoCod($codigo[$x]["imo_isn"]);
	   }
	   @odbc_close($conn);		
	   return $imovel;
 }
 function carregarFotoCod($cod) {
	   $con = new Conexao(); 
	   $codigo = array();
	   $sql = "SELECT IMI_NOM 
	   FROM TAB_IMI WHERE IMO_ISN = $cod AND
	   IMI_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$img  =  odbc_result($res,"IMI_NOM"); 	   
	   }
	   @odbc_close($conn);		
	   return $img;
 }
 function buscarImagensAdm($cod) {
	   $con = new Conexao(); 
	   $codigo = array();
	   $i = 0;
	   $sql = "SELECT IMI_NOM 
	   FROM TAB_IMI WHERE IMO_ISN = $cod"; 
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$img[$i]["nome"]  =  odbc_result($res,"IMI_NOM"); 	   
			$i++;
	   }
	   @odbc_close($conn);
	   for($x=0;$x < count($img);$x++) {
	       $imagem[$x]["nome"]     =  $img[$x]["nome"];
		   if($imagem[$x]["nome"] == $this->carregarFotoCod($cod)) {
		      $imagem[$x]["destaque"] =  $imagem[$x]["nome"];
		   }
	   }
	   return $imagem;
  }
  function alterarImagemAdm($cod,$nom) {
	   $con = new Conexao(); 
	   $sql = "UPDATE TAB_IMI SET IMI_TIP_DEST = 0 
	           WHERE IMO_ISN = $cod AND IMI_TIP_DEST = 1"; 
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   $sql2 = "UPDATE TAB_IMI SET IMI_TIP_DEST = 1
	            WHERE IMO_ISN = $cod AND IMI_NOM = '$nom'";
	   $res = @odbc_exec($conn,$sql2);
	   @odbc_close($conn);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
  function setarDestaque($cod) {
       $con = new Conexao();
	   $sql = "UPDATE TAB_IMO SET IMO_TIP_DEST = 1
	          WHERE IMO_ISN = $cod";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
  function setarDestaquePrim($cod) {
       $con = new Conexao();
	   $sql = "UPDATE TAB_IMO SET IMO_TIP_DEST = 1,
	           IMO_TIP_DEST_PRIM_PAG = 1 
	           WHERE IMO_ISN = $cod";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
  function retirarDestaque($cod) {
       $con = new Conexao();
	   $sql = "UPDATE TAB_IMO SET IMO_TIP_DEST = 0
	           WHERE IMO_ISN = $cod AND IMO_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
  function retirarDestaquePrim($cod) {
       $con = new Conexao();
	   $sql = "UPDATE TAB_IMO SET IMO_TIP_DEST_PRIM_PAG = 0
	           WHERE IMO_ISN = $cod AND IMO_TIP_DEST_PRIM_PAG = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
  }
  
  function consultarAllImoveis() {
	   $con = new Conexao(); 
	   $i = 0;
	   $imovel = array();
	   $sql = "SELECT IMO.IMO_ISN, IMO.IMO_DES_BAI, IMO.IMO_DES_TXT_DEST,IMO.IMO_DES_LOC, IMO.IMO_DES_END, IMO.IMO_VAL_ALU, IMO.IMO_VAL_ARE, TIM.TIM_DES 
	   		   FROM TAB_IMO IMO, TAB_TIM TIM 
			   WHERE TIM.TIM_ISN = IMO.TIM_ISN ORDER BY IMO.IMO_ISN";	  
	   
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (@odbc_error($res)) { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
			$imovel[$i]["imo_des_bai"]       = odbc_result($res,"IMO_DES_BAI"); 	   
			$imovel[$i]["imo_des_end"]       = odbc_result($res,"IMO_DES_END"); 	   
			$imovel[$i]["imo_val_alu"]       = odbc_result($res,"IMO_VAL_ALU"); 	   
			$imovel[$i]["imo_val_are"]       = odbc_result($res,"IMO_VAL_ARE");
			$imovel[$i]["imo_des_loc"]       = odbc_result($res,"IMO_DES_LOC");
			$imovel[$i]["imo_des_txt_dest"]  = odbc_result($res,"IMO_DES_TXT_DEST"); 	   
			$imovel[$i]["tim_des"]           = odbc_result($res,"TIM_DES"); 	   
            $i++; 
	   }
	   @odbc_close($conn);		
	   return $imovel;
	}
}
?>