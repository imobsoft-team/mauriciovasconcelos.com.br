<?php
  class Imovel extends Model{	 
	 function Imovel() {
	 	parent::Model();
	 }
	 
   function cadastrarImovel($nome,$fone1,$fone2,$email,$cel,$end,$comp,$numero,$cep,$uf,$bairro,$cidade,$fin,$resumo) {     
		 $sql = "INSERT INTO TAB_CIM (CIM_NOM,CIM_NUM_FON_1,CIM_NUM_FON_2,CIM_NUM_CEL,CIM_MAIL,CIM_DES_LOG,CIM_NUM_END,CIM_DES_COMP,CIM_DES_CEP,CIM_DES_BAI,CIM_DES_CID,CIM_FIN,CIM_RES_IMO,CIM_RES_UF)
		         VALUES('$nome','$fone1','$fone2','$cel','$email','$end','$numero','$comp','$cep','$bairro','$cidade',$fin,'$resumo','$uf')";
	     $res = $this->db->query($sql);
	     if($this->db->affected_rows()>0){ 
		     $msg = "Operacao relizada com sucesso";			  
		 }else {
		  	  $msg = "Operacao não relizada";
		 }
		 return $msg;
	  }
	  
    function cadastrarPretendentePF($pre) {
		$tipoImo            = $pre['tipoImo'];
		$valorImo           = $pre['valorImo'];
		$endImo             = $pre['endImo'];
		$bairroImo          = $pre['bairroImo'];
		$codImo             = $pre['codImo'];
		$nomeInq            = $pre['nomeInq'];
		$estadoCivilInq     = $pre['estadoCivilInq'];
		$cpfInq             = $pre['cpfInq'];
		$numResInq          = $pre['numResInq'];
		$ruaInq             = $pre['ruaInq'];
		$compEndInq         = $pre['compEndInq'];
		$bairroInq          = $pre['bairroInq'];
		$cidadeInq          = $pre['cidadeInq'];
		$fone1Inq           = $pre['fone1Inq'];
		$fone2Inq           = $pre['fone2Inq'];
		$ufInq              = $pre['ufInq'];
		$cepInq             = $pre['cepInq'];
		$celularInq         = $pre['celularInq'];
		$nacionalidadeInq   = $pre['nacionalidadeInq'];
		$naturalidadeInq    = $pre['naturalidadeInq'];
		$datanascInq        = $pre['datanascInq'];		
		$tipoRes            = $pre['tipoImovel'];
		$tempoMoradiaAntInq = $pre['tempoMoradiaAntInq'];
		$rgInq              = $pre['rgInq'];
		$exprgInq           = $pre['exprgInq'];
		$filiacao           = $pre['paiInq']." - ".$pre['maeInq'];
		$sexoInq            = $pre['sexoInq'];
		$pga                = $pre['pgsimInq'];
		$nomepropInq        = $pre['nomepropInq'];
		$enderecopropInq    = $pre['enderecopropInq'];
		$valorImoInq        = $pre['valorImoInq'];
		$fonepropInq        = $pre['fonepropInq'];
		$explicacaoInq      = $pre['explicacaoInq'];
		$profissaoInq       = $pre['profissaoInq'];
		$endTrabInq         = $pre['endTrabInq'];
		$numEndTrabInq      = $pre['numEndTrabInq'];
		$bairroTrabInq      = $pre['bairroTrabInq'];
		$cidadeTrabInq      = $pre['cidadeTrabInq'];
		$cepTrabInq         = $pre['cepTrabInq'];
		$nomeDirSocInq      = $pre['nomeDirSocInq'];
		$nomeFirmaInq       = $pre['nomeFirmaInq'];
		$foneFirmaInq       = $pre['foneFirmaInq'];
		$funcaoInq          = $pre['funcaoInq'];
		$tempoServicoInq    = $pre['tempoServicoInq'];
		$salarioAtualInq    = $pre['salarioAtualInq'];
		if($pre['rendimentoSimInq'] == 1) {
		   $rend = 1;
		}else {
		   $rend = 0;
		}
		$tipoRendimentoInq    = $pre['tipoRendimentoInq'];
		$fontePagadoraInq     = $pre['fontePagadoraInq'];
		$valorRendimentoInq   = $pre['valorRendimentoInq'];
		$endOutRendimentoInq  = $pre['endOutRendimentoInq'];
		$foneOutRendimentoInq = $pre['foneOutRendimentoInq'];
		$nomeConjInq          = $pre['nomeConjInq'];
		$cpfConjInq           = $pre['cpfConjInq'];
		$dtnascConjInq        = $pre['dtnascConjInq'];		
		$rgConjInq            = $pre['rgConjInq'];
		$orgexpConjInq        = $pre['orgexpConjInq'];
		$natConjInq           = $pre['natConjInq'];
		$nacConjInq           = $pre['nacConjInq'];
		$trab                 = $pre['conjTrabSimInq'];
		$nomeFirmaConjInq     = $pre['nomeFirmaConjInq'];
		$foneConjInq          = $pre['foneConjInq'];
		$celularConjInq       = $pre['celularConjInq'];
		$funcaoConjInq        = $pre['funcaoConjInq'];
		$endTrabConjInq       = $pre['endTrabConjInq']; 
		$salarioConjInq       = $pre['salarioConjInq'];
        $tipoConta            = $pre['conta1'];		
        $tipoConta2           = $pre['conta2'];
		
        $numCcPoup1           = $pre['numCcPoup1'];
        $numCcPoup2           = $pre['numCcPoup2'];
        $agencia1             = $pre['agencia1'];
        $agencia2             = $pre['agencia2'];
        $banco1               = $pre['banco1'];
        $banco2               = $pre['banco2'];

		$nomeRefInq           = $pre['nomeRefInq'];
		$foneRefInq           = $pre['foneRefInq'];
		$nomeRef2Inq          = $pre['nomeRef2Inq'];
		$foneRef2Inq          = $pre['foneRef2Inq'];
        $nomeRefComInq        = $pre['nomeRefComInq'];
        $foneRefComInq        = $pre['foneRefComInq'];
		$nomeRefImobInq       = $pre['nomeRefImobInq'];
		$foneRefImobInq       = $pre['foneRefImobInq'];
		$endImoPatrimonioInq  = $pre['endImoPatrimonioInq'];
		$valorImoPatrInq      = round($pre['valorImoPatrInq']);
		$numregmatImoPatrInq  = $pre['numregmatImoPatrInq'];
		$zonaImoPatrInq       = $pre['zonaImoPatrInq'];
		$endImoPatrimonio2Inq = $pre['endImoPatrimonio2Inq'];
		$valorImoPatr2Inq     = round($pre['valorImoPatr2Inq']);
		$numregmatImoPatr2Inq = $pre['numregmatImoPatr2Inq'];
		$zonaImoPatr2Inq      = $pre['zonaImoPatr2Inq'];
		$marcaVeiculoInq      = $pre['marcaVeiculoInq'];
		$placaVeiculoInq      = $pre['placaVeiculoInq'];
		$anoVeiculoInq        = $pre['anoVeiculoInq'];
		$valorVeiculoInq      = round($pre['valorVeiculoInq']);
		$marcaVeiculo2Inq     = $pre['marcaVeiculo2Inq'];
		$placaVeiculo2Inq     = $pre['placaVeiculo2Inq'];
		$anoVeiculo2Inq       = $pre['anoVeiculo2Inq'];
		$valorVeiculo2Inq     = round($pre['valorVeiculo2Inq']);
		$data = date("Y-m-d H:i:s");	    
        $sql = "INSERT INTO TAB_PPC (PPC_DAT, PPC_IMO_COD, PPC_TIP_FIA, PPC_TIP_PES, PPC_NOM, PPC_NUM_CPF_CNPJ, PPC_NUM_FON_1, PPC_NUM_FON_2,PPC_NUM_CEL, PPC_DES_LOG_RES, PPC_DES_NUM_END_RES, PPC_DES_COMP_RES, PPC_DES_BAI_RES, PPC_DES_LOC_RES, PPC_UF_RES,PPC_DES_NUM_CEP_RES, PPC_TMP_DUR_END_RES, PPC_TIP_RES, PPC_VAL_RES, PPC_DES_OUTROS, PPC_FAV_ALU, PPC_NUM_FON_FAV,PPC_DES_END, PPC_TIP_REND, PPC_DES_REND, PPC_DES_REF1, PPC_DES_REF2, PPC_FON_REF1, PPC_FON_REF2,PPC_NOM_REF_COM1, PPC_NUM_FON_REF_COM1, PPC_REF_IMOB, PPC_NUM_FON_REF_IMOB,PPC_TIP_CTA_1, PPC_TIP_CTA_2, PPC_DES_CTA_1, PPC_DES_CTA_2, PPC_COD_AGE_1, PPC_COD_AGE_2, PPC_COD_BCO_1, PPC_COD_BCO_2, PPC_DES_IMO_BEM_1,PPC_VAL_IMO_BEM_1, PPC_MAT_IMO_BEM_1, PPC_ZON_IMO_BEM_1, PPC_DES_IMO_BEM_2, PPC_VAL_IMO_BEM_2, PPC_MAT_IMO_BEM_2,PPC_ZON_IMO_BEM_2, PPC_DES_AUTO_1,PPC_VAL_AUTO_1, PPC_PLACA_AUTO_1, PPC_ANO_AUTO_1,PPC_DES_AUTO_2,PPC_VAL_AUTO_2, PPC_PLACA_AUTO_2, PPC_ANO_AUTO_2,PPC_FONT_PAG_REND,PPC_VAL_REND,PPC_END_OUT_REND,PPC_FONE_OUT_REND) VALUES ('$data','$codImo',0,1,'$nomeInq','$cpfInq','$fone1Inq','$fone2Inq','$celularInq','$ruaInq',$numResInq,'$compEndInq','$bairroInq','$cidadeInq','$ufInq','$cepInq','$tempoMoradiaAntInq',$tipoRes,'$valorImoInq','$explicacaoInq','$nomepropInq','$fonepropInq','$enderecopropInq',$rend,'$tipoRendimentoInq','$nomeRefInq','$nomeRef2Inq','$foneRefInq','$foneRef2Inq','$nomeRefComInq','$foneRefComInq','$nomeRefImobInq ','$foneRefImobInq ',$tipoConta,$tipoConta2,'$numCcPoup1','$numCcPoup2','$agencia1','$agencia2','$banco1','$banco2','$endImoPatrimonioInq',$valorImoPatrInq,'$numregmatImoPatrInq','$zonaImoPatrInq','$endImoPatrimonio2Inq',$valorImoPatr2Inq,'$numregmatImoPatr2Inq','$zonaImoPatr2Inq','$marcaVeiculoInq',$valorVeiculoInq,'$placaVeiculoInq','$anoVeiculoInq','$marcaVeiculo2Inq',$valorVeiculo2Inq,'$placaVeiculo2Inq','$anoVeiculo2Inq','$fontePagadoraInq','$valorRendimentoInq','$endOutRendimentoInq','$foneOutRendimentoInq')";				
	    $res = $this->db->query($sql);	
		$codigo = $this->db->insert_id();
	    if(!$codigo>0){
		   return 0;
		}
       $sql3 = "INSERT INTO TAB_CPF (PPC_ISN, CPF_SEXO, CPF_EST_CIVIL, CPF_FILIACAO, CPF_NUM_RG, CPF_ORG_EXP, CPF_DES_NAC,CPF_DES_NAT, CPF_DAT_NASC, CPF_DES_PRF, CPF_DES_END_TRAB, CPF_DES_NUM_END_TRAB, CPF_DES_BAI_TRAB, CPF_DES_LOC_TRAB,CPF_DES_SOC, CPF_NOM_FIR_TRAB, CPF_NUM_FON1_TRAB, CPF_DES_FUN_TRAB, CPF_TMP_TRAB, CPF_DES_SAL, CPF_TIP_REND,CPF_NOM_CJG, CPF_DAT_NAS_CJG, CPF_NUM_RG_CJG, CPF_ORG_EXP_CJG, CPF_PRF_CJG, CPF_NUM_CPF_CJG, CPF_DES_NAT_CJG,CPF_DES_NAS_CJG,CPF_TRAB_CJG,CPF_FIRM_CJG,CPF_FONE_CJG,CPF_CEL_CJG,CPF_FUNC_CJG,CPF_SAL_CJG) VALUES($codigo,$sexoInq,$estadoCivilInq,'$filiacao','$rgInq','$exprgInq','$nacionalidadeInq','$naturalidadeInq',	'$datanascInq','$profissaoInq','$endTrabInq','$numEndTrabInq','$bairroTrabInq','$cidadeTrabInq','$nomeDirSocInq','$nomeFirmaInq','$foneFirmaInq','$funcaoInq','$tempoServicoInq','$salarioAtualInq',1$rend,'$nomeConjInq','$dtnascConjInq','$rgConjInq','$orgexpConjInq','$funcaoConjInq','$cpfConjInq','$natConjInq','$nacConjInq','$trab','$nomeFirmaConjInq','$foneConjInq','$celularConjInq','$funcaoConjInq','$salarioConjInq')";
		$res3 = $this->db->query($sql3);
	    if(!$this->db->affected_rows()>0){
		   return 0;
		}
	    return $codigo;
	}  
     function cadastrarPretendentePJR($pre) {
		$codImo         = $pre['codigo'];
		$tipoImo        = $pre['tipoImo'];
		$valorImo       = $pre['valorImo'];
		$endereco       = $pre['endEmp'];
		$bairro         = $pre['bairroEmp'];
		$nomeEmp        = $pre['nomeEmp'];
		$fone1          = $pre['fone1'];
		$fone2          = $pre['fone2'];
		$emailEmp       = $pre['emailEmp'];
		$dataConst      = $pre['dataConst'];
		if($dataConst){
			$dataConst = explode("/",$dataConst);
			$dataConst = $dataConst[2]."-".$dataConst[1]."-".$dataConst[0];
		}		
		$cgccnpj        = $pre['cgccnpj'];
		$inscEst        = $pre['inscEst'];
		$numregjucec    = $pre['numregjucec'];
		$tiposociedade  = $pre['tiposociedade'];
		$setorAtiv      = $pre['setorAtiv']; 
		$endEmp         = $pre['endEmp'];
		$numEndEmp      = $pre['numEndEmp'];
		$bairroEmp      = $pre['bairroEmp'];
		$cidadeEmp      = $pre['cidadeEmp'];
		$ufEmp          = $pre['ufEmp'];
		$cepEmp         = $pre['cepEmp'];
		$socioEmp       = $pre['socioEmp'];
		$telResid       = $pre['telResid'];
		$celular        = $pre['celular'];
		$email          = $pre['emailEmp'];
		$datanasc       = $pre['datanasc'];
		$rg             = $pre['rg'];
		$rgExp          = $pre['rgExp'];
		$cpfSoc         = $pre['cpfSoc'];
		$naturalidade   = $pre['naturalidade'];
		$nacionalidade  = $pre['nacionalidade'];
		$tempoResidAtual = $pre['tempoResidAtual'];
		$endAtual        = $pre['endAtual'];
		$estadocivilSoc  = $pre['estadocivilSoc'];
		$bairroSoc       = $pre['bairroSoc'];
		$cidadeSoc       = $pre['cidadeSoc'];
		$ufSoc           = $pre['ufSoc'];
		$cepSoc          = $pre['cepSoc'];
		$tipoImovel      = $pre['tipoImovel'];
		$valorImoSoc     = round($pre['valorImoSoc']);
		$nomeprop        = $pre['nomeprop'];
		$enderecoprop    = $pre['enderecoprop'];
		$foneprop        = $pre['foneprop'];
		$explicacao      = $pre['explicacao'];
		$outrosRend      = "";
		$rend            = $pre['outRendSim'];
		$fontePag        = $pre['fontePag'];
		$valorOutRend    = round($pre['valorOutRend']);
		$nomeRefPes1     = $pre['nomeRefPes1'];
		$foneRefPes1     = $pre['foneRefPes1'];
		$nomeRefPes2     = $pre['nomeRefPes2'];  
		$foneRefPes2     = $pre['foneRefPes2'];
		$nomeRefCom      = $pre['nomeRefCom'];
		$foneRefCom      = $pre['foneRefCom'];
		$nomeRefImob     = $pre['nomeRefImob'];
		$foneRefImob     = $pre['foneRefImob'];
		$nomeRefEmpPes1  = $pre['nomeRefEmpPes1'];
		$foneRefEmpPes1  = $pre['foneRefEmpPes1'];
		$nomeRefEmpPes2  = $pre['nomeRefEmpPes2'];  
		$foneRefEmpPes2  = $pre['foneRefEmpPes2'];
		$nomeRefEmpCom1  = $pre['nomeRefEmpCom1'];
		$foneRefEmpCom1  = $pre['foneRefEmpCom1'];
		$nomeRefEmpCom2  = $pre['nomeRefEmpCom2'];
		$foneRefEmpCom2  = $pre['foneRefEmpCom2'];
		$conta           = $pre['conta'];
		$poupanca        = "";	
		$tipoConta       = $pre['conta'];
		$numCcPoup       = $pre['numCcPoup'];
		$agencia         = $pre['agencia'];
		$banco           = $pre['banco'];
		$tipoConta2      = $pre['contaEmp'];
		$contaPoupEmp    = $pre['contaPoupEmp'];
		$agenciaEmp      = $pre['agenciaEmp'];
		$bancoEmp        = $pre['bancoEmp'];
		$endBensImo      = $pre['endBensImo'];
		$valorBensImo    = round($pre['valorBensImo']);
		$matBensImo      = $pre['matBensImo'];
		$zonaBensImo     = $pre['zonaBensImo'];
		$endBensImo2     = $pre['endBensImo2'];
		$valorBensImo2   = round($pre['valorBensImo2']);
		$matBensImo2     = $pre['matBensImo2'];
		$zonaBensImo2    = $pre['zonaBensImo2'];
		$marcaBensVei1   = $pre['marcaBensVei1'];
		$valorBensVei1   = round($pre['valorBensVei1']);
		$anoBensVei1     = $pre['anoBensVei1'];
		$placaBensVei1   = $pre['placaBensVei1'];
		$marcaBensVei2   = $pre['marcaBensVei2'];
		$valorBensVei2   = round($pre['valorBensVei2']);
		$anoBensVei2     = $pre['anoBensVei2'];
		$placaBensVei2   = $pre['placaBensVei2'];
		$outrosBens      = $pre['outrosBens'];
		$valorOutros     = $pre['valorOutros'];
		$outrosRend      = "";
		$data = date("Y-m-d H:i:s");	    
        $sql = "INSERT INTO TAB_PPC (PPC_DAT, PPC_IMO_COD, PPC_TIP_FIA, PPC_TIP_PES, PPC_NOM, PPC_NUM_CPF_CNPJ, PPC_NUM_FON_1, PPC_NUM_FON_2,PPC_NUM_CEL, PPC_DES_LOG_RES, PPC_DES_NUM_END_RES, PPC_DES_COMP_RES, PPC_DES_BAI_RES, PPC_DES_LOC_RES, PPC_UF_RES,PPC_DES_NUM_CEP_RES, PPC_TMP_DUR_END_RES, PPC_TIP_RES, PPC_VAL_RES, PPC_DES_OUTROS, PPC_FAV_ALU, PPC_NUM_FON_FAV,PPC_DES_END, PPC_TIP_REND, PPC_DES_REND, PPC_DES_REF1, PPC_DES_REF2, PPC_FON_REF1, PPC_FON_REF2,PPC_NOM_REF_COM1, PPC_NUM_FON_REF_COM1, PPC_REF_IMOB, PPC_NUM_FON_REF_IMOB,PPC_TIP_CTA_1, PPC_TIP_CTA_2, PPC_DES_CTA_1, PPC_DES_CTA_2, PPC_COD_AGE_1, PPC_COD_AGE_2, PPC_COD_BCO_1, PPC_COD_BCO_2,PPC_DES_IMO_BEM_1,PPC_VAL_IMO_BEM_1, PPC_MAT_IMO_BEM_1, PPC_ZON_IMO_BEM_1, PPC_DES_IMO_BEM_2, PPC_VAL_IMO_BEM_2, PPC_MAT_IMO_BEM_2,PPC_ZON_IMO_BEM_2, PPC_DES_AUTO_1,PPC_VAL_AUTO_1, PPC_PLACA_AUTO_1, PPC_ANO_AUTO_1,PPC_DES_AUTO_2,PPC_VAL_AUTO_2, PPC_PLACA_AUTO_2, PPC_ANO_AUTO_2)VALUES ('$data','$codImo',0,2,'$nomeEmp','$cgccnpj','$fone1','$fone2','$celular','$endEmp',$numEndEmp,'','$bairroEmp','$cidadeEmp','$ufEmp','$cepEmp','$tempoResidAtual',$tipoImovel,'$valorImoSoc','$explicacao','$nomeprop','$foneprop','$enderecoprop',$rend,'$fontePag','$nomeRefPes1','$nomeRefPes2','$foneRefPes1','$foneRefPes2','$nomeRefCom','$foneRefCom','$nomeRefImob','$foneRefImob',$tipoConta,$tipoConta2,'$numCcPoup','$contaPoupEmp','$agencia','$agenciaEmp','$banco','$bancoEmp','$endBensImo',$valorBensImo,'$matBensImo','$zonaBensImo','$endBensImo2',$valorBensImo2,'$matBensImo2','$zonaBensImo','$marcaBensVei1',$valorBensVei1,'$placaBensVei1','$anoBensVei1','$marcaBensVei2',$valorBensVei2,'$placaBensVei2','$anoBensVei2')";
	    $res = $this->db->query($sql);	
		$codigo = $this->db->insert_id();
	    if(!$codigo>0){
		   return 0;
		}		
		$sql3 = "INSERT INTO TAB_CPJ (PPC_ISN,CPJ_DAT_CONST,CPJ_INS_EST,CPJ_NUM_REG_JUC,CPJ_TIP_SOC,CPJ_SET_ATV_COM,CPJ_NOM_SOC_EMP,CPJ_TEL_RES_SOC,CPJ_CEL_SOC,CPJ_DES_MAIL,CPJ_DAT_NAS,CPJ_NUM_RG,CPJ_ORG_EXP,CPJ_NUM_CPF,CPJ_NAT_SOC,CPJ_NAC_SOC,CPJ_EST_CIVIL,CPJ_END_SOC,CPJ_BAI_SOC,CPJ_DES_LOC,CPJ_DES_UF,CPJ_NUM_CEP,CPJ_TMP_RES_AUT,CPJ_TIP_IMO,CPJ_VAL_RES,CPJ_OUT_REND,CPJ_FNT_APOS)VALUES($codigo,'$dataConst','$inscEst','$numregjucec','$tiposociedade','$setorAtiv','$socioEmp','$telResid','$celular','$email','$datanasc','$rg','$rgExp','$cpfSoc','$naturalidade','$nacionalidade',$estadocivilSoc,'$endAtual','$bairroSoc','$cidadeSoc','$ufSoc','$cepSoc','$tempoResidAtual',$tipoImovel,$valorImoSoc,'$fontePag','$fontePag')";
	    $res3 = $this->db->query($sql3);		
	    if(!$this->db->affected_rows()>0){
			return 0;
		}	   		
	    return $codigo;
	}  
    function cadastrarFiador($pre) {
		$tipoImo            = $pre['tipoImo'];
		$valorImo           = $pre['valorImo'];
		$endImo             = $pre['endImo'];
		$bairroImo          = $pre['bairroImo'];
		$codImo             = $pre['codImo'];
		$nomeInq            = $pre['nomeInq'];
		$estadoCivilInq     = $pre['estadoCivilInq'];
		$cpfInq             = $pre['cpfInq'];
		$numResInq          = $pre['numResInq'];
		$ruaInq             = $pre['ruaInq'];
		$compEndInq         = $pre['compEndInq'];
		$bairroInq          = $pre['bairroInq'];
		$cidadeInq          = $pre['cidadeInq'];
		$fone1Inq           = $pre['fone1Inq'];
		$fone2Inq           = $pre['fone2Inq'];
		$ufInq              = $pre['ufInq'];
		$cepInq             = $pre['cepInq'];
		$celularInq         = $pre['celularInq'];
		$nacionalidadeInq   = $pre['nacionalidadeInq'];
		$naturalidadeInq    = $pre['naturalidadeInq'];
		$datanascInq        = $pre['datanascInq'];		
		$tipoRes            = $pre['tipoImovel'];
		$tempoMoradiaAntInq = $pre['tempoMoradiaAntInq'];
		$rgInq              = $pre['rgInq'];
		$exprgInq           = $pre['exprgInq'];
		$filiacao           = $pre['paiInq']." - ".$pre['maeInq'];
		$sexoInq            = $pre['sexoInq'];
		$pga                = $pre['pgsimInq'];
		$nomepropInq        = $pre['nomepropInq'];
		$enderecopropInq    = $pre['enderecopropInq'];
		$valorImoInq        = $pre['valorImoInq'];
		$fonepropInq        = $pre['fonepropInq'];
		$explicacaoInq      = $pre['explicacaoInq'];
		$profissaoInq       = $pre['profissaoInq'];
		$endTrabInq         = $pre['endTrabInq'];
		$numEndTrabInq      = $pre['numEndTrabInq'];
		$bairroTrabInq      = $pre['bairroTrabInq'];
		$cidadeTrabInq      = $pre['cidadeTrabInq'];
		$cepTrabInq         = $pre['cepTrabInq'];
		$nomeDirSocInq      = $pre['nomeDirSocInq'];
		$nomeFirmaInq       = $pre['nomeFirmaInq'];
		$foneFirmaInq       = $pre['foneFirmaInq'];
		$funcaoInq          = $pre['funcaoInq'];
		$tempoServicoInq    = $pre['tempoServicoInq'];
		$salarioAtualInq    = $pre['salarioAtualInq'];
		if($pre['rendimentoSimInq'] == 1) {
		   $rend = 1;
		}else {
		   $rend = 0;
		}
		$tipoRendimentoInq    = $pre['tipoRendimentoInq'];
		$fontePagadoraInq     = $pre['fontePagadoraInq'];
		$valorRendimentoInq   = $pre['valorRendimentoInq'];
		$endOutRendimentoInq  = $pre['endOutRendimentoInq'];
		$foneOutRendimentoInq = $pre['foneOutRendimentoInq'];
		$nomeConjInq          = $pre['nomeConjInq'];
		$cpfConjInq           = $pre['cpfConjInq'];
		$dtnascConjInq        = $pre['dtnascConjInq'];		
		$rgConjInq            = $pre['rgConjInq'];
		$orgexpConjInq        = $pre['orgexpConjInq'];
		$natConjInq           = $pre['natConjInq'];
		$nacConjInq           = $pre['nacConjInq'];
		$trab                 = $pre['conjTrabSimInq'];
		$nomeFirmaConjInq     = $pre['nomeFirmaConjInq'];
		$foneConjInq          = $pre['foneConjInq'];
		$celularConjInq       = $pre['celularConjInq'];
		$funcaoConjInq        = $pre['funcaoConjInq'];
		$endTrabConjInq       = $pre['endTrabConjInq']; 
		$salarioConjInq       = $pre['salarioConjInq'];
        $tipoConta            = $pre['conta1'];		
        $tipoConta2           = $pre['conta2'];
		
        $numCcPoup1           = $pre['numCcPoup1'];
        $numCcPoup2           = $pre['numCcPoup2'];
        $agencia1             = $pre['agencia1'];
        $agencia2             = $pre['agencia2'];
        $banco1               = $pre['banco1'];
        $banco2               = $pre['banco2'];

		$nomeRefInq           = $pre['nomeRefInq'];
		$foneRefInq           = $pre['foneRefInq'];
		$nomeRef2Inq          = $pre['nomeRef2Inq'];
		$foneRef2Inq          = $pre['foneRef2Inq'];
        $nomeRefComInq        = $pre['nomeRefComInq'];
        $foneRefComInq        = $pre['foneRefComInq'];
		$nomeRefImobInq       = $pre['nomeRefImobInq'];
		$foneRefImobInq       = $pre['foneRefImobInq'];
		$endImoPatrimonioInq  = $pre['endImoPatrimonioInq'];
		$valorImoPatrInq      = round($pre['valorImoPatrInq']);
		$numregmatImoPatrInq  = $pre['numregmatImoPatrInq'];
		$zonaImoPatrInq       = $pre['zonaImoPatrInq'];
		$endImoPatrimonio2Inq = $pre['endImoPatrimonio2Inq'];
		$valorImoPatr2Inq     = round($pre['valorImoPatr2Inq']);
		$numregmatImoPatr2Inq = $pre['numregmatImoPatr2Inq'];
		$zonaImoPatr2Inq      = $pre['zonaImoPatr2Inq'];
		$marcaVeiculoInq      = $pre['marcaVeiculoInq'];
		$placaVeiculoInq      = $pre['placaVeiculoInq'];
		$anoVeiculoInq        = $pre['anoVeiculoInq'];
		$valorVeiculoInq      = round($pre['valorVeiculoInq']);
		$marcaVeiculo2Inq     = $pre['marcaVeiculo2Inq'];
		$placaVeiculo2Inq     = $pre['placaVeiculo2Inq'];
		$anoVeiculo2Inq       = $pre['anoVeiculo2Inq'];
		$valorVeiculo2Inq     = round($pre['valorVeiculo2Inq']);
		$vinculo              = $pre['ppc'];
		$data = date("Y-m-d H:i:s");
        $sql = "INSERT INTO TAB_PPC (PPC_DAT, PPC_IMO_COD, PPC_TIP_FIA, PPC_TIP_PES, PPC_NOM, PPC_NUM_CPF_CNPJ, PPC_NUM_FON_1, PPC_NUM_FON_2,
		        PPC_NUM_CEL, PPC_DES_LOG_RES, PPC_DES_NUM_END_RES, PPC_DES_COMP_RES, PPC_DES_BAI_RES, PPC_DES_LOC_RES, PPC_UF_RES,
				PPC_DES_NUM_CEP_RES, PPC_TMP_DUR_END_RES, PPC_TIP_RES, PPC_VAL_RES, PPC_DES_OUTROS, PPC_FAV_ALU, PPC_NUM_FON_FAV,
				PPC_DES_END, PPC_TIP_REND, PPC_DES_REND, PPC_DES_REF1, PPC_DES_REF2, PPC_FON_REF1, PPC_FON_REF2,
                PPC_NOM_REF_COM1, PPC_NUM_FON_REF_COM1, PPC_REF_IMOB, PPC_NUM_FON_REF_IMOB,
				PPC_TIP_CTA_1, PPC_TIP_CTA_2, PPC_DES_CTA_1, PPC_DES_CTA_2, PPC_COD_AGE_1, PPC_COD_AGE_2, PPC_COD_BCO_1, PPC_COD_BCO_2, PPC_DES_IMO_BEM_1,
				PPC_VAL_IMO_BEM_1, PPC_MAT_IMO_BEM_1, PPC_ZON_IMO_BEM_1, PPC_DES_IMO_BEM_2, PPC_VAL_IMO_BEM_2, PPC_MAT_IMO_BEM_2,
				PPC_ZON_IMO_BEM_2, PPC_DES_AUTO_1,PPC_VAL_AUTO_1, PPC_PLACA_AUTO_1, PPC_ANO_AUTO_1,
				PPC_DES_AUTO_2,PPC_VAL_AUTO_2, PPC_PLACA_AUTO_2, PPC_ANO_AUTO_2,PPC_FONT_PAG_REND,PPC_VAL_REND,PPC_END_OUT_REND,PPC_FONE_OUT_REND,PPC_VINCULO)
				VALUES ('$data','$codImo',1,1,'$nomeInq','$cpfInq','$fone1Inq','$fone2Inq','$celularInq','$ruaInq',$numResInq,
				'$compEndInq','$bairroInq','$cidadeInq','$ufInq',
			    '$cepInq','$tempoMoradiaAntInq',$tipoRes,'$valorImoInq','$explicacaoInq','$nomepropInq','$fonepropInq',
				'$enderecopropInq',$rend,'$tipoRendimentoInq','$nomeRefInq','$nomeRef2Inq','$foneRefInq','$foneRef2Inq',
				'$nomeRefComInq','$foneRefComInq','$nomeRefImobInq ','$foneRefImobInq ',$tipoConta,$tipoConta2,'$numCcPoup1',
				'$numCcPoup2','$agencia1','$agencia2','$banco1','$banco2','$endImoPatrimonioInq',$valorImoPatrInq,'$numregmatImoPatrInq',
				'$zonaImoPatrInq','$endImoPatrimonio2Inq',$valorImoPatr2Inq,'$numregmatImoPatr2Inq','$zonaImoPatr2Inq','$marcaVeiculoInq',
				$valorVeiculoInq,'$placaVeiculoInq','$anoVeiculoInq','$marcaVeiculo2Inq',$valorVeiculo2Inq,'$placaVeiculo2Inq','$anoVeiculo2Inq','$fontePagadoraInq','$valorRendimentoInq','$endOutRendimentoInq','$foneOutRendimentoInq','$vinculo')";
				
	    $res = $this->db->query($sql);	
		$codigo = $this->db->insert_id();
	    if(!$codigo>0){
		   return 0;
		}				
       $sql3 = "INSERT INTO TAB_CPF (PPC_ISN, CPF_SEXO, CPF_EST_CIVIL, CPF_FILIACAO, CPF_NUM_RG, CPF_ORG_EXP, CPF_DES_NAC,
	           CPF_DES_NAT, CPF_DAT_NASC, CPF_DES_PRF, CPF_DES_END_TRAB, CPF_DES_NUM_END_TRAB, CPF_DES_BAI_TRAB, CPF_DES_LOC_TRAB,
			   CPF_DES_SOC, CPF_NOM_FIR_TRAB, CPF_NUM_FON1_TRAB, CPF_DES_FUN_TRAB, CPF_TMP_TRAB, CPF_DES_SAL, CPF_TIP_REND,
			   CPF_NOM_CJG, CPF_DAT_NAS_CJG, CPF_NUM_RG_CJG, CPF_ORG_EXP_CJG, CPF_PRF_CJG, CPF_NUM_CPF_CJG, CPF_DES_NAT_CJG, 
			   CPF_DES_NAS_CJG,CPF_TRAB_CJG,CPF_FIRM_CJG,CPF_FONE_CJG,CPF_CEL_CJG,CPF_FUNC_CJG,CPF_SAL_CJG)
			   VALUES($codigo,$sexoInq,$estadoCivilInq,'$filiacao','$rgInq','$exprgInq','$nacionalidadeInq','$naturalidadeInq',
			   '$datanascInq','$profissaoInq','$endTrabInq','$numEndTrabInq','$bairroTrabInq','$cidadeTrabInq','$nomeDirSocInq',
			   '$nomeFirmaInq','$foneFirmaInq','$funcaoInq','$tempoServicoInq','$salarioAtualInq',1$rend,'$nomeConjInq','$dtnascConjInq',
			   '$rgConjInq','$orgexpConjInq','$funcaoConjInq','$cpfConjInq','$natConjInq','$nacConjInq','$trab','$nomeFirmaConjInq','$foneConjInq','$celularConjInq','$funcaoConjInq','$salarioConjInq')";
		$res3 = $this->db->query($sql3);
	    if(!$this->db->affected_rows()>0){
		   return 0;
		}
	    return $codigo;
	}
	
	function buscarProposta($cod) {		 
		 $sql = "SELECT * FROM TAB_PPC WHERE PPC_ISN = $cod";
		 $res = $this->db->query($sql);
		 $titular="";
		 if($res->num_rows() > 0){
			 foreach($res->result() as $array){
				 $titular["id"]                = $array->PPC_ISN;
				 $titular["data"]              = $array->PPC_DAT;
				 $titular["nome"]              = $array->PPC_NOM;
				 $titular["tip_pes"]           = $array->PPC_TIP_PES;
				 $titular["imo_cod"]           = $array->PPC_IMO_COD;
				 $titular["tip_fia"]           = $array->PPC_TIP_FIA;
				 $titular["cpf_cnpj"]          = $array->PPC_NUM_CPF_CNPJ;
				 $titular["num_fon_1"]         = $array->PPC_NUM_FON_1;
				 $titular["num_fon_2"]         = $array->PPC_NUM_FON_2;
				 $titular["num_cel"]           = $array->PPC_NUM_CEL;
				 $titular["des_log_res"]       = $array->PPC_DES_LOG_RES;
				 $titular["des_num_end_res"]   = $array->PPC_DES_NUM_END_RES;
				 $titular["des_comp_res"]      = $array->PPC_DES_COMP_RES;
				 $titular["des_bai_res"]       = $array->PPC_DES_BAI_RES;
				 $titular["des_loc_res"]       = $array->PPC_DES_LOC_RES;
				 $titular["uf_res"]            = $array->PPC_UF_RES;
				 $titular["des_num_cep_res"]   = $array->PPC_DES_NUM_CEP_RES;
				 $titular["tpm_dur_end_res"]   = $array->PPC_TMP_DUR_END_RES;
				 $titular["tip_res"]           = $array->PPC_TIP_RES;
				 $titular["val_res"]           = $array->PPC_VAL_RES;
				 $titular["des_outros"]        = $array->PPC_DES_OUTROS;
				 $titular["fav_alu"]           = $array->PPC_FAV_ALU;
				 $titular["num_fon_fav"]       = $array->PPC_NUM_FON_FAV;
				 $titular["des_end"]           = $array->PPC_DES_END;
				 $titular["tip_rend"]          = $array->PPC_TIP_REND;
				 $titular["des_rend"]          = $array->PPC_DES_REND;
				 $titular["des_ref1"]          = $array->PPC_DES_REF1;
				 $titular["des_ref2"]          = $array->PPC_DES_REF2;
				 $titular["fon_ref1"]          = $array->PPC_FON_REF1;
				 $titular["fon_ref2"]          = $array->PPC_FON_REF2;
				 $titular["nom_ref_com1"]      = $array->PPC_NOM_REF_COM1;
				 $titular["num_fon_ref_com1"]  = $array->PPC_NUM_FON_REF_COM1;
				 $titular["ref_imob"]          = $array->PPC_REF_IMOB;
				 $titular["num_fon_ref_imob"]  = $array->PPC_NUM_FON_REF_IMOB;
				 $titular["tip_cta_1"]         = $array->PPC_TIP_CTA_1;
				 $titular["tip_cta_2"]         = $array->PPC_TIP_CTA_2;
				 $titular["des_cta_1"]         = $array->PPC_DES_CTA_1;
				 $titular["des_cta_2"]         = $array->PPC_DES_CTA_2;
				 $titular["cod_age_1"]         = $array->PPC_COD_AGE_1;
				 $titular["cod_age_2"]         = $array->PPC_COD_AGE_2;
				 $titular["cod_bco_1"]         = $array->PPC_COD_BCO_1;
				 $titular["cod_bco_2"]         = $array->PPC_COD_BCO_2;
				 $titular["des_imo_bem_1"]     = $array->PPC_DES_IMO_BEM_1;
				 $titular["val_imo_bem_1"]     = $array->PPC_VAL_IMO_BEM_1;
				 $titular["mat_imo_bem_1"]     = $array->PPC_MAT_IMO_BEM_1;
				 $titular["zon_imo_bem_1"]     = $array->PPC_ZON_IMO_BEM_1;
				 $titular["des_imo_bem_2"]     = $array->PPC_DES_IMO_BEM_2;
				 $titular["val_imo_bem_2"]     = $array->PPC_VAL_IMO_BEM_2;
				 $titular["mat_imo_bem_2"]     = $array->PPC_MAT_IMO_BEM_2;
				 $titular["zon_imo_bem_2"]     = $array->PPC_ZON_IMO_BEM_2;
				 $titular["des_auto_1"]        = $array->PPC_DES_AUTO_1;
				 $titular["val_auto_1"]        = $array->PPC_VAL_AUTO_1;
				 $titular["placa_auto_1"]      = $array->PPC_PLACA_AUTO_1;
				 $titular["ano_auto_1"]        = $array->PPC_ANO_AUTO_1;
				 $titular["des_auto_2"]        = $array->PPC_DES_AUTO_2;
				 $titular["val_auto_2"]        = $array->PPC_VAL_AUTO_2;
				 $titular["placa_auto_2"]      = $array->PPC_PLACA_AUTO_2;
				 $titular["ano_auto_2"]        = $array->PPC_ANO_AUTO_2;
			 }
		 
		}	
		return $titular;
	}
	  
  }
?>