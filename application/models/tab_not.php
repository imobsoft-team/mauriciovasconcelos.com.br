<?php
class Tab_not extends Model{
	function Tab_not(){
		parent::Model();
	}
	
	function getNoticias($maximo,$inicio,$busca,$ordem='NOT_ISN ASC') {
			$sql = "SELECT NOT_ISN,NOT_TITULO,DAY(NOT_DAT_NOT) AS DIA, MONTH(NOT_DAT_NOT) AS MES, YEAR(NOT_DAT_NOT) AS ANO,DAY(NOT_DAT_VAL) AS DIAV, MONTH(NOT_DAT_VAL) AS MESV, YEAR(NOT_DAT_VAL) AS ANOV FROM tab_not";
		    if($busca){
				$sql.=" WHERE NOT_TITULO LIKE '%$busca%' ";
			}
			$sql.=" ORDER BY $ordem";
			$sql.=" LIMIT $inicio,$maximo";
			$not="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				
				foreach($res->result() as $array){
				   $not[$i]["id"]    =  $array->NOT_ISN; 
				   $not[$i]["tit"]   =  $array->NOT_TITULO; 
				  
				
				   $not[$i]["dia"]   =  str_pad($array->DIA,2,"0",STR_PAD_LEFT); 
				   $not[$i]["mes"]   =  str_pad($array->MES,2,"0",STR_PAD_LEFT);
				   $not[$i]["ano"]   =  str_pad($array->ANO,4,"0",STR_PAD_LEFT);
				   $not[$i]["diav"]  =  str_pad($array->DIAV,2,"0",STR_PAD_LEFT);
				   $not[$i]["mesv"]  =  str_pad($array->MESV,2,"0",STR_PAD_LEFT);
				   $not[$i]["anov"]  =  str_pad($array->ANOV,4,"0",STR_PAD_LEFT);
				   $i++;
				}
			}
			return $not;
	}
	
	function contaRegistros($busca){
		$sql = "SELECT COUNT(NOT_ISN) AS TOTAL FROM tab_not";
		if($busca){
			$sql.=" WHERE NOT_TITULO LIKE '%$busca%' ";
		}
		$res = $this->db->query($sql);
		foreach($res->result() as $array){
			$total = $array->TOTAL;
		}
		return $total;
	}
	
	function cadastrarNoticia($titulo,$texto,$fonte,$data,$datval) {	   
	   $sql = "INSERT INTO tab_not (NOT_TITULO,NOT_DES,NOT_FONTE,NOT_DAT_NOT,NOT_DAT_VAL) VALUES ('$titulo','".$texto."','$fonte','$data','$datval')";	  
	   $res = $this->db->query($sql);	
	   $codigo = $this->db->insert_id();	 
	   if ($codigo>0) {		   
			return $codigo;
	   }else {
		   return false;
	   } 	
	}
	
	function buscarNoticia($cod) {     
	  $sql  = "SELECT * FROM tab_not WHERE NOT_ISN = $cod";
	  $res = $this->db->query($sql);
	  $not="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			 $not["id"]     =  $array->NOT_ISN;
			 $not["tit"]    =  $array->NOT_TITULO;
			 $not["texto"]  =  $array->NOT_DES;
			 $not["fonte"]  =  $array->NOT_FONTE;
			 if($array->NOT_DAT_VAL){
			 	$venc   		=  explode('-',$array->NOT_DAT_VAL);
			 	$not["venc"] = $venc[2].'/'.$venc[1].'/'.$venc[0];
			 }else {
			 	$not["venc"] = "0000-00-00";
			 } 
		  }
	  }
	  return $not;
	}
	
	function alterarNoticia($titulo,$texto,$fonte,$venc,$codigo) {     
	   $sql  = "UPDATE tab_not SET NOT_TITULO = '$titulo', NOT_DES = '$texto', NOT_FONTE = '$fonte', NOT_DAT_VAL = '$venc' WHERE NOT_ISN = $codigo";	  
	   $this->db->query($sql);
	   return $codigo;
	}
	
	function excluirItens($cods) { 	
		$sql  = "DELETE FROM tab_not WHERE NOT_ISN IN ($cods)";
		$this->db->query($sql);
	} 

}
?>