<?php
session_start();
$param=$_SESSION['param'];
$total=$_SESSION['total'];
$m=$_SESSION['m'];
$tipo=$_SESSION['tipo'];
header("Content-Type:application/xml; charset=UTF-8");
?>
<?php if($tipo==1){ ?>
<chart caption='Os dez tipos mais buscados no Site' xAxisName='Total de <?=$total?> buscas' yAxisName='<?='Buscas no m�s de '.$mes[$mat]?>' showValues='0' decimals='0' formatNumberScale='0'>
<?php for($x=0;$x<10;$x++){ if(!empty($m[$x]['tipo'])){ ?>	
     <set label='<?=$m[$x]['tipo']?>' value='<?=$m[$x]['acs']?>' />
<?php } } ?>
</chart>
<?php }else if($tipo==2){ ?>
<chart palette='4' decimals='0' enableSmartLabels='1' enableRotation='0' bgColor='99CCFF,FFFFFF' bgAlpha='40,100' bgRatio='0,100' bgAngle='360' showBorder='1' startingAngle='70' >
<?php for($x=0;$x<10;$x++){ if(!empty($m[$x]['tipo'])){ ?>	
     <set label='<?=$m[$x]['tipo']?>' value='<?=$m[$x]['acs']?>' />
<?php } } ?>            
</chart>
<?php }else { ?>
<chart caption='Os dez tipos mais buscados no Site' subcaption='Total de <?=$total?> buscas' xAxisName='<?='Buscas no m�s de '.$mes[$mat]?>' yAxisName='<?='Buscas no m�s de '.$mes[$mat]?>' yAxisMinValue='15000'  numberPrefix='' showValues='0' alternateHGridColor='FCB541' alternateHGridAlpha='20' divLineColor='FCB541' divLineAlpha='50' canvasBorderColor='666666' baseFontColor='666666' lineColor='FCB541'>
	<?php for($x=0;$x<10;$x++){ if(!empty($m[$x]['tipo'])){ ?>	
     <set label='<?=$m[$x]['tipo']?>' value='<?=$m[$x]['acs']?>' />
<?php } } ?>

	<styles>
		<definition>
			<style name='Anim1' type='animation' param='_xscale' start='0' duration='1' />
			<style name='Anim2' type='animation' param='_alpha' start='0' duration='0.6' />
			<style name='DataShadow' type='Shadow' alpha='40'/>
		</definition>
		<application>
			<apply toObject='DIVLINES' styles='Anim1' />
			<apply toObject='HGRID' styles='Anim2' />
			<apply toObject='DATALABELS' styles='DataShadow,Anim2' />
	</application>	
	</styles>

</chart> 
<?php } ?>