<?
function getCaracPai($imovel,$cont) {
   $car["desc"] = $imovel[$cont]["car_car_desPai"];
   if($imovel[$cont]["car_car_tipPai"] == 1) {
      $car["tipo"] = $imovel[$cont]["imc_tip_con"];
   }
   if($imovel[$cont]["car_car_tipPai"] == 2) {
      $car["tipo"] = $imovel[$cont]["imc_qtd"];
   }
   if($imovel[$cont]["car_car_tipPai"] == 3) {
      $car["tipo"] = $imovel[$cont]["dca_des"];
   }
   if($imovel[$cont]["car_car_tipPai"] == 4) {
      $car["tipo"] = $imovel[$cont]["imc_des"];
   }
   if(($imovel[$cont]["car_car_tipPai"] == 5) && ($imovel[$cont]["car_car_des_uni"] == "R$")) {
      $car["tipo"] = "R$ ".number_format($imovel[$cont]["imc_val"],2,",",".");
   }
   if(($imovel[$cont]["car_car_tipPai"] == 5) && ($imovel[$cont]["car_car_des_uni"] != "R$")) {
      $car["tipo"] = number_format($imovel[$cont]["imc_val"],2,",",".")." ".$imovel[$cont]["car_car_des_uni"];
   }
   return $car;
}
function getCaracFilha($imovel) {
   $i=0;
   foreach($imovel as $imo) {
   	   $carf[$i]["desc"] = $imo["car_car_des"];
	   switch($imo["car_car_tipFilha"]){
	   		case 1:
				$carf[$i]["tipo"] = $imo["imc_tip_con"];
				break;
			case 2:
				$carf[$i]["tipo"] = $imo["imc_qtd"];
				break;
			case 3:
				$carf[$i]["tipo"] = $imo["dca_des"];
				break;
			case 4:
				$carf[$i]["tipo"] = $imo["imc_des"];
				break;
			case 5:
				$carf[$i]["tipo"] = $imo["imc_val"];
				break;
	   }
	   $i++;
   }
   return $carf;
}
?>