<table align="center">
	<tr>
		<td width="565">
		<p class="destaque">ESTAT&Iacute;STICAS<br></p>
		</td>
	</tr>
</table>
<br>
<table width="74%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="buscav-label"><div align="center">Demosntrativo de espa&ccedil;o usado pelo sistema</div></td>
  </tr>
</table>
<br>
<table width="74%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="41%" class="buscav-label"><div align="left">Imagens Cadimo:</div></td>
    <td width="37%"></td>
    <td width="22%" class="texto-pequeno"><div align="right">&nbsp;&nbsp; 
        <?php echo $tamCad?> MB<br>
      </div></td>
  </tr>
  <tr> 
    <td width="41%" class="buscav-label"><div align="left">Imagens Imobsale:</div></td>
    <td  width="37%"></td>
    <td width="22%" class="texto-pequeno"><div align="right">&nbsp;&nbsp; 
        <?php echo $tamImob?> MB<br>
      </div></td>
  </tr>
  <tr> 
    <td width="41%" class="buscav-label"><div align="left">Banco de Dados:</div></td>
    <td  width="37%"></td>
    <td width="22%" class="texto-pequeno"><div align="right">&nbsp;&nbsp; 
        <?php echo $tamBd?> MB<br>
      </div></td>
  </tr>
  <tr> 
    <td height="20" colspan="2"></td>
  </tr>
  <tr> 
    <td width="41%" class="buscav-label"><div align="left">Total de espa�o reservado:</div></td>
    <td  width="37%"></td>
    <td width="22%" class="texto-pequeno"><div align="right">&nbsp;&nbsp;150,00 
        MB<br>
      </div></td>
  </tr>
  <tr> 
    <td width="41%" class="buscav-label"><div align="left">Total de espa�o usado:</div></td>
    <td  width="37%"></td>
    <td width="22%" class="texto-pequeno"><div align="right">&nbsp;&nbsp; 
        <?php echo $total?> MB<br>
      </div></td>
  </tr>
  <tr> 
    <td width="41%" class="buscav-label"><div align="left"><strong><font color="#009900">Total 
        de espa�o dispon�vel:</font></strong></div></td>
    <td  width="37%"></td>
    <td width="22%" class="texto-pequeno"><div align="right"><strong><font color="#009900">&nbsp;&nbsp; 
        <?php echo $totalDisp?> MB<br>
        </font></strong></div></td>
  </tr>
</table>
<br>
<table width="74%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><p><font color="#FF0000"><strong>Aten&ccedil;&atilde;o!!</strong></font></p>
      <div class="buscav-texto">Prezado Administrador, n&atilde;o esque&ccedil;a 
        de realizar a manuten&ccedil;&atilde;o de fotos n&atilde;o mais existentes 
        no banco, pois as mesmas ocupam espa&ccedil;o desnecess&aacute;rio no 
        sistema.</div></td>
  </tr>
</table>
