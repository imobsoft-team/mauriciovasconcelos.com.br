<?php

class Tab_func extends Model{

    public function __construct()
    {
        parent::Model();
    }
    
    public function getCorretor(){
        $this->db->where('FUNC_ISN','1');
        $query = $this->db->get('tab_func');
        $row ="";
        if($query->num_rows() > 0){
                $row = $query->row();
        }
        return $row;
    }
    
    public function cadastrarCorretor($campos,$file) {
        
        if(isset($file['arquivo']['type']) and !empty($file['arquivo']['type']))
        {
            $imagem = '';
        
            $corretor_atual = $this->db->query('SELECT * FROM tab_func WHERE FUNC_ISN = 1');

            if($corretor_atual->num_rows() > 0) {
                $corretor_atual = $corretor_atual->row();

                @unlink(str_replace('adm/', '', $corretor_atual->FUNC_FOTO));
                $res_del = $this->db->query('DELETE FROM tab_func WHERE FUNC_ISN = 1');

            }
        
            preg_match("/\.(pjpeg|jpeg|JPEG|jpg|JPG){1}$/i", $file['arquivo']['name'], $ext);
            // Gera um nome único para a imagem
            $imagem_nome = uniqid(time()).".".$ext[1];
            // Caminho de onde a imagem ficar�
            $imagem_dir = "imagens/corretor/".$imagem_nome;
            // Faz o upload da imagem
            if(move_uploaded_file($file['arquivo']["tmp_name"], $imagem_dir)){
                $sql = "INSERT INTO tab_func (FUNC_ISN, FUNC_NOME, FUNC_CRECI, FUNC_FOTO) 
                VALUES (1,'{$campos['nome']}','{$campos['creci']}','adm/$imagem_dir')";
                $this->db->query($sql);
                
                $codigo = $this->db->insert_id();
                
                if($codigo == 1) {
                    return 'Corretor do mês cadastrado com sucesso';
                }else {
                    //erro ao inserir no banco de dados
                    unlink($imagem_dir);
                    
                    return 'Erro ao cadastrar corretor do mês';
                }
            } else {
                return 'Erro ao cadastrar corretor do mês';
            }
        } else {
            return 'Erro ao cadastrar corretor do mês, favor enviar uma foto';
        }
        
    } 
    
    
      
        public function cadastrarBanner($nome, $ativa, $banner, $cam='') {
            
            if($ativa != 0 && $ativa != 1)
                $ativa = 0;
            
            if($banner["type"])
            {
                if(preg_match("/^image\/(pjpeg|jpeg|JPEG|jpg|JPG)$/i", $banner["type"]))
                {
                    if($banner["size"] < 6000000)
                    {
						// Para verificar as dimens�es da imagem
						$tamanhos = getimagesize($banner["tmp_name"]);
						// Verifica largura e altura da imagem
						if($tamanhos[0] > 5000){
								//erro de largura
								return 3;
						}else if($tamanhos[1] > 5000){
								//erro de altura
								return 4;
						}else {
						//pega a extens�o
							preg_match("/\.(pjpeg|jpeg|JPEG|jpg|JPG){1}$/i", $banner["name"], $ext);
							// Gera um nome �nico para a imagem
							$imagem_nome = uniqid(time()).".".$ext[1];
							// Caminho de onde a imagem ficar�
							$imagem_dir = "adm/imagens/".$imagem_nome;
							// Faz o upload da imagem
							if(@move_uploaded_file($banner["tmp_name"], $cam.$imagem_dir)){
													
							 $sql = "INSERT INTO tab_banner (BANNER_NOME, BANNER_ATIVA, BANNER_IMG) VALUES ('$nome',$ativa,'$imagem_dir')";
							
							   $res = $this->db->query($sql);	
							   $codigo = $this->db->insert_id();	 
							   if ($codigo>0) {		   
									return 7;
							   }else {
								   //erro ao inserir no banco de dados
								   @unlink($cam.$imagem_dir);
								   return 6;
							   } 	
							}else {
								//erro no upload para o servidor.
								return 5;
							}
						}
					}else {
						//erro do tamnho do arquivo
						return 2;
					}
				}else {
					//erro no tipo do arquivo
					return 1;
				}
			} 
    }
    
    
    public function alterarBanner($codigo, $nome, $ativa, $banner, $cam='') {
	  if($banner["type"]){
		   if(preg_match("/^image\/(pjpeg|jpeg|JPEG|jpg|JPG)$/i", $banner["type"])){
		      if($banner["size"] < 6000000){
				// Para verificar as dimens�es da imagem
				$tamanhos = getimagesize($banner["tmp_name"]);
				// Verifica largura e altura da imagem
				if($tamanhos[0] > 5000){
						//erro de largura
						return 3;
				}else if($tamanhos[1] > 5000){
						//erro de altura
						return 4;
				}else {
					//pega a extens�o
					preg_match("/\.(pjpeg|jpeg|JPEG|jpg|JPG){1}$/i", $banner["name"], $ext);
					// Gera um nome �nico para a imagem
        			$imagem_nome = uniqid(time()).".".$ext[1];
					// Caminho de onde a imagem ficar�
        			$imagem_dir = "adm/imagens/".$imagem_nome;
					// Faz o upload da imagem
					$ver = move_uploaded_file($banner["tmp_name"], $cam.$imagem_dir);
					
					$sql = "UPDATE tab_banner SET BANNER_NOME = '$nome', BANNER_ATIVA = '$ativa', BANNER_IMG = '$imagem_dir'  WHERE BANNER_ISN = $codigo";
					
					   $res = $this->db->query($sql);	
					   $codigo = $this->db->insert_id();	 
				       return 7;
				}
			  }else {
			  	 //erro do tamnho do arquivo
				 return 2;
			  }
		   }else {
		   	  //erro no tipo do arquivo
			  return 1;
		   }
	   }else {
                   
		   $sql = "UPDATE tab_banner SET BANNER_NOME = '{$nome}' , BANNER_ATIVA = {$ativa}  WHERE BANNER_ISN = {$codigo}";
		   
		   $res = $this->db->query($sql);	
		   $codigo = $this->db->insert_id();	 
		   if ($codigo>0) {		   
			   return 7;
		   }else {
			   //erro ao inserir no banco de dados
			   return 6;
		   } 	
	   }	   
	   
	    
	   //$sql  = "UPDATE tab_not SET NOT_TITULO = '$titulo',NOT_TITULO_ING = '$tituloing',NOT_TITULO_ESP = '$tituloesp', NOT_DES = '$texto',NOT_DES_ING = '$textoing',NOT_DES_ESP = '$textoesp', NOT_FONTE = '$fonte', NOT_DAT_VAL = '$venc' WHERE NOT_ISN = $codigo";	  
	   //$this->db->query($sql);
	   //return $codigo;
	}
    
}