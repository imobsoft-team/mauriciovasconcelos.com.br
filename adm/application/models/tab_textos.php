<?php
class Tab_textos extends Model{
	function Tab_textos(){
		parent::Model();
	}
	
	function getPagina($pag){
		$this->db->where('TEXTO_ISN', $pag);
		$query = $this->db->get('tab_textos');
		$row ="";
		if($query->num_rows() > 0){
			$row = $query->row();
		}
		return $row;
	}
	
	function getFixas($maximo,$inicio,$busca,$ordem='TEXTO_ISN ASC') {
			$sql = "SELECT * FROM tab_textos";
		    if($busca){
				$sql.=" WHERE TEXTO_NOME LIKE '%$busca%' ";
			}
			$sql.=" ORDER BY $ordem";
			$sql.=" LIMIT $inicio,$maximo";
			$cont="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $array){
				    $cont[$i]["id"]        =  $array->TEXTO_ISN; 
					$cont[$i]["nome"]      =  $array->TEXTO_NOME; 
					$cont[$i]["des"]       =  $array->TEXTO_DES; 
					 if($array->TEXTO_DATA_CAD){
						$venc   		=  explode(' ',$array->TEXTO_DATA_CAD);
						$venc   		=  explode('-',$venc[0]);
						$cont[$i]["data"] = $venc[2].'/'.$venc[1].'/'.$venc[0];
					 }else {
						$cont[$i]["data"] = "0000-00-00";
					 } 
				    $i++;
				}
			}
			return $cont;
	}
	
	function contaRegistros($busca){
		$sql = "SELECT COUNT(TEXTO_ISN) AS TOTAL FROM tab_textos";
		if($busca){
			$sql.=" WHERE TEXTO_NOME LIKE '%$busca%' ";
		}
		$res = $this->db->query($sql);
		foreach($res->result() as $array){
			$total = $array->TOTAL;
		}
		return $total;
	}
	
	function cadastrarFixa($titulo,$tituloing,$tituloesp,$texto,$textoing,$textoesp) {	   
	   $sql = "INSERT INTO tab_textos (TEXTO_NOME,TEXTO_NOME_ING,TEXTO_NOME_ESP,TEXTO_DES,TEXTO_DES_ING,TEXTO_DES_ESP,TEXTO_DATA_CAD) VALUES ('$titulo','$tituloing','$tituloesp','$texto','$textoing','$textoesp','".date("Y-m-d H:i:s")."')";	  
	   $res = $this->db->query($sql);	
	   $codigo = $this->db->insert_id();	 
	   if ($codigo>0) {		   
			return $codigo;
	   }else {
		   return false;
	   } 	
	}
	
	function buscarFixa($cod) {     
	  $sql  = "SELECT * FROM tab_textos WHERE TEXTO_ISN = $cod";
	  $res = $this->db->query($sql);
	  $mod="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			$mod["id"]       =  $array->TEXTO_ISN; 
			$mod["nome"]     =  $array->TEXTO_NOME; 
			$mod["nome_ing"] =  $array->TEXTO_NOME_ING; 
			$mod["nome_esp"] =  $array->TEXTO_NOME_ESP; 
			$mod["des"]      =  $array->TEXTO_DES; 
			$mod["des_ing"]  =  $array->TEXTO_DES_ING; 
			$mod["des_esp"]  =  $array->TEXTO_DES_ESP; 
			if($array->TEXTO_DATA_CAD){
				$venc   		=  explode(' ',$array->TEXTO_DATA_CAD);
				$venc   		=  explode('-',$venc[0]);
				$mod["data"]    =  $venc[2].'/'.$venc[1].'/'.$venc[0];
			 }else {
				$mod["data"] = "0000-00-00";
			 } 
		  }
	  }
	  return $mod;
	}
	
	function alterarFixa($titulo,$tituloing,$tituloesp,$texto,$textoing,$textoesp,$codigo) {     
	   $sql  = "UPDATE tab_textos SET TEXTO_NOME = '$titulo',TEXTO_NOME_ING = '$tituloing',TEXTO_NOME_ESP = '$tituloesp', TEXTO_DES = '$texto', TEXTO_DES_ING = '$textoing', TEXTO_DES_ESP = '$textoesp' WHERE TEXTO_ISN = $codigo";	  
	   $this->db->query($sql);
	   return $codigo;
	}
	
	function excluirItens($cods) { 	
		$sql  = "DELETE FROM tab_textos WHERE TEXTO_ISN IN ($cods)";
		$this->db->query($sql);
	} 

}
?>