<?php
require_once('conexao.php');

class tab_corretores extends Conexao{
	
	public function __construct()
    {
        parent::Model();
    }
    
    public function getPagina($pag)
	{
        $this->db->where('CORR_ISN', $pag);
		$query = $this->db->get('tab_corretores');
		$row ="";
		if($query->num_rows() > 0){
				$row = $query->row();
		}
		return $row;
    }
    
    function excluirItens($cods) { 	
		$sql  = "DELETE FROM tab_corretores WHERE CORR_ISN IN ($cods)";
		$this->db->query($sql);
	}
    
    
    public function contaRegistros($busca)
	{
        $sql = "SELECT COUNT(CORR_ISN) AS TOTAL FROM tab_corretores";
		if($busca)
		{
			$sql.=" WHERE CORR_NOME LIKE '%$busca%' ";
		}
		$res = $this->db->query($sql);
		foreach($res->result() as $array){
            $total = $array->TOTAL;
		}
		return $total;
    }
    
    
    public function getCorretores($maximo,$inicio,$busca,$ordem='CORR_ISN ASC') 
	{
        $sql = "SELECT * FROM tab_corretores";
		if($busca){
            $sql.=" WHERE CORR_NOME LIKE '%$busca%' ";
		}
		$sql.=" ORDER BY $ordem";
		$sql.=" LIMIT $inicio,$maximo";
		$corrs="";
		$res = $this->db->query($sql);
		if($res->num_rows() > 0)
        {
            $i = 0;
            foreach($res->result() as $array)
            {
                $corrs[$i]["id"]        =  $array->CORR_ISN; 
				$corrs[$i]["nome"]      =  $array->CORR_NOME;  
				$i++;
            }
		}
		return $corrs;
    }  
    
    
    public function buscarCorretor($cod) {     
	  $sql  = "SELECT * FROM tab_corretores WHERE CORR_ISN = $cod";
	  $res = $this->db->query($sql);
	  $ban="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			$ban["id"]    = $array->CORR_ISN; 
			$ban["nome"]  = $array->CORR_NOME;  
			$ban["login"] = $array->CORR_LOGIN;
            $ban["senha"]   = $array->CORR_SENHA; 
		  }
	  }
	  return $ban;
	}
        
	public function cadastrarCorretor($nome, $login, $senha) {
        
		$sql = "INSERT INTO tab_corretores(CORR_NOME, CORR_LOGIN, CORR_SENHA) VALUES ('$nome','$login','$senha')";
					
		$res = $this->db->query($sql);	
		$codigo = $this->db->insert_id();	 
		if ($codigo>0) {		   
			return 7;
		}else {
			//erro ao inserir no banco de dados
			return 6;
		} 	
	}
    
    
    public function alterarCorretor($codigo, $nome, $login, $senha) {
	                
		   $sql = "UPDATE tab_corretores SET CORR_NOME = '{$nome}' , CORR_LOGIN = '{$login}', CORR_SENHA = '{$senha}' WHERE CORR_ISN = {$codigo}";
		   $res = $this->db->query($sql);	
		   $codigo = $this->db->insert_id();	 
		   if ($codigo>0) 
		   {		   
			   return 7;
		   }
		   else 
		   {
			   //erro ao inserir no banco de dados
			   return 6;
		   } 		   
	 
	}
	
}
?>