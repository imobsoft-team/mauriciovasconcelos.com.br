<?php

class Tab_par extends Model{

    public function __construct()
    {
        parent::Model();
    }
    
    public function getPagina($pag){
        $this->db->where('par_isn', $pag);
	$query = $this->db->get('tab_par');
	$row ="";
	if($query->num_rows() > 0){
            $row = $query->row();
	}
	return $row;
    }
    
    public function excluirItem($cod) {
        $sql  = "DELETE FROM tab_par WHERE par_isn = {$cod}";
		$this->db->query($sql);
    } 
    
    
    public function contaRegistros($busca){
        $sql = "SELECT COUNT(par_isn) AS TOTAL FROM tab_par";
	if($busca){
            $sql.=" WHERE par_nome LIKE '%$busca%' ";
	}
	$res = $this->db->query($sql);
	foreach($res->result() as $array){
            $total = $array->TOTAL;
	}
	return $total;
    }
    
    
    public function getParceiros($maximo,$inicio,$busca,$ordem='par_isn ASC') {
        $sql = "SELECT * FROM tab_par";
	if($busca){
            $sql.=" WHERE par_nome LIKE '%$busca%' ";
	}
	$sql.=" ORDER BY $ordem";
	$sql.=" LIMIT $inicio,$maximo";
	$parceiros="";
	$res = $this->db->query($sql);
	if($res->num_rows() > 0)
        {
            $i = 0;
            foreach($res->result() as $array)
            {
        $parceiros[$i]["id"]        =  $array->par_isn; 
		$parceiros[$i]["nome"]      =  $array->par_nome; 
		$parceiros[$i]["data"]      =  $array->par_img; 
		$i++;
            }
	}
        
	return $parceiros;
    }  
    
    
    public function buscarparceironer($cod) {     
	  $sql  = "SELECT * FROM tab_par WHERE par_isn = $cod";
	  $res = $this->db->query($sql);
	  $parceiro="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			$parceiro["id"]    = $array->par_isn; 
			$parceiro["nome"]  = $array->par_nome;  
            $parceiro["img"]   = $array->par_img; 
		  }
	  }
	  return $parceiro;
	}
        
	public function cadastrarParceiro($nome, $imagem = null, $id = null) {
	    if(!empty($id)) {
			$sql = "UPDATE tab_par SET par_isn = '', par_nome = '{$nome}', par_img = '{$imagem}' WHERE par_isn = {$id}";
		} else {
			$sql = "INSERT INTO tab_par (par_isn, par_nome, par_img) VALUES ('','{$nome}', '{$imagem}')";
		}
		$query = $this->db->query($sql);
		$res = "";
		if($this->db->affected_rows() == 1){
			if(!empty($id)) {
				$res = "Parceiro atualizado com sucesso.";
			} else {
				$res = "Parceiro cadastrado com sucesso.";
			}
		} else {
			if(!empty($id)){
				$res = "Ocorreu um erro na atualização do parceiro, por favor, entre em contato com o suporte.";
			} else {
				$res = "Ocorreu um erro no cadastro do parceiro, por favor, entre em contato com o suporte.";
			}
		}
		return $res;
    }
	
}