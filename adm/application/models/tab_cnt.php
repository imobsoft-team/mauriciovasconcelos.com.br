<?php
class Tab_cnt extends Model{
	function Tab_cnt(){
		parent::Model();
	}
	
	function getContatos($maximo,$inicio,$busca,$ordem='CNT_ISN ASC') {
			$sql = "SELECT * FROM tab_cnt";
		    if($busca){
				$sql.=" WHERE CNT_NOM LIKE '%$busca%' ";
			}
			$sql.=" ORDER BY $ordem";
			$sql.=" LIMIT $inicio,$maximo";
			$cot="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $array){
				    $cot[$i]["id"]     =  $array->CNT_ISN; 
					$dat 			   =  explode(" ",$array->CNT_DAT);
					$dat2 			   =  explode("-",$dat[0]);
					$cot[$i]["data"]   =  $dat2[2]."/".$dat2[1]."/".$dat2[0]." ".$dat[1];
					$cot[$i]["nome"]   =  $array->CNT_NOM; 
					$cot[$i]["email"]  =  $array->CNT_MAIL; 
					$cot[$i]["fone"]   =  $array->CNT_FONE; 
					$cot[$i]["msg"]    =  $array->CNT_MSG;
					$cot[$i]["depto"]  =  $array->CNT_DEPTO;
					$cot[$i]["cpf"]    =  $array->CNT_CPF;
					$cot[$i]["sta"]    =  $array->CNT_STA; 
				    $i++;
				}
			}
			return $cot;
	}
	
	function contaRegistros($busca){
		$sql = "SELECT COUNT(CNT_ISN) AS TOTAL FROM tab_cnt";
		if($busca){
			$sql.=" WHERE CNT_NOM LIKE ?";
		}
		$res = $this->db->query($sql,array('%'.$busca.'%'));
		foreach($res->result() as $array){
			$total = $array->TOTAL;
		}
		return $total;
	}
	
	function buscarContato($cod) {     
	  $sql  = "SELECT * FROM tab_cnt WHERE CNT_ISN = ?";
	  $res = $this->db->query($sql,array($cod));
	  $cot="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			$cot["id"]     =  $array->CNT_ISN; 
			$dat 		   =  explode(" ",$array->CNT_DAT);
			$dat2 		   =  explode("-",$dat[0]);
			$cot["data"]   =  $dat2[2]."/".$dat2[1]."/".$dat2[0]." ".$dat[1];
			$cot["nome"]   =  $array->CNT_NOM; 
			$cot["email"]  =  $array->CNT_MAIL; 
			$cot["fone"]   =  $array->CNT_FONE; 
			$cot["msg"]    =  $array->CNT_MSG;
			$cot["depto"]  =  $array->CNT_DEPTO;
			$cot["cpf"]    =  $array->CNT_CPF;
			$cot["sta"]    =  $array->CNT_STA; 
		  }
	  }
	  return $cot;
	}
	
	function excluirItens($cods) { 	
		$sql  = "DELETE FROM tab_cnt WHERE CNT_ISN IN ($cods)";
		$this->db->query($sql);
	}
	
	function setSta($id,$sta){
		$sql = "UPDATE tab_cnt SET CNT_STA = ? WHERE CNT_ISN = ?";
		$this->db->query($sql,array($sta,$id));
	} 

}
?>