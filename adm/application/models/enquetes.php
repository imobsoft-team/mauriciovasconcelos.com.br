<?php
class Enquetes extends Model{
	function Enquetes(){
		parent::Model();
	}
	
	function getEnq($maximo,$inicio,$busca,$ordem='COTA_ISN ASC') {
			$sql = "SELECT * FROM tab_enq_per";
		    if($busca){
				$sql.=" WHERE ENQ_PER_DES LIKE '%".$busca."%' ";
			}
			$sql.=" ORDER BY $ordem";
			$sql.=" LIMIT $inicio,$maximo";
			$cot="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $array){
				    $cot[$i]["id"]     =  $array->ENQ_PER_ISN; 
					$cot[$i]["des"]    =  $array->ENQ_PER_DES; 
					 if($array->ENQ_PER_DAT){
						$dat   		=  explode(' ',$array->ENQ_PER_DAT);
						$dat   		=  explode('-',$dat[0]);
						$cot[$i]["dat"] = $dat[2].'/'.$dat[1].'/'.$dat[0];
					 }else {
						$cot[$i]["dat"] = "00-00-0000";
					 } 
					 if($array->ENQ_PER_DAT_FIM){
						$dat   		=  explode('-',$array->ENQ_PER_DAT_FIM);
						$cot[$i]["datf"] = $dat[2].'/'.$dat[1].'/'.$dat[0];
					 }else {
						$cot[$i]["datf"] = "00-00-0000";
					 } 
					$cot[$i]["parc"]   =  $array->ENQ_PER_EXIB_PARC; 
				    $i++;
				}
			}
			return $cot;
	}
	
	function contaRegistros($busca){
		$sql = "SELECT COUNT(ENQ_PER_ISN) AS TOTAL FROM tab_enq_per";
		if($busca){
			$sql.=" WHERE ENQ_PER_DES LIKE '%".$busca."%' ";
		}
		$res = $this->db->query($sql);
		foreach($res->result() as $array){
			$total = $array->TOTAL;
		}
		return $total;
	}
	
	function excluirItens($cods) { 	
		$sql  = "DELETE FROM tab_enq_per WHERE ENQ_PER_ISN IN ($cods)";
		$this->db->query($sql);
		$sql  = "DELETE FROM tab_enq_res WHERE ENQ_RES_PER IN ($cods)";
		$this->db->query($sql);
		$sql  = "DELETE FROM tab_enq_resP WHERE ENQ_PER_ISN IN ($cods)";
		$this->db->query($sql);
	}
	
	function buscarDados($cod) {   
		$sql  =  "SELECT * FROM tab_enq_per WHERE ENQ_PER_ISN = $cod";	
		$res  =  $this->db->query($sql);
		$dados="";
		if($res->num_rows() > 0){
		   foreach($res->result() as $array){
			    $dados["id"]    =  $array->ENQ_PER_ISN;
			    $dados["per_des"]   =  $array->ENQ_PER_DES;
			     if($array->ENQ_PER_DAT){
					$dat   		=  explode(' ',$array->ENQ_PER_DAT);
					$dat   		=  explode('-',$dat[0]);
					$dados["per_dat"] = $dat[2].'/'.$dat[1].'/'.$dat[0];
				 }else {
					$dados["per_dat"] = "00-00-0000";
				 } 
				 if($array->ENQ_PER_DAT_FIM){
					$dat   		=  explode('-',$array->ENQ_PER_DAT_FIM);
					$dados["per_datF"] = $dat[2].'/'.$dat[1].'/'.$dat[0];
				 }else {
					$dados["per_datF"] = "00-00-0000";
				 }
				 if($array->ENQ_PER_DAT_INI){
					$dat   		=  explode('-',$array->ENQ_PER_DAT_INI);
					$dados["per_datI"] = $dat[2].'/'.$dat[1].'/'.$dat[0];
				 }else {
					$dados["per_datI"] = "00-00-0000";
				 } 
			    $dados["per_tipo"]  =  $array->ENQ_PER_TIPO;
			    $dados["per_parc"]  =  $array->ENQ_PER_EXIB_PARC;
				$dados["resp"]      =  $this->getRespostas($array->ENQ_PER_ISN);
			}	
		}
		return $dados;
	}
	
	function getRespostas($cod) {   
		$sql  =  "SELECT * FROM tab_enq_resP WHERE ENQ_PER_ISN = $cod";	
		$res  =  $this->db->query($sql);
		$dados="";
		if($res->num_rows() > 0){
		   $i=0;
		   foreach($res->result() as $array){
			     $dados[$i]["id"]        =  $array->ENQ_RESP_ISN;
		   		 $dados[$i]["res_des"]   =  $array->ENQ_RESP_DES;
				 $dados[$i]['quant']     =  $this->getResultResp($cod,$array->ENQ_RESP_ISN);
				 $i++;
			}	
		}
		return $dados;
	}
	
	function cadastrarEnq($perg,$resp,$ini,$fim,$parc,$tipo) {	   
	  empty($ini)?$ini=date("Y-m-d"):$ini=$ini;
	  $sql  =  "INSERT INTO tab_enq_per (ENQ_PER_DES,ENQ_PER_DAT,ENQ_PER_DAT_INI,ENQ_PER_DAT_FIM,
	         ENQ_PER_TIPO,ENQ_PER_EXIB_PARC) VALUES('$perg','".date("Y-m-d H:i:s")."','$ini','$fim',$tipo,$parc)";		  
	   $res = $this->db->query($sql);	
	   $codigo = $this->db->insert_id();	 
	   if ($codigo>0) {		   
			 foreach($resp as $r){
			 	$sql  =  "INSERT INTO tab_enq_resP (ENQ_RESP_DES,ENQ_RESP_DAT,ENQ_PER_ISN) VALUES ('".htmlentities($r,TRUE)."','".date("Y-m-d H:i:s")."',$codigo)";	  
	   		 	$this->db->query($sql);
			 }
			 return $codigo;	
	   }else {
		   return false;
	   } 	
	}
	
	function alterarEnquete($perg,$resp,$ini,$fim,$parc,$tipo,$cod) {     
	  empty($ini)?$ini=date("Y-m-d"):$ini=$ini;
	  $sql  =  "UPDATE tab_enq_per SET ENQ_PER_DES = '$perg',ENQ_PER_DAT_INI = '$ini', ENQ_PER_DAT_FIM = '$fim', ENQ_PER_TIPO = $tipo, ENQ_PER_EXIB_PARC = $parc WHERE ENQ_PER_ISN = $cod";
	   $this->db->query($sql);
	   $sql = "DELETE FROM tab_enq_resP WHERE ENQ_PER_ISN = $cod";
	   $this->db->query($sql);
		foreach($resp as $r){
			$sql  =  "INSERT INTO tab_enq_resP (ENQ_RESP_DES,ENQ_RESP_DAT,ENQ_PER_ISN) VALUES ('".htmlentities($r,TRUE)."','".date("Y-m-d H:i:s")."',$cod)";	  
			$this->db->query($sql);
		 }
		 return 1;
	}
	
	function getResultResp($pergunta,$resposta) {   
		$sql = "SELECT COUNT(ENQ_RES_ISN) AS QUANTIDADE FROM tab_enq_res WHERE ENQ_RES_PER = $pergunta AND ENQ_RES_RESP = $resposta"; 
		$qtd=0;
		$res = $this->db->query($sql);
		if($res->num_rows() > 0){
			foreach($res->result() as $dados){
			   $qtd   =   $dados->QUANTIDADE;
		    }
		}
		return $qtd;
	}

}
?>