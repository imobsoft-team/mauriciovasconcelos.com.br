<?php
require_once('conexao.php');
class ResumoContrato extends Conexao{
     function ResumoContrato(){
	 	parent::Conexao();
	 }	 
	 function carregaDados($cod) {
	    $resumo = "";
		$sql = "SELECT * FROM TAB_INF WHERE PES_ISN_PROP =".$cod;
	    $conn=$this->connDb();	
		$i = 0;
	    $res = odbc_exec($conn,$sql);
	    if (odbc_errormsg() != "") {
		   return false;
	    }
	    while(@odbc_fetch_row($res)) {
            $resumo[$i]["inf_isn"]         = odbc_result($res,"INF_ISN"); 	   
			$resumo[$i]["imo_des"]         = odbc_result($res,"IMO_DES"); 
			$resumo[$i]["pes_isn_prop"]    = odbc_result($res,"PES_ISN_PROP"); 
			$resumo[$i]["pes_isn_inq"]     = odbc_result($res,"PES_ISN_INQ");
			$resumo[$i]["inf_des_con"]     = odbc_result($res,"INF_DES_CON"); 
			$resumo[$i]["inf_dat_ini"]     = odbc_result($res,"INF_DAT_INI");
			$resumo[$i]["inf_dat_fim"]     = odbc_result($res,"INF_DAT_FIM"); 
			$resumo[$i]["inf_dat_reaj"]    = odbc_result($res,"INF_DAT_REAJ"); 
			$resumo[$i]["inf_val_alu"]     = odbc_result($res,"INF_VAL_ALU"); 
			$resumo[$i]["inf_vald_desc"]   = odbc_result($res,"INF_VAL_DESC"); 
			$resumo[$i]["inf_des_ind"]     = odbc_result($res,"INF_DES_IND"); 
			$resumo[$i]["inf_val_cot"]     = odbc_result($res,"INF_VAL_COT"); 
			$resumo[$i]["inf_des_gar"]     = odbc_result($res,"INF_DES_GAR");
			$i++;
			
	    }
		is_array($resumo)?$quant=count($resumo):$quant=0;
		for($cont=0;$cont < $quant;$cont++) {
            $sql2 = "SELECT PES_NOM FROM TAB_PES WHERE PES_ISN=".$resumo[$cont]["pes_isn_inq"];	   
			$resultado = odbc_exec($conn,$sql2);
			while(@odbc_fetch_row($resultado)) {
			   $resumo[$cont]["pes_nom"]  =  odbc_result($resultado,"PES_NOM");
			}   
		}
	    odbc_close($conn);		
	    return $resumo;
	 }

  }
?>