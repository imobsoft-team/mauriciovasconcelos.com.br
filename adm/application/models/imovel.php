<?php
  class Imovel extends Model{	 
	 function Imovel() {
	 	parent::Model();
	 }
	 
     function getProp($maximo,$inicio,$busca,$ordem='PPC_ISN ASC') {
			$sql = "SELECT * FROM tab_ppc WHERE PPC_TIP_FIA = 0 AND PPC_VINCULO IS NULL ";
		    if($busca){
				$sql.=" AND PPC_NOM LIKE '%".utf8_encode($busca)."%'";
			}
			$sql.=" ORDER BY $ordem";
			$sql.=" LIMIT $inicio,$maximo";
			$prop="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $array){
				    $prop[$i]["id"]        = $array->PPC_ISN;
					if($array->PPC_DAT){
						$dat   		=  explode(' ',$array->PPC_DAT);
						$dat   		=  explode('-',$dat[0]);
						$prop[$i]["data"]  = $dat[2].'/'.$dat[1].'/'.$dat[0];
					 }else {
						$prop[$i]["data"]  = "00/00/0000";
					 } 
				 	$prop[$i]["nome"]      = $array->PPC_NOM;
				 	$prop[$i]["tip_pes"]   = $array->PPC_TIP_PES;
				    $i++;
				}
			}
			return $prop;
	}
	
	function getImo($maximo,$inicio,$busca,$ordem='CIM_ISN ASC') {
			$sql = "SELECT * FROM tab_cim ";
		    if($busca){
				$sql.=" WHERE CIM_NOM LIKE '%".utf8_encode($busca)."%'";
			}
			$sql.=" ORDER BY $ordem";
			$sql.=" LIMIT $inicio,$maximo";
			$imo="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $array){
				      $imo[$i]["id"]          = $array->CIM_ISN;
					  $imo[$i]["imo_isn"]     = $array->CIM_ISN;
					  $imo[$i]["imo_nom"]     = $array->CIM_NOM;
					  $imo[$i]["imo_log"]     = $array->CIM_DES_LOG;
					  $imo[$i]["imo_num"]     = $array->CIM_NUM_END;
					  $imo[$i]["imo_bai"]     = $array->CIM_DES_BAI;
					  $imo[$i]["imo_cid"]     = $array->CIM_DES_CID;
					  $imo[$i]["imo_mail"]    = $array->CIM_MAIL;
					  $imo[$i]["imo_resumo"]  = $array->CIM_RES_IMO;
					  $imo[$i]["imo_finali"]  = $array->CIM_FIN;
					  $imo[$i]["imo_fone1"]   = $array->CIM_NUM_FON_1;
					  $imo[$i]["imo_fone2"]   = $array->CIM_NUM_FON_2;
					  $imo[$i]["imo_cel"]     = $array->CIM_NUM_CEL;
					  $imo[$i]["imo_comp"]    = $array->CIM_DES_COMP;
					  $imo[$i]["imo_cep"]     = $array->CIM_DES_CEP;
					  $imo[$i]["imo_uf"]      = $array->CIM_RES_UF;
				    $i++;
				}
			}
			return $imo;
	}
	
	function buscaImo($cod) {
			$sql = "SELECT * FROM tab_cim WHERE CIM_ISN = $cod";
			$imo="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $array){
				      $imo["id"]          = $array->CIM_ISN;
					  $imo["imo_isn"]     = $array->CIM_ISN;
					  $imo["imo_nom"]     = $array->CIM_NOM;
					  $imo["imo_log"]     = $array->CIM_DES_LOG;
					  $imo["imo_num"]     = $array->CIM_NUM_END;
					  $imo["imo_bai"]     = $array->CIM_DES_BAI;
					  $imo["imo_cid"]     = $array->CIM_DES_CID;
					  $imo["imo_mail"]    = $array->CIM_MAIL;
					  $imo["imo_resumo"]  = $array->CIM_RES_IMO;
					  $imo["imo_finali"]  = $array->CIM_FIN;
					  $imo["imo_fone1"]   = $array->CIM_NUM_FON_1;
					  $imo["imo_fone2"]   = $array->CIM_NUM_FON_2;
					  $imo["imo_cel"]     = $array->CIM_NUM_CEL;
					  $imo["imo_comp"]    = $array->CIM_DES_COMP;
					  $imo["imo_cep"]     = $array->CIM_DES_CEP;
					  $imo["imo_uf"]      = $array->CIM_RES_UF;
				    $i++;
				}
			}
			return $imo;
	}
	
	function contaRegistros($busca){
		$sql = "SELECT COUNT(PPC_ISN) AS TOTAL FROM tab_ppc WHERE PPC_TIP_FIA = 0 AND PPC_VINCULO IS NULL ";
		if($busca){
			$sql.=" AND PPC_NOM LIKE '%".utf8_encode($busca)."%'";
		}
		$res = $this->db->query($sql);
		foreach($res->result() as $array){
			$total = $array->TOTAL;
		}
		return $total;
	}
	
	function contaImos($busca){
		$sql = "SELECT COUNT(CIM_ISN) AS TOTAL FROM tab_cim ";
		if($busca){
			$sql.=" WHERE CIM_NOM LIKE '%".utf8_encode($busca)."%'";
		}
		$res = $this->db->query($sql);
		foreach($res->result() as $array){
			$total = $array->TOTAL;
		}
		return $total;
	}
	
	function buscarProp($cod) {     
	  $sql  = "SELECT * FROM tab_ppc WHERE PPC_ISN = $cod";
	  $res = $this->db->query($sql);
	  $titular="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			 $titular["id"]                = $array->PPC_ISN;
			 if($array->PPC_DAT){
				$dat   		=  explode(' ',$array->PPC_DAT);
				$dat   		=  explode('-',$dat[0]);
				$titular["data"]  = $dat[2].'/'.$dat[1].'/'.$dat[0];
			 }else {
				$titular["data"]  = "00/00/0000";
			 } 
			 $titular["nome"]              = $array->PPC_NOM;
			 $titular["tip_pes"]           = $array->PPC_TIP_PES;
			 $titular["imo_cod"]           = $array->PPC_IMO_COD;
			 $titular["tip_fia"]           = $array->PPC_TIP_FIA;
			 $titular["cpf_cnpj"]          = $array->PPC_NUM_CPF_CNPJ;
			 $titular["num_fon_1"]         = $array->PPC_NUM_FON_1;
			 $titular["num_fon_2"]         = $array->PPC_NUM_FON_2;
			 $titular["num_cel"]           = $array->PPC_NUM_CEL;
			 $titular["des_log_res"]       = $array->PPC_DES_LOG_RES;
			 $titular["des_num_end_res"]   = $array->PPC_DES_NUM_END_RES;
			 $titular["des_comp_res"]      = $array->PPC_DES_COMP_RES;
			 $titular["des_bai_res"]       = $array->PPC_DES_BAI_RES;
			 $titular["des_loc_res"]       = $array->PPC_DES_LOC_RES;
			 $titular["uf_res"]            = $array->PPC_UF_RES;
			 $titular["des_num_cep_res"]   = $array->PPC_DES_NUM_CEP_RES;
			 $titular["tpm_dur_end_res"]   = $array->PPC_TMP_DUR_END_RES;
			 $titular["tip_res"]           = $array->PPC_TIP_RES;
			 $titular["val_res"]           = $array->PPC_VAL_RES;
			 $titular["des_outros"]        = $array->PPC_DES_OUTROS;
			 $titular["fav_alu"]           = $array->PPC_FAV_ALU;
			 $titular["num_fon_fav"]       = $array->PPC_NUM_FON_FAV;
			 $titular["des_end"]           = $array->PPC_DES_END;
			 $titular["tip_rend"]          = $array->PPC_TIP_REND;
			 $titular["des_rend"]          = $array->PPC_DES_REND;
			 $titular["des_ref1"]          = $array->PPC_DES_REF1;
			 $titular["des_ref2"]          = $array->PPC_DES_REF2;
			 $titular["fon_ref1"]          = $array->PPC_FON_REF1;
			 $titular["fon_ref2"]          = $array->PPC_FON_REF2;
			 $titular["nom_ref_com1"]      = $array->PPC_NOM_REF_COM1;
			 $titular["num_fon_ref_com1"]  = $array->PPC_NUM_FON_REF_COM1;
			 $titular["ref_imob"]          = $array->PPC_REF_IMOB;
			 $titular["num_fon_ref_imob"]  = $array->PPC_NUM_FON_REF_IMOB;
			 $titular["tip_cta_1"]         = $array->PPC_TIP_CTA_1;
			 $titular["tip_cta_2"]         = $array->PPC_TIP_CTA_2;
			 $titular["des_cta_1"]         = $array->PPC_DES_CTA_1;
			 $titular["des_cta_2"]         = $array->PPC_DES_CTA_2;
			 $titular["cod_age_1"]         = $array->PPC_COD_AGE_1;
			 $titular["cod_age_2"]         = $array->PPC_COD_AGE_2;
			 $titular["cod_bco_1"]         = $array->PPC_COD_BCO_1;
			 $titular["cod_bco_2"]         = $array->PPC_COD_BCO_2;
			 $titular["des_imo_bem_1"]     = $array->PPC_DES_IMO_BEM_1;
			 $titular["val_imo_bem_1"]     = $array->PPC_VAL_IMO_BEM_1;
			 $titular["mat_imo_bem_1"]     = $array->PPC_MAT_IMO_BEM_1;
			 $titular["zon_imo_bem_1"]     = $array->PPC_ZON_IMO_BEM_1;
			 $titular["des_imo_bem_2"]     = $array->PPC_DES_IMO_BEM_2;
			 $titular["val_imo_bem_2"]     = $array->PPC_VAL_IMO_BEM_2;
			 $titular["mat_imo_bem_2"]     = $array->PPC_MAT_IMO_BEM_2;
			 $titular["zon_imo_bem_2"]     = $array->PPC_ZON_IMO_BEM_2;
			 $titular["des_auto_1"]        = $array->PPC_DES_AUTO_1;
			 $titular["val_auto_1"]        = $array->PPC_VAL_AUTO_1;
			 $titular["placa_auto_1"]      = $array->PPC_PLACA_AUTO_1;
			 $titular["ano_auto_1"]        = $array->PPC_ANO_AUTO_1;
			 $titular["des_auto_2"]        = $array->PPC_DES_AUTO_2;
			 $titular["val_auto_2"]        = $array->PPC_VAL_AUTO_2;
			 $titular["placa_auto_2"]      = $array->PPC_PLACA_AUTO_2;
			 $titular["ano_auto_2"]        = $array->PPC_ANO_AUTO_2; 
			 $titular["font_rend"]         = $array->PPC_FONT_PAG_REND;
			 $titular["val_rend"]          = $array->PPC_VAL_REND;
			 $titular["end_rend"]          = $array->PPC_END_OUT_REND;
			 $titular["fone_rend"]         = $array->PPC_FONE_OUT_REND;
		  }
	  }
	  return $titular;
	}
	
	function consultarDadosCpf($cod) {     
	  $sql  = "SELECT * FROM TAB_CPF WHERE PPC_ISN = $cod";
	  $res = $this->db->query($sql);
	  $dados="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			   $dados["ppc_isn"]                =  $array->PPC_ISN;
			   $dados["cpf_sexo"]               =  $array->CPF_SEXO;
			   $dados["cpf_est_civil"]          =  $array->CPF_EST_CIVIL;
			   $dados["cpf_filiacao"]           =  $array->CPF_FILIACAO;
			   $dados["cpf_num_rg"]             =  $array->CPF_NUM_RG;
			   $dados["cpf_org_exp"]            =  $array->CPF_ORG_EXP;
			   $dados["cpf_des_nac"]            =  $array->CPF_DES_NAC;
			   $dados["cpf_des_nat"]            =  $array->CPF_DES_NAT;
			   $dados["cpf_dat_nasc"]           =  $array->CPF_DAT_NASC;
			   $dados["cpf_des_prf"]            =  $array->CPF_DES_PRF;
			   $dados["cpf_des_end_trab"]       =  $array->CPF_DES_END_TRAB;
			   $dados["cpf_des_num_end_trab"]   =  $array->CPF_DES_NUM_END_TRAB;
			   $dados["cpf_des_comp_trab"]      =  $array->CPF_DES_COMP_TRAB;
			   $dados["cpf_des_bai_trab"]       =  $array->CPF_DES_BAI_TRAB;
			   $dados["cpf_des_loc_trab"]       =  $array->CPF_DES_LOC_TRAB;
			   $dados["cpf_des_uf_trab"]        =  $array->CPF_DES_UF_TRAB;
			   $dados["cpf_des_soc"]            =  $array->CPF_DES_SOC;
			   $dados["cpf_nom_fir_trab"]       =  $array->CPF_NOM_FIR_TRAB;
			   $dados["cpf_num_fon1_trab"]      =  $array->CPF_NUM_FON1_TRAB;
			   $dados["cpf_num_fon1_ram_trab"]  =  $array->CPF_NUM_FON1_RAM_TRAB;
			   $dados["cpf_num_fon2_trab"]      =  $array->CPF_NUM_FON2_TRAB;
			   $dados["cpf_des_fun_trab"]       =  $array->CPF_DES_FUN_TRAB;
			   $dados["cpf_tmp_trab"]           =  $array->CPF_TMP_TRAB;
			   $dados["cpf_des_sal"]            =  $array->CPF_DES_SAL;
			   $dados["cpf_tip_rend"]           =  $array->CPF_TIP_REND;
			   $dados["cpf_nom_cjg"]            =  $array->CPF_NOM_CJG;
			   $dados["cpf_fil_cjg"]            =  $array->CPF_FIL_CJG;
			   $dados["cpf_dat_nas_cjg"]        =  $array->CPF_DAT_NAS_CJG;
			   $dados["cpf_num_rg_cjg"]         =  $array->CPF_NUM_RG_CJG;
			   $dados["cpf_org_exp_cjg"]        =  $array->CPF_ORG_EXP_CJG;
			   $dados["cpf_prf_cjg"]            =  $array->CPF_PRF_CJG;
			   $dados["cpf_num_cpf_cjg"]        =  $array->CPF_NUM_CPF_CJG;
			   $dados["cpf_des_nat_cjg"]        =  $array->CPF_DES_NAT_CJG;
			   $dados["cpf_des_nas_cjg"]        =  $array->CPF_DES_NAS_CJG;
		  }
	  }
	  return $dados;
	}
	
	function consultarDadosCpj($cod) {     
	  $sql  = "SELECT * FROM TAB_CPJ WHERE PPC_ISN = $cod";
	  $res = $this->db->query($sql);
	  $dados="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			   $dados["ppc_isn"]                =  $array->PPC_ISN;
			     if($array->CPJ_DAT_CONST){
					$dat   		=  explode('-',$array->CPJ_DAT_CONST);
					$dados["cpj_dat_cons"]  = $dat[2].'/'.$dat[1].'/'.$dat[0];
				 }else {
					$dados["cpj_dat_cons"]  = "00/00/0000";
				 } 			   
			   $dados["cpj_ins_est"]            =  $array->CPJ_INS_EST;
			   $dados["cpj_num_reg_juc"]        =  $array->CPJ_NUM_REG_JUC;
			   $dados["cpj_tip_soc"]            =  $array->CPJ_TIP_SOC;
			   $dados["cpj_set_atv_com"]        =  $array->CPJ_SET_ATV_COM;
			   $dados["cpj_nom_soc_emp"]        =  $array->CPJ_NOM_SOC_EMP;
			   $dados["cpj_tel_res_soc"]        =  $array->CPJ_TEL_RES_SOC;
			   $dados["cpj_cel_soc"]            =  $array->CPJ_CEL_SOC;
			   $dados["cpj_des_mail"]           =  $array->CPJ_DES_MAIL;
			   $dados["cpj_dat_nas"]            =  $array->CPJ_DAT_NAS;
			   $dados["cpj_num_rg"]             =  $array->CPJ_NUM_RG;
			   $dados["cpj_org_exp"]            =  $array->CPJ_ORG_EXP;
			   $dados["cpj_num_cpf"]            =  $array->CPJ_NUM_CPF;
			   $dados["cpj_nat_soc"]            =  $array->CPJ_NAT_SOC;
			   $dados["cpj_nac_soc"]            =  $array->CPJ_NAC_SOC;
			   $dados["cpj_est_civil"]          =  $array->CPJ_EST_CIVIL;
			   $dados["cpj_end_soc"]            =  $array->CPJ_END_SOC;
			   $dados["cpj_bai_soc"]            =  $array->CPJ_BAI_SOC;
			   $dados["cpj_des_loc"]            =  $array->CPJ_DES_LOC;
			   $dados["cpj_des_uf"]             =  $array->CPJ_DES_UF;
			   $dados["cpj_num_cep"]            =  $array->CPJ_NUM_CEP;
			   $dados["cpj_tmp_res_aut"]        =  $array->CPJ_TMP_RES_AUT;
			   $dados["cpj_tip_imo"]            =  $array->CPJ_TIP_IMO;
			   $dados["cpj_val_res"]            =  $array->CPJ_VAL_RES;
			   $dados["cpj_out_rend"]           =  $array->CPJ_OUT_REND;
			   $dados["cpj_fnt_apos"]           =  $array->CPJ_FNT_APOS;
			   $dados["tip_res"]           		=  $array->CPJ_TIP_IMO;
		  }
	  }
	  return $dados;
	}
	
	function buscarFiadorProp($cod) {     
	  $sql  = "SELECT * FROM tab_ppc WHERE PPC_VINCULO = $cod AND PPC_TIP_FIA = 1";
	  $res = $this->db->query($sql);
	  $fiador="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			 $fiador["id"]                = $array->PPC_ISN;
			 if($array->PPC_DAT){
				$dat   		=  explode(' ',$array->PPC_DAT);
				$dat   		=  explode('-',$dat[0]);
				$fiador["data"]   = $dat[2].'/'.$dat[1].'/'.$dat[0];
			 }else {
				$fiador["data"]   = "00/00/0000";
			 } 
			 $fiador["nome"]              = $array->PPC_NOM;
			 $fiador["tip_pes"]           = $array->PPC_TIP_PES;
			 $fiador["imo_cod"]           = $array->PPC_IMO_COD;
			 $fiador["tip_fia"]           = $array->PPC_TIP_FIA;
			 $fiador["cpf_cnpj"]          = $array->PPC_NUM_CPF_CNPJ;
			 $fiador["num_fon_1"]         = $array->PPC_NUM_FON_1;
			 $fiador["num_fon_2"]         = $array->PPC_NUM_FON_2;
			 $fiador["num_cel"]           = $array->PPC_NUM_CEL;
			 $fiador["des_log_res"]       = $array->PPC_DES_LOG_RES;
			 $fiador["des_num_end_res"]   = $array->PPC_DES_NUM_END_RES;
			 $fiador["des_comp_res"]      = $array->PPC_DES_COMP_RES;
			 $fiador["des_bai_res"]       = $array->PPC_DES_BAI_RES;
			 $fiador["des_loc_res"]       = $array->PPC_DES_LOC_RES;
			 $fiador["uf_res"]            = $array->PPC_UF_RES;
			 $fiador["des_num_cep_res"]   = $array->PPC_DES_NUM_CEP_RES;
			 $fiador["tpm_dur_end_res"]   = $array->PPC_TMP_DUR_END_RES;
			 $fiador["tip_res"]           = $array->PPC_TIP_RES;
			 $fiador["val_res"]           = $array->PPC_VAL_RES;
			 $fiador["des_outros"]        = $array->PPC_DES_OUTROS;
			 $fiador["fav_alu"]           = $array->PPC_FAV_ALU;
			 $fiador["num_fon_fav"]       = $array->PPC_NUM_FON_FAV;
			 $fiador["des_end"]           = $array->PPC_DES_END;
			 $fiador["tip_rend"]          = $array->PPC_TIP_REND;
			 $fiador["des_rend"]          = $array->PPC_DES_REND;
			 $fiador["des_ref1"]          = $array->PPC_DES_REF1;
			 $fiador["des_ref2"]          = $array->PPC_DES_REF2;
			 $fiador["fon_ref1"]          = $array->PPC_FON_REF1;
			 $fiador["fon_ref2"]          = $array->PPC_FON_REF2;
			 $fiador["nom_ref_com1"]      = $array->PPC_NOM_REF_COM1;
			 $fiador["num_fon_ref_com1"]  = $array->PPC_NUM_FON_REF_COM1;
			 $fiador["ref_imob"]          = $array->PPC_REF_IMOB;
			 $fiador["num_fon_ref_imob"]  = $array->PPC_NUM_FON_REF_IMOB;
			 $fiador["tip_cta_1"]         = $array->PPC_TIP_CTA_1;
			 $fiador["tip_cta_2"]         = $array->PPC_TIP_CTA_2;
			 $fiador["des_cta_1"]         = $array->PPC_DES_CTA_1;
			 $fiador["des_cta_2"]         = $array->PPC_DES_CTA_2;
			 $fiador["cod_age_1"]         = $array->PPC_COD_AGE_1;
			 $fiador["cod_age_2"]         = $array->PPC_COD_AGE_2;
			 $fiador["cod_bco_1"]         = $array->PPC_COD_BCO_1;
			 $fiador["cod_bco_2"]         = $array->PPC_COD_BCO_2;
			 $fiador["des_imo_bem_1"]     = $array->PPC_DES_IMO_BEM_1;
			 $fiador["val_imo_bem_1"]     = $array->PPC_VAL_IMO_BEM_1;
			 $fiador["mat_imo_bem_1"]     = $array->PPC_MAT_IMO_BEM_1;
			 $fiador["zon_imo_bem_1"]     = $array->PPC_ZON_IMO_BEM_1;
			 $fiador["des_imo_bem_2"]     = $array->PPC_DES_IMO_BEM_2;
			 $fiador["val_imo_bem_2"]     = $array->PPC_VAL_IMO_BEM_2;
			 $fiador["mat_imo_bem_2"]     = $array->PPC_MAT_IMO_BEM_2;
			 $fiador["zon_imo_bem_2"]     = $array->PPC_ZON_IMO_BEM_2;
			 $fiador["des_auto_1"]        = $array->PPC_DES_AUTO_1;
			 $fiador["val_auto_1"]        = $array->PPC_VAL_AUTO_1;
			 $fiador["placa_auto_1"]      = $array->PPC_PLACA_AUTO_1;
			 $fiador["ano_auto_1"]        = $array->PPC_ANO_AUTO_1;
			 $fiador["des_auto_2"]        = $array->PPC_DES_AUTO_2;
			 $fiador["val_auto_2"]        = $array->PPC_VAL_AUTO_2;
			 $fiador["placa_auto_2"]      = $array->PPC_PLACA_AUTO_2;
			 $fiador["ano_auto_2"]        = $array->PPC_ANO_AUTO_2;
			 $fiador["font_rend"]         = $array->PPC_FONT_PAG_REND;
			 $fiador["val_rend"]          = $array->PPC_VAL_REND;
			 $fiador["end_rend"]          = $array->PPC_END_OUT_REND;
			 $fiador["fone_rend"]         = $array->PPC_FONE_OUT_REND;
		  }
	  }
	  return $fiador;
	}
	
	function excluirItens($cods) { 	
		$sql  = "DELETE FROM tab_cpf WHERE PPC_ISN IN ($cods)";
		$this->db->query($sql);
		$sql  = "DELETE FROM tab_cpj WHERE PPC_ISN IN ($cods)";
		$this->db->query($sql);
		$sql  = "DELETE FROM tab_ppc WHERE PPC_VINCULO IN ($cods)";
		$this->db->query($sql);
		$sql  = "DELETE FROM tab_ppc WHERE PPC_ISN IN ($cods)";
		$this->db->query($sql);
	} 
	
	function excluirImos($cods) { 	
		$sql  = "DELETE FROM TAB_CIM WHERE CIM_ISN IN ($cods)";
		$this->db->query($sql);
	} 
	  
  }
?>