<?php
class tab_grup extends Model{
	function tab_grup(){
		parent::Model();
	}
	
	function getGrupos($maximo,$inicio,$busca,$ordem='GRUP_ISN ASC') {
			$sql = "SELECT * FROM tab_grup";
		    if($busca){
				$sql.=" WHERE GRUP_NOME LIKE '%".$busca."%'";
			}
			$sql.=" ORDER BY $ordem";
			$sql.=" LIMIT $inicio,$maximo";
			$mod="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $array){
				    $mod[$i]["id"]       =  $array->GRUP_ISN; 
				    $mod[$i]["nome"]     =  $array->GRUP_NOME;
				    $i++;
				}
			}
			return $mod;
	}
	
	function contaRegistros($busca){
		$sql = "SELECT COUNT(GRUP_ISN) AS TOTAL FROM tab_grup";
		if($busca){
			$sql.=" WHERE GRUP_NOME LIKE '%".$busca."%'";
		}
		$res = $this->db->query($sql);
		foreach($res->result() as $array){
			$total = $array->TOTAL;
		}
		return $total;
	}
	
	function cadastrarGrupo($nome) {	   
	    $sql = "INSERT INTO tab_grup (GRUP_NOME,GRUP_DATA_CAD) VALUES ('$nome','".date("Y-m-d H:i:s")."')";	 
		$res = $this->db->query($sql);	
		$codigo = $this->db->insert_id();	 
		if ($codigo>0) {		   
		   return $codigo;
		}else {
		   return false;
		} 
	}
	
	function buscarGrupo($cod) {     
	  $sql  = "SELECT * FROM tab_grup WHERE GRUP_ISN = $cod";
	  $res = $this->db->query($sql);
	  $mod="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			 $mod["id"]       =  $array->GRUP_ISN; 
			 $mod["nome"]     =  $array->GRUP_NOME;
		  }
	  }
	  return $mod;
	}
	
	function alterarGrupo($nome,$id) {     
	     $sql = "UPDATE tab_grup SET GRUP_NOME = '$nome' WHERE GRUP_ISN = $id";					   	 
		 $this->db->query($sql);
		 return 1;
	}
	
	function excluirItens($cods) {
		$sql  = "DELETE FROM Tab_news WHERE NWS_GRUPO IN ($cods)";
		$this->db->query($sql);
		$sql  = "DELETE FROM tab_grup WHERE GRUP_ISN IN ($cods)";
		$this->db->query($sql);
	} 

}
?>