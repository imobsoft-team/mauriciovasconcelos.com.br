<?php
class tab_prec extends Model{
	function tab_prec(){
		parent::Model();
	}
	
	function getPrec($maximo,$inicio,$busca,$ordem='prec_isn ASC') {
			$sql = "SELECT * FROM tab_prec";
		    if($busca){
				$sql.=" WHERE prec_corretor LIKE '%".$busca."%' ";
			}
			$sql.=" ORDER BY $ordem";
			$sql.=" LIMIT $inicio,$maximo";
			$dados="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $array){
				    $dados[$i]["id"]          =  $array->prec_isn;
					$dados[$i]["corretor"]    =  $array->prec_corretor;
					$dados[$i]["fone_cor"]    =  $array->prec_fone_cor;
					$dados[$i]["imobiliaria"] =  $array->prec_imobiliaria;
					$dados[$i]["fone_imo"]    =  $array->prec_fone_imob;
					$dados[$i]["descricao"]   =  $array->prec_descricao;					
				    $i++;
				}
			}
			return $dados;
	}
	
	function contaRegistros($busca){
		$sql = "SELECT COUNT(prec_isn) AS TOTAL FROM tab_prec";
		if($busca){
			$sql.=" WHERE prec_corretor LIKE '%".$busca."%' ";
		}
		$res = $this->db->query($sql);
		foreach($res->result() as $array){
			$total = $array->TOTAL;
		}
		return $total;
	}
	
	function excluirItens($cods) { 	
		$sql  = "DELETE FROM tab_prec WHERE prec_isn IN ($cods)";
		$this->db->query($sql);
	}
	
	function buscarDados($cod) {   
		$sql  =  "SELECT * FROM tab_prec WHERE prec_isn = $cod";	
		$res  =  $this->db->query($sql);
		$dados="";
		if($res->num_rows() > 0){
		   foreach($res->result() as $array){
			    $dados["id"]          =  $array->prec_isn;
				$dados["corretor"]    =  $array->prec_corretor;
				$dados["fone_cor"]    =  $array->prec_fone_cor;
				$dados["imobiliaria"] =  $array->prec_imobiliaria;
				$dados["fone_imo"]    =  $array->prec_fone_imob;
				$dados["descricao"]   =  $array->prec_descricao;	
			}	
		}
		return $dados;
	}
	
	function getRespostas($cod) {   
		$sql  =  "SELECT * FROM tab_enq_resp WHERE ENQ_PER_ISN = $cod";	
		$res  =  $this->db->query($sql);
		$dados="";
		if($res->num_rows() > 0){
		   $i=0;
		   foreach($res->result() as $array){
			     $dados[$i]["id"]        =  $array->ENQ_RESP_ISN;
		   		 $dados[$i]["res_des"]   =  $array->ENQ_RESP_DES;
				 $dados[$i]['quant']     =  $this->getResultResp($cod,$array->ENQ_RESP_ISN);
				 $i++;
			}	
		}
		return $dados;
	}
	
	function cadastrarPrec($corretor,$fonecor,$imobiliaria,$foneimob,$desc) {	   
	   $sql  =  "INSERT INTO tab_prec (prec_corretor, prec_fone_cor, prec_imobiliaria, prec_fone_imob, prec_descricao) VALUES(?, ?, ?, ?, ?)";		  
	   $this->db->query($sql,array($corretor,$fonecor,$imobiliaria,$foneimob,$desc));	
	   $codigo = $this->db->insert_id();	
	   return $codigo; 
	  
	}
	
	function alterarPrec($corretor,$fonecor,$imobiliaria,$foneimob,$desc,$id) {
	  $sql  =  "UPDATE tab_prec SET prec_corretor = ?, prec_fone_cor = ?, prec_imobiliaria = ?, prec_fone_imob = ?, prec_descricao = ? WHERE prec_isn = ?";
	  $this->db->query($sql,array($corretor,$fonecor,$imobiliaria,$foneimob,$desc,$id));
	  return 1;
	}
	
	function getResultResp($pergunta,$resposta) {   
		$sql = "SELECT COUNT(ENQ_RES_ISN) AS QUANTIDADE FROM tab_enq_res WHERE ENQ_RES_PER = $pergunta AND ENQ_RES_RESP = $resposta"; 
		$qtd=0;
		$res = $this->db->query($sql);
		if($res->num_rows() > 0){
			foreach($res->result() as $dados){
			   $qtd   =   $dados->QUANTIDADE;
		    }
		}
		return $qtd;
	}

}
?>