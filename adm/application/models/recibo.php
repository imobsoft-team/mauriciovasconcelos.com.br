<?php
require_once('conexao.php');
 class Recibo extends Conexao{
    function Recibo() {
		parent::Conexao();
	}
	
	 function carregarDados($codigo) {
	   $dadosRec = "";
	   $i = 0;
	   $sql = "SELECT * FROM TAB_REC WHERE PES_ISN = $codigo"; 
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   $dadosRec ="";
	   while(@odbc_fetch_row($res)) {
			$dadosRec[$i]["rec_isn"]                = odbc_result($res,"REC_ISN"); 	   
			$dadosRec[$i]["rec_nom_inq"]            = odbc_result($res,"REC_NOM_INQ"); 	   
			$dadosRec[$i]["rec_num_cpf_cgc_inq"]    = odbc_result($res,"REC_NUM_CPF_CGC_INQ"); 	   
			$dadosRec[$i]["rec_des_end"]            = odbc_result($res,"REC_DES_END"); 	   
			$dadosRec[$i]["rec_des_bai"]            = odbc_result($res,"REC_DES_BAI"); 	   
			$dadosRec[$i]["rec_des_end_cob"]        = odbc_result($res,"REC_DES_END_COB"); 	   
			$dadosRec[$i]["rec_des_bai_cob"]        = odbc_result($res,"REC_DES_BAI_COB"); 	   
			$dadosRec[$i]["rec_des_cid_cob"]        = odbc_result($res,"REC_DES_CID_COB"); 	   
			$dadosRec[$i]["rec_num_cep_cob"]        = odbc_result($res,"REC_NUM_CEP_COB"); 	   
			$dadosRec[$i]["rec_des_est_cob"]        = odbc_result($res,"REC_DES_EST_COB"); 	   
			$dadosRec[$i]["rec_nom_prop"]           = odbc_result($res,"REC_NOM_PROP"); 	   
			$dadosRec[$i]["rec_num_cpf_cgc_prop"]   = odbc_result($res,"REC_NUM_CPF_CGC_PROP"); 	   
			$dadosRec[$i]["rec_num"]                = odbc_result($res,"REC_NUM"); 	   
			$dadosRec[$i]["rec_dat_vct"]            = odbc_result($res,"REC_DAT_VCT"); 	   
			$dadosRec[$i]["rec_dat_emi"]            = odbc_result($res,"REC_DAT_EMI"); 	   
			$dadosRec[$i]["rec_dst_car"]            = odbc_result($res,"REC_DAT_CAR"); 	   
			$dadosRec[$i]["rec_dat_reaj"]           = odbc_result($res,"REC_DAT_REAJ"); 	   
			$dadosRec[$i]["rec_val_jur_mora"]       = odbc_result($res,"REC_VAL_JUR_MORA"); 	   
			$dadosRec[$i]["rec_val_mul"]            = odbc_result($res,"REC_VAL_MUL"); 	   
			$dadosRec[$i]["rec_val_alu_base"]       = odbc_result($res,"REC_VAL_ALU_BASE"); 	   
			$dadosRec[$i]["rec_val_des"]            = odbc_result($res,"REC_VAL_DES"); 	   
			$dadosRec[$i]["rec_val_tot"]            = odbc_result($res,"REC_VAL_TOT"); 	   
			$dadosRec[$i]["rec_cod_con"]            = odbc_result($res,"REC_COD_CON"); 	
			$dadosRec[$i]["rec_des_cod_bar"]        = odbc_result($res,"REC_DES_COD_BAR"); 	
			$i++;   
	   }
	   odbc_close($conn);		
	   return $dadosRec; 	   
	}
	
	function carregarDados2($codigo) {
	   $dadosRec = "";
	   $i = 0;
	   $sql = "SELECT * FROM TAB_REC WHERE REC_ISN = $codigo"; 
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   $dadosRec ="";
	   while(@odbc_fetch_row($res)) {
			$dadosRec[$i]["rec_isn"]                = odbc_result($res,"REC_ISN"); 	   
			$dadosRec[$i]["rec_nom_inq"]            = odbc_result($res,"REC_NOM_INQ"); 	   
			$dadosRec[$i]["rec_num_cpf_cgc_inq"]    = odbc_result($res,"REC_NUM_CPF_CGC_INQ"); 	   
			$dadosRec[$i]["rec_des_end"]            = odbc_result($res,"REC_DES_END"); 	   
			$dadosRec[$i]["rec_des_bai"]            = odbc_result($res,"REC_DES_BAI"); 	   
			$dadosRec[$i]["rec_des_end_cob"]        = odbc_result($res,"REC_DES_END_COB"); 	   
			$dadosRec[$i]["rec_des_bai_cob"]        = odbc_result($res,"REC_DES_BAI_COB"); 	   
			$dadosRec[$i]["rec_des_cid_cob"]        = odbc_result($res,"REC_DES_CID_COB"); 	   
			$dadosRec[$i]["rec_num_cep_cob"]        = odbc_result($res,"REC_NUM_CEP_COB"); 	   
			$dadosRec[$i]["rec_des_est_cob"]        = odbc_result($res,"REC_DES_EST_COB"); 	   
			$dadosRec[$i]["rec_nom_prop"]           = odbc_result($res,"REC_NOM_PROP"); 	   
			$dadosRec[$i]["rec_num_cpf_cgc_prop"]   = odbc_result($res,"REC_NUM_CPF_CGC_PROP"); 	   
			$dadosRec[$i]["rec_num"]                = odbc_result($res,"REC_NUM"); 	   
			$dadosRec[$i]["rec_dat_vct"]            = odbc_result($res,"REC_DAT_VCT"); 	   
			$dadosRec[$i]["rec_dat_emi"]            = odbc_result($res,"REC_DAT_EMI"); 	   
			$dadosRec[$i]["rec_dst_car"]            = odbc_result($res,"REC_DAT_CAR"); 	   
			$dadosRec[$i]["rec_dat_reaj"]           = odbc_result($res,"REC_DAT_REAJ"); 	   
			$dadosRec[$i]["rec_val_jur_mora"]       = odbc_result($res,"REC_VAL_JUR_MORA"); 	   
			$dadosRec[$i]["rec_val_mul"]            = odbc_result($res,"REC_VAL_MUL"); 	   
			$dadosRec[$i]["rec_val_alu_base"]       = odbc_result($res,"REC_VAL_ALU_BASE"); 	   
			$dadosRec[$i]["rec_val_des"]            = odbc_result($res,"REC_VAL_DES"); 	   
			$dadosRec[$i]["rec_val_tot"]            = odbc_result($res,"REC_VAL_TOT"); 	   
			$dadosRec[$i]["rec_cod_con"]            = odbc_result($res,"REC_COD_CON"); 	
			$dadosRec[$i]["rec_des_cod_bar"]        = odbc_result($res,"REC_DES_COD_BAR"); 	
			$i++;   
	   }
	   odbc_close($conn);		
	   return $dadosRec; 	   
	}
	
	 function consultarDados($nome,$num) {
	   $con = new Conexao(); 
	   $dadosRec = array();
	   $i = 0;
	   $sql = "SELECT REC_ISN,REC_NOM_INQ,REC_NUM,REC_DAT_VCT,REC_VAL_TOT FROM TAB_REC WHERE 1=1 "; 
		if($nome) {
		   $sql = $sql."AND REC_NOM_INQ LIKE '$nome%' ";
		}
		if($num) {
		   $sql = $sql."AND REC_NUM = '$num' ";
		}
		$sql = $sql." ORDER BY REC_NOM_INQ";
	   $conn=$con->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
	   }
	   while(odbc_fetch_row($res)) {
			$dadosRec[$i]["rec_isn"]                = odbc_result($res,"REC_ISN"); 	   
			$dadosRec[$i]["rec_nom_inq"]            = odbc_result($res,"REC_NOM_INQ"); 	   
			$dadosRec[$i]["rec_num"]                = odbc_result($res,"REC_NUM"); 	   
			$dadosRec[$i]["rec_dat_vct"]            = odbc_result($res,"REC_DAT_VCT"); 	   
			$dadosRec[$i]["rec_val_tot"]            = odbc_result($res,"REC_VAL_TOT"); 	   
			$i++;   
	   }
	   odbc_close($conn);		
	   return $dadosRec; 	   
	}

	 function carregarItens($codigo) {
	   $dadosRec = "";
	   $i = 0;
	   $sql = "SELECT * FROM TAB_IRC WHERE REC_ISN = $codigo ORDER BY IRC_NUM_SEQ"; 
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$itensRec[$i]["irc_tip"]                = odbc_result($res,"IRC_TIP"); 	   
			$itensRec[$i]["irc_des"]                = odbc_result($res,"IRC_DES"); 	   
			$itensRec[$i]["irc_dat"]                = odbc_result($res,"IRC_DAT"); 	   
			$itensRec[$i]["irc_val"]                = odbc_result($res,"IRC_VAL"); 	   
			$i++;
	   }
	   odbc_close($conn);		
	   return $itensRec; 	   
	}
	 function carregarDadosBlt($codigo) { 
	   $dadosBlt = "";
	   $sql = "SELECT * FROM TAB_BLT WHERE REC_ISN = $codigo "; 
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$dadosBlt["blt_isn"]                    = odbc_result($res,"BLT_ISN"); 	   
			$dadosBlt["blt_des_lin_dig"]            = odbc_result($res,"BLT_DES_LIN_DIG"); 	   
			$dadosBlt["blt_cod_bco"]                = odbc_result($res,"BLT_COD_BCO"); 	   
			$dadosBlt["blt_des_loc_pag"]            = odbc_result($res,"BLT_DES_LOC_PAG"); 	   
			$dadosBlt["blt_nom_ced"]                = odbc_result($res,"BLT_NOM_CED"); 	   
			$dadosBlt["blt_des_vct"]                = odbc_result($res,"BLT_DES_VCT"); 	   
			$dadosBlt["blt_des_age_cod_ced"]        = odbc_result($res,"BLT_DES_AGE_COD_CED"); 	   
			$dadosBlt["blt_dat_doc"]                = odbc_result($res,"BLT_DAT_DOC"); 	   
			$dadosBlt["blt_num_doc"]                = odbc_result($res,"BLT_NUM_DOC"); 	   
			$dadosBlt["blt_des_esp_doc"]            = odbc_result($res,"BLT_DES_ESP_DOC"); 	   
			$dadosBlt["blt_des_aceite"]             = odbc_result($res,"BLT_DES_ACEITE"); 	   
			$dadosBlt["blt_dat_proc"]               = odbc_result($res,"BLT_DAT_PROC"); 	   
			$dadosBlt["blt_des_nos_num"]            = odbc_result($res,"BLT_DES_NOS_NUM"); 	   
			$dadosBlt["blt_des_uso_bco"]            = odbc_result($res,"BLT_DES_USO_BCO"); 	   
			$dadosBlt["blt_des_cart"]               = odbc_result($res,"BLT_DES_CART"); 	   
			$dadosBlt["blt_des_esp_moeda"]          = odbc_result($res,"BLT_DES_ESP_MOEDA"); 	   
			$dadosBlt["blt_des_ctr"]                = odbc_result($res,"BLT_DES_CTR"); 	   
			$dadosBlt["blt_val"]                    = odbc_result($res,"BLT_VAL"); 	   
			$dadosBlt["blt_nom_sac"]                = odbc_result($res,"BLT_NOM_SAC"); 	   
			$dadosBlt["blt_des_end"]                = odbc_result($res,"BLT_DES_END"); 	   
			$dadosBlt["blt_des_bai"]                = odbc_result($res,"BLT_DES_BAI"); 	   
			$dadosBlt["blt_des_cid"]                = odbc_result($res,"BLT_DES_CID"); 	   
			$dadosBlt["blt_des_uf"]                 = odbc_result($res,"BLT_DES_UF"); 	   
			$dadosBlt["blt_des_cep"]                = odbc_result($res,"BLT_DES_CEP"); 	   
			$dadosBlt["blt_cod_bar"]            = odbc_result($res,"BLT_COD_BAR"); 	   
	   }
	   odbc_close($conn);		
	   return $dadosBlt; 	   
	}
	 function carregarDadosIbl($codigo) {
	   $dadosIbl = "";
	   $i = 0;
	   $sql = "SELECT * FROM TAB_IBL WHERE BLT_ISN = $codigo ORDER BY IBL_NUM_SEQ_MSG"; 
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(odbc_fetch_row($res)) {
			$dadosIbl[$i]["ibl_isn"]                    = odbc_result($res,"IBL_ISN"); 	   
			$dadosIbl[$i]["ibl_des_msg"]                = odbc_result($res,"IBL_DES_MSG"); 
			$i+=1;	   
	   }
	   odbc_close($conn);		
	   return $dadosIbl; 	   
	}
 }
?>