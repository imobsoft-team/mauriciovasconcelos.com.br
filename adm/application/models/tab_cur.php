<?php
class Tab_cur extends Model{
	function Tab_cur(){
		parent::Model();
	}
	
	function getCur($maximo,$inicio,$busca,$ordem='CUR_ISN ASC') {
			$sql = "SELECT * FROM tab_cur";
		    if($busca){
				$sql.=" WHERE CUR_NOME LIKE '%".utf8_encode($busca)."%'";
			}
			$sql.=" ORDER BY $ordem";
			$sql.=" LIMIT $inicio,$maximo";
			$cur="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $res){
				    $cur[$i]["id"]      =  $res->CUR_ISN; 
					$cur[$i]["nome"]    =  $res->CUR_NOME; 
					if($res->CUR_DAT_CAD){
						$dat   		=  explode(' ',$res->CUR_DAT_CAD);
						$dat   		=  explode('-',$dat[0]);
						$cur[$i]["data"] = $dat[2].'/'.$dat[1].'/'.$dat[0];
					 }else {
						$cur[$i]["data"] = "00/00/0000";
					 } 
					 if($res->CUR_DAT_NASC){
						$dat   		=  explode('-',$res->CUR_DAT_NASC);
						$cur[$i]["dt_nas"] = $dat[2].'/'.$dat[1].'/'.$dat[0];
					 }else {
						$cur[$i]["dt_nas"] = "00-00-0000";
					 } 
					$cur[$i]["bairro"]  =  $res->CUR_BAIRRO; 
					$cur[$i]["cidade"]  =  $res->CUR_CIDADE;
				    $i++;
				}
			}
			return $cur;
	}
	
	function contaRegistros($busca){
		$sql = "SELECT COUNT(CUR_ISN) AS TOTAL FROM tab_cur";
		if($busca){
			$sql.=" WHERE CUR_NOME LIKE '%".utf8_encode($busca)."%'";
		}
		$res = $this->db->query($sql);
		foreach($res->result() as $array){
			$total = $array->TOTAL;
		}
		return $total;
	}
	
	function buscarDados($cod) {     
	  $sql  = "SELECT * FROM tab_cur WHERE CUR_ISN = $cod";
	  $res = $this->db->query($sql);
	  $cur="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $res){
			$cur["id"]        =  $res->CUR_ISN; 
			$cur["nome"]      =  $res->CUR_NOME; 
			 if($res->CUR_DAT_CAD){
				$dat   		=  explode(' ',$res->CUR_DAT_CAD);
				$dat   		=  explode('-',$dat[0]);
				$cur["dt_cad"] = $dat[2].'/'.$dat[1].'/'.$dat[0];
			 }else {
				$cur["dt_cad"] = "00/00/0000";
			 } 
			 if($res->CUR_DAT_NASC){
				$dat   		=  explode('-',$res->CUR_DAT_NASC);
				$cur["dt_nas"] = $dat[2].'/'.$dat[1].'/'.$dat[0];
			 }else {
				$cur["dt_nas"] = "00-00-0000";
			 } 			
			$cur["bairro"]    =  $res->CUR_BAIRRO; 
			$cur["cidade"]    =  $res->CUR_CIDADE; 
			$cur["cpf"]       =  $res->CUR_CPF; 
			$cur["fone"]      =  $res->CUR_FONE; 
			$cur["ender"]     =  $res->CUR_ENDERECO; 
			$cur["cep"]       =  $res->CUR_CEP; 
			$cur["uf"]        =  $res->CUR_UF; 
			$cur["intere"]    =  $res->CUR_INTERESSE; 
			$cur["conhec"]    =  $res->CUR_CONHECIMENTO;
			 
			$cur["ultform"]   =  $res->CUR_ULT_FORM; 
			$cur["periodo"]   =  $res->CUR_PERIODO; 
			$cur["institu"]   =  $res->CUR_INSTITUICAO; 
			$cur["local"]     =  $res->CUR_LOCAL; 
			
			$cur["per_c1"]    =  $res->CUR_PERIODO_C1; 
			$cur["curso1"]    =  $res->CUR_CURSO1; 
			$cur["instit1"]   =  $res->CUR_INSTITUICAO1; 
			
			$cur["per_c2"]    =  $res->CUR_PERIODO_C2; 
			$cur["curso2"]    =  $res->CUR_CURSO2; 
			$cur["instit2"]   =  $res->CUR_INSTITUICAO2; 
			
			$cur["per_c3"]    =  $res->CUR_PERIODO_C3;
			$cur["curso3"]    =  $res->CUR_CURSO3; 
			$cur["instit3"]   =  $res->CUR_INSTITUICAO3; 
			 
			$cur["emp1"]      =  $res->CUR_EMPRESA1; 
			$cur["per_exp1"]  =  $res->CUR_PERIODO_EXP1; 
			$cur["cur_carg1"] =  $res->CUR_CARGO1; 
			$cur["atv_resp1"] =  $res->CUR_ATV_RESP1; 
			$cur["emp2"]      =  $res->CUR_EMPRESA2; 
			$cur["per_exp2"]  =  $res->CUR_PERIODO_EXP2; 
			$cur["cur_carg2"] =  $res->CUR_CARGO2; 
			$cur["atv_resp2"] =  $res->CUR_ATV_RESP2; 
			$cur["emp3"]      =  $res->CUR_EMPRESA3; 
			$cur["per_exp3"]  =  $res->CUR_PERIODO_EXP3; 
			$cur["cur_carg3"] =  $res->CUR_CARGO3; 
			$cur["atv_resp3"] =  $res->CUR_ATV_RESP3; 
			$cur["motivos"]   =  $res->CUR_MOTIVOS; 
			$cur["import"]    =  $res->CUR_IMPORTANCIA; 
			$cur["expect"]    =  $res->CUR_EXPECTATIVAS;  
		  }
	  }
	  return $cur;
	}
	
	function excluirItens($cods) { 	
		$sql  = "DELETE FROM tab_cur WHERE CUR_ISN IN ($cods)";
		$this->db->query($sql);
	} 

}
?>