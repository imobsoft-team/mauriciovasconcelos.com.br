<?php
class Tab_email extends Model{
	function Tab_email(){
		parent::Model();
	}
	
	function getEmails($maximo,$inicio,$busca,$ordem='N.NWS_ISN ASC') {
			$sql = "SELECT N.*,G.GRUP_NOME FROM tab_news N INNER JOIN tab_grup G ON G.GRUP_ISN = N.NWS_GRUPO";
		    if($busca){
				$sql.=" WHERE N.NWS_NOM LIKE '%".$busca."%'";
			}
			$sql.=" ORDER BY $ordem";
			$sql.=" LIMIT $inicio,$maximo";
			$mod="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $array){
				    $mod[$i]["id"]       =  $array->NWS_ISN; 
				    $mod[$i]["email"]    =  $array->NWS_DES_MAIL;
					$mod[$i]["nome"]     =  $array->NWS_NOM; 
				    $mod[$i]["sta"]      =  $array->NWS_STA;
					$mod[$i]["grupo"]    =  $array->NWS_GRUPO; 
					$mod[$i]["nmgrup"]   =  $array->GRUP_NOME;  
				    $i++;
				}
			}
			return $mod;
	}
	
	function contaRegistros($busca){
		$sql = "SELECT COUNT(NWS_ISN) AS TOTAL FROM tab_news";
		if($busca){
			$sql.=" WHERE NWS_NOM LIKE ? ";
		}
		$res = $this->db->query($sql,array('%$busca%'));
		foreach($res->result() as $array){
			$total = $array->TOTAL;
		}
		return $total;
	}
	
	function cadastrarEmail($email,$nome,$sta,$grupo) {	   
	    $sql  = "SELECT * FROM tab_news WHERE NWS_DES_MAIL = '$email'";
	  	$res = $this->db->query($sql);
		if($res->num_rows() == 0){
			  $sql = "INSERT INTO tab_news (NWS_DES_MAIL,NWS_NOM,NWS_STA,NWS_GRUPO,NWS_DAT) VALUES ('$email','$nome',$sta,$grupo,'".date("Y-m-d H:i:s")."')";	 
			  $res = $this->db->query($sql);	
			  $codigo = $this->db->insert_id();	 
			   if ($codigo>0) {		   
					return $codigo;
			   }else {
				   return false;
			   } 
		}else {
		   return 1;
		}	
	}
	
	function buscarEmail($cod) {     
	  $sql  = "SELECT * FROM tab_news WHERE NWS_ISN = $cod";
	  $res = $this->db->query($sql);
	  $mod="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			$mod["id"]       =  $array->NWS_ISN; 
			$mod["email"]    =  $array->NWS_DES_MAIL;
			$mod["nome"]     =  $array->NWS_NOM; 
			$mod["sta"]      =  $array->NWS_STA;
			$mod["grupo"]    =  $array->NWS_GRUPO;
		  }
	  }
	  return $mod;
	}
	
	function alterarEmail($email,$nome,$sta,$grupo,$id) {     
	     $sql = "UPDATE tab_news SET NWS_DES_MAIL = '$email', NWS_NOM = '$nome', NWS_GRUPO = $grupo, NWS_STA = $sta WHERE NWS_ISN = $id";					   	 
		 $this->db->query($sql);
		 return 1;
	}
	
	function excluirItens($cods) {
		$sql  = "DELETE FROM tab_news WHERE NWS_ISN IN ($cods)";
		$this->db->query($sql);
	} 
	
	function getGrupos(){
	  $sql  = "SELECT * FROM tab_grup";
	  $res = $this->db->query($sql);
	  $mod="";
	  if($res->num_rows() > 0){
		  $i=0;
		  foreach($res->result() as $array){
			$mod[$i]["id"]       =  $array->GRUP_ISN; 
			$mod[$i]["nome"]     =  utf8_decode($array->GRUP_NOME); 
			$i++;
		  }
	  }
	  return $mod;
	}
	
	function getEmailsGrup($grupo) {
			$sql = "SELECT * FROM tab_news";
		    if($grupo){
				$sql.=" WHERE NWS_GRUPO = $grupo";
			}
			$mod="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $array){
				    $mod[$i]["id"]       =  $array->NWS_ISN; 
				    $mod[$i]["email"]    =  $array->NWS_DES_MAIL;
					$mod[$i]["nome"]     =  $array->NWS_NOM; 
				    $mod[$i]["sta"]      =  $array->NWS_STA;
					$mod[$i]["grupo"]    =  $array->NWS_GRUPO; 
				    $i++;
				}
			}
			return $mod;
	}

}
?>