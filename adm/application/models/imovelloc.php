<?php
require_once('conexao.php');
 class ImovelLoc extends Conexao {
     function ImovelLoc(){
	 	parent::Conexao();
	 }
	 function getDestaques($tam=130){
		$idm = $_SESSION['idioma'];	   
		if($idm == 2) {
		   $textDestq = "IMO_DES_TXT_DEST_ING";
		}else if($idm == 3){
		   $textDestq = "IMO_DES_TXT_DEST_ESP";
		}else {
		   $textDestq = "IMO_DES_TXT_DEST";
		}
		$sql = "SELECT IMO.IMO_ISN,IMO_COD_ESP,IMO.IMO_VAL_ALU,IMO.IMO_DES_TXT_DEST, IMO.IMO_DES_BAI, IMO.IMO_DES_END, TIM.TIM_DES, IMI.IMI_NOM FROM TAB_IMO IMO, TAB_TIM TIM, TAB_IMI IMI WHERE IMO.IMO_TIP_DEST = 1 AND IMO.IMO_TIP_DEST_PRIM_PAG = 1AND TIM.TIM_ISN = IMO.TIM_ISN AND IMI.IMO_ISN = IMO.IMO_ISN AND IMI.IMI_TIP_DEST = 1 ORDER BY IMO.IMO_ISN";
	    $i = 0;
	    $codigo = "";
	    $conn=$this->connDb();	
	    $res = @odbc_exec($conn,$sql);
	    if (odbc_errormsg() != "") { 
		   return false;
	    }
	    while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]        = odbc_result($res,"IMO_ISN"); 
			$codigo[$i]["cod_esp"]            = odbc_result($res,"IMO_COD_ESP"); 	   
			$codigo[$i]["imo_des_est"]    = odbc_result($res,"IMO_DES_TXT_DEST"); 
			$codigo[$i]["tim_des"]        = odbc_result($res,"TIM_DES");
			$codigo[$i]["imo_des_bai"]    = odbc_result($res,"IMO_DES_BAI");	
			$codigo[$i]["imo_des_end"]    = odbc_result($res,"IMO_DES_END"); 
			$codigo[$i]["imi_nom"]        = odbc_result($res,"IMI_NOM");  
			$codigo[$i]["imo_val_alu"]    = odbc_result($res,"IMO_VAL_ALU");			
		 	$i++; 
	   }
	   for($x=0;$x<count($codigo);$x++){		 	
			$codigo[$x]["foto"] = $this->buscarFoto($codigo[$x]["imo_isn"],$this->CaminhoFoto(),$tam);
	   }
	   @odbc_close($conn);		
	   return $codigo;		
	
	  }
	 
	  public function buscarFoto($imo_isn,$caminho,$tamanho=130){
		$sql = "SELECT *  FROM TAB_IMI WHERE IMO_ISN = $imo_isn AND IMI_TIP_DEST = 1";
		$conn2=$this->connDb();	
		$res2 = @odbc_exec($conn2,$sql);
		if (odbc_errormsg() != "") { 
			return false;
		}		
		@odbc_fetch_row($res2);
		if(@file_exists($caminho.odbc_result($res2,"IMI_NOM"))){
			$foto = 'application/helpers/inc_thumb.php?img=../../'.$caminho.odbc_result($res2,"IMI_NOM").'&size='.$tamanho; 
		}		    
		if(empty($foto)){
			$foto = 'application/helpers/inc_thumb.php?img=../images/indisponivel.jpg&size=160';
		} 
		@odbc_close($conn2);		
		return $foto;
	 }
	 function consultarCidades() { 
		   $i = 0;
		   $cidade = array();
		   $sql = "SELECT DISTINCT IMO_DES_LOC FROM TAB_IMO ORDER BY IMO_DES_LOC";
		   $conn=$this->connDb();	
		   $res = @odbc_exec($conn,$sql);
		   if (odbc_errormsg() != "") { 
			   return false;
		   }
		   while(@odbc_fetch_row($res)) {
				$cidade[$i]["imo_des_loc"]           = odbc_result($res,"IMO_DES_LOC"); 	   
				$i++; 
		   }
		   odbc_close($conn);		
		   return $cidade;
	 }
	 
	function consultarCidAlu(){
		$con = new Conexao();
		$conn=$con->connDb();
		
		$sql ="SELECT DISTINCT(IMO_DES_LOC) FROM TAB_IMO";
		
		$res = @odbc_exec($conn,$sql);
		
		if (odbc_errormsg() != "") { 
		   return false;
	   }
		
		$bai = array();
		$i=0; 
		while(@odbc_fetch_row($res)) {
			$imo[$i]["imo_des_loc"] = odbc_result($res,"IMO_DES_LOC"); 	   
			$i++;
	   }
	   odbc_close($conn);		
	   return $imo;
	
	}


	 function consultarTipo() {
	   $i = 0;
	   $tipoesp = array();
	   $sql = "SELECT * FROM TAB_TIM ORDER BY TIM_DES";
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") {
		   return false;
	   } 
	   while(@odbc_fetch_row($res)) {
         $tipoesp[$i]["tim_id"]           = odbc_result($res,"TIM_ISN"); 	   
			$tipoesp[$i]["tim_des"]          = odbc_result($res,"TIM_DES"); 
            $i++; 
	   }
	   odbc_close($conn);		
	   return $tipoesp;
	 
	 }

	 function consultarBairros() {
	   $num_arg = func_num_args();	
	   $i = 0;
	   $bairro = array();
	   if($num_arg == 1){
	   		$cid = func_get_arg(0);
			$sql = "SELECT DISTINCT IMO_DES_BAI FROM TAB_IMO WHERE IMO_DES_LOC = '$cid' ORDER BY IMO_DES_BAI";
	   }else{
	   		$sql = "SELECT DISTINCT IMO_DES_BAI FROM TAB_IMO ORDER BY IMO_DES_BAI";
	   }
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$bairro[$i]["imo_des_bai"]           = odbc_result($res,"IMO_DES_BAI"); 	   
            $i++; 
	   }
	   odbc_close($conn);		
	   return $bairro;
	 
	 }

	 function consultarImoveis($bairro,$tipo,$valini,$valfim,$cidade,$tam=300,$local) {
	   $br = "";
	   if(is_array($bairro)){
			$cont = count($bairro);
			if($bairro[0] == "TODOS" || $bairro[0]==" "){
				$br="";
			}else {		
				for($i=0;$i<$cont;$i++) {		  
				   if($cont == 1) {
					  $br = "AND IMO.IMO_DES_BAI='".$bairro[$i]."'";
				   }else if($cont > 1) {
					 if($i==0){
						$br.= " AND ( IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }else if($i == ($cont - 1)) {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' ) ";
					 }else {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }
				   }
				   
				}
			}
	   }else {
	   	  $br = "";
	   }
	   $bairro = $br;
	   $i = 0;
	   $imovel = array();
	   if(!empty($cidade)){
			$sql = "SELECT IMO.IMO_ISN,IMO_COD_ESP, IMO.IMO_DES_BAI, IMO.IMO_DES_TXT_DEST,IMO.IMO_DES_LOC, IMO.IMO_DES_END, IMO.IMO_VAL_ALU, IMO.IMO_VAL_ARE, TIM.TIM_DES 
				   FROM TAB_IMO IMO, TAB_TIM TIM 
				   WHERE TIM.TIM_ISN = IMO.TIM_ISN 
				   AND TIM.TIM_ISN = $tipo 
				   $bairro AND IMO.IMO_DES_LOC = '$cidade' ";
		}else if(empty($cidade) && !empty($tipo)){
			$sql = "SELECT IMO.IMO_ISN,IMO_COD_ESP, IMO.IMO_DES_BAI, IMO.IMO_DES_TXT_DEST,IMO.IMO_DES_LOC, IMO.IMO_DES_END, IMO.IMO_VAL_ALU, IMO.IMO_VAL_ARE, TIM.TIM_DES 
				   FROM TAB_IMO IMO, TAB_TIM TIM 
				   WHERE TIM.TIM_ISN = IMO.TIM_ISN 
				   AND TIM.TIM_ISN = $tipo 
				   $bairro ";
		}else {
			$sql = "SELECT IMO.IMO_ISN,IMO_COD_ESP, IMO.IMO_DES_BAI, IMO.IMO_DES_TXT_DEST,IMO.IMO_DES_LOC, IMO.IMO_DES_END, IMO.IMO_VAL_ALU, IMO.IMO_VAL_ARE, TIM.TIM_DES 
				   FROM TAB_IMO IMO, TAB_TIM TIM 
				   WHERE TIM.TIM_ISN = IMO.TIM_ISN $bairro ";
		}		
			   
		if($valini) { 
	      $sql = $sql."AND IMO.IMO_VAL_ALU >= $valini ";
		}	
		if($valfim) { 
	      $sql = $sql."AND IMO.IMO_VAL_ALU <= $valfim ";  
		}	
	   $sql = $sql."ORDER BY IMO.IMO_ISN";
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 
			$imovel[$i]["cod_esp"]           = odbc_result($res,"IMO_COD_ESP");	   
			$imovel[$i]["imo_des_bai"]       = odbc_result($res,"IMO_DES_BAI"); 	   
			$imovel[$i]["imo_des_end"]       = odbc_result($res,"IMO_DES_END"); 	   
			$imovel[$i]["imo_val_alu"]       = odbc_result($res,"IMO_VAL_ALU"); 	   
			$imovel[$i]["imo_val_are"]       = odbc_result($res,"IMO_VAL_ARE");
			$imovel[$i]["imo_des_loc"]       = odbc_result($res,"IMO_DES_LOC");
			$imovel[$i]["imo_des_txt_dest"]  = odbc_result($res,"IMO_DES_TXT_DEST"); 	   
			$imovel[$i]["tim_des"]           = odbc_result($res,"TIM_DES"); 
				   
            $i++; 
	   }
	   for($x=0;$x<count($imovel);$x++){
			$imovel[$x]["caracs"] = $this->consultarCaracteristicaPai($imovel[$x]["imo_isn"]);
			$imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
			$imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
	   }
	   odbc_close($conn);		
	   return $imovel;
	}

	 function carregarDados($codigo) {
	   $con = new Conexao(); 
	   $i = 0;
	   $imovel = array();
	   $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_END,IMO_VAL_ALU,TIM_DES 
	   FROM TAB_IMO IMO, TAB_TIM TIM WHERE TIM.TIM_ISN = IMO.TIM_ISN 
	   AND IMO.IMO_ISN = $codigo
	   ORDER BY IMO_ISN";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
			$imovel[$i]["imo_des_bai"]       = odbc_result($res,"IMO_DES_BAI"); 	   
			$imovel[$i]["imo_des_end"]       = odbc_result($res,"IMO_DES_END"); 	   
			$imovel[$i]["imo_val_alu"]       = odbc_result($res,"IMO_VAL_ALU"); 	   
			$imovel[$i]["tim_des"]           = odbc_result($res,"TIM_DES"); 	   
            $i++; 
	   }
	   odbc_close($conn);		
	   return $imovel;
	}

	 function carregarImoveisAVC($bairro,$tipo,$codigoVal,$codigoQtde,$codigoAlt,$codigoPos,$cidade,$valini,$valfim,$tam=300,$local) {
	   $imovel = "";
	    $br = "";
	   if(is_array($bairro)){
			$cont = count($bairro);
			if($bairro[0] == "TODOS" || $bairro[0]==" "){
				$br="";
			}else {		
				for($i=0;$i<$cont;$i++) {		  
				   if($cont == 1) {
					  $br = "AND IMO.IMO_DES_BAI='".$bairro[$i]."'";
				   }else if($cont > 1) {
					 if($i==0){
						$br.= " AND ( IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }else if($i == ($cont - 1)) {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' ) ";
					 }else {
						$br.= " IMO.IMO_DES_BAI='".$bairro[$i]."' OR ";
					 }
				   }
				   
				}
			}
	   }else {
	   	  $br = "";
	   }	
	   $bairro = $br;   
	   $sql = "SELECT IMO_ISN,IMO_DES_BAI,IMO_DES_END,IMO_VAL_ALU,IMO_VAL_ARE,TIM_DES 
	           FROM TAB_IMO IMO LEFT JOIN TAB_TIM TIM ON TIM.TIM_ISN = IMO.TIM_ISN 
			   WHERE TIM.TIM_ISN = $tipo $bairro";
		   
	   if(($valini > 0) && ($valfim > 0)) {
	      $sql = $sql."AND IMO.IMO_VAL_ALU >= $valini AND IMO.IMO_VAL_ALU <= $valfim ";
	   }else if(($valini > 0) && ($valfim == 0)) {
	      $sql = $sql."AND IMO.IMO_VAL_ALU >= $valini";
	   }else if(($valini == 0) && ($valfim > 0)) {
	      $sql = $sql."AND IMO.IMO_VAL_ALU <= $valfim";
	   }else if(($valini == 0) && ($valfim == 0)) {
	      $sql = $sql;
	   }
	   
	   if($codigoVal) {
	      $str = implode(",",$codigoVal);
		  $sql = $sql." AND IMO_ISN IN (".$str.")";
	   }
	   if($codigoQtde) {
	   	   $str = implode(",",$codigoQtde);
		   $sql = $sql." AND IMO_ISN IN (".$str.")";
		}
	   if($codigoAlt) {
	      $str = implode(",",$codigoAlt);
		  $sql = $sql." AND IMO_ISN IN (".$str.")";
	   }
	   if($codigoPos) {
			$str = implode(",",$codigoPos);
		    $sql = $sql." AND IMO_ISN IN (".$str.")";
	   }
	   $sql = $sql."AND IMO.IMO_DES_LOC = '". $cidade."' ORDER BY IMO_VAL_ALU";
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   $i = 0;
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
			$imovel[$i]["imo_des_bai"]       = odbc_result($res,"IMO_DES_BAI"); 	   
			$imovel[$i]["imo_des_end"]       = odbc_result($res,"IMO_DES_END"); 	   
			$imovel[$i]["imo_val_alu"]       = odbc_result($res,"IMO_VAL_ALU"); 	   
			$imovel[$i]["imo_val_are"]       = odbc_result($res,"IMO_VAL_ARE"); 	   
			$imovel[$i]["tim_des"]           = odbc_result($res,"TIM_DES"); 	   
            $i++; 
	   }
	   odbc_close($conn);
	   is_array($imovel)?$quant=count($imovel):$quant=0;
	   for($x=0;$x<count($imovel);$x++){
			$imovel[$x]["caracs"] = $this->consultarCaracteristicaPai($imovel[$x]["imo_isn"]);
			$imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
			$imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
	   }		
	   return $imovel;
	}
	 function carregarCodigosBR($bairro) {
	   $con = new Conexao(); 
	   $i = 0;
	   $codimo = array();
	   if($bairro == "") {
	      $sql = "SELECT IMO_ISN   FROM TAB_IMO IMO  ORDER BY IMO_ISN";
	   }
	   else {
	      $sql = "SELECT IMO_ISN   FROM TAB_IMO IMO WHERE  $bairro 
	              ORDER BY IMO_ISN";
	   }
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codimo[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
            $i++; 
	   }
	   odbc_close($conn);		
	   return $codimo;
	}

	 function carregarCodigosTP($tipo) {
	   $con = new Conexao(); 
	   $i = 0;
	   $codimo = array();
       $sql = "SELECT IMO_ISN  FROM TAB_IMO WHERE TIM_ISN = $tipo
	           ORDER BY IMO_ISN";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codimo[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
            $i++; 
	   }
	   odbc_close($conn);		
	   return $codimo;
	}

	 function carregarCodigosALT($id,$val) {
	   $i = 0;
	   $codimo = "";
	   $sql = "SELECT IMO_ISN  FROM TAB_IMC WHERE CAR_ISN = $id AND IMC_TIP_CON = $val ORDER BY IMO_ISN";
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		  return false;
	   }
	   while(@odbc_fetch_row($res)) {
		  $codimo[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
		  $i++; 
	   }
	  odbc_close($conn);
	  $ret="";
	   is_array($codimo)?$quant=count($codimo):$quant=0;
	   for($x=0;$x<$quant;$x++){
	   	  if($x==$quant-1){
		  	$ret.=$codimo[$x]['imo_isn'];
		  }else {
		  	$ret.=$codimo[$x]['imo_isn'].',';
		  } 
	   }		
	  return $ret;
	 
	}

	 function carregarCodigosQTDE($id,$val_ini,$val_fim) {
	   $i = 0;
	   $codimo = "";	   
	   if(empty($val_ini)) {
	      $val_ini = "";
	   }
	   if(empty($val_fim)) {
	      $val_fim = "";
	   }	   
	   if(($val_ini == "") && ($val_fim)) {
		   $sql = "SELECT IMO_ISN  FROM TAB_IMC WHERE CAR_ISN = $id
				   AND IMC_QTD <= $val_fim
				   ORDER BY IMO_ISN";
	   }else if(($val_fim == "") && ($val_ini)) {
		   $sql = "SELECT IMO_ISN  FROM TAB_IMC WHERE CAR_ISN = $id
				   AND IMC_QTD >= $val_ini
				   ORDER BY IMO_ISN";
	   }else if(($val_ini == "") && ($val_fim == "")) {
		   return $codimo;
	   }else if(($val_ini) && ($val_fim)) {
		   $sql = "SELECT IMO_ISN  FROM TAB_IMC WHERE CAR_ISN = $id
				   AND IMC_QTD >= $val_ini AND IMC_QTD <= $val_fim
				   ORDER BY IMO_ISN";
	   }
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codimo[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
            $i++; 
	   }
	   odbc_close($conn);
	   $ret="";
	   is_array($codimo)?$quant=count($codimo):$quant=0;
	   for($x=0;$x<$quant;$x++){
	   	  if($x==$quant-1){
		  	$ret.=$codimo[$x]['imo_isn'];
		  }else {
		  	$ret.=$codimo[$x]['imo_isn'].',';
		  } 
	   }
	   return $ret;
	}
	
	function carregarCodigosPOS($id) {
	   $i = 0;
	   $codimo = "";
       $sql = "SELECT IMO_ISN FROM TAB_IMC WHERE DCA_ISN = $id ORDER BY IMO_ISN";	
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codimo[$i]["imo_isn"] = odbc_result($res,"IMO_ISN"); 	   
            $i++; 
	   }
	   odbc_close($conn);
	   $ret="";
	   is_array($codimo)?$quant=count($codimo):$quant=0;
	   for($x=0;$x<$quant;$x++){
	   	  if($x==$quant-1){
		  	$ret.=$codimo[$x]['imo_isn'];
		  }else {
		  	$ret.=$codimo[$x]['imo_isn'].',';
		  } 
	   }
	   return $ret;
	}

	 function carregarCodigosVALOR($id,$val_ini,$val_fim) {
	   $i = 0;
	   $codimo = "";	   
	   if(empty($val_ini)) {
	      $val_ini = "";
	   }
	   if(empty($val_fim)) {
	      $val_fim = "";
	   }	 	   
	   if(($val_ini == "") && ($val_fim)) {
		   $sql = "SELECT IMO_ISN  FROM TAB_IMC WHERE CAR_ISN = $id
				   AND IMC_VAL <= $val_fim
				   ORDER BY IMO_ISN";
		}else if(($val_fim == "") && ($val_ini)) {
		   $sql = "SELECT IMO_ISN  FROM TAB_IMC WHERE CAR_ISN = $id
				   AND IMC_VAL >= $val_ini
				   ORDER BY IMO_ISN";
		}else if(($val_ini == "") && ($val_fim == "")) {
		   return $codimo;
		}else if(($val_ini) && ($val_fim)) {
		   $sql = "SELECT IMO_ISN  FROM TAB_IMC WHERE CAR_ISN = $id
		           AND IMC_VAL >= $val_ini AND IMC_VAL <= $val_fim
		           ORDER BY IMO_ISN";
		}
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codimo[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
            $i++; 
	   }
	   odbc_close($conn);
	   $ret="";
	   is_array($codimo)?$quant=count($codimo):$quant=0;
	   for($x=0;$x<$quant;$x++){
	   	  if($x==$quant-1){
		  	$ret.=$codimo[$x]['imo_isn'];
		  }else {
		  	$ret.=$codimo[$x]['imo_isn'].',';
		  } 
	   }
	   return $ret;
	}
	
	public function buscarFoto2($imo_isn,$caminho,$tamanho=300,$local=''){
		$sql = "SELECT *  FROM TAB_IMI WHERE IMO_ISN = $imo_isn AND IMI_TIP_DEST = 1";
		$conn=$this->connDb();	
		$res = @odbc_exec($conn,$sql);
		if (odbc_errormsg() != "") { 
			return false;
		}		
		@odbc_fetch_row($res);
		if(@file_exists($caminho.odbc_result($res,"IMI_NOM"))){
			$foto = $local.'application/helpers/inc_thumb.php?img=../../'.$caminho.odbc_result($res,"IMI_NOM").'&size='.$tamanho;
		}		
		if(empty($foto)){
			$sql = "SELECT * FROM TAB_IMI WHERE IMO_ISN = ".$imo_isn." ORDER BY IMI_NUM ASC ";
			$res = @odbc_exec($conn,$sql);
			if (odbc_errormsg() != "") { 
				return false;
			}
			while(@odbc_fetch_row($res)) {
			 if(@file_exists($caminho.odbc_result($res,"IMI_NOM"))){ 	
				$foto = $local.'application/helpers/inc_thumb.php?img=../../'.$caminho.odbc_result($res,"IMI_NOM").'&size='.$tamanho;
				break;
			  }
			}
		}		
		if(empty($foto)){
			$foto = $local.'application/helpers/inc_thumb.php?img=../images/indisponivel.jpg&size=80';
		} 
		@odbc_close($conn);		
		return $foto;
	}
	
	public function carregarFotos($cod,$caminho,$tamanho=130,$local='') {
	   $sql = "SELECT * FROM TAB_IMI  WHERE IMO_ISN = $cod ORDER BY IMI_NUM";
	   $i = 0;	   
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") {
		   return false;
	   } 
	   $img = "";
	   while(@odbc_fetch_row($res)) {
		 if(@file_exists($caminho.odbc_result($res,"IMI_NOM"))){
			$img[$i]["descricao"] = odbc_result($res,"IMI_DES"); 	   
			$img[$i]["nome"] = $local.'application/helpers/inc_thumb.php?img=../../'.$caminho.odbc_result($res,"IMI_NOM").'&size='.$tamanho; 
			$img[$i]["codigo"]    = odbc_result($res,"IMI_ISN"); 
			$i++; 
		 }
	   }
	   odbc_close($conn);
	   return $img;
	 }

	 function consultarImovel($codigo,$tam=300,$local){
	   $idm = $_SESSION['idioma'];
	   if(is_numeric($codigo)){
	   		$sql = "SELECT IMO.*,TIM.TIM_DES 
	   		   FROM TAB_IMO IMO, TAB_TIM TIM 
			   WHERE TIM.TIM_ISN = IMO.TIM_ISN 
			   AND IMO.IMO_ISN = $codigo	   
			   ORDER BY IMO.IMO_ISN";
		}else {
			$sql = "SELECT IMO.*,TIM.TIM_DES 
	   		   FROM TAB_IMO IMO, TAB_TIM TIM 
			   WHERE TIM.TIM_ISN = IMO.TIM_ISN 
			   AND IMO.IMO_COD_ESP = '$codigo'	   
			   ORDER BY IMO.IMO_ISN";
		}
	   $i = 0;
	   $imovel = array();
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 
			$imovel[$i]["cod_esp"]           = odbc_result($res,"IMO_COD_ESP"); 	   
			$imovel[$i]["imo_des_bai"]       = odbc_result($res,"IMO_DES_BAI"); 	   
			$imovel[$i]["imo_des_end"]       = odbc_result($res,"IMO_DES_END");
			$imovel[$i]["imo_des_loc"]       = odbc_result($res,"IMO_DES_LOC");	   
			$imovel[$i]["imo_val_ven"]       = odbc_result($res,"IMO_VAL_ALU"); 	   
			$imovel[$i]["imo_val_are"]       = odbc_result($res,"IMO_VAL_ARE");
			$imovel[$i]["imo_val_are_priv"]  = odbc_result($res,"IMO_VAL_ARE");
			$imovel[$i]["imo_tip_usar"]      = odbc_result($res,"IMO_TIP_USAR_TXT_DET"); 
			$imovel[$i]["imo_dest_det"]      = odbc_result($res,"IMO_DES_TXT_DET"); 
			$imovel[$i]["imo_dest_det_2"]    = odbc_result($res,"IMO_DES_TXT_DET_2");
			$imovel[$i]["tim_des"] = odbc_result($res,"TIM_DES");   
            $i++; 
	   }
	   for($x=0;$x<count($imovel);$x++){
	   		$imovel[$x]["caracs"] = $this->consultarCaracteristicaPai($codigo);
			$imovel[$x]["foto"] = $this->buscarFoto2($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
			$imovel[$x]["fotos"] = $this->carregarFotos($imovel[$x]["imo_isn"],$this->CaminhoFoto(),$tam,$local);
	   }
	   odbc_close($conn);		
	   return $imovel;
	}

	 function consultarCaracteristicaPai($codigo) {
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descCarDes = "CAR_DES_ING"; 
	   }else if($idm == 3){
	   	  $descCarDes = "CAR_DES_ESP"; 
	   }else {
	      $descCarDes = "CAR_DES"; 
	   }  
	   $i = 0;
	   $caracPai = array();
	   $caracPaiVal = array();
	   $sql = "SELECT IMC.*, CAR.CAR_DES,CAR.$descCarDes,CAR.CAR_TIP, CAR.CAR_DES_UNI FROM TAB_IMC IMC
              LEFT JOIN TAB_CAR CAR ON CAR.CAR_ISN = IMC.CAR_ISN
              WHERE IMC.IMO_ISN = $codigo AND
              CAR.CAR_ISN_PAI = 0 
              ORDER BY CAR.CAR_DES";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		  return false;
	   }
	   $carsFilha = array();
	   while(@odbc_fetch_row($res)) {
	   		$caracPai[$i]["imc_isn"]           = odbc_result($res,"IMC_ISN"); 	   
			$caracPai[$i]["imc_car_isn"]       = odbc_result($res,"CAR_ISN"); 
			$caracPai[$i]["car_car_tip"]       = odbc_result($res,"CAR_TIP"); 	   
			$caracPai[$i]["car_car_des"]       = odbc_result($res,$descCarDes); 
			$caracPai[$i]["car_car_des_uni"]   = odbc_result($res,"CAR_DES_UNI"); 
			$caracPai[$i]["imc_qtd"]           = odbc_result($res,"IMC_QTD");
			$caracPai[$i]["imc_des"]           = odbc_result($res,"IMC_DES");
			$caracPai[$i]["imc_val"]           = odbc_result($res,"IMC_VAL");
			$caracPai[$i]["imc_tip_con"]       = odbc_result($res,"IMC_TIP_CON");
			$caracPai[$i]["imc_dca_isn"]       = odbc_result($res,"DCA_ISN");
			if(empty($caracPai[$i]["car_car_des"])){
				$caracPai[$i]["car_car_des"] = odbc_result($res,"CAR_DES"); 
			}
			$i++;
		}	
		for($i=0;$i<count($caracPai);$i++) {
			if($caracPai[$i]["car_car_tip"] == 1) {
			   if($caracPai[$i]["imc_tip_con"] == 0) {
			      $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			      $caracPaiVal[$i]["imc_tip_con"] = "NAO";
			   }else if($caracPai[$i]["imc_tip_con"] == 1) {
			      $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			      $caracPaiVal[$i]["imc_tip_con"] = "SIM";
			   }
			}else if($caracPai[$i]["car_car_tip"] == 2) {
			   $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			   $caracPaiVal[$i]["imc_qtd"] = $caracPai[$i]["imc_qtd"];
			}else if($caracPai[$i]["car_car_tip"] == 3) {
			      $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			      $caracPaiVal[$i]["dca_des"]      = $this->dcaValor($caracPai[$i]["imc_dca_isn"]);
			}else if($caracPai[$i]["car_car_tip"] == 4) {
			   $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			   $caracPaiVal[$i]["imc_des"] = $caracPai[$i]["imc_des"];
			}else if($caracPai[$i]["car_car_tip"] == 5) {
			   $caracPaiVal[$i]["car_car_desPai"] = $caracPai[$i]["car_car_des"] ;
			   $caracPaiVal[$i]["imc_val"] = $caracPai[$i]["imc_val"];
			   $caracPaiVal[$i]["car_car_des_uni"] = $caracPai[$i]["car_car_des_uni"] ;
			}
            $caracPaiVal[$i]["car_car_tipPai"] = $caracPai[$i]["car_car_tip"]; 
			$caracPaiVal[$i]["imo_obs"] = $this->obsValor($codigo); 
		}
		for($i=0;$i < count($caracPai);$i++) {
		   $caracPai[$i]["imc_isn"] = $caracPai[$i]["imc_isn"];
			$caracPai[$i]["imc_car_isn"] = $caracPai[$i]["imc_car_isn"];
			$caracPai[$i]["car_car_tip"] = $caracPai[$i]["car_car_tip"];
			$caracPai[$i]["car_car_des"] = $caracPai[$i]["car_car_des"];
			$caracPai[$i]["car_car_des_uni"] = $caracPai[$i]["car_car_des_uni"];
			$caracPai[$i]["imc_qtd"] = $caracPai[$i]["imc_qtd"];
			$caracPai[$i]["imc_des"]  = $caracPai[$i]["imc_des"];
			$caracPai[$i]["imc_val"]  = $caracPai[$i]["imc_val"];
			$caracPai[$i]["imc_tip_con"] = $caracPai[$i]["imc_tip_con"]; 
			$caracPai[$i]["imc_dca_isn"] = $caracPai[$i]["imc_dca_isn"];
		    $caracPaiVal[$i]["caracteristicas"]  = $this->consultarCaracteristicaFilha($codigo,$caracPai[$i]["imc_car_isn"]);
		}	
	   odbc_close($conn);		
	   return $caracPaiVal;
	 }
	 
	 function dcaValor($numero) {
	   $sql = "SELECT DCA.DCA_DES FROM TAB_DCA DCA WHERE DCA.DCA_ISN = ".$numero;
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
		  $dca_des  = odbc_result($res,"DCA_DES");
	   }   
	   odbc_close($conn);		
	   return $dca_des;
	}
	
	function obsValor($numero) {
	   $sql = "SELECT IMO_OBS FROM TAB_IMO WHERE IMO_ISN = ".$numero;
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
		  $obs_des  = odbc_result($res,"IMO_OBS");
	   }	   
	   odbc_close($conn);		
	   return $obs_des;
	}
	
	function consultarCaracteristicaFilha($codigo,$car_isn) {
	   $idm = $_SESSION['idioma'];
	   if($idm == 2) {
	      $descCarDes = "CAR_DES_ING"; 
	   }else if($idm == 3){
	   	  $descCarDes = "CAR_DES_ESP"; 
	   }else {
	      $descCarDes = "CAR_DES"; 
	   }  
	   $caracFilha = array();
	   $caracFilhaVal = array();
	   $carsPai = array();
	   $i = 0;
	   $sql = "SELECT CAR.CAR_DES,CAR.$descCarDes,CAR.CAR_TIP, IMC.*  
			   FROM TAB_IMC IMC
			   LEFT JOIN TAB_CAR CAR ON CAR.CAR_ISN = IMC.CAR_ISN 
			   WHERE IMC.IMO_ISN = $codigo AND
			   IMC.CAR_ISN IN (SELECT CAR_ISN FROM TAB_CAR WHERE CAR_ISN_PAI =".$car_isn.")ORDER BY IMC_NUM_SEQ";
	   $conn=$this->connDb();	
	   $res = odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		  return false;
	   }
	   while(@odbc_fetch_row($res)) {			
			$caracFilha[$i]["imc_isn"]           = odbc_result($res,"IMC_ISN"); 	   
			$caracFilha[$i]["imc_car_isn"]       = odbc_result($res,"CAR_ISN"); 	   
			$caracFilha[$i]["car_car_des"]       = odbc_result($res,$descCarDes); 	   
			$caracFilha[$i]["car_car_tip"]       = odbc_result($res,"CAR_TIP"); 	   
			$caracFilha[$i]["imc_tip_con"]       = odbc_result($res,"IMC_TIP_CON"); 	   
			$caracFilha[$i]["imc_qtd"]           = odbc_result($res,"IMC_QTD"); 	   
			$caracFilha[$i]["imc_val"]           = odbc_result($res,"IMC_VAL"); 	   
			$caracFilha[$i]["imc_des"]           = odbc_result($res,"IMC_DES"); 	   
			$caracFilha[$i]["imc_num_seq"]       = odbc_result($res,"IMC_NUM_SEQ"); 	   
			$caracFilha[$i]["dca_isn"]           = odbc_result($res,"DCA_ISN"); 
			if(empty($caracFilha[$i]["car_car_des"])){
				$caracFilha[$i]["car_car_des"]  = odbc_result($res,"CAR_DES");
			}	
			$i++;
	   }	
		for($i=0;$i<count($caracFilha);$i++) {
			if($caracFilha[$i]["car_car_tip"] == 1) {
			   $caracFilha[$i]["imc_tip_con"];
			   if($caracFilha[$i]["imc_tip_con"] == 0) {
				  $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
				  $caracFilhaVal[$i]["imc_tip_con"] = "NAO";
			   }else if($caracFilha[$i]["imc_tip_con"] == 1) {
				  $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
				  $caracFilhaVal[$i]["imc_tip_con"] = "SIM";
			   }
			}else if($caracFilha[$i]["car_car_tip"] == 2) {
			   $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
			   $caracFilhaVal[$i]["imc_qtd"] = $caracFilha[$i]["imc_qtd"];
			}else if($caracFilha[$i]["car_car_tip"] == 3) {
				  $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
				  $caracFilhaVal[$i]["dca_des"]      = $this->dcaValor($caracFilha[$i]["dca_isn"]);
			}else if($caracFilha[$i]["car_car_tip"] == 4) {
			   $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
			   $caracFilhaVal[$i]["imc_des"] = $caracFilha[$i]["imc_des"];
			}else if($caracFilha[$i]["car_car_tip"] == 5) {
			   $caracFilhaVal[$i]["car_car_des"] = $caracFilha[$i]["car_car_des"] ;
			   $caracFilhaVal[$i]["imc_val"] = $caracFilha[$i]["imc_val"];
			}
			$caracFilhaVal[$i]["imc_num_seq"] = $caracFilha[$i]["imc_num_seq"]; 
			$caracFilhaVal[$i]["car_car_tipFilha"] = $caracFilha[$i]["car_car_tip"]; 
		}
	   odbc_close($conn);		
	   return $caracFilhaVal;
	 }
	
	 function buscarCaracteristicaPai() {
       $crPai = new Carac_Pai();
	   $caracPai = array();
	   $imoveis = $_SESSION["arrayImoveisIMF"];
	   $qtd = count($imoveis);
	   for($i=0;$i<$qtd;$i++) {
	     $caracPai[$i] = $crPai->carregarCaracteristicaPai($imoveis[$i][imo_isn]);
	   } 
	   return $caracPai;
	}
	
	function buscarCaracteristicaPaiDetal() {
       $crPai = new Carac_Pai();
	   $caracPai = array();
	   $imoveis = $_SESSION["arrayImoveisHOU"];
	   $qtd = count($imoveis);
	   if(!is_array($_SESSION["idchkCHOU"])){
	   		for($i=0;$i<$qtd;$i++) {
			  $caracPai[$i] = $crPai->carregarCaracteristicaPai($imoveis[$i][imo_isn]);
		    } 
	   }else {
	   		$idcs = implode(",",$_SESSION["idchkCHOU"]);
	   		for($i=0;$i<$qtd;$i++) {
			  if(eregi($imoveis[$i]['imo_isn'],$idcs)){
			  		$caracPai[$i] = $crPai->carregarCaracteristicaPai($imoveis[$i][imo_isn]);
			  }
		    } 			
	   }
	   
	   return $caracPai;
	}

	 function carregarCaracteristicaPai($codigo) {
       $crPai = new Carac_Pai();
	   $caracPai = array();
	   $caracPai = $crPai->carregarCaracteristicaPai($codigo);
	   return $caracPai;
	}

	function carregaCaracteristicasAv($tip) {
	   $i = 0;
	   $imovel = "";
	   $sql = "SELECT DISTINCT CAR_DES,CAR_ISN,CAR_TIP,CAR_DES_UNI
	   FROM TAB_CAR WHERE CAR_TIP_NET_PESQ = 1 AND CAR_TIP <> 4	 AND CAR_ISN_PAI = 0 AND TIM_ISN = $tip ORDER BY CAR_DES";
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["car_isn"]           = odbc_result($res,"CAR_ISN"); 	   
			$imovel[$i]["car_des"]           = odbc_result($res,"CAR_DES"); 	   
			$imovel[$i]["car_tip"]           = odbc_result($res,"CAR_TIP"); 	   
			$imovel[$i]["car_des_uni"]       = odbc_result($res,"CAR_DES_UNI"); 	   
         $i++; 
	   }
	   $valpre = "";
	   is_array($imovel)?$quant=count($imovel):$quant=0;
	   for($cont=0;$cont<$quant;$cont++) {
	      if($imovel[$cont]["car_tip"] == 3) {
		     $valpre = $this->obterValPre($imovel[$cont]["car_isn"]);  
			 $imovel[$cont]["val_CAR"] = $valpre;
		  }	 
	   }
	   odbc_close($conn);		
	   return $imovel;
	}

	function obterValPre($num) {
	   $i = 0;
	   $valores = "";
	   $sql = "SELECT DCA_ISN,DCA_DES 
	   FROM TAB_DCA WHERE CAR_ISN = $num   ORDER BY DCA_DES";
	   $conn=$this->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$valores[$i]["dca_isn"]           = odbc_result($res,"DCA_ISN"); 	   
			$valores[$i]["dca_des"]           = odbc_result($res,"DCA_DES"); 	   
            $i++; 
	   }
	   odbc_close($conn);		
	   return $valores;
	}
   function carregarCodigos() {
	   $con = new Conexao(); 
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT IMO.IMO_ISN, IMO.IMO_DES_TXT_DEST, IMO.IMO_DES_BAI, IMO.IMO_DES_END, TIM.TIM_DES, IMI.IMI_NOM 
	   		   FROM TAB_IMO IMO, TAB_TIM TIM, TAB_IMI IMI 
			   WHERE IMO.IMO_TIP_DEST = 1 
			   AND IMO.IMO_TIP_DEST_PRIM_PAG = 1
			   AND TIM.TIM_ISN = IMO.TIM_ISN
			   AND IMI.IMO_ISN = IMO.IMO_ISN
			   AND IMI.IMI_TIP_DEST = 1   
			   ORDER BY IMO.IMO_ISN";
	   //echo "<pre>$sql</pre>";exit;
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]        = odbc_result($res,"IMO_ISN"); 	   
			$codigo[$i]["imo_des_est"]    = odbc_result($res,"IMO_DES_TXT_DEST"); 
			$codigo[$i]["tim_des"]        = odbc_result($res,"TIM_DES");
			$codigo[$i]["imo_des_bai"]        = odbc_result($res,"IMO_DES_BAI");	
			$codigo[$i]["imo_des_end"]        = odbc_result($res,"IMO_DES_END"); 
			$codigo[$i]["imi_nom"]        = odbc_result($res,"IMI_NOM");  
         $i++; 
	   }
	   odbc_close($conn);		
	   return $codigo;
   }

   function carregarTodosCodigos() {
	   $con = new Conexao(); 
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT IMO_ISN,IMO_DES_TXT_DEST 
	   FROM TAB_IMO WHERE IMO_TIP_DEST = 1 ORDER BY IMO_ISN";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]            = odbc_result($res,"IMO_ISN"); 	   
			$codigo[$i]["imo_des_est"]        = odbc_result($res,"IMO_DES_TXT_DEST"); 	   
         $i++; 
	   }
	   odbc_close($conn);		
	   return $codigo;
   }

   function carregarDestaquesCapa($cod) {
	   $con = new Conexao(); 
	   //$i = 0;
	   $destaque = array();
	   $sql = "SELECT IMI_NOM  FROM TAB_IMI WHERE IMO_ISN = $cod AND IMI_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$destaque["imi_nom"] = odbc_result($res,"IMI_NOM"); 	   
	   }
	   odbc_close($conn);		
	   return $destaque;
   }

   function carregarDestaques($cod) {
	   $con = new Conexao(); 
	   //$i = 0;
	   $destaque = array();
	   $sql = "SELECT IMI_NOM  FROM TAB_IMI WHERE IMO_ISN = $cod AND IMI_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$destaque["imi_nom"] = odbc_result($res,"IMI_NOM"); 	   
	   }
	   odbc_close($conn);		
	   return $destaque;
   }
   
   	 function consultarFotoDestaque($codigo,$bairro,$cidade,$tipo,$valini,$valfim) {
	   $con = new Conexao(); 
	   $i = 0;
	   $imovel = array();
	   $sql = "SELECT  IMO.IMO_ISN,  IMO.IMO_DES_BAI,  IMO.IMO_DES_END,  IMO.IMO_VAL_ALU,  IMO.IMO_VAL_ARE, TIM.TIM_DES, IMI.IMI_NOM 
	   		   FROM TAB_IMO IMO, TAB_TIM TIM, TAB_IMI IMI
			   WHERE TIM.TIM_ISN = IMO.TIM_ISN 
	   		   AND TIM.TIM_ISN = $tipo 
			   $bairro 
			   AND IMO.IMO_DES_LOC = '$cidade' 
			   AND IMI.IMO_ISN = IMO.IMO_ISN
			   AND IMI.IMI_TIP_DEST = 1 ";
			   
		if($valini) { 
	      $sql = $sql."AND IMO.IMO_VAL_ALU >= $valini ";
		}	
		if($valfim) { 
	      $sql = $sql."AND IMO.IMO_VAL_ALU <= $valfim ";  
		}	
	   $sql = $sql."ORDER BY IMO.IMO_VAL_ALU";
	   //echo $sql;exit;
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
			$imovel[$i]["imi_nom"]           = odbc_result($res,"IMI_NOM");	   
            $i++; 
	   }
	   odbc_close($conn);		
	   return $imovel;
	}
	
	function consultarFotoDestaquePorCodigo($codigo) {
	   $con = new Conexao(); 
	   $i = 0;
	   $imovel = array();
	   $sql = "SELECT  IMI.IMO_ISN, IMI.IMI_NOM 
	   		   FROM TAB_IMI IMI
			   WHERE IMI.IMO_ISN = $codigo
			   AND IMI.IMI_TIP_DEST = 1 ";
			   
		//echo $sql;exit;
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
			$imovel[$i]["imi_nom"]           = odbc_result($res,"IMI_NOM");	   
            $i++; 
	   }
	   odbc_close($conn);		
	   return $imovel;
	}

	function consultarCroqui($cod) {
	  $con = new Conexao();
	  $imo = array();
	  $sql = "SELECT IMO_DES_END,IMO_DES_LOG_DIR1,IMO_DES_LOG_DIR2,IMO_DES_LOG_ESQ1,IMO_DES_LOG_ESQ2,
	          IMO_DES_LOG_TRAS,IMO_DES_REF_1,IMO_DES_REF_2 FROM TAB_IMO WHERE IMO_ISN = $cod";
		$conn=$con->connDb();			 
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imo["imo_log_pr"] = odbc_result($res,"IMO_DES_END"); 	   
			$imo["imo_log_dir1"] = odbc_result($res,"IMO_DES_LOG_DIR1"); 	   
			$imo["imo_log_dir2"] = odbc_result($res,"IMO_DES_LOG_DIR2"); 	   
			$imo["imo_log_esq1"] = odbc_result($res,"IMO_DES_LOG_ESQ1"); 	   
			$imo["imo_log_esq2"] = odbc_result($res,"IMO_DES_LOG_ESQ2"); 	   
			$imo["imo_log_tras"] = odbc_result($res,"IMO_DES_LOG_TRAS"); 	   
			$imo["imo_des_ref1"] = odbc_result($res,"IMO_DES_REF_1"); 	   
			$imo["imo_des_ref2"] = odbc_result($res,"IMO_DES_REF_2"); 	   
	   }
	   odbc_close($conn);		
	   return $imo;
	}
 function carregarCodigosAdm($end=0, $num=0) {
	   $con = new Conexao(); 
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT IMO.IMO_ISN,IMO.IMO_DES_END,IMO.IMO_TIP_DEST,
	           IMO.IMO_TIP_DEST_PRIM_PAG,TIM.TIM_DES 
	           FROM TAB_IMO IMO,TAB_TIM TIM WHERE IMO.TIM_ISN = TIM.TIM_ISN ";
	   if($num){
	   $sql = $sql."AND IMO_DES_END LIKE '%$end%' ";
	   }
	   if($num){
	   $sql = $sql."AND IMO.IMO_ISN = $end ";
	   }
	   $sql = $sql."ORDER BY IMO_ISN";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]              = odbc_result($res,"IMO_ISN"); 	   
			$codigo[$i]["imo_des_end"]          = odbc_result($res,"IMO_DES_END"); 	   
			$codigo[$i]["imo_tip_dest"]         = odbc_result($res,"IMO_TIP_DEST"); 	   
			$codigo[$i]["imo_tip_dest_prim"]    = odbc_result($res,"IMO_TIP_DEST_PRIM_PAG"); 	
			$codigo[$i]["tipo"]                 = odbc_result($res,"TIM_DES");
			$i++; 
	   }
	   for($x=0;$x < count($codigo);$x++) {
			$imovel[$x]["imo_isn"]              = $codigo[$x]["imo_isn"]; 	   
			$imovel[$x]["imo_des_end"]          = $codigo[$x]["imo_des_end"]; 	   
			$imovel[$x]["imo_tip_dest"]         = $codigo[$x]["imo_tip_dest"]; 	   
			$imovel[$x]["imo_tip_dest_prim"]    = $codigo[$x]["imo_tip_dest_prim"]; 				            $imovel[$x]["tipo"]                 = $codigo[$x]["tipo"];
            $imovel[$x]["foto"]                 = $this->carregarFotoCod($codigo[$x]["imo_isn"]);
	   }
	   odbc_close($conn);		
	   return $imovel;
 }
 function carregarCodigosAdmEsp($cod) {
	   $con = new Conexao(); 
	   $i = 0;
	   $codigo = array();
	   $sql = "SELECT IMO.IMO_ISN,IMO.IMO_DES_END,IMO.IMO_TIP_DEST,
	           IMO.IMO_TIP_DEST_PRIM_PAG,TIM.TIM_DES 
	           FROM TAB_IMO IMO,TAB_TIM TIM WHERE IMO.TIM_ISN = TIM.TIM_ISN 
			   AND IMO.IMO_ISN = $cod";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$codigo[$i]["imo_isn"]              = odbc_result($res,"IMO_ISN"); 	   
			$codigo[$i]["imo_des_end"]          = odbc_result($res,"IMO_DES_END"); 	   
			$codigo[$i]["imo_tip_dest"]         = odbc_result($res,"IMO_TIP_DEST"); 	   
			$codigo[$i]["imo_tip_dest_prim"]    = odbc_result($res,"IMO_TIP_DEST_PRIM_PAG"); 	            $codigo[$i]["tipo"]                 = odbc_result($res,"TIM_DES");
            $i++; 
	   }
	   for($x=0;$x < count($codigo);$x++) {
			$imovel[$x]["imo_isn"]              = $codigo[$x]["imo_isn"]; 	   
			$imovel[$x]["imo_des_end"]          = $codigo[$x]["imo_des_end"]; 	   
			$imovel[$x]["imo_tip_dest"]         = $codigo[$x]["imo_tip_dest"]; 	   
			$imovel[$x]["imo_tip_dest_prim"]    = $codigo[$x]["imo_tip_dest_prim"]; 				            $imovel[$x]["tipo"]                 = $codigo[$x]["tipo"];
            $imovel[$x]["foto"]                 = $this->carregarFotoCod($codigo[$x]["imo_isn"]);
	   }
	   odbc_close($conn);		
	   return $imovel;
 }
 function carregarFotoCod($cod) {
	   $con = new Conexao(); 
	   $codigo = array();
	   $sql = "SELECT IMI_NOM 
	   FROM TAB_IMI WHERE IMO_ISN = $cod AND
	   IMI_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$img  =  odbc_result($res,"IMI_NOM"); 	   
	   }
	   odbc_close($conn);		
	   return $img;
 }
 function buscarImagensAdm($cod) {
	   $con = new Conexao(); 
	   $codigo = array();
	   $i = 0;
	   $sql = "SELECT IMI_NOM 
	   FROM TAB_IMI WHERE IMO_ISN = $cod"; 
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$img[$i]["nome"]  =  odbc_result($res,"IMI_NOM"); 	   
			$i++;
	   }
	   odbc_close($conn);
	   for($x=0;$x < count($img);$x++) {
	       $imagem[$x]["nome"]     =  $img[$x]["nome"];
		   if($imagem[$x]["nome"] == $this->carregarFotoCod($cod)) {
		      $imagem[$x]["destaque"] =  $imagem[$x]["nome"];
		   }
	   }
	   return $imagem;
  }
  function alterarImagemAdm($cod,$nom) {
	   $con = new Conexao(); 
	   $sql = "UPDATE TAB_IMI SET IMI_TIP_DEST = 0 
	           WHERE IMO_ISN = $cod AND IMI_TIP_DEST = 1"; 
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   $sql2 = "UPDATE TAB_IMI SET IMI_TIP_DEST = 1
	            WHERE IMO_ISN = $cod AND IMI_NOM = '$nom'";
	   $res = @odbc_exec($conn,$sql2);
	   odbc_close($conn);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
  }
  function setarDestaque($cod) {
       $con = new Conexao();
	   $sql = "UPDATE TAB_IMO SET IMO_TIP_DEST = 1
	          WHERE IMO_ISN = $cod";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
  }
  function setarDestaquePrim($cod) {
       $con = new Conexao();
	   $sql = "UPDATE TAB_IMO SET IMO_TIP_DEST = 1,
	           IMO_TIP_DEST_PRIM_PAG = 1 
	           WHERE IMO_ISN = $cod";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
  }
  function retirarDestaque($cod) {
       $con = new Conexao();
	   $sql = "UPDATE TAB_IMO SET IMO_TIP_DEST = 0
	           WHERE IMO_ISN = $cod AND IMO_TIP_DEST = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
  }
  function retirarDestaquePrim($cod) {
       $con = new Conexao();
	   $sql = "UPDATE TAB_IMO SET IMO_TIP_DEST_PRIM_PAG = 0
	           WHERE IMO_ISN = $cod AND IMO_TIP_DEST_PRIM_PAG = 1";
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
  }
  
  function consultarAllImoveis() {
	   $con = new Conexao(); 
	   $i = 0;
	   $imovel = array();
	   $sql = "SELECT IMO.IMO_ISN, IMO.IMO_DES_BAI, IMO.IMO_DES_TXT_DEST,IMO.IMO_DES_LOC, IMO.IMO_DES_END, IMO.IMO_VAL_ALU, IMO.IMO_VAL_ARE, TIM.TIM_DES 
	   		   FROM TAB_IMO IMO, TAB_TIM TIM 
			   WHERE TIM.TIM_ISN = IMO.TIM_ISN ORDER BY IMO.IMO_ISN";	  
	   
	   $conn=$con->connDb();	
	   $res = @odbc_exec($conn,$sql);
	   if (odbc_errormsg() != "") { 
		   return false;
	   }
	   while(@odbc_fetch_row($res)) {
			$imovel[$i]["imo_isn"]           = odbc_result($res,"IMO_ISN"); 	   
			$imovel[$i]["imo_des_bai"]       = odbc_result($res,"IMO_DES_BAI"); 	   
			$imovel[$i]["imo_des_end"]       = odbc_result($res,"IMO_DES_END"); 	   
			$imovel[$i]["imo_val_alu"]       = odbc_result($res,"IMO_VAL_ALU"); 	   
			$imovel[$i]["imo_val_are"]       = odbc_result($res,"IMO_VAL_ARE");
			$imovel[$i]["imo_des_loc"]       = odbc_result($res,"IMO_DES_LOC");
			$imovel[$i]["imo_des_txt_dest"]  = odbc_result($res,"IMO_DES_TXT_DEST"); 	   
			$imovel[$i]["tim_des"]           = odbc_result($res,"TIM_DES"); 	   
            $i++; 
	   }
	   odbc_close($conn);		
	   return $imovel;
	}
	
	function buscarDadosImo($codigo) {
		$sql = "SELECT I.IMO_DES_END,I.IMO_DES_BAI,I.IMO_VAL_ALU,T.TIM_DES 
				  FROM TAB_IMO I,TAB_TIM T WHERE I.TIM_ISN = T.TIM_ISN
				  AND I.IMO_ISN = $codigo";
		$conn=$this->connDb();	
	    $res = @odbc_exec($conn,$sql);
	    if (odbc_errormsg() != "") { 
		   return false;
	    }
	    $imovel="";
		while(@odbc_fetch_row($res)) {
			 $imovel["des_end"]     = odbc_result($res,"IMO_DES_END");
			 $imovel["des_bai"]     = odbc_result($res,"IMO_DES_BAI");
			 $imovel["val_alu"]     = odbc_result($res,"IMO_VAL_ALU");
			 $imovel["tim_des"]     = odbc_result($res,"TIM_DES");
		 }
		odbc_close($conn);	
		return $imovel;
	 }
	 
	 function get_folder_size($target,$output=false){
	   $totalsize=0;
	   $sourcedir = opendir($target); 
	   while(false !== ($filename = readdir($sourcedir))){ 
		   if($filename != "." && $filename != ".."){ 
			   if(is_dir($target."/".$filename)){ 
				   $totalsize += $this->get_folder_size($target."/".$filename, false); 
			   }else if(is_file($target."/".$filename)){ 
				   $totalsize += filesize($target."/".$filename); 
			   } 
		   } 
	   } 
	   closedir($sourcedir); 
	   return $totalsize; 
	} 
	
	function getSize($dest) {
		$dest==1?$dest=$this->CaminhoFoto():$dest=$this->Banco();
		$size = $this->get_folder_size($dest,false); 
		$tam = (int)($size/1024); 
		$tamanho = ($tam/1024);
		return number_format($tamanho,2,",",".");
	}
	
	/*function Manutencao(){
		$dados = array();
		$h=opendir($this->CaminhoFoto());
		$i=0;
		$x=0;
		$file = "";
		$conn=$this->connDb();	
		while (($arquivo=readdir($h))!==false) {
				if(($arquivo != "") && ($arquivo != ".") && ($arquivo != "..")) {
				   $sql = "SELECT IMI_ISN FROM TAB_IMI WHERE IMI_NOM = '$arquivo'";
				   $res = odbc_exec($conn,$sql);
				   if (odbc_errormsg() != "") {
					   continue;
				   } 
				   $img="";
				   while(odbc_fetch_row($res)) {
						$img = odbc_result($res,"IMI_ISN"); 
				   }
				   if(!$img) {
				   	   $x++;
					   @unlink($this->CaminhoFoto().$arquivo);
				   }
				   $i++;
			   }	
		}
		@odbc_close($conn);		
		@closedir($h);
		$dados['anterior']  = $i;
		$dados['atual']     = $i-$x;
		$dados['excluidas'] = $x;
		return $dados;
	}*/

	 //Fun��o de Exclus�o de fotos atualizada por Diego Pedrosa.
	 function Manutencao () {
		 $dados = array();
		 $i=0;
		 $x=0;
		 $y=0;
		 $dir = $this->CaminhoFoto();
		 $arquivos = scandir($dir);
		 unset($arquivos[0]);
		 unset($arquivos[1]);
		 unset($arquivos[2]);
		 $arquivos = array_values($arquivos);
		 $conn=$this->connDb();
		 $sql = "SELECT IMI_NOM FROM TAB_IMI ORDER BY IMI_NOM ASC";
		 $res = odbc_exec($conn,$sql);
		 while(@odbc_fetch_row($res)) {
			 $fotos[$y]    = odbc_result($res,"IMI_NOM");
			 $y++;
		 }
		 odbc_close($conn);

		 foreach ($arquivos as $arquivo) {
			 $verifica = array_search($arquivo, $fotos);
			 if ($verifica == '') {
				 @unlink($this->CaminhoFoto().$arquivo);
				 $x++;
			 }
			 $i++;
		 }

		 $dados['anterior']  = count($arquivos);
		 $dados['atual']     = count($arquivos)-$x;
		 $dados['excluidas'] = $x;
		 return $dados;

	 }
	
	function contaBoletos($busca){
		$conn=$this->connDb();
		$sql = "SELECT COUNT(REC_ISN) AS TOTAL FROM TAB_REC";
		if($busca){
			$sql.=" WHERE REC_NOM_INC LIKE '%".$this->db->escape($busca)."%'";
		}
		$res = odbc_exec($conn,$sql);
		if (odbc_errormsg() != "") {
		   return false;
		}
		$total=0;
		while(odbc_fetch_row($res)) {
			$total = odbc_result($res,"TOTAL");
		}
		@odbc_close($conn);	
		return $total;
	}
	
	function getBoletos($busca,$ordem='REC_ISN ASC') {
		$conn=$this->connDb();
		$sql = "SELECT * FROM TAB_REC";
		if($busca){
			$sql.=" WHERE REC_NOM_INQ LIKE '%".$busca."%'";
		}
		$sql.=" ORDER BY $ordem";
		$mod="";
		$res = odbc_exec($conn,$sql);
		if (odbc_errormsg() != "") {
		   return false;
		}
		$i = 0;
		while(odbc_fetch_row($res)) {
			$mod[$i]["id"]    =  odbc_result($res,"REC_ISN"); 
			$mod[$i]["nome"]  =  odbc_result($res,"REC_NOM_INQ"); 
			$mod[$i]["num"]   =  odbc_result($res,"REC_NUM_CPF_CGC_INQ"); 
			$mod[$i]["end"]   =  odbc_result($res,"REC_DES_END");
			$i++;
		}	
		@odbc_close($conn);		
		return $mod;
	}
}
?>