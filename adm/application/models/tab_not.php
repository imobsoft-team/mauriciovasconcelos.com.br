<?php
class Tab_not extends Model{
	function Tab_not(){
		parent::Model();
	}
	
	function getNoticias($maximo,$inicio,$busca,$ordem='NOT_ISN ASC') {
			$sql = "SELECT NOT_ISN,NOT_TITULO,DAY(NOT_DAT_NOT) AS DIA, MONTH(NOT_DAT_NOT) AS MES, YEAR(NOT_DAT_NOT) AS ANO,DAY(NOT_DAT_VAL) AS DIAV, MONTH(NOT_DAT_VAL) AS MESV, YEAR(NOT_DAT_VAL) AS ANOV FROM tab_not";
		    if($busca){
				$sql.=" WHERE NOT_TITULO LIKE '%$busca%' ";
			}
			$sql.=" ORDER BY $ordem";
			$sql.=" LIMIT $inicio,$maximo";
			$not="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $array){
				   $not[$i]["id"]    =  $array->NOT_ISN; 
				   $not[$i]["tit"]   =  $array->NOT_TITULO; 
				   $not[$i]["dia"]   =  str_pad($array->DIA,2,"0",STR_PAD_LEFT); 
				   $not[$i]["mes"]   =  str_pad($array->MES,2,"0",STR_PAD_LEFT);
				   $not[$i]["ano"]   =  str_pad($array->ANO,4,"0",STR_PAD_LEFT);
				   $not[$i]["diav"]  =  str_pad($array->DIAV,2,"0",STR_PAD_LEFT);
				   $not[$i]["mesv"]  =  str_pad($array->MESV,2,"0",STR_PAD_LEFT);
				   $not[$i]["anov"]  =  str_pad($array->ANOV,4,"0",STR_PAD_LEFT);
				   $i++;
				}
			}
			return $not;
	}
	
	function contaRegistros($busca){
		$sql = "SELECT COUNT(NOT_ISN) AS TOTAL FROM tab_not";
		if($busca){
			$sql.=" WHERE NOT_TITULO LIKE '%$busca%' ";
		}
		$res = $this->db->query($sql);
		foreach($res->result() as $array){
			$total = $array->TOTAL;
		}
		return $total;
	}
	
	function cadastrarNoticia($titulo,$tituloing,$tituloesp,$texto,$textoing,$textoesp,$fonte,$data,$datval,$banner,$cam='') {
	if($banner["type"]){
		   if(eregi("^image\/(pjpeg|jpeg|JPEG|jpg|JPG)$", $banner["type"])){
		      if($banner["size"] < 6000000){
				// Para verificar as dimens�es da imagem
				$tamanhos = getimagesize($banner["tmp_name"]);
				// Verifica largura e altura da imagem
				if($tamanhos[0] > 5000){
						//erro de largura
						return 3;
				}else if($tamanhos[1] > 5000){
						//erro de altura
						return 4;
				}else {
					//pega a extens�o
					preg_match("/\.(pjpeg|jpeg|JPEG|jpg|JPG){1}$/i", $banner["name"], $ext);
					// Gera um nome �nico para a imagem
        			$imagem_nome = uniqid(time()).".".$ext[1];
					// Caminho de onde a imagem ficar�
        			$imagem_dir = "adm/imagens/".$imagem_nome;
					// Faz o upload da imagem
					if(@move_uploaded_file($banner["tmp_name"], $cam.$imagem_dir)){
					
					 $sql = "INSERT INTO tab_not (NOT_TITULO,NOT_TITULO_ING,NOT_TITULO_ESP,NOT_DES,NOT_DES_ING,NOT_DES_ESP,NOT_FONTE,NOT_DAT_NOT,NOT_DAT_VAL,NOT_BAN_IMG) VALUES ('$titulo','$tituloing','$tituloesp','$texto','$textoing','$textoesp','$fonte','$data','$datval','$imagem_dir')";
					
					   $res = $this->db->query($sql);	
					   $codigo = $this->db->insert_id();	 
					   if ($codigo>0) {		   
							return 7;
					   }else {
						   //erro ao inserir no banco de dados
						   @unlink($cam.$imagem_dir);
						   return 6;
					   } 	
					}else {
						//erro no upload para o servidor.
						return 5;
					}
				}
			  }else {
			  	 //erro do tamnho do arquivo
				 return 2;
			  }
		   }else {
		   	  //erro no tipo do arquivo
			  return 1;
		   }
	   }else {
	   	   $sql = "INSERT INTO tab_not (NOT_TITULO,NOT_TITULO_ING,NOT_TITULO_ESP,NOT_DES,NOT_DES_ING,NOT_DES_ESP,NOT_FONTE,NOT_DAT_NOT,NOT_DAT_VAL) VALUES ('$titulo','$tituloing','$tituloesp','$texto','$textoing','$textoesp','$fonte','$data','$datval')";
		   $res = $this->db->query($sql);	
		   $codigo = $this->db->insert_id();	 
		   if ($codigo>0) {		   
				return 7;
		   }else {
			   //erro ao inserir no banco de dados
			   return 6;
		   } 	
	   }	   
	}
	
	function buscarNoticia($cod) {     
	  $sql  = "SELECT * FROM tab_not WHERE NOT_ISN = $cod";
	  $res = $this->db->query($sql);
	  $not="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			 $not["id"]     =  $array->NOT_ISN;
			 $not["tit"]    =  $array->NOT_TITULO;
			 $not["titing"]    =  $array->NOT_TITULO_ING;
			 $not["titesp"]    =  $array->NOT_TITULO_ESP;
			 $not["texto"]  =  $array->NOT_DES;
			 $not["textoing"]  =  $array->NOT_DES_ING;
			 $not["textoesp"]  =  $array->NOT_DES_ESP;
			 $not["fonte"]  =  $array->NOT_FONTE;
			 if($array->NOT_DAT_VAL){
			 	$venc   		=  explode('-',$array->NOT_DAT_VAL);
			 	$not["venc"] = $venc[2].'/'.$venc[1].'/'.$venc[0];
			 }else {
			 	$not["venc"] = "0000-00-00";
			 } 
		  }
	  }
	  return $not;
	}
	
	function alterarNoticia($titulo,$tituloing,$tituloesp,$texto,$textoing,$textoesp,$fonte,$venc,$codigo,$banner,$cam='') {
	  if($banner["type"]){
		   if(eregi("^image\/(pjpeg|jpeg|JPEG|jpg|JPG)$", $banner["type"])){
		      if($banner["size"] < 6000000){
				// Para verificar as dimens�es da imagem
				$tamanhos = getimagesize($banner["tmp_name"]);
				// Verifica largura e altura da imagem
				if($tamanhos[0] > 5000){
						//erro de largura
						return 3;
				}else if($tamanhos[1] > 5000){
						//erro de altura
						return 4;
				}else {
					//pega a extens�o
					preg_match("/\.(pjpeg|jpeg|JPEG|jpg|JPG){1}$/i", $banner["name"], $ext);
					// Gera um nome �nico para a imagem
        			$imagem_nome = uniqid(time()).".".$ext[1];
					// Caminho de onde a imagem ficar�
        			$imagem_dir = "adm/imagens/".$imagem_nome;
					// Faz o upload da imagem
					$ver = move_uploaded_file($banner["tmp_name"], $cam.$imagem_dir);
					
					$sql = "UPDATE tab_not SET NOT_TITULO = '$titulo',NOT_TITULO_ING = '$tituloing',NOT_TITULO_ESP = '$tituloesp', NOT_DES = '$texto',NOT_DES_ING = '$textoing',NOT_DES_ESP = '$textoesp', NOT_FONTE = '$fonte', NOT_DAT_VAL = '$venc',NOT_BAN_IMG ='$imagem_dir'  WHERE NOT_ISN = $codigo";
					
					   $res = $this->db->query($sql);	
					   $codigo = $this->db->insert_id();	 
				       return 7;
				}
			  }else {
			  	 //erro do tamnho do arquivo
				 return 2;
			  }
		   }else {
		   	  //erro no tipo do arquivo
			  return 1;
		   }
	   }else {
		   $sql = "UPDATE tab_not SET NOT_TITULO = '$titulo',NOT_TITULO_ING = '$tituloing',NOT_TITULO_ESP = '$tituloesp', NOT_DES = '$texto',NOT_DES_ING = '$textoing',NOT_DES_ESP = '$textoesp', NOT_FONTE = '$fonte', NOT_DAT_VAL = '$venc'  WHERE NOT_ISN = $codigo";
		   
		   $res = $this->db->query($sql);	
		   $codigo = $this->db->insert_id();	 
		   if ($codigo>0) {		   
				return 7;
		   }else {
			   //erro ao inserir no banco de dados
			   return 6;
		   } 	
	   }	   
	   
	    
	   $sql  = "UPDATE tab_not SET NOT_TITULO = '$titulo',NOT_TITULO_ING = '$tituloing',NOT_TITULO_ESP = '$tituloesp', NOT_DES = '$texto',NOT_DES_ING = '$textoing',NOT_DES_ESP = '$textoesp', NOT_FONTE = '$fonte', NOT_DAT_VAL = '$venc' WHERE NOT_ISN = $codigo";	  
	   $this->db->query($sql);
	   return $codigo;
	}
	
	function excluirItens($cods) { 	
		$sql  = "DELETE FROM tab_not WHERE NOT_ISN IN ($cods)";
		$this->db->query($sql);
	} 

}
?>