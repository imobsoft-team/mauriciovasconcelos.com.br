<?
 require_once("../conexaoImovel.php");
 include("../conexaoMy.php");
 class UsuAdm {
    function UsuAdm() {
	 
	 }
	 function logar($login,$senha) {
		 $usu = array();
	    $con = new conexaoImovel();   
		 $sql = "SELECT * FROM tab_usu WHERE USU_LOG='$login' AND USU_PSWD='$senha'";
	    $conn=$con->connDb();	
	    $res = odbc_exec($conn,$sql);
		 while(odbc_fetch_row($res)) {
		    $usu[id] = odbc_result($res,"USU_ISN");
			 $usu[nome] = odbc_result($res,"USU_NOM");
			 $usu[senha] = odbc_result($res,"USU_PSWD");
			 $usu[perm]  = odbc_result($res,"USU_PERM");
		 }
	    if (odbc_errormsg() != "") { 
		   die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
	    }
		 if($usu) {
		    $this->alteraStatusUsu($usu[id],1);
		 }
	   odbc_close($conn);		
	   return $usu;
	 }
	 function alteraStatusUsu($cod,$st) {
	    $con = new conexaoImovel();   
		$sql = "UPDATE tab_usu SET USU_STA = $st WHERE USU_ISN = $cod";
	    $conn=$con->connDb();	
	    $res = odbc_exec($conn,$sql);
	    if (odbc_errormsg() != "") { 
		   die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
	    }
		 odbc_close($conn);
	 }
	 function consultarContatos($nome,$dataI,$dataF,$depto) {
	    $contato = array();
		 $con = new conexaoImovel();
		 $sql = "SELECT CNT_ISN, CNT_NOM, CNT_MAIL, CNT_MSG, DAY(CNT_DAT) AS DIA, MONTH(CNT_DAT) AS MES, YEAR(CNT_DAT) AS ANO FROM tab_cnt WHERE  1=1 ";
		 if($nome) {
		    $sql = $sql." AND CNT_NOM LIKE '%$nome%'";
		 }
		 if(($dataI) && ($dataF)) {
		    $sql = $sql." AND CNT_DAT >= CDATE('$dataI') AND CNT_DAT <= CDATE('$dataF')";
		 } 
		 
		 if(!empty($depto)) {
		    $sql = $sql." AND CNT_DEPTO = $depto ";
		 } 
	    $conn=$con->connDb();	
	    $res = odbc_exec($conn,$sql);
	    if (odbc_errormsg() != "") { 
		   die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
	    }
		 $i = 0;
		 while(odbc_fetch_row($res)) {
			 $contato[$i][isn]  = odbc_result($res,"CNT_ISN");
			 $contato[$i][nome]  = odbc_result($res,"CNT_NOM");
			 $contato[$i][data_dia]  = odbc_result($res,"DIA"); 
 			 $contato[$i][data_mes]  = odbc_result($res,"MES"); 
 			 $contato[$i][data_ano]  = odbc_result($res,"ANO"); 
			 $contato[$i][email] = odbc_result($res,"CNT_MAIL"); 			 
			 $contato[$i][msg]   = odbc_result($res,"CNT_MSG"); 			 
			 $i++;
		 }
	    odbc_close($conn);		
	    return $contato;
	 }
	 function excluirContato($cont) {
	    $con = new conexaoImovel();
		 for($i=0;$i<count($cont);$i++) {
			 $sql = "DELETE * FROM tab_cnt WHERE CNT_ISN = $cont[$i]";
			 $conn=$con->connDb();	
			 $res = odbc_exec($conn,$sql);
			 if (odbc_errormsg() != "") { 
				die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
			 }
			 else {
			    $msg = 1;
			 }
		 }
		 odbc_close($conn);
		 return msg;
	 } 
	 function consultarTMSG() {
	     $tipo = array();
	     $con = new conexaoImovel();
		  $sql = "SELECT * FROM tab_tpm ORDER BY TPM_DES";
		  $conn = $con->connDb();	  
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  $i = 0;
		  while(odbc_fetch_row($res)) {
			 $tipo[$i][tpm_isn] = odbc_result($res,"TPM_ISN"); 
			 $tipo[$i][tpm_des] = odbc_result($res,"TPM_DES");
			 $tipo[$i][tpm_tem_atd] = odbc_result($res,"TPM_TEM_ATD");
			 $i++;
		  } 
		  odbc_close($conn);	
		  return $tipo;
	  }
	  function consultarTPSOL() {
	     $tipo = array();
	     $con = new conexaoImovel();
		  $sql = "SELECT * FROM tab_tps ORDER BY TPS_DES";
		  $conn = $con->connDb();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  $i = 0;
		  while(odbc_fetch_row($res)) {
			 $tipo[$i][tps_isn] = odbc_result($res,"TPS_ISN"); 
			 $tipo[$i][tps_des] = odbc_result($res,"TPS_DES");
			 $i++;
		  } 
		  odbc_close($conn);	
		  return $tipo;
	  }
	  function consultarOuv($nome,$dataI,$dataF,$tipo) {
	     $ouvi = array();
	     $con = new conexaoImovel();
		  $sql = "SELECT OUV_ISN, OUV_NOM, OUV_MAIL, OUV_MSG, OUV_NOM_ATD, DAY(OUV_DAT) AS DIA,
		         MONTH(OUV_DAT) AS MES, YEAR(OUV_DAT) AS ANO, OUV_STA FROM tab_ouv WHERE 1=1 ";
		  if($nome) {
		     $sql = $sql." AND OUV_NOM LIKE '%$nome%' ";
		  }
		  if($tipo) {
		     $sql = $sql." AND TPM_ISN = $tipo ";
		  }
		  if(($dataI) && ($dataF)) {
		     $sql = $sql." AND OUV_DAT >= CDATE('$dataI') AND OUV_DAT <= CDATE('$dataF')";
		  }
		  $conn = $con->connDb();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  $i = 0;
		  while(odbc_fetch_row($res)) {
			 $ouvi[$i][ouv_isn]      = odbc_result($res,"OUV_ISN"); 
			 $ouvi[$i][ouv_data_dia] = odbc_result($res,"DIA");
			 $ouvi[$i][ouv_data_mes] = odbc_result($res,"MES");
			 $ouvi[$i][ouv_data_ano] = odbc_result($res,"ANO");
			 $ouvi[$i][ouv_nome]     = odbc_result($res,"OUV_NOM");
			 $ouvi[$i][ouv_email]    = odbc_result($res,"OUV_MAIL");
			 $ouvi[$i][ouv_msg]      = odbc_result($res,"OUV_MSG");
			 $ouvi[$i][ouv_nome_atd] = odbc_result($res,"OUV_NOM_ATD");
			 $ouvi[$i][ouv_sta]      = odbc_result($res,"OUV_STA");
			 $i++;
		  } 
		  odbc_close($conn);	
		  return $ouvi;
	  }
     function excluirOuvidoria($ouv) {
	     $con = new conexaoImovel();
		  for($i=0;$i<count($ouv);$i++) {
			  $sql = "DELETE * FROM tab_ouv WHERE OUV_ISN = $ouv[$i]";   
			  $conn = $con->connDb();
			  $res = odbc_exec($conn,$sql);
			  if (odbc_errormsg() != "") {
				 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
			  }
			  else {
				  $msg = 1;
			  }
		  }
		  odbc_close($conn);
		  return $msg;
	  }
	  function consultarSol($nome,$dataI,$dataF,$tipo) {
	     $sol = array();
	     $con = new conexaoImovel();
		  $sql = "SELECT SOL_ISN, SOL_NOM, SOL_MAIL, SOL_MSG, DAY(SOL_DAT) AS DIA,
		         MONTH(SOL_DAT) AS MES, YEAR(SOL_DAT) AS ANO, SOL_STA FROM tab_sol WHERE 1=1 ";
		  if($nome) {
		     $sql = $sql." AND SOL_NOM LIKE '%$nome%' ";
		  }
		  if($tipo) {
		     $sql = $sql." AND TPS_ISN = $tipo ";
		  }
		  if(($dataI) && ($dataF)) {
		     $sql = $sql." AND SOL_DAT >= CDATE('$dataI') AND SOL_DAT <= CDATE('$dataF')";
		  }
		  $conn = $con->connDb();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  $i = 0;
		  while(odbc_fetch_row($res)) {
			 $sol[$i][sol_isn]       = odbc_result($res,"SOL_ISN"); 
			 $sol[$i][sol_data_dia]  = odbc_result($res,"DIA");
			 $sol[$i][sol_data_mes]  = odbc_result($res,"MES");
			 $sol[$i][sol_data_ano]  = odbc_result($res,"ANO");
			 $sol[$i][sol_nome]      = odbc_result($res,"SOL_NOM");
			 $sol[$i][sol_email]     = odbc_result($res,"SOL_MAIL");
			 $sol[$i][sol_msg]       = odbc_result($res,"SOL_MSG");
			 $sol[$i][sol_sta]       = odbc_result($res,"SOL_STA");
			 $i++;
		  } 
		  odbc_close($conn);	
		  return $sol;
	  }
     function excluirSolicitacao($sol) {
	     $con = new conexaoImovel();
		  for($i=0;$i<count($sol);$i++) {
			  $sql = "DELETE * FROM tab_sol WHERE SOL_ISN = $sol[$i] ";	  
			  $conn = $con->connDb();
			  $res = odbc_exec($conn,$sql);
			  if (odbc_errormsg() != "") {
				 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
			  }
			  else {
				 $msg = 1;
			  }
		  }
		  odbc_close($conn);
		  return $msg;
	  }
	  function alterarSitOuv($opc,$sta) {
	    $con = new conexaoImovel();   
		 $qtd = count($opc);
		 $ult = $qtd - 1;
       for($i=0;$i < $qtd;$i++) {
		    if(($i == 0) && ($i < $ult)) {
			    $cod = "(".$opc[$i].",";
			 }
			 if(($i > 0) && $i < $ult) {
			    $cod = $cod."".$opc[$i].",";
			 }
			 if(($ult > 0) && ($i == $ult)) {
			    $cod = $cod."".$opc[$i];
			 }
			 if(($ult == 0) && ($i == $ult)) {
			    $cod = $cod."(".$opc[$i];
			 }
		 }
		 $cod = $cod.")";
		 $sql = "UPDATE tab_ouv SET OUV_STA = $sta WHERE OUV_ISN IN $cod";
	    $conn=$con->connDb();	
	    $res = odbc_exec($conn,$sql);
	    if (odbc_errormsg() != "") { 
		   die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
	    }
		 else {
		    $sql2 = "SELECT OUV_STA FROM tab_ouv ";
			 $res2 = odbc_exec($conn,$sql2);
			 $sitAt = array();
			 $id = 0;
			 while(odbc_fetch_row($res2)) {
			    $sitAt[$id] = odbc_result($res2,"OUV_STA");
				 $id++;
			 }
		 }
	   odbc_close($conn);		
	   return $sitAt;
	  }
	  function alterarSitSol($opc,$sta) {
	    $con = new conexaoImovel();   
		 $qtd = count($opc);
		 $ult = $qtd - 1;
       for($i=0;$i < $qtd;$i++) {
		    if(($i == 0) && ($i < $ult)) {
			    $cod = "(".$opc[$i].",";
			 }
			 if(($i > 0) && $i < $ult) {
			    $cod = $cod."".$opc[$i].",";
			 }
			 if(($ult > 0) && ($i == $ult)) {
			    $cod = $cod."".$opc[$i];
			 }
			 if(($ult == 0) && ($i == $ult)) {
			    $cod = $cod."(".$opc[$i];
			 }
		 }
		 $cod = $cod.")";
		 $sql = "UPDATE tab_sol SET SOL_STA = $sta WHERE SOL_ISN IN $cod";
	    $conn=$con->connDb();	
	    $res = odbc_exec($conn,$sql);
	    if (odbc_errormsg() != "") { 
		   die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
	    }
		 else {
		    $sql2 = "SELECT SOL_STA FROM tab_sol ";
			 $res2 = odbc_exec($conn,$sql2);
			 $sitAt = array();
			 $id = 0;
			 while(odbc_fetch_row($res2)) {
			    $sitAt[$id] = odbc_result($res2,"SOL_STA");
				 $id++;
			 }
		 }
	   odbc_close($conn);		
	   return $sitAt;
	  }
	  function cadastrarTipoMsg($tipo,$atd,$exb) {
	     if($exb) {
	        $exib = 1;
		  }
		  else {
	        $exib = 0;
		  }
	     $con = new conexaoImovel();
		  $sql = "INSERT INTO tab_tpm "; 
        if(($tipo) && ($atd)) {
		  $sql = $sql."(TPM_DES,TPM_TEM_ATD,TPM_EXIB) VALUES ('$tipo',$atd,$exib)";
		  }
        if(($tipo) && (!$atd)) {
		  $sql = $sql."(TPM_DES,TPM_EXIB) VALUES ('$tipo',$exib)";
		  }
        if((!$tipo) && ($atd)) {
		  $sql = $sql."(TPM_TEM_ATD,TPM_EXIB) VALUES ($atd,$exib)";
		  }
		  $conn = $con->connDb();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  else {
		     $msg = 1;
		  }
	    odbc_close($conn);		
	    return $msg;
	  }
	  function alterarTipoMsg($cod,$tipo,$atd,$exb) {
	     if($exb) {
	        $exib = 1;
		  }
		  if(!$exb) {
	        $exib = 0;
		  }
		  if(!$atd) {
		     $atd = 0;
		  }
	     $con = new conexaoImovel();
		  $sql = "UPDATE tab_tpm SET TPM_DES = '$tipo', TPM_TEM_ATD = '$atd', TPM_EXIB = $exib WHERE TPM_ISN = $cod"; 
		  $conn = $con->connDb();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  else {
		     $msg = 1;
		  }
	    odbc_close($conn);		
	    return $msg;
	  }
	  function consultarTipoMsg() {
	     $con = new conexaoImovel();
		  $sql = "SELECT * FROM tab_tpm";
		  $conn = $con->connDb();
		  $tipo = array();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  $i = 0;
		  while(odbc_fetch_row($res)) {
		     $tipo[$i][tpm_isn]     = odbc_result($res,"TPM_ISN");
			  $tipo[$i][tpm_des]     = odbc_result($res,"TPM_DES");
			  $tipo[$i][tpm_tem_atd] = odbc_result($res,"TPM_TEM_ATD");
			  $tipo[$i][tpm_exib]    = odbc_result($res,"TPM_EXIB");
			  $i++;
		  }
	    odbc_close($conn);		
	    return $tipo;
	  }
	  function editarTipoMsg($cod) {
	     $con = new conexaoImovel();
		  $sql = "SELECT * FROM tab_tpm WHERE TPM_ISN = $cod";
		  $conn = $con->connDb();
		  $tipo = array();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  while(odbc_fetch_row($res)) {
		     $tipo[tpm_isn]     = odbc_result($res,"TPM_ISN");
			  $tipo[tpm_des]     = odbc_result($res,"TPM_DES");
			  $tipo[tpm_tem_atd] = odbc_result($res,"TPM_TEM_ATD");
			  $tipo[tpm_exib]    = odbc_result($res,"TPM_EXIB");
		  }
	    odbc_close($conn);		
	    return $tipo;
	  }
	  function excluirTipoMsg($cod) {
	     $con = new conexaoImovel();
  		  $conn = $con->connDb();
		  $sql2 = "SELECT OUV_ISN FROM tab_ouv WHERE TPM_ISN = $cod";
		  $res2 = odbc_exec($conn,$sql2); 
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  $i = 0;
		  while(odbc_fetch_row($res2)) {
		     $codigo[$i] = odbc_result($res2,"OUV_ISN");
			  $i++;
		  }
		  if($codigo) {
		     ?>
			     <script language="javascript">
				     alert("N�o � poss�vel excluir o registro, o mesmo est� sendo usado");
					  location.href = "../../adm/index_adm.php?cod=14";
				  </script>
			  <?
		  }
		  else {
			  $sql = "DELETE * FROM tab_tpm WHERE TPM_ISN = $cod";
			  $tipo = array();
			  $res = odbc_exec($conn,$sql);
			  if (odbc_errormsg() != "") {
				 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
			  }
			  else {
				  $tipo = $this->consultarTipoMsg();
			  }
		 }	  
	    odbc_close($conn);		
	    return $tipo;
	  }
	  function cadastrarTipoSol($tipo,$exb) {
	     if($exb) {
		     $exib = 1;
		  }
		  else {
		     $exib = 0;
		  }
	     $con = new conexaoImovel();
		  $sql = "INSERT INTO tab_tps "; 
        if($tipo) {
		  $sql = $sql."(TPS_DES,TPS_EXIB) VALUES ('$tipo',$exib)";
		  }
		  $conn = $con->connDb();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  else {
		     $msg = 1;
		  }
	    odbc_close($conn);		
	    return $msg;
	  }
	  function alterarTipoSol($cod,$tipo,$exb) {
	     if($exb) {
		     $exib = 1;
		  }
		  else {
		     $exib = 0;
		  }
	     $con = new conexaoImovel();
		  $sql = "UPDATE tab_tps SET TPS_DES = '$tipo', TPS_EXIB = $exib WHERE TPS_ISN = $cod"; 
		  $conn = $con->connDb();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  else {
		     $msg = 1;
		  }
	    odbc_close($conn);		
	    return $msg;
	  }
	  function consultarTipoSol() {
	     $con = new conexaoImovel();
		  $sql = "SELECT * FROM tab_tps";
		  $conn = $con->connDb();
		  $tipo = array();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  $i = 0;
		  while(odbc_fetch_row($res)) {
		     $tipo[$i][tps_isn]     = odbc_result($res,"TPS_ISN");
			  $tipo[$i][tps_des]     = odbc_result($res,"TPS_DES");
			  $tipo[$i][tps_exib]    = odbc_result($res,"TPS_EXIB");
			  $i++;
		  }
	    odbc_close($conn);		
	    return $tipo;
	  }
	  function editarTipoSol($cod) {
	     $con = new conexaoImovel();
		  $sql = "SELECT * FROM tab_tps WHERE TPS_ISN = $cod";
		  $conn = $con->connDb();
		  $tipo = array();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  while(odbc_fetch_row($res)) {
		     $tipo[tps_isn]     = odbc_result($res,"TPS_ISN");
			  $tipo[tps_des]     = odbc_result($res,"TPS_DES");
			  $tipo[tps_exib]    = odbc_result($res,"TPS_EXIB");
		  }
	    odbc_close($conn);		
	    return $tipo;
	  }
	  function excluirTipoSol($cod) {
	     $con = new conexaoImovel();
  		  $conn = $con->connDb();
		  $sql2 = "SELECT SOL_ISN FROM tab_sol WHERE TPS_ISN = $cod";
		  $res2 = odbc_exec($conn,$sql2); 
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  $i = 0;
		  while(odbc_fetch_row($res2)) {
		     $codigo[$i] = odbc_result($res2,"SOL_ISN");
			  $i++;
		  }
		  if($codigo) {
		     ?>
			     <script language="javascript">
				     alert("N�o � poss�vel excluir o registro, o mesmo est� sendo usado");
					  location.href = "../../adm/index_adm.php?cod=17";
				  </script>
			  <?
		  }
		  else {
			  $sql = "DELETE * FROM tab_tps WHERE TPS_ISN = $cod";
			  $tipo = array();
			  $res = odbc_exec($conn,$sql);
			  if (odbc_errormsg() != "") {
				 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
			  }
			  else {
				  $tipo = $this->consultarTipoSol();
			  }
		 }	  
	    odbc_close($conn);		
	    return $tipo;
	  }
	  function cadastrarUsu($nome,$login,$senha,$tipoUsu) {
	     $con = new conexaoImovel();
		  $sql = "SELECT USU_ISN FROM tab_usu WHERE USU_NOM = '$nome' AND USU_LOG = '$login' AND USU_PSWD = '$senha' "; 
		  $conn = $con->connDb();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  while(odbc_fetch_row($res)) {
		     $cod_usu = odbc_result($res,"USU_ISN");
		  }
		  if($cod_usu) {
		  ?>
		     <script language="javascript">
			     alert("Usu�rio j� cadastrado");
				  location.href = "../../adm/index_adm.php?cod=19";
			  </script>
		  <?
		  }
		  else {
		     $sql2 = "INSERT INTO tab_usu (USU_NOM,USU_LOG,USU_PSWD,USU_PERM,USU_NUM_MAX_ATD,USU_NUM_ATD_ATU,USU_STA) VALUES ('$nome','$login','$senha',$tipoUsu,3,0,0)";
			  $res2 = odbc_exec($conn,$sql2);
			  if (odbc_errormsg() != "") {
				 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
			  }
			  else {
			     $msg = 1;
			  }
		  }
	    odbc_close($conn);		
	    return $msg;
	  }
	  function consultarUsu() {
	     $con = new conexaoImovel();
		  $sql = "SELECT * FROM tab_usu WHERE USU_ISN <> 10";
		  $conn = $con->connDb();
		  $usuario = array();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  $i = 0;
		  while(odbc_fetch_row($res)) {
		     $usuario[$i][usu_isn]     = odbc_result($res,"USU_ISN");
			  $usuario[$i][usu_nom]     = odbc_result($res,"USU_NOM");
			  $usuario[$i][usu_login]   = odbc_result($res,"USU_LOG");
			  $usuario[$i][usu_senha]   = odbc_result($res,"USU_PSWD");
			  $i++;
		  }
	    odbc_close($conn);		
	    return $usuario;
	  }
	  function editarUsu($cod) {
	     $con = new conexaoImovel();
		  $sql = "SELECT * FROM tab_usu WHERE USU_ISN = $cod";
		  $conn = $con->connDb();
		  $usu = array();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  while(odbc_fetch_row($res)) {
		     $usu[usu_isn]     = odbc_result($res,"USU_ISN");
			  $usu[usu_nom]     = odbc_result($res,"USU_NOM");
			  $usu[usu_log]     = odbc_result($res,"USU_LOG");
  			  $usu[usu_pswd]    = odbc_result($res,"USU_PSWD");
			  $usu[usu_perm]    = odbc_result($res,"USU_PERM");
		  }
	    odbc_close($conn);		
	    return $usu;
	  }
	  function alterarUsu($nome,$login,$senha,$cod,$tipoUsu) {
	     $con = new conexaoImovel();
		  $sql = "UPDATE tab_usu SET USU_NOM = '$nome', USU_LOG = '$login', USU_PSWD = '$senha', USU_PERM = $tipoUsu WHERE USU_ISN = $cod"; 
		  $conn = $con->connDb();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  else {
		     $usu = $this->consultarUsu();
		  }
	    odbc_close($conn);		
	    return $usu;
	  }
	  function excluirUsu($cod) {
	     $con = new conexaoImovel();
		  $sql = "DELETE * FROM  tab_atd WHERE USU_ISN = $cod";
		  $conn = $con->connDb();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  $sql = "DELETE * FROM  tab_usu WHERE USU_ISN = $cod"; 
		  $conn = $con->connDb();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  else {
		     $usu = $this->consultarUsu();
		  }
	    odbc_close($conn);		
	    return $usu;
	  }
	  function consultarImoLoc($nome,$log,$num,$bairro,$cidade,$fin) {
	     $con = new conexaoImovel();
		  $sql = "SELECT * FROM tab_cim WHERE 1 = 1 ";
		  if($fin) {
		     $sql = $sql."AND CIM_FIN = $fin ";
		  }
		  if($nome) {
		     $sql = $sql."AND CIM_NOM LIKE '%$nome%'";
		  }
		  if($log) {
		     $sql = $sql."AND CIM_DES_LOG LIKE '%$log%'";
		  }
		  if($num) {
		     $sql = $sql."AND CIM_NUM_END LIKE '%$num%'";
		  }
		  if($bairro) {
		     $sql = $sql."AND CIM_DES_BAI LIKE '%$bairro%'";
		  }
		  if($cidade) {
		     $sql = $sql."AND CIM_DES_CID LIKE '%$cidade%'";
		  }
		  $conn = $con->connDb();
		  $imo = array();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  $i=0;
		  while(odbc_fetch_row($res)) {
		     $imo[$i][imo_isn]     = odbc_result($res,"CIM_ISN");
			  $imo[$i][imo_nom]     = odbc_result($res,"CIM_NOM");
			  $imo[$i][imo_log]     = odbc_result($res,"CIM_DES_LOG");
			  $imo[$i][imo_num]     = odbc_result($res,"CIM_NUM_END");
  			  $imo[$i][imo_bai]     = odbc_result($res,"CIM_DES_BAI");
			  $imo[$i][imo_cid]     = odbc_result($res,"CIM_DES_CID");
			  $imo[$i][imo_mail]    = odbc_result($res,"CIM_MAIL");
  			  $imo[$i][imo_resumo]  = odbc_result($res,"CIM_RES_IMO");
			  $imo[$i][imo_finali]   = odbc_result($res,"CIM_FIN");
			  $imo[$i][imo_fone1]   = odbc_result($res,"CIM_NUM_FON_1");
			  $imo[$i][imo_fone2]   = odbc_result($res,"CIM_NUM_FON_2");
			  $imo[$i][imo_cel]   = odbc_result($res,"CIM_NUM_CEL");
			  $imo[$i][imo_comp]   = odbc_result($res,"CIM_DES_COMP");
			  $imo[$i][imo_cep]   = odbc_result($res,"CIM_DES_CEP");
			  $imo[$i][imo_uf]   = odbc_result($res,"CIM_RES_UF");
			  $i++;
		  }
	    odbc_close($conn);		
	    return $imo;
	  }
	  function excluirCadastro($cad) {
	     $con = new conexaoImovel();
		  for($i=0;$i<count($cad);$i++) {
			  $sql = "DELETE * FROM tab_cim WHERE CIM_ISN = $cad[$i]";  
		     $conn = $con->connDb();
			  $res = odbc_exec($conn,$sql);
			  if (odbc_errormsg() != "") {
				 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
			  }
			  else {
				 $msg = 1;
			  }		  
		  }
		  odbc_close($conn);
		  return $msg;
	  }
	  function buscarUsuarios() {
	     $con = new conexaoImovel();
        $sql = "SELECT USU_NOM, USU_ISN FROM tab_usu WHERE USU_ISN <> 10";	  
		  $conn = $con->connDb();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  $i = 0;
		  while(odbc_fetch_row($res)) {
			  $usu[$i][usu_isn]     = odbc_result($res,"USU_ISN");
			  $usu[$i][usu_nom]     = odbc_result($res,"USU_NOM");
			  $i++;
		  }
	    odbc_close($conn);		
	    return $usu;
    }
	 function buscarAtd($usu,$dataI,$dataF) {
	     $con = new conexaoImovel();
        $sql = "SELECT ATD.ATD_ISN,ATD.ATD_STA,CLI.CLI_NOME, USU.USU_NOM FROM tab_atd ATD,tab_cli CLI,tab_usu USU 
		         WHERE ATD.USU_ISN = USU.USU_ISN AND ATD.CLI_ISN = CLI.CLI_ISN ";	
		  if($usu) {
		     $sql = $sql."AND USU.USU_ISN = $usu ";
		  }			  
		  if($dataI) {
		     $sql = $sql."AND ATD.ATD_DAT_INI >= $dataI ";
		  }			
		  if($dataF) {
		     $sql = $sql."AND ATD.ATD_DAT_FIM <= $dataF";
		  }
		  $conn = $con->connDb();
		  $res = odbc_exec($conn,$sql);
		  if (odbc_errormsg() != "") {
			 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
		  }
		  $i = 0;
		  while(odbc_fetch_row($res)) {
			  $atd[$i][atd_isn]     = odbc_result($res,"ATD_ISN");
			  $atd[$i][atd_sta]     = odbc_result($res,"ATD_STA");
			  $atd[$i][cli_nome]    = odbc_result($res,"CLI_NOME");
			  $atd[$i][usu_nome]    = odbc_result($res,"USU_NOM");
			  $i++;
		  }
	    odbc_close($conn);		
	    return $atd;
	 }
	 function excluirAtd($id) {
	     $con = new conexaoImovel();
	     $qtd = count($id);
		  for($i=0;$i < $qtd;$i++) {
		     $sql = "DELETE FROM tab_msg WHERE ATD_ISN = $id[$i]";
			  $conn = $con->connDb();
			  $res = odbc_exec($conn,$sql);
			  if (odbc_errormsg() != "") {
				 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
			  }
		  } 
		  for($i=0;$i < $qtd;$i++) {
		     $sql2 = "DELETE FROM tab_atd WHERE ATD_ISN = $id[$i]";
			  $res2 = odbc_exec($conn,$sql2);
			  if (odbc_errormsg() != "") {
				 die('<br><br>Query invalida.: <br><br>' . odbc_errormsg());
			  }
			  else {
			     $msg = 1;
			  }
		  } 
		  odbc_close($conn);
		  return msg;
	 }
 }
?>