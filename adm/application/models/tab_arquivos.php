<?php
require_once('conexao.php');

class tab_arquivos extends Conexao{
	
	public function __construct()
    {
        parent::Model();
    }
    
    public function getPagina($pag)
	{
        $this->db->where('ARQ_ISN', $pag);
		$query = $this->db->get('tab_arquivos');
		$row ="";
		if($query->num_rows() > 0){
				$row = $query->row();
		}
		return $row;
    }
    
    function excluirItens($cods) { 	
		$sql  = "DELETE FROM tab_arquivos WHERE ARQ_ISN IN ($cods)";
		$this->db->query($sql);
	}
    
    
    public function contaRegistros($busca)
	{
        $sql = "SELECT COUNT(ARQ_ISN) AS TOTAL FROM tab_arquivos";
		if($busca)
		{
			$sql.=" WHERE ARQ_NOME LIKE '%$busca%' ";
		}
		$res = $this->db->query($sql);
		foreach($res->result() as $array){
            $total = $array->TOTAL;
		}
		return $total;
    }
    
    
    public function getArquivos($maximo,$inicio,$busca,$ordem='ARQ_ISN ASC') 
	{
        $sql = "SELECT * FROM tab_arquivos";
		if($busca){
            $sql.=" WHERE ARQ_NOME LIKE '%$busca%' ";
		}
		$sql.=" ORDER BY $ordem";
		$sql.=" LIMIT $inicio,$maximo";
		$arqs="";
		$res = $this->db->query($sql);
		if($res->num_rows() > 0)
        {
            $i = 0;
            foreach($res->result() as $array)
            {
                $arqs[$i]["id"]        =  $array->ARQ_ISN; 
				$arqs[$i]["nome"]      =  $array->ARQ_NOME;
				$arqs[$i]["tipo"]      =  $array->ARQ_TIPO;
				$arqs[$i]["caminho"]   =  $array->ARQ_CAM;				
				$i++;
            }
		}
		return $arqs;
    }  
    
    
    public function buscarArquivo($cod) {     
	  $sql  = "SELECT * FROM tab_arquivos WHERE ARQ_ISN = $cod";
	  $res = $this->db->query($sql);
	  $arqs="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			$arqs["id"]    = $array->ARQ_ISN; 
			$arqs["nome"]  = $array->ARQ_NOME;  
			$arqs["tipo"] = $array->ARQ_TIPO;
			$arqs["caminho"] =  $array->ARQ_CAM;			
		  }
	  }
	  return $arqs;
	}
        
		
	private function RemoveAcentos($s){
		$s = preg_replace("/[����]/","a",$s);
		$s = preg_replace("/[����]/","A",$s);
		$s = preg_replace("/[���]/","e",$s);
		$s = preg_replace("/[���]/","E",$s);
		$s = preg_replace("/[�����]/","o",$s);
		$s = preg_replace("/[����]/","O",$s);
		$s = preg_replace("/[���]/","u",$s);
		$s = preg_replace("/[���]/","U",$s);
		$s = preg_replace("/�/","c",$s);
		$s = preg_replace("/�/","C",$s);
		$s = preg_replace("/ /","_",$s);
		return $s;
	}	
	
	
		
	public function cadastrarArquivo($nome, $tipo, $arquivo, $cam='') {
        
		if($arquivo["type"])
		{
			if($arquivo["size"] < 10000000) // 10 mega
			{
				preg_match("/\.(ZIP|zip|XLS|xls|PPT|ppt|GIF|gif|PNG|png|JPG|jpg|docx|DOCX|doc|DOC|txt|TXT|pdf|PDF){1}$/i", $arquivo["name"], $ext);
				// Gera um nome �nico para a imagem
				//$imagem_nome = uniqid(time()).".".  strtolower($ext[1]);
				$name = explode(".",$arquivo["name"]);
				$imagem_nome = $this->RemoveAcentos($name[0]).".". strtolower($ext[1]);
				// Caminho de onde a imagem ficar�
				$imagem_dir = "adm/arquivos/".$imagem_nome;
				// Faz o upload da imagem
				if(@move_uploaded_file($arquivo["tmp_name"], $cam.$imagem_dir))
				{
					$sql = "INSERT INTO tab_arquivos(ARQ_NOME, ARQ_TIPO, ARQ_CAM) VALUES ('$nome','$tipo','$imagem_dir')";
					$res = $this->db->query($sql);	
					$codigo = $this->db->insert_id();	 
					if ($codigo>0) 
					{		   
						return 7;
					}
					else 	
					{
						//erro ao inserir no banco de dados
						return 6;
					}
				}
			}
			else 
			{
				//erro do tamnho do arquivo
				return 2;
			}
		}
		else 
		{
			//erro no tipo do arquivo
			return 1;
		} 	
	}
    
    
    public function alterarArquivo($codigo, $nome, $tipo, $arquivo, $cam='') {
		
		if($arquivo["type"])
		{
			if($arquivo["size"] < 6000000)
			{
				preg_match("/\.(ZIP|zip|XLS|xls|PPT|ppt|GIF|gif|PNG|png|JPG|jpg|docx|DOCX|doc|DOC|txt|TXT|pdf|PDF){1}$/i", $arquivo["name"], $ext);
				// Gera um nome �nico para a imagem
        		//$imagem_nome = uniqid(time()).".".strtolower($ext[1]);
				$name = explode(".",$arquivo["name"]);
				$imagem_nome = $name[0].".". strtolower($ext[1]);
				// Caminho de onde a imagem ficar�
        		$imagem_dir = "adm/arquivos/".$imagem_nome;
				// Faz o upload da imagem
				$ver = move_uploaded_file($arquivo["tmp_name"], $cam.$imagem_dir);
				
				$sql = "UPDATE tab_arquivos SET ARQ_NOME = '{$nome}', ARQ_TIPO = '{$tipo}', ARQ_CAM = '{$imagem_dir}'  WHERE ARQ_ISN = {$codigo}";
					
				$res = $this->db->query($sql);	
				$codigo = $this->db->insert_id();	 
				return 7;
			}
			else 
			{
				//erro do tamnho do arquivo
				return 2;
			}
		}
		else
		{
			$sql = "UPDATE tab_arquivos SET ARQ_NOME = '{$nome}', ARQ_TIPO = '{$tipo}' WHERE ARQ_ISN = {$codigo}";
		   
			$res = $this->db->query($sql);	
			$codigo = $this->db->insert_id();	 
			if ($codigo>0) {		   
				return 7;
			}else {
			   //erro ao inserir no banco de dados
			   return 6;
		   }
		} 		   
	}
}
?>