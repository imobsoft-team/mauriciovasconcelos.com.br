<?php
class tab_modelo extends Model{
	function tab_modelo(){
		parent::Model();
	}
	
	function getModelosAll($ordem='MOD_ISN ASC') {
			$sql = "SELECT * FROM tab_modelo ORDER BY $ordem";
			$mod="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $array){
				    $mod[$i]["id"]       =  $array->MOD_ISN; 
				    $mod[$i]["nom"]      =  $array->MOD_NOM; 
				    $mod[$i]["sta"]      =  $array->MOD_STA;  
				    $i++;
				}
			}
			return $mod;
	}
	
	function getModelos($maximo,$inicio,$busca,$ordem='MOD_ISN ASC') {
			$sql = "SELECT * FROM tab_modelo";
		    if($busca){
				$sql.=" WHERE MOD_NOM LIKE '%".$busca."%'";
			}
			$sql.=" ORDER BY $ordem";
			$sql.=" LIMIT $inicio,$maximo";
			$mod="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $array){
				    $mod[$i]["id"]       =  $array->MOD_ISN; 
				    $mod[$i]["nom"]      =  $array->MOD_NOM; 
				    $mod[$i]["sta"]      =  $array->MOD_STA;  
				    $i++;
				}
			}
			return $mod;
	}
	
	function contaRegistros($busca){
		$sql = "SELECT COUNT(MOD_ISN) AS TOTAL FROM tab_modelo";
		if($busca){
			$sql.=" WHERE MOD_NOM LIKE '%".$busca."%'";
		}
		$res = $this->db->query($sql);
		foreach($res->result() as $array){
			$total = $array->TOTAL;
		}
		return $total;
	}
	
	function cadastrarNews($nome,$texto,$sta,$banner,$cam='') {	   
	   if($banner["type"]){
		   if(eregi("^image\/(pjpeg|jpeg|JPEG|jpg|JPG)$", $banner["type"])){
		      if($banner["size"] < 6000000){
				// Para verificar as dimens�es da imagem
				$tamanhos = getimagesize($banner["tmp_name"]);
				// Verifica largura e altura da imagem
				if($tamanhos[0] > 5000){
						//erro de largura
						return 3;
				}else if($tamanhos[1] > 5000){
						//erro de altura
						return 4;
				}else {
					//pega a extens�o
					preg_match("/\.(pjpeg|jpeg|JPEG|jpg|JPG){1}$/i", $banner["name"], $ext);
					// Gera um nome �nico para a imagem
        			$imagem_nome = uniqid(time()).".".$ext[1];
					// Caminho de onde a imagem ficar�
        			$imagem_dir = "adm/imagens/".$imagem_nome;
					// Faz o upload da imagem
					if(@move_uploaded_file($banner["tmp_name"], $cam.$imagem_dir)){
					   $sql = "INSERT INTO tab_modelo (MOD_NOM,MOD_DES,MOD_FOTO,MOD_STA) VALUES ('$nome','$texto','$imagem_dir',$sta)";	 
					   $res = $this->db->query($sql);	
					   $codigo = $this->db->insert_id();	 
					   if ($codigo>0) {		   
							return 7;
					   }else {
						   //erro ao inserir no banco de dados
						   @unlink($cam.$imagem_dir);
						   return 6;
					   } 	
					}else {
						//erro no upload para o servidor.
						return 5;
					}
				}
			  }else {
			  	 //erro do tamnho do arquivo
				 return 2;
			  }
		   }else {
		   	  //erro no tipo do arquivo
			  return 1;
		   }
	   }else {
	   	   $sql = "INSERT INTO tab_modelo (MOD_NOM,MOD_DES,MOD_STA) VALUES ('$nome','$texto',$sta)";
		   $res = $this->db->query($sql);	
		   $codigo = $this->db->insert_id();	 
		   if ($codigo>0) {		   
				return 7;
		   }else {
			   //erro ao inserir no banco de dados
			   return 6;
		   } 	
	   }	   
	}
	
	function buscarNews($cod) {     
	  $sql  = "SELECT * FROM tab_modelo WHERE MOD_ISN = $cod";
	  $res = $this->db->query($sql);
	  $mod="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			$mod["id"]       =  $array->MOD_ISN; 
			$mod["nom"]      =  $array->MOD_NOM; 
			$mod["des"]      =  $array->MOD_DES; 
			$mod["sta"]      =  $array->MOD_STA; 
			$mod["foto"]     =  $array->MOD_FOTO;   
		  }
	  }
	  return $mod;
	}
	
	function alterarNews($nome,$texto,$sta,$banner,$id,$antiga,$cam) {     
	   if($banner["type"]){
		   if(eregi("^image\/(pjpeg|jpeg|JPEG|jpg|JPG)$", $banner["type"])){
		      if($banner["size"] < 6000000){
				// Para verificar as dimens�es da imagem
				$tamanhos = getimagesize($banner["tmp_name"]);
				// Verifica largura e altura da imagem
				if($tamanhos[0] > 5000){
						//erro de largura
						return 3;
				}else if($tamanhos[1] > 5000){
						//erro de altura
						return 4;
				}else {
					//pega a extens�o
					preg_match("/\.(pjpeg|jpeg|JPEG|jpg|JPG){1}$/i", $banner["name"], $ext);
					// Gera um nome �nico para a imagem
        			$imagem_nome = uniqid(time()).".".$ext[1];
					// Caminho de onde a imagem ficar�
        			$imagem_dir = "adm/imagens/".$imagem_nome;
					// Faz o upload da imagem
					if(@move_uploaded_file($banner["tmp_name"], $cam.$imagem_dir)){
					   @unlink($cam.$antiga);
					   $sql = "UPDATE tab_modelo SET MOD_NOM = '$nome', MOD_DES = '$texto', MOD_FOTO = '$imagem_dir', MOD_STA = $sta WHERE MOD_ISN = $id";					   	 
					   $this->db->query($sql);	 
					   return 6; 	
					}else {
						//erro no upload para o servidor.
						return 5;
					}
				}
			  }else {
			  	 //erro do tamnho do arquivo
				 return 2;
			  }
		   }else {
		   	  //erro no tipo do arquivo
			  return 1;
		   }
	   }else {
	   	   $sql = "UPDATE tab_modelo SET MOD_NOM = '$nome', MOD_DES = '$texto', MOD_STA = $sta WHERE MOD_ISN = $id";
		  $this->db->query($sql);	
		  return 6;
	   }	   
	}
	
	function excluirItens($cods,$cam) { 	
		$sql = "SELECT MOD_FOTO FROM tab_modelo WHERE MOD_ISN IN ($cods)";
		$res = $this->db->query($sql);
		if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			@unlink($cam.$array->MOD_FOTO); 
		  }
		}	
		$sql  = "DELETE FROM tab_modelo WHERE MOD_ISN IN ($cods)";
		$this->db->query($sql);
	} 

}
?>