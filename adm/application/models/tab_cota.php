<?php
class Tab_cota extends Model{
	function Tab_cota(){
		parent::Model();
	}
	
	function getDados1(){
		$this->db->where('COTA_NUM', 1);
		$query = $this->db->get('tab_cota');
		$row ="";
		if($query->num_rows() > 0){
			foreach($query->result() as $dados){
				$row[]=array('id'=>$dados->COTA_ISN,'num'=>$dados->COTA_NUM,'mes'=>$dados->COTA_MES,'ano'=>$dados->COTA_ANO,'val'=>$dados->COTA_VAL);
			}
			
		}
		return $row;
	}
	
	function getDados2(){
		$this->db->where('COTA_NUM', 2);
		$query = $this->db->get('tab_cota');
		$row ="";
		if($query->num_rows() > 0){
			foreach($query->result() as $dados){
				$row[]=array('id'=>$dados->COTA_ISN,'num'=>$dados->COTA_NUM,'mes'=>$dados->COTA_MES,'ano'=>$dados->COTA_ANO,'val'=>$dados->COTA_VAL);
			}
			
		}
		return $row;
	}
	
	function getCotas($maximo,$inicio,$busca,$ordem='COTA_ISN ASC') {
			$sql = "SELECT * FROM tab_cota";
		    if($busca){
				$busca = explode('/',$busca); 
				$sql.=" WHERE COTA_MES = ".$busca[0]." AND COTA_ANO = ".$busca[1];
			}
			$sql.=" ORDER BY $ordem";
			$sql.=" LIMIT $inicio,$maximo";
			$cot="";
			$res = $this->db->query($sql);
			if($res->num_rows() > 0){
				$i = 0;
				foreach($res->result() as $array){
				    $cot[$i]["id"]     =  $array->COTA_ISN; 
					$cot[$i]["num"]    =  $array->COTA_NUM; 
					$cot[$i]["mes"]    =  str_pad($array->COTA_MES,2,"0",STR_PAD_LEFT); 
					$cot[$i]["ano"]    =  $array->COTA_ANO; 
					$cot[$i]["val"]    =  $array->COTA_VAL; 
				    $i++;
				}
			}
			return $cot;
	}
	
	function contaRegistros($busca){
		$sql = "SELECT COUNT(COTA_ISN) AS TOTAL FROM tab_cota";
		if($busca){
			$busca = explode('/',$busca); 
			$sql.=" WHERE COTA_MES = ".$busca[0]." AND COTA_ANO = ".$busca[1];
		}
		$res = $this->db->query($sql);
		foreach($res->result() as $array){
			$total = $array->TOTAL;
		}
		return $total;
	}
	
	function cadastrarCota($indice,$mes,$ano,$val) {	   
	   $sql = "INSERT INTO tab_cota (COTA_NUM,COTA_MES,COTA_ANO,COTA_VAL) 
	       VALUES ($indice,$mes,$ano,$val)";	  
	   $res = $this->db->query($sql);	
	   $codigo = $this->db->insert_id();	 
	   if ($codigo>0) {		   
			return $codigo;
	   }else {
		   return false;
	   } 	
	}
	
	function buscarCota($cod) {     
	  $sql  = "SELECT * FROM tab_cota WHERE COTA_ISN = $cod";
	  $res = $this->db->query($sql);
	  $cot="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			$cot["id"]     =  $array->COTA_ISN; 
			$cot["num"]    =  $array->COTA_NUM; 
			$cot["mes"]    =  str_pad($array->COTA_MES,2,"0",STR_PAD_LEFT); 
			$cot["ano"]    =  $array->COTA_ANO; 
			$cot["val"]    =  $array->COTA_VAL; 
		  }
	  }
	  return $cot;
	}
	
	function alterarCota($indice,$mes,$ano,$val,$cod) {     
	   $sql = "UPDATE tab_cota SET COTA_NUM= $indice, COTA_MES= $mes, COTA_ANO= $ano, COTA_VAL= $val WHERE COTA_ISN = $cod";	  
	   $this->db->query($sql);
	   return true;
	}
	
	function excluirItens($cods) { 	
		$sql  = "DELETE FROM tab_cota WHERE COTA_ISN IN ($cods)";
		$this->db->query($sql);
	} 

}
?>