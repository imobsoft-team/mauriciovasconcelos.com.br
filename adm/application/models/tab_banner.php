<?php

class Tab_banner extends Model{

    public function __construct()
    {
        parent::Model();
    }
    
    public function getPagina($pag){
        $this->db->where('BANNER_ISN', $pag);
	$query = $this->db->get('tab_banner');
	$row ="";
	if($query->num_rows() > 0){
            $row = $query->row();
	}
	return $row;
    }
    
    public function excluirIten($cod) {
        $ban = $this->buscarBanner($cod);
        unlink("../".$ban['img']);
        $sql  = "DELETE FROM tab_banner WHERE BANNER_ISN = {$cod}";
	$this->db->query($sql);
    } 
    
    
    public function contaRegistros($busca){
        $sql = "SELECT COUNT(BANNER_ISN) AS TOTAL FROM tab_banner";
	if($busca){
            $sql.=" WHERE BANNER_NOME LIKE '%$busca%' ";
	}
	$res = $this->db->query($sql);
	foreach($res->result() as $array){
            $total = $array->TOTAL;
	}
	return $total;
    }
    
    
    public function getBanners($maximo,$inicio,$busca,$ordem='BANNER_ISN ASC') {
        $sql = "SELECT * FROM tab_banner";
	if($busca){
            $sql.=" WHERE BANNER_NOME LIKE '%$busca%' ";
	}
	$sql.=" ORDER BY $ordem";
	//$sql.=" LIMIT $inicio,$maximo";
	$bans="";
	$res = $this->db->query($sql);
	if($res->num_rows() > 0)
        {
            $i = 0;
            foreach($res->result() as $array)
            {
                $bans[$i]["id"]           =  $array->BANNER_ISN; 
				$bans[$i]["nome"]         =  $array->BANNER_NOME; 
				$bans[$i]["data"]         =  $array->BANNER_IMG; 
				$bans[$i]["link"]      	  =  $array->BANNER_LINK;
				$bans[$i]["finalidade"]   = ($array->BANNER_FIN == 1 ? 'Loca��o':'Vendas'); 
				$i++;
            }
	}
        
	return $bans;
    }  
    
    
    public function buscarBanner($cod) {     
	  $sql  = "SELECT * FROM tab_banner WHERE BANNER_ISN = $cod";
	  $res = $this->db->query($sql);
	  $ban="";
	  if($res->num_rows() > 0){
		  foreach($res->result() as $array){
			$ban["id"]    		 = $array->BANNER_ISN; 
			$ban["nome"]  		 = $array->BANNER_NOME;  
			$ban["ativa"] 		 = $array->BANNER_ATIVA;
            $ban["img"]   		 = $array->BANNER_IMG;
			$ban["link"]   		 = $array->BANNER_LINK; 			
			$ban["finalidade"]   = $array->BANNER_FIN; 			
		  }
	  }
	  return $ban;
	}
        
        public function cadastrarBanner($nome, $ativa, $banner,$link, $cam='',$finalidade) {
            
            if($ativa != 0 && $ativa != 1)
                $ativa = 0;
            
            if($banner["type"])
            {
                if(preg_match("/^image\/(pjpeg|jpeg|JPEG|jpg|JPG)$/i", $banner["type"]))
                {
                    if($banner["size"] < 6000000)
                    {
						// Para verificar as dimens�es da imagem
						$tamanhos = getimagesize($banner["tmp_name"]);
						// Verifica largura e altura da imagem
						if($tamanhos[0] > 5000){
								//erro de largura
								return 3;
						}else if($tamanhos[1] > 5000){
								//erro de altura
								return 4;
						}else {
						//pega a extens�o
							preg_match("/\.(pjpeg|jpeg|JPEG|jpg|JPG){1}$/i", $banner["name"], $ext);
							// Gera um nome �nico para a imagem
							$imagem_nome = uniqid(time()).".".$ext[1];
							// Caminho de onde a imagem ficar�
							$imagem_dir = "adm/imagens/".$imagem_nome;
							// Faz o upload da imagem
							if(move_uploaded_file($banner["tmp_name"], $cam.$imagem_dir)){
													
							 $sql = "INSERT INTO tab_banner (BANNER_NOME, BANNER_ATIVA, BANNER_IMG, BANNER_LINK, BANNER_FIN) VALUES ('$nome',$ativa,'$imagem_dir','$link','$finalidade')";
							
							   $res = $this->db->query($sql);	
							   $codigo = $this->db->insert_id();	 
							   if ($codigo>0) {		   
									return 7;
							   }else {
								   //erro ao inserir no banco de dados
								   @unlink($cam.$imagem_dir);
								   return 6;
							   } 	
							}else {
								//erro no upload para o servidor.
								return 5;
							}
						}
					}else {
						//erro do tamnho do arquivo
						return 2;
					}
				}else {
					//erro no tipo do arquivo
					return 1;
				}
			} 
    }
    
    public function alterarBanner($codigo, $nome, $ativa, $banner,$link, $cam='', $finalidade) {
	  if($banner["type"]){
		   if(preg_match("/^image\/(pjpeg|jpeg|JPEG|jpg|JPG)$/i", $banner["type"])){
		      if($banner["size"] < 6000000){
				// Para verificar as dimens�es da imagem
				$tamanhos = getimagesize($banner["tmp_name"]);
				// Verifica largura e altura da imagem
				if($tamanhos[0] > 5000){
						//erro de largura
						return 3;
				}else if($tamanhos[1] > 5000){
						//erro de altura
						return 4;
				}else {
					//pega a extens�o
					preg_match("/\.(pjpeg|jpeg|JPEG|jpg|JPG){1}$/i", $banner["name"], $ext);
					// Gera um nome �nico para a imagem
        			$imagem_nome = uniqid(time()).".".$ext[1];
					// Caminho de onde a imagem ficar�
        			$imagem_dir = "adm/imagens/".$imagem_nome;
					// Faz o upload da imagem
					$ver = move_uploaded_file($banner["tmp_name"], $cam.$imagem_dir);
					
					$sql = "UPDATE tab_banner SET BANNER_NOME = '$nome', BANNER_ATIVA = '$ativa', BANNER_IMG = '$imagem_dir', BANNER_LINK = '$link', BANNER_FIN = '$finalidade'  WHERE BANNER_ISN = $codigo";
					
					   $res = $this->db->query($sql);	
					   $codigo = $this->db->insert_id();	 
				       return 7;
				}
			  }else {
			  	 //erro do tamnho do arquivo
				 return 2;
			  }
		   }else {
		   	  //erro no tipo do arquivo
			  return 1;
		   }
	   }else {
                   
		   $sql = "UPDATE tab_banner SET BANNER_NOME = '{$nome}' , BANNER_ATIVA = {$ativa}, BANNER_LINK = '{$link}', BANNER_FIN = '{$finalidade}'  WHERE BANNER_ISN = {$codigo}";
		   
		   $res = $this->db->query($sql);	
		   $codigo = $this->db->insert_id();	 
		   if ($codigo>0) {		   
			   return 7;
		   }else {
			   //erro ao inserir no banco de dados
			   return 6;
		   } 	
	   }	   
	   
	    
	   //$sql  = "UPDATE tab_not SET NOT_TITULO = '$titulo',NOT_TITULO_ING = '$tituloing',NOT_TITULO_ESP = '$tituloesp', NOT_DES = '$texto',NOT_DES_ING = '$textoing',NOT_DES_ESP = '$textoesp', NOT_FONTE = '$fonte', NOT_DAT_VAL = '$venc' WHERE NOT_ISN = $codigo";	  
	   //$this->db->query($sql);
	   //return $codigo;
	}
	
	
	/****************************  banner box ***************************************/
	function getBann(){
		  $sql  = "SELECT * FROM tab_bannerbox WHERE id = 1";
		  $res = $this->db->query($sql);
		  $not="";
		  if($res->num_rows() > 0){
			  foreach($res->result() as $array){
				 $not  =  $array->imagem;
			  }
		  }
		  return $not;
	}
	
	function alterar($banner,$cam='') {
	   if($banner["type"]){
		   //if(eregi("^image\/(pjpeg|jpeg|JPEG|jpg|JPG)$", $banner["type"])){
		   if(preg_match("/^image\/(pjpeg|jpeg|JPEG|jpg|JPG)$/i", $banner["type"])){
		      if($banner["size"] < 6000000){
				// Para verificar as dimens�es da imagem
				$tamanhos = getimagesize($banner["tmp_name"]);
				// Verifica largura e altura da imagem
				if($tamanhos[0] > 5000){
						//erro de largura
						return 3;
				}else if($tamanhos[1] > 5000){
						//erro de altura
						return 4;
				}else {
					//pega a extens�o
					preg_match("/\.(pjpeg|jpeg|JPEG|jpg|JPG){1}$/i", $banner["name"], $ext);
					// Gera um nome �nico para a imagem
        			$imagem_nome = uniqid(time()).".".$ext[1];
					// Caminho de onde a imagem ficar�
        			$imagem_dir = "adm/imagens/".$imagem_nome;
					// Faz o upload da imagem
					
					if(@move_uploaded_file($banner["tmp_name"], $cam.$imagem_dir)){
					
					   $sql  = "UPDATE tab_bannerbox SET imagem = '$imagem_dir' WHERE id = 1"; 
					   $res = $this->db->query($sql);	
					   $codigo = $this->db->insert_id();
					   return 6; 	
					}else {
						//erro no upload para o servidor.
						return 5;
					}
				}
			  }else {
			  	 //erro do tamnho do arquivo
				 return 2;
			  }
		   }else {
		   	  //erro no tipo do arquivo
			  return 1;
		   }
		}else {
			
			 $imagem_dir = $this->getBann();
			 @unlink($cam."adm/imagens/".$imagem_dir);
			 $sql  = "UPDATE tab_bannerbox SET imagem = '' WHERE id = 1"; 
			 $this->db->query($sql);
			 return 1;
		}
	}
	/************************************************************************/
	
    
    
}