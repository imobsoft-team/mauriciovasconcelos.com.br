function mudaCampos(formulario,acao){
	var action;
	action = acao == '0'?true:false;
	for (i = 0; i < formulario.length; i++)
	{
		formulario.elements[i].disabled = action;	
	}
}

function FormataCpfCgc(campo,tammax,teclapres,frmName) {
	if (frmName == undefined) frmName = "form";
	nmCampo = eval("document."+frmName+"."+campo);
	if (nmCampo) {
		vr = limpaCampo(nmCampo.value);
		if (vr.length <= 11) {
			FormataCpf(campo,tammax,teclapres,frmName)
		} else {
			FormataCgc(campo,tammax,teclapres,frmName)
		}
	}
}

function FormataCpf(campo,tammax,teclapres,frmName) {
	if (frmName == undefined) frmName = "form";
	nmCampo = eval("document."+frmName+"."+campo);
	if (nmCampo) {
		var tecla = teclapres.keyCode;
		vr = limpaCampo(nmCampo.value);
		tam = vr.length + 1;

		if (tam > 3) {
			vr = vr.substr(0, 3) + '.' + vr.substr(3);
			if (tam > 6)
				vr = vr.substr(0, 7) + '.' + vr.substr(7);
				if (tam > 9)
					vr = vr.substr(0, 11) + '-' + vr.substr(11);
		}
  	nmCampo.value = vr;
	}
}

function FormataCgc(campo,tammax,teclapres,frmName) {
	if (frmName == undefined) frmName = "form";
	nmCampo = eval("document."+frmName+"."+campo);
	if (nmCampo) {
		var tecla = teclapres.keyCode;
		vr = limpaCampo(nmCampo.value);
		tam = vr.length + 1;

		if (tam > 2) {
			vr = vr.substr(0, 2) + '.' + vr.substr(2);
			if (tam > 5)
				vr = vr.substr(0, 6) + '.' + vr.substr(6);
				if (tam > 8)
					vr = vr.substr(0, 10) + '/' + vr.substr(10);
					if (tam > 12)
						vr = vr.substr(0, 15) + '-' + vr.substr(15);
		}
  	nmCampo.value = vr;
	}
}
function limpaCampo(valor) {
// Retira os simbolos de um campo, retornando apenas os seus digitos
	var numeros = "0123456789";
	var cont = 0;
	while (cont < valor.length)
	  {
	  	if (numeros.indexOf(valor.charAt(cont)) < 0)
				valor = valor.substr(0, cont) + valor.substr(cont + 1);
			else
				cont++;
	  }
	return valor;
}

function validaNumeros(campo, event){
  var BACKSPACE= 8;
  var key;
  var tecla;
	
  CheckTAB=9;
  if (navigator.appName.indexOf("Netscape")!= -1) 
	tecla= event.which;
  else
	tecla= event.keyCode;
	 
  key = String.fromCharCode( tecla);
	
  if (tecla == 13) return false;
	 
  if (tecla == BACKSPACE) return true;
  
  if (tecla == CheckTAB) return true;
  
   if (tecla == 0) return true;
	
  return (numerico(key));
} // fim da funcao validaTecla 

function numerico(caractere)
{
  var strValidos = "0123456789,";
  if (strValidos.indexOf(caractere) == -1) return false;
  return true;
}

function validaNumeros2(campo, event){
  var BACKSPACE= 8;
  var key;
  var tecla;
	
  CheckTAB=9;
  if (navigator.appName.indexOf("Netscape")!= -1) 
	tecla= event.which;
  else
	tecla= event.keyCode;
	 
  key = String.fromCharCode( tecla);
	
  if (tecla == 13) return false;
	 
  if (tecla == BACKSPACE) return true;
  
  if (tecla == CheckTAB) return true;
  
   if (tecla == 0) return true;
	
  return (numerico2(key));
} // fim da funcao validaTecla 

function numerico2(caractere)
{
  var strValidos = "0123456789";
  if (strValidos.indexOf(caractere) == -1) return false;
  return true;
}

//da receita federal
function validaCPF(obj, str){
var numero;
var digito = new Array(10); // array para os d�gitos do CPF.
var aux = 0; // �ndice para a string num.
var posicao
var i
var soma
var dv
var dvInformado;

if(obj != null)
{
str = obj.value;
}

//numero = _extraiNumero(str);

// Retira os d�gitos formatadores de CPF '.' e '-', caso existam.
if (str.length > 0){
while ((str.indexOf('.') != -1) || (str.indexOf('-') != -1))
{
if (str.indexOf('.') != -1)
{
aux = str.indexOf('.');
str = str.substr(0, aux) + str.substr(aux+1, str.length-1);
}
if (str.indexOf('-') != -1)
{
aux = str.indexOf('-');
str = str.substr(0, aux) + str.substr(aux+1, str.length-1);
}
} //while
} //if

//verifica CPFs manjados
switch (str) {
case '0':
case '00':
case '000':
case '0000':
case '00000':
case '000000':
case '0000000':
case '00000000':
case '000000000':
case '0000000000':
case '00000000000':
case '11111111111':
case '22222222222':
case '33333333333':
case '44444444444':
case '55555555555':
case '66666666666':
case '77777777777':
case '88888888888':
case '99999999999':
return false;
}

// In�cio da valida��o do CPF.
/* Retira do n�mero informado os dois �ltimos d�gitos */
dvInformado = str.substr(9,2);
/* Desmembra o n�mero do CPF no array digito */
for (i=0; i<=8; i++)
{
digito[i] = str.substr(i,1);
}
/* Calcula o valor do 10o. digito de verifica��o */
posicao = 10;
soma = 0;
for (i=0; i<=8; i++)
{
soma = soma + digito[i] * posicao;
posicao--;
}
digito[9] = soma % 11;
if (digito[9] < 2)
{
digito[9] = 0;
}
else
{
digito[9] = 11 - digito[9];
}
/* Calcula o valor do 11o. digito de verifica��o */
posicao = 11;
soma = 0;
for (i=0; i<=9; i++)
{
soma = soma + digito[i] * posicao;
posicao--;
}
digito[10] = soma % 11;
if (digito[10] < 2)
{
digito[10] = 0;
}
else
{
digito[10] = 11 - digito[10];
}
dv = digito[9] * 10 + digito[10];
/* Verifica se o DV calculado � igual ao informado */
if(dv != dvInformado)
{
return false;
}
else
{
return true;
}
}

function valida_cnpj(cnpj){
      var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
      digitos_iguais = 1;
//adicionado para tirar os digitos		
if (cnpj.length > 0){
while ((cnpj.indexOf('.') != -1) || (cnpj.indexOf('-') != -1) || (cnpj.indexOf('/') != -1))
{
if (cnpj.indexOf('.') != -1)
{
aux = cnpj.indexOf('.');
cnpj = cnpj.substr(0, aux) + cnpj.substr(aux+1, cnpj.length-1);
}
if (cnpj.indexOf('-') != -1)
{
aux = cnpj.indexOf('-');
cnpj = cnpj.substr(0, aux) + cnpj.substr(aux+1, cnpj.length-1);
}
if (cnpj.indexOf('/') != -1)
{
aux = cnpj.indexOf('/');
cnpj = cnpj.substr(0, aux) + cnpj.substr(aux+1, cnpj.length-1);
}
} //while
} //if
		
		
      if (cnpj.length < 14 && cnpj.length < 15)
            return false;
      for (i = 0; i < cnpj.length - 1; i++)
            if (cnpj.charAt(i) != cnpj.charAt(i + 1)){
                  digitos_iguais = 0;
                  break;
             }
      if (!digitos_iguais){
            tamanho = cnpj.length - 2
            numeros = cnpj.substring(0,tamanho);
            digitos = cnpj.substring(tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (i = tamanho; i >= 1; i--)
                  {
                  soma += numeros.charAt(tamanho - i) * pos--;
                  if (pos < 2)
                        pos = 9;
                  }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0))
                  return false;
            tamanho = tamanho + 1;
            numeros = cnpj.substring(0,tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (i = tamanho; i >= 1; i--)
                  {
                  soma += numeros.charAt(tamanho - i) * pos--;
                  if (pos < 2)
                        pos = 9;
                  }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1))
                  return false;
            return true;
            }
      else
            return false;
} 

function validaEmailBO(email) {
		if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.val()))) {
			return false;
		}
		return true;
}

function enterloga(oEvent,funcao){
  var oEvent = (oEvent)? oEvent : event;
  var oTarget =(oEvent.target)? oEvent.target : oEvent.srcElement;
  if(oEvent.keyCode==13){
    //oEvent.keyCode = 9;
	 eval(funcao+";");
  }else if(oTarget.type=="text" && oEvent.keyCode==13){
    //return false;
    //oEvent.keyCode = 9;
	 eval(funcao+";");
  }else if (oTarget.type=="radio" && oEvent.keyCode==13){
    //oEvent.keyCode = 9;
	 eval(funcao+";");
  }
}

function ajaxHtml(local,servidor,param){
	$.ajax({
							 type: "POST",
							 url: servidor,
							 dataType: "html",
							 data: param,
							 success: function(msg){
								 if(msg){									 
									 $("#carreg").css("display","none");
									 $('#'+local).html(msg);
									 fechaCarr();
									 									
								 }else{									 
									 alert("Sem resposta do servidor!");
									 fechaCarr();
								 }
							 },
			 error: function(){	
			 			 alert("Erro no servidor de envio, favor contactar o suporte!");			 				
						 fechaCarr();
						 $("#mask4").fadeOut("slow");
						 
			 },
							 beforeSend: function(){
									abreCarr();
									
								}
	});	
}

function ajaxHtmlParent(local,servidor,param){
	$.ajax({
							 type: "POST",
							 url: servidor,
							 dataType: "html",
							 data: param,
							 success: function(msg){
								 if(msg){									 
									 $('#'+local,window.parent.document).html(msg);				
									 									
								 }else{									 
									 alert("Sem resposta do servidor!");						 
								 }
							 },
			 error: function(){	
			 			 alert("Erro no servidor de envio, favor contactar o suporte!");
						 $("#mask4",window.parent.document).fadeOut("slow");
						 
			 },
							 beforeSend: function(){
									
									
								}
	});	
}

function ajaxXml(destino,servidor,param,form,frase,antes,erro){
	if(form){
		form_string = $("#"+form).serialize();
		eval("form = document."+form+";");
	}else{
		form_string = "";
	}
	$.ajax({
							 type: "POST",
							 url: servidor,
							 dataType: "xml",
							 data: param+"&"+form_string,
							 success: function(msg){
								  var retorno = msg.getElementsByTagName('retorno')[0];
								 if(retorno.getElementsByTagName('msg')[0].firstChild.nodeValue == '1'){	
									 if(frase == 1){	 
										alert(retorno.getElementsByTagName('alerta')[0].firstChild.nodeValue);				 				
										eval(destino);
										
									 }else{
										eval(destino);
									 }									 
									 if(form){
									 	mudaCampos(form,1);
									 }
			
								 }else{									 
									 alert(retorno.getElementsByTagName('alerta')[0].firstChild.nodeValue);
									 eval(erro);						 							 
									 if(form){
									 	mudaCampos(form,1);
									 }
									 
								 }
							 },
			 error: function(){	
			 			 alert("Erro no servidor de envio, favor contactar o suporte!");			 				
						 if(form){
						 	mudaCampos(form,1);
						 }
						 
			 },
			 beforeSend: function(){
				if(form){
					mudaCampos(form,0);
				}
				eval(antes);									
			}
	});	
}

function formataValorMonetario(campooriginal,decimais){
		  if(campooriginal.value){
		    var posicaoPontoDecimal;
		    var campo = '';
		    var resultado = '';
		    var pos,sep,dec;
		
		    //Retira possiveis separadores de milhar
		    for (pos=0; pos < campooriginal.value.length; pos ++)
		    {
			  if (campooriginal.value.charAt(pos)!='.')
				campo = campo + campooriginal.value.charAt(pos);
		    }     
		
		    //Formata valor monet�rio com decimais
		    posicaoPontoDecimal = campo.indexOf(',');
		    if (posicaoPontoDecimal != -1)
		    {
			  sep = 0;
			  for (pos=posicaoPontoDecimal-1;pos >= 0;pos--)
			  {
				sep ++;
				if (sep > 3)
				{
				   //resultado = '.' + resultado;
				   sep = 1;
				}
		
				resultado = campo.charAt(pos) + resultado;   
			  }
		
			  // Trata parte decimal
			  if (parseInt(decimais) > 0 )
			  {
				 resultado = resultado + ',';
			  
				 pos=posicaoPontoDecimal+1;
				 for (dec = 1;dec <= parseInt(decimais); dec++)
				 {
				   if (pos < campo.length)
				   {
					  resultado = resultado + campo.charAt(pos);
					  pos++;
				   }
				   else
					  resultado = resultado + '0';   
				 }
		
			  } // trata decimais
		   }
		   // Trata valor monet�rio sem decimais
		   else
		   {
			  sep = 0;
			  for (pos=campo.length-1;pos >= 0;pos--)
			  {
				sep ++;
				if (sep > 3)
				{
				  // resultado = '.' + resultado;
				   sep = 1;
				}
				resultado = campo.charAt(pos) + resultado;   
			  }
			  // Trata parte decimal
			  if (parseInt(decimais) > 0 )
			  {
				 resultado = resultado + ',';
				 for (dec = 1;dec <= parseInt(decimais); dec++)
				 {
					  resultado = resultado + '0';   
				 }
			  } // trata decimais
		   }
		   campooriginal.value = resultado;
		}
}

function Maiusculas(id){
	$("#"+id).val($("#"+id).val().toUpperCase());
	
}

function tamanhoPagina() {
			var xScroll, yScroll;
			if (window.innerHeight && window.scrollMaxY) {	
				xScroll = window.innerWidth + window.scrollMaxX;
				yScroll = window.innerHeight + window.scrollMaxY;
			} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
				xScroll = document.body.scrollWidth;
				yScroll = document.body.scrollHeight;
			} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
				xScroll = document.body.offsetWidth;
				yScroll = document.body.offsetHeight;
			}
			var windowWidth, windowHeight;
			if (self.innerHeight) {	// all except Explorer
				if(document.documentElement.clientWidth){
					windowWidth = document.documentElement.clientWidth; 
				} else {
					windowWidth = self.innerWidth;
				}
				windowHeight = self.innerHeight;
			} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
				windowWidth = document.documentElement.clientWidth;
				windowHeight = document.documentElement.clientHeight;
			} else if (document.body) { // other Explorers
				windowWidth = document.body.clientWidth;
				windowHeight = document.body.clientHeight;
			}	
			// for small pages with total height less then height of the viewport
			if(yScroll < windowHeight){
				pageHeight = windowHeight;
			} else { 
				pageHeight = yScroll;
			}
			// for small pages with total width less then width of the viewport
			if(xScroll < windowWidth){	
				pageWidth = xScroll;		
			} else {
				pageWidth = windowWidth;
			}
			arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight);
			return arrayPageSize;
};
		
function scrollPagina() {
			var xScroll, yScroll;
			if (self.pageYOffset) {
				yScroll = self.pageYOffset;
				xScroll = self.pageXOffset;
			} else if (document.documentElement && document.documentElement.scrollTop) {	 // Explorer 6 Strict
				yScroll = document.documentElement.scrollTop;
				xScroll = document.documentElement.scrollLeft;
			} else if (document.body) {// all other Explorers
				yScroll = document.body.scrollTop;
				xScroll = document.body.scrollLeft;	
			}
			arrayPageScroll = new Array(xScroll,yScroll);
			return arrayPageScroll;
};

function listarCidades(opcao,local){
	if(opcao>0){
	campo_select = document.getElementById("cidade");
	$.ajax({
					 type: "POST",
					 url: local+"index.php/ajax/consultarCidade",
					 dataType: "html",
					 data: "opcao="+opcao,
					 success: function(msg){
						 if(msg){
							document.getElementById("bairro").length = 0;								
							document.getElementById("bairro").options[0] = new Option("Escolha uma cidade","0");
							campo_select.options.length = 0;
							results = msg.split(",");	 
							for( i = 0; i < results.length; i++ ){ 
							  string = results[i].split( "|" );
							  campo_select.options[i] = new Option( string[0], string[1] );	
							}
							mudaCampos(document.frmBusca,0);
							carregarTipoDeImoveis(local);		 									 
				
						 }else{
							alert("Erro na aplica��o, favor tentar em alguns minutos!");
							document.getElementById("bairro").length = 0;								
							document.getElementById("bairro").options[0] = new Option("Escolha uma cidade","0");
							campo_select.options.length = 0;
							document.getElementById("cidade").options[0] = new Option("Escolha uma cidade","0");
							mudaCampos(document.frmBusca,1);			 												
										 
						 }
					 },
					 beforeSend: function(){												
						document.getElementById("cidade").length = 0;
						document.getElementById("cidade").options[0] = new Option("carregando...","0");
						document.getElementById("bairro").length = 0;
						document.getElementById("bairro").options[0] = new Option("carregando...","0");
						mudaCampos(document.frmBusca,0);													
										
					 }
	});
  }
}

function carregarTipoDeImoveis(local){
  opcao  = document.getElementById('opcoes').value;
  campo_select = document.getElementById("tipo");	
  $.ajax({
		 type: "POST",
		 url:  local+"index.php/ajax/consultarTipo",
		 dataType: "html",
		 data: "opcao="+opcao,
		 success: function(msg){
			 if(msg){																	
				campo_select.options.length = 0;
				results = msg.split(",");	 
				for( i = 0; i < results.length; i++ ){ 
				  string = results[i].split( "|" );
				  campo_select.options[i] = new Option( string[0], string[1] );
				}
				mudaCampos(document.frmBusca,1); 	 	 									 
	
			 }else{
				alert("Erro na aplica��o, favor tentar em alguns minutos!");																		
				campo_select.options.length = 0;
				document.getElementById("tipo").options[0] = new Option("Escolha uma op��o","0");
				mudaCampos(document.frmBusca,1);			 												
							 
			 }
		 },
		 beforeSend: function(){
			campo_select.options.length = 0;												
			document.getElementById("tipo").options[0] = new Option("Carregando...","0");							
			mudaCampos(document.frmBusca,0);													
							
		 }
	});			
	
}

function carregarBairros(local){
  opcao  = document.getElementById('opcoes').value;
  cidade = document.getElementById('cidade').value;
  if(opcao>0 && opcao<3 && cidade){
  	campo_select = document.getElementById("bairro");		
	$.ajax({
					type: "POST",
					url:  local+"index.php/ajax/consultarBairros",
					dataType: "html",
					 data: "opcao="+opcao+"&cidade="+cidade,
					 success: function(msg){
						 if(msg){																	
							campo_select.options.length = 0;
							results = msg.split(",");	 
							for( i = 0; i < results.length; i++ ){ 
							  string = results[i].split( "|" );
							  campo_select.options[i] = new Option( string[0], string[1] );
							  if(i==0){
							  	campo_select.options[i].selected = true;
							  }
							}
							mudaCampos(document.frmBusca,1); 	 	 									 
				
						 }else{
							alert("Erro na aplica��o, favor tentar em alguns minutos!");																		
							campo_select.options.length = 0;
							campo_select.options[0] = new Option("Escolha uma cidade","0");
							mudaCampos(document.frmBusca,1);			 												
										 
						 }
					 },
					 beforeSend: function(){
						campo_select.options.length = 0;												
						campo_select.options[0] = new Option("Carregando...","0");							
						mudaCampos(document.frmBusca,0);													
										
					 }
			});	
	}		
}

function buscaRapida(){
	 op = document.frmBusca.opcoes.value;
	 vali = document.frmBusca.valorini.value;
	 valf = document.frmBusca.valorfim.value;
	 tipo = document.frmBusca.tipo.value;
	 codigo = document.frmBusca.codigo.value;
	 cidade = document.frmBusca.cidade.value;
	 bairro = document.frmBusca.bairro.value;
	 if(codigo){
		 $("#frmBusca").submit();
	 }else {
		 if((op == 0)) {
			alert("� necess�rio escolher uma pretens�o!");
			document.frmBusca.opcoes.focus();
			return false;
		 }else if(tipo == 0){
			alert("� necess�rio escolher um tipo de imovel!");
			document.frmBusca.tipo.focus();
			return false;
		 }else if(cidade == 0){
			alert("� necess�rio escolher uma cidade!");
			document.frmBusca.cidade.focus();
			return false;
		 }else {
		 	$("#frmBusca").submit();
		 }
	 }
}

function validaLogin() {
  login = document.form.LOGIN.value;
  senha = document.form.SENHA.value;
  nSenha = fncReplace(senha,"'","");
  document.form.SENHA.value = nSenha;
  nLogin = fncReplace(login,"'","");
  document.form.LOGIN.value = nLogin;
  if(!nLogin) {
	  alert("� neces�rio preencher o campo Login");
	  return false;
  }else if(!nSenha) {
	  alert("� necess�rio preencher o campo Senha");
	  return false;
  }else {
	  document.form.submit();
  }
} 

function fncReplace(str,find,repla)
{
    var newStr = ""
    
    for(i=0;i<=str.length-1;i++)
    {
 strValue = str.substring(i,i+1)

 if (strValue == find)
     newStr += repla
 else
     newStr += str.substring(i,i+1)
    }
    return newStr
}

function validarVoto(local) {
   val = document.frmVoto.valor.value;
	if(!val) {
	   alert("Voc� deve escolher uma op��o antes de votar");
	}else {
		form = document.frmVoto;
		form_string = $("#frmVoto").serialize();		   
			$.ajax({
			 type: "POST",
			 url: local+"index.php/ajax/votoenquete",
			 dataType: "html",
			 data: form_string,
			 success: function(msg){
				 if(msg){		 
					 $("#local").html(msg);
														
				 }else{
					 alert("Erro na a��o, Favor tentar em alguns minutos!");						 
					 
				 }
			 },
			 beforeSend: function(){
					$("#local").html('<div style="width:100%; height:57px; clear:both;"></div><div style="float:left; margin-top:10px; margin-left:58px;"><img src="'+local+'application/images/load.gif"></div></div><div style="width:100%; height:30px; text-align:center; font-family:Arial, Helvetica, sans-serif; font-size:10px;color: #a52828; font-weight: bold;">Votando...</div>');
					
				}
		});	
	}
}

function voltar(origem){
	window.location = origem;
}

function menu(param, obj)
{

	if (param == 1)
	{
		document.getElementById('menu_' + obj).style.display = '';
	}
	if (param == 2)
	{
		document.getElementById('menu_' + obj).style.display = 'none';
	}

}

function sair(local){
	window.location = local+"index.php/internas/sair";
}