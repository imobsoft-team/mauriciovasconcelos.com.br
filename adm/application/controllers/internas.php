<?php
class Internas extends Controller {
	public $layout = 'default';
	public $title = 'DMV';
	public $css = array('global','jquery.lightbox-0.5');
	public $js = array('jquery-1.3.2.min','jquery.maskedinput-1.1.1','jquery.innerfade','slider.js','js','internas');
	public $data = array('local'=>"",'logomarca'=>'../images/logo.jpg','empresa'=>'dmv','cnpj'=>'08.684.865/0001-26','fones'=>'(85) 3456-3336','endereco'=>'Av. Santos Dumont, 3131 - sala 1015 Aldeota','sessao'=>'imoveisstyl','sessaoAdm'=>'stylAdm','url'=>'http://www.dmvimoveis.com.br/');
	public $configMail = array('protocol' => '0',
    'smtp_host' => '0',
    'smtp_port' => 0,
    'smtp_user' => '0',
    'smtp_pass' => '0',
    );
	public $emails = array('Contato | locacao@dmvimoveis.com.br');
	
	function Internas()
	{
		parent::Controller();
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
		if(!isset($_SESSION[''.$this->data['sessaoAdm']])){
			$_SESSION[''.$this->data['sessaoAdm']] = "";
		}
		$this->load->model('usuario');
		$this->load->helper('ckeditor');
		$this->data['editor'] = array( 
				//ID of the textarea that will be replaced
				'id' 	=> 	'content', 	// Must match the textarea's id
				'path'	=>	'application/js/ckeditor',	// Path to the ckeditor folder relative to index.php
				//Optionnal values
				'config' => array(
					'toolbar' 	=> 	"Full", 	//Using the Full toolbar
					'width' 	=> 	"550px",	//Setting a custom width
					'height' 	=> 	'100px',	//Setting a custom height
					'filebrowserBrowseUrl '=> base_url().'application/js/ckeditor/ckfinder/ckfinder.html',
					'filebrowserImageBrowseUrl' => base_url().'application/js/ckeditor/ckfinder/ckfinder.html?type=Images',
					'filebrowserFlashBrowseUrl' => base_url().'application/js/ckeditor/ckfinder/ckfinder.html?type=Flash',
					'filebrowserUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
					'filebrowserImageUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
'filebrowserFlashUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
	 
				),
	 
				//Replacing styles from the "Styles tool"
				'styles' => array(
	 
					//Creating a new style named "style 1"
					'style 1' => array (
						'name' 		=> 	'Blue Title',
						'element' 	=> 	'h2',
						'styles' => array(
							'color' 		=> 	'Blue',
							'font-weight' 		=> 	'bold'
						)
					),
	 
					//Creating a new style named "style 2"
					'style 2' => array (
						'name' 		=> 	'Red Title',
						'element' 	=> 	'h2',
						'styles' => array(
							'color' 		=> 	'Red',
							'font-weight' 		=> 	'bold',
							'text-decoration'	=> 	'underline'
						)
					)				
				)
			);
	}
	
	function index()
	{
		$this->load->helper('url');
		redirect(site_url(),'refresh');
	}
	
	function logar(){
		if($this->input->post('SENHA',TRUE)){
			$this->data['usuario'] = $this->usuario->logarAdm($this->input->post('LOGIN',TRUE),md5($this->input->post('SENHA',TRUE)));
			if($this->data['usuario']){
				$_SESSION[''.$this->data['sessaoAdm']] = $this->data['usuario'];
			}else {
				$_SESSION[''.$this->data['sessaoAdm']]="";
			}
		}
		redirect('index.php/internas/sistema','refresh');		
	}
	
	function sistema(){
		$this->data['usuario'] = $_SESSION[''.$this->data['sessaoAdm']];
		if($this->data['usuario']){
			$this->data['usuario'] = $this->usuario->seguranca($this->data['usuario']);
		}
		$this->data['meses'] = array('01' => 'JANEIRO','02' => 'FEVEREIRO','03' => 'MAR�O','04' => 'ABRIL','05' => 'MAIO','06' => 'JUNHO','07' => 'JULHO','08' => 'AGOSTO','09' => 'SETEMBRO','10' => 'OUTUBRO','11' => 'NOVEMBRO','12' => 'DEZEMBRO');
		$this->data['datAtual'] = date("d")." de ".$this->data['meses'][date('m')]." de ".date("Y"); 
		$this->load->view('principal/sistema',$this->data);
	}
	
	function sair()
	{
		unset($_SESSION[''.$this->data['sessaoAdm']]);
		redirect(site_url(),'refresh');
	}
	
	function noticias()
	{
               
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_not');
			if($this->input->post('del')){
				$cods = implode(',', $_POST["item"]);
  				$this->tab_not->excluirItens($cods);
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'NOT_ISN ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/noticias/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->tab_not->contaRegistros($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["noticias"] = $this->tab_not->getNoticias(10,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->load->view('cadastros/noticias',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	function videos(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('videos');
			if($this->input->post('del')){
				$cods = implode(',', $_POST["item"]);
  				$this->videos->excluirVideos($cods);
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'ID_VID ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/videos/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->videos->contaRegistrosVideos($busca);
			$config['per_page']   = 5;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["videos"] = $this->videos->getVideos(5,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			array_push($this->css,'shadowbox/shadowbox');
			array_push($this->js,'shadowbox','shadowAux');
			$this->load->view('cadastros/videos',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function formularioNot(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_not');
			$id = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			$this->data["noticia"]="";
			if($id>0){
				$this->data["noticia"] = $this->tab_not->buscarNoticia($id);	
			}	
			$this->data['editoring'] = array( 
				//ID of the textarea that will be replaced
				'id' 	=> 	'contenting', 	// Must match the textarea's id
				'path'	=>	'application/js/ckeditor',	// Path to the ckeditor folder relative to index.php
				//Optionnal values
				'config' => array(
					'toolbar' 	=> 	"Full", 	//Using the Full toolbar
					'width' 	=> 	"550px",	//Setting a custom width
					'height' 	=> 	'100px',	//Setting a custom height
					'filebrowserBrowseUrl '=> base_url().'application/js/ckeditor/ckfinder/ckfinder.html',
					'filebrowserImageBrowseUrl' => base_url().'application/js/ckeditor/ckfinder/ckfinder.html?type=Images',
					'filebrowserFlashBrowseUrl' => base_url().'application/js/ckeditor/ckfinder/ckfinder.html?type=Flash',
					'filebrowserUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
					'filebrowserImageUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
'filebrowserFlashUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
	 
				),
	 
				//Replacing styles from the "Styles tool"
				'styles' => array(
	 
					//Creating a new style named "style 1"
					'style 1' => array (
						'name' 		=> 	'Blue Title',
						'element' 	=> 	'h2',
						'styles' => array(
							'color' 		=> 	'Blue',
							'font-weight' 		=> 	'bold'
						)
					),
	 
					//Creating a new style named "style 2"
					'style 2' => array (
						'name' 		=> 	'Red Title',
						'element' 	=> 	'h2',
						'styles' => array(
							'color' 		=> 	'Red',
							'font-weight' 		=> 	'bold',
							'text-decoration'	=> 	'underline'
						)
					)				
				)
			);
			$this->data['editoresp'] = array( 
				//ID of the textarea that will be replaced
				'id' 	=> 	'contentesp', 	// Must match the textarea's id
				'path'	=>	'application/js/ckeditor',	// Path to the ckeditor folder relative to index.php
				//Optionnal values
				'config' => array(
					'toolbar' 	=> 	"Full", 	//Using the Full toolbar
					'width' 	=> 	"550px",	//Setting a custom width
					'height' 	=> 	'100px',	//Setting a custom height
					'filebrowserBrowseUrl '=> base_url().'application/js/ckeditor/ckfinder/ckfinder.html',
					'filebrowserImageBrowseUrl' => base_url().'application/js/ckeditor/ckfinder/ckfinder.html?type=Images',
					'filebrowserFlashBrowseUrl' => base_url().'application/js/ckeditor/ckfinder/ckfinder.html?type=Flash',
					'filebrowserUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
					'filebrowserImageUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
'filebrowserFlashUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
	 
				),
	 
				//Replacing styles from the "Styles tool"
				'styles' => array(
	 
					//Creating a new style named "style 1"
					'style 1' => array (
						'name' 		=> 	'Blue Title',
						'element' 	=> 	'h2',
						'styles' => array(
							'color' 		=> 	'Blue',
							'font-weight' 		=> 	'bold'
						)
					),
	 
					//Creating a new style named "style 2"
					'style 2' => array (
						'name' 		=> 	'Red Title',
						'element' 	=> 	'h2',
						'styles' => array(
							'color' 		=> 	'Red',
							'font-weight' 		=> 	'bold',
							'text-decoration'	=> 	'underline'
						)
					)				
				)
			);		
			$this->load->view('cadastros/inc_not',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
		function formularioVid(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('videos');
			$id = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			$this->data["videos"]="";
			if($id>0){
				$this->data["videos"] = $this->videos->buscarVideo($id);	
			}			
			$this->load->view('cadastros/inc_video',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	
	/************************************ Banner Box *******************************************/
	function formularioBann()
	{
		if($_SESSION[''.$this->data['sessaoAdm']])
		{
			$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
			if($this->data['usuario']){
				$this->layout = 'popup';
				$this->load->model('tab_banner');
				$this->data["bann"] = $this->tab_banner->getBann();
				$this->load->view('cadastros/inc_bannerr',$this->data);
			}
			else 
			{
				print("Acesso negado!");
				die;
			}
		}
		else 
		{
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function cadBann(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_banner');			
			$ret = $this->tab_banner->alterar($_FILES["bann"],'../');
			$msg = 'Cadastro realizado com sucesso!';	
			print('<script type="text/javascript">alert("'.$msg.$ret.'"); window.location = "'.site_url().'index.php/internas/formularioBann"; </script>');
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	/*************************************************************************************/
	

	function cadBan(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_banner');
			$this->input->post('exp')?$exp=explode('/',$this->input->post('exp',TRUE)):$exp=date('Y-m-d');
			if(is_array($exp)){ $exp = $exp[2].'-'.$exp[1].'-'.$exp[0]; }
			if($this->input->post('id')>0){
				$veri = $this->tab_banner->alterarBanner($this->input->post('id') ,htmlentities($this->input->post('nome',TRUE),TRUE), $this->input->post('ativa'),$_FILES["imagem"],$this->input->post('link'),'../',$this->input->post('finalidade'));
			}else {
                                
				$veri = $this->tab_banner->cadastrarBanner(htmlentities($this->input->post('nome',TRUE),TRUE), $this->input->post('ativa'),$_FILES["imagem"],$this->input->post('link'),'../',$this->input->post('finalidade'));
			}
			if($veri>0){
				$this->input->post('id')>0?$msg = 'Alteração realizada com sucesso!':$msg = 'Cadastro realizado com sucesso!';
			}else {
				$msg = 'Erro no cadastro da not&iacute;cia!';
			}			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/banner"; </script>');
			
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
        
        function cadNot(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_not');
			$this->input->post('exp')?$exp=explode('/',$this->input->post('exp',TRUE)):$exp=date('Y-m-d');
			if(is_array($exp)){ $exp = $exp[2].'-'.$exp[1].'-'.$exp[0]; }
			if($this->input->post('id')>0){
				$veri = $this->tab_not->alterarNoticia(htmlentities($this->input->post('titulo',TRUE),TRUE),htmlentities($this->input->post('tituloing',TRUE),TRUE),htmlentities($this->input->post('tituloesp',TRUE),TRUE),htmlentities($this->input->post('content',TRUE),TRUE),htmlentities($this->input->post('contenting',TRUE),TRUE),htmlentities($this->input->post('contentesp',TRUE),TRUE),htmlentities($this->input->post('fonte',TRUE),TRUE),$exp,$this->input->post('id'),$_FILES["imgBanner"],'../');
			}else {
			
				$veri = $this->tab_not->cadastrarNoticia(htmlentities($this->input->post('titulo',TRUE),TRUE),htmlentities($this->input->post('tituloing',TRUE),TRUE),htmlentities($this->input->post('tituloesp',TRUE),TRUE),htmlentities($this->input->post('content',TRUE),TRUE),htmlentities($this->input->post('contenting',TRUE),TRUE),htmlentities($this->input->post('contentesp',TRUE),TRUE),htmlentities($this->input->post('fonte',TRUE),TRUE),date('Y-m-d H:i:s'),$exp,$_FILES["imgBanner"],'../');
			}
			if($veri>0){
				$this->input->post('id')>0?$msg = 'Altera��o realizada com sucesso!':$msg = 'Cadastro realizado com sucesso!';
			}else {
				$msg = 'Erro no cadastro da not�cia!';
			}			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/noticias"; </script>');
			
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function cadVid(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('videos');
			
			if($this->input->post('id')>0){
				$veri = $this->videos->alterarVideo(htmlentities($this->input->post('titulo',TRUE),TRUE),htmlentities($this->input->post('link',TRUE),TRUE),$this->input->post('id'));
			}else {
				$veri = $this->videos->cadastrarVideo(htmlentities($this->input->post('titulo',TRUE),TRUE),htmlentities($this->input->post('link',TRUE),TRUE));
			}
			if($veri>0){
				$this->input->post('id')>0?$msg = 'Altera��o realizada com sucesso!':$msg = 'Cadastro realizado com sucesso!';
			}else {
				$msg = 'Erro no cadastro da not�cia!';
			}			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/videos"; </script>');
			
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function fixos(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_textos');
			if($this->input->post('del')){
				$cods = implode(',', $_POST["item"]);
  				$this->tab_textos->excluirItens($cods);
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'TEXTO_ISN ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/fixos/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->tab_textos->contaRegistros($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["paginas"] = $this->tab_textos->getFixas(10,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->load->view('cadastros/conteudo_fixo',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
        
        
	
	function formularioFix(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_textos');
			$id = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			$this->data["pagina"]="";
			if($id>0){
				$this->data["pagina"] = $this->tab_textos->buscarFixa($id);	
				$this->data['editorIng'] = array( 
				//ID of the textarea that will be replaced
				'id' 	=> 	'contenting', 	// Must match the textarea's id
				'path'	=>	'application/js/ckeditor',	// Path to the ckeditor folder relative to index.php
				//Optionnal values
				'config' => array(
					'toolbar' 	=> 	"Full", 	//Using the Full toolbar
					'width' 	=> 	"550px",	//Setting a custom width
					'height' 	=> 	'100px',	//Setting a custom height
					'filebrowserBrowseUrl '=> base_url().'application/js/ckeditor/ckfinder/ckfinder.html',
					'filebrowserImageBrowseUrl' => base_url().'application/js/ckeditor/ckfinder/ckfinder.html?type=Images',
					'filebrowserFlashBrowseUrl' => base_url().'application/js/ckeditor/ckfinder/ckfinder.html?type=Flash',
					'filebrowserUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
					'filebrowserImageUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
'filebrowserFlashUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
	 
				),
	 
				//Replacing styles from the "Styles tool"
				'styles' => array(
	 
					//Creating a new style named "style 1"
					'style 1' => array (
						'name' 		=> 	'Blue Title',
						'element' 	=> 	'h2',
						'styles' => array(
							'color' 		=> 	'Blue',
							'font-weight' 		=> 	'bold'
						)
					),
	 
					//Creating a new style named "style 2"
					'style 2' => array (
						'name' 		=> 	'Red Title',
						'element' 	=> 	'h2',
						'styles' => array(
							'color' 		=> 	'Red',
							'font-weight' 		=> 	'bold',
							'text-decoration'	=> 	'underline'
						)
					)				
				)
			);
			$this->data['editorEsp'] = array( 
				//ID of the textarea that will be replaced
				'id' 	=> 	'contentesp', 	// Must match the textarea's id
				'path'	=>	'application/js/ckeditor',	// Path to the ckeditor folder relative to index.php
				//Optionnal values
				'config' => array(
					'toolbar' 	=> 	"Full", 	//Using the Full toolbar
					'width' 	=> 	"550px",	//Setting a custom width
					'height' 	=> 	'100px',	//Setting a custom height
					'filebrowserBrowseUrl '=> base_url().'application/js/ckeditor/ckfinder/ckfinder.html',
					'filebrowserImageBrowseUrl' => base_url().'application/js/ckeditor/ckfinder/ckfinder.html?type=Images',
					'filebrowserFlashBrowseUrl' => base_url().'application/js/ckeditor/ckfinder/ckfinder.html?type=Flash',
					'filebrowserUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
					'filebrowserImageUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
'filebrowserFlashUploadUrl' => base_url().'application/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
	 
				),
	 
				//Replacing styles from the "Styles tool"
				'styles' => array(
	 
					//Creating a new style named "style 1"
					'style 1' => array (
						'name' 		=> 	'Blue Title',
						'element' 	=> 	'h2',
						'styles' => array(
							'color' 		=> 	'Blue',
							'font-weight' 		=> 	'bold'
						)
					),
	 
					//Creating a new style named "style 2"
					'style 2' => array (
						'name' 		=> 	'Red Title',
						'element' 	=> 	'h2',
						'styles' => array(
							'color' 		=> 	'Red',
							'font-weight' 		=> 	'bold',
							'text-decoration'	=> 	'underline'
						)
					)				
				)
			);
			}			
			$this->load->view('cadastros/inc_cont',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function cadFix(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_textos');			
			if($this->input->post('id')>0){
				$veri = $this->tab_textos->alterarFixa(htmlentities($this->input->post('titulo',TRUE),TRUE),htmlentities($this->input->post('tituloing',TRUE),TRUE),htmlentities($this->input->post('tituloesp',TRUE),TRUE),htmlentities($this->input->post('content',TRUE),TRUE),htmlentities($this->input->post('contenting',TRUE),TRUE),htmlentities($this->input->post('contentesp',TRUE),TRUE),$this->input->post('id'));
			}else {
				$veri = $this->tab_textos->cadastrarFixa(htmlentities($this->input->post('titulo',TRUE),TRUE),htmlentities($this->input->post('tituloing',TRUE),TRUE),htmlentities($this->input->post('tituloesp',TRUE),TRUE),htmlentities($this->input->post('content',TRUE),TRUE),htmlentities($this->input->post('contenting',TRUE),TRUE),htmlentities($this->input->post('contentesp',TRUE),TRUE));
			}
			if($veri>0){
				$this->input->post('id')>0?$msg = 'Altera��o realizada com sucesso!':$msg = 'Cadastro realizado com sucesso!';
			}else {
				$msg = 'Erro no cadastro!';
			}			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/fixos"; </script>');
			
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
        
        function banner($del = null){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_banner');
			if($del){
                            $this->tab_banner->excluirIten($del);
                            print('<script type="text/javascript">alert("item excluido com sucesso!");window.location = "'.site_url().'index.php/internas/banner"; </script>');    
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'BANNER_ISN ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/fixos/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->tab_banner->contaRegistros($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["paginas"] = $this->tab_banner->getBanners(100,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->load->view('cadastros/banners',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function cotacoes(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_cota');
			if($this->input->post('del')){
				$cods = implode(',', $_POST["item"]);
  				$this->tab_cota->excluirItens($cods);
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'COTA_ISN ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/cotacoes/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->tab_cota->contaRegistros($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["cotas"] = $this->tab_cota->getCotas(10,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->load->view('cadastros/cotacoes',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function formularioCot(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_cota');
			$id = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			$this->data["pagina"]="";
			$this->data["cota"]="";
			if($id>0){
				$this->data["cota"] = $this->tab_cota->buscarCota($id);	
			}			
			$this->load->view('cadastros/inc_cotacoes',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function cadCot(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_cota');			
			if($this->input->post('id')>0){
				$veri = $this->tab_cota->alterarCota($this->input->post('indice',TRUE),$this->input->post('mes',TRUE),$this->input->post('ano',TRUE),str_replace(",",".",$this->input->post('valor',TRUE)),$this->input->post('id'));
			}else {
				$veri = $this->tab_cota->cadastrarCota($this->input->post('indice',TRUE),$this->input->post('mes',TRUE),$this->input->post('ano',TRUE),str_replace(",",".",$this->input->post('valor',TRUE)));
			}
			if($veri>0){
				$this->input->post('id')>0?$msg = 'Altera��o realizada com sucesso!':$msg = 'Cadastro realizado com sucesso!';
			}else {
				$msg = 'Erro no cadastro!';
			}			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/cotacoes"; </script>');
			
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function enquete(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('enquetes');
			if($this->input->post('del')){
				$cods = implode(',', $_POST["item"]);
  				$this->enquetes->excluirItens($cods);
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'ENQ_PER_ISN ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/enquete/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->enquetes->contaRegistros($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["enquetes"] = $this->enquetes->getEnq(10,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->load->view('cadastros/enquetes',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function formularioEnq(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('enquetes');
			$id = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			$this->data["pagina"]="";
			$this->data["enquete"]="";
			if($id>0){
				$this->data["enquete"] = $this->enquetes->buscarDados($id);	
			}			
			$this->load->view('cadastros/inc_enq',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function cadEnq(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('enquetes');
			$this->input->post('dati')?$ini=explode("/",$this->input->post('dati')):$ini=date('Y-m-d');		
			is_array($ini)?$ini=$ini[2]."-".$ini[1]."-".$ini[0]:$ini=$ini;
			$this->input->post('datf')?$fim=explode("/",$this->input->post('datf')):$fim=date('Y-m-d');		
			is_array($fim)?$fim=$fim[2]."-".$fim[1]."-".$fim[0]:$fim=$fim;	
			if($this->input->post('id')>0){
				$veri = $this->enquetes->alterarEnquete(htmlentities($this->input->post('pergunta',TRUE),TRUE),$_POST['resp'],$ini,$fim,$this->input->post('parcial'),$this->input->post('tipo'),$this->input->post('id'));
			}else {
				$veri = $this->enquetes->cadastrarEnq(htmlentities($this->input->post('pergunta',TRUE),TRUE),$_POST['resp'],$ini,$fim,$this->input->post('parcial'),$this->input->post('tipo'));
			}
			if($veri>0){
				$this->input->post('id')>0?$msg = 'Altera��o realizada com sucesso!':$msg = 'Cadastro realizado com sucesso!';
			}else {
				$msg = 'Erro no cadastro!';
			}			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/enquete"; </script>');
			
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function precisamos(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_prec');
			if($this->input->post('del')){
				$cods = implode(',', $_POST["item"]);
  				$this->tab_prec->excluirItens($cods);
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'prec_isn ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/precisamos/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->tab_prec->contaRegistros($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"]   =  $this->pagination->create_links();
			$this->data["precisamos"]  = $this->tab_prec->getPrec(10,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->load->view('cadastros/precisamos',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function formularioPrec(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
			$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
			if($this->data['usuario']){
				$this->load->model('tab_prec');
				$id = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
				$this->data["pagina"]="";
				$this->data["prec"]="";
				if($id>0){
					$this->data["prec"] = $this->tab_prec->buscarDados($id);	
				}			
				$this->load->view('cadastros/inc_prec',$this->data);
			}else {
				print("Acesso negado!");
				die;
			}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function cadPrec(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_prec');	
			if($this->input->post('id')>0){
				$veri = $this->tab_prec->alterarPrec(htmlentities($this->input->post('corretor',TRUE),TRUE),$this->input->post('fonecor',TRUE),htmlentities($this->input->post('imobiliaria',TRUE),TRUE),$this->input->post('foneimob',TRUE),htmlentities($this->input->post('descricao',TRUE),TRUE),$this->input->post('id'));
			}else {
				$veri = $this->tab_prec->cadastrarPrec(htmlentities($this->input->post('corretor',TRUE),TRUE),$this->input->post('fonecor',TRUE),htmlentities($this->input->post('imobiliaria',TRUE),TRUE),$this->input->post('foneimob',TRUE),htmlentities($this->input->post('descricao',TRUE),TRUE));
			}
			if($veri>0){
				$this->input->post('id')>0?$msg = 'Altera��o realizada com sucesso!':$msg = 'Cadastro realizado com sucesso!';
			}else {
				$msg = 'Erro no cadastro!';
			}			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/precisamos"; </script>');
			
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function enqGraf($cod){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->layout = 'popup';
			array_push($this->js,'JSClass/FusionCharts');
			$this->load->model('enquetes');
			$this->data["enquete"] = $this->enquetes->buscarDados($cod);
			$this->data["param"]="";
			if($this->data["enquete"]){
				$this->data["param"]=$this->data["enquete"]['per_des']."|";
				$this->data["param"].=$this->data["enquete"]['resp'][0]['res_des']."|";
				$this->data["param"].=$this->data["enquete"]['resp'][0]['quant']."|";
				$this->data["param"].=$this->data["enquete"]['resp'][1]['res_des']."|";
				$this->data["param"].=$this->data["enquete"]['resp'][1]['quant']."|";
				$this->data["param"].=$this->data["enquete"]['resp'][2]['res_des']."|";
				$this->data["param"].=$this->data["enquete"]['resp'][2]['quant'];				
			}
			$this->load->view('cadastros/grafico',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function curriculos(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_cur');
			if($this->input->post('del')){
				$cods = implode(',', $_POST["item"]);
  				$this->tab_cur->excluirItens($cods);
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'CUR_ISN ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/curriculos/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->tab_cur->contaRegistros($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["curriculos"] = $this->tab_cur->getCur(10,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->load->view('cadastros/curriculos',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function verCur(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_cur');
			$id = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			$this->data["curriculo"]="";
			if($id>0){
				$this->data["curriculo"] = $this->tab_cur->buscarDados($id);	
			}			
			$this->load->view('cadastros/alt_curriculo',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function clientes(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'CLI_ISN ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/clientes/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->usuario->contaCli($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["clientes"] = $this->usuario->getCli(10,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->load->view('cadastros/clientes',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function propostas(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('imovel');
			if($this->input->post('del')){
				$cods = implode(',', $_POST["item"]);
  				$this->imovel->excluirItens($cods);
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'PPC_ISN ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/propostas/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->imovel->contaRegistros($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["propostas"] = $this->imovel->getProp(10,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->load->view('cadastros/list_prop',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function verProp($prop,$tipo){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->layout = 'popup';
			$this->load->model('imovel');
			$this->load->model('imovelloc');
			$this->data["tit"] = $this->imovel->buscarProp($prop);
			$this->data["imovel"]="";
			$this->data["dadosFia"]="";
			if($this->data["tit"]){
				$this->data["imovel"] = $this->imovelloc->buscarDadosImo($this->data["tit"]["imo_cod"]);
			}
			if($tipo==1){
				$this->data["dadosTit"] = $this->imovel->consultarDadosCpf($prop);
				$this->data["fia"] = $this->imovel->buscarFiadorProp($prop);
				if($this->data["fia"]){
					$this->data["dadosFia"] = $this->imovel->consultarDadosCpf($this->data["fia"]["id"]);
				}
				$pag='vis_proposta_pf';
			}else {
				$this->data["dadosTit"] = $this->imovel->consultarDadosCpj($prop);
				$this->data["fia"] = $this->imovel->buscarFiadorProp($prop);
				if($this->data["fia"]){
					$this->data["dadosFia"] = $this->imovel->consultarDadosCpf($this->data["fia"]["id"]);
				}
				$pag='vis_proposta_pj';
			}
			$this->load->view('cadastros/'.$pag,$this->data);
		}else {
			print("Acesso negado!");
			die;
		}	
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function imoveis(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('imovel');
			if($this->input->post('del')){
				$cods = implode(',', $_POST["item"]);
  				$this->imovel->excluirImos($cods);
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'CIM_ISN ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/imoveis/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->imovel->contaImos($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["imoveis"] = $this->imovel->getImo(10,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->load->view('cadastros/list_imo_loc',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}

	
	function verImo($cod){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->layout = 'popup';
			$this->load->model('imovel');
			$this->data["imo"]=$this->imovel->buscaImo($cod);
			if($this->data["imo"]){
				$this->data["imo"]["imo_finali"]==1?$this->data["fin"]="LOCA��O":$this->data["fin"]="VENDA";
			}
			$this->load->view('cadastros/resumo_imo',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}	
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function estatisticas(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('imovelloc');
			$this->data['tamCad'] = $this->imovelloc->getSize(1);
			$this->load->model('imovelvenda');
			$this->data['tamImob'] = $this->imovelloc->getSize(1);
			$this->data['tamBd'] = $this->imovelloc->getSize(2);
			$this->data['total'] = number_format($this->data['tamCad'] + $this->data['tamImob'] + $this->data['tamBd'],2,",",".");
			$this->data['totalDisp'] = number_format(150 - $this->data['total'],2,",",".");
			$this->load->view('cadastros/estatisticas',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function acessos(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			array_push($this->js,'JSClass/FusionCharts');
			$meses = (!$this->uri->segment("3")) ? 2 : $this->uri->segment("3");
			$this->data['tipo'] = (!$this->uri->segment("4")) ? 1 : $this->uri->segment("4");
			$this->data['w']='900';
			$this->data['h']='500';
			$this->data['linha'] = 'border-bottom:#999999 solid 1px';
			switch($this->data['tipo']){
				case 2:
					$this->data['swf'] = 'Pie3D.swf';
					break;
				case 3:
					$this->data['swf'] = 'Line.swf';
					break;
				default:
					$this->data['swf'] = 'Column3D.swf';
					$this->data['linha'] = '';
					break;
			}
			$this->load->model('tab_acs');
			$mes = array('1' => 'JANEIRO','2' => 'FEVEREIRO','3' => 'MAR�O','4' => 'ABRIL','5' => 'MAIO','6' => 'JUNHO','7' => 'JULHO','8' => 'AGOSTO','9' => 'SETEMBRO','10' => 'OUTUBRO','11' => 'NOVEMBRO','12' => 'DEZEMBRO');
			$m = array();
			$total=0;
			for($i=0;$i<$meses;$i++){
				$mat = date('m')-$i;
				$ano = date('Y');
				if($mat==0){
					$mat = 12;
					$ano-=1;
				}else if($mat<0){
					$mat = 12 + $mat;
					$ano-=1;
				}
				$m[$i]['mes'] = $mes[$mat];			
				$quant = $this->tab_acs->getAcsMes($mat,$ano);
				is_array($quant)?$quant=count($quant):$quant=0;
				$m[$i]['acs'] = $quant;
				$total+=$quant;		
			}
			$_SESSION['m'] = $m;
			$_SESSION['total'] = $total;
			$_SESSION['tipo'] = $this->data['tipo'];
			$_SESSION['param'] = $meses;
			$this->data['meses'] = $meses;		
			$this->load->view('cadastros/acessos',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function acessosdia(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			array_push($this->js,'JSClass/FusionCharts');
			$meses = (!$this->uri->segment("3")) ? date("m") : $this->uri->segment("3");
			$this->data['tipo'] = (!$this->uri->segment("4")) ? 1 : $this->uri->segment("4");
			$this->data['w']='900';
			$this->data['h']='500';
			$this->data['linha'] = 'border-bottom:#999999 solid 1px';
			switch($this->data['tipo']){
				case 2:
					$this->data['swf'] = 'Pie3D.swf';
					break;
				case 3:
					$this->data['swf'] = 'Line.swf';
					break;
				default:
					$this->data['swf'] = 'Column3D.swf';
					$this->data['linha'] = '';
					break;
			}
			$this->load->model('tab_acs');
			$mes = array('1' => 'JANEIRO','2' => 'FEVEREIRO','3' => 'MAR�O','4' => 'ABRIL','5' => 'MAIO','6' => 'JUNHO','7' => 'JULHO','8' => 'AGOSTO','9' => 'SETEMBRO','10' => 'OUTUBRO','11' => 'NOVEMBRO','12' => 'DEZEMBRO');
			$dia = array('1' => 'DOMINGO','2' => 'SEGUNDA','3' => 'TER�A','4' => 'QUARTA','5' => 'QUINTA','6' => 'SEXTA','7' => 'S�BADO');
			$mat = str_pad($meses,2,"0",STR_PAD_LEFT);
			$ano = date('Y');
			$total=0;
			for($i=1;$i<8;$i++){					
				$quant = $this->tab_acs->getAcsMesDiaSemana($mat,$ano,$i);
				is_array($quant)?$quant=count($quant):$quant=0;
				$m[$i]['acs'] = $quant;
				$m[$i]['dia'] = $dia[$i];
				$total+=$quant;		
			}
			$_SESSION['m'] = $m;
			$_SESSION['total'] = $total;
			$_SESSION['tipo'] = $this->data['tipo'];
			$_SESSION['param'] = $meses;
			$this->data['meses'] = $meses;
			$this->data['mes'] = $mes;	
			$this->load->view('cadastros/acessosdia',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function buscastipo(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			array_push($this->js,'JSClass/FusionCharts');
			$meses = (!$this->uri->segment("3")) ? date("m") : $this->uri->segment("3");
			$this->data['tipo'] = (!$this->uri->segment("4")) ? 1 : $this->uri->segment("4");
			$pretensao = (!$this->uri->segment("5")) ? 1 : $this->uri->segment("5");
			$this->data['w']='900';
			$this->data['h']='500';
			$this->data['linha'] = 'border-bottom:#999999 solid 1px';
			switch($this->data['tipo']){
				case 2:
					$this->data['swf'] = 'Pie3D.swf';
					break;
				case 3:
					$this->data['swf'] = 'Line.swf';
					break;
				default:
					$this->data['swf'] = 'Column3D.swf';
					$this->data['linha'] = '';
					break;
			}
			$this->load->model('tab_acs');
			$mes = array('1' => 'JANEIRO','2' => 'FEVEREIRO','3' => 'MAR�O','4' => 'ABRIL','5' => 'MAIO','6' => 'JUNHO','7' => 'JULHO','8' => 'AGOSTO','9' => 'SETEMBRO','10' => 'OUTUBRO','11' => 'NOVEMBRO','12' => 'DEZEMBRO');
			$m = array();
			$total=0;
			$mat = str_pad($meses,2,"0",STR_PAD_LEFT);
			$ano = date('Y');
			$pretensao=2;
			if($pretensao==1){
				$this->load->model('imovelLoc');
				$tipos = $this->imovelLoc->consultarTipo();
			}else {
				$this->load->model('imovelvenda');
				$tipos = $this->imovelvenda->consultarTipo();
				
			}
			if($tipos){
				$i = 0;
				foreach($tipos as $t){
					$quant = $this->tab_acs->getBscMesTipo($mat,$ano,$t['tim_des'],$pretensao);
					is_array($quant)?$quant=count($quant):$quant=0;
					$m[$i]['acs'] = $quant;
					$m[$i]['tipo'] = $t['tim_des'];
					$total+=$quant;
					$i++;
				}
				$tam = count($m);
				for($i=0;$i<($tam-1);$i++){
				   for($j=$i+1;$j<$tam;$j++){
					   if($m[$i]["acs"]<$m[$j]["acs"]){
							$aux=$m[$i];
							$m[$i]=$m[$j];
							$m[$j]=$aux;
					   }
				   
				   }
				
				}
			}
			
			$_SESSION['m'] = $m;
			$_SESSION['total'] = $total;
			$_SESSION['tipo'] = $this->data['tipo'];
			$_SESSION['param'] = $meses;
			$this->data['meses'] = $meses;
			$this->data['mes'] = $mes;
			$this->data['pret'] = $pretensao;	
			$this->load->view('cadastros/tipos',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function newsletter(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_modelo');
			if($this->input->post('del')){
				$cods = implode(',', $_POST["item"]);
  				$this->tab_modelo->excluirItens($cods,'../');
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'MOD_ISN ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/newsletter/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->tab_modelo->contaRegistros($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["modelos"] = $this->tab_modelo->getModelos(10,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->load->view('cadastros/newsletter',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function formularioNews(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_modelo');
			$id = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			$this->data["pagina"]="";
			$this->data["modelo"]="";
			if($id>0){
				$this->data["modelo"] = $this->tab_modelo->buscarNews($id);	
			}			
			$this->load->view('cadastros/inc_newsletter',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function cadNews(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_modelo');			
			if($this->input->post('id')>0){
				$veri = $this->tab_modelo->alterarNews(htmlentities($this->input->post('nome',TRUE),TRUE),htmlentities($this->input->post('content',TRUE),TRUE),$this->input->post('sta',TRUE),$_FILES["foto"],$this->input->post('id'),htmlentities($this->input->post('anterior',TRUE),TRUE),'../');
			}else {
				$veri = $this->tab_modelo->cadastrarNews(htmlentities($this->input->post('nome',TRUE),TRUE),htmlentities($this->input->post('content',TRUE),TRUE),$this->input->post('sta',TRUE),$_FILES["foto"],'../');
			}
			if($veri>0){
				switch($veri){
					case 1:
						$msg = 'Tipo de arquivo inv�lido, favor enviar somente arquivos no formato JPG!';
						break;
					case 2:
						$msg = 'Tamanho do arquivo n�o permitido somente arquivos de at� 5MB podem ser enviados!';
						break;
					case 3:
						$msg = 'Largura da foto inv�lida, favor envia uma com dimens�es menores!';
						break;
					case 4:
						$msg = 'Altura da foto inv�lida, favor envia uma com dimens�es menores!';
						break;
					case 5:
						$msg = 'Erro no upload da imagem, servidor n�o permitiu o envio!';
						break;
					case 6:
						$msg = 'A��o realizada com sucesso!';
						break;
					default:
						$this->input->post('id')>0?$msg = 'Altera��o realizada com sucesso!':$msg = 'Cadastro realizado com sucesso!';
						break;
				}
			}else {
				$msg = 'Erro no cadastro!';
			}			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/newsletter"; </script>'); 
			
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function emails(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_email');
			if($this->input->post('del')){
				$cods = implode(',', $_POST["item"]);
  				$this->tab_email->excluirItens($cods);
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'N.NWS_ISN ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/emails/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->tab_email->contaRegistros($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["emails"] = $this->tab_email->getEmails(10,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->load->view('cadastros/emails',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function formularioEmail(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_email');
			$id = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			$this->data["pagina"]="";
			$this->data["modelo"]="";
			$this->data["email"]="";
			if($id>0){
				$this->data["email"] = $this->tab_email->buscarEmail($id);	
			}	
			$this->data["grupos"] = $this->tab_email->getGrupos();	
			$this->load->view('cadastros/inc_email',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function formularioEmails(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_email');
			$this->data["grupos"] = $this->tab_email->getGrupos();	
			$this->load->view('cadastros/inc_emails',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function cadEmail(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_email');			
			if($this->input->post('id')>0){
				$veri = $this->tab_email->alterarEmail(htmlentities($this->input->post('email',TRUE),TRUE),htmlentities($this->input->post('nome',TRUE),TRUE),$this->input->post('sta',TRUE),$this->input->post('grupo',TRUE),$this->input->post('id'));
			}else {
				$veri = $this->tab_email->cadastrarEmail(htmlentities($this->input->post('email',TRUE),TRUE),htmlentities($this->input->post('nome',TRUE),TRUE),$this->input->post('sta',TRUE),$this->input->post('grupo',TRUE));
			}
			if($veri){
				$this->input->post('id')>0?$msg = 'Altera��o realizada com sucesso!':$msg = 'Cadastro realizado com sucesso!';
			}else {
				$msg = 'Erro no cadastro!';
			}			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/emails"; </script>'); 
			
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function cadEmails(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
			if($this->data['usuario']){
				$this->load->model('tab_email');
				if($this->input->post('emails',TRUE)){	
					$emails = explode(";",$this->input->post('emails'));
					if(is_array($emails)){
						foreach($emails as $e){
							if($e){
								$nome = explode("@",$e);
								$this->tab_email->cadastrarEmail(htmlentities($e,TRUE),$nome[0],1,$this->input->post('grupo',TRUE));
							}
						}
					}		
				}		
				$msg = 'Cadastro realizado com sucesso!';
				print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/emails"; </script>'); 
				
			}else {
				print("Acesso negado!");
				die;
			}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function grupos(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_grup');
			if($this->input->post('del')){
				$cods = implode(',', $_POST["item"]);
  				$this->tab_grup->excluirItens($cods);
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'GRUP_ISN ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/grupos/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->tab_grup->contaRegistros($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["grupos"] = $this->tab_grup->getGrupos(10,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->load->view('cadastros/grupos',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function formularioGrupo(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_grup');
			$id = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			$this->data["grupo"]="";
			if($id>0){
				$this->data["grupo"] = $this->tab_grup->buscarGrupo($id);	
			}
			$this->load->view('cadastros/inc_grupo',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function cadGrupo(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_grup');			
			if($this->input->post('id')>0){
				$veri = $this->tab_grup->alterarGrupo(htmlentities($this->input->post('nome',TRUE),TRUE),$this->input->post('id'));
			}else {
				$veri = $this->tab_grup->cadastrarGrupo(htmlentities($this->input->post('nome',TRUE),TRUE));
			}
			if($veri){
				$this->input->post('id')>0?$msg = 'Altera��o realizada com sucesso!':$msg = 'Cadastro realizado com sucesso!';
			}else {
				$msg = 'Erro no cadastro!';
			}			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/grupos"; </script>'); 
			
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function news(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_email');
			$this->load->model('tab_modelo');
			$this->data["grupos"] = $this->tab_email->getGrupos();
			$this->data["modelos"] = $this->tab_modelo->getModelosAll('MOD_NOM');
			$this->load->view('cadastros/envia_email',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function enviaNews(){
		$this->load->model('tab_email');
		$this->load->model('tab_modelo');
		$mod = $this->input->post("modelo",TRUE);
		$grp = $this->input->post("grupo",TRUE);
		$ass = htmlentities($this->input->post("assunto",TRUE),TRUE);
		$pos = $this->input->post("pos",TRUE);
		//para por 10 minutos para a regra anti spam
		set_time_limit(300); //seta o tempo de resposta do php
		if(($pos%98)==0 && $pos>96){
			sleep(600);
		}
		$parar=0;
		if((($pos+1)%98)==0 && $pos>96){
			$parar=1;
		}
		$dados = $this->tab_email->getEmailsGrup($grp);
		is_array($dados)?$quant = count($dados):$quant=0;
		if($quant>0){
			$msgEmail = $this->tab_modelo->buscarNews($mod);
			if($msgEmail){
				if($msgEmail['foto']){
					$msgEmail = "<center><img src='".$this->data['url']."adm/imagens/".html_entity_decode($msgEmail['foto'])."' name='' style='vertical-align:middle' /> <center><br /><br />".html_entity_decode($msgEmail['des']);
				}else {
					$msgEmail = html_entity_decode($msgEmail['des']);
				}
			}
			$msgEmail.="<br /><br /><center><label style='font-family:tahoma;font-size:11px;color:#FF0000;font-weight:bold'>Se voc� n�o tem interesse em receber nossas mensagens, favor, clique <a href='".$this->data['url']."index.php/cliente/descadastrarnews/".base64_encode($dados[$pos]['id'])."' target='_blank'>aqui</a> e descadastre seu e-mail.</label></center>";
			$this->load->plugin('phpmailer');
			$mail=new PHPMailer();
			if($this->configMail['protocol']=="smtp"){
				$mail->IsSMTP();
				$mail->SMTPAuth   = true;
				$mail->SMTPSecure = "";
				$mail->Host       = $this->configMail['smtp_host'];
				$mail->Port       = $this->configMail['smtp_port'];
				$mail->Username   = $this->configMail['smtp_user'];  
				$mail->Password   = $this->configMail['smtp_pass'];            
			}
			$destino = explode('|',$this->emails[0]);
			$mail->From       = $destino[1];
			$mail->FromName   = $this->data['empresa'];			
			$mail->Subject    = utf8_encode($ass);
			$mail->Body       = utf8_encode($msgEmail);
			//$mail->AltBody    = "This is the body when user views in plain text format"; 
			$mail->WordWrap   = 50;	 
			$mail->IsHTML(true);
			if(!empty($dados[$pos])){
				$email=$dados[$pos]['email'];
				$mail->AddAddress($email);
				if($mail->send()){
					$alerta = "Ok";
				}else {
					$alerta = "Erro no envio!";
				}
			}else {
				$alerta="Sem e-mail para essa posi��o";
				$email="nenhum!";
			}			
		
		}else {
			$alerta="Sem e-mails para essa op��o de grupo";
			$email="nenhum!";
		}
		header("Content-Type:application/xml; charset=UTF-8");
		print('<root><retorno><quant>'.$quant.'</quant><alerta>'.utf8_encode($alerta).'</alerta><email>'.$email.'</email><parar>'.$parar.'</parar></retorno></root>');
		die;
	}
	
	function cadimo(){
		set_time_limit(120);
		$this->load->model('imovelloc');
		$this->data['valores'] = $this->imovelloc->Manutencao();	
		$this->data['produto'] = 'LOCA��O';	
		$this->load->view('cadastros/manutencao_cadimo',$this->data);
	}
	
	function imobsale(){
		set_time_limit(120);
		$this->load->model('imovelvenda');
		$this->data['valores'] = $this->imovelvenda->Manutencao();	
		$this->data['produto'] = 'VENDA';		
		$this->load->view('cadastros/manutencao_cadimo',$this->data);
	}
	
	function boletos(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('imovelloc');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			}
			$ordem = (!$this->uri->segment("6")) ? 'REC_ISN ASC' : $this->uri->segment("6");
			//controle para n�o ficar indo direto no mdb
			if(!isset($_SESSION['ordemBoletos'])){
				session_register('ordemBoletos');
			}
			if(!isset($_SESSION['boletosSite'])){
				session_register('boletosSite');
			}
			if($busca || ($ordem != $_SESSION['ordemBoletos']) || empty($_SESSION['boletosSite']) || $this->uri->segment("3")=='init'){
				$this->data["boletos"]   = $this->imovelloc->getBoletos($busca,$ordem);
				$_SESSION['boletosSite'] = $this->data["boletos"];
				$_SESSION['ordemBoletos'] = $ordem;
			}else {
				$this->data["boletos"] = $_SESSION['boletosSite'];
			}
			$this->data['busca'] = $busca;
			$this->data['ordem'] = $ordem;
			//pagina��o
			if($this->data["boletos"]){
				$this->data['atual'] = (!$this->uri->segment("3")) ? 1 : $this->uri->segment("3");
				if($this->uri->segment("3")=='init'){
					$this->data['atual']=1;
				}
				$this->data['quant'] = 50;
				is_array($this->data["boletos"])?$this->data["total"]=count($this->data["boletos"]):$this->data["total"]=0;
				$this->data['pags'] = ceil($this->data["total"]/$this->data['quant']);
				$this->data['aux']=($this->data['atual']-1)*$this->data['quant'];
				$this->data['pagst'] = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
				if($this->data['atual']>=$this->data['pagst']+10){
					$this->data['pagst']+=10;
				}else if(($this->data['atual']==$this->data['pagst'] || $this->data['atual']==$this->data['pagst']-1) && $this->data['pagst']!=0){
					$this->data['pagst']-=10;
				}		
				if($this->data['pagst']>=10){
					$this->data['x']=0;
				}else {
					$this->data['x']=1;
				}
				($this->data['pagst']-1)>0?$this->data['limite'] = $this->data['pags']-$this->data['pagst']:$this->data["limite"] = $this->data['pags'];
				$this->data["limite"]<10?$this->data["limite"]=$this->data["limite"]:$this->data["limite"]=10;
				//calculo do �ltimo registro
				$this->data['ultimoreg'] = (ceil($this->data['pags']/10)-1)*10;
				
			}
			//fim da pagina��o
			$this->load->view('cadastros/boletos',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function envioBoleto(){
		//texto do email
		$msgEmail='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /><title>2 via boleto</title></head><body>Prezado Cliente, <br />Segue abaixo link para acesso e impress�o da 2� via do boleto banc�rio solicitado por V.Sa.<br />
<a href="'.$this->data['url'].'index.php/cliente/segundaviaboleto/'.$this->input->post("codBol",TRUE).'">Boleto Banc�rio 2&deg; via</a></body></html>';
		//fim do texto do email
		$this->load->plugin('phpmailer');
		$mail=new PHPMailer();
		if($this->configMail['protocol']=="smtp"){
			$mail->IsSMTP();
			$mail->SMTPAuth   = true;
			$mail->SMTPSecure = "";
			$mail->Host       = $this->configMail['smtp_host'];
			$mail->Port       = $this->configMail['smtp_port'];
			$mail->Username   = $this->configMail['smtp_user'];  
			$mail->Password   = $this->configMail['smtp_pass'];            
		}
		$destino = explode('|',$this->emails[0]);
		$mail->From       = $destino[1];
		$mail->FromName   = $this->data['empresa'];			
		$mail->Subject    = 'Boleto segunda via';
		$mail->Body       = utf8_encode($msgEmail);
		//$mail->AltBody    = "This is the body when user views in plain text format"; 
		$mail->WordWrap   = 50;	 
		$mail->IsHTML(true);
		$mail->AddAddress($this->input->post("maildest",TRUE));
		if($mail->send()){
			$msg=1;
			$alerta = "Boleto enviado com sucesso!";
		}else {
			$msg=0;
			$alerta = "Erro no envio!";
		}		
		header("Content-Type:application/xml; charset=UTF-8");
		print('<root><retorno><msg>$msg</msg><alerta>'.utf8_encode($alerta).'</alerta></retorno></root>');
		die;
	}
	
	function segundaviaboleto($rec){
		if($rec){
			$this->title = "Recibo de cobran�a";
			$this->layout = "popup";
			$this->css = array('popup');
			$this->load->helper('url');	   
			$this->data['local'] = site_url();
			$this->data['logomarca'] = $this->data['local'].$this->data['logomarca'];
			$this->data['data'] = date("d-m-Y H:i:s");
			$this->data['dadosRec']="";
			$this->data['itensRec']="";
			$this->data['dadosBlt']="";
			$this->data['cod_bco']="";
			$this->data['dadosIbl']="";
			$this->data['logo_bco']="";
			$this->data['contador']="";
			$this->data['lin_dig']= "";
			$this->data['aviso']= "";			
			$this->load->model('recibo');
			$this->data['dadosRec'] = $this->recibo->carregarDados2($rec);
			$this->data['itensRec'] = $this->recibo->carregarItens($rec);
			$this->data['dadosBlt'] = $this->recibo->carregarDadosBlt($rec);
			if($this->data['dadosBlt']){
				  $this->data['cod_bco']  = substr($this->data['dadosBlt']["blt_cod_bco"],0,3);
				  $this->data['dadosIbl'] = $this->recibo->carregarDadosIbl($this->data['dadosBlt']["blt_isn"]);
				  switch($this->data['cod_bco']){
					case 409:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogUnibanco.jpg";
						break;
					case 001:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogBB.jpg";
						break;
					case 104:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogCaixa3.jpg";
						break;
					case 004:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogoBNB.jpg";
						break;
					case 237:
						$this->data['logo_bco'] = $this->data['local']."application/images/logo_bradesco.gif";
						break;
					case 356:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogBancoReal.jpg";
						break;
					case 422:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogoSafra.jpg";
					case 341:
						$this->data['logo_bco'] = $this->data['local']."application/images/LogItau.jpg";
						break;
				  }
			}
			$this->data['contador'] = 0;
			$this->data['width'] = '100%';
			
		}
		$this->load->view('cadastros/exibirRecibo',$this->data);
	}
	
	function contatos(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_cnt');
			if($this->input->post('del')){
				$cods = implode(',', $_POST["item"]);
  				$this->tab_cnt->excluirItens($cods);
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'CNT_ISN ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/contatos/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->tab_cnt->contaRegistros($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = 'Ultimo';
			$config['next_link']  = 'Proximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["contatos"] = $this->tab_cnt->getContatos(10,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->data['deptos1'] = $this->emails;
			$this->load->view('cadastros/contatos',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sessao foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
        
        public function formularioBanner($id = null){
            if($_SESSION[''.$this->data['sessaoAdm']]){
                $this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
                if($this->data['usuario'])
                {
                    $this->load->model('tab_banner');
                    $id = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
                    $this->data["pagina"]="";
                    $this->data["banner"]="";
                    if($id>0)
                    {
                        $this->data["banner"] = $this->tab_banner->buscarBanner($id);	
                    }			
                        $this->load->view('cadastros/inc_banner',$this->data);
                }
                else 
                {
                    print("Acesso negado!");
                    die;
                }
            }
            else 
            {
                print("Sua sess�o foi expirado, favor logar novamente no sistema.");
                die;
            }
	}
        
	
	function formularioCont($id){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->layout = 'popup';
			$this->load->model('tab_cnt');
			$this->data["contato"]="";
			if($id>0){
				$this->data["contato"] = $this->tab_cnt->buscarContato($id);	
			}		
			if($this->data["contato"]){
				$dept = $this->emails[0];
				$this->data['dept'] = "";
				$this->data['de'] = "";
				if($dept){
					$dept = explode("|",$dept);
					$de   = $dept[1];
					$dept = $dept[0];
					$this->data['dept'] = $dept;
					$this->data['de'] = $de;
				}
				$this->data["contato"]['sta']==1?$this->data['sta']='RESPONDIDO':$this->data['sta']='PENDENTE';
			}	
			$this->load->view('cadastros/inc_contatos',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function cadCont(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_cnt');
			$this->load->plugin('phpmailer');
			$mail=new PHPMailer();
			if($this->configMail['protocol']=="smtp"){
				$mail->IsSMTP();
				$mail->SMTPAuth   = true;
				$mail->SMTPSecure = "";
				$mail->Host       = $this->configMail['smtp_host'];
				$mail->Port       = $this->configMail['smtp_port'];
				$mail->Username   = $this->configMail['smtp_user'];  
				$mail->Password   = $this->configMail['smtp_pass'];            
			}
			$mail->From       = $this->input->post("from",TRUE);
			$mail->FromName   = $this->data['empresa'];			
			$mail->Subject    = utf8_encode($this->input->post("assunto",TRUE));
			$mail->Body       = utf8_encode($this->input->post("msg",TRUE));
			//$mail->AltBody    = "This is the body when user views in plain text format"; 
			$mail->WordWrap   = 50;	 
			$mail->IsHTML(true);
			$mail->AddAddress($this->input->post("to",TRUE));
			if($mail->send()){
				$alerta = "Resposta enviada com sucesso!";
				$this->tab_cnt->setSta($this->input->post("id",TRUE),1);
			}else {
				$alerta = "Erro no envio!";
			}	
			print('<script type="text/javascript">alert("'.$alerta.'"); window.close(); </script>');
			
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	public function corretores()
	{
		$this->load->model('tab_corretores');
		
		if($this->input->post('del')){
			$cods = implode(',', $_POST["item"]);
  			$this->tab_corretores->excluirItens($cods);
		}
		
		$this->load->library('pagination');
		$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
		$ordem = (!$this->uri->segment("4")) ? 'CORR_ISN ASC' : $this->uri->segment("4");
		$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
		$config['base_url']   = $this->data['local'].'index.php/internas/corretores/'.$busca.'/'.$ordem.'/';
		if(empty($busca)) { $busca=""; }
		$config['total_rows'] = $this->tab_corretores->contaRegistros($busca);
		$config['per_page']   = 10;
		$config['first_link'] = 'Primeiro';
		$config['last_link']  = '�ltimo';
		$config['next_link']  = 'Pr�ximo';
		$config['prev_link']  = 'Anterior';
		$config['uri_segment'] = 5;
		$this->pagination->initialize($config); 
		$this->data["paginacao"] =  $this->pagination->create_links();
		$this->data["corretores"] = $this->tab_corretores->getCorretores(10,$inicio,$busca,$ordem);
		//echo "<pre>"; print_r( $this->data["corretores"] );echo "</pre>";die;
		$this->data['inicio'] = $inicio;
		$busca?$busca=$busca:$busca=0;
		$this->data['busca'] = $busca;
		//$this->data['deptos'] = $this->emails;
		
		$this->data["usuarios"] = array();
		$this->load->view('cadastros/corretores',$this->data);
	}
	
	
	
	public function arquivos()
	{
		$this->load->model('tab_arquivos');
		
		if($this->input->post('del')){
			$cods = implode(',', $_POST["item"]);
  			$this->tab_arquivos->excluirItens($cods);
		}
		
		$this->load->library('pagination');
		$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
		
		$ordem = (!$this->uri->segment("4")) ? 'ARQ_ISN ASC' : $this->uri->segment("4");
		$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
		$config['base_url']   = $this->data['local'].'index.php/internas/arquivos/'.$busca.'/'.$ordem.'/';
		if(empty($busca)) { $busca=""; }
		$config['total_rows'] = $this->tab_arquivos->contaRegistros($busca);
		$config['per_page']   = 10;
		$config['first_link'] = 'Primeiro';
		$config['last_link']  = '�ltimo';
		$config['next_link']  = 'Pr�ximo';
		$config['prev_link']  = 'Anterior';
		$config['uri_segment'] = 5;
		$this->pagination->initialize($config); 
		$this->data["paginacao"] =  $this->pagination->create_links();
		$this->data["arquivos"] = $this->tab_arquivos->getArquivos(10,$inicio,$busca,$ordem);
		$this->data['inicio'] = $inicio;
		$busca?$busca=$busca:$busca=0;
		$this->data['busca'] = $busca;
		
		$this->load->view('cadastros/arquivos',$this->data);
	}
	
	public function verArquivos()
	{
		$this->load->model('tab_arquivos');
		
		if($this->input->post('del')){
			$cods = implode(',', $_POST["item"]);
  			$this->tab_arquivos->excluirItens($cods);
		}
		
		$this->load->library('pagination');
		$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");	
			}
		
		$ordem = (!$this->uri->segment("4")) ? 'ARQ_ISN ASC' : $this->uri->segment("4");
		$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
		$config['base_url']   = $this->data['local'].'index.php/internas/verArquivos/'.$busca.'/'.$ordem.'/';
		if(empty($busca)) { $busca=""; }
		$config['total_rows'] = $this->tab_arquivos->contaRegistros($busca);
		$config['per_page']   = 10;
		$config['first_link'] = 'Primeiro';
		$config['last_link']  = '�ltimo';
		$config['next_link']  = 'Pr�ximo';
		$config['prev_link']  = 'Anterior';
		$config['uri_segment'] = 5;
		$this->pagination->initialize($config); 
		$this->data["paginacao"] =  $this->pagination->create_links();
		$this->data["arquivos"] = $this->tab_arquivos->getArquivos(10,$inicio,$busca,$ordem);
		$this->data['inicio'] = $inicio;
		$busca?$busca=$busca:$busca=0;
		$this->data['busca'] = $busca;
		
		$this->load->view('cadastros/verarquivos',$this->data);
	}
	
	public function downArquivo()
	{
		
		$this->load->model('tab_arquivos');
		$arquivo = $this->tab_arquivos->buscarArquivo($this->uri->segment("3"));
		
		switch(strtolower(substr(strrchr(basename($arquivo['caminho']),"."),1))){ // verifica a extens�o do arquivo para pegar o tipo
			case "pdf": $tipo="application/pdf"; break;
			case "zip": $tipo="application/zip"; break;
			case "doc": $tipo="application/msword"; break;
			case "docx": $tipo="application/msword"; break;
			case "xls": $tipo="application/vnd.ms-excel"; break;
			case "ppt": $tipo="application/vnd.ms-powerpoint"; break;
			case "gif": $tipo="image/gif"; break;
			case "png": $tipo="image/png"; break;
			case "jpg": $tipo="image/jpg"; break;
			case "txt": $tipo="application/save"; break;
			
		}
		
		//echo dirname(__FILE__);die;
		
		header("Content-Type: ".$tipo); // informa o tipo do arquivo ao navegador
		header("Content-Length: ".filesize("../".$arquivo['caminho'])); // informa o tamanho do arquivo ao navegador
		header("Content-Disposition: attachment; filename=".basename("../".$arquivo['caminho'])); // informa ao navegador que � tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
		readfile("../".$arquivo['caminho']); // l� o arquivo
		exit; // aborta p�s-a��es
	}
	
	
	function formularioArq(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
                $this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
                if($this->data['usuario'])
                {
                    $this->load->model('tab_arquivos');
                    
					$id = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
                    $this->data["pagina"]="";
                    $this->data["arquivos"]="";
                    if($id>0)
                    {
                        $this->data["arquivos"] = $this->tab_arquivos->buscarArquivo($id);
                    }			
                        $this->load->view('cadastros/inc_arquivos',$this->data);
                }
                else 
                {
                    print("Acesso negado!");
                    die;
                }
            }
            else 
            {
                print("Sua sess�o foi expirado, favor logar novamente no sistema.");
                die;
            }
	}
	
	function cadArq(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_arquivos');
			//$this->input->post('exp')?$exp=explode('/',$this->input->post('exp',TRUE)):$exp=date('Y-m-d');
			//if(is_array($exp)){ $exp = $exp[2].'-'.$exp[1].'-'.$exp[0]; }
			if($this->input->post('id')>0){
				$veri = $this->tab_arquivos->alterarArquivo($this->input->post('id') ,htmlentities($this->input->post('nome',TRUE),TRUE), $this->input->post('tipo'),$_FILES["arquivo"],'../');
			}else {                
				$veri = $this->tab_arquivos->cadastrarArquivo(htmlentities($this->input->post('nome',TRUE),TRUE), $this->input->post('tipo'),$_FILES["arquivo"],'../');
			}
			if($veri>0){
				$this->input->post('id')>0?$msg = 'Altera��o realizada com sucesso!':$msg = 'Cadastro realizado com sucesso!';
			}else {
				$msg = 'Erro no cadastro!';
			}			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/arquivos"; </script>');
			
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	
	function usuarios(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			if($this->input->post('del')){
				$cods = implode(',', $_POST["item"]);
  				$this->usuario->excluirItens($cods);
			}
			$this->load->library('pagination');
			$this->input->post('busca',TRUE)?$busca = $this->input->post('busca',TRUE):$busca="";
			if(!$busca){
				$busca = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			}
			$ordem = (!$this->uri->segment("4")) ? 'USU_ISN ASC' : $this->uri->segment("4");
			$inicio = (!$this->uri->segment("5")) ? 0 : $this->uri->segment("5");
			$config['base_url']   = $this->data['local'].'index.php/internas/usuarios/'.$busca.'/'.$ordem.'/';
			if(empty($busca)) { $busca=""; }
			$config['total_rows'] = $this->usuario->contaRegistros($busca);
			$config['per_page']   = 10;
			$config['first_link'] = 'Primeiro';
			$config['last_link']  = '�ltimo';
			$config['next_link']  = 'Pr�ximo';
			$config['prev_link']  = 'Anterior';
			$config['uri_segment'] = 5;
			$this->pagination->initialize($config); 
			$this->data["paginacao"] =  $this->pagination->create_links();
			$this->data["usuarios"] = $this->usuario->getUsuarios(10,$inicio,$busca,$ordem);
			$this->data['inicio'] = $inicio;
			$busca?$busca=$busca:$busca=0;
			$this->data['busca'] = $busca;
			$this->data['deptos'] = $this->emails;
			$this->load->view('cadastros/usuarios',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function formularioCorr(){
		
		if($_SESSION[''.$this->data['sessaoAdm']]){
                $this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
                if($this->data['usuario'])
                {
                    $this->load->model('tab_corretores');
                    
					$id = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
                    $this->data["pagina"]="";
                    $this->data["corretores"]="";
                    if($id>0)
                    {
                        $this->data["corretores"] = $this->tab_corretores->buscarCorretor($id);
                    }			
                        $this->load->view('cadastros/inc_corretores',$this->data);
                }
                else 
                {
                    print("Acesso negado!");
                    die;
                }
            }
            else 
            {
                print("Sua sess�o foi expirado, favor logar novamente no sistema.");
                die;
            }
	}
	
	function cadCorr(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->load->model('tab_corretores');
			//$this->input->post('exp')?$exp=explode('/',$this->input->post('exp',TRUE)):$exp=date('Y-m-d');
			//if(is_array($exp)){ $exp = $exp[2].'-'.$exp[1].'-'.$exp[0]; }
			if($this->input->post('id')>0){
				$veri = $this->tab_corretores->alterarCorretor($this->input->post('id') ,htmlentities($this->input->post('nome',TRUE),TRUE), $this->input->post('loginn'),$this->input->post('senhaa'));
			}else {                
				$veri = $this->tab_corretores->cadastrarCorretor(htmlentities($this->input->post('nome',TRUE),TRUE), $this->input->post('loginn'), $this->input->post('senhaa'));
			}
			if($veri>0){
				$this->input->post('id')>0?$msg = 'Altera��o realizada com sucesso!':$msg = 'Cadastro realizado com sucesso!';
			}else {
				$msg = 'Erro no cadastro!';
			}			
			print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/corretores"; </script>');
			
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function formularioUsu(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			$this->layout = 'popup';
			$this->data["usuarioAlt"]="";
			$id = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
			if($id>0){
				$this->data["usuarioAlt"] = $this->usuario->buscarUsuario($id);	
			}
			//print_r($this->data["usuarioAlt"]);die;
			$this->load->view('cadastros/inc_usuario',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
	
	function cadUsu(){
		if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
			if($this->input->post('id')>0){
				$veri = $this->usuario->alterarUsuario(htmlentities($this->input->post('nome',TRUE),TRUE),htmlentities($this->input->post('login',TRUE),TRUE),htmlentities($this->input->post('senha',TRUE),TRUE),$this->input->post('id'), $this->input->post('permissao'));
				if($veri){
					if($veri['id']==$_SESSION[''.$this->data['sessaoAdm']]['id']){
						$_SESSION[''.$this->data['sessaoAdm']] = $veri;
					}
				}
			}else {
				$veri = $this->usuario->cadastrarUsuario(htmlentities($this->input->post('nome',TRUE),TRUE),htmlentities($this->input->post('login',TRUE),TRUE),md5(htmlentities($this->input->post('senha',TRUE),TRUE)), $this->input->post('permissao'));
			}
			if($veri){
				$this->input->post('id')>0?$msg = 'Altera��o realizada com sucesso!':$msg = 'Cadastro realizado com sucesso!';
			}else {
				$msg = 'Erro no cadastro!';
			}
			
			if($this->data['usuario']['Permissao']==1)
				print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/usuarios"; </script>'); 
			else
				print('<script type="text/javascript">alert("'.$msg.'"); window.location = "'.site_url().'index.php/internas/formularioUsu/'. $this->data['usuario']['id'] .'"; </script>'); 
			
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess�o foi expirado, favor logar novamente no sistema.");
			die;
		}
	}
    
    public function corretor_mes() {
        if($_SESSION[''.$this->data['sessaoAdm']]){
		$this->data['usuario'] = $this->usuario->seguranca($_SESSION[''.$this->data['sessaoAdm']]);
		if($this->data['usuario']){
            $this->load->model('tab_func');
            $this->data['message'] = '';
            if(isset($_POST['nome']) and !empty($_POST['nome'])) {
                
                $msg = $this->tab_func->cadastrarCorretor($_POST,$_FILES);
                $this->data['message'] = $msg;
            }
            
			$this->layout = 'popup';
            
            $corretor = $this->tab_func->getCorretor();
			$this->data["corretor"] = $corretor;
			//print_r($this->data["usuarioAlt"]);die;
			$this->load->view('cadastros/inc_corretor_mes',$this->data);
		}else {
			print("Acesso negado!");
			die;
		}
		}else {
			print("Sua sess&atilde;o foi expirado, favor logar novamente no sistema.");
			die;
		}
    }
		
}
/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>