<?php
class Welcome extends Controller {
	public $layout = 'default';
	public $title = 'DMV';
	public $css = array('default','jquery.lightbox-0.5');
	public $js = array('jquery-1.3.2.min','js');
	public $data = array('local'=>"",'sessaoAdm'=>'dmvSesssion');

	function Welcome()
	{
		parent::Controller();
		$this->load->helper('url');	   
	    $this->data['local'] = site_url();
	}
	
	function index()
	{
		$this->load->view('principal/login',$this->data);
	}
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>